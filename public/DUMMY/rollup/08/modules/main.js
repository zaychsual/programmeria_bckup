import React from 'react';
import ReactDOM from 'react-dom';
import {Btn} from './Btn';

function App(){
	const [count, setCount] = React.useState(0);
	
	return (
		<div className='container'>
			<Btn onClick={() => setCount(count + 1)} className='btn btn-primary'>Count: {count}</Btn>
		</div>
	);
}

ReactDOM.render(React.createElement(App, null), document.getElementById('frameRoot'));