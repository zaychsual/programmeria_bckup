import React from 'react';

export function Btn({className, onClick, ...etc}){
	return <button {...etc} onClick={onClick} className={className} type='button' />;
}