/*
  @license
	Rollup.js v2.6.1
	Sun, 12 Apr 2020 21:37:03 GMT - commit e440b709a2861726183d57b4b1f25d049ed1ed6c


	https://github.com/rollup/rollup

	Released under the MIT License.
*/
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var rollup_js = require('./shared/rollup.js');
require('path');
require('crypto');
require('fs');
require('events');



exports.VERSION = rollup_js.version;
exports.rollup = rollup_js.rollup;
exports.watch = rollup_js.watch;
//# sourceMappingURL=rollup.js.map
