/*
  @license
	Rollup.js v2.6.1
	Sun, 12 Apr 2020 21:37:03 GMT - commit e440b709a2861726183d57b4b1f25d049ed1ed6c


	https://github.com/rollup/rollup

	Released under the MIT License.
*/
'use strict';

require('./shared/rollup.js');
require('path');
require('crypto');
require('fs');
require('events');
require('./shared/mergeOptions.js');
var loadConfigFile_js = require('./shared/loadConfigFile.js');
require('url');



module.exports = loadConfigFile_js.loadAndParseConfigFile;
//# sourceMappingURL=loadConfigFile.js.map
