/*
  @license
	Rollup.js v2.6.1
	Sun, 12 Apr 2020 21:37:03 GMT - commit e440b709a2861726183d57b4b1f25d049ed1ed6c


	https://github.com/rollup/rollup

	Released under the MIT License.
*/
'use strict';

var path = require('path');
var events = require('events');
var util = require('util');



exports.require$$0 = events;
exports.require$$1 = util;
exports.sysPath = path;
//# sourceMappingURL=_events_commonjs-external.js.map
