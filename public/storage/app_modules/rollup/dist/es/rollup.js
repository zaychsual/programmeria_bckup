/*
  @license
	Rollup.js v2.6.1
	Sun, 12 Apr 2020 21:37:03 GMT - commit e440b709a2861726183d57b4b1f25d049ed1ed6c


	https://github.com/rollup/rollup

	Released under the MIT License.
*/
export { v as VERSION, r as rollup, w as watch } from './shared/rollup.js';
import 'path';
import 'crypto';
import 'fs';
import 'events';
