// 
export default function copy2Clipboard(str){
  // console.log('Q: ',Q);
  return new Promise((resolve, reject) => {
    let DOC = document,
        el = DOC.createElement("textarea"),
        iOS = window.navigator.userAgent.match(/ipad|iphone/i);
    
    el.contentEditable = true; // needed for iOS >= 10
    el.readOnly = false; // needed for iOS >= 10
    el.value = str;
    // el.style.border = "0";
    // el.style.padding = "0";
    // el.style.margin = "0";
    el.style.opacity = "0";
    el.style.position = "absolute";
    
    // sets vertical scroll
    // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
    // el.style.top = yPosition + "px";
  
    DOC.body.appendChild(el);

    let OK = DOC.execCommand("copy");
    
    if(OK){
      if(iOS){
        let range = DOC.createRange();
        range.selectNodeContents(el);
        
        let selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        el.setSelectionRange(0, 999999);

        resolve(OK);
      }else{
        el.select();
        resolve(OK);
      }
    }
    else{
      reject(new Error('Failed to copy'));// + urlSrc
    }
  
    // let OK = DOC.execCommand("copy");
    DOC.body.removeChild(el);
    // return OK;
  });
}

// export { copyToClipboard };