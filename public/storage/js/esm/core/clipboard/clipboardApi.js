// import Import from '../../component-etc/route/Import';

// , {
// 	onOk = () => {},
//   onErr = () => {}
// } = {}

// == Copy ==
export default function clipboardApi(target){
	return new Promise((resolve, reject) => {
		let clipboard = navigator.clipboard,
				txt;
		
		if(typeof target === 'string' && !target.tagName) txt = target;
		else if(target?.tagName) txt = target.textContent; // target.innerText
		else reject(new Error('Text or target DOM to copy is required.'));

		if(clipboard){
			// if(!target){
			// 	// console.warn('Text or target DOM to copy is required.');
			// 	reject(new Error('Text or target DOM to copy is required.'));
			// 	return;
			// }
			// console.log(txt.replace(/\n/gm, ''));
			
			// return clipboard.writeText(txt);
			clipboard.writeText(txt).then(() => resolve(txt)).catch(e => reject(e));
		}
		else{
			console.log('%cnavigator.clipboard NOT SUPPORT','color:yellow');
			// const m = await import('/storage/js/esm/copy2Clipboard.js');
			// console.log(typeof m.default);
			// return m.default(txt);
			// return import('/storage/js/esm/copy2Clipboard.js');

			// return new Promise((resolve, reject) => {
			// 	import('/storage/js/esm/copy2Clipboard.js').then((m) => {
			// 		m.default(txt).then(ok => {
			// 			console.log('ok: ', ok);
			// 			resolve(ok);
			// 		}).catch(e => reject(e));
			// 	}).catch(e => reject(e));
			// });

			// return (async () => {
			// 	const m = await import('/storage/js/esm/copy2Clipboard.js');
			// 	console.log(m.default);
			// 	return m.default(txt);
			// })();
		}
	});
}

// For npm
// export { Copy };

// navigator.clipboard.writeText(txt)
// .then(() => onOk(txt))
// .catch(err => onErr(err));
