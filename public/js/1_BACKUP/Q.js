/** DEV ONLY */
// const LOG_DEV = 'color:yellow';// ;font-size:15px

/** END DEV ONLY */

/** GET favicon offline file */
// const FAV_OFF = localStorage.getItem('favicon-offline');
/** Check support native lazy loading image, iframe */
// const SUPPORT_LOADING = 'loading' in HTMLImageElement.prototype;

// https://medium.com/javascript-in-plain-english/you-must-understand-these-14-javasript-functions-1f4fa1c620e2
/* function cached(fn){
// Create an object to store the results returned after each function execution.
  const cache = Object.create(null);
  // Returns the wrapped function
  return function cachedFn(str){
    // If the cache is not hit, the function will be executed
    if(!cache[str]){
			let result = fn(str);
			// Store the result of the function execution in the cache
			cache[str] = result;
    }
    return cache[str]
  }
} */

/** FROM: Vue.js */
function cached(fn){
	let cache = Object.create(null);
	return (function cachedFn(s){
		let hit = cache[s];
		return hit || (cache[s] = fn(s))
	})
}

/** === Type checking === */
function isStr(v){
	return typeof v === 'string' || v instanceof String;
}
function isObj(v){
	return v && typeof v === 'object' && v.constructor === Object;
}
function isFunc(v){
	return v && typeof v === 'function';
}
/** Q classnames */
function Cx(){
  let hasOwn = {}.hasOwnProperty,
			c = [],
			alength = arguments.length;
  for(let i = 0; i < alength; i++){
		let arg = arguments[i];
		if(!arg) continue;

		/* let argType = typeof arg; */
		if(isStr(arg) || typeof arg === 'number'){
			c.push(arg);
    }else if(Array.isArray(arg) && arg.length){
			let inr = Cx.apply(null, arg);
			if(inr) c.push(inr);
		}else if(isObj(arg)){
			for(let k in arg){
				if(hasOwn.call(arg, k) && arg[k]) c.push(k);
			}
		}
  }
  return c.length > 0 ? c.join(' ') : undefined;
}

/** FROM: blueprint.js utils - Safely invoke the function with the given arguments, if it is indeed a
 * function, and return its value. Otherwise, return undefined */
/* function safeInvoke(fn, ...args){
	if(isFunc(fn)){
		return fn(...args);
	}
	return undefined;
} */
/** === END Type checking === */

/** == dom-q.js === */
function domQ(q, dom = document){
  return dom.querySelector(q);
}
/** DEV OPTIONS: only this / return array with check length */
function domQall(q, dom = document){
	return dom.querySelectorAll(q);
}
/** USAGE:
	add = setClass(element, "btn active");
	remove = setClass(element, "btn active", 'remove'); */
function setClass(el, c, fn = 'add'){
  let cls = c.split(" ");
  el.classList[fn](...cls);
}
function toggleClass(el, c, cek){
  el.classList.toggle(c, cek);
}
function replaceClass(el, o, n){
  hasClass(el, o) ? el.classList.replace(o, n) : el.classList.replace(n, o);
}
function hasClass(el, c){
  return el.classList.contains(c);
}
function hasAttr(el, a){
  return el.hasAttribute(a);
}
function getAttr(el, a){
  return el.getAttribute(a);
}
/**
	@el : Element / node
	@attr : attribute name & value (Object)
*/
function setAttr(el, attr){
	if(el){
		if(isObj(attr)) Object.entries(attr).forEach(v => el.setAttribute(v[0], v[1]));
		else if(isStr(attr)) attr.split(" ").forEach(v => el.removeAttribute(v));
		else console.warn('setAttr() : params 2 required Object to add / string to remove, To remove several attributes, separate the attribute names with a space.');
	}else{
		console.warn('setAttr() : params 1 required DOM');
	}
}

function makeEl(t){
	return document.createElement(t);
}
/** === END dom-q.js === */

function getScript(obj, to = 'body'){
  return new Promise((resolve, reject) => {
    let el = isStr(to) ? document[to] : to;
    let urlSrc = obj.src || obj.href;
    // let As = "stylesheet preload".includes(obj.rel) && (urlSrc.endsWith('.css') || obj.type === "text/css" || obj.as === "style") ? 'link' : 'script';
    let tag = obj.tag ? obj.tag : 'script';

		if(domQ(`${tag}[${tag === 'link' ? 'href':'src'}="${urlSrc}"]`, el)) return;/* MAKE SURE */
		
		let dom = makeEl(tag);// document.createElement(tag);
		function loadend(){
			dom.onerror = dom.onload = null;
		}
		dom.onload = function(e){
			loadend();
			resolve({e, dom});
		}
		dom.onerror = function(){
			loadend();
			dom.remove();
			reject(new Error('Failed to load '+ urlSrc));// obj.src
		}

		window.onerror = function(msg, url, line, column, errObj){
			let str = msg.toLowerCase(),
					scriptErr = "script error";
			if(str.indexOf(scriptErr) > -1){
				dom.remove();
				reject({
					msg: 'Script Error: See browser console for detail',
					/* type: 'Script Error', */
					path: urlSrc /* OPTION */
				});
			}else{
				dom.remove();
				reject({msg, url, line, column,
					errorObj: JSON.stringify(errObj),
					path: obj.src /* OPTION */
				});
			}
			window.onerror = null;
			return false;
		}
		
		/* Set more valid attributes to script tag */
		setAttr(dom, omit(obj, ['tag','onerror','onload','innerText','innerHTML']));// 'async', 'crossOrigin', 'src','text'
		/* // Available attributes script tag
		{
			type: "",
			noModule: false,
			async: true,
			defer: false,
			crossOrigin: null,
			text: "",
			referrerPolicy: null,
			event: "",
			integrity: ""
		} */
		
		if(obj.innerText) dom.innerText = obj.innerText;
		if(obj.innerHTML) dom.innerHTML = obj.innerHTML;
		
		let noInner = !obj.innerText && !obj.innerHTML;
    if(tag !== 'link' && !obj.async && noInner) dom.async = 1;
    if((!obj.crossOrigin || tag !== 'style') && noInner) dom.crossOrigin = "anonymous";
		el.appendChild(dom);
  });
}

/** === Generate id === */
function Qid(){
  function S4(){
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  }
  return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + Date.now())
}
/** === END Generate id === */

/** No Action */
function preventQ(e){
  e.preventDefault();
  e.stopPropagation();
}
function noop(){};
/** END No Action */

/** Bind multiple component methods:
	* @param {this} context
	* @param {Array} functions
	constructor(){
		...
		bindFuncs.call(this,['onClick','onModal']);
	} */
function bindFuncs(fns){
	fns.forEach(f => (this[f] = this[f].bind(this)));
}

/** reactstrap utils */
// function omit(obj, omitKeys){
  // let result = {};
  // Object.keys(obj).forEach(key => {
    // if(omitKeys.indexOf(key) === -1) result[key] = obj[key];
  // });
  // return result;
// }
/** END reactstrap utils */

/** Polyfill (Edge) */
// typeof HTMLDetailsElement == "undefined" && getScripts({src:"/js/libs/polyfill/details.js","data-js":"details"}).then(e => console.log('+ details polyfill')).catch(e => console.warn(e));

// !"srcdoc" in document.createElement("iframe") && getScripts({src:"/js/libs/polyfill/srcdoc.js","data-js":"srcdoc"}).then(v => console.log('+ srcdoc polyfill')).catch(e => console.warn(e));
/** END Polyfill (Edge) */

/** DEV: Check ES Module support */
// try{
	// new Function('import("/js/esm/tryModule.js")')();// new Function('import("' + src + '")')();
// }catch(e){
	// getScripts({src:"/storage/app_modules/es-module-shims/dist/es-module-shims.min.js","data-js":"es-module-shims"})
	// .then(v => console.log(v)).catch(e => console.warn(e));
// }
/** END DEV: Check ES Module support */

/** add / remove EventListener
	{
		@to = target to bind event | default = document
		@listen = add / remove event | default = 'add'
		@type 	= event type name, e.g 'click','keyup' etc (required)
		@fn 		= function in EventListener (required)
	}
*/
// function toggleEvent({
	// to = document,
	// listen = 'add',
	// type,
	// fn
// } = {}){
	// to[listen + 'EventListener'](type, fn);
// }

// function isSupportLocaleDateString(date){
  // try {
    // date.toLocaleDateString('i');// new Date().toLocaleDateString('i');
  // }catch(e){
    // return e.name === 'RangeError';
  // }
  // return false;
// }

// function isValidLang(lang){
// 	if(!window.Intl) return;
// 	try {
// 		let supportLocOf = Intl.DateTimeFormat.supportedLocalesOf(lang, {localeMatcher:'lookup'});// lookup | best fit
// 		let isOk = supportLocOf && Intl.getCanonicalLocales(lang) ? supportLocOf.length : false;
// 		return isOk;
// 	}catch(e){
// 		console.warn(e.message);// expected output: RangeError: invalid language tag: lang
// 	}
// }

// function dateIntl(dt, options, lang = 'en'){
// 	if(!dt || !isValidLang(lang)) return;// && !isSupportLocaleDateString(dt)

// 	// console.log('isValidLang: ',!isValidLang(lang));
// 	try {
// 		let setOptions = {
// 			weekday:'long', // long | short | narrow
// 			year:'numeric', // numeric | 2-digit
// 			month:'long', // numeric | 2-digit | long | short | narrow
// 			day:'numeric', // numeric | 2-digit
// 			// hour: '', // numeric | 2-digit
// 			// minute: '', // numeric | 2-digit
// 			// second: '', // numeric | 2-digit
// 			...options
// 		};
// 		// console.log(setOptions);

// 		return new Intl.DateTimeFormat(lang, setOptions).format(dt);
// 	}catch(e){
// 		console.warn(e.message);
// 	}
// }

// DEV ONLY:
/* function dummyArray(space = ' ', lng){
	// const str = () => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	let str = `Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non
proident sunt in culpa qui officia deserunt mollit anim id est laborum`;

	if(Number.isInteger(lng) && lng > 0) return Array.from({length: lng}, (v, k) => v + space + (k + 1));
	else return Array.from(str.split(" "), (v, k) => v + space + (k + 1));
} */

/* FROM ContentEditable.js */
/* function normalizeHtml(str){
  return str && str.replace(/&nbsp;|\u202F|\u00A0/g, ' ');
}
function replaceCaret(el){
  // Place the caret at the end of the element
  let target = document.createTextNode('');
  el.appendChild(target);
  // do not move caret if element was not focused
  let isTargetFocused = document.activeElement === el;
  if(target !== null && target.nodeValue !== null && isTargetFocused){
    let sel = window.getSelection();
    if(sel !== null){
      let range = document.createRange();
      range.setStart(target, target.nodeValue.length);
      range.collapse(true);
      sel.removeAllRanges();
      sel.addRange(range);
    }
    if(el instanceof HTMLElement) el.focus();
  }
} */

// FOR check mobile device
/* function isMobile(){
  return !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
} */

/* function dateToStr(dt){
	let date = new Date(dt);
	return date.toDateString();
} */

/** Check html tagName **/
/* function isTag(t){ // tagCheck
	// let c = document.createElement(t),
			// b = c.toString() !== "[object HTMLUnknownElement]"
  // return {valid:b, el:c};
	return document.createElement(t).toString() !== "[object HTMLUnknownElement]";
} */

/** Babel Helpers:  */
// function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }
// function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// function _interopRequireDefault(obj){
	// return obj && obj.__esModule ? obj : {default: obj};
// }
/** END Babel Helpers:  */

// export {
// 	// SUPPORT_LOADING, 
// 	cached, 
// 	isStr, isObj, isFunc, 
// 	Cx, domQ, domQall, setClass, toggleClass, replaceClass, hasClass, hasAttr, getAttr, setAttr, 
// 	Qid, preventQ, noop, 
// 	bindFuncs, 
// 	makeEl
// 	// omit, getScripts
// };

