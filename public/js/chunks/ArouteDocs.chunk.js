(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ArouteDocs"],{

/***/ "./resources/js/src/pages/admin/docs/q-ui-react/ArouteDocs.js":
/*!********************************************************************!*\
  !*** ./resources/js/src/pages/admin/docs/q-ui-react/ArouteDocs.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ArouteDocs; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _DocsPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../DocsPage */ "./resources/js/src/pages/admin/docs/DocsPage.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
 // , { useState, useEffect, Fragment }


 // import Browser from '../../../../components/browser/Browser';



function ArouteDocs() {
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in ArouteDocs','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocsPage__WEBPACK_IMPORTED_MODULE_2__["default"], {
    browser: true,
    api: "/storage/json/docs/components/q-ui-react/Aroute.json",
    scope: {
      Dropdown: react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"],
      Flex: _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__["default"],
      Aroute: _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_4__["default"]
    } // Browser

  });
}

/***/ })

}]);
//# sourceMappingURL=ArouteDocs.chunk.js.map