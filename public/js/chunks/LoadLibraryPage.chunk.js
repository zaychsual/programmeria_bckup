(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["LoadLibraryPage"],{

/***/ "./resources/js/src/components/libraries/BabelStandalone.js":
/*!******************************************************************!*\
  !*** ./resources/js/src/components/libraries/BabelStandalone.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BabelStandalone; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loadable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @loadable/component */ "./node_modules/@loadable/component/dist/loadable.esm.js");
/* harmony import */ var _q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
 // useState, useEffect, 
// import loadable from '@loadable/component';

 // loadable

 // const Moment = loadable.lib(() => import('@babel/standalone'));

var Lib = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return __webpack_require__.e(/*! import() | BabelStandalone */ "vendors~BabelStandalone").then(__webpack_require__.t.bind(null, /*! @babel/standalone */ "./node_modules/@babel/standalone/babel.js", 7));
}); // props.module

function BabelStandalone(_ref) {
  var children = _ref.children,
      load = _ref.load;

  // , onLoad
  // const loadProps = Q.isBool(load);
  // const [isLoad, setIsLoad] = useState(loadProps);
  // useEffect(() => {
  //   if(!isLoad) setIsLoad(true);
  // }, [isLoad]);
  if (load) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
      fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
        bg: "/icon/android-icon-36x36.png"
      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Lib, null, function (Babel) {
      if (Babel && children) {
        // window.Babel = Babel;
        // if(onLoad) onLoad(Babel);
        return children(Babel);
      }

      return null;
    }));
  }

  return null;
}
/* 
if(load){
  if(window.Babel){
    return children(window.Babel);
  }

  return (
    <Suspense fallback={<SpinLoader bg="/icon/android-icon-36x36.png" />}>
      <Lib>
        {(Babel) => {
          if(Babel && children){
            window.Babel = Babel;

            return children(Babel);
          }
          return null;
        }}
      </Lib>
    </Suspense>
  )
}
*/
// export default BabelStandalone;
// export { LoadLibrary };

/***/ }),

/***/ "./resources/js/src/components/libraries/RollupBrowser.js":
/*!****************************************************************!*\
  !*** ./resources/js/src/components/libraries/RollupBrowser.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RollupBrowser; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loadable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @loadable/component */ "./node_modules/@loadable/component/dist/loadable.esm.js");
/* harmony import */ var _q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
 // useState, useEffect, 



var Lib = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return Promise.all(/*! import() | RollupBrowser */[__webpack_require__.e("vendors~BundlerPage~RollupBrowser"), __webpack_require__.e("vendors~RollupBrowser")]).then(__webpack_require__.t.bind(null, /*! rollup/dist/rollup.browser.js */ "./node_modules/rollup/dist/rollup.browser.js", 7));
});
function RollupBrowser(_ref) {
  var children = _ref.children,
      load = _ref.load;

  if (load) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
      fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
        bg: "/icon/android-icon-36x36.png"
      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Lib, null, function (rollup) {
      if (rollup && children) {
        return children(rollup);
      }

      return null;
    }));
  }

  return null;
}

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/LoadLibraryPage.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/LoadLibraryPage.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LoadLibraryPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_monaco_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/monaco-react */ "./resources/js/src/components/monaco-react/index.js");
/* harmony import */ var _components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/monaco-react/ide/onDidMount */ "./resources/js/src/components/monaco-react/ide/onDidMount.js");
/* harmony import */ var _components_libraries_BabelStandalone__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/libraries/BabelStandalone */ "./resources/js/src/components/libraries/BabelStandalone.js");
/* harmony import */ var _components_libraries_RollupBrowser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/libraries/RollupBrowser */ "./resources/js/src/components/libraries/RollupBrowser.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // , { useState, useEffect, useRef, } 








function LoadLibraryPage() {
  var ideRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // const [load, setLoad] = useState(false);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      editorReady = _useState2[0],
      setEditorReady = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      code = _useState4[0],
      setCode = _useState4[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    console.log('%cuseEffect in LoadLibraryPage', 'color:yellow;'); // Q.getScript({
    // 	tag:"link", 
    // 	rel:"stylesheet", 
    // 	href:"https://res.cloudinary.com/finnhvman/raw/upload/matter/matter-0.2.2.min.css"
    // }, "head")
    // 	.then(v => {
    // 		console.log(v);
    // 	}).catch(e => {
    // 		console.log(e);
    // 	});
  }, []);

  var onDidChangeEditor = function onDidChangeEditor(v, editor) {
    if (!editorReady) {
      setEditorReady(true);
      ideRef.current = editor;
      Object(_components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_5__["default"])(v, editor); // console.log('onDidChangeEditor editor: ', editor);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Load Library"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "Load Library"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_libraries_BabelStandalone__WEBPACK_IMPORTED_MODULE_6__["default"], {
    load: true // onLoad={Babel => {
    // 	console.log('BabelStandalone onLoad: ', Babel);
    // }}

  }, function (Babel) {
    return Babel ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__["default"], {
      dir: "column"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_2__["default"], {
      onClick: function onClick() {
        console.log('Babel: ', Babel);
      }
    }, "Babel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_monaco_react__WEBPACK_IMPORTED_MODULE_4__["ControlledEditor"] // loading={false} 
    , {
      className: "monacoFixedCtxMenu",
      height: "75vh" // HEIGHT_1 
      ,
      editorDidMount: onDidChangeEditor,
      value: code,
      onChange: function onChange(e, val) {
        return setCode(val);
      } // onChangeCode(e, code) | this.setState({code})
      // language={lang} 
      // theme={theme} 
      // options={MONACO_OPTIONS} 

    })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Error load Babel");
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_libraries_RollupBrowser__WEBPACK_IMPORTED_MODULE_7__["default"], {
    load: true
  }, function (rollup) {
    return rollup ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_2__["default"], {
      onClick: function onClick() {
        console.log('rollup: ', rollup);
      }
    }, "rollup") : null;
  }));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=LoadLibraryPage.chunk.js.map