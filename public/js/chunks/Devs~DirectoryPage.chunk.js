(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs~DirectoryPage"],{

/***/ "./resources/js/src/components/directory/Directory.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/directory/Directory.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Directory; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dnd */ "./node_modules/react-dnd/dist/esm/index.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _q_ui_react_ContextMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../q-ui-react/ContextMenu */ "./resources/js/src/components/q-ui-react/ContextMenu.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useMemo, useEffect, useRef, useContext, useLayoutEffect }

 // import { DndProvider } from 'react-dnd';

 // import DndBackend from './parts/DndBackend';
// import Details from '../q-ui-react/Details';




function getExt(fname, bool) {
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

var treeType = {
  DIR: "directory",
  FILE: "file"
};

function TreeItem(_ref) {
  var _Q$Cx;

  var type = _ref.type,
      data = _ref.data,
      className = _ref.className,
      isDrop = _ref.isDrop,
      children = _ref.children,
      title = _ref.title,
      titleAttr = _ref.titleAttr,
      icon = _ref.icon,
      appendCtxMenu = _ref.appendCtxMenu,
      _ref$paste = _ref.paste,
      paste = _ref$paste === void 0 ? false : _ref$paste,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? Q.noop : _ref$onClick,
      _ref$onCut = _ref.onCut,
      onCut = _ref$onCut === void 0 ? Q.noop : _ref$onCut,
      _ref$onCopy = _ref.onCopy,
      onCopy = _ref$onCopy === void 0 ? Q.noop : _ref$onCopy,
      _ref$onPaste = _ref.onPaste,
      onPaste = _ref$onPaste === void 0 ? Q.noop : _ref$onPaste,
      _ref$onRename = _ref.onRename,
      onRename = _ref$onRename === void 0 ? Q.noop : _ref$onRename,
      _ref$onDelete = _ref.onDelete,
      onDelete = _ref$onDelete === void 0 ? Q.noop : _ref$onDelete,
      _ref$As = _ref.As,
      As = _ref$As === void 0 ? "div" : _ref$As;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      focusCtxMenu = _useState2[0],
      setFocusCtxMenu = _useState2[1];

  var _useDrag = Object(react_dnd__WEBPACK_IMPORTED_MODULE_1__["useDrag"])({
    item: _objectSpread({
      type: type
    }, data),
    // type: `${color}`
    canDrag: type === treeType.DIR || type === treeType.FILE,
    // !forbidDrag
    collect: function collect(monitor) {
      return {
        isDragging: monitor.isDragging()
      };
    }
  }),
      _useDrag2 = _slicedToArray(_useDrag, 3),
      isDragging = _useDrag2[0].isDragging,
      drag = _useDrag2[1],
      preview = _useDrag2[2]; // const containerStyle = useMemo(
  //   () => ({
  //     opacity: isDragging ? 0.4 : undefined,
  //     // cursor: isDragging ? "drag" : undefined
  //   }),
  //   [isDragging] // , forbidDrag, backgroundColor
  // );


  var BTNS = [{
    t: "Rename",
    i: "pen",
    fn: onRename
  }, // () => 
  {
    t: "Delete",
    i: "trash",
    fn: onDelete
  }]; // .filter(() => type !== "root");

  var PASTE = paste && type === treeType.DIR ? {
    t: "Paste",
    i: "check",
    fn: onPaste
  } : null;
  var CUT_COPY = [{
    t: "Cut",
    i: "pen",
    fn: onCut
  }, {
    t: "Copy",
    i: "copy",
    fn: onCopy
  }, PASTE].filter(function (v) {
    return v;
  }); //  && type !== "root"

  var unFocus = function unFocus() {
    return setFocusCtxMenu(false);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_ContextMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    className: "zi-1001" // zi-5
    ,
    esc: false // appendTo={this.wrap.current || document.body} 
    // popperConfig={{
    // 	strategy: "fixed",
    // 	// modifiers: [
    // 	//   {
    // 	//     name: 'preventOverflow',
    // 	//     options: {
    // 	//       // padding: 100, 
    // 	//       // boundary: document.body, // this.wrap.current
    // 	//       // altBoundary: true, // false by default
    // 	//       // rootBoundary: 'document', // 'viewport' by default
    // 	//       // mainAxis: false, // true by default
    // 	//     }
    // 	//   },
    // 	// ]
    // }} 
    // , ctxMenu
    ,
    component: function component(hide) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "dropdown-menu show py-1 v-dd-sets mnw-auto w-auto" // ctxMenu | Q.preventQ | (e) => {Q.preventQ(e);hide()}
        ,
        onContextMenu: function onContextMenu(e) {
          Q.preventQ(e);
          hide(); // ctxMenu(e);

          unFocus(); // setFocusCtxMenu(true);
        }
      }, appendCtxMenu && appendCtxMenu(hide, unFocus), [].concat(_toConsumableArray(CUT_COPY), BTNS).map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          key: v.t,
          onClick: function onClick(e) {
            hide();
            unFocus();
            v.fn(e); // , Q.domQ('#' + summaryID)
          },
          className: "dropdown-item qi q-fw qi-" + v.i,
          type: "button"
        }, v.t);
      }));
    },
    onContextMenu: function onContextMenu(active) {
      // , e
      // console.log('ContextMenu onContextMenu active: ', active);
      setFocusCtxMenu(!active);
    } // onEsc={() => {
    // 	setFocusCtxMenu(false);
    // }}
    ,
    onScrolls: function onScrolls() {
      setFocusCtxMenu(false);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, {
    ref: preview // style={containerStyle} 
    ,
    role: "treeitem" // id={summaryID} 
    // className={Q.Cx("input-group input-group-xs tree-item", className)}
    ,
    className: Q.Cx("tree-item", {
      // "bg-strip": isOver && !isDragging, 
      "bg-main": isDragging,
      "focus": focusCtxMenu
    }, isDrop) // className="tree-item"
    ,
    onMouseLeave: function onMouseLeave(e) {
      var et = e.target;
      var dd = Q.domQ('[aria-expanded="true"]', et.closest('.tree-item'));
      if (dd) dd.click();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    // ref={dropRef} 
    // style={{ opacity: isOver ? 0.7 : undefined }} 
    className: Q.Cx("input-group input-group-xs flex-nowrap", className) // className={
    // 	Q.Cx("input-group input-group-xs", { 
    // 		// "bg-strip": isOver && !isDragging, 
    // 		"bg-main": isDragging && !isOver 
    // 	}, className)
    // } 

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    ref: drag,
    className: Q.Cx("tree-label form-control btn btn-flat text-left", (_Q$Cx = {}, _defineProperty(_Q$Cx, "i-color qi qi-" + icon, icon), _defineProperty(_Q$Cx, "isFile", As === "div"), _Q$Cx)) // fab
    ,
    title: titleAttr ? titleAttr : title,
    onClick: onClick
  }, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-append tree-tool"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    alignRight: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    variant: "flat",
    size: "xs",
    className: "rounded",
    tabIndex: "-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
    className: "dropdown-menu show py-0 v-dd-sets mnw-auto w-auto" // onClick={e => Q.preventQ(e)} 
    ,
    popperConfig: {
      modifiers: [{
        name: "offset",
        options: {
          offset: [0, -0.05]
        }
      }]
    }
  }, children, CUT_COPY.map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      key: i,
      as: "button",
      type: "button",
      className: "qi qi-" + v.i,
      tabIndex: "-1",
      onClick: v.fn // e => v.fn(e, Q.domQ('#' + summaryID))

    }, v.t);
  }))), BTNS.map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      key: v.t,
      onClick: v.fn,
      blur: true,
      kind: "flat",
      className: "rounded qi qi-" + v.i + (As === "div" ? " isFile" : ""),
      title: v.t,
      tabIndex: "-1"
    });
  })))));
}

function Dir(_ref2) {
  var _extends2;

  var inputFileId = _ref2.inputFileId,
      data = _ref2.data,
      inRef = _ref2.inRef,
      isOver = _ref2.isOver,
      _ref2$type = _ref2.type,
      type = _ref2$type === void 0 ? treeType.DIR : _ref2$type,
      title = _ref2.title,
      className = _ref2.className,
      _ref2$open = _ref2.open,
      open = _ref2$open === void 0 ? false : _ref2$open,
      renderOpen = _ref2.renderOpen,
      children = _ref2.children,
      summaryID = _ref2.summaryID,
      _ref2$paste = _ref2.paste,
      paste = _ref2$paste === void 0 ? false : _ref2$paste,
      _ref2$onToggle = _ref2.onToggle,
      onToggle = _ref2$onToggle === void 0 ? Q.noop : _ref2$onToggle,
      _ref2$onCut = _ref2.onCut,
      onCut = _ref2$onCut === void 0 ? Q.noop : _ref2$onCut,
      _ref2$onPaste = _ref2.onPaste,
      onPaste = _ref2$onPaste === void 0 ? Q.noop : _ref2$onPaste,
      etc = _objectWithoutProperties(_ref2, ["inputFileId", "data", "inRef", "isOver", "type", "title", "className", "open", "renderOpen", "children", "summaryID", "paste", "onToggle", "onCut", "onPaste"]);

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(renderOpen),
      _useState4 = _slicedToArray(_useState3, 2),
      one = _useState4[0],
      setOne = _useState4[1]; // false


  var Toggle = function Toggle(e) {
    // let et = e.target;
    // console.log("onToggle e: ", e.target.open);
    if (!one) setOne(true);
    onToggle(e.target.open, e);
  };

  var clickCtxMenuUploadFiles = function clickCtxMenuUploadFiles() {
    console.log("clickCtxMenuUploadFiles");
    var fileInput = Q.domQ("#" + inputFileId);

    if (fileInput) {
      fileInput.click();
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("details", _extends({}, etc, (_extends2 = {
    open: open,
    role: "tree",
    ref: inRef // ref={drop}
    // className={Q.Cx("detail-q tree-q tree-folder w-100", className)} 
    ,
    className: Q.Cx("detail-q tree-q tree-folder w-100", {
      "bg-light": isOver && type !== "root"
    }, className)
  }, _defineProperty(_extends2, "open", Boolean(one) ? one : open), _defineProperty(_extends2, "onToggle", Toggle), _extends2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(TreeItem, {
    type: type // dropRef={dropRef} 
    ,
    As: "summary",
    icon: "folder",
    title: title,
    summaryID: summaryID // 
    ,
    isDrop: isOver && type !== "root" ? "bg-strip" : undefined,
    data: data,
    appendCtxMenu: function appendCtxMenu(hide, unFocus) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, [{
        t: "Create file",
        i: "file",
        fn: function fn() {
          return console.log("onClick Create file");
        }
      }, {
        t: "Create directory",
        i: "folder",
        fn: function fn() {
          return console.log("onClick Create directory");
        }
      }, {
        t: "Upload files",
        i: "upload",
        fn: clickCtxMenuUploadFiles
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          key: v.t,
          onClick: function onClick() {
            hide();
            unFocus();
            v.fn();
          },
          className: "dropdown-item qi q-fw qi-" + v.i,
          type: "button"
        }, v.t);
      }));
    },
    paste: paste,
    onCut: onCut,
    onPaste: onPaste
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "label",
    className: "btnFile qi qi-upload",
    tabIndex: "-1"
  }, "Upload files", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    tabIndex: "-1",
    hidden: true,
    id: inputFileId
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "button",
    type: "button",
    className: "qi qi-file",
    tabIndex: "-1"
  }, "New file"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "button",
    type: "button",
    className: "qi qi-folder",
    tabIndex: "-1"
  }, "New directory")), one && children && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "group",
    className: "detail-content"
  }, children));
}

function Directory(_ref3) {
  var data = _ref3.data,
      _ref3$paste = _ref3.paste,
      paste = _ref3$paste === void 0 ? false : _ref3$paste,
      _ref3$onDrop = _ref3.onDrop,
      onDrop = _ref3$onDrop === void 0 ? Q.noop : _ref3$onDrop,
      _ref3$onCut = _ref3.onCut,
      _onCut = _ref3$onCut === void 0 ? Q.noop : _ref3$onCut,
      _ref3$onPaste = _ref3.onPaste,
      _onPaste = _ref3$onPaste === void 0 ? Q.noop : _ref3$onPaste;

  // useEffect(() => {
  // 	console.log('%cuseEffect in Directory','color:yellow;');
  // }, []);
  // draggingColor, canDrop
  var _useDrop = Object(react_dnd__WEBPACK_IMPORTED_MODULE_1__["useDrop"])({
    accept: [treeType.DIR, treeType.FILE],
    drop: function drop(item) {
      // onDrop({
      // 	...item, 
      // 	...data
      // });
      handleDrop(_objectSpread({}, item));
      return undefined;
    },
    // canDrop: ,
    collect: function collect(monitor) {
      return {
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop() // draggingColor: monitor.getItemType(),

      };
    }
  }),
      _useDrop2 = _slicedToArray(_useDrop, 2),
      isOver = _useDrop2[0].isOver,
      drop = _useDrop2[1];

  var handleDrop = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (dropData) {
    // console.log('handleDrop dropData: ', dropData);
    onDrop(dropData);
  }, []);

  var renderTree = function renderTree(node, key) {
    if (node == null) return;
    var lng = node.length;
    var tree = [];
    var pathEnd;

    var _loop = function _loop(i) {
      pathEnd = node[i].path.split('/').pop();

      if (node[i].type === 'file' || node[i].isFile) {
        tree = [].concat(_toConsumableArray(tree), [/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(TreeItem, {
          key: i,
          className: "tree-file",
          type: treeType.FILE,
          icon: getExt(pathEnd, 1) ? getExt(pathEnd) : "file",
          title: pathEnd,
          data: node[i],
          paste: paste,
          onClick: function onClick() {
            // e
            console.log('onClick TreeItem label Directory node[i]: ', node[i]);
          },
          onCut: function onCut(e) {
            return _onCut(e, node[i]);
          } // (e, sum) => onCut(e, sum, node[i])
          // onPaste={() => onPaste(i)} // onPaste(node[i])

        })]);
      } else {
        //  text-truncate |  className="dark"
        tree = [/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Dir, {
          key: i,
          inRef: drop // dropRef 
          ,
          isOver: isOver,
          data: node[i],
          title: pathEnd // summaryID={Q.Qid() + "-" + pathEnd} 
          ,
          inputFileId: Q.Qid(),
          paste: paste,
          onCut: function onCut(e) {
            return _onCut(e, node[i]);
          } // (e, sum) => onCut(e, sum, node[i])
          ,
          onPaste: function onPaste(e) {
            return _onPaste(e, i);
          } // (e, sum) => onPaste(e, sum, i)

        }, renderTree(node[i][key], "files"))].concat(_toConsumableArray(tree));
      }
    };

    for (var i = 0; i < lng; i++) {
      _loop(i);
    }

    return tree;
  }; // if(data){
  // 	return (
  // 		<div className="detail-q tree-q tree-folder w-100">
  // 			{renderTree(data, "files")}
  // 		</div>
  // 	)
  // }
  // return null;


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "detail-q tree-q tree-folder w-100"
  }, data && renderTree(data, "files"));
}
/*
		<Dir 
			// paste={paste} 
			open renderOpen 
			title="Project" 
			type="root"
		>
			{data && renderTree(data, "files")}
		</Dir>

<Dir open renderOpen title="Folder name" type="root"></Dir>

		<Details 
			open={open} 
			renderOpen={renderOpen} 
			className="tree-q tree-folder" 
			onToggle={(open, e) => {
				console.log('onToggle open: ', open);
				console.log('onToggle e: ', e);
			}}
		>

		</Details>

		<DndBackend>
			{(backend) => {
				// console.log('backend: ',backend);
				return (

				)
			}}
		</DndBackend>

<div className="input-group input-group-xs tree-item">
	<div className="form-control btn btn-flat text-left fab i-color fa-js" title="App.js">App.js</div>
	<div className="input-group-append tree-tool">
		<Btn kind="flat" className="rounded far fa-pen" title="Rename" tabIndex="-1" />
		<Btn kind="flat" className="rounded far fa-times xx" title="Delete" tabIndex="-1" />
	</div>
</div>
*/

/***/ }),

/***/ "./resources/js/src/components/directory/parts/DndBackend.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/components/directory/parts/DndBackend.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DndBackend; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loadable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @loadable/component */ "./node_modules/@loadable/component/dist/loadable.esm.js");
/* harmony import */ var _q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");

 // loadable
// import Flex from '../../q-ui-react/Flex';

 // import { isMobile } from '../../../utils/Q';
// import { HTML5 } from './HTML5';
// import { Touch } from './Touch';
// Q.UA.platform.type === "mobile" || screen.width <= 480

var HTML5 = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return __webpack_require__.e(/*! import() | HTML5Backend */ "vendors~HTML5Backend").then(__webpack_require__.bind(null, /*! react-dnd-html5-backend */ "./node_modules/react-dnd-html5-backend/dist/esm/index.js"));
}); // loadable.lib

var Touch = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return __webpack_require__.e(/*! import() | TouchBackend */ "vendors~TouchBackend").then(__webpack_require__.bind(null, /*! react-dnd-touch-backend */ "./node_modules/react-dnd-touch-backend/dist/esm/index.js"));
});
function DndBackend(_ref) {
  var children = _ref.children;
  var IS_TOUCH = 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
      bg: "/icon/android-icon-36x36.png"
    })
  }, ["mobile", "tablet"].includes(Q_appData.UA.platform.type) && IS_TOUCH ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Touch, null, function (_ref2) {
    var TouchBackend = _ref2.TouchBackend;
    return children(TouchBackend);
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HTML5, null, function (_ref3) {
    var HTML5Backend = _ref3.HTML5Backend;
    return children(HTML5Backend);
  }));
}
/* 
<Flex justify="center" align="center" className="h-100 cwait">
  <div className="spinner-border text-primary" style={{ width: '3rem', height: '3rem' }} role="status" />
</Flex>

fallback={date.toLocaleDateString()} 
*/

/***/ })

}]);
//# sourceMappingURL=Devs~DirectoryPage.chunk.js.map