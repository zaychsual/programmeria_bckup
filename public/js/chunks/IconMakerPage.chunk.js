(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["IconMakerPage"],{

/***/ "./resources/js/src/components/q-ui-react/Iframe.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Iframe.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Iframe; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import {findDOMNode} from "react-dom";
// import { Cx } from '../../utils/Q.js';
// import { domQ } from '../../libs/dom-q';
// sameOrigin, onClick, onKeyDown, onError, onLoad

function Iframe(_ref) {
  var src = _ref.src,
      inRef = _ref.inRef,
      bs = _ref.bs,
      className = _ref.className,
      title = _ref.title,
      wrapClass = _ref.wrapClass,
      wrapProps = _ref.wrapProps,
      children = _ref.children,
      _ref$ratio = _ref.ratio,
      ratio = _ref$ratio === void 0 ? '16by9' : _ref$ratio,
      etc = _objectWithoutProperties(_ref, ["src", "inRef", "bs", "className", "title", "wrapClass", "wrapProps", "children", "ratio"]);

  // const [titleFrame, setTitleFrame] = React.useState(null);
  // const Err = e => {
  // onError(e);
  // };

  /*const Load = e => {
    // let titleTag = e.target.contentWindow.document.title;
    // if(!title && titleTag){
      // setTitleFrame(titleTag);
    // }
  let et = e.target;
  
  et.contentWindow.document.addEventListener('click', e => {
  	e.stopPropagation();
  	// let docActive = document.activeElement;
  	let activeEl = domQ('[aria-expanded="true"]');//  || domQ('.active')
  		// console.log(docActive);
  	if(activeEl) activeEl.click();
  	
  	if(onClick) onClick(e);
  });
  
  if(typeof onKeyDown === 'function'){
  	et.contentWindow.document.addEventListener('keydown', e => {
  		e.stopPropagation();
  		
  		onKeyDown(e);
  	});
  }
  
    onLoad(e);
  };*/
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, wrapProps, {
    className: Q.Cx('iframe-q', _defineProperty({
      'embed-responsive': bs
    }, "embed-responsive-".concat(ratio), bs && ratio), wrapClass)
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("iframe", _extends({}, etc, {
    ref: inRef,
    src: Q.newURL(src) // src={sameOrigin & !src ? window.location.origin +'/'+ src : src} 
    // onError={e => onError(e)}
    // onLoad={e => onLoad(e)} // Load | onLoad(e)
    ,
    title: title,
    className: Q.Cx({
      'embed-responsive-item': bs
    }, className)
  })), children);
}
/* Iframe.defaultProps = {
  ratio: '16by9', // 21by9 = 21:9 | 16by9 = 16:9 | 4by3 = 4:3 | 1by1 = 1:1 
  // title: '', 
  // sameOrigin: true
}; */

/***/ }),

/***/ "./resources/js/src/pages/admin/tools/IconMakerPage.js":
/*!*************************************************************!*\
  !*** ./resources/js/src/pages/admin/tools/IconMakerPage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return IconMakerPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Iframe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Iframe */ "./resources/js/src/components/q-ui-react/Iframe.js");
 // Component
// import axios from 'axios';



function IconMakerPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Icon Maker"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Iframe__WEBPACK_IMPORTED_MODULE_2__["default"] // bs 
  , {
    title: "Icon Maker",
    src: Q.baseURL + "/iconmaker",
    wrapClass: "position-relative mh-full-navmain" //  h-100
    ,
    className: "iframe-app position-absolute position-full" // style={{ height: 'calc(100vh - 48px)' }}

  }));
}

/***/ })

}]);
//# sourceMappingURL=IconMakerPage.chunk.js.map