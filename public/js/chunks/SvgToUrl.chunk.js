(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["SvgToUrl"],{

/***/ "./resources/js/src/apps/SvgToUrl.js":
/*!*******************************************!*\
  !*** ./resources/js/src/apps/SvgToUrl.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SvgToUrl; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/q-ui-react/Textarea */ "./resources/js/src/components/q-ui-react/Textarea.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // , { createRef }
// import Head from '../components/q-ui-react/Head';




var SYMBOLS = /[\r\n%#()<>?\[\\\]^`{|}]/g;

var SvgToUrl = /*#__PURE__*/function (_React$Component) {
  _inherits(SvgToUrl, _React$Component);

  var _super = _createSuper(SvgToUrl);

  function SvgToUrl(props) {
    var _this;

    _classCallCheck(this, SvgToUrl);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "encodeSVG", function (data) {
      var externalQuotesValue = _this.state.externalQuotesValue; // Use single quotes instead of double to avoid encoding.

      if (externalQuotesValue === "\"") {
        data = data.replace(/"/g, '\'');
      } else {
        data = data.replace(/'/g, '"');
      }

      data = data.replace(/>\s{1,}</g, "><");
      data = data.replace(/\s{2,}/g, " ");
      return data.replace(SYMBOLS, encodeURIComponent);
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeInit", function (e) {
      var val = e.target.value;

      _this.setState({
        initTextarea: val,
        resultTextarea: val.length > 0 ? _this.encodeSVG(val) : val
      }, function () {
        _this.getResults();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getResults", function () {
      var _this$state = _this.state,
          initTextarea = _this$state.initTextarea,
          externalQuotesValue = _this$state.externalQuotesValue;

      if (initTextarea.length < 1) {
        _this.setState({
          resultCssTextarea: "",
          bgSvg: null
        });

        return;
      } // let namespaced = this.addNameSpace(initTextarea);


      var escaped = _this.encodeSVG(_this.addNameSpace(initTextarea)); // background-image: 
      // let bg = escaped ? `url(${externalQuotesValue}data:image/svg+xml,${escaped}${externalQuotesValue})` : "";


      _this.setState({
        // bgSvg: bg, 
        resultTextarea: escaped // resultCssTextarea: bg

      }, function () {
        if (escaped) {
          var bgSvg = "url(".concat(externalQuotesValue, "data:image/svg+xml,").concat(escaped).concat(externalQuotesValue, ")");

          _this.setState({
            bgSvg: bgSvg,
            resultCssTextarea: bgSvg
          });
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onBackground", function (bg) {
      return _this.setState({
        bg: bg
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onSetQuotes", function (q) {
      if (_this.state.externalQuotesValue === q) return;

      _this.setState({
        externalQuotesValue: q
      }, function () {
        _this.getResults();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onExample", function () {
      var svg = "<svg>\n  <circle r=\"50\" cx=\"50\" cy=\"50\" fill=\"tomato\"/>\n  <circle r=\"41\" cx=\"47\" cy=\"50\" fill=\"orange\"/>\n  <circle r=\"33\" cx=\"48\" cy=\"53\" fill=\"gold\"/>\n  <circle r=\"25\" cx=\"49\" cy=\"51\" fill=\"yellowgreen\"/>\n  <circle r=\"17\" cx=\"52\" cy=\"50\" fill=\"lightseagreen\"/>\n  <circle r=\"9\" cx=\"55\" cy=\"48\" fill=\"teal\"/>\n</svg>";

      _this.setState({
        initTextarea: svg // resultTextarea: this.encodeSVG(svg)

      }, function () {
        _this.getResults();
      });
    });

    _this.state = {
      initTextarea: "",
      resultTextarea: "",
      resultCssTextarea: "",
      externalQuotesValue: "'",
      // 'single'
      bg: null,
      bgSvg: null
    }; // this.symbols = /[\r\n%#()<>?\[\\\]^`{|}]/g;

    return _this;
  }

  _createClass(SvgToUrl, [{
    key: "addNameSpace",
    value: function addNameSpace(data) {
      if (data.indexOf('http://www.w3.org/2000/svg') < 0) {
        data = data.replace(/<svg/g, "<svg xmlns=\"http://www.w3.org/2000/svg\"");
      }

      return data.trim();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state2 = this.state,
          initTextarea = _this$state2.initTextarea,
          resultTextarea = _this$state2.resultTextarea,
          resultCssTextarea = _this$state2.resultCssTextarea,
          externalQuotesValue = _this$state2.externalQuotesValue,
          bgSvg = _this$state2.bgSvg,
          bg = _this$state2.bg; // const { pos, tipTxt } = this.props;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid py-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-white p-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
        className: "hr-h"
      }, "URL-encoder for SVG"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
        className: "py-2 ml-1-next"
      }, "External quotes:", " ", [{
        q: "'",
        t: "Single"
      }, {
        q: "\"",
        t: "Double"
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
          key: v.t,
          outline: true,
          size: "xs",
          kind: "info",
          onClick: function onClick() {
            return _this2.onSetQuotes(v.q);
          },
          active: externalQuotesValue === v.q,
          title: v.t + " quotes"
        }, v.t);
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-6 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], {
        justify: "between",
        className: "mb-1"
      }, "Insert your SVG: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
        onClick: this.onExample,
        size: "xs",
        kind: "info"
      }, "Example")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_3__["default"], {
        rows: "6",
        spellCheck: "false",
        value: initTextarea,
        onChange: this.onChangeInit
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-6 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "mb-1"
      }, "Take encoded:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_3__["default"], {
        readOnly: true,
        rows: "6",
        value: resultTextarea
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-6 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "mb-1"
      }, "Ready for CSS:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_3__["default"], {
        readOnly: true,
        rows: "6",
        value: resultCssTextarea
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-6 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], {
        justify: "between",
        className: "mb-1"
      }, "Preview:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        className: "btn btn-xs btn-fff",
        type: "color",
        title: "Change background color",
        onChange: function onChange(e) {
          return _this2.onBackground(e.target.value);
        }
      }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
        onClick: function onClick() {
          return _this2.onBackground(null);
        },
        size: "xs",
        kind: "fff"
      }, "Transparent"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "svg2url-view",
        style: {
          backgroundColor: bg
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-no-repeat ovauto resize-native resize-v fal fa-arrows-v",
        style: {
          backgroundImage: bgSvg,
          height: 156
        }
      }))))));
    }
  }]);

  return SvgToUrl;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);



/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Textarea.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Textarea.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Textarea; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { Cx } from '../../utils/Q';

function Textarea(_ref) {
  var h = _ref.h,
      className = _ref.className,
      style = _ref.style,
      onChange = _ref.onChange,
      _ref$bs = _ref.bs,
      bs = _ref$bs === void 0 ? "form-control" : _ref$bs,
      etc = _objectWithoutProperties(_ref, ["h", "className", "style", "onChange", "bs"]);

  var elRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      txt = _useState2[0],
      setTxt = _useState2[1]; // "" | value || defaultValue || 


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(h),
      _useState4 = _slicedToArray(_useState3, 2),
      height = _useState4[0],
      setHeight = _useState4[1]; // const [parentHeight, setParentHeight] = useState("auto");


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // setParentHeight(`${elRef.current.scrollHeight}px`);
    var el = elRef.current; // console.log('clientHeight: ', el.clientHeight);
    // console.log('scrollHeight: ', el.scrollHeight);

    if (el && el.clientHeight !== el.scrollHeight) {
      // text.length > 0 && 
      setHeight(el.scrollHeight + 2); // (el.scrollHeight + 2) + "px"
    }
  }, [txt]);

  var Change = function Change(e) {
    setHeight(h); // null | "auto"
    // setParentHeight(`${elRef.current.scrollHeight}px`);

    setTxt(e.target.value); // setHeight(elRef.current.scrollHeight + "px");

    if (onChange) onChange(e);
  }; // const Keyup = (e) => {
  //   let et = e.target;
  //   // setTimeout(() => {
  //     // et.style.height = 'auto';// ;padding:0
  //     // for box-sizing other than "content-box" use:
  //     // et.style.cssText = '-moz-box-sizing:content-box';
  //     let scrl = et.scrollHeight;
  //     // et.value.length === 0
  //     if(et.clientHeight !== scrl){//  && e.keyCode === 8
  //       // et.style.height = h; // 'height:' + h + 'px';
  //       setHeight((scrl + 2) + "px");// setHeight(h);
  //     }
  //     // else{
  //     //   // et.style.height = scrl + 'px';
  //     //   setHeight((scrl + 2) + "px");
  //     // }
  //     // et.classList.remove("ovhide");
  //   // }, 1);
  //   if(onKeyUp) onKeyUp(e);
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", _extends({}, etc, {
    ref: elRef,
    className: Q.Cx(bs, className),
    style: _objectSpread(_objectSpread({}, style), {}, {
      height: height
    }) // value={txt} 
    // onKeyUp={Keyup} 
    ,
    onChange: Change
  }));
}
;
/*
<div
  style={{
    minHeight: parentHeight,
  }}
>
    
</div>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/tools/Svg2UrlPage.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/pages/admin/tools/Svg2UrlPage.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Svg2UrlPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _apps_SvgToUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../apps/SvgToUrl */ "./resources/js/src/apps/SvgToUrl.js");
 // Component
// import axios from 'axios';
// import RouteParams from '../../../parts/RouteParams';

 // import Btn from '../../../components/q-ui-react/Btn';
// import Modal from '../../../components/q-react-bootstrap/Modal';
// import ModalHeader from '../../../components/q-react-bootstrap/ModalHeader';
// import Iframe from '../../../components/q-ui-react/Iframe';// Loops


function Svg2UrlPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Svg to Url"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_SvgToUrl__WEBPACK_IMPORTED_MODULE_2__["default"], null));
}

/***/ })

}]);
//# sourceMappingURL=SvgToUrl.chunk.js.map