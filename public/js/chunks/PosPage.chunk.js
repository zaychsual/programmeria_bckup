(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PosPage"],{

/***/ "./resources/js/src/apps/Pos.js":
/*!**************************************!*\
  !*** ./resources/js/src/apps/Pos.js ***!
  \**************************************/
/*! exports provided: Pos */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pos", function() { return Pos; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-bootstrap/Modal */ "./node_modules/react-bootstrap/esm/Modal.js");
/* harmony import */ var react_barcode__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-barcode */ "./node_modules/react-barcode/lib/react-barcode.js");
/* harmony import */ var react_barcode__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_barcode__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_q_ui_react_NetWork__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/q-ui-react/NetWork */ "./resources/js/src/components/q-ui-react/NetWork.js");
/* harmony import */ var _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/q-ui-react/Form */ "./resources/js/src/components/q-ui-react/Form.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/react-confirm/util/confirm */ "./resources/js/src/components/react-confirm/util/confirm.js");


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











 // , confirmComplex

var EXPORT_TO = [{
  txt: "Excel",
  fn: function fn() {
    return console.log('Excel');
  }
}, {
  txt: "Pdf",
  fn: function fn() {
    return console.log('Pdf');
  }
}, {
  txt: "Word",
  fn: function fn() {
    return console.log('Word');
  }
}];
var Pos = /*#__PURE__*/function (_React$Component) {
  _inherits(Pos, _React$Component);

  var _super = _createSuper(Pos);

  function Pos(props) {
    var _this;

    _classCallCheck(this, Pos);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onSave", function (e) {
      var et = e.target;

      if (et.checkValidity()) {
        var _this$state = _this.state,
            products = _this$state.products,
            brand = _this$state.brand,
            type = _this$state.type,
            price = _this$state.price,
            code = _this$state.code,
            qty = _this$state.qty,
            loadingFirst = _this$state.loadingFirst; // DEV GENERATE BARCODE string

        var d = Date.now().toString().split("");
        d.slice(d.pop());
        var generateCode = d.join(""); // const generateCode = Math.round(Math.random() * 10000000000000);

        var checkCode = products.find(function (v) {
          return v.code === code;
        }); // console.log('checkCode: ',checkCode);

        if (checkCode) {
          alert("Product Code, must unique");
        } else {
          var newData = {
            id: products.length + 1,
            barcode: generateCode,
            brand: brand,
            type: type,
            price: price,
            code: code,
            qty: qty
          };

          if (loadingFirst < 1) {
            _this.setState({
              brand: "",
              type: "",
              price: "",
              code: "",
              qty: 0,
              barcode: generateCode
            }, function () {
              return _this.onTab("Products", newData);
            });
          } else {
            _this.setState(function (s) {
              return {
                brand: "",
                type: "",
                price: "",
                code: "",
                qty: 0,
                barcode: generateCode,
                products: [].concat(_toConsumableArray(s.products), [newData])
              };
            });
          }
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "deleteProduct", /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(id) {
        var options;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                //  
                options = {
                  type: "toast",
                  // title: "Title",
                  // toastInfoText: "11 min ago",
                  // icon: <i className="fab fa-react mr-2" />, 
                  bodyClass: "text-center bold",
                  btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next",
                  modalProps: {
                    centered: true // modalClassName: "modal-right", // up | down | left | right
                    // returnFocusAfterClose: false, 

                  }
                };
                _context.next = 3;
                return Object(_components_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_11__["confirm"])("Do you Want to delete these product?", options);

              case 3:
                if (!_context.sent) {
                  _context.next = 7;
                  break;
                }

                _this.setState(function (s) {
                  return {
                    products: s.products.filter(function (v) {
                      return v.id !== id;
                    })
                  };
                });

                _context.next = 8;
                break;

              case 7:
                console.log('NO');

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(_assertThisInitialized(_this), "toggleBarcode", function (barcode, code) {
      _this.setState({
        printBarcode: true,
        barcode: barcode,
        code: code
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onTab", function (tab, add) {
      var url = _this.props.url;
      var loadingFirst = _this.state.loadingFirst;

      if (loadingFirst < 1 && tab === "Products" && url) {
        // if(url){
        axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(url).then(function (r) {
          console.log(r);

          if (r.status === 200) {
            var setData = add ? [].concat(_toConsumableArray(r.data), [add]) : r.data;

            _this.setState({
              products: setData,
              loadingFirst: 1 // loadingFirst + 1

            }, function () {
              // Set Focus if click search button in nav tab first open Products Tab
              if (!add && document.activeElement.tagName === "LABEL") _this.findRef.focus();
            });
          }
        })["catch"](function (e) {
          return console.log(e);
        }); // }
      }

      _this.setState({
        tab: tab
      });
    });

    _this.state = {
      loadingFirst: 0,
      tab: "Sales",
      brand: "",
      type: "",
      price: "",
      code: "",
      qty: 0,
      // ""
      barcode: null,
      printBarcode: false,
      products: []
    };
    return _this;
  }

  _createClass(Pos, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      console.log('%ccomponentDidMount in Pos', 'color:yellow;');
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var className = this.props.className;
      var _this$state2 = this.state,
          tab = _this$state2.tab,
          brand = _this$state2.brand,
          type = _this$state2.type,
          price = _this$state2.price,
          code = _this$state2.code,
          qty = _this$state2.qty,
          barcode = _this$state2.barcode,
          products = _this$state2.products,
          printBarcode = _this$state2.printBarcode;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_NetWork__WEBPACK_IMPORTED_MODULE_8__["default"], null, function (network) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Container, {
          mountOnEnter: true,
          id: "pos-tabs" // defaultActiveKey="Sales" 
          ,
          activeKey: tab,
          onSelect: function onSelect(k) {
            return _this2.onTab(k);
          }
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: Q.Cx("card shadow-sm pos-app", className)
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "card-header d-flex align-items-center"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", {
          className: "mb-0"
        }, "Point Of Sales - Connection: ", "".concat(network.online)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"], {
          variant: "tabs",
          className: "ml-auto link-sm nav-card-head"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
          eventKey: "Sales",
          as: "button",
          type: "button"
        }, "Sales"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
          eventKey: "Products",
          as: "button",
          type: "button"
        }, "Products"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
          eventKey: "addProduct",
          as: "button",
          type: "button"
        }, "Add Product"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "d-flex flex-row align-items-center ml-1"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
          eventKey: "Products",
          as: "label" // className="tab-noactive mb-0 px-2 fal fa-search"
          ,
          bsPrefix: "btn btn-sm btn-flat fal fa-search tip tipL",
          htmlFor: "findProductInput",
          role: "button",
          tabIndex: "0",
          "aria-label": "Search Product"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
          size: "sm",
          kind: "flat",
          className: "fal fa-cog tip tipL",
          "aria-label": "Settings"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
          size: "sm",
          kind: "flat",
          className: "fal fa-info-circle tip tipL",
          "aria-label": "Info"
        })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Content, {
          className: "card-body"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
          eventKey: "Sales"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_9__["default"], {
          noValidate: true,
          className: "col-lg-8 mx-auto py-2"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
          className: "btn btn-block btn-lg btn-outline-dark fal fa-barcode-read q-mr",
          type: "button"
        }, " Scan Barcode"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
          className: "hr-h my-3"
        }, "OR"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "card p-4 shadow-sm"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", {
          className: "hr-h mb-4"
        }, "Input Product"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "inputCode",
          className: "col-sm-3 col-form-label col-form-label-sm"
        }, "Product Code"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-sm-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "text",
          className: "form-control form-control-sm",
          id: "inputCode",
          required: true // value={brand} 
          // onChange={e => this.setState({brand: e.target.value})}

        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "invalid-feedback"
        }, "Product Code is Required"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "inputQuantity",
          className: "col-sm-3 col-form-label col-form-label-sm"
        }, "Quantity"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-sm-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "number",
          className: "form-control form-control-sm",
          id: "inputQuantity",
          required: true,
          min: 0 // value={qty} 
          // onChange={e => this.setState({qty: e.target.value})}

        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "invalid-feedback"
        }, "Quantity is Required"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "text-right"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
          type: "submit"
        }, "Submit"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
          eventKey: "Products"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "nav mb-2 ml-1-next"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_5__["default"], {
          className: "ml-auto"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_5__["default"].Toggle, {
          size: "sm",
          variant: "info",
          disabled: products.length < 1
        }, "Export to"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_5__["default"].Menu, null, EXPORT_TO.map(function (v, i) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_5__["default"].Item, {
            key: i,
            onClick: v.fn,
            as: "button",
            type: "button"
          }, v.txt);
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          className: "input-group input-group-sm w-auto mb-0",
          role: "group"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          ref: function ref(n) {
            return _this2.findRef = n;
          },
          disabled: products.length < 1,
          type: "text",
          className: "form-control",
          placeholder: "Search",
          id: "findProductInput"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "input-group-append"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
          As: "div",
          outline: true,
          kind: "secondary",
          className: "fal fa-search",
          tabIndex: "0"
        })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "table-responsive"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
          className: "table table-sm table-bordered table-striped table-hover"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "ID"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Code"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Barcode"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Brand"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Qty"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", null, "Action"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", null, products.map(function (v, i) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
            key: i
          }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.code), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.barcode), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.brand), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.price), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, v.qty), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
            As: "div",
            onClick: function onClick() {
              return _this2.deleteProduct(v.id);
            },
            size: "xs",
            kind: "danger",
            className: "fa fa-trash",
            role: "button",
            title: "Delete"
          }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
            As: "div",
            size: "xs",
            kind: "info",
            className: "fa fa-pencil",
            role: "button",
            title: "Edit"
          }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
            As: "div",
            outline: true,
            size: "xs",
            kind: "dark",
            onClick: function onClick() {
              return _this2.toggleBarcode(v.barcode, v.code);
            },
            className: "fa fa-barcode",
            role: "button",
            title: "Show Barcode"
          })));
        }))), products.length < 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "alert alert-info mb-0"
        }, "NO DATA"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
          eventKey: "addProduct",
          className: "col-lg-8 mx-auto py-2"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", {
          className: "hr-h mb-4"
        }, "Add Product"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_9__["default"], {
          noValidate: true,
          onSubmit: _this2.onSave
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "productName",
          className: "col-md-3 col-form-label col-form-label-sm"
        }, "Product Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-md-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "text",
          className: "form-control form-control-sm",
          id: "productName",
          required: true,
          value: brand,
          onChange: function onChange(e) {
            return _this2.setState({
              brand: e.target.value
            });
          }
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "invalid-feedback"
        }, "Product Name is Required"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "productType",
          className: "col-md-3 col-form-label col-form-label-sm"
        }, "Product Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-md-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "text",
          className: "form-control form-control-sm",
          id: "productType",
          required: true,
          value: type,
          onChange: function onChange(e) {
            return _this2.setState({
              type: e.target.value
            });
          }
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "invalid-feedback"
        }, "Product Type is Required"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "productPrice",
          className: "col-md-3 col-form-label col-form-label-sm"
        }, "Product Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-md-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "text",
          className: "form-control form-control-sm",
          id: "productPrice",
          required: true,
          value: price,
          onChange: function onChange(e) {
            return _this2.setState({
              price: e.target.value
            });
          }
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "invalid-feedback"
        }, "Product Price is Required"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "productCode",
          className: "col-md-3 col-form-label col-form-label-sm"
        }, "Product Code (SKU)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-md-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "text",
          className: "form-control form-control-sm",
          id: "productCode",
          required: true,
          value: code,
          onChange: function onChange(e) {
            return _this2.setState({
              code: e.target.value
            });
          }
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "invalid-feedback"
        }, "Product Code is Required"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "form-group row"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
          htmlFor: "Quantity",
          className: "col-md-3 col-form-label col-form-label-sm"
        }, "Quantity"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "col-md-9"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          type: "number",
          className: "form-control form-control-sm",
          id: "Quantity" // required 
          ,
          min: 0,
          value: qty,
          onChange: function onChange(e) {
            return _this2.setState({
              qty: e.target.value
            });
          }
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "text-right"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
          type: "submit"
        }, "Save"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_6__["default"], {
          show: printBarcode,
          onHide: function onHide() {
            return _this2.setState({
              printBarcode: false
            });
          }
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_6__["default"].Header, {
          closeButton: true
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", {
          className: "modal-title"
        }, "Barcode Result")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_6__["default"].Body, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "d-inline-block p-2 border"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", {
          className: "mb-0 text-center"
        }, code), barcode && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_barcode__WEBPACK_IMPORTED_MODULE_7___default.a, {
          value: barcode // "123456789012" //  
          ,
          format: "EAN13" // CODE128 | EAN13 | CODE39
          // lineColor="red"
          // textMargin={-10}

        }))))));
      });
    }
  }]);

  return Pos;
}(react__WEBPACK_IMPORTED_MODULE_1___default.a.Component);
/*
            {[
              {label:"Product Name", id:"productName", val:brand, fn:e => this.setState({brand: e.target.value})},
            ].map((v, i) => 
              <div key={i} className="form-group row">
                <label htmlFor="productName" className="col-sm-2 col-form-label col-form-label-sm">
                  {v.label}
                </label>
                <div className="col-sm-10">
                  <input type="text" className="form-control form-control-sm" id={v.id} 
                    required 
                    value={v.brand} 
                    onChange={v.fn}
                  />
                  <div className="invalid-feedback">Is Required</div>
                </div>
              </div>
            )}
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Form.js":
/*!********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Form.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Form; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import {findDOMNode} from "react-dom";
// import { Cx, toggleClass } from '../../utils/Q';
// wrapClass, upload, id, encType

function Form(_ref) {
  var inRef = _ref.inRef,
      className = _ref.className,
      fieldsetClass = _ref.fieldsetClass,
      noValidate = _ref.noValidate,
      valid = _ref.valid,
      disabled = _ref.disabled,
      onSubmit = _ref.onSubmit,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "className", "fieldsetClass", "noValidate", "valid", "disabled", "onSubmit", "children"]);

  // React.useEffect(() => {
  // console.log('%cuseEffect in Form','color:yellow;');
  // console.log(getEl());
  // console.log(findDOMNode(Form));
  // }, []);

  /*const getEl = () => { // getElement
  	try{
  		// using findDOMNode for two reasons:
  		// 1. cloning to insert a ref is unwieldy and not performant.
  		// 2. ensure that we resolve to an actual DOM node (instead of any JSX ref instance).
  		return findDOMNode(this);
  	}catch{
  		return null;// swallow error if findDOMNode is run on unmounted component.
  	}
  }*/
  var Submit = function Submit(e) {
    if (disabled) return;
    var et = e.target; // Prevent submit form if use xhr

    if (noValidate) {
      e.preventDefault();
      e.stopPropagation(); // console.log(!Q.isBool(valid));
      // if(!Q.isBool(valid)) 

      if (!Q.isBool(valid)) {
        et.classList.toggle("was-validated", !et.checkValidity());
      }
    }

    if (onSubmit && !disabled) onSubmit(e, et.checkValidity()); // onSubmit to props
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", _extends({}, etc, {
    ref: inRef,
    className: Q.Cx('form-q', {
      "disabled": disabled,
      // Q.isBool(valid) //  && !isValid
      "was-validated": noValidate && valid && Object.keys(valid).length > 0
    }, className) // enctype values: (OPTION)
    // 1. application/x-www-form-urlencoded (Default)
    // 2. multipart/form-data (form upload)
    // 3. text/plain (Spaces are converted to "+" symbols, but no special characters are encoded)
    // encType={upload ? 'multipart/form-data' : encType} 
    //data-reset={reset} 
    ,
    noValidate: noValidate // noValidate ? true : undefined
    ,
    onSubmit: Submit
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
    disabled: disabled,
    className: fieldsetClass
  }, children));
}

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/NetWork.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/NetWork.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NetWork; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ahooks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ahooks */ "./node_modules/ahooks/es/index.js");


function NetWork(_ref) {
  var children = _ref.children;
  var network = Object(ahooks__WEBPACK_IMPORTED_MODULE_1__["useNetwork"])(); // React.useEffect(() => {
  // 	console.log('%cuseEffect in NetWork','color:yellow;');
  // }, []);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, children && children(network));
}

/***/ }),

/***/ "./resources/js/src/pages/private/PosPage.js":
/*!***************************************************!*\
  !*** ./resources/js/src/pages/private/PosPage.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PosPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _apps_Pos__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../apps/Pos */ "./resources/js/src/apps/Pos.js");


function PosPage() {
  // const [data, setData] = React.useState();
  // React.useEffect(() => {
  // 	console.log('%cuseEffect in PosPage','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_Pos__WEBPACK_IMPORTED_MODULE_1__["Pos"], {
    url: "/DUMMY/json/products.json"
  }));
}
/*
<React.Fragment>
	Devs
</React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=PosPage.chunk.js.map