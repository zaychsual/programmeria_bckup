(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["DirectoryPage"],{

/***/ "./resources/js/src/components/directory/DirItem.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/directory/DirItem.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DirItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dnd */ "./node_modules/react-dnd/dist/esm/index.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utils */ "./resources/js/src/components/directory/utils.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // , { useRef, useState, useEffect }

 // useDrop


 // import ContextMenu from '../q-ui-react/ContextMenu';
// import { clipboardCopy } from '../../utils/clipboard';

 // function stringify(obj, replacer, spaces, cycleReplacer) {
//   return JSON.stringify(obj, serializer(replacer, cycleReplacer), spaces)
// }
// function serializer(replacer, cycleReplacer) {
//   let stack = [], keys = []
//   if (cycleReplacer == null) cycleReplacer = function(key, value) {
//     if (stack[0] === value) return "[Circular ~]"
//     return "[Circular ~." + keys.slice(0, stack.indexOf(value)).join(".") + "]"
//   }
//   return function(key, value) {
//     if (stack.length > 0) {
//       let thisPos = stack.indexOf(this)
//       ~thisPos ? stack.splice(thisPos + 1) : stack.push(this)
//       ~thisPos ? keys.splice(thisPos, Infinity, key) : keys.push(key)
//       if (~stack.indexOf(value)) value = cycleReplacer.call(this, key, value)
//     }
//     else stack.push(value)
//     return replacer == null ? value : replacer.call(this, key, value)
//   }
// }

function DirItem(_ref) {
  var _Q$Cx;

  var type = _ref.type,
      data = _ref.data,
      className = _ref.className,
      isDrop = _ref.isDrop,
      children = _ref.children,
      title = _ref.title,
      titleAttr = _ref.titleAttr,
      icon = _ref.icon,
      ariaControls = _ref.ariaControls,
      expanded = _ref.expanded,
      labelClass = _ref.labelClass,
      copy = _ref.copy,
      cut = _ref.cut,
      rename = _ref.rename,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? Q.noop : _ref$onClick,
      _ref$onCut = _ref.onCut,
      onCut = _ref$onCut === void 0 ? Q.noop : _ref$onCut,
      _ref$onCopy = _ref.onCopy,
      onCopy = _ref$onCopy === void 0 ? Q.noop : _ref$onCopy,
      _ref$onPaste = _ref.onPaste,
      onPaste = _ref$onPaste === void 0 ? Q.noop : _ref$onPaste,
      _ref$onClickRename = _ref.onClickRename,
      onClickRename = _ref$onClickRename === void 0 ? Q.noop : _ref$onClickRename,
      _ref$onRename = _ref.onRename,
      onRename = _ref$onRename === void 0 ? Q.noop : _ref$onRename,
      _ref$onBlurRename = _ref.onBlurRename,
      onBlurRename = _ref$onBlurRename === void 0 ? Q.noop : _ref$onBlurRename,
      _ref$onSaveRename = _ref.onSaveRename,
      onSaveRename = _ref$onSaveRename === void 0 ? Q.noop : _ref$onSaveRename,
      _ref$onDelete = _ref.onDelete,
      onDelete = _ref$onDelete === void 0 ? Q.noop : _ref$onDelete;

  // const ID = "DirItem" + "-" + Q.Qid(3);
  // const targetFlip = useRef(null);
  // const [focusCtxMenu, setFocusCtxMenu] = useState(false);
  // const [targetFlip, setTargetFlip] = useState(null);
  // const [focusPaste, setFocusPaste] = useState(null);// false
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showMenu = _useState2[0],
      setShowMenu = _useState2[1];

  var _useDrag = Object(react_dnd__WEBPACK_IMPORTED_MODULE_1__["useDrag"])({
    item: _objectSpread({
      type: type
    }, data),
    // type: `${color}`
    canDrag: type === _utils__WEBPACK_IMPORTED_MODULE_4__["treeType"].DIR || type === _utils__WEBPACK_IMPORTED_MODULE_4__["treeType"].FILE,
    // !forbidDrag
    collect: function collect(monitor) {
      return {
        isDragging: monitor.isDragging()
      };
    }
  }),
      _useDrag2 = _slicedToArray(_useDrag, 3),
      isDragging = _useDrag2[0].isDragging,
      drag = _useDrag2[1],
      preview = _useDrag2[2];

  var BTNS = [{
    t: "Rename",
    i: "pen",
    fn: onClickRename
  }, {
    t: "Delete",
    i: "trash",
    fn: onDelete
  } // fn: onDelete | fn: (e) => onDelete(data, e)
  ].filter(function () {
    return type !== "root";
  }); // paste && type === treeType.DIR ? {t:"Paste", i:"check", fn: onPaste} : null;
  // (paste && type !== treeType.FILE) || (copy && type !== treeType.DIR)

  var PASTE = (cut || copy) && type === _utils__WEBPACK_IMPORTED_MODULE_4__["treeType"].DIR ? {
    t: "Paste",
    i: "check",
    fn: onPaste
  } : null; // const onCopyItem = (e) => {
  //   console.log("onCopyItem e.target: ", e.target);
  //   console.log('onCopyItem data: ', data);
  //   const dataObj = stringify(data, null, 2);
  //   console.log('onCopyItem dataObj: ', dataObj);
  //   onCopy(e);
  //   // clipboardCopy(JSON.stringify(data)).then(v => {
  //   //   console.log(v);
  //   // }).catch(e => console.log(e));
  // }

  var CUT_COPY = [{
    t: "Cut",
    i: "cut",
    fn: onCut
  }, {
    t: "Copy",
    i: "copy",
    fn: onCopy
  }, PASTE].filter(function (v) {
    return v && v.fn;
  }); // const unFocus = () => setFocusCtxMenu(false);
  // const onMouseLeave = () => { // e
  // 	// const et = e.target;
  // 	// const dd = Q.domQ('button[aria-expanded="true"]', et.closest('.tree-item'));
  // 	// if(dd) dd.click();
  // 	if(showMenu) setShowMenu(false);
  // }
  // const Click = (e, item) => {
  //   setFocusPaste({ e, item }); // Q.domQ("#" + ID)
  //   onClick(e);
  // }
  // console.log('focusPaste: ', focusPaste);
  // console.log('document.activeElement: ', document.activeElement);
  // useEffect(() => {
  //   const pasteEvt = e => {
  //     Q.preventQ(e);
  //     let dataPaste = (e.clipboardData || window.clipboardData).getData('text');
  //     console.log('pasteEvt dataPaste: ', dataPaste);
  //     onCtrlPaste(e);// onCtrlPaste | onPaste, 
  //   }
  //   if(focusPaste) document.addEventListener("paste", pasteEvt);
  //   else document.removeEventListener("paste", pasteEvt);
  //   return () => {
  //     if(focusPaste) document.removeEventListener("paste", pasteEvt);
  //   }
  // }, [focusPaste]);
  // console.log('targetFlip: ', targetFlip);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    ref: preview,
    role: "treeitem",
    className: Q.Cx("tree-item", {
      "isDragging bg-main": isDragging // "focus": showMenu // focusCtxMenu

    }, isDrop) // onMouseLeave={onMouseLeave} 

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    noValidate: true // ref={targetFlip} 
    ,
    onSubmit: onSaveRename,
    onContextMenu: function onContextMenu(e) {
      Q.preventQ(e);
      document.body.click();
      setShowMenu(true);
    },
    className: Q.Cx("input-group input-group-xs", className) //  flex-nowrap

  }, Q.isStr(rename) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-prepend"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-text bg-white border-transparent i-color qi qi-" + icon
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    defaultValue: rename,
    onChange: onRename,
    className: "form-control shadow-none",
    type: "text",
    autoFocus: true,
    spellCheck: "false",
    onBlur: onBlurRename // onKeyDown={onKeyDownRename} 

  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    ref: drag,
    className: Q.Cx("tree-label form-control btn btn-flat text-left", (_Q$Cx = {}, _defineProperty(_Q$Cx, "i-color qi qi-" + icon, icon), _defineProperty(_Q$Cx, "isFile", type === _utils__WEBPACK_IMPORTED_MODULE_4__["treeType"].FILE), _Q$Cx), labelClass),
    tabIndex: "0",
    "aria-controls": ariaControls,
    "aria-expanded": expanded,
    title: titleAttr || title // titleAttr ? titleAttr : title
    ,
    onClick: onClick // e => Click(e, data) | onClick 
    // onDoubleClick={onDoubleClick} 
    // OPTION for paste
    // onBlur={() => setFocusPaste(null)} 
    // id={ID} 
    // spellCheck={focusPaste ? false : null} 
    // contentEditable={focusPaste ? true : null} 
    // dangerouslySetInnerHTML={focusPaste ? { __html: title } : undefined} 
    // onKeyDown={e => {
    //   console.log(e.key);
    //   console.log(e.ctrlKey);
    //   if(e.key.toLowerCase() === "v" && e.ctrlKey){
    //     Q.preventQ(e);
    //   }
    // }} 
    // onPaste={(e) => {
    //   console.log('onPaste e: ', e);
    //   // console.log('onPaste e.target: ', e.target);
    //   // console.log('onPaste e.nativeEvent: ', e.nativeEvent);
    // }}

  }, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-append tree-tool"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    bsPrefix: "btn-group btn-group-xs",
    alignRight: true,
    show: showMenu,
    onToggle: function onToggle(see) {
      return setShowMenu(see);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    variant: "flat",
    className: "rounded-0",
    tabIndex: "-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
    className: "dropdown-menu show py-0 v-dd-sets mnw-auto w-auto" // onClick={e => Q.preventQ(e)} 
    ,
    popperConfig: {
      modifiers: [{
        name: "offset",
        options: {
          offset: [0, -0.05]
        }
      }]
    }
  }, children, CUT_COPY.map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      key: i,
      as: "button",
      type: "button",
      className: "qi qi-" + v.i,
      tabIndex: "-1",
      onClick: v.fn
    }, v.t);
  }))), BTNS.map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      key: v.t,
      onClick: v.fn,
      blur: true,
      kind: "flat",
      className: "qi qi-" + v.i + (type === _utils__WEBPACK_IMPORTED_MODULE_4__["treeType"].FILE ? " isFile" : ""),
      title: v.t,
      tabIndex: "-1"
    });
  }))));
}

/***/ }),

/***/ "./resources/js/src/components/directory/Directories.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/components/directory/Directories.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Directories; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Directory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Directory */ "./resources/js/src/components/directory/Directory.js");
/* harmony import */ var _DirItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./DirItem */ "./resources/js/src/components/directory/DirItem.js");
/* harmony import */ var _react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../react-confirm/util/confirm */ "./resources/js/src/components/react-confirm/util/confirm.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils */ "./resources/js/src/components/directory/utils.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useMemo, useEffect, useRef, useContext, useLayoutEffect, useCallback }
// import { useDrag, useDrop } from 'react-dnd';
// import { DndProvider } from 'react-dnd';
// import DndBackend from './parts/DndBackend';


 // import Flex from '../q-ui-react/Flex';





function getExt(fname, bool) {
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

var CONFIRM_OPTION = {
  type: "toast",
  // title: "Title",
  // icon: <i className="fab fa-react mr-2" />, 
  bodyClass: "text-center",
  btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next",
  modalProps: {
    centered: true,
    returnFocusAfterClose: false
  }
};
var NEW_FILE = {
  type: "file",
  path: ""
};
function Directories(_ref) {
  var data = _ref.data,
      copy = _ref.copy,
      cut = _ref.cut,
      _ref$onDrop = _ref.onDrop,
      onDrop = _ref$onDrop === void 0 ? Q.noop : _ref$onDrop,
      _ref$onCut = _ref.onCut,
      _onCut = _ref$onCut === void 0 ? Q.noop : _ref$onCut,
      _ref$onCopy = _ref.onCopy,
      _onCopy = _ref$onCopy === void 0 ? Q.noop : _ref$onCopy,
      _ref$onPaste = _ref.onPaste,
      _onPaste = _ref$onPaste === void 0 ? Q.noop : _ref$onPaste,
      _ref$onDelete = _ref.onDelete,
      onDelete = _ref$onDelete === void 0 ? Q.noop : _ref$onDelete,
      _ref$onNewFile = _ref.onNewFile,
      _onNewFile = _ref$onNewFile === void 0 ? Q.noop : _ref$onNewFile;

  var inputFile = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      itemFile = _useState2[0],
      setItemFile = _useState2[1]; // For upload file item position


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      rename = _useState4[0],
      setRename = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      renameVal = _useState6[0],
      setRenameVal = _useState6[1]; // const onConfirm = async (txt, onOk = Q.noop, options = {}) => { //  
  // 	if(await confirm(txt, { ...CONFIRM_OPTION, ...options })){
  // 		onOk();
  // 	}
  // }
  // console.log('rename: ', rename);
  // console.log('renameVal: ', renameVal);


  var onDeleteItem = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(item) {
      var newState, node, parent, foundNode, dataName, conf;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              newState = _toConsumableArray(data); // _cloneDeep(state);

              node = null;
              parent = null;
              foundNode = Object(_utils__WEBPACK_IMPORTED_MODULE_5__["searchDFS"])({
                data: newState,
                cond: function cond(val) {
                  return val.path === item.path; // val.path === action.payload.id
                }
              });
              node = foundNode.item;
              parent = node.parentNode; // foundNode.parent | node.parentNode

              dataName = node.path.split('/').pop(); // const conf = await confirm(<Flex As="h6" dir="column" justify="center" className={"q-mb qi-3x i-color qi qi-" + (item.type === "file" ? getExt(dataName):"folder")}><small className="mb-3">{dataName}</small>Are you sure to delete this {node.type}</Flex>, CONFIRM_OPTION);

              _context.next = 9;
              return Object(_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_4__["confirm"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Are you sure to delete this ", node.type, "?", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), dataName), CONFIRM_OPTION);

            case 9:
              conf = _context.sent;

              if (!parent || Array.isArray(parent)) {
                // onConfirm(msg, () => {
                // 	newState = newState.filter(file => file.path !== item.path); // file.id !== action.payload.id
                // 	onDelete(newState);
                // });
                if (conf) {
                  newState = newState.filter(function (file) {
                    return file.path !== item.path;
                  }); // file.id !== action.payload.id

                  onDelete(newState);
                }
              } else {
                if (conf) {
                  parent.files = parent.files.filter(function (file) {
                    return file.path !== item.path;
                  });
                  onDelete(newState);
                }
              } // console.log('onDeleteItem ext: ', getExt(dataName));
              // console.log('onDeleteItem item: ', item);


            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function onDeleteItem(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var onClickRenameItem = function onClickRenameItem(i, data, val) {
    console.log('onClickRenameItem i: ', i);
    console.log('onClickRenameItem data: ', data);
    console.log('onClickRenameItem val: ', val);
    setRename({
      i: i,
      data: data
    });
    setRenameVal(val);
  };

  var onRenameItem = function onRenameItem(e) {
    var val = e.target.value;
    console.log('onRename val: ', val);
    setRenameVal(val); // onRename();
  };

  var onBlurRenameItem = function onBlurRenameItem() {
    var isVal = renameVal.length > 0; // if(isVal){
    // 	setRename(null);
    // }

    setRename(null);
    setRenameVal(isVal ? renameVal : rename.data.path.split('/').pop());
  }; // const onKeyDownRenameItem = e => {
  // 	console.log(e.key);
  // 	if(e.key === "Enter" && renameVal.length > 0){
  // 		setRename(null);
  // 	}
  // }


  var onSaveRenameItem = function onSaveRenameItem(e) {
    Q.preventQ(e); // const valid = /^[^\\\/\:\*\?\"\<\>\|\.]+(\.[^\\\/\:\*\?\"\<\>\|\.]+)+$/.test(renameVal);

    var valid = /^[^\\/?%*:|"<>\.]+$/.test(renameVal);
    console.log("onSaveRenameItem valid: ", valid);

    if (renameVal.length > 0 && valid) {
      setRename(null);
    }
  };

  var renderTree = function renderTree(node, parentNode) {
    var key = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "files";
    if (node == null) return;
    var lng = node.length;
    var tree = [];

    var _loop = function _loop(i) {
      var item = node[i];
      var pathEnd = item.path.split('/').pop();
      item.parentNode = parentNode;

      if (!parentNode) {
        item.parentNode = item.files;
      }

      if (item.type === 'file' || item.isFile) {
        tree = [].concat(_toConsumableArray(tree), [/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_DirItem__WEBPACK_IMPORTED_MODULE_3__["default"], {
          key: i,
          className: "tree-file",
          type: _utils__WEBPACK_IMPORTED_MODULE_5__["treeType"].FILE,
          icon: getExt(pathEnd, 1) ? getExt(pathEnd) : "file",
          title: pathEnd,
          data: item,
          copy: copy,
          cut: cut,
          rename: (rename === null || rename === void 0 ? void 0 : rename.data.path) === item.path ? renameVal : null // ariaControls={ID} 
          ,
          labelClass: (cut === null || cut === void 0 ? void 0 : cut.path) === item.path ? "o-05" : "",
          onClick: function onClick(e) {
            console.log('onClick TreeItem label Directory item: ', item); // console.log('onClick TreeItem label Directory document.activeElement: ', document.activeElement);
          },
          onCut: function onCut(e) {
            return _onCut(item, e);
          } // (e, sum) => onCut(e, sum, item)
          ,
          onCopy: function onCopy(e) {
            return _onCopy(item, e);
          } // 
          ,
          onDelete: function onDelete() {
            return onDeleteItem(item);
          },
          onClickRename: function onClickRename() {
            return onClickRenameItem(i, item, pathEnd);
          },
          onRename: onRenameItem,
          onBlurRename: onBlurRenameItem // onKeyDownRename={onKeyDownRenameItem} 
          ,
          onSaveRename: onSaveRenameItem
        })]);
      } else {
        tree = [/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_Directory__WEBPACK_IMPORTED_MODULE_2__["default"], {
          key: i,
          data: item,
          title: pathEnd // id={Q.Qid() + "-" + pathEnd} 
          // inputFileId={Q.Qid()} 
          ,
          copy: copy,
          cut: cut,
          rename: (rename === null || rename === void 0 ? void 0 : rename.data.path) === item.path ? renameVal : null,
          labelClass: (cut === null || cut === void 0 ? void 0 : cut.path) === item.path ? "o-05" : "",
          onCut: function onCut(e) {
            return _onCut(item, e);
          } // (e, sum) => onCut(e, sum, item)
          ,
          onCopy: function onCopy(e) {
            return _onCopy(item, e);
          },
          onPaste: function onPaste(e) {
            return _onPaste(i, e);
          } // item
          // onCtrlPaste={e => {
          // 	console.log('onCtrlPaste e: ', e);
          // 	console.log('onCtrlPaste item: ', item);
          // }}
          ,
          onClickRename: function onClickRename() {
            return onClickRenameItem(i, item, pathEnd);
          },
          onRename: onRenameItem,
          onBlurRename: onBlurRenameItem // onKeyDownRename={onKeyDownRenameItem} 
          ,
          onSaveRename: onSaveRenameItem,
          onDelete: function onDelete() {
            return onDeleteItem(item);
          } // (datas, e) => onDeleteItem(datas, item)
          // onToggle={onToggle} 
          ,
          onDrop: onDrop,
          onNewFile: function onNewFile() {
            var newData = data.map(function (v, i2) {
              if (i2 === i) {
                return _objectSpread(_objectSpread({}, v), {}, {
                  files: [].concat(_toConsumableArray(v.files), [NEW_FILE])
                });
              }

              return v;
            });
            console.log("onNewFile newData: ", newData);

            _onNewFile(newData);

            setTimeout(function () {
              onClickRenameItem(i, NEW_FILE, "");
            }, 99);
          },
          onNewDirectory: function onNewDirectory() {
            console.log("onNewDirectory");
          },
          onClickUpload: function onClickUpload() {
            inputFile.current.click();
            setItemFile({
              index: i,
              data: item
            });
          }
        }, renderTree(item[key], item))].concat(_toConsumableArray(tree));
      }
    };

    for (var i = 0; i < lng; i++) {
      _loop(i);
    }

    return tree;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_Directory__WEBPACK_IMPORTED_MODULE_2__["default"], {
    open: true,
    mountOnEnter: true,
    title: "Project",
    type: "root",
    cut: cut,
    onCut: null,
    onCopy: null,
    onPaste: function onPaste(e) {
      return _onPaste(e, 0);
    }
  }, data && renderTree(data), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    ref: inputFile,
    type: "file",
    tabIndex: "-1",
    hidden: true,
    onChange: function onChange(e) {
      var file = e.target.files[0];
      console.log('onChange file: ', file);
      console.log('onChange itemFile: ', itemFile);
    }
  }));
}
/*
		<div className="detail-q tree-q tree-folder w-100">
			{data && renderTree(data, "files")}
		</div>
*/

/***/ }),

/***/ "./resources/js/src/components/directory/Directory.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/directory/Directory.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Directory; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dnd */ "./node_modules/react-dnd/dist/esm/index.js");
/* harmony import */ var react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Collapse */ "./node_modules/react-bootstrap/esm/Collapse.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _DirItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./DirItem */ "./resources/js/src/components/directory/DirItem.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils */ "./resources/js/src/components/directory/utils.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useMemo, useEffect, useRef, useContext, useLayoutEffect, useCallback }

 // useDrag, 





function Directory(_ref) {
  var data = _ref.data,
      className = _ref.className,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? _utils__WEBPACK_IMPORTED_MODULE_5__["treeType"].DIR : _ref$type,
      title = _ref.title,
      id = _ref.id,
      children = _ref.children,
      _ref$open = _ref.open,
      open = _ref$open === void 0 ? false : _ref$open,
      _ref$timeout = _ref.timeout,
      timeout = _ref$timeout === void 0 ? 50 : _ref$timeout,
      _ref$mountOnEnter = _ref.mountOnEnter,
      mountOnEnter = _ref$mountOnEnter === void 0 ? true : _ref$mountOnEnter,
      labelClass = _ref.labelClass,
      copy = _ref.copy,
      cut = _ref.cut,
      rename = _ref.rename,
      _ref$onDrop = _ref.onDrop,
      onDrop = _ref$onDrop === void 0 ? Q.noop : _ref$onDrop,
      _ref$onCut = _ref.onCut,
      onCut = _ref$onCut === void 0 ? Q.noop : _ref$onCut,
      _ref$onCopy = _ref.onCopy,
      onCopy = _ref$onCopy === void 0 ? Q.noop : _ref$onCopy,
      _ref$onPaste = _ref.onPaste,
      onPaste = _ref$onPaste === void 0 ? Q.noop : _ref$onPaste,
      _ref$onClickRename = _ref.onClickRename,
      onClickRename = _ref$onClickRename === void 0 ? Q.noop : _ref$onClickRename,
      _ref$onRename = _ref.onRename,
      onRename = _ref$onRename === void 0 ? Q.noop : _ref$onRename,
      _ref$onBlurRename = _ref.onBlurRename,
      onBlurRename = _ref$onBlurRename === void 0 ? Q.noop : _ref$onBlurRename,
      _ref$onSaveRename = _ref.onSaveRename,
      onSaveRename = _ref$onSaveRename === void 0 ? Q.noop : _ref$onSaveRename,
      _ref$onDelete = _ref.onDelete,
      onDelete = _ref$onDelete === void 0 ? Q.noop : _ref$onDelete,
      _ref$onNewFile = _ref.onNewFile,
      onNewFile = _ref$onNewFile === void 0 ? Q.noop : _ref$onNewFile,
      _ref$onNewDirectory = _ref.onNewDirectory,
      onNewDirectory = _ref$onNewDirectory === void 0 ? Q.noop : _ref$onNewDirectory,
      _ref$onClickUpload = _ref.onClickUpload,
      onClickUpload = _ref$onClickUpload === void 0 ? Q.noop : _ref$onClickUpload,
      etc = _objectWithoutProperties(_ref, ["data", "className", "type", "title", "id", "children", "open", "timeout", "mountOnEnter", "labelClass", "copy", "cut", "rename", "onDrop", "onCut", "onCopy", "onPaste", "onClickRename", "onRename", "onBlurRename", "onSaveRename", "onDelete", "onNewFile", "onNewDirectory", "onClickUpload"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(open),
      _useState2 = _slicedToArray(_useState, 2),
      see = _useState2[0],
      setSee = _useState2[1];

  var MENUS = [{
    t: "New file",
    i: "file",
    fn: onNewFile
  }, {
    t: "New directory",
    i: "folder",
    fn: onNewDirectory
  }, {
    t: "Upload files",
    i: "upload",
    fn: onClickUpload
  } // clickCtxMenuUploadFiles
  ];

  var _useDrop = Object(react_dnd__WEBPACK_IMPORTED_MODULE_1__["useDrop"])({
    accept: [_utils__WEBPACK_IMPORTED_MODULE_5__["treeType"].DIR, _utils__WEBPACK_IMPORTED_MODULE_5__["treeType"].FILE],
    drop: function drop(item) {
      // , monitor
      // const didDrop = monitor.didDrop();
      // console.log('drop didDrop: ', didDrop);
      // handleDrop({
      // 	...item, 
      // 	...data
      // });
      // if(didDrop){
      // 	return;
      // }
      if (!see) setSee(true);
      onDrop(item, data);
      return undefined;
    },
    // hover(){
    // 	setSee(true);
    // },
    collect: function collect(monitor) {
      return {
        isOver: monitor.isOver(),
        isOverCurrent: monitor.isOver({
          shallow: true
        })
      };
    }
  }),
      _useDrop2 = _slicedToArray(_useDrop, 2),
      _useDrop2$ = _useDrop2[0],
      isOver = _useDrop2$.isOver,
      isOverCurrent = _useDrop2$.isOverCurrent,
      drop = _useDrop2[1]; // const handleDrop = useCallback((dropData) => {
  // 	console.log('handleDrop dropData: ', dropData);
  // 	onDrop(dropData, data);
  // }, []);


  var toggle = function toggle(e) {
    setSee(!see); // onToggle(!see, e);
  }; // const onPasteData = (e) => {
  //   // if(!see) setSee(true);
  //   const label = Q.domQ(`[aria-controls="${id}"]`);
  //   console.log("onPasteData label:", label);
  //   if(label){
  //     console.log('onPasteData label: ', label);
  //     label.click();
  //     label.blur();
  //   }
  //   setTimeout(() => {
  //     onPaste(e);
  //   }, 300);
  // }
  // const clickCtxMenuUploadFiles = () => {
  // 	console.log("clickCtxMenuUploadFiles");
  // 	let fileInput = Q.domQ("#" + inputFileId);
  // 	if(fileInput){
  // 		fileInput.click();
  // 	}
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    ref: drop,
    role: "tree",
    className: // Q.Cx("detail-q tree-q tree-folder w-100", { "bg-light": (isOver && isOverCurrent) && type !== "root" }, className)
    Q.Cx("detail-q tree-q tree-folder w-100", {
      "bg-light": isOver && isOverCurrent
    }, className)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DirItem__WEBPACK_IMPORTED_MODULE_4__["default"], {
    type: type,
    icon: "folder",
    title: title,
    ariaControls: id,
    expanded: see // type !== "root" ? see : null
    // isDrop={(isOver && isOverCurrent) && type !== "root" ? "bg-strip" : undefined}
    ,
    isDrop: isOver && isOverCurrent ? "bg-strip" : undefined,
    data: data // appendCtxMenu={(hide, unFocus) => 
    // 	<>
    // 		{MENUS.map(v => 
    //       <button key={v.t} 
    //         onClick={() => {
    //           hide();
    //           unFocus();
    //           v.fn();
    //         }} 
    //         className={"dropdown-item qi q-fw qi-" + v.i} type="button">{v.t}</button>	
    //       )
    // 		}
    // 	</>
    // } 
    ,
    labelClass: labelClass,
    copy: copy,
    cut: cut,
    rename: rename,
    onClick: function onClick(e) {
      return toggle(e);
    } // onDoubleClick={e => toggle(e)} // OPTION
    ,
    onCut: onCut,
    onCopy: onCopy,
    onPaste: onPaste // onPasteData 
    // onCtrlPaste={onCtrlPaste} // DEVS
    ,
    onClickRename: onClickRename,
    onRename: onRename,
    onBlurRename: onBlurRename // onKeyDownRename={onKeyDownRename} 
    ,
    onSaveRename: onSaveRename,
    onDelete: onDelete // (data, e) => onDelete(data, e)

  }, MENUS.map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_3__["default"].Item, {
      key: i,
      as: "button",
      type: "button",
      onClick: v.fn,
      className: "qi qi-" + v.i,
      tabIndex: "-1"
    }, v.t);
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({}, etc, {
    mountOnEnter: mountOnEnter,
    timeout: timeout,
    "in": see
  }), children && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "tree-content",
    id: id,
    "aria-hidden": !see
  }, children)));
}

/***/ }),

/***/ "./resources/js/src/components/directory/parts/DndBackend.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/components/directory/parts/DndBackend.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DndBackend; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loadable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @loadable/component */ "./node_modules/@loadable/component/dist/loadable.esm.js");
/* harmony import */ var _q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");

 // loadable
// import Flex from '../../q-ui-react/Flex';

 // import { isMobile } from '../../../utils/Q';
// import { HTML5 } from './HTML5';
// import { Touch } from './Touch';
// Q.UA.platform.type === "mobile" || screen.width <= 480

var HTML5 = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return __webpack_require__.e(/*! import() | HTML5Backend */ "vendors~HTML5Backend").then(__webpack_require__.bind(null, /*! react-dnd-html5-backend */ "./node_modules/react-dnd-html5-backend/dist/esm/index.js"));
}); // loadable.lib

var Touch = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return __webpack_require__.e(/*! import() | TouchBackend */ "vendors~TouchBackend").then(__webpack_require__.bind(null, /*! react-dnd-touch-backend */ "./node_modules/react-dnd-touch-backend/dist/esm/index.js"));
});
function DndBackend(_ref) {
  var children = _ref.children;
  var IS_TOUCH = 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
      bg: "/icon/android-icon-36x36.png"
    })
  }, ["mobile", "tablet"].includes(Q_appData.UA.platform.type) && IS_TOUCH ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Touch, null, function (_ref2) {
    var TouchBackend = _ref2.TouchBackend;
    return children(TouchBackend);
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HTML5, null, function (_ref3) {
    var HTML5Backend = _ref3.HTML5Backend;
    return children(HTML5Backend);
  }));
}
/* 
<Flex justify="center" align="center" className="h-100 cwait">
  <div className="spinner-border text-primary" style={{ width: '3rem', height: '3rem' }} role="status" />
</Flex>

fallback={date.toLocaleDateString()} 
*/

/***/ }),

/***/ "./resources/js/src/components/directory/utils.js":
/*!********************************************************!*\
  !*** ./resources/js/src/components/directory/utils.js ***!
  \********************************************************/
/*! exports provided: treeType, searchDFS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "treeType", function() { return treeType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchDFS", function() { return searchDFS; });
// 
var treeType = {
  DIR: "directory",
  FILE: "file"
}; // FROM: https://github.com/anuraghazra/react-folder-tree

var searchDFS = function searchDFS(_ref) {
  var data = _ref.data,
      cond = _ref.cond,
      _ref$childPathKey = _ref.childPathKey,
      childPathKey = _ref$childPathKey === void 0 ? "files" : _ref$childPathKey;
  var _final = null;
  var parentPath = [];
  var parent = null;
  var next = null;
  var prev = null;

  var recursiveFind = function recursiveFind(tree) {
    tree.forEach(function (item, index) {
      if (cond(item, index)) {
        _final = item;

        if (parentPath) {
          parentPath.forEach(function (p) {
            // check if parent has the `current item`
            if (p && p[childPathKey].includes(item)) {
              parent = p; // set next & previous indexes

              next = p[childPathKey][index + 1];
              prev = p[childPathKey][index - 1];
            } else {
              parent = tree; // if parent is null then check the root of the tree

              next = tree[index + 1];
              prev = tree[index - 1];
            }
          });
        }

        return;
      }

      if (item[childPathKey]) {
        // push parent stack
        parentPath.push(item);
        recursiveFind(item[childPathKey]);
      }
    });
  };

  recursiveFind(data);
  return {
    parent: parent,
    item: _final,
    nextSibling: next,
    previousSibling: prev
  };
};



/***/ }),

/***/ "./resources/js/src/pages/admin/devs/DirectoryPage.js":
/*!************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/DirectoryPage.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DirectoryPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dnd */ "./node_modules/react-dnd/dist/esm/index.js");
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_directory_parts_DndBackend__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/directory/parts/DndBackend */ "./resources/js/src/components/directory/parts/DndBackend.js");
/* harmony import */ var _components_directory_Directories__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/directory/Directories */ "./resources/js/src/components/directory/Directories.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // , { useState, useEffect, useRef, } 


 // import Flex from '../../../components/q-ui-react/Flex';

 // import DirectoryDetails from '../../../components/directory/DirectoryDetails';


var DATAS = [{
  "path": "/package.json",
  "type": "file",
  // isFile: true, 
  "contentType": "application/json",
  // contentType
  "integrity": "sha384-Pdp+4TbCSkCdnlbhVzYTcNg0VdRQEnLsrB8A18y/tptwTZUq6Zn8BnWcA4Om7E3I",
  "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
  "size": 759
}, {
  "path": "/build-info.json",
  "type": "file",
  "contentType": "application/json",
  "integrity": "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
  "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
  "size": 163
}, {
  "path": "/index.js",
  "type": "file",
  "contentType": "application/javascript",
  "integrity": "sha384-uYepeL3qyzb/7G5T1fizoxPoKmV6ftFXdz4jeQlBff4HqMuNVJPqiNjvN38BeHUk",
  "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
  "size": 190
}, {
  "path": "/LICENSE",
  "type": "file",
  "contentType": "text/plain",
  "integrity": "sha384-dIN5gxuW3odCH42AKpxql36fRQlrVsjCi4EJgGC2Ty8Ihkcq9cdrChEZpKhSAfks",
  "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
  "size": 1086
}, {
  "path": "/README.md",
  "type": "file",
  "contentType": "text/markdown",
  "integrity": "sha384-WuEvTet+scWBliRT6iLsyZZfFLBEFU/QBmfzdOvLj9mfU4CNvX7WbMosvPxHLjrS",
  "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
  "size": 737
}, {
  "path": "/cjs",
  "type": "directory",
  // isDirectory: true, 
  "files": [{
    "path": "/cjs/react.development.js",
    "type": "file",
    "contentType": "application/javascript",
    "integrity": "sha384-M/8nyoIVh1dygRP3vsJsXo7izkbkAKPA2bON89F2e26vho6tVvv08kWLp2hZ7M86",
    "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
    "size": 74430
  }, {
    "path": "/cjs/react.production.min.js",
    "type": "file",
    "contentType": "application/javascript",
    "integrity": "sha384-sbVXGGS221NzmofOIH3f7+u0yCox/aqW276fniG9ttEWvZV8NSnpj+fFo7ZxrjRf",
    "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
    "size": 6645
  }]
}, {
  "path": "/umd",
  "type": "directory",
  // isDirectory: true, 
  "files": [{
    path: "/umd/test",
    type: "directory",
    files: [{
      "path": "/umd/test/dev.js",
      "type": "file",
      "contentType": "application/javascript",
      "integrity": "sha384-5l9PFjfzS2E5Gx88inFin+gZtXaPxAFp9a54+T34j",
      "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
      "size": 1156
    }, {
      "path": "/umd/test/testDev.js",
      "type": "file",
      "contentType": "application/javascript",
      "integrity": "sha384-4l9PFjfzS2E5Gx88inFin+gZtXaPxAFp9a54+Tdsdsd3",
      "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
      "size": 1323
    }]
  }, {
    "path": "/umd/react.development.js",
    "type": "file",
    "contentType": "application/javascript",
    "integrity": "sha384-5l9PFjfzS2E5Gx88inFinqBHRa+gZtXaPxAFp9a54+T34j/Mp4dKFndKLBWJs9dz",
    "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
    "size": 115653
  }, {
    "path": "/umd/react.production.min.js",
    "type": "file",
    "contentType": "application/javascript",
    "integrity": "sha384-3LaF+GDeugL4KEmnsbb+HuxO42PMnX+W5VYrAF98SHC8Wq47T6D7WpBckxQuazOA",
    "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
    "size": 12588
  }, {
    "path": "/umd/react.profiling.min.js",
    "type": "file",
    "contentType": "application/javascript",
    "integrity": "sha384-lclrAlvhGDKbAhot+O7b99hyYkco0LGhaVbQwwBOkCfdx+OE3/Qonk2VvSvU1tVN",
    "lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
    "size": 16191
  }]
}];
function DirectoryPage() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(DATAS),
      _useState2 = _slicedToArray(_useState, 2),
      tree = _useState2[0],
      setTree = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      cutData = _useState4[0],
      setCutData = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState6 = _slicedToArray(_useState5, 2),
      copyData = _useState6[0],
      setCopyData = _useState6[1];
  /* useEffect(() => {
  	console.log('%cuseEffect in DirectoryPage','color:yellow;');
  	
  }, []); */

  /* const moveItem = (id, afterId, nodeId) => {
     if (id == afterId) return
  
  	// let {tree} = this.state
  	let dataTree = tree;
  
     const removeNode = (id, items) => {
       for (const node of items) {
         if (node.id == id) {
           items.splice(items.indexOf(node), 1)
           return
         }
  
         if (node.children && node.children.length) {
           removeNode(id, node.children)
         }
       }
     }
  
     const item = { ...findItem(id, dataTree) }
     if (!item.id) {
       return
     }
  
     const dest = nodeId ? findItem(nodeId, dataTree).children : dataTree
  
     if (!afterId) {
       removeNode(id, dataTree)
       dest.push(item)
     } else {
       const index = dest.indexOf(dest.filter(v => v.id == afterId).shift())
       removeNode(id, dataTree)
       dest.splice(index, 0, item)
     }
  
  	// this.setState({tree});
  	setTree(dataTree);
   }
  
   const findItem = (id, items) => {
     for (const node of items) {
       if (node.id == id) return node
       if (node.children && node.children.length) {
         const result = findItem(id, node.children)
         if (result) {
           return result
         }
       }
     }
  
     return false
   } */


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_2__["default"], {
    title: "Directory Tree"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "Directory Tree"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("pre", {
    className: "alert alert-warning"
  }, "1. Rename Directory & File name (With validation)\n2. Dnd\n3. Cut, Copy & Paste\n4. New File (With validation)\n5. New Directory (With validation)\n6. Upload File"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-3 px-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_directory_parts_DndBackend__WEBPACK_IMPORTED_MODULE_3__["default"], null, function (backend) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dnd__WEBPACK_IMPORTED_MODULE_1__["DndProvider"], {
      backend: backend
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_directory_Directories__WEBPACK_IMPORTED_MODULE_4__["default"] // open 
    , {
      data: tree,
      copy: copyData,
      cut: cutData,
      onDrop: function onDrop(dropData, target) {
        console.log('onDrop dropData: ', dropData);
        console.log('onDrop target: ', target);
      },
      onDelete: function onDelete(data) {
        return setTree(data);
      },
      onCut: function onCut(data, e) {
        // sum
        // console.log('onCut sum: ', sum);
        console.log('onCut data: ', data);
        console.log('onCut e: ', e);
        if (copyData) setCopyData(null);
        setCutData(data);
      },
      onCopy: function onCopy(data, e) {
        console.log('onCopy data: ', data);
        console.log('onCopy e: ', e);
        if (cutData) setCutData(null);
        setCopyData(data);
      },
      onPaste: function onPaste(idx, e) {
        console.log('onPaste e: ', e);
        console.log('onPaste idx: ', idx); // const dataIndex = tree.findIndex(v => v.path === cutData.path);
        // MUST recursive:

        var newData;

        if (copyData) {
          newData = tree.map(function (v, i) {
            if (i === idx) {
              return _objectSpread(_objectSpread({}, v), {}, {
                files: [].concat(_toConsumableArray(v.files), [_objectSpread(_objectSpread({}, copyData), {}, {
                  path: v.path + copyData.path
                })])
              });
            }

            return v;
          });
        } else {
          newData = tree.map(function (v, i) {
            if (i === idx) {
              return _objectSpread(_objectSpread({}, v), {}, {
                files: [].concat(_toConsumableArray(v.files), [_objectSpread(_objectSpread({}, cutData), {}, {
                  path: v.path + cutData.path
                })])
              });
            }

            return v;
          }).filter(function (v) {
            return v.path !== cutData.path;
          });
        } // console.log('onPaste dataIndex: ', dataIndex);


        console.log('onPaste newData: ', newData);
        setTree(newData);
        setCutData(null); // console.log('onPaste label: ', label);
      } // onRename={(i, item) => {
      // console.log('onRename i: ', i);
      // console.log('onRename item: ', item);
      // }} 
      // onToggle
      ,
      onNewFile: function onNewFile(data) {
        console.log('onNewFile DirectoryPage: ', data);
        setTree(data);
      }
    }));
  })));
} // export default DragDropContext(HTML5Backend);

/***/ })

}]);
//# sourceMappingURL=DirectoryPage.chunk.js.map