(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Drops"],{

/***/ "./resources/js/src/pages/admin/devs/Drops.js":
/*!****************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/Drops.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Drops; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useDropzone } from 'react-dropzone';
// import Btn from '../../components/q-ui-react/Btn';

function Drops() {
  // const { acceptedFiles, getRootProps, getInputProps } = useDropzone();
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in Drops','color:yellow;');
  // }, []);
  // const files = acceptedFiles.map(file => {
  // 	return (
  // 		<li key={file.path}>
  // 			{file.path} - {file.size} bytes
  // 		</li>
  // 	)
  // });
  // console.log('files: ', files);
  // const drop
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "jumbotron" // onDrop={}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lead"
  }, "Drag 'n' drop some files here, or click to select files")));
}
/*
      <div {...getRootProps({className: "jumbotron dropzone"})}>
        <input {...getInputProps()} />
        <p className="lead">Drag 'n' drop some files here, or click to select files</p>
      </div>

      <aside>
			<h4>Files</h4>
			<ul>{files}</ul>
			</aside>
*/

/***/ })

}]);
//# sourceMappingURL=Drops.chunk.js.map