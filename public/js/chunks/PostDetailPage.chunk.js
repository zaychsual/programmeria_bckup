(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PostDetailPage"],{

/***/ "./resources/js/src/components/q-ui-react/Placeholder.js":
/*!***************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Placeholder.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Placeholder; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { Fragment, useState, useEffect }
// import Btn from '../../components/q-ui-bootstrap/Btn';

function Placeholder(_ref) {
  var className = _ref.className,
      w = _ref.w,
      h = _ref.h,
      style = _ref.style,
      label = _ref.label,
      _ref$length = _ref.length,
      length = _ref$length === void 0 ? 1 : _ref$length,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "p" : _ref$type,
      etc = _objectWithoutProperties(_ref, ["className", "w", "h", "style", "label", "length", "type"]);

  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in Placeholder','color:yellow;');
  // }, []);
  // const As = "thumb form".includes(type) ? "div" : type; // type === "thumb" || type === "form"
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.toArray(Array.from({
    length: parseInt(length)
  }).map(function () {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, etc, {
      className: "holder ".concat(type).concat(className ? " ".concat(className) : ""),
      style: _objectSpread(_objectSpread({}, style), {}, {
        width: w,
        height: h
      }),
      "aria-label": label
    }));
  }));
}
/*
<Fragment>
	{Array.from({ length: parseInt(length) }).map((v, i) => 
		<div key={i} 
			{...etc}
			
			className={`holder ${type}${className ? ` ${className}` : ""}`}
			style={{
				...style, 
				width: w,
				height: h
			}} 
			
		/>
	)}
</Fragment>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/PostDetailPage.js":
/*!*************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/PostDetailPage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PostDetailPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _parts_PostLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../parts/PostLayout */ "./resources/js/src/parts/PostLayout.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
/* harmony import */ var _components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/q-ui-react/Img */ "./resources/js/src/components/q-ui-react/Img.js");
/* harmony import */ var _components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/q-ui-react/ModalQ */ "./resources/js/src/components/q-ui-react/ModalQ.js");
/* harmony import */ var _components_player_q_PlayerQ__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../components/player-q/PlayerQ */ "./resources/js/src/components/player-q/PlayerQ.js");
/* harmony import */ var _components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../components/q-ui-react/Placeholder */ "./resources/js/src/components/q-ui-react/Placeholder.js");
/* harmony import */ var _utils_clipboard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../utils/clipboard */ "./resources/js/src/utils/clipboard/index.js");
/* harmony import */ var _data_appData__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../data/appData */ "./resources/js/src/data/appData.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }


 // import Head from '../../../components/q-ui-react/Head';










 // import captureVideoFrame from '../../../utils/img/captureVideoFrame';
// const EXTERNAL_VIDEO_KEY = "external-video";
// const EXTERNAL_IMG_KEY = "external-image";
// From Ion Auth CI
// 60 * 60 * 24 * 365 * 2
// if the user_expire is set to zero we'll set the expiration two years from now.
// if($this->config->item('user_expire', 'ion_auth') === 0){
// $expire = (60*60*24*365*2);
// }

/* const DUMMY_DATA = {
	id: "dummy-video-id", 
	created_at: "Feb 14, 2020", 
	title: "Dummy post data", 
	views: "1,359,848", // NOTE: Number parse to separate
	like: 22, // NOTE: Number parse to k = 22k
	dislike: 438, // NOTE: Number parse to separate
	mediaType: "video", 
	media: {
		poster: "/media/poster/video_360.png", // maxresdefault.webp | video_360.png
		// /media/video_360.mp4
		// https://www.youtube.com/watch?v=ysz5S6PUM-U
		// https://youtu.be/6Ovj_XK6yqc
		// url="/media/video_360.mp4" 
		src: "/media/video_360.mp4" 
		// src: [
			// {src:"/media/SampleVideo_240p_10mb.3gp", type:"video/3gp", sizes:240}, // "data-size"
			// {src:"/media/video_360.mp4", type:"video/mp4", sizes:360},
		// ], 
		// tracks: [
			// {kind:"subtitles", src:"/media/vtt/introduction-subtitle-en.vtt", label:"English", srcLang:"en", default:true},
			// {kind:"subtitles", src:"/media/vtt/introduction-subtitle-id.vtt", label:"Bahasa Indonesia", srcLang:"id"},
		// ]
	}
}; */

function kFormat(num) {
  return Math.abs(num) > 999 ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + "k" : Math.sign(num) * Math.abs(num);
} // {
// className, 
// dataEdit, 
// data, 
// children, 
// }


function PostDetailPage() {
  var _data$media, _data$media2;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      data = _useState2[0],
      setData = _useState2[1]; // null | dataEdit


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      modal = _useState4[0],
      setModal = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      tmode = _useState6[0],
      setTmode = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      startCheck = _useState8[0],
      setStartCheck = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("0:00"),
      _useState10 = _slicedToArray(_useState9, 2),
      start = _useState10[0],
      setStart = _useState10[1]; // const [like, setLike] = useState(false);
  // const [dislike, setDislike] = useState(false);
  // const [capture, setCapture] = useState(null);


  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      autoPlay = _useState12[0],
      setAutoPlay = _useState12[1]; // let likeInit;
  // let dislikeInit;


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // console.log('%cuseEffect in PostDetailPage','color:yellow;');
    // if(!dataEdit){
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/DUMMY/json/detail-post-1.json").then(function (r) {
      console.log('r: ', r);

      if (r.status === 200) {
        setData(r.data);
      }
    })["catch"](function (e) {
      console.log('Error: ', e);
    }); // setTimeout(() => {
    // setData(DUMMY_DATA);
    // likeInit = DUMMY_DATA.like;
    // dislikeInit = DUMMY_DATA.dislike;
    // );
    // }, 3000);
    // }
  }, []);
  /* const onLike = () => {
  	// Xhr here save dislike data to db
  	setLike(!like);
  	
  	setData({ 
  		...data, 
  		like: like ? data.like - 1 : data.like + 1
  	});
  	
  	if(dislike){
  		setDislike(false);
  		setData({ ...data, dislike: data.dislike - 1 });
  	}
  };
  
  const onDislike = () => {
  	// Xhr here save dislike data to db
  	setDislike(!dislike);
  	
  	setData({ 
  		...data, 
  		dislike: dislike ? data.dislike - 1 : data.dislike + 1
  	});
  	
  	if(like){
  		setLike(false);
  		setData({ ...data, like: data.like - 1 });
  	}
  }; */

  var onModalShare = function onModalShare() {
    return setModal(!modal);
  };

  var onCopy = function onCopy(txt, e) {
    var et = e === null || e === void 0 ? void 0 : e.target;
    Object(_utils_clipboard__WEBPACK_IMPORTED_MODULE_11__["clipboardCopy"])(txt).then(function () {
      if (et) {
        Q.setAttr(et, {
          "aria-label": "Copied!"
        });
        setTimeout(function () {
          Q.setAttr(et, "aria-label");
        }, 3000);
      }
    })["catch"](function (e) {
      return console.log(e);
    });
  };

  var onSetStart = function onSetStart(e) {
    setStart(e.target.value); // val.length === 0 ? "0:00" : val
  };

  var onBlurStart = function onBlurStart(e) {
    if (e.target.value.length === 0) setStart("0:00");
  }; // DEVS:
  // const onCapture = () => {
  // const frame = captureVideoFrame(".playerQ video", "png");
  // console.log('frame: ', frame);
  // setCapture(frame.uri);// dataUri
  // }
  // console.log("data: ", data);
  // console.log(data?.mediaType);
  // Q.Cx("container pt-0 pt-md-3 pb-3", className)


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_parts_PostLayout__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: "pb-3",
    data: data // rowStyle={{ minHeight:'calc(100vh - 115px)' }} 
    // append={}

  }, data ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-0 px-md-2 col-md-" + (tmode ? 12 : 8)
  }, ((data === null || data === void 0 ? void 0 : data.mediaType.startsWith("video/")) || (data === null || data === void 0 ? void 0 : data.mediaType) === _data_appData__WEBPACK_IMPORTED_MODULE_12__["EXTERNAL_VIDEO_KEY"]) &&
  /*#__PURE__*/
  // data.mediaType === "video"
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_player_q_PlayerQ__WEBPACK_IMPORTED_MODULE_9__["default"], {
    className: "bg-000 shadow-sm" // /media/video_360.mp4
    // https://www.youtube.com/watch?v=ysz5S6PUM-U
    // https://youtu.be/6Ovj_XK6yqc
    // url="/media/video_360.mp4" 
    ,
    url: data.media.src // fileConfig={data.media?.tracks ? { tracks: data.media.tracks } : undefined} 
    ,
    tracks: ((_data$media = data.media) === null || _data$media === void 0 ? void 0 : _data$media.tracks) || undefined,
    poster: (_data$media2 = data.media) === null || _data$media2 === void 0 ? void 0 : _data$media2.poster,
    videoID: data.id,
    wrapStyle: {
      height: tmode ? 'calc(83vh - 48px)' : undefined
    } // 'calc(100vh - 242px)'
    ,
    enablePip: true,
    theaterMode: true,
    tmode: tmode,
    autoPlay: autoPlay,
    onTheaterMode: function onTheaterMode() {
      return setTmode(!tmode);
    },
    onSetAutoPlay: function onSetAutoPlay(c) {
      return setAutoPlay(c);
    }
  }), ((data === null || data === void 0 ? void 0 : data.mediaType.startsWith("image/")) || (data === null || data === void 0 ? void 0 : data.mediaType) === _data_appData__WEBPACK_IMPORTED_MODULE_12__["EXTERNAL_IMG_KEY"]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_7__["default"], {
    frame: true,
    WrapAs: "figure",
    fluid: true,
    frameClass: "mb-0 d-block",
    className: "w-100 bg-dark",
    style: {
      minHeight: 400
    },
    alt: data.title,
    src: data.media.src
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-8 px-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "h5 my-3"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
    wrap: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "fs-14"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("time", null, data.created_at), " \u2022 ", data.views, " views"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group mt-2 mt-lg-0 ml-0 ml-lg-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"] // onClick={onLike} active={like} 
  , {
    kind: "light",
    className: "qi qi-thumbs-up"
  }, kFormat(data.like)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"] // onClick={onDislike} active={dislike} 
  , {
    kind: "light",
    className: "qi qi-thumbs-down"
  }, kFormat(data.dislike)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    onClick: onModalShare,
    kind: "light",
    className: "qi qi-share"
  }, "SHARE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    kind: "light",
    className: "qi qi-plus"
  }, "SAVE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    bsPrefix: "btn-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    variant: "light",
    bsPrefix: "rounded-right qi qi-ellipsis-h"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "button",
    type: "button",
    className: "q-mr qi q-fw qi-flag"
  }, "Report"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "button",
    type: "button",
    className: "q-mr qi q-fw qi-file"
  }, "Open transcript"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-3 py-3 border-top"
  }, Array.from({
    length: 30
  }).map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      key: i
    }, "Dummy - ", i + 1);
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-4 mt-1-next " + (tmode ? "mt-4" : "absolute-lg t0 r0")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
    className: "p-2"
  }, "Up next", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "custom-control custom-switch ml-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "checkbox",
    className: "custom-control-input",
    checked: autoPlay,
    onChange: function onChange(e) {
      return setAutoPlay(e.target.checked);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "custom-control-label"
  }, "Autoplay"))), [{
    thumb: "/media/poster/poster_SampleVideo_360x240_20mb.png"
  }, {
    thumb: "/media/poster/video_360.png"
  }, {
    thumb: "/media/poster/video_360.png"
  }].map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: "media position-relative media-post"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_7__["default"], {
      w: 120,
      h: 65,
      className: "of-cov mr-2 shadow-sm",
      alt: "Post title",
      src: v.thumb
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "media-body lh-normal"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
      As: "h6",
      className: "mb-1 text-sm"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_6__["default"], {
      noLine: true,
      stretched: true,
      kind: "dark",
      to: "/post/id",
      className: "d-block pr-3"
    }, "Post title test long text title user data ok"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
      className: "ml-auto zi-2" // drop="left" 
      // onToggle={(open, e) => {
      // console.log(e);
      // if(open && e){
      // Q.setClass(e.target.closest(".media"), "menuOpen");
      // }
      // }}

    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
      size: "sm",
      variant: "flat",
      bsPrefix: "rounded-right qi qi-ellipsis-v"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
      alignRight: true
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      as: "button",
      type: "button",
      className: "q-mr qi q-fw qi-flag"
    }, "Add to queue"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      as: "button",
      type: "button",
      className: "q-mr qi q-fw qi-clock"
    }, "Save to Watch later"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      as: "button",
      type: "button",
      className: "q-mr qi q-fw qi-playlist-add q-s13"
    }, "Save to playlist"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      as: "button",
      type: "button",
      className: "q-mr qi q-fw qi-ban"
    }, "Not interested"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      as: "button",
      type: "button",
      className: "q-mr qi q-fw qi-flag"
    }, "Report")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "mb-0 small text-muted"
    }, "Paul Gilbert", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "477k views \u2022 ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("time", null, "2 years ago"))));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_8__["default"], {
    open: modal,
    toggle: onModalShare,
    title: "Share",
    position: "center",
    body: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "input-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      readOnly: true,
      className: "form-control",
      value: "https://programmeria.com/" + data.id,
      type: "text"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "input-group-append"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
      onClick: function onClick(e) {
        return onCopy("https://programmeria.com/" + data.id, e);
      },
      kind: "light",
      className: "tip tipTL"
    }, "Copy"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      className: "custom-control custom-checkbox d-inline-block"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "checkbox",
      className: "custom-control-input",
      checked: startCheck,
      onChange: function onChange(e) {
        return setStartCheck(e.target.checked);
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "custom-control-label"
    }, "Start at")), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      className: "form-control form-control-sm d-inline-block w-95px",
      type: "text",
      readOnly: !startCheck,
      value: start,
      onChange: onSetStart,
      onBlur: onBlurStart
    }))
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
    type: "thumb",
    h: 370 // {screen.height / 2} // "55vh" 
    // className="card-img-top"
    // label="Post" 

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
    type: "h5",
    className: "w-50 my-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
    w: "40%"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
    className: "flex1 ml-5"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-4 mt-1-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
    type: "h3",
    className: "pb-1"
  }), [1, 2, 3].map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: v,
      className: "media media-post"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
      w: 120,
      h: 65,
      className: "mr-2"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "media-body lh-normal"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
      type: "h6",
      className: "mb-1"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_10__["default"], {
      className: "mb-0"
    })));
  }))));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/parts/PostLayout.js":
/*!**********************************************!*\
  !*** ./resources/js/src/parts/PostLayout.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PostLayout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/q-ui-react/Placeholder */ "./resources/js/src/components/q-ui-react/Placeholder.js");
 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }




function PostLayout(_ref) {
  var className = _ref.className,
      data = _ref.data,
      rowStyle = _ref.rowStyle,
      append = _ref.append,
      children = _ref.children;
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in PostLayout','color:yellow;');
  // }, []);
  // pb-3
  //  + (tmode ? 12 : 8)
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Q.Cx("container pt-0 pt-md-3", className)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: rowStyle,
    className: "row mx-lg-n2 position-relative"
  }, data ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: data.title
  }), children) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "thumb",
    h: 370 // {screen.height / 2} // "55vh" 
    // className="card-img-top"
    // label="Post" 

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "h5",
    className: "w-50 my-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    w: "40%"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: "flex1 ml-5"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-4 mt-1-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "h3",
    className: "pb-1"
  }), [1, 2, 3].map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: v,
      className: "media media-post"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
      w: 120,
      h: 65,
      className: "mr-2"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "media-body lh-normal"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
      type: "h6",
      className: "mb-1"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
      className: "mb-0"
    })));
  })))), append);
}
/*
<div className={Q.Cx("px-0 px-md-2", colMediaClass)}>

</div>
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=PostDetailPage.chunk.js.map