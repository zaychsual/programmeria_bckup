(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BundlerPage"],{

/***/ "./resources/js/src/apps/repl/bundler/Bundler.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/apps/repl/bundler/Bundler.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Bundler; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _rollup_plugin_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @rollup/plugin-json */ "./node_modules/@rollup/plugin-json/dist/index.es.js");
/* harmony import */ var _rollup_plugin_data_uri__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @rollup/plugin-data-uri */ "./node_modules/@rollup/plugin-data-uri/dist/index.es.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_libraries_BabelStandalone__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/libraries/BabelStandalone */ "./resources/js/src/components/libraries/BabelStandalone.js");
/* harmony import */ var _components_libraries_RollupBrowser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/libraries/RollupBrowser */ "./resources/js/src/components/libraries/RollupBrowser.js");
/* harmony import */ var _utils_path__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./utils/path */ "./resources/js/src/apps/repl/bundler/utils/path.js");
/* harmony import */ var _utils_rollupVersion__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./utils/rollupVersion */ "./resources/js/src/apps/repl/bundler/utils/rollupVersion.js");
/* harmony import */ var _components_monaco_react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../components/monaco-react */ "./resources/js/src/components/monaco-react/index.js");
/* harmony import */ var _components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../components/monaco-react/ide/onDidMount */ "./resources/js/src/components/monaco-react/ide/onDidMount.js");
/* harmony import */ var _data_monaco__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../data/monaco */ "./resources/js/src/data/monaco.js");
/* harmony import */ var _utils_clipboard__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../utils/clipboard */ "./resources/js/src/utils/clipboard/index.js");


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }














var ROLLUP_OPTIONS = {
  format: 'umd',
  // amd, cjs, es / esm, iife, umd, system
  name: 'Qapp',
  version: "1",
  amd: {
    id: "" // define: ''

  },
  globals: {
    "react": "React",
    "react-dom": "ReactDOM"
  } // compact: true

}; // DEVS: Handle monaco error

var ErrorBoundary = /*#__PURE__*/function (_React$Component) {
  _inherits(ErrorBoundary, _React$Component);

  var _super = _createSuper(ErrorBoundary);

  function ErrorBoundary(props) {
    var _this;

    _classCallCheck(this, ErrorBoundary);

    _this = _super.call(this, props);
    _this.state = {
      hasError: false
    };
    return _this;
  }

  _createClass(ErrorBoundary, [{
    key: "componentDidCatch",
    value: function componentDidCatch(err, info) {
      console.log('componentDidCatch err: ', err);
      console.log('componentDidCatch info: ', info);
    }
  }, {
    key: "render",
    value: function render() {
      if (this.state.hasError) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Something went wrong."); // You can render any custom fallback UI
      }

      return this.props.children;
    }
  }], [{
    key: "getDerivedStateFromError",
    value: function getDerivedStateFromError() {
      // error
      return {
        hasError: true
      };
    }
  }]);

  return ErrorBoundary;
}(react__WEBPACK_IMPORTED_MODULE_1___default.a.Component);

var Bundler = /*#__PURE__*/function (_React$Component2) {
  _inherits(Bundler, _React$Component2);

  var _super2 = _createSuper(Bundler);

  function Bundler(props) {
    var _this2;

    _classCallCheck(this, Bundler);

    _this2 = _super2.call(this, props);

    _defineProperty(_assertThisInitialized(_this2), "winErr", function (msg) {
      // , url, lineNo, colNo, errObj
      console.log('window error msg: ', msg);

      if (msg) {
        Q.setClass(document.body, 'monacoErrorNotFix hasErr'); // OPTION hide react dev error iframe

        setTimeout(function () {
          var xErr = Q.domQ('iframe[style]', document.body); // span[title="Click or press Escape to dismiss."]
          // console.log('xErr: ', xErr);
          // if(xErr) xErr.remove();// xErr.click();

          if (xErr && xErr.contentDocument) {
            // setTimeout(() => {
            // console.log('xErr.contentDocument: ', xErr.contentDocument);
            var x = Q.domQ('span[title="Click or press Escape to dismiss."]', xErr.contentDocument);
            if (x) x.click(); // },150);
          }

          Q.setClass(document.body, 'hasErr', 'remove');
        }, 1500); // 5000

        _this2.setState({
          errApp: msg
        }, function () {// window.alert(msg.message);
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "bundleFinal", function () {
      var _this2$state = _this2.state,
          modules = _this2$state.modules,
          activeFile = _this2$state.activeFile; // code, options, selectedProjectModules, 

      var editCodes = modules.map(function (v) {
        // if(activeFile && activeFile.name === v.name){//  && code.length > 0
        // return {
        // ...v,
        // code: this.babelTransform(this.state.code) // activeFile.code
        // }
        // }
        var setCode = activeFile && activeFile.name === v.name ? _this2.state.code : v.code;
        return _objectSpread(_objectSpread({}, v), {}, {
          code: _this2.babelTransform(setCode) // v.code | MUST Check babel transform status

        });
      });
      console.log('modules: ', modules);
      console.log('editCodes: ', editCodes);

      _this2.setState({
        // modules: editCodes, // data.modules
        selectedProjectModules: editCodes,
        // selectModule
        modulesBundle: editCodes
      }, function () {
        _this2.bundle(); // Run rollup bundle

      });
    });

    _defineProperty(_assertThisInitialized(_this2), "babelTransform", function (code) {
      if (window.Babel) {
        try {
          var parseCode = window.Babel.transform(code, {
            // ast: true,
            // code: false,
            // retainLines: true,
            // compact: true, // OPTION ISSUE: CAN'T REPLACE function _extends()
            // minified: true, // OK
            // inputSourceMap: false,
            // sourceMaps: false,
            filename: 'file.tsx',
            comments: false,
            presets: ['es2017', 'react', 'typescript', 'flow'],
            // ,'stage-2' | 'env',
            plugins: [['proposal-class-properties'] // , { "loose": true }
            // 'transform-modules-amd',
            // 'transform-modules-commonjs',
            // 'transform-modules-umd',
            ] // modules:['amd','umd','cjs']

          });
          var resultCode = parseCode ? parseCode.code : "";
          var strExtendsFn = 'function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }'; // const strExtendsFn = 'function _extends(){_extends=Object.assign||function(target){for(var i=1;i<arguments.length;i++){var source=arguments[i];for(var key in source){if(Object.prototype.hasOwnProperty.call(source,key)){target[key]=source[key];}}}return target;};return _extends.apply(this,arguments);}';

          var replaceExtends = resultCode.replace(strExtendsFn, "");
          var ext2ObjectAssign = replaceExtends.replace(/_extends/gm, 'Object.assign').trim(); //

          console.log('%cresultCode: ', 'color:yellow', resultCode);
          console.log('%creplaceExtends: ', 'color:yellow', replaceExtends);
          console.log('%cext2ObjectAssign: ', 'color:yellow', ext2ObjectAssign);
          return ext2ObjectAssign;
        } catch (e) {
          console.warn(e);
          return null;
        }
      } else {
        console.log('%cwindow.Babel NOT AVAILABLE: ', 'color:yellow');
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "bundle", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var _this2$state2, options, selectedProject, selectedProjectModules, codeSplitting, modulesBundle, moduleById, inputOptions, generated;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!window.rollup) {
                _context.next = 21;
                break;
              }

              // rollup, warnings, modules
              _this2$state2 = _this2.state, options = _this2$state2.options, selectedProject = _this2$state2.selectedProject, selectedProjectModules = _this2$state2.selectedProjectModules, codeSplitting = _this2$state2.codeSplitting, modulesBundle = _this2$state2.modulesBundle; // console.log(`running Rollup version %c${window.rollup.VERSION}`, 'color:yellow;');

              if (selectedProject && selectedProjectModules.length) {
                // if(modules.length !== selectedProjectModules.length || selectedProjectModules.some((module, i) => {
                if (modulesBundle.length !== selectedProjectModules.length || selectedProjectModules.some(function (module, i) {
                  var currentModule = modulesBundle[i]; // const currentModule = modules[i];

                  return currentModule.name !== module.name || currentModule.code !== module.code || currentModule.isEntry !== module.isEntry;
                })) {
                  _this2.setState({
                    selectedProject: undefined,
                    // null | ''
                    selectedProjectModules: []
                  });
                }
              } // updateUrl();


              moduleById = {}; // modulesBundle | modules

              modulesBundle.forEach(function (module) {
                moduleById[module.name] = module;
              }); // warnings = [];

              inputOptions = {
                plugins: [{
                  resolveId: function resolveId(importee, importer) {
                    if (!importer) return importee;
                    if (importee[0] !== '.') return false;
                    var resolved = Object(_utils_path__WEBPACK_IMPORTED_MODULE_8__["resolve"])(Object(_utils_path__WEBPACK_IMPORTED_MODULE_8__["dirname"])(importer), importee).replace(/^\.\//, '');
                    if (resolved in moduleById) return resolved;
                    resolved += '.js';
                    if (resolved in moduleById) return resolved;
                    throw new Error("Could not resolve " + importee + " from " + importer); // throw new Error(`Could not resolve '${importee}' from '${importer}'`);
                  },
                  load: function load(id) {
                    return moduleById[id].code;
                  }
                }, Object(_rollup_plugin_json__WEBPACK_IMPORTED_MODULE_2__["default"])(), Object(_rollup_plugin_data_uri__WEBPACK_IMPORTED_MODULE_3__["default"])()],
                onwarn: function onwarn(warning) {
                  // this.setState({warnings: [...warnings, warning]}); // warnings.push(warning);
                  console.group(warning.loc ? warning.loc.file : "");
                  console.warn(warning.message);

                  if (warning.frame) {
                    console.log(warning.frame);
                  }

                  if (warning.url) {
                    console.log("See " + warning.url + " for more information");
                  }

                  console.groupEnd();
                }
              };

              if (codeSplitting) {
                inputOptions.input = modulesBundle.filter(function (module, index) {
                  return index === 0 || module.isEntry;
                }).map(function (module) {
                  return module.name;
                });
              } else {
                inputOptions[Object(_utils_rollupVersion__WEBPACK_IMPORTED_MODULE_9__["supportsInput"])(window.rollup.VERSION) ? 'input' : 'entry'] = 'main.js';
              }

              _context.prev = 7;
              _context.next = 10;
              return window.rollup.rollup(inputOptions);

            case 10:
              _context.next = 12;
              return _context.sent.generate(options);

            case 12:
              generated = _context.sent;

              if (codeSplitting) {
                _this2.setState({
                  output: generated.output,
                  error: null
                });
              } else {
                _this2.setState({
                  output: [generated],
                  error: null
                });
              }

              _context.next = 19;
              break;

            case 16:
              _context.prev = 16;
              _context.t0 = _context["catch"](7);

              // error = err;
              _this2.setState({
                error: _context.t0
              }, function () {
                var error = _this2.state.error;
                if (error.frame) console.log(error.frame);
                setTimeout(function () {
                  throw error;
                });
              });

            case 19:
              _context.next = 22;
              break;

            case 21:
              console.log('%cwindow.rollup NOT load: ', 'color:yellow');

            case 22:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[7, 16]]);
    })));

    _defineProperty(_assertThisInitialized(_this2), "loadCodes", function () {
      // updateSelectedExample
      var selectedProject = _this2.state.selectedProject;

      if (selectedProject) {
        fetch("/DUMMY/rollup/".concat(selectedProject, "/").concat(selectedProject, ".json")).then(function (r) {
          return r.json();
        }).then(function (data) {
          // modules = example.modules;
          // selectedProjectModules = modules.map(module => ({...module}))
          var selectModule = data.modules.map(function (module) {
            return _objectSpread({}, module);
          });
          console.log('data: ', data);
          console.log('modules: ', data.modules);
          console.log('selectModule: ', selectModule);

          _this2.setState({
            moduleTitle: data.title,
            modules: data.modules.map(function (v) {
              return _objectSpread(_objectSpread({}, v), {}, {
                id: v.name + "-" + Q.Qid(3)
              });
            }),
            // data.modules
            selectedProjectModules: selectModule
          });
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "addFile", function () {
      var modules = _this2.state.modules;
      var ID = "Untitled-" + Number(modules.length + 1);
      var QID = ID + "-" + Q.Qid(3);

      _this2.setState(function () {
        return {
          modules: [].concat(_toConsumableArray(modules), [{
            name: ID,
            code: "",
            isEntry: false,
            // For editable & focus input filename
            edit: true,
            id: QID
          }])
        };
      }, function () {
        setTimeout(function () {
          var _Q$domQ;

          return (_Q$domQ = Q.domQ("#" + QID)) === null || _Q$domQ === void 0 ? void 0 : _Q$domQ.focus();
        }, 9);
      });
    });

    _defineProperty(_assertThisInitialized(_this2), "onClickFile", function (e, v) {
      e.stopPropagation();

      if (!v.edit) {
        _this2.setState({
          code: v.code,
          activeFile: v
        }, function () {
          var _this2$ideMain, _this2$ideMain$curren;

          (_this2$ideMain = _this2.ideMain) === null || _this2$ideMain === void 0 ? void 0 : (_this2$ideMain$curren = _this2$ideMain.current) === null || _this2$ideMain$curren === void 0 ? void 0 : _this2$ideMain$curren.focus();
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "onChangeFileName", function (e, v) {
      if (v.edit) {
        var name = e.target.value;

        _this2.setState(function (s) {
          return {
            modules: s.modules.map(function (v2) {
              return v2.name === v.name ? _objectSpread(_objectSpread({}, v2), {}, {
                name: name
              }) : v2;
            })
          };
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "onBlurFile", function (v) {
      if (v.edit) {
        _this2.setState(function (s) {
          return {
            modules: s.modules.map(function (v2) {
              return v2.name === v.name ? _objectSpread(_objectSpread({}, v2), {}, {
                edit: false
              }) : v2;
            })
          };
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "onClickEdit", function (e, v) {
      e.stopPropagation();

      _this2.setState(function (s) {
        return {
          modules: s.modules.map(function (d) {
            return d.name === v.name ? _objectSpread(_objectSpread({}, d), {}, {
              edit: true
            }) : d;
          })
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this2), "onRemoveFile", function (e, v) {
      e.stopPropagation();

      _this2.setState(function (s) {
        return {
          modules: s.modules.filter(function (v2) {
            return v2.name !== v.name;
          })
        };
      }, function () {
        if (_this2.state.activeFile) _this2.setState({
          activeFile: null
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this2), "onRemoveImport", function () {
      var _this2$state3 = _this2.state,
          options = _this2$state3.options,
          output = _this2$state3.output;

      if (options.format === "esm" || options.format === "es") {
        var removeImport = function removeImport() {
          if (output.length > 0) {
            var finalCode = output[0].output[0].code;
            return finalCode.replace(/import\s+?(?:(?:(?:[\w*\s{},]*)\s+from\s+?)|)(?:(?:".*?")|(?:'.*?'))[\s]*?(?:;|$|)/gm, "");
          }
        };

        console.log('removeImport(): ', removeImport()); // this.setState(s => ({
        // output: [
        // {
        // ...s.output, 
        // code: s.output[0].output[0].code.replace(/import\s+?(?:(?:(?:[\w*\s{},]*)\s+from\s+?)|)(?:(?:".*?")|(?:'.*?'))[\s]*?(?:;|$|)/gm, '')
        // }
        // ]
        // }), () => {
        // console.log('this.state.output: ', this.state.output);
        // });
      } // output.length > 0 ? output[0].output[0].code : ''
      // console.log('output: ', output);

    });

    _defineProperty(_assertThisInitialized(_this2), "onZip", function () {
      var _this2$state4 = _this2.state,
          modules = _this2$state4.modules,
          moduleTitle = _this2$state4.moduleTitle;

      if (JSZip && modules.length) {
        var zip = new JSZip(); // console.log(moduleTitle);

        modules.forEach(function (v) {
          zip.file(moduleTitle + "/" + v.name, v.code);
        });
        zip.generateAsync({
          type: "blob"
        }).then(function (blob) {
          console.log("zip: ", blob);
          var url = window.URL || window.webkitURL;
          var data = url.createObjectURL(blob); // console.log("objUrl: ", data);

          var a = Q.makeEl('a');
          a.href = data;
          a.rel = "noopener";
          a.download = moduleTitle;
          setTimeout(function () {
            return a.click();
          }, 1);
          setTimeout(function () {
            return url.revokeObjectURL(a.href);
          }, 4E4); // 40s
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "onChangeEditor", function (e, code) {
      _this2.setState({
        code: code
      }, function () {
        var _this2$state5 = _this2.state,
            modules = _this2$state5.modules,
            activeFile = _this2$state5.activeFile; // console.log('onChange modules: ', modules);
        // console.log('onChange activeFile: ', activeFile);

        if (activeFile && modules.length > 0) {
          // console.log('edit Modules: ');
          _this2.setState({
            modules: modules.map(function (v) {
              return v.id === activeFile.id ? _objectSpread(_objectSpread({}, v), {}, {
                code: code
              }) : v;
            })
          });
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this2), "onClickBundleFormat", function (v) {
      _this2.setState(function (s) {
        return {
          options: _objectSpread(_objectSpread({}, s.options), {}, {
            format: v
          })
        };
      }, function () {
        if (_this2.state.modules.length > 0) _this2.bundleFinal();
      });
    });

    _defineProperty(_assertThisInitialized(_this2), "onCopy", function () {
      var output = _this2.state.output; // let code = output.length > 0 ? output[0].output[0].code : "";

      if (output.length > 0) {
        Object(_utils_clipboard__WEBPACK_IMPORTED_MODULE_13__["clipboardCopy"])(output[0].output[0].code).then(function (t) {
          return console.log('onOk: ');
        })["catch"](function (e) {
          return console.log(e);
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "onDidChangeEditor", function (v, editor) {
      // console.log('editor: ',editor);
      if (!_this2.state.editorReady) {
        _this2.setState({
          editorReady: true
        });

        _this2.ideMain.current = editor; // v | editor

        Object(_components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_11__["default"])(v, editor); // console.log('monaco: ', window.monaco);

        /* window.monaco.languages.registerCompletionItemProvider('javascript', {
        	provideCompletionItems: (model, position) => {
        		// find out if we are completing a property in the 'dependencies' object.
        		// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
        		// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
        		// if(!match){
        			// return { suggestions: [] };
        		// }
        		
        		// let word = model.getWordUntilPosition(position);
        		// let range = {
        			// startLineNumber: position.lineNumber,
        			// endLineNumber: position.lineNumber,
        			// startColumn: word.startColumn,
        			// endColumn: word.endColumn
        		// };
        		return {
        			suggestions: setDefineWindow() // range
        		};
        	}
        }); */
      }
    });

    _this2.state = {
      editorReady: false,
      code: "",
      // string code to <ControlledEditor />
      // files: [],
      activeFile: null,
      libsOk: false,
      output: [],
      options: _objectSpread({}, ROLLUP_OPTIONS),
      codeSplitting: false,
      selectedProject: undefined,
      // BEFORE: selectedExample, OPTIONS:  '' | null | undefined | '00'
      selectedProjectModules: [],
      // BEFORE: selectedExampleModules
      moduleTitle: '',
      modules: [],
      warnings: [],
      // input: '', // null // ???
      error: null,
      modulesBundle: null,
      errApp: null
    };
    _this2.ideMain = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
    return _this2;
  }

  _createClass(Bundler, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this3 = this;

      console.log('%ccomponentDidMount in Bundler', 'color:yellow;');
      /* if(window.Babel && !window.rollup){
      	Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js","data-js":"rollup"}).then(() => {
      		if(window.rollup){
      			Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
      				if(window.JSZip) this.setState({ libsOk: true });
      				else console.log("%cjszip failed", "color:yellow");
      			}).catch(e => console.log(e));
      		}
      	}).catch(e => {
      		console.log('catch e: ', e);
      	});
      }
      else if(window.Babel && window.rollup){
      	this.setState({ libsOk: true });
      }else{
      	Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js","data-js":"rollup"}).then(() => {
      		if(window.rollup){
      			Q.getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js","data-js":"Babel"}).then(() => {
      				if(window.Babel){
      					Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
      						if(window.JSZip) this.setState({ libsOk: true });
      						else console.log("%cjszip failed", "color:yellow");
      					}).catch(e => console.log(e));
      				}
      			}).catch(e => {
      				console.log('catch e: ', e);
      			});
      		}
      	}).catch(e => {
      		console.log('catch e: ', e);
      	});
      			// importShim("https://jspm.dev/jszip").then(m => {
      	// 	console.log("importShim m: ", m);
      	// }).catch(e => console.log("Error importShim e: ", e));
      } */
      // if(window.rollup){
      // 	this.setState({ libsOk: true });
      // }else{
      // 	Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js","data-js":"rollup"}).then(() => {
      // 		if(window.rollup){
      // 			Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
      // 				if(window.JSZip) this.setState({ libsOk: true });
      // 				else console.log("%cjszip failed", "color:yellow");
      // 			}).catch(e => console.log(e));
      // 		}
      // 	}).catch(e => {
      // 		console.log('catch e: ', e);
      // 	});
      // }

      if (window.JSZip) {
        this.setState({
          libsOk: true
        });
      } else {
        Q.getScript({
          src: "/js/libs/jszip/jszip.min.js",
          "data-js": "jszip"
        }).then(function () {
          if (window.JSZip) _this3.setState({
            libsOk: true
          });else console.log("%cjszip failed", "color:yellow");
        })["catch"](function (e) {
          return console.log(e);
        });
      }

      window.addEventListener('error', this.winErr); // DEVS: Handle monaco error
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      // DEVS: Handle monaco error
      window.removeEventListener('error', this.winErr);
      if (Q.hasClass(document.body, 'monacoErrorNotFix')) Q.setClass(document.body, 'monacoErrorNotFix', 'remove');
    } // DEVS: Handle monaco error

  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      // selectedProjectModules, modulesBundle, activeFile, 
      var _this$state = this.state,
          libsOk = _this$state.libsOk,
          code = _this$state.code,
          modules = _this$state.modules,
          moduleTitle = _this$state.moduleTitle,
          options = _this$state.options,
          output = _this$state.output,
          selectedProject = _this$state.selectedProject;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(ErrorBoundary, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_libraries_BabelStandalone__WEBPACK_IMPORTED_MODULE_6__["default"], {
        load: true
      }, function (Babel) {
        if (Babel) {
          if (!window.Babel) {
            window.Babel = Babel;
          }

          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_libraries_RollupBrowser__WEBPACK_IMPORTED_MODULE_7__["default"], {
            load: true
          }, function (rollup) {
            if (rollup) {
              if (!window.rollup) {
                window.rollup = rollup;
              }

              return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                dir: "row",
                className: "mh-full-navmain appBox"
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                dir: "column",
                className: "flexno sticky asideApp",
                style: {
                  width: 190
                }
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("small", null, "Projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
                className: "custom-select custom-select-sm rounded-0 bw-y1",
                value: selectedProject,
                onChange: function onChange(e) {
                  return _this4.setState({
                    selectedProject: e.target.value
                  });
                }
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", null, "Choose Project"), ["Dynamic imports", "Multiple Entry Modules", "Tree-shaking", "Default exports", "Named exports", "External imports", "Static namespaces", "Dynamic namespaces", "React component", "React router dom basic"].map(function (v, i) {
                return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
                  key: i,
                  value: "0" + i
                }, "0", i, ". ", v);
              })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
                className: "px-1 pb-2 border-bottom small"
              }, "Package name:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
                className: "form-control form-control-sm",
                value: moduleTitle,
                onChange: function onChange(e) {
                  return _this4.setState({
                    moduleTitle: e.target.value
                  });
                }
              })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                onClick: _this4.addFile,
                size: "xs"
              }, "Add file"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
                className: "list-group list-group-flush py-2 flex-auto ovxhide q-scroll"
              }, modules.map(function (v, i) {
                return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                  key: i,
                  As: "label",
                  align: "center",
                  className: "list-group-item list-group-item-action p-0 mb-0 select-no fileItem"
                }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
                  type: "text",
                  readOnly: !v.edit,
                  value: v.name,
                  id: v.id || v.name,
                  className: Q.Cx("form-control form-control-sm border-0 rounded-0 bg-transparent", {
                    "shadow-none cpoin select-no": !v.edit
                  }),
                  onChange: function onChange(e) {
                    return _this4.onChangeFileName(e, v);
                  },
                  onClick: function onClick(e) {
                    return _this4.onClickFile(e, v);
                  },
                  onBlur: function onBlur() {
                    return _this4.onBlurFile(v);
                  },
                  onKeyDown: function onKeyDown(e) {
                    if (e.key === "Enter") _this4.onBlurFile(v);
                  }
                }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
                  className: "btn-group btn-group-xs mr-1"
                }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                  onClick: function onClick(e) {
                    return _this4.onClickEdit(e, v);
                  },
                  As: "div",
                  hidden: v.edit // {"tip tipL qi q-fw qi-" + (v.edit ? "check":"pen")} 
                  ,
                  className: "tip tipL qi qi-pen",
                  "aria-label": "Rename"
                }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                  blur: true,
                  onClick: function onClick(e) {
                    return _this4.onRemoveFile(e, v);
                  },
                  className: "tip tipL qi qi-close xx",
                  "aria-label": "Remove"
                })));
              }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                dir: "row",
                grow: "1",
                className: "appMain"
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                dir: "column",
                className: "w-50"
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("nav", {
                className: "flexno ml-1-next"
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                onClick: _this4.loadCodes,
                size: "sm",
                kind: "danger",
                className: "bw-x1"
              }, "Load Codes"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                onClick: _this4.bundleFinal,
                disabled: !selectedProject && !(modules.length > 0),
                size: "sm",
                kind: "warning",
                className: "bw-x1"
              }, "Bundle Final"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                onClick: _this4.onRemoveImport,
                size: "sm",
                disabled: output.length < 1,
                className: "bw-x1"
              }, "Remove imports"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                size: "sm",
                className: "bw-x1",
                onClick: function onClick() {
                  console.log('modules: ', modules);
                }
              }, "RUN"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                onClick: _this4.onZip,
                size: "sm"
              }, "ZIP")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
                className: "ovhide position-relative flex-auto"
              }, libsOk && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_monaco_react__WEBPACK_IMPORTED_MODULE_10__["ControlledEditor"], {
                className: "position-absolute position-full" // flex-auto 
                ,
                editorDidMount: _this4.onDidChangeEditor // value={selectedProjectModules && selectedProjectModules.length > 0 ? JSON.stringify(selectedProjectModules) : ''}
                ,
                value: code,
                onChange: _this4.onChangeEditor,
                language: "javascript",
                theme: "dark" // {theme}
                ,
                options: _data_monaco__WEBPACK_IMPORTED_MODULE_12__["MONACO_OPTIONS"]
              }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                dir: "column",
                className: "w-50"
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
                nowrap: true,
                align: "center",
                className: "flexno ml-1-next"
              }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
                className: "flexno"
              }, "Format:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
                className: "btn-group btn-group-sm flex1"
              }, ['amd', 'cjs', 'esm', 'iife', 'umd', 'system'].map(function (v, i) {
                return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                  key: i,
                  kind: "dark",
                  className: "rounded-0 bw-x1 shadow-none",
                  active: options.format === v,
                  onClick: function onClick() {
                    return _this4.onClickBundleFormat(v);
                  }
                }, v);
              }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
                onClick: _this4.onCopy,
                size: "sm",
                kind: "dark",
                className: "rounded-0 bw-x1 shadow-none"
              }, "Copy"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
                className: "ovhide position-relative flex-auto"
              }, libsOk && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_monaco_react__WEBPACK_IMPORTED_MODULE_10__["ControlledEditor"], {
                className: "position-absolute position-full",
                value: output.length > 0 ? output[0].output[0].code : '' // onChange={(e, value) => this.setState({code: value})} 
                ,
                language: "javascript",
                theme: "dark" // {theme}
                ,
                options: _data_monaco__WEBPACK_IMPORTED_MODULE_12__["MONACO_OPTIONS"]
              })))));
            }

            return null;
          });
        }

        return null;
      }));
    }
  }]);

  return Bundler;
}(react__WEBPACK_IMPORTED_MODULE_1___default.a.Component);



/***/ }),

/***/ "./resources/js/src/apps/repl/bundler/utils/path.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/apps/repl/bundler/utils/path.js ***!
  \**********************************************************/
/*! exports provided: absolutePath, relativePath, isAbsolute, isRelative, dirname, resolve */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "absolutePath", function() { return absolutePath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "relativePath", function() { return relativePath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAbsolute", function() { return isAbsolute; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isRelative", function() { return isRelative; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dirname", function() { return dirname; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolve", function() { return resolve; });
// eslint-disable-next-line
var absolutePath = /^(?:\/|(?:[A-Za-z]:)?[\\|\/])/;
var relativePath = /^\.?\.\//;
function isAbsolute(path) {
  return absolutePath.test(path);
}
function isRelative(path) {
  return relativePath.test(path);
}
/* export function basename(path){
	return path.split(/(\/|\\)/).pop();
} */

function dirname(path) {
  // eslint-disable-next-line
  var match = /(\/|\\)[^\/\\]*$/.exec(path);
  if (!match) return '.';
  var dir = path.slice(0, -match[0].length); // If `dir` is the empty string, we're at root.

  return dir ? dir : '/';
}
/* export function extname(path){
	// eslint-disable-next-line
	const match = /\.[^\.]+$/.exec(basename(path));
	if(!match) return '';
	return match[0];
} */

/* export function relative(from, to){
	// eslint-disable-next-line
	const fromParts = from.split(/[\/\\]/).filter(Boolean);
	// eslint-disable-next-line
	const toParts = to.split(/[\/\\]/).filter(Boolean);

	while (fromParts[0] && toParts[0] && fromParts[0] === toParts[0]){
		fromParts.shift();
		toParts.shift();
	}

	while (toParts[0] === '.' || toParts[0] === '..'){
		const toPart = toParts.shift();
		if(toPart === '..'){
			fromParts.pop();
		}
	}

	while (fromParts.pop()){
		toParts.unshift('..');
	}

	return toParts.join('/');
} */

function resolve() {
  for (var _len = arguments.length, paths = new Array(_len), _key = 0; _key < _len; _key++) {
    paths[_key] = arguments[_key];
  }

  // eslint-disable-next-line
  var resolvedParts = paths.shift().split(/[\/\\]/);
  paths.forEach(function (path) {
    if (isAbsolute(path)) {
      // eslint-disable-next-line
      resolvedParts = path.split(/[\/\\]/);
    } else {
      // eslint-disable-next-line
      var parts = path.split(/[\/\\]/);

      while (parts[0] === '.' || parts[0] === '..') {
        var part = parts.shift();

        if (part === '..') {
          resolvedParts.pop();
        }
      }

      resolvedParts.push.apply(resolvedParts, parts);
    }
  });
  return resolvedParts.join('/');
}

/***/ }),

/***/ "./resources/js/src/apps/repl/bundler/utils/rollupVersion.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/apps/repl/bundler/utils/rollupVersion.js ***!
  \*******************************************************************/
/*! exports provided: supportsInput, supportsCodeSplitting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsInput", function() { return supportsInput; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsCodeSplitting", function() { return supportsCodeSplitting; });
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var isRollupVersionAtLeast = function isRollupVersionAtLeast(version, major, minor) {
  if (!version) return true;

  var _version$split$map = version.split('.').map(Number),
      _version$split$map2 = _slicedToArray(_version$split$map, 2),
      currentMajor = _version$split$map2[0],
      currentMinor = _version$split$map2[1];

  return currentMajor > major || currentMajor === major && currentMinor >= minor;
};

var supportsInput = function supportsInput(version) {
  return isRollupVersionAtLeast(version, 0, 48);
};
var supportsCodeSplitting = function supportsCodeSplitting(version) {
  return isRollupVersionAtLeast(version, 1, 0);
};

/***/ }),

/***/ "./resources/js/src/components/libraries/BabelStandalone.js":
/*!******************************************************************!*\
  !*** ./resources/js/src/components/libraries/BabelStandalone.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BabelStandalone; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loadable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @loadable/component */ "./node_modules/@loadable/component/dist/loadable.esm.js");
/* harmony import */ var _q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
 // useState, useEffect, 
// import loadable from '@loadable/component';

 // loadable

 // const Moment = loadable.lib(() => import('@babel/standalone'));

var Lib = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return __webpack_require__.e(/*! import() | BabelStandalone */ "vendors~BabelStandalone").then(__webpack_require__.t.bind(null, /*! @babel/standalone */ "./node_modules/@babel/standalone/babel.js", 7));
}); // props.module

function BabelStandalone(_ref) {
  var children = _ref.children,
      load = _ref.load;

  // , onLoad
  // const loadProps = Q.isBool(load);
  // const [isLoad, setIsLoad] = useState(loadProps);
  // useEffect(() => {
  //   if(!isLoad) setIsLoad(true);
  // }, [isLoad]);
  if (load) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
      fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
        bg: "/icon/android-icon-36x36.png"
      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Lib, null, function (Babel) {
      if (Babel && children) {
        // window.Babel = Babel;
        // if(onLoad) onLoad(Babel);
        return children(Babel);
      }

      return null;
    }));
  }

  return null;
}
/* 
if(load){
  if(window.Babel){
    return children(window.Babel);
  }

  return (
    <Suspense fallback={<SpinLoader bg="/icon/android-icon-36x36.png" />}>
      <Lib>
        {(Babel) => {
          if(Babel && children){
            window.Babel = Babel;

            return children(Babel);
          }
          return null;
        }}
      </Lib>
    </Suspense>
  )
}
*/
// export default BabelStandalone;
// export { LoadLibrary };

/***/ }),

/***/ "./resources/js/src/components/libraries/RollupBrowser.js":
/*!****************************************************************!*\
  !*** ./resources/js/src/components/libraries/RollupBrowser.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RollupBrowser; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loadable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @loadable/component */ "./node_modules/@loadable/component/dist/loadable.esm.js");
/* harmony import */ var _q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
 // useState, useEffect, 



var Lib = _loadable_component__WEBPACK_IMPORTED_MODULE_1__["lazy"].lib(function () {
  return Promise.all(/*! import() | RollupBrowser */[__webpack_require__.e("vendors~BundlerPage~RollupBrowser"), __webpack_require__.e("vendors~RollupBrowser")]).then(__webpack_require__.t.bind(null, /*! rollup/dist/rollup.browser.js */ "./node_modules/rollup/dist/rollup.browser.js", 7));
});
function RollupBrowser(_ref) {
  var children = _ref.children,
      load = _ref.load;

  if (load) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
      fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
        bg: "/icon/android-icon-36x36.png"
      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Lib, null, function (rollup) {
      if (rollup && children) {
        return children(rollup);
      }

      return null;
    }));
  }

  return null;
}

/***/ }),

/***/ "./resources/js/src/data/monaco.js":
/*!*****************************************!*\
  !*** ./resources/js/src/data/monaco.js ***!
  \*****************************************/
/*! exports provided: MONACO_THEMES, MONACO_LANGS, MONACO_OPTIONS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MONACO_THEMES", function() { return MONACO_THEMES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MONACO_LANGS", function() { return MONACO_LANGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MONACO_OPTIONS", function() { return MONACO_OPTIONS; });
// SETUP
var MONACO_THEMES = ["dark", "light"]; // defaultThemes
// const monacoThemes = [
//   "Active4D",
//   "All Hallows Eve",
//   "Amy",
//   "Birds of Paradise",
//   "Blackboard",
//   "Brilliance Black",
//   "Brilliance Dull",
//   "Chrome DevTools",
//   "Clouds Midnight",
//   "Clouds",
//   "Cobalt",
//   "Dawn",
//   "Dominion Day",
//   "Dreamweaver",
//   "Eiffel",
//   "Espresso Libre",
//   "GitHub",
//   "IDLE",
//   "Katzenmilch",
//   "Kuroir Theme",
//   "LAZY",
//   "MagicWB (Amiga)",
//   "Merbivore Soft",
//   "Merbivore",
//   "Monokai Bright",
//   "Monokai",
//   "Night Owl",
//   "Oceanic Next",
//   "Pastels on Dark",
//   "Slush and Poppies",
//   "Solarized-dark",
//   "Solarized-light",
//   "SpaceCadet",
//   "Sunburst",
//   "Textmate (Mac Classic)",
//   "Tomorrow-Night-Blue",
//   "Tomorrow-Night-Bright",
//   "Tomorrow-Night-Eighties",
//   "Tomorrow-Night",
//   "Tomorrow",
//   "Twilight",
//   "Upstream Sunburst",
//   "Vibrant Ink",
//   "Xcode_default",
//   "Zenburnesque",
//   "iPlastic",
//   "idleFingers",
//   "krTheme",
//   "monoindustrial",
//   "themelist",
// ];
// monacoLangs

var MONACO_LANGS = [{
  id: 1,
  name: "apex",
  type: "text/x-java",
  ex: "cls"
}, // {id:2, name:"azcli", type:"", ex:""}, // ???
// {id:3, name:"bat", type:"application/bat", ex:"bat"},
{
  id: 4,
  name: "c",
  type: "text/x-csrc",
  ex: "c"
}, {
  id: 5,
  name: "clojure",
  type: "text/x-clojure",
  ex: "clj"
}, {
  id: 6,
  name: "coffeescript",
  type: "text/x-coffeescript",
  ex: "cson"
}, {
  id: 7,
  name: "cpp",
  type: "text/x-c++src",
  ex: "cpp"
}, {
  id: 8,
  name: "csharp",
  type: "text/x-csharp",
  ex: "cs"
}, // {id:9, name:"csp", type:"", ex:"csp"},
{
  id: 10,
  name: "css",
  type: "text/css",
  ex: "css"
}, // {id:11, name:"dockerfile", type:"text/x-dockerfile", ex:"dockerfile"},
// {id:12, name:"fsharp", type:"text/x-fsharp", ex:"fs"},
{
  id: 13,
  name: "go",
  type: "text/x-go",
  ex: "go"
}, {
  id: 14,
  name: "graphql",
  type: "text/plain",
  ex: "graphql"
}, // ex:"gql"
// {id:15, name:"handlebars", type:"", ex:"hbs"},
{
  id: 16,
  name: "html",
  type: "text/html",
  ex: "html"
}, {
  id: 17,
  name: "ini",
  type: "text/x-properties",
  ex: "ini"
}, {
  id: 18,
  name: "java",
  type: "text/x-java",
  ex: "java"
}, {
  id: 19,
  name: "javascript",
  type: "text/javascript",
  ex: "js"
}, {
  id: 20,
  name: "json",
  type: "application/json",
  ex: "json"
}, {
  id: 21,
  name: "kotlin",
  type: "text/x-kotlin",
  ex: "kt"
}, {
  id: 22,
  name: "less",
  type: "text/css",
  ex: "less"
}, // {id:23, name:"lua", type:"text/x-lua", ex:"lua"},
{
  id: 24,
  name: "markdown",
  type: "text/x-gfm",
  ex: "md"
}, // {id:25, name:"msdax", type:"", ex:""}, // ???
// {id:26, name:"mysql", type:"text/x-sql", ex:"sql"},
{
  id: 27,
  name: "objective-c",
  type: "text/x-objectivec",
  ex: "m"
}, {
  id: 28,
  name: "pascal",
  type: "text/x-pascal",
  ex: "pas"
}, {
  id: 29,
  name: "perl",
  type: "text/x-perl",
  ex: "pl"
}, // {id:30, name:"pgsql", type:"text/x-sql", ex:""}, // ???
{
  id: 31,
  name: "php",
  type: "application/x-httpd-php",
  ex: "php"
}, {
  id: 32,
  name: "plaintext",
  type: "text/plain",
  ex: "txt"
}, // {id:33, name:"postiats", type:"", ex:""}, // ???
// {id:34, name:"powerquery", type:"", ex:""}, // ???
{
  id: 35,
  name: "powershell",
  type: "application/x-powershell",
  ex: "ps1"
}, {
  id: 36,
  name: "pug",
  type: "text/x-pug",
  ex: "jade"
}, {
  id: 37,
  name: "python",
  type: "text/x-python",
  ex: "py"
}, {
  id: 38,
  name: "r",
  type: "text/x-rsrc",
  ex: "r"
}, // {id:39, name:"razor", type:"", ex:""}, // ???
// {id:40, name:"redis", type:"", ex:""}, // ???
// {id:41, name:"redshift", type:"", ex:""}, // ???
{
  id: 42,
  name: "ruby",
  type: "text/x-ruby",
  ex: "rb"
}, // {id:43, name:"rust", type:"", ex:""}, // ???
// {id:44, name:"sb", type:"", ex:""}, // ???
{
  id: 45,
  name: "scheme",
  type: "text/x-scheme",
  ex: "scm"
}, {
  id: 46,
  name: "scss",
  type: "text/x-scss",
  ex: "scss"
}, {
  id: 47,
  name: "shell",
  type: "text/x-sh",
  ex: "sh"
}, // {id:48, name:"sol", type:"", ex:""}, // ???
{
  id: 49,
  name: "sql",
  type: "",
  ex: "sql"
}, // type:"text/x-sql"
// {id:50, name:"st", type:"", ex:""}, // ???
{
  id: 51,
  name: "swift",
  type: "text/x-swift",
  ex: "swift"
}, // {id:52, name:"tcl", type:"", ex:""}, // ???
{
  id: 53,
  name: "typescript",
  type: "text/plain",
  ex: "ts"
}, {
  id: 54,
  name: "vb",
  type: "text/x-vb",
  ex: "vb"
}, {
  id: 55,
  name: "xml",
  type: "text/xml",
  ex: "xml"
}, {
  id: 56,
  name: "yaml",
  type: "text/x-yaml",
  ex: "yml"
}];
var MONACO_OPTIONS = {
  // Consolas, "Courier New", monospace
  fontFamily: 'Monaco, SFMono-Regular, Menlo, Consolas, "Liberation Mono", "Courier New", monospace',
  fontSize: 12,
  // 14
  tabSize: 2,
  // tabSize
  // dragAndDrop: false,
  minimap: {
    enabled: false // Defaults = true
    // side: minimapSide, // Default = right | left
    // showSlider: 'always', // Default = mouseover
    // renderCharacters: false, // Default =  true

  } // colorDecorators: false, // Css color preview
  // readOnly: true,
  // mouseWheelZoom: zoomIde

};
/** END: user define object */

/* function setDefineWindow(range, mon){
	// returning a static list of proposals, not even looking at the prefix (filtering is done by the Monaco editor),
	// here you could do a server side lookup
	// return [
		// {
			// label: '"lodash"',
			// kind: mon.languages.CompletionItemKind.Function,
			// documentation: "The Lodash library exported as Node.js modules.",
			// insertText: '"lodash": "*"',
			// range: range
		// },{
			// label: '"express"',
			// kind: mon.languages.CompletionItemKind.Function,
			// documentation: "Fast, unopinionated, minimalist web framework",
			// insertText: '"express": "*"',
			// range: range
		// }
	// ];

// !v.startsWith('Esm_') : For Es Module have store in window
// eslint-disable-next-line
	return window.getDefineObj().filter(v => {
		let wv = window[v];
		// // !['__core-js_shared__','__REACT_DEVTOOLS_APPEND_COMPONENT_STACK__','__REACT_DEVTOOLS_COMPONENT_FILTERS__','__VUE_DEVTOOLS_TOAST__'].includes(v)
		if(wv && !wv.toString().includes('[Command Line API]') && !(v.startsWith('__') || v.endsWith('__')) && !v.startsWith('Esm_')){
			return v;
		}
	}).map(v => ({
		label: v,
		documentation: window[v].toString(),
		insertText: v
	}));
	
	// return setWin.map(v => ({
		// label: v,
		// insertText: v
	// }));
} */

/** END: Get user define object */

 // setDefineWindow, monacoThemes, defaultThemes, monacoLangs

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/BundlerPage.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/BundlerPage.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BundlerPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _apps_repl_bundler_Bundler__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../apps/repl/bundler/Bundler */ "./resources/js/src/apps/repl/bundler/Bundler.js");
 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }



function BundlerPage() {
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in BundlerPage','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Bundler"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_repl_bundler_Bundler__WEBPACK_IMPORTED_MODULE_2__["default"], null));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/utils/clipboard/index.js":
/*!***************************************************!*\
  !*** ./resources/js/src/utils/clipboard/index.js ***!
  \***************************************************/
/*! exports provided: clipboardCopy, copyFn, pasteBlob, permissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clipboardCopy", function() { return clipboardCopy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyFn", function() { return copyFn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pasteBlob", function() { return pasteBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "permissions", function() { return permissions; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Copy to clipboard:
function clipboardCopy(target) {
  return new Promise(function (resolve, reject) {
    var clipboard = navigator.clipboard,
        txt;
    if (typeof target === 'string' && !target.tagName) txt = target;else if (target === null || target === void 0 ? void 0 : target.tagName) txt = target.textContent; // target.innerText
    else reject(new Error('Text or target DOM to copy is required.'));

    if (clipboard) {
      clipboard.writeText(txt).then(function () {
        return resolve(txt);
      })["catch"](function (e) {
        return reject(e);
      });
    } else {
      console.log('%cnavigator.clipboard NOT SUPPORT', 'color:yellow');
      copyFn(txt, {
        onOk: function onOk() {
          resolve(txt);
        },
        onErr: function onErr(e) {
          reject(e);
        }
      });
    }
  });
}

function copyFn(str, _ref) {
  var _ref$onOk = _ref.onOk,
      onOk = _ref$onOk === void 0 ? function () {} : _ref$onOk,
      _ref$onErr = _ref.onErr,
      onErr = _ref$onErr === void 0 ? function () {} : _ref$onErr;
  var DOC = document,
      el = DOC.createElement("textarea"),
      iOS = window.navigator.userAgent.match(/ipad|iphone/i);
  el.className = "sr-only sr-only-focusable";
  el.contentEditable = true; // needed for iOS >= 10

  el.readOnly = false; // needed for iOS >= 10

  el.value = str; // el.style.border = "0";
  // el.style.padding = "0";
  // el.style.margin = "0";
  // el.style.position = "absolute";
  // sets vertical scroll
  // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
  // el.style.top = `${yPosition}px`;

  DOC.body.appendChild(el);

  if (iOS) {
    var range = DOC.createRange();
    range.selectNodeContents(el);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    el.setSelectionRange(0, 999999);
  } else {
    el.focus({
      preventScroll: false
    });
    el.select();
  }

  var OK = DOC.execCommand("copy");

  if (OK) {
    onOk(str);
  } else {
    onErr('Failed to copy');
  }

  el.remove(); // DOC.body.removeChild(el);
} // 
// const pasteBlob = async (src) => {
// 	try{
// 		// const imgURL = '/images/generic/file.png';
// 		const data = await fetch(src);
// 		const blob = await data.blob();
// 		await navigator.clipboard.write([
// 			new ClipboardItem({
// 				[blob.type]: blob
// 			})
// 		]);
// 		console.log('Image copied.');
// 	}catch(e){
// 		console.error(e.name, e.message);
// 	}
// }
// async function pasteBlob(){
//   try{
// 		if(!navigator.clipboard.read) return false;
// 		const clipboardItems = await navigator.clipboard.read();
// 		// console.log('clipboardItems: ', clipboardItems);
// 		let data = false;
//     for(const item of clipboardItems){
//       for(const type of item.types){
// 				const blob = await item.getType(type);
// 				// console.log('blob: ', blob);
// 				if(blob.type.startsWith("image/")){
// 					// const objURL = window.URL.createObjectURL(blob);
// 					// console.log('objURL: ', objURL);
// 					data = blob;
// 				}
//       }
// 		}
// 		return data;
//   }catch(e){
// 		console.error(e.name, e.message);
// 		return false;
//   }
// }


function pasteBlob(_x) {
  return _pasteBlob.apply(this, arguments);
} // CHECK Permisson paste
// clipboard-write - granted by default


function _pasteBlob() {
  _pasteBlob = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
    var items, lng, i;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (e.clipboardData) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            items = e.clipboardData.items;

            if (items) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return");

          case 6:
            lng = items.length;
            i = 0;

          case 8:
            if (!(i < lng)) {
              _context.next = 15;
              break;
            }

            if (!(items[i].type.indexOf("image/") === -1)) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("continue", 12);

          case 11:
            return _context.abrupt("return", items[i].getAsFile());

          case 12:
            i++;
            _context.next = 8;
            break;

          case 15:
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log('Error pasteBlob e: ', _context.t0);
            return _context.abrupt("return", false);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));
  return _pasteBlob.apply(this, arguments);
}

function permissions() {
  return _permissions.apply(this, arguments);
}

function _permissions() {
  _permissions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var query;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return navigator.permissions.query({
              name: "clipboard-read",
              allowWithoutGesture: false
            });

          case 3:
            query = _context2.sent;
            // Will be 'granted', 'denied' or 'prompt':
            console.log(query.state); // Listen for changes to the permission state

            query.onchange = function () {
              console.log(query.state);
            };

            return _context2.abrupt("return", query);

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", false);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 9]]);
  }));
  return _permissions.apply(this, arguments);
}



/***/ })

}]);
//# sourceMappingURL=BundlerPage.chunk.js.map