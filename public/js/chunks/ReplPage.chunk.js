(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ReplPage"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/react-bootstrap/esm/Collapse.js":
/*!******************************************************!*\
  !*** ./node_modules/react-bootstrap/esm/Collapse.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _babel_runtime_helpers_esm_objectWithoutPropertiesLoose__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/objectWithoutPropertiesLoose */ "./node_modules/@babel/runtime/helpers/esm/objectWithoutPropertiesLoose.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var dom_helpers_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dom-helpers/css */ "./node_modules/dom-helpers/esm/css.js");
/* harmony import */ var dom_helpers_transitionEnd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! dom-helpers/transitionEnd */ "./node_modules/dom-helpers/esm/transitionEnd.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_transition_group_Transition__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/esm/Transition.js");
/* harmony import */ var _createChainedFunction__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./createChainedFunction */ "./node_modules/react-bootstrap/esm/createChainedFunction.js");
/* harmony import */ var _triggerBrowserReflow__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./triggerBrowserReflow */ "./node_modules/react-bootstrap/esm/triggerBrowserReflow.js");



var _collapseStyles;








var MARGINS = {
  height: ['marginTop', 'marginBottom'],
  width: ['marginLeft', 'marginRight']
};

function getDefaultDimensionValue(dimension, elem) {
  var offset = "offset" + dimension[0].toUpperCase() + dimension.slice(1);
  var value = elem[offset];
  var margins = MARGINS[dimension];
  return value + // @ts-ignore
  parseInt(Object(dom_helpers_css__WEBPACK_IMPORTED_MODULE_3__["default"])(elem, margins[0]), 10) + // @ts-ignore
  parseInt(Object(dom_helpers_css__WEBPACK_IMPORTED_MODULE_3__["default"])(elem, margins[1]), 10);
}

var collapseStyles = (_collapseStyles = {}, _collapseStyles[react_transition_group_Transition__WEBPACK_IMPORTED_MODULE_6__["EXITED"]] = 'collapse', _collapseStyles[react_transition_group_Transition__WEBPACK_IMPORTED_MODULE_6__["EXITING"]] = 'collapsing', _collapseStyles[react_transition_group_Transition__WEBPACK_IMPORTED_MODULE_6__["ENTERING"]] = 'collapsing', _collapseStyles[react_transition_group_Transition__WEBPACK_IMPORTED_MODULE_6__["ENTERED"]] = 'collapse show', _collapseStyles);
var defaultProps = {
  in: false,
  timeout: 300,
  mountOnEnter: false,
  unmountOnExit: false,
  appear: false,
  getDimensionValue: getDefaultDimensionValue
};
var Collapse = react__WEBPACK_IMPORTED_MODULE_5___default.a.forwardRef(function (_ref, ref) {
  var onEnter = _ref.onEnter,
      onEntering = _ref.onEntering,
      onEntered = _ref.onEntered,
      onExit = _ref.onExit,
      onExiting = _ref.onExiting,
      className = _ref.className,
      children = _ref.children,
      _ref$dimension = _ref.dimension,
      dimension = _ref$dimension === void 0 ? 'height' : _ref$dimension,
      _ref$getDimensionValu = _ref.getDimensionValue,
      getDimensionValue = _ref$getDimensionValu === void 0 ? getDefaultDimensionValue : _ref$getDimensionValu,
      props = Object(_babel_runtime_helpers_esm_objectWithoutPropertiesLoose__WEBPACK_IMPORTED_MODULE_1__["default"])(_ref, ["onEnter", "onEntering", "onEntered", "onExit", "onExiting", "className", "children", "dimension", "getDimensionValue"]);

  /* Compute dimension */
  var computedDimension = typeof dimension === 'function' ? dimension() : dimension;
  /* -- Expanding -- */

  var handleEnter = Object(react__WEBPACK_IMPORTED_MODULE_5__["useMemo"])(function () {
    return Object(_createChainedFunction__WEBPACK_IMPORTED_MODULE_7__["default"])(function (elem) {
      elem.style[computedDimension] = '0';
    }, onEnter);
  }, [computedDimension, onEnter]);
  var handleEntering = Object(react__WEBPACK_IMPORTED_MODULE_5__["useMemo"])(function () {
    return Object(_createChainedFunction__WEBPACK_IMPORTED_MODULE_7__["default"])(function (elem) {
      var scroll = "scroll" + computedDimension[0].toUpperCase() + computedDimension.slice(1);
      elem.style[computedDimension] = elem[scroll] + "px";
    }, onEntering);
  }, [computedDimension, onEntering]);
  var handleEntered = Object(react__WEBPACK_IMPORTED_MODULE_5__["useMemo"])(function () {
    return Object(_createChainedFunction__WEBPACK_IMPORTED_MODULE_7__["default"])(function (elem) {
      elem.style[computedDimension] = null;
    }, onEntered);
  }, [computedDimension, onEntered]);
  /* -- Collapsing -- */

  var handleExit = Object(react__WEBPACK_IMPORTED_MODULE_5__["useMemo"])(function () {
    return Object(_createChainedFunction__WEBPACK_IMPORTED_MODULE_7__["default"])(function (elem) {
      elem.style[computedDimension] = getDimensionValue(computedDimension, elem) + "px";
      Object(_triggerBrowserReflow__WEBPACK_IMPORTED_MODULE_8__["default"])(elem);
    }, onExit);
  }, [onExit, getDimensionValue, computedDimension]);
  var handleExiting = Object(react__WEBPACK_IMPORTED_MODULE_5__["useMemo"])(function () {
    return Object(_createChainedFunction__WEBPACK_IMPORTED_MODULE_7__["default"])(function (elem) {
      elem.style[computedDimension] = null;
    }, onExiting);
  }, [computedDimension, onExiting]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_transition_group_Transition__WEBPACK_IMPORTED_MODULE_6__["default"] // @ts-ignore
  , Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    ref: ref,
    addEndListener: dom_helpers_transitionEnd__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, props, {
    "aria-expanded": props.role ? props.in : null,
    onEnter: handleEnter,
    onEntering: handleEntering,
    onEntered: handleEntered,
    onExit: handleExit,
    onExiting: handleExiting
  }), function (state, innerProps) {
    return react__WEBPACK_IMPORTED_MODULE_5___default.a.cloneElement(children, Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, innerProps, {
      className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, children.props.className, collapseStyles[state], computedDimension === 'width' && 'width')
    }));
  });
}); // @ts-ignore

// @ts-ignore
Collapse.defaultProps = defaultProps;
/* harmony default export */ __webpack_exports__["default"] = (Collapse);

/***/ }),

/***/ "./resources/js/src/apps/repl/Repl.js":
/*!********************************************!*\
  !*** ./resources/js/src/apps/repl/Repl.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Repl; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Collapse */ "./node_modules/react-bootstrap/esm/Collapse.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _components_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/q-ui-react/ModalQ */ "./resources/js/src/components/q-ui-react/ModalQ.js");
/* harmony import */ var _components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/devs/react-split-pane-2/SplitPane */ "./resources/js/src/components/devs/react-split-pane-2/SplitPane.js");
/* harmony import */ var _components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/devs/react-split-pane-2/Pane */ "./resources/js/src/components/devs/react-split-pane-2/Pane.js");
/* harmony import */ var _components_antd_tree_AntdTree__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../components/antd/tree/AntdTree */ "./resources/js/src/components/antd/tree/AntdTree.js");
/* harmony import */ var _parts_NavIde__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./parts/NavIde */ "./resources/js/src/apps/repl/parts/NavIde.js");
/* harmony import */ var _components_monaco_react__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../components/monaco-react */ "./resources/js/src/components/monaco-react/index.js");
/* harmony import */ var _components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../components/monaco-react/ide/onDidMount */ "./resources/js/src/components/monaco-react/ide/onDidMount.js");
/* harmony import */ var _data_monaco__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../data/monaco */ "./resources/js/src/data/monaco.js");
/* harmony import */ var _components_browser_Browser__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../components/browser/Browser */ "./resources/js/src/components/browser/Browser.js");
/* harmony import */ var _components_react_new_window_NewWindow__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../components/react-new-window/NewWindow */ "./resources/js/src/components/react-new-window/NewWindow.js");
/* harmony import */ var _components_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../components/react-confirm/util/confirm */ "./resources/js/src/components/react-confirm/util/confirm.js");


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




 // import { Hook, Console, Decode } from 'console-feed';





/** NOTES: MOVE TO q-ui-react */
// import Switch from '../../ui-frameworks/q-react-bootstrap/Switch';



 // parts:


 // @monaco-editor/react | {ControlledEditor} | Editor | DiffEditor | IdeCtrl


 // MONACO_LANGS , setDefineWindow


 // import { permissions } from '../../utils/clipboard';

 // const ACCEPT_EXT = [...MONACO_LANGS.map(v => "."+v.ex), '.sass','.jsx','.tsx'];
// const ACCEPT_MIME = [...(new Set(MONACO_LANGS.map(v => v.type)))]; // Array.from(new Set(MONACO_LANGS.map(v => v.type)))
// const INIT_BABEL_PRESETS = ['es2015','es2015-loose','es2016','es2017','stage-0','stage-1','stage-2','stage-3','react','flow','typescript'];

var COMPILE_MODE = ["bundler", "js", "scss"]; //  | "app", "component", "scss"

var ACCEPT_MIME = ["text/html", "text/css", "text/scss", "text/javascript", "text/plain", "application/json"]; // const ROLLUP_OPTIONS = {
// 	format: "umd", // amd, cjs, es / esm, iife, umd, system
// 	name: 'Qapp',
// 	version: "1",
// 	amd: {
// 		id: "", // define: ''
// 	},
// 	globals: {
// 		"react": "React",
// 		"react-dom": "ReactDOM"
// 	},
// 	// compact: true
// };

var ASIDE_ID = "repl-aside-" + Q.Qid(); // 3

var CONSOLE_ID = "repl-console-" + Q.Qid();
var HEIGHT_1 = "calc(100% - 31px)"; // For height Editor & Browser Component

var Repl = /*#__PURE__*/function (_React$Component) {
  _inherits(Repl, _React$Component);

  var _super = _createSuper(Repl);

  function Repl(props) {
    var _this;

    _classCallCheck(this, Repl);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onDidChangeEditor", function (v, editor) {
      var editorReady = _this.state.editorReady; // paste

      if (!editorReady) {
        _this.setState({
          editorReady: true
        });

        _this.ideMain.current = editor; // v | editor

        Object(_components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_14__["default"])(v, editor); // console.log('onDidChangeEditor editor: ', editor);

        [{
          id: "replDependencies",
          label: "Add dependencies",
          fn: function fn() {
            return _this.setState({
              modal: true
            });
          }
        }, // {id:"replExportToZip", label:"Export to zip", fn: () => console.log('Export to zip')}, 
        {
          id: "replUploadfiles",
          label: "Upload files",
          fn: function fn() {
            console.log('Upload files');

            _this.fileRef.current.click();
          }
        }, {
          id: "replNewFile",
          label: "New file",
          fn: function fn() {
            return console.log('New file');
          }
        } // {id:"replNewDirectory", label:"New directory", fn: () => console.log('New directory')}
        ].forEach(function (v) {
          editor.addAction({
            id: v.id,
            label: v.label,
            // keybindings: [v.KeyMod.CtrlCmd | v.KeyCode.KEY_V],
            contextMenuGroupId: "replMonacoCtxMenu",
            // 9_cutcopypaste | 1_modification
            run: function run(editor) {
              // console.log("Add your custom pasting( code here", editor.getPosition());
              v.fn();
            }
          });
        }); // if(paste){
        // 	editor.addAction({
        // 		id: "replPaste",
        // 		label: "Paste",
        // 		// keybindings: [v.KeyMod.CtrlCmd | v.KeyCode.KEY_V],
        // 		contextMenuGroupId: "9_cutcopypaste",
        // 		run: (editor) => {
        // 			console.log("Add your custom pasting( code here", editor.getPosition());
        // 			// this.onPasteEditor();
        // 		}
        // 	});
        // }
        // OPTION: Add define / custom window object

        /* window.monaco.languages.registerCompletionItemProvider('javascript', {
        	provideCompletionItems: (model, position) => {
        		// find out if we are completing a property in the 'dependencies' object.
        		// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
        		// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
        		// if(!match){
        			// return { suggestions: [] };
        		// }
        		
        		// let word = model.getWordUntilPosition(position);
        		// let range = {
        			// startLineNumber: position.lineNumber,
        			// endLineNumber: position.lineNumber,
        			// startColumn: word.startColumn,
        			// endColumn: word.endColumn
        		// };
        		return {
        			suggestions: setDefineWindow() // range
        		};
        	}
        }); */
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onModal", function () {
      return _this.setState(function (s) {
        return {
          modal: !s.modal
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleConsole", function () {
      _this.setState(function (s) {
        if (s.openConsole) {
          var pane = Q.domQ("#" + CONSOLE_ID);
          var h = pane.style.height;
          var def = "calc(25% - 0.25px)";

          if (h !== def) {
            pane.style.height = def;
          }
        }

        return {
          openConsole: !s.openConsole
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onResetSize", function (e) {
      var pl = e.target.previousElementSibling;
      var w = "calc(50% - 0.5px)";
      if (pl.style.width !== w) pl.style.width = w;
    });

    _defineProperty(_assertThisInitialized(_this), "onTabLeft", function (tab) {
      var aside = Q.domQ('#' + ASIDE_ID);

      if (aside && aside.clientWidth < 150) {
        aside.style.width = "220px";
      }

      _this.setState({
        tabLeft: tab
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onLoadIframe", function (e) {
      var onLoadIframe = _this.props.onLoadIframe; // script

      if (Q.isFunc(onLoadIframe)) onLoadIframe(e); // script
    });

    _defineProperty(_assertThisInitialized(_this), "onClickRun", function () {
      var onRun = _this.props.onRun;
      console.log('onClickRun: ');
      if (Q.isFunc(onRun)) onRun();
    });

    _defineProperty(_assertThisInitialized(_this), "onConfirm", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var options;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              //  
              options = {
                type: "toast",
                title: "Title",
                // toastInfoText: "11 min ago",
                // icon: <i className="fab fa-react mr-2" />, 
                size: "sm",
                bodyClass: "text-center",
                btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next",
                // backdrop: false, 
                modalProps: {
                  // size: "sm",
                  // contentClassName: "HELL", 
                  centered: true,
                  // modalClassName: "modal-right", // up | down | left | right
                  returnFocusAfterClose: false // onOpened: () => {
                  // 	console.log('onOpened');
                  // },
                  // onClosed: () => {
                  // 	console.log('onClosed');
                  // }

                }
              };
              _context.next = 3;
              return Object(_components_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_18__["confirm"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Are your sure?"), options);

            case 3:
              if (!_context.sent) {
                _context.next = 7;
                break;
              }

              console.log('YES');
              _context.next = 8;
              break;

            case 7:
              console.log('NO');

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })));

    _defineProperty(_assertThisInitialized(_this), "renderBtnCollapse", function (state, txt) {
      var s = _this.state[state];
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        block: true,
        blur: true,
        size: "sm",
        kind: _this.state.theme,
        active: s,
        className: "text-left bw-x1 rounded-0 qi qi-chevron-" + (s ? "down" : "right"),
        onClick: function onClick() {
          return _this.setState(_defineProperty({}, state, !s));
        }
      }, txt);
    });

    _this.state = {
      autoCompile: props.autoRun,
      // presets: props.presets,
      editorReady: false,
      lang: props.mode === "bundler" || props.mode === "js" ? "javascript" : "scss",
      code: "",
      // Main editor value
      // Compiler mode:
      // js = JavaScript compiler & bundling
      // scss = Scss to Css 
      modeCompiler: props.mode,
      // OPTIONS IN: COMPILE_MODE
      theme: props.theme,
      openConsole: false,
      modal: false,
      exCssVal: "",
      // Aside Collapse:
      info: true,
      files: true,
      dependencies: true,
      externalSrc: false,
      // Directory tree data:
      modules: [],
      tabLeft: "Project",
      tabRight: props.mode === "bundler" ? "Compiler Result" : "browser",
      // logs: [], // For <Console />
      // paste: null, 
      newWindow: false
    };
    _this.ideMain = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
    _this.fileRef = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
    _this.newWin = null;
    return _this;
  }

  _createClass(Repl, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      console.log('%ccomponentDidMount in Repl', 'color:yellow;');
      var projectSrc = this.props.projectSrc; // presets
      // if(presets.includes('react')){
      // }

      var setData = function setData() {
        if (Q.isStr(projectSrc)) {
          axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(projectSrc).then(function (r) {
            console.log('Xhr r: ', r);
            var data = r.data;

            if (r.status === 200 && data) {
              var children = data.modules.map(function (v, i) {
                return {
                  title: v.name,
                  // REGEX forder / file name /^[\w.-]+$/.test(".")
                  key: v.name + i,
                  icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
                    className: "i-color qi qi-" + Q.fileName(v.name).ext
                  })
                };
              });
              console.log('children: ', children);
              var modules = [_objectSpread(_objectSpread({}, data), {}, {
                key: data.id,
                children: children
              })];
              console.log('modules: ', modules);
              loadJsZip();

              _this2.setState({
                modules: modules,
                libsOk: true
              }); // Hook(window.console, log => {
              // 	this.setState(({ logs }) => ({ logs: [...logs, Decode(log)] }))
              // });

            }
          })["catch"](function (e) {
            console.log('Error xhr: ', e);
          });
        } else {
          loadJsZip();

          _this2.setState({
            libsOk: true
          }); // Hook(window.console, log => {
          // 	this.setState(({ logs }) => ({ logs: [...logs, Decode(log)] }))
          // });

        }
      };

      var loadJsZip = function loadJsZip() {
        Q.getScript({
          src: "/js/libs/jszip/jszip.min.js",
          "data-js": "JSZip"
        }).then(function () {
          if (window.JSZip) {
            _this2.setState({
              zip: true
            });
          } else console.log("%cjszip failed", "color:yellow");
        })["catch"](function (e) {
          return console.log(e);
        });
      };

      if (window.Babel && !window.rollup) {
        var windowDefine = window.define;

        if (window.monaco && windowDefine) {
          window.define = null;
        }

        Q.getScript({
          src: "/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js",
          "data-js": "rollup"
        }).then(function () {
          if (window.rollup) {
            window.define = windowDefine;
            setData();
          }
        })["catch"](function (e) {
          console.log('catch e: ', e);
        });
      } else if (window.Babel && window.rollup) {
        setData();
      } else {
        // Load rollup & babel
        var _windowDefine = window.define;

        if (window.monaco && _windowDefine) {
          window.define = null;
        }

        Q.getScript({
          src: "/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js",
          "data-js": "rollup"
        }).then(function () {
          if (window.rollup) {
            Q.getScript({
              src: "/storage/app_modules/@babel-standalone/babel.min.js",
              "data-js": "Babel"
            }).then(function () {
              if (window.Babel) {
                window.define = _windowDefine;
                setData(); // loadJsZip();
              } else {
                // If failed load use jspm.io cdn
                importShim("https://jspm.dev/@babel/standalone").then(function (m) {
                  window.Babel = m["default"]; // store Babel to window

                  window.define = _windowDefine; // loadJsZip();

                  setData();
                })["catch"](function (e) {
                  return console.log('importShim catch: ', e);
                });
              }
            })["catch"](function (e) {
              console.log('catch e: ', e);
            });
          }
        })["catch"](function (e) {
          console.log('catch e: ', e);
        });
      } // if(navigator.clipboard.readText){
      // 	console.log('navigator.clipboard.readText support');
      // 	permissions().then(r => {
      // 		console.log("permissions navigator query r: ", r);
      // 		this.setState({ paste: r.state });
      // 	}).catch(e => {
      // 		console.log("Errror navigator query e: ", e);
      // 	});
      // }else{
      // 	console.log('Clipboard readText function not support');
      // }

    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          className = _this$props.className,
          aside = _this$props.aside,
          stylesheet = _this$props.stylesheet,
          onCloseTab = _this$props.onCloseTab; // presets, 

      var _this$state = this.state,
          lang = _this$state.lang,
          code = _this$state.code,
          modeCompiler = _this$state.modeCompiler,
          autoCompile = _this$state.autoCompile,
          theme = _this$state.theme,
          libsOk = _this$state.libsOk,
          editorReady = _this$state.editorReady,
          openConsole = _this$state.openConsole,
          modal = _this$state.modal,
          exCssVal = _this$state.exCssVal,
          info = _this$state.info,
          files = _this$state.files,
          dependencies = _this$state.dependencies,
          externalSrc = _this$state.externalSrc,
          modules = _this$state.modules,
          tabLeft = _this$state.tabLeft,
          tabRight = _this$state.tabRight,
          newWindow = _this$state.newWindow;
      var textWhite = {
        "text-white": theme === "dark"
      };
      var tipWhite = {
        "tip-white": theme === "dark"
      };
      var iconInfoClass = Q.Cx("float-right qi qi-info q-mr chelp tip tipTR", tipWhite);
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        dir: "row",
        className: Q.Cx("repl bg-ide-" + theme, textWhite, className)
      }, libsOk ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "btn-group-vertical justify-content-start zi-4 bg-" + (theme === "dark" ? "secondary" : "light")
      }, [{
        t: "Project",
        i: "box"
      }, {
        t: "Settings",
        i: "cog"
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
          key: v.t,
          blur: true,
          kind: theme,
          onClick: function onClick() {
            return _this3.onTabLeft(v.t);
          },
          active: tabLeft === v.t,
          className: Q.Cx("flexno border-0 rounded-0 tip tipR qi qi-" + v.i, tipWhite),
          "aria-label": v.t
        });
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_9__["default"], {
        theme: theme,
        split: "vertical",
        resizeClass: "l7"
      }, aside &&
      /*#__PURE__*/
      // (aside && editorReady) NOTE: Don't change aside props with state in use component
      react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_10__["default"], {
        id: ASIDE_ID,
        initialSize: "220px" // 270 | 200
        ,
        minSize: "0px" // 150px | 170
        ,
        maxSize: "400px",
        className: "flex-column ovyauto q-scroll pb-3 repl-aside bg-ide-" + theme
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
        timeout: 1,
        "in": tabLeft === "Project"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        dir: "column",
        className: "border-top-next"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, this.renderBtnCollapse("info", "Info"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
        timeout: 90,
        "in": info
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "p-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "pre-wrap small"
      }, "Project name: \nProgrammeria\n\nCreate by: \nHusein"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, this.renderBtnCollapse("files", "Files"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
        timeout: 90,
        "in": files
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "btn-group btn-group-xs"
      }, [{
        t: "Export to zip",
        i: "zip"
      }, {
        t: "Upload files",
        i: "upload",
        el: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
          ref: this.fileRef,
          type: "file",
          accept: ACCEPT_MIME,
          hidden: true
        })
      }, {
        t: "New file",
        i: "file"
      }, {
        t: "New directory",
        i: "folder"
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
          key: v.t,
          As: v.el ? "label" : "button",
          kind: theme,
          className: Q.Cx("tip tipTL q-fw qi qi-" + v.i, {
            "btnFile": v.el
          }, tipWhite),
          "aria-label": v.t
        }, v.el);
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_antd_tree_AntdTree__WEBPACK_IMPORTED_MODULE_11__["default"], {
        data: modules // autoExpandParent 
        // defaultExpandParent 
        ,
        defaultExpandedKeys: ['main.js'] // 
        ,
        className: Q.Cx("repl-directory bg-" + theme + " tree-" + theme, textWhite)
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, this.renderBtnCollapse("dependencies", "Dependencies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
        timeout: 90,
        "in": dependencies
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "input-group input-group-xs"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        className: "form-control border-0 rounded-0",
        placeholder: "Search dependencies",
        type: "text"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "input-group-append"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        As: "div",
        kind: theme,
        className: "border-0 qi qi-search"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        onClick: this.onModal,
        kind: theme,
        className: Q.Cx("qi qi-window-maximize border-0 rounded-0 tip tipTR", tipWhite),
        "aria-label": "Package manager"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("nav", {
        className: "nav flex-column small"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        className: "nav-link p-0 border-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        className: "form-control form-control-sm bg-transparent border-0 rounded-0 text-" + theme,
        type: "text",
        readOnly: true,
        value: "react"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "btn-group btn-group-sm"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        kind: theme,
        className: "qi qi-close border-0 rounded-0"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "nav-link",
        href: "/"
      }, "Link"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, this.renderBtnCollapse("externalSrc", "External src"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
        mountOnEnter: true,
        timeout: 90,
        "in": externalSrc
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("fieldset", {
        disabled: false
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "input-group input-group-sm mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "input-group-prepend"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "input-group-text bg-transparent rounded-0 border-left-0 px-1 q-fw i-color qi qi-css"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        className: "form-control form-control-sm rounded-0 border-right-0",
        placeholder: "CSS",
        value: exCssVal,
        onChange: function onChange(e) {
          return _this3.setState({
            exCssVal: e.target.value
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "input-group input-group-sm"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "input-group-prepend"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "input-group-text bg-transparent rounded-0 border-left-0 px-1 q-fw i-color qi qi-js"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        className: "form-control form-control-sm rounded-0 border-right-0",
        placeholder: "JavaScript" // value={exJsVal} 
        // onChange={e => this.setState({ exJsVal: e.target.value })} 

      }))))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
        mountOnEnter: true,
        timeout: 1,
        "in": tabLeft === "Settings"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "p-2 mt-2-next"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", {
        className: "hr-h"
      }, "Settings"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("fieldset", {
        className: "fset small",
        disabled: false
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("legend", null, "Editor"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "d-block"
      }, "Theme ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
        className: iconInfoClass,
        "aria-label": "Info"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        className: "custom-select custom-select-sm mt-1 text-capitalize",
        value: theme,
        onChange: function onChange(e) {
          return _this3.setState({
            theme: e.target.value
          });
        }
      }, _data_monaco__WEBPACK_IMPORTED_MODULE_15__["MONACO_THEMES"].map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
          key: i,
          value: v
        }, v);
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("fieldset", {
        className: "fset small",
        disabled: false
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("legend", null, "Compiler"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "d-block"
      }, "Mode ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
        className: iconInfoClass,
        "aria-label": "Info"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        className: "custom-select custom-select-sm mt-1 text-capitalize" // disabled={!online} 
        ,
        value: modeCompiler,
        onChange: function onChange(e) {
          return _this3.setState({
            modeCompiler: e.target.value
          });
        }
      }, COMPILE_MODE.map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
          key: v,
          value: v
        }, v);
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "custom-control custom-switch"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        onChange: function onChange(e) {
          return _this3.setState({
            autoCompile: e.target.checked
          });
        },
        checked: autoCompile,
        className: "custom-control-input",
        type: "checkbox"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "custom-control-label"
      }, "Auto Run")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_9__["default"], {
        theme: theme,
        onDoubleClick: this.onResetSize
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_10__["default"], {
        initialSize: "50%",
        minSize: "9%",
        maxSize: "90%",
        className: "ovhide bg-ide-" + theme
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "position-absolute position-full w-100 h-100"
      }, editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_parts_NavIde__WEBPACK_IMPORTED_MODULE_12__["NavIde"], {
        theme: theme // tabs={tabs} 
        // active={tabActive} 
        // onClickTab={onClickTab} 
        ,
        onCloseTab: onCloseTab // onOpenSet={() => {
        // 	this.setState({modal: true})
        // }} 

      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm",
        kind: theme,
        className: Q.Cx("tip tipL far fa-play", tipWhite) // onClick={() => this.onCompile(modeCompiler, code)} 
        ,
        onClick: this.onClickRun,
        disabled: autoCompile // autoCompile || !online
        ,
        "aria-label": "Run Compiler"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_monaco_react__WEBPACK_IMPORTED_MODULE_13__["ControlledEditor"], {
        loading: false,
        className: "monacoFixedCtxMenu replEditorMain",
        height: HEIGHT_1,
        editorDidMount: this.onDidChangeEditor,
        value: code,
        onChange: function onChange(e, code) {
          return _this3.setState({
            code: code
          });
        } // onChangeCode(e, code) | this.setState({code})
        ,
        language: lang,
        theme: theme,
        options: _data_monaco__WEBPACK_IMPORTED_MODULE_15__["MONACO_OPTIONS"]
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_9__["default"], {
        theme: theme,
        split: "horizontal",
        allowResize: openConsole
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_10__["default"], {
        className: "flex-column"
      }, editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("nav", {
        className: "btn-group btn-group-sm"
      }, [{
        s: "browser",
        i: "browser"
      }, {
        s: "Compiler Result",
        i: "code"
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
          key: v.s,
          blur: true,
          className: "py-1 border-transparent rounded-0 qi qi-" + v.i,
          onClick: function onClick() {
            return _this3.setState({
              tabRight: v.s
            });
          },
          active: tabRight === v.s,
          kind: theme
        }, v.s);
      })), [{
        s: "Compiler Result",
        el: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
          wrap: true,
          align: "center",
          className: "flexno small pl-2 ml-1-next"
        }, "Format :", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "btn-group btn-group-sm ml-auto"
        }, [{
          m: "amd",
          "long": "Asynchronous Module Definition"
        }, {
          m: "cjs",
          "long": "CommonJS"
        }, {
          m: "esm",
          "long": "Es Module"
        }, {
          m: "iife",
          "long": "Immediately Invoked Function Expression"
        }, {
          m: "umd",
          "long": "Universal Module Definition"
        }, {
          m: "system",
          "long": "SystemJS"
        }].map(function (v, i) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
            key: i,
            As: "b",
            outline: true,
            kind: theme === "dark" ? "light" : "dark",
            className: Q.Cx("rounded-0 bw-x1 tip tipB", tipWhite),
            "aria-label": v["long"] // active={options.format === v} 
            // onClick={() => this.onClickBundleFormat(v)} 

          }, v.m);
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"], {
          bsPrefix: "btn-group"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Toggle, {
          size: "sm",
          variant: "outline-" + (theme === "dark" ? "light" : "dark"),
          className: "rounded-0"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Menu, {
          className: "fs-14"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Item, {
          as: "button",
          type: "button"
        }, "Copy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Item, {
          as: "button",
          type: "button"
        }, "Download"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_monaco_react__WEBPACK_IMPORTED_MODULE_13__["ControlledEditor"], {
          loading: false,
          height: "calc(100% - 70px)" // {HEIGHT_1} 
          ,
          className: "position-absolute b0 l0 r0" // value={output.length > 0 ? output[0].output[0].code : ""} 
          // onChange={(e, value) => this.setState({code: value})} 
          // language="javascript" 
          ,
          theme: theme,
          options: _data_monaco__WEBPACK_IMPORTED_MODULE_15__["MONACO_OPTIONS"]
        }))
      }, {
        s: "browser",
        el: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_browser_Browser__WEBPACK_IMPORTED_MODULE_16__["default"], {
          style: {
            height: HEIGHT_1
          },
          className: "position-absolute b0 l0 r0" // position-full
          // type={type} // DEFAULT = "native" | "component" 
          // addStyleParent={addStyleParent} 
          ,
          theme: theme,
          frameProps: {
            name: "iframeBrowser" // className: "repl-web-programming",
            // title: "Test title",

          } // frameRefNative={frameRefNative} 
          // html={html} 
          ,
          stylesheet: stylesheet // script={script} 
          ,
          append: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"], {
            className: "input-group-append"
          }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Toggle, {
            variant: theme,
            bsPrefix: "rounded-0 qi qi-ellipsis-v tip tipBR",
            "aria-label": "More"
          }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Menu, {
            className: "fs-14"
          }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Item, {
            onClick: function onClick() {
              return window.open("/", "_blank");
            },
            as: "button",
            type: "button"
          }, "Open in new tab"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Item, {
            as: "button",
            type: "button",
            onClick: function onClick() {
              if (newWindow) {
                if (_this3.newWin) _this3.newWin.focus();
              } else {
                _this3.setState({
                  newWindow: true
                });
              }
            }
          }, "Open in new window"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_4__["default"].Item, {
            as: "button",
            type: "button",
            onClick: this.onToggleConsole
          }, openConsole ? "Close" : "Open", " console"))),
          onLoad: this.onLoadIframe
        })
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Collapse__WEBPACK_IMPORTED_MODULE_3__["default"], {
          key: v.s,
          mountOnEnter: true,
          timeout: 1,
          "in": tabRight === v.s
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, v.el));
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_10__["default"], {
        initialSize: "25%",
        minSize: "20px",
        maxSize: "85%" // size={openConsole ? "25%":"0"} 
        ,
        id: CONSOLE_ID,
        className: "flex-column paneConsole"
      }, openConsole && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        dir: "column",
        className: Q.Cx("h-100 ovhide", {
          "bg-dark text-white": theme === "dark"
        }) // className={Q.Cx("h-100 ovhide", { "bg-white": theme !== "dark" })} // Q.Cx("h-100 ovhide", {'bg-white':tm === 'light'}) | "h-100 ovhide bg-" + tm
        // style={{ backgroundColor: theme === "dark" ? "#242424":null }} 

      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        className: "flexno border-bottom"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        kind: theme,
        size: "xs",
        className: "border-0 rounded-0 qi qi-ban",
        title: "Clear console" // onClick={() => {
        // 	if(logs.length > 0){
        // 		this.setState({ logs: [] });
        // 		console.clear();
        // 		// console.log("");
        // 	}
        // }} 
        // disabled={logs.length < 1} 

      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        onClick: this.onToggleConsole,
        kind: theme,
        size: "xs",
        className: "border-0 rounded-0 ml-auto qi qi-close xx",
        title: "Close"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        dir: "column",
        grow: "1",
        className: "repl-logs"
      })))))), editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_8__["default"], {
        scrollable: true,
        open: modal,
        toggle: this.onModal,
        modalClassName: "modalRepl",
        contentClassName: Q.Cx("bg-" + theme, textWhite) // size={modalTitle === 'Settings' ? 'lg':'xl'} // "xl" 
        ,
        size: "lg",
        position: "modal-down" // up 
        ,
        headProps: {
          tag: "h6"
        },
        title: "Package Manager",
        bodyClass: "p-0",
        body: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, Array.from({
          length: 50
        }).map(function (v, i) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
            key: i
          }, "DUMMY - ", i + 1);
        })) // foot={
        // 	<>
        // 		<Btn size="sm" onClick={this.onModal}>Close</Btn>
        // 	</>
        // }

      }), newWindow && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_react_new_window_NewWindow__WEBPACK_IMPORTED_MODULE_17__["default"] // features={null} 
      , {
        url: "/" // url={newWindow} // "/admin/devs/post-edit" 
        // center="parent" // screen 
        // name="_blank" 
        ,
        title: "Programmeria",
        onBlock: function onBlock() {
          console.log('onBlock: ');
        },
        onUnload: function onUnload() {
          console.log('onUnload: ');

          if (newWindow) {
            console.log('onUnload setState: ');

            _this3.setState({
              newWindow: false
            });
          }
        },
        onOpen: function onOpen(win) {
          console.log('onOpen win: ', win);
          _this3.newWin = win;
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Hi \uD83D\uDC4B")))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_5__["default"], {
        className: "position-absolute bg-" + theme,
        bg: "/icon/android-icon-36x36.png"
      }));
    }
  }]);

  return Repl;
}(react__WEBPACK_IMPORTED_MODULE_1___default.a.Component);


Repl.defaultProps = {
  presets: [],
  // babelPreset
  mode: "bundler",
  // Editor:
  theme: "dark",
  // dark | light
  // 
  autoRun: false,
  onLoadIframe: Q.noop,
  onChangeCode: Q.noop
};
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/antd/tree/AntdTree.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/antd/tree/AntdTree.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AntdTree; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_tree_style_index_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/tree/style/index.css */ "./node_modules/antd/lib/tree/style/index.css");
/* harmony import */ var antd_lib_tree_style_index_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_tree_style_index_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import 'antd/dist/antd.css';


 // import Btn from '../../components/q-ui-react/Btn';
// const { DirectoryTree } = Tree;

function AntdTree(_ref) {
  var _ref$data = _ref.data,
      data = _ref$data === void 0 ? [] : _ref$data,
      className = _ref.className,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? Q.noop : _ref$onClick,
      etc = _objectWithoutProperties(_ref, ["data", "className", "onClick"]);

  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in AntdTree','color:yellow;');
  // }, []);
  // NOT WORK???
  // const ClickTree = (selectedKeys, info) => { // SelectTree
  // 	console.log('ClickTree selectedKeys: ', selectedKeys);
  // 	console.log('ClickTree info: ', info);
  // }
  var Expand = function Expand(keys, data) {
    console.log('Expand keys: ', keys);
    console.log('Expand data: ', data);
    onClick(data);
  };

  var CtxMenu = function CtxMenu(data) {
    console.log('CtxMenu data: ', data);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Tree"].DirectoryTree, _extends({}, etc, {
    className: Q.Cx("no-caret-ico", className) // showLine 
    // multiple 
    ,
    selectable: false,
    draggable: true // blockNode 
    // switcherIcon={undefined} // <DownOutlined />
    // defaultExpandedKeys={} // ['0-0-0'] 
    // icon={(p) => <i className="fal fa-cog">{console.log(p)}</i>} 
    // onSelect={ClickTree} // SelectTree
    ,
    onExpand: Expand,
    onRightClick: CtxMenu,
    treeData: data
  }));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/react-new-window/NewWindow.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/components/react-new-window/NewWindow.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NewWindow; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_css_copyStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/css/copyStyles */ "./resources/js/src/utils/css/copyStyles.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


 // import P from 'prop-types';


/** Convert features props to window features format (name=value,other=value).
 * @param {Object} obj
 * @return {String} */

function toWindowFeatures(obj) {
  if (Q.isObj(obj)) {
    return Object.keys(obj).reduce(function (features, name) {
      var value = obj[name];

      if (Q.isBool(value)) {
        // typeof value === 'boolean'
        features.push(name + "=" + (value ? "yes" : "no")); // `${name}=${value ? "yes" : "no"}`
      } else {
        features.push(name + "=" + value); // `${name}=${value}`
      }

      return features;
    }, []).join(",");
  }

  return null;
}

var NewWindow = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(NewWindow, _React$PureComponent);

  var _super = _createSuper(NewWindow);

  function NewWindow(props) {
    var _this;

    _classCallCheck(this, NewWindow);

    _this = _super.call(this, props);
    _this.div = Q.makeEl("div"); // document.createElement('div')

    _this.win = null;
    _this.windowCheckerInterval = null;
    _this.released = false;
    _this.state = {
      mounted: false
    };
    return _this;
  }

  _createClass(NewWindow, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.openChild();
      this.setState({
        mounted: true
      });
    }
    /** Close the opened window (if any) when NewWindow will unmount */

  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.win) this.win.close();
    }
    /** Create the new window when NewWindow component mount. */

  }, {
    key: "openChild",
    value: function openChild() {
      var _this2 = this;

      var _this$props = this.props,
          url = _this$props.url,
          title = _this$props.title,
          name = _this$props.name,
          features = _this$props.features,
          onBlock = _this$props.onBlock,
          onOpen = _this$props.onOpen,
          center = _this$props.center;

      if (features) {
        // Prepare position of the new window to be centered against the 'parent' window or 'screen'.
        // typeof center === 'string' && (features.width === undefined || features.height === undefined)
        if (Q.isStr(center) && (features.width === undefined || features.height === undefined)) {
          console.warn("width and height window features must be present when a center prop is provided");
        } else if (center === "parent") {
          features.left = window.top.outerWidth / 2 + window.top.screenX - features.width / 2;
          features.top = window.top.outerHeight / 2 + window.top.screenY - features.height / 2;
        } else if (center === 'screen') {
          var screenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screen.left;
          var screenTop = window.screenTop !== undefined ? window.screenTop : window.screen.top;
          var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : window.screen.width;
          var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : window.screen.height;
          features.left = width / 2 - features.width / 2 + screenLeft;
          features.top = height / 2 - features.height / 2 + screenTop;
        }
      } // Open a new window.


      this.win = window.open(url, name, toWindowFeatures(features)); // When a new window use content from a cross-origin there's no way we can attach event
      // to it. Therefore, we need to detect in a interval when the new window was destroyed
      // or was closed.

      this.windowCheckerInterval = setInterval(function () {
        if (!_this2.win || _this2.win.closed) {
          _this2.release();
        }
      }, 50); // Check if the new window was succesfully opened.

      if (this.win) {
        this.win.document.title = title;
        this.win.document.body.appendChild(this.div); // If specified, copy styles from parent window's document.

        if (this.props.copyCss) {
          setTimeout(function () {
            return Object(_utils_css_copyStyles__WEBPACK_IMPORTED_MODULE_2__["copyStyles"])(document, _this2.win.document);
          }, 1); // 0
        }

        if (Q.isFunc(onOpen)) {
          // typeof onOpen === 'function'
          onOpen(this.win);
        } // Release anything bound to this component before the new window unload.


        this.win.addEventListener('beforeunload', function () {
          return _this2.release();
        });
      } else {
        // Handle error on opening of new window.
        if (Q.isFunc(onBlock)) {
          // typeof onBlock === 'function'
          onBlock(null);
        } else {
          console.warn('A new window could not be opened. Maybe it was blocked.');
        }
      }
    }
    /** Release the new window and anything that was bound to it */

  }, {
    key: "release",
    value: function release() {
      // This method can be called once.
      if (this.released) return;
      this.released = true; // Remove checker interval.

      clearInterval(this.windowCheckerInterval); // Call any function bound to the `onUnload` prop.

      var onUnload = this.props.onUnload;
      if (Q.isFunc(onUnload)) onUnload(null); // typeof onUnload === 'function'
    }
  }, {
    key: "render",
    value: function render() {
      if (!this.state.mounted) return null;
      return /*#__PURE__*/react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.createPortal(this.props.children, this.div);
    }
  }]);

  return NewWindow;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.PureComponent);


NewWindow.defaultProps = {
  url: "",
  name: "",
  title: "",
  features: {
    width: "600px",
    height: "640px"
  },
  // onBlock: null, 
  // onOpen: null, 
  // onUnload: null, 
  center: "parent",
  // parent | screen
  copyCss: true
}; // NewWindow.propTypes = {
//   children: P.node,
//   url: P.string,
//   // name: P.string,
//   title: P.string,
//   // features: P.object,
//   onUnload: P.func,
//   onBlock: P.func,
//   onOpen: P.func,
//   center: P.oneOf(['parent','screen']),
//   copyCss: P.bool // copyStyles
// };

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/ReplPage.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/ReplPage.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ReplPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _apps_repl_Repl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../apps/repl/Repl */ "./resources/js/src/apps/repl/Repl.js");
/* harmony import */ var _utils_string__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../utils/string */ "./resources/js/src/utils/string.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }



 // INIT TABS CODE:

/* const TABS = [
	// {id:"HTML", lang:"html", name:"HTML", code:"<!doctype html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\t\t<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n\t\t<title>Programmeria</title>\n\t\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" crossorigin=\"anonymous\">\n\t</head>\n\t<body>\n\t\t<!-- Button trigger modal -->\n\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n\t\t\tLaunch demo modal\n\t\t</button>\n\n\t\t<!-- Modal -->\n\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n\t\t\t<div class=\"modal-dialog\">\n\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\tModal content\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<script src=\"https://unpkg.com/jquery@3.5.1/dist/jquery.min.js\" crossorigin=\"anonymous\"></script>\n\t\t<script src=\"https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js\" crossorigin=\"anonymous\"></script>\n\t</body>\n<html>"},
	{id:"HTML", lang:"html", name:"HTML", code:"<!doctype html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\t\t<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n\t\t<title>Programmeria</title>\n\t\t\n\t</head>\n\t<body>\n\t\t<!-- Button trigger modal -->\n\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n\t\t\tLaunch demo modal\n\t\t</button>\n\n\t\t<!-- Modal -->\n\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n\t\t\t<div class=\"modal-dialog\">\n\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\tModal content\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</body>\n<html>"},
	{id:"CSS", lang:"css", name:"CSS", code:"body{\n\tbackground-color: lightblue;\n}"},
	// {id:"JS", lang:"javascript", name:"JS", code:"console.log('This JS man', 6+1);\n$('.btn').trigger('click');"},
]; */

var ReplPage = /*#__PURE__*/function (_Component) {
  _inherits(ReplPage, _Component);

  var _super = _createSuper(ReplPage);

  function ReplPage(props) {
    var _this;

    _classCallCheck(this, ReplPage);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "setTab", function (v, i) {
      _this.setState({
        tabActive: i,
        lang: v.lang,
        code: v.code
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onTab", function (v, i, e) {
      var tabActive = _this.state.tabActive; // tabs, code, autoRun

      if (i === tabActive) return; // console.log('onClickTab v: ',v);

      var _this$state = _this.state,
          tabs = _this$state.tabs,
          code = _this$state.code;
      var activeTab = tabs[tabActive]; // console.log('onClickTab activeTab.code: ', activeTab.code);

      if (activeTab.code !== code) {
        _this.setState(function (s) {
          var tabs = s.tabs.map(function (t) {
            if (activeTab.id === t.id) {
              return _objectSpread(_objectSpread({}, t), {}, {
                code: code
              });
            }

            return t;
          });
          var htmlCode = s.autoRun ? _this.addScriptInline(tabs) : s.htmlCode;
          return {
            tabs: tabs,
            htmlCode: htmlCode
          };
        }, function () {
          _this.setTab(v, i); // NOT FIX: Add css code to tag style in iframe with click Reload Button
          // if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();

        });
      } else {
        _this.setTab(v, i);
      }
    });

    _this.state = {// tabs: [...TABS], 
      // tabActive: 0, 
      // lang: "html", // HTML Mode
      // code: TABS[0].code, // HTML Mode & Code
      // htmlCode: "", 
      // autoRun: false, 
      // script: [
      // "https://unpkg.com/jquery@3.5.1/dist/jquery.min.js", 
      // "https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"
      // ]
    }; // this.iframe = React.createRef();

    return _this;
  }

  _createClass(ReplPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {// console.log('%ccomponentDidMount in ReplPage','color:yellow;');
      // this.setState(s => ({
      // htmlCode: this.addScriptInline(s.tabs)
      // }));
    }
  }, {
    key: "addScriptInline",
    value: function addScriptInline(tabs) {
      // const {tabs} = this.state;// script
      // let blob = new Blob([tabs[1].code], {type: 'text/css'});
      // let objUrl = window.URL.createObjectURL(blob);
      // console.log('blob: ', blob);
      // console.log('objUrl: ', objUrl);
      // const addScripts = script.map(v => `<script src="${v}" crossorigin="anonymous"></script>`);
      // HTML
      var setHtml = tabs[0].code; // DEV OPTION: Insert history.js for manipulate iframe history API
      // setHtml = strInsertBefore(setHtml, "</head>", historyApi);
      // CSS

      if (tabs[1] && tabs[1].code.length) setHtml = Object(_utils_string__WEBPACK_IMPORTED_MODULE_3__["strInsertBefore"])(setHtml, "</head>", "<style>".concat(tabs[1].code, "</style>")); // Minify Html & Css

      setHtml = setHtml.replace(/\n|\t/gm, ''); // JS

      if (tabs[2] && tabs[2].code.length) setHtml = Object(_utils_string__WEBPACK_IMPORTED_MODULE_3__["strInsertBefore"])(setHtml, "</body>", "<script>".concat(tabs[2].code, "</script>")); // addScripts.join('\n\t') + '\n'
      // console.log('setHtml: ', setHtml.replace(/\n|\t/gm,''));

      return setHtml; // setHtml.replace(/\n|\t/gm,'');// setHtml
    }
  }, {
    key: "render",
    value: function render() {
      // code, tabs, tabActive, autoRun, htmlCode, 
      // const { lang } = this.state;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "w-100 mh-full-navmain position-relative"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
        title: "Repl"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_repl_Repl__WEBPACK_IMPORTED_MODULE_2__["default"] // theme="light" 
      , {
        aside: true // autoRun={false}  
        ,
        projectSrc: "/DUMMY/rollup/08/08.json",
        className: "w-100 h-100 position-absolute position-full" // theme="light" // dark | light
        // presets={['esm']} 
        // type="component" // DEFAULT = native | component 

        /* frameProps={{
        	name: "iframeBrowser"
        	// className: "repl-web-programming",
        	// title: "Test title",
        }}  */
        // frameRefNative={this.iframe} 
        // addStyleParent={false} // 
        // html={htmlCode} // tabs[0].code | html | lang === "html" ? code : "" | `<h1>OKEH</h1>`
        // stylesheet={[
        // "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
        // ]}

        /* script={[
        	// "https://code.jquery.com/jquery-3.5.1.min.js"
        	"https://unpkg.com/jquery@3.5.1/dist/jquery.min.js", 
        	"https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"
        ]}  */
        // onLoadIframe={(e, script) => {
        // 	let history = createBrowserHistory({
        // 		window: e.target.contentWindow // iframe.contentWindow
        // 	});
        // 	console.log('onLoadIframe e.target.contentWindow: ', e.target.contentWindow);
        // 	console.log('onLoadIframe history: ', history);
        // 	// this.onLoadIframe(e, script, tabs);
        // 	// this.onLoadIframe(e, 'style', tabs[1].code);// ADD CSS
        // 	// this.onLoadIframe(e, 'script', tabs[2].code);// ADD JS
        // }}
        // lang={lang} // "html" 
        // code={code} 

        /* onChangeCode={(e, code) => {
        	this.setState({code}, () => {
        		// const { autoRun } = this.state;
        		// console.log('onChangeCode autoRun: ', autoRun);
        		// if(autoRun){
        		// 	const activeTab = tabs[tabActive];
        		// 	// // NOT FIX: Add css code to tag style in iframe with click Reload Button
        		// 	// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();
        				// 	// this.addScriptInline();
        				// 	if(activeTab.code !== code){
        		// 		this.setState(s => {
        		// 			const tabs = s.tabs.map(t => { 
        		// 				if(activeTab.id === t.id){
        		// 					return {...t, code};
        		// 				}
        		// 				return t;
        		// 			});
        		// 			const htmlCode = s.autoRun ? this.addScriptInline(tabs) : s.htmlCode;
        		// 			return {tabs, htmlCode};
        		// 		}, () => {
        		// 			// this.setTab(v, i);
        		// 			// NOT FIX: Add css code to tag style in iframe with click Reload Button
        		// 			// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();
        		// 		});
        		// 	}
        		// 	// else{
        		// 	// 	this.setTab(v, i);
        		// 	// }
        		// }
        	})
        }}  */
        // onChangeLang={e => this.setState({lang: e.target.value})} 
        // onChangeAutoRun={e => this.setState({autoRun: e.target.checked})} 

        /* onRun={e => { // onClickRun
        	if(!autoRun){
        		// NOT FIX:
        		// this.setState(s => ({
        			// htmlCode: this.addScriptInline(s.tabs)
        		// }));
        		
        		this.setState(s => {
        			const tabs = s.tabs.map(t => { 
        				if(s.tabs[tabActive].id === t.id){
        					return {...t, code};
        				}
        				return t;
        			});
        			// const htmlCode = autoRun ?  : s.htmlCode;
        			return {tabs, htmlCode: this.addScriptInline(tabs)};
        		});
        	}
        }} */
        // tabs={tabs} 
        // tabActive={tabActive} 
        // onClickTab={this.onTab} 
        // onCloseTab={(v, i, e) => {
        // console.log('onCloseTab v: ',v);
        // console.log('onCloseTab i: ',i);
        // }} 

      }));
    }
  }]);

  return ReplPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);
/*
<React.Fragment></React.Fragment>
*/




/***/ })

}]);
//# sourceMappingURL=ReplPage.chunk.js.map