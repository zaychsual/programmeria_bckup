(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["DropsPage"],{

/***/ "./resources/js/src/components/drops/Drops.js":
/*!****************************************************!*\
  !*** ./resources/js/src/components/drops/Drops.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Drops; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Details__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/q-ui-react/Details */ "./resources/js/src/components/q-ui-react/Details.js");
/* harmony import */ var _utils_file_traverseDirectory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/file/traverseDirectory */ "./resources/js/src/utils/file/traverseDirectory.js");
/* harmony import */ var _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/file/fileRead */ "./resources/js/src/utils/file/fileRead.js");


function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useDropzone } from 'react-dropzone';




 // , 
// import { MONACO_LANGS } from '../../data/monaco';
// const ACCEPT_EXT = [...MONACO_LANGS.map(v => "."+v.ex), '.sass','.jsx','.tsx'];
// console.log('ACCEPT_EXT: ', ACCEPT_EXT);

function getExt(fname, bool) {
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

var EXT_FILES = ["local", "example", "lock"]; // File name like .env.local, yarn.lock, .env.example
// directory="" webkitdirectory=""

var directoryAttr = _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__["SUPPORT_DIR"] && {
  directory: "",
  webkitdirectory: ""
};
function Drops() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      data = _useState2[0],
      setData = _useState2[1]; // []
  // useEffect(() => {
  // 	console.log('%cuseEffect in Drops','color:yellow;');
  // }, []);


  var inputID = "inputID-" + Q.Qid(); // Change & Drop

  var onChangeFile = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      var _datas;

      var data, dir, items, datas, i, item, res, _i2, file, fileItem, fullPath;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              data = e.dataTransfer ? e.dataTransfer : e.target; // e.dataTransfer.items | .files

              _context.next = 3;
              return Object(_utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__["isDir"])(data.items);

            case 3:
              dir = _context.sent;
              // console.log('isDir: ', dir);
              items = data.items;
              datas = [];

              if (!dir) {
                _context.next = 20;
                break;
              }

              i = 0;

            case 8:
              if (!(i < items.length)) {
                _context.next = 18;
                break;
              }

              item = items[i].webkitGetAsEntry();

              if (!item) {
                _context.next = 15;
                break;
              }

              _context.next = 13;
              return Object(_utils_file_traverseDirectory__WEBPACK_IMPORTED_MODULE_4__["traverseDirectory"])(item);

            case 13:
              res = _context.sent;
              datas = res[0];

            case 15:
              i++;
              _context.next = 8;
              break;

            case 18:
              _context.next = 22;
              break;

            case 20:
              items = data.files;

              for (_i2 = 0; _i2 < items.length; _i2++) {
                file = items[_i2];
                fileItem = {
                  file: file,
                  isDirectory: false,
                  // OPTION
                  isFile: true,
                  type: "file",
                  name: file.name,
                  path: "/" + file.name
                }; // OPTION

                fullPath = file.webkitRelativePath;
                if (fullPath) fileItem.fullPath = "/" + fullPath; // Object.defineProperty(fileItem, "file", {
                // 	value: fileItem
                // });
                // console.log('files file: ', file);
                // console.log('files fileItem: ', fileItem);

                datas.push(fileItem);
              }

            case 22:
              if ((_datas = datas) === null || _datas === void 0 ? void 0 : _datas.length) {
                setData(datas);
                console.log('Drop datas: ', datas);
              }

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function onChangeFile(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var renderTree = function renderTree(node, key) {
    if (node == null) return;
    var lng = node.length;
    var tree = [];
    var pathEnd, me, ext, parseExt;

    for (var i = 0; i < lng; i++) {
      me = node[i];

      if (me.type === "file" || me.isFile) {
        ext = getExt(me.name);
        parseExt = EXT_FILES.includes(ext) ? "file" : getExt(me.name, 1) ? ext : "file";
        tree = [].concat(_toConsumableArray(tree), [/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Details__WEBPACK_IMPORTED_MODULE_3__["Summary"], {
          key: i,
          As: "div",
          role: "treeitem",
          className: "text-truncate tree-item tree-file q-fw i-color qi qi-" + parseExt // (getExt(me.name, 1) ? parseExt : "file")
          ,
          title: me.name
        }, " ", me.name)]);
      } else {
        pathEnd = me.path.split("/").pop();
        tree = [/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Details__WEBPACK_IMPORTED_MODULE_3__["default"], {
          key: i,
          summary: pathEnd,
          className: "tree-q tree-folder",
          summaryClass: "text-truncate tree-item tree-label q-mr qi qi-folder",
          summaryProps: {
            role: "treeitem",
            title: pathEnd
          }
        }, renderTree(me[key], "files"))].concat(_toConsumableArray(tree));
      }
    } // console.log('tree: ', tree);


    return tree;
  }; // const renderDrop = () => {
  // 	return (
  // 		<label className="mb-0 w-100 jumbotron"
  // 			onDrop={onChangeFile} 
  // 		>
  // 			<input onChange={onChangeFile} type="file" {...directoryAttr} multiple hidden />
  // 			<div className="lead">Drag 'n' drop some files here, or click to select files</div>
  // 		</label>
  // 	)
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "drops"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mb-0 w-100 jumbotron",
    hidden: (data === null || data === void 0 ? void 0 : data.length) > 0,
    onDrop: onChangeFile
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", _extends({
    onChange: onChangeFile,
    id: inputID,
    type: "file"
  }, directoryAttr, {
    multiple: true,
    hidden: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lead"
  }, "Drag 'n' drop some files here, or click to select files")), data && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_2__["default"], {
    As: "label",
    htmlFor: inputID
  }, "Change folder"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_2__["default"], {
    onClick: function onClick() {
      return setData(null);
    },
    kind: "danger"
  }, "Remove")), data && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-3 px-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "small"
  }, renderTree(data, "files"))));
}
/*
<Details 
	open 
	renderOpen 
	summary="Folder" // DEV OPTION: get root folder 
	className="small w-100 tree-q tree-folder" 
	summaryClass="text-truncate tree-item tree-file" 
	summaryProps={{
		role: "treeitem",
	}}
>
	{renderTree(data, "files")} 
</Details>

      <div {...getRootProps({className: "jumbotron dropzone"})}>
        <input {...getInputProps()} />
        <p className="lead">Drag 'n' drop some files here, or click to select files</p>
      </div>

      <aside>
			<h4>Files</h4>
			<ul>{files}</ul>
			</aside>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Details.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Details.js ***!
  \***********************************************************/
/*! exports provided: Summary, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Summary", function() { return Summary; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Details; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import Btn from '../../components/q-ui-react/Btn';

var Summary = function Summary(_ref) {
  var _ref$As = _ref.As,
      As = _ref$As === void 0 ? "summary" : _ref$As,
      inRef = _ref.inRef,
      etc = _objectWithoutProperties(_ref, ["As", "inRef"]);

  // const KeyDown = e => {
  // let parent = e.target.parentElement;
  // let next = parent.nextElementSibling;
  // console.log('key: ', e.key);
  // console.log('next: ', next);
  // console.log('parent: ', parent);
  // console.log('next.role: ', Q.hasAttr(next, "role"));
  // let isRole = Q.hasAttr(next, "role");
  // if(e.key === "ArrowDown" && ((parent?.tagName === "DETAILS" && next?.tagName === "DETAILS") || (next && isRole) )){
  // console.log("focus: ", next.firstElementChild);
  // if(isRole){
  // next.focus();
  // }else{
  // next?.firstElementChild?.focus();// click
  // }
  // }
  // }
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, _extends({
    tabIndex: "0"
  }, etc));
};
function Details(_ref2) {
  var _extends2;

  var inRef = _ref2.inRef,
      className = _ref2.className,
      summary = _ref2.summary,
      summaryClass = _ref2.summaryClass,
      summaryProps = _ref2.summaryProps,
      groupClass = _ref2.groupClass,
      children = _ref2.children,
      _ref2$open = _ref2.open,
      open = _ref2$open === void 0 ? false : _ref2$open,
      renderOpen = _ref2.renderOpen,
      _ref2$onToggle = _ref2.onToggle,
      onToggle = _ref2$onToggle === void 0 ? Q.noop : _ref2$onToggle,
      etc = _objectWithoutProperties(_ref2, ["inRef", "className", "summary", "summaryClass", "summaryProps", "groupClass", "children", "open", "renderOpen", "onToggle"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(renderOpen),
      _useState2 = _slicedToArray(_useState, 2),
      one = _useState2[0],
      setOne = _useState2[1]; // false


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(open),
      _useState4 = _slicedToArray(_useState3, 2),
      isOpen = _useState4[0],
      seIsOpen = _useState4[1]; // useEffect(() => {
  // 	console.log('%cuseEffect in Details','color:yellow;');
  // }, []);


  var Toggle = function Toggle(e) {
    var et = e.target; // console.log("onToggle e: ", e.target.open);

    if (!one) setOne(true); // setTimeout(() => {
    // 	et.open ? Q.domQ(".detail-content", et)
    // }, 1);
    // Q.setAttr(et, {"aria-expanded": et.open});

    seIsOpen(et.open);
    onToggle(et.open, e);
  };
  /* const clickSummary = e => {
  // console.log('clickSummary e: ', e);
  onClickSummary(e);
  } */


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("details", _extends({}, etc, (_extends2 = {
    ref: inRef,
    open: open // open | isOpen
    // aria-expanded={isOpen} // OPTION: for accessibility
    ,
    className: Q.Cx("detail-q", className)
  }, _defineProperty(_extends2, "open", Boolean(one) ? one : open), _defineProperty(_extends2, "onToggle", Toggle), _extends2)), summary && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Summary, _extends({
    className: summaryClass
  }, summaryProps, {
    "aria-expanded": isOpen // OPTION: for accessibility

  }), summary), one && ( // , { "d-none": !open }
  summary ? // children | Q.Cx("detail-content", groupClass)
  children && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "group",
    "aria-hidden": !isOpen,
    className: groupClass
  }, children) : children));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/DropsPage.js":
/*!********************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/DropsPage.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DropsPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_drops_Drops__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/drops/Drops */ "./resources/js/src/components/drops/Drops.js");
 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useDropzone } from 'react-dropzone';



function DropsPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Drops"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_drops_Drops__WEBPACK_IMPORTED_MODULE_2__["default"], null));
}
/*
      <div {...getRootProps({className: "jumbotron dropzone"})}>
        <input {...getInputProps()} />
        <p className="lead">Drag 'n' drop some files here, or click to select files</p>
      </div>

      <aside>
			<h4>Files</h4>
			<ul>{files}</ul>
			</aside>
*/

/***/ }),

/***/ "./resources/js/src/utils/file/traverseDirectory.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/utils/file/traverseDirectory.js ***!
  \**********************************************************/
/*! exports provided: traverseDirectory, fileEntry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "traverseDirectory", function() { return traverseDirectory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fileEntry", function() { return fileEntry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import {toFileWithPath} from '../dropzone/file';
// https://stackoverflow.com/questions/18815197/javascript-file-dropping-and-reading-directories-asynchronous-recursion
// === Read Directory Browser API === 
function fileEntry(entry) {
  return new Promise(function (resolve, reject) {
    return entry.file(resolve, reject);
  });
}

function traverseDirectory(entry) {
  var reader = entry.createReader();

  function errorHandler(e) {
    console.log(e);
  } // Resolved when the entire directory is traversed


  return new Promise(function (resolveDir) {
    var iterationAttempts = [];

    (function ReadEntries() {
      // According to the FileSystem API spec, readEntries() must be called until
      // it calls the callback with an empty array. Seriously??
      reader.readEntries(function (entries) {
        if (!entries.length) {
          resolveDir(Promise.all(iterationAttempts)); // Done iterating this particular directory
        } else {
          // Add a list of promises for each directory entry.  If the entry is itself
          // a directory, then that promise won't resolve until it is fully traversed.
          iterationAttempts.push(Promise.all(entries.map( /*#__PURE__*/function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(entry) {
              var getFile, loop;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (!entry.isFile) {
                        _context.next = 7;
                        break;
                      }

                      _context.next = 3;
                      return fileEntry(entry).then(function (v) {
                        return v;
                      });

                    case 3:
                      getFile = _context.sent;
                      return _context.abrupt("return", {
                        type: "file",
                        isFile: entry.isFile,
                        isDirectory: entry.isDirectory,
                        path: '/' + entry.name,
                        fullPath: entry.fullPath,
                        name: entry.name,
                        // contentType: getFile.type,
                        // integrity: "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
                        // lastModified: "Sat, 26 Oct 1985 08:15:00 GMT",
                        // size: getFile.size
                        file: getFile // entry

                      });

                    case 7:
                      _context.next = 9;
                      return traverseDirectory(entry);

                    case 9:
                      loop = _context.sent;
                      return _context.abrupt("return", {
                        isFile: entry.isFile,
                        isDirectory: entry.isDirectory,
                        path: entry.fullPath,
                        type: "directory",
                        files: loop[0]
                      });

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee);
            }));

            return function (_x) {
              return _ref.apply(this, arguments);
            };
          }())));
          ReadEntries(); // Try calling readEntries() again for the same dir, according to spec
        }
      }, errorHandler);
    })();
  });
}

 // USAGE:

/* dropzone.addEventListener("dragover",function(event){
  event.preventDefault();
}, false);

dropzone.addEventListener("drop",async function(event){
  let items = event.dataTransfer.items;

  event.preventDefault();// listing.innerHTML = "";

  for(let i=0; i<items.length; i++){
    let item = items[i].webkitGetAsEntry();

    if(item){
      // scanFiles(item, listing);
      let res = await traverseDirectory(item);
      console.log(res);
    }
  }

  // traverseDirectory(items).then(() => {
  //   // AT THIS POINT THE DIRECTORY SHOULD BE FULLY TRAVERSED.
  //   console.log();
  // });
}, false); */

/***/ })

}]);
//# sourceMappingURL=DropsPage.chunk.js.map