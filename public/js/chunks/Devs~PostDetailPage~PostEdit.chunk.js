(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs~PostDetailPage~PostEdit"],{

/***/ "./resources/js/src/components/player-q/PlayerQ.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/player-q/PlayerQ.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PlayerQ; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! screenfull */ "./node_modules/screenfull/dist/screenfull.js");
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(screenfull__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_player_lazy__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-player/lazy */ "./node_modules/react-player/lazy/index.js");
/* harmony import */ var react_player_lazy__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_player_lazy__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_idle_timer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-idle-timer */ "./node_modules/react-idle-timer/dist/index.es.js");
/* harmony import */ var _q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _q_ui_react_ContextMenu__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../q-ui-react/ContextMenu */ "./resources/js/src/components/q-ui-react/ContextMenu.js");
/* harmony import */ var _q_ui_react_BgImage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../q-ui-react/BgImage */ "./resources/js/src/components/q-ui-react/BgImage.js");
/* harmony import */ var _utils_clipboard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils/clipboard */ "./resources/js/src/utils/clipboard/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./utils */ "./resources/js/src/components/player-q/utils/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState,  }
// import { useFullscreen } from 'ahooks';





 // import { useInViewport } from 'ahooks';






 // function InViewPort({ inRef, children }){
//   // const ref = useRef();
//   const inView = useInViewport(inRef);
//   return (
//     <Fragment>
//       {children && children(inView)}
//     </Fragment>
//   );
// };

function validateExternalUrl(val) {
  var urlReg = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; // /^((https?|ftp|file):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;

  var isUrl = urlReg.exec(val);

  if (isUrl) {
    var _isUrl$;

    var sUrl;

    if ((_isUrl$ = isUrl[0]) === null || _isUrl$ === void 0 ? void 0 : _isUrl$.startsWith("https://youtu.be/")) {
      sUrl = "https://www.youtube.com/embed" + isUrl[4].replace("/embed", "");
    } else {
      sUrl = isUrl[1] ? val : isUrl[4] ? "https://www.youtube.com/embed" + isUrl[4].replace("/embed", "") : 'https://' + isUrl[0];
    }

    return sUrl;
  } else {
    // const iframeReg =  /<iframe.*?src=\s*(["'])(.*?)\1/; //  /<iframe.*?src="(.*?)"/;
    var getSrc = /<iframe.*?src=\s*(["'])(.*?)\1/.exec(val);

    if (getSrc) {
      console.log(getSrc[2]);
      return getSrc[2];
    }

    return false;
  }
}

var getYouTubeId = Q.cached(function (url) {
  var arr = url.split(/(vi\/|v%3D|v=|\/v\/|youtu\.be\/|\/embed\/\/?)/);
  var res = undefined !== arr[2] ? arr[2].split(/[^\w-]/i)[0] : arr[0];
  return res;
}); // const calcHeight = cached((h) => ({ height: `calc(100vh - ${h})` }));

var DdItem = function DdItem(_ref) {
  var className = _ref.className,
      etc = _objectWithoutProperties(_ref, ["className"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", _extends({}, etc, {
    className: Q.Cx("dropdown-item", className),
    type: "button"
  }));
};

var PlayerQ = /*#__PURE__*/function (_Component) {
  _inherits(PlayerQ, _Component);

  var _super = _createSuper(PlayerQ);

  function PlayerQ(props) {
    var _this;

    _classCallCheck(this, PlayerQ);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "ref", function (p) {
      _this.player = p;
    });

    _defineProperty(_assertThisInitialized(_this), "getPlayerDOM", function () {
      return _this.player.getInternalPlayer();
    });

    _this.state = {
      ready: false,
      playing: false,
      played: 0,
      // seeking: false, 
      duration: 0,
      loaded: 0,
      loop: false,
      volume: 0.5,
      // 0.8
      muted: false,
      pip: false,
      playbackRate: 1.0,
      lastVol: 0,
      size: null,
      // 360 
      // ccActive: false, 
      ccShow: "Off",
      // Selected track cc: null | "Off"
      ccFontSize: 100,
      ccFontColor: {
        name: "White",
        hex: "#fff"
      },
      // defaultCC: null, 
      active: false,
      ddSet: false,
      // Dropdown setting
      keyVol: false,
      bezelPlay: null,
      bezelCopy: false,
      // vPoster: props.poster ? true : false
      mouseTime: {
        time: null,
        position: 0
      },
      isFullscreen: false,
      tab: "menus",
      openInfo: false,
      autoPlayState: false,
      oncePlay: false // First play palyer if youtube src (DEVS for performance)

    };
    _this.wrap = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createRef"])();
    _this.btnBeezelRef = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createRef"])();
    _this.idleTimer = null;
    _this.timePos = 20;
    Q.bindFuncs.call(_assertThisInitialized(_this), ['onFs', 'onFsError', 'onPlay', 'onPause', 'onPlayPause', 'onSeekMouseDown', 'onSeekMouseUp', 'onSeekChange', 'onProgress', 'onDuration', 'onEnded', 'onVolumeChange', 'onMute', 'onBezelPlayPause', 'onBezelKeyDown', 'onActiveCC', 'onSetCc', 'onCueChange', 'onReady', 'onActive', 'onIdle', 'onPip', 'onCtxCopy', 'onToggleSet', 'onSetPlaybackRate', 'onSetSize', 'onMouseMoveRange', 'toFocusBezel']);
    return _this;
  }

  _createClass(PlayerQ, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // console.log('%ccomponentDidMount in PlayerQ','color:yellow;');
      if (screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.isEnabled) {
        screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.on('change', this.onFs);
        screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.on('error', this.onFsError);
      }
    } // componentDidUpdate(prevProps){
    //   const { tracks } = this.props;
    //   if(tracks !== prevProps.tracks && tracks?.length > 0){
    //     let ply = this.getPlayerDOM();
    //     let trackActive = Q.domQ("track[default]", ply);
    //     console.log('componentDidUpdate PlayerQ tracks: ', tracks);
    //     // console.log('onReady this.state.played: ', this.state.played);// trackActive.label
    //     // this.setState({  });
    //     if(trackActive){
    //       // const ccShow = trackActive.label;
    //       this.track = trackActive;
    //       // this.defaultCC = trackActive.label;// ccShow;
    //       // this.track.addEventListener("cuechange", this.onCueChange);
    //       this.setState({ defaultCC: trackActive.label });// , ccActive: true
    //     }
    //   }
    // }

  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.isEnabled) {
        screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.off('change', this.onFs);
        screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.off('error', this.onFsError);
      }

      if (this.track) {
        this.track.removeEventListener("cuechange", this.onCueChange);
      } // this.idleTimer = null;// OPTION

    }
  }, {
    key: "onFs",
    value: function onFs() {
      this.setState({
        isFullscreen: screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.isFullscreen
      });
    }
  }, {
    key: "onFsError",
    value: function onFsError(e) {
      console.log('onFsError: ', e);
      this.setState({
        fullscreenError: "Failed to enable fullscreen"
      });
    }
  }, {
    key: "onPlay",
    value: function onPlay() {
      this.setState({
        playing: true
      });
    }
  }, {
    key: "onPause",
    value: function onPause() {
      this.setState({
        playing: false
      });
    }
  }, {
    key: "onPlayPause",
    value: function onPlayPause() {
      this.setState({
        playing: !this.state.playing
      }); // this.setState(s => ({ playing: !s.playing }));
    }
  }, {
    key: "onSeekMouseDown",
    value: function onSeekMouseDown() {
      // console.log('onSeekMouseDown');
      this.setState({
        seeking: true
      });
    }
  }, {
    key: "onSeekMouseUp",
    value: function onSeekMouseUp(e) {
      var val = e.target.value; // this.player.seekTo(parseFloat(e.target.value));
      // setTimeout(() => {
      //   this.setState({ seeking: false });
      // }, 9);
      // console.log('onSeekMouseUp');

      this.setState({
        seeking: false
      });
      this.player.seekTo(parseFloat(val));
    }
  }, {
    key: "onSeekChange",
    value: function onSeekChange(e) {
      // let played = parseFloat(e.target.value);
      // this.player.seekTo(played);
      // this.setState({ played });
      this.setState({
        played: parseFloat(e.target.value)
      });
    }
  }, {
    key: "onProgress",
    value: function onProgress(state) {
      // console.log('onProgress state: ', state);
      // We only want to update time slider if we are not currently seeking
      // setTimeout(() => {
      // console.log('onProgress seeking: ', !this.state.seeking);
      // }, 1);
      if (!this.state.seeking) {
        this.setState(state); // { ...this.state, ...state }
      }
    }
  }, {
    key: "onDuration",
    value: function onDuration(duration) {
      // console.log('onDuration duration: ', duration);// this.state.
      if (duration >= 1000) this.timePos = 27;
      this.setState({
        duration: duration
      });
    }
  }, {
    key: "onEnded",
    value: function onEnded() {
      // console.log('onEnded');
      this.setState(function (s) {
        return {
          playing: s.loop,
          ccTxt: null
        };
      });
    } // NOT FIX:

  }, {
    key: "onVolumeChange",
    value: function onVolumeChange(e) {
      var _this2 = this;

      var v = e.target.value;
      this.setState(function (s) {
        return {
          // lastVol: s.volume, 
          volume: parseFloat(v)
        };
      }, function () {
        _this2.setState(function (s) {
          return {
            muted: s.volume === 0
          };
        });
      });
    } // NOT FIX:

  }, {
    key: "onMute",
    value: function onMute() {
      var _this3 = this;

      // const { muted, volume, lastVol } = this.state;
      this.setState(function (s) {
        return {
          // muted: !s.muted,
          lastVol: s.volume !== 0 ? s.volume : 0.5 // volume: s.muted && s.volume === 0 ? s.lastVol : 0

        };
      }, function () {
        _this3.setState(function (s) {
          return {
            muted: !s.muted,
            volume: s.muted && s.volume === 0 ? s.lastVol : 0
          };
        });
      });
    }
  }, {
    key: "onBezelPlayPause",
    value: function onBezelPlayPause() {
      var _this4 = this;

      this.onPlayPause();
      this.setState(function (s) {
        return {
          bezelPlay: "qi qi-" + (s.playing ? "play" : "pause") + "-circle "
        };
      });
      setTimeout(function () {
        return _this4.setState({
          bezelPlay: null
        });
      }, 500);
      if (!this.state.oncePlay) this.setState({
        oncePlay: true
      }); // First play if youtube src
    }
  }, {
    key: "onBezelKeyDown",
    value: function onBezelKeyDown(e) {
      if (_utils__WEBPACK_IMPORTED_MODULE_11__["BEZEL_KEYS"].includes(e.key)) {
        Q.preventQ(e);
        var _this$state = this.state,
            volume = _this$state.volume,
            keyVol = _this$state.keyVol;
        var v;

        switch (e.key) {
          case "k":
            // Play/Pause
            this.onBezelPlayPause();
            break;

          case "m":
            // Muted
            this.onMute();
            break;

          case "f":
            // Fullscreen
            screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.toggle(this.wrap.current);
            break;

          case "i":
            // Pip / Mini player
            this.onPip();
            break;

          case "ArrowRight":
            console.log("ArrowRight NEXT");
            break;

          case "ArrowLeft":
            console.log("ArrowLeft PREV");
            break;

          case "ArrowUp":
            // Volume Up
            v = volume + 0.05;
            if (v > 1) v = 1;
            this.setState({
              volume: v
            }); // Show range volume

            if (!keyVol) this.setState({
              keyVol: true
            });
            break;

          case "ArrowDown":
            // Volume Down
            v = volume - 0.05;
            if (v < 0) v = 0;
            this.setState({
              volume: v
            }); // Show range volume

            if (!keyVol) this.setState({
              keyVol: true
            });
            break;

          default:
            return;
        }
      }
    }
  }, {
    key: "onActiveCC",
    value: function onActiveCC() {
      var _this$state2 = this.state,
          ccShow = _this$state2.ccShow,
          defaultCC = _this$state2.defaultCC; // ccActive, 
      // this.setState({ ccActive: !ccActive });
      // this.onSetCc(!ccActive ? ccShow : "Off");

      this.onSetCc(ccShow === "Off" ? defaultCC : "Off");
      console.log('ccShow: ', ccShow);
      console.log('defaultCC: ', defaultCC);
    } // setCc = v => {
    //   if(ccShow !== v) onSetCc(v);
    //   this.setState({ tab: "menus" });
    // }

  }, {
    key: "onSetCc",
    value: function onSetCc(ccShow) {
      if (this.track) {
        // let isOff = ccShow === "Off";
        this.setState({
          ccShow: ccShow
        }); // , ccActive: isOff

        if (ccShow === "Off") {
          this.setState({
            ccTxt: null
          });
          this.track.removeEventListener("cuechange", this.onCueChange);
        } else {
          var ply = this.getPlayerDOM();
          var trackActive = Q.domQ("track[default]", ply);
          Q.setAttr(trackActive, "default");
          var trackActiveNow = Q.domQ("track[label=\"".concat(ccShow, "\"]"), ply);
          trackActiveNow["default"] = true; // console.log('trackActive: ', trackActive);

          this.track = trackActiveNow;
          this.track.addEventListener("cuechange", this.onCueChange);
        }
      }

      this.setState({
        tab: "menus"
      });
    }
  }, {
    key: "setUrl",
    value: function setUrl(url) {
      return Array.isArray(url) ? url.map(function (v) {
        return _objectSpread(_objectSpread({}, v), {}, {
          src: Q.newURL(v.src).href,
          sizes: v.sizes ? v.sizes + "px" : undefined
        });
      }) : Q.newURL(url).href;
    }
  }, {
    key: "onCueChange",
    value: function onCueChange(e) {
      var _cues$;

      var cues = e.target.track.activeCues;
      var ccTxt = (_cues$ = cues[0]) === null || _cues$ === void 0 ? void 0 : _cues$.text; // console.log('e: ', e);
      // console.log('cues: ', cues);
      // console.log('ccTxt: ', ccTxt);

      this.setState({
        ccTxt: ccTxt
      });
    }
  }, {
    key: "onReady",
    value: function onReady(p) {
      var tracks = this.props.tracks;
      this.setState({
        ready: true
      });

      if ((tracks === null || tracks === void 0 ? void 0 : tracks.length) > 0) {
        var ply = p.getInternalPlayer();
        var trackActive = Q.domQ("track[default]", ply); // console.log('onReady p: ', p);
        // console.log('onReady trackActive: ', trackActive.label);

        if (trackActive) {
          // const ccShow = trackActive.label;
          this.track = trackActive; // this.defaultCC = trackActive.label;// ccShow;
          // this.track.addEventListener("cuechange", this.onCueChange);

          this.setState({
            defaultCC: trackActive.label
          }); // , ccActive: true
        }
      }
    } // onAction = e => {
    //   let et = e?.target;
    //   console.log('onAction - user did something e: ', e);
    //   console.log('onAction target: ', et);
    // }

  }, {
    key: "onActive",
    value: function onActive() {
      // let et = e?.target;
      // console.log('onActive - user is active e: ', e);
      // console.log('onActive - time remaining: ', this.idleTimer.getRemainingTime());
      // console.log('onActive target: ', et?.tagName);
      this.setState({
        active: true
      });
    }
  }, {
    key: "onIdle",
    value: function onIdle() {
      var _this$state3 = this.state,
          ddSet = _this$state3.ddSet,
          keyVol = _this$state3.keyVol; // console.log('onIdle: ');

      if (!ddSet) this.setState({
        active: false
      });
      if (keyVol) this.setState({
        keyVol: false
      });
    }
  }, {
    key: "onPip",
    value: function onPip() {
      this.setState(function (s) {
        return {
          pip: !s.pip
        };
      });
    }
  }, {
    key: "onCtxCopy",
    value: function onCtxCopy(txt) {
      var _this5 = this;

      // let id = window.location.href.split("/");
      // id[id.length - 1]
      Object(_utils_clipboard__WEBPACK_IMPORTED_MODULE_10__["clipboardCopy"])(txt).then(function () {
        _this5.setState({
          bezelCopy: true
        });

        setTimeout(function () {
          return _this5.setState({
            bezelCopy: false
          });
        }, 500);
      })["catch"](function (e) {
        return console.log(e);
      });
    } // parseFileConfig(fileConfig){
    //   return fileConfig?.tracks ? { ...fileConfig, tracks: fileConfig.tracks.map(v => ({ ...v, src: Q.newURL(v.src).href })) } : fileConfig;
    // }

  }, {
    key: "parseCC",
    value: function parseCC(tracks) {
      // fileConfig
      return tracks ? ["Off"].concat(_toConsumableArray(tracks.map(function (v) {
        return v.label;
      }))) : null;
    }
  }, {
    key: "onToggleSet",
    value: function onToggleSet(open) {
      this.setState({
        ddSet: open
      });
      if (!open && this.state.tab !== "menus") this.setState({
        tab: "menus"
      });
    }
  }, {
    key: "onSetPlaybackRate",
    value: function onSetPlaybackRate(v) {
      if (this.state.playbackRate !== v) this.setState({
        playbackRate: v
      });
      this.setState({
        tab: "menus"
      });
    }
  }, {
    key: "onSetSize",
    value: function onSetSize(size) {
      if (size !== this.state.size) this.setState({
        size: size
      });
      this.setState({
        tab: "menus"
      });
    }
  }, {
    key: "onMouseMoveRange",
    value: function onMouseMoveRange(e) {
      var x = e.pageX;
      if (!x) return;
      var et = e.target;
      var duration = this.state.duration;
      var time = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["getPointerPosition"])(et, e).x * duration;
      var rect = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["findElPosition"])(et);
      var position = x - rect.left - this.timePos;
      var rectW = Math.round(rect.width) - this.timePos;
      if (position + this.timePos >= rectW) position = rectW - this.timePos;
      if (position < 0) position = 0;
      this.setState({
        mouseTime: {
          time: time,
          position: position
        }
      });
    }
  }, {
    key: "toFocusBezel",
    value: function toFocusBezel() {
      this.btnBeezelRef.current.focus();
    }
  }, {
    key: "render",
    value: function render() {
      var _this6 = this;

      var _this$props = this.props,
          reactPlayerClass = _this$props.reactPlayerClass,
          url = _this$props.url,
          config = _this$props.config,
          poster = _this$props.poster,
          tracks = _this$props.tracks,
          className = _this$props.className,
          wrapStyle = _this$props.wrapStyle,
          tmode = _this$props.tmode,
          enablePip = _this$props.enablePip,
          videoID = _this$props.videoID,
          playbackRates = _this$props.playbackRates,
          theaterMode = _this$props.theaterMode,
          onTheaterMode = _this$props.onTheaterMode,
          autoPlay = _this$props.autoPlay,
          onSetAutoPlay = _this$props.onSetAutoPlay;
      var _this$state4 = this.state,
          ready = _this$state4.ready,
          playing = _this$state4.playing,
          played = _this$state4.played,
          duration = _this$state4.duration,
          loaded = _this$state4.loaded,
          loop = _this$state4.loop,
          volume = _this$state4.volume,
          muted = _this$state4.muted,
          playbackRate = _this$state4.playbackRate,
          pip = _this$state4.pip,
          tab = _this$state4.tab,
          size = _this$state4.size,
          active = _this$state4.active,
          ddSet = _this$state4.ddSet,
          keyVol = _this$state4.keyVol,
          ccShow = _this$state4.ccShow,
          ccFontSize = _this$state4.ccFontSize,
          ccFontColor = _this$state4.ccFontColor,
          ccTxt = _this$state4.ccTxt,
          bezelPlay = _this$state4.bezelPlay,
          bezelCopy = _this$state4.bezelCopy,
          isFullscreen = _this$state4.isFullscreen,
          fullscreenError = _this$state4.fullscreenError,
          openInfo = _this$state4.openInfo,
          mouseTime = _this$state4.mouseTime,
          autoPlayState = _this$state4.autoPlayState,
          oncePlay = _this$state4.oncePlay; // 

      var sizes = Array.isArray(url) ? url.map(function (v) {
        return v.sizes && Number(v.sizes);
      }) : undefined;
      var cc = this.parseCC(tracks); // fileConfig

      var isYoutube = !oncePlay && Q.isStr(url) && validateExternalUrl(url);
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_ContextMenu__WEBPACK_IMPORTED_MODULE_8__["default"], {
        className: "zi-5",
        appendTo: this.wrap.current // this.wrap.current || document.body
        // esc={false} // Default = true
        // hideOnScroll={false} // Default = true
        // NOT FIX: popper config
        ,
        popperConfig: {
          strategy: "fixed" // enabled: false, 
          // modifiers: [
          //   // {
          //   //   name: "preventOverflow",
          //   //   options: {
          //   //     // padding: 100, 
          //   //     // boundary: document.body, // this.wrap.current
          //   //     // altBoundary: true, // false by default
          //   //     // rootBoundary: 'document', // 'viewport' by default
          //   //     // mainAxis: false, // true by default
          //   //     boundariesElement: this.wrap.current, // "scrollParent", 
          //   //     escapeWithReference: true
          //   //   }
          //   // }, 
          //   {
          //     name: "flip",
          //     options: {
          //       boundariesElement: this.wrap.current, // scrollParent | viewport
          //       enabled: false, 
          //       flipVariations: false
          //     }
          //   }
          // ]

        } // , ctxMenu | _, active
        ,
        component: function component(hide) {
          // if(!view && active){
          //   hide();
          //   // return null;
          // }
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "dropdown-menu show v-dd-sets",
            onContextMenu: function onContextMenu(e) {
              Q.preventQ(e);
              hide();
            } // ctxMenu | Q.preventQ | (e) => {Q.preventQ(e);hide()}

          }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
            className: "q-mr qi qi-" + (playing ? "pause" : "play"),
            onClick: function onClick() {
              hide();

              _this6.onBezelPlayPause();

              _this6.toFocusBezel();
            }
          }, playing ? "Pause" : "Play"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
            className: "q-mr qi qi-fullscreen" + (isFullscreen ? "-exit" : ""),
            onClick: function onClick() {
              hide();
              screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.toggle(_this6.wrap.current);

              _this6.toFocusBezel();
            }
          }, isFullscreen ? "Exit " : "", "Full screen"), videoID && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
            className: "q-mr qi qi-copy",
            onClick: function onClick() {
              hide();

              _this6.onCtxCopy(Q.baseURL + "/" + videoID);

              _this6.toFocusBezel();
            }
          }, "Copy video URL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
            className: "q-mr qi qi-copy",
            onClick: function onClick() {
              hide();

              _this6.onCtxCopy(Q.baseURL + "/" + videoID + "?t=" + Object(_utils__WEBPACK_IMPORTED_MODULE_11__["formatTime"])(duration * played, "."));

              _this6.toFocusBezel();
            }
          }, "Copy video URL at current time"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
            className: "q-mr qi qi-code",
            onClick: function onClick() {
              hide();

              _this6.onCtxCopy("<iframe width=\"716\" height=\"403\" src=\"".concat(Q.baseURL, "/embed/").concat(videoID, "\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));

              _this6.toFocusBezel();
            }
          }, "Copy embed code")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
            className: "q-mr qi qi-info",
            onClick: function onClick() {
              hide();

              _this6.setState({
                openInfo: true
              });
            }
          }, "Info"));
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        // role="application" // region
        ref: this.wrap,
        className: Q.Cx("embed-responsive embed-responsive-16by9 playerQ", {
          "v-ready": ready,
          "v-playing": playing,
          "v-active": active,
          "v-pip": pip,
          "ddSetOpen": ddSet
        }, className),
        style: wrapStyle
      }, ready && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_idle_timer__WEBPACK_IMPORTED_MODULE_5__["default"], {
        ref: function ref(r) {
          _this6.idleTimer = r;
        },
        element: this.wrap.current,
        timeout: 3000 // 1000 * 60 * 15
        ,
        debounce: 250,
        startOnMount: false,
        onActive: this.onActive,
        onIdle: this.onIdle // onAction={this.onAction} 

      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        ref: this.btnBeezelRef,
        type: "button",
        className: Q.Cx("btn rounded-0 shadow-none cauto w-100 text-primary qi-3x position-absolute position-full zi-2 v-bezel", _defineProperty({
          "i-load": !isYoutube && !ready,
          // !ready
          "qi qi-play-circle": isYoutube,
          // cpoin
          "playerQ-bezel-animate": bezelPlay || bezelCopy,
          "qi qi-copy": bezelCopy
        }, "" + bezelPlay, bezelPlay)) // !isYoutube && ready
        ,
        style: ready ? undefined : {
          '--bg-i-load': '95px'
        },
        onClick: this.onBezelPlayPause,
        onKeyDown: this.onBezelKeyDown
      }), isYoutube ?
      /*#__PURE__*/
      //  
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_BgImage__WEBPACK_IMPORTED_MODULE_9__["default"], {
        noRepeat: true // Play button: d-grid place-center text-primary qi qi-play-circle qi-4x
        ,
        className: "position-absolute position-full zi-1 bg-000 bg-pos-center point-no v-poster" // NOT FIX:
        ,
        url: "https://i3.ytimg.com/vi/" + getYouTubeId(url) + "/hqdefault.jpg" // bgSize={"auto " + el.clientHeight + " px"}
        ,
        onLoad: function onLoad(el) {
          // , e
          // console.log("e: ", e.path[0].width);// naturalWidth
          // console.log('el.clientWidth: ', el.clientWidth);
          el.style.backgroundSize = "auto " + el.clientHeight + "px"; // el.clientWidth + "px " + el.clientHeight + "px";
        }
      }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player_lazy__WEBPACK_IMPORTED_MODULE_4___default.a, {
        ref: this.ref // controls 
        ,
        className: reactPlayerClass,
        width: "100%",
        height: "100%",
        config: _objectSpread(_objectSpread({}, config), {}, {
          file: {
            // ...fileConfig, 
            tracks: tracks || [],
            // ...this.parseFileConfig(fileConfig), 
            attributes: {
              preload: poster ? "metadata" : "auto",
              // OPTION
              // crossOrigin: "anonymous", // OPTION
              tabIndex: "-1",
              controlsList: "nodownload nofullscreen noremoteplayback",
              // 
              playsInline: true,
              // OPTION
              "webkit-playsinline": "",
              "x-webkit-airplay": "allow",
              poster: poster
            }
          }
        }),
        url: this.setUrl(url),
        playing: playing,
        loop: loop,
        playbackRate: playbackRate,
        volume: volume,
        muted: muted,
        pip: pip,
        onReady: this.onReady // onStart={() => console.log('onStart')} 
        ,
        onError: function onError(e) {
          return console.log('onError: ', e);
        },
        onPlay: this.onPlay,
        onPause: this.onPause,
        onProgress: this.onProgress,
        onDuration: this.onDuration,
        onEnded: this.onEnded // onBuffer={() => console.log('onBuffer')} 
        // onBufferEnd={() => console.log('onBufferEnd')} 
        ,
        onEnablePIP: function onEnablePIP(e) {
          console.log('onEnablePIP: ', e);

          _this6.setState({
            pip: true
          });
        },
        onDisablePIP: function onDisablePIP(e) {
          console.log('onDisablePIP: ', e);

          _this6.setState({
            pip: false
          });
        } // onSeek={(e) => {
        //   console.log('onSeek e: ', e);
        //   this.setState({ played });
        // }}

      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: Q.Cx("text-white zi-2 v-ctrl", {
          "keyVol": keyVol
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "v-range",
        onMouseMove: this.onMouseMoveRange
      }, (mouseTime === null || mouseTime === void 0 ? void 0 : mouseTime.time) > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("time", {
        className: "tooltip-inner small point-no v-time-tip",
        style: {
          left: mouseTime.position
        }
      }, Object(_utils__WEBPACK_IMPORTED_MODULE_11__["formatTime"])(mouseTime.time)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "range",
        min: 0,
        max: 0.999999,
        step: "any",
        className: "q-range rounded-pill",
        style: {
          '--slider-val': played * 100 + '%'
        },
        value: played,
        onMouseDown: this.onSeekMouseDown,
        onMouseUp: this.onSeekMouseUp,
        onChange: this.onSeekChange
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        role: "progressbar",
        className: "q-progress rounded-pill point-no",
        "aria-valuemin": "0",
        "aria-valuemax": "100",
        "aria-valuenow": loaded * 100,
        style: {
          height: 5,
          "--val": loaded * 100 + "%"
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        align: "center",
        className: "mt-2 v-btn-ctrl"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm" // kind="flat" 
        ,
        "aria-label": (playing ? "Pause" : "Play") + " (k)",
        className: "mr-1 tip tipTL qi qi-" + (playing ? "pause" : "play"),
        onClick: function onClick() {
          _this6.onPlayPause();

          _this6.toFocusBezel();
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        align: "center",
        className: "mr-2 v-volume"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm",
        className: Q.Cx("px-1 tip tipT flexno q-s13 q-fw qi", {
          "qi-volume": volume * 10 >= 5,
          // volume > 0, // 
          "qi-volume-down": volume * 10 < 5,
          // volume.toFixed(1) < 0.5
          "qi-volume-mute": muted || volume === 0 // slash

        }),
        "aria-label": (muted || volume === 0 ? "Unmute" : "Mute") + " (m)",
        onClick: function onClick() {
          _this6.onMute();

          _this6.toFocusBezel();
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "range",
        min: 0,
        max: 1,
        step: "any",
        className: "q-range rounded-pill ml-1",
        style: {
          '--slider-val': volume * 100 + '%'
        },
        value: volume,
        onChange: this.onVolumeChange
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
        className: "mr-auto v-time"
      }, Object(_utils__WEBPACK_IMPORTED_MODULE_11__["formatTime"])(duration * played), " / ", Object(_utils__WEBPACK_IMPORTED_MODULE_11__["formatTime"])(duration)), (cc === null || cc === void 0 ? void 0 : cc.length) > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm",
        className: "tip tipT q-s12 qi qi-cc",
        active: ccShow !== "Off",
        "aria-label": "Subtitles/closed captions",
        onClick: function onClick() {
          _this6.onActiveCC();

          _this6.toFocusBezel();
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
        alignRight: true,
        drop: "up",
        show: ddSet,
        onToggle: this.onToggleSet
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
        size: "sm",
        bsPrefix: "tip tipT q-s11 qi qi-cog v-btn-set",
        "aria-label": "Settings"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
        flip: false,
        className: "v-dd-sets"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Container, {
        id: "playerQ-tabSets",
        activeKey: tab,
        onSelect: function onSelect(tab) {
          return _this6.setState({
            tab: tab
          });
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Content, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "menus"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        As: "label",
        justify: "between",
        align: "center",
        className: "dropdown-item"
      }, "Autoplay", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "custom-control custom-switch d-block ml-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "checkbox",
        className: "custom-control-input",
        checked: Q.isBool(autoPlay) ? autoPlay : autoPlayState,
        onChange: function onChange(e) {
          var checked = e.target.checked;
          Q.isBool(autoPlay) ? onSetAutoPlay(checked) : _this6.setState({
            autoPlayState: checked
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "custom-control-label"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        As: "label",
        justify: "between",
        align: "center",
        className: "dropdown-item"
      }, "Annotations", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "custom-control custom-switch d-block ml-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "checkbox",
        className: "custom-control-input"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "custom-control-label"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        justify: "between",
        align: "center",
        className: "dropdown-item",
        role: "button",
        onClick: function onClick() {
          return _this6.setState({
            tab: "playbackSpeed"
          });
        }
      }, "Playback speed", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "ml-5 qia qia-chevron-right q-ml"
      }, playbackRate === 1.0 ? "Normal" : playbackRate)), (cc === null || cc === void 0 ? void 0 : cc.length) > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        justify: "between",
        align: "center",
        className: "dropdown-item",
        role: "button",
        onClick: function onClick() {
          return _this6.setState({
            tab: "cc"
          });
        }
      }, "Subtitles/CC", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "ml-5 qia qia-chevron-right q-ml"
      }, ccShow)), size && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        justify: "between",
        align: "center",
        className: "dropdown-item",
        role: "button",
        onClick: function onClick() {
          return _this6.setState({
            tab: "quality"
          });
        }
      }, "Quality", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "ml-5"
      }, size, "p"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "playbackSpeed"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
        onClick: function onClick() {
          return _this6.setState({
            tab: "menus"
          });
        },
        className: "qi qi-chevron-left q-mr"
      }, "Playback speed"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), playbackRates.map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
          key: i,
          className: playbackRate === v ? "qi qi-check q-mr" : "pl-36px",
          onClick: function onClick() {
            return _this6.onSetPlaybackRate(v);
          }
        }, v === 1.0 ? "Normal" : v);
      })), (cc === null || cc === void 0 ? void 0 : cc.length) > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "cc"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        justify: "between",
        className: "py-1 px-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        onClick: function onClick() {
          return _this6.setState({
            tab: "menus"
          });
        },
        outline: true,
        kind: "dark",
        size: "xs",
        className: "fw600 qi qi-chevron-left q-mr"
      }, "Subtitles/CC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        onClick: function onClick() {
          return _this6.setState({
            tab: "ccOptions"
          });
        },
        outline: true,
        kind: "dark",
        size: "xs",
        className: "fw600"
      }, "Options")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
        className: "dd-hr"
      }), cc.map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
          key: i,
          className: ccShow === v ? "qi qi-check q-mr" : "pl-36px",
          onClick: function onClick() {
            return _this6.onSetCc(v);
          }
        }, v);
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "ccOptions"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
        onClick: function onClick() {
          return _this6.setState({
            tab: "cc"
          });
        },
        className: "qi qi-chevron-left q-mr"
      }, "Options"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), [// {name:'Font family'},
      {
        name: "Font color",
        use: ccFontColor.name
      }, {
        name: "Font size",
        use: ccFontSize + "%"
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
          key: v.name,
          justify: "between",
          align: "center",
          className: "dropdown-item",
          role: "button",
          onClick: function onClick() {
            return _this6.setState({
              tab: v.name
            });
          }
        }, v.name, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "ml-5 qia qia-chevron-right q-ml"
        }, v.use));
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "Font color"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
        onClick: function onClick() {
          return _this6.setState({
            tab: "ccOptions"
          });
        },
        className: "qi qi-chevron-left q-mr"
      }, "Font color"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), _utils__WEBPACK_IMPORTED_MODULE_11__["CC_FONT_COLORS"].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
          key: v.name,
          className: ccFontColor.hex === v.hex ? "qi qi-check q-mr" : "pl-36px",
          onClick: function onClick() {
            return _this6.setState({
              ccFontColor: v
            });
          }
        }, v.name);
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "Font size"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
        onClick: function onClick() {
          return _this6.setState({
            tab: "ccOptions"
          });
        },
        className: "qi qi-chevron-left q-mr"
      }, "Font size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), _utils__WEBPACK_IMPORTED_MODULE_11__["CC_FONT_SIZES"].map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
          key: i,
          className: ccFontSize === v ? "qi qi-check q-mr" : "pl-36px",
          onClick: function onClick() {
            return _this6.setState({
              ccFontSize: v
            });
          }
        }, v, "%");
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
        eventKey: "quality"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
        onClick: function onClick() {
          return _this6.setState({
            tab: "menus"
          });
        },
        className: "qi qi-chevron-left q-mr"
      }, "Quality"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), (sizes === null || sizes === void 0 ? void 0 : sizes.length) > 0 && sizes.map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DdItem, {
          key: i,
          className: size === v ? "qi qi-check q-mr" : "pl-36px",
          onClick: function onClick() {
            return _this6.onSetSize(v);
          }
        }, v, "p");
      })))))), enablePip && !isFullscreen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm",
        className: "tip tipT qi qi-tv-alt",
        "aria-label": "Mini player (i)",
        onClick: function onClick() {
          _this6.onPip();

          _this6.toFocusBezel();
        }
      }), theaterMode && !isFullscreen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm",
        className: "tip tipTR q-s11 qi qi-film",
        "aria-label": (tmode ? "Default view" : "Theater mode") + " (t)",
        onClick: function onClick() {
          _this6.toFocusBezel();

          onTheaterMode();
        }
      }), screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.isEnabled && !fullscreenError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        size: "sm" // className="tip tipTR qi qi-expand" 
        ,
        className: "tip tipTR q-s11 qi qi-fullscreen" + (isFullscreen ? "-exit" : ""),
        "aria-label": (isFullscreen ? "Exit " : "") + "Full screen (f)",
        onClick: function onClick() {
          screenfull__WEBPACK_IMPORTED_MODULE_1___default.a.toggle(_this6.wrap.current);

          _this6.toFocusBezel();
        } // toggleFull

      }))), ccTxt && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "position-absolute l0 r0 mx-auto text-center v-cc",
        style: {
          fontSize: ccFontSize !== 100 ? ccFontSize + "%" : undefined,
          color: ccFontColor.name !== "White" ? ccFontColor.hex : undefined
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, ccTxt)), (openInfo || fullscreenError) &&
      /*#__PURE__*/
      // OPTION
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_6__["default"], {
        justify: "center",
        align: "center",
        className: "position-absolute position-full h-100 zi-3 v-info",
        "aria-live": "polite",
        "aria-atomic": true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "toast show"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "toast-header"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
        className: "mr-auto qi qi-info q-mr"
      }, "Q Player"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
        onClick: function onClick() {
          return _this6.setState({
            openInfo: false
          });
        },
        close: true,
        className: "mb-1",
        "aria-label": "Close"
      }, "\xD7")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "toast-body"
      }, fullscreenError)))));
    }
  }]);

  return PlayerQ;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


PlayerQ.defaultProps = {
  playbackRates: [0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2],
  onTheaterMode: function onTheaterMode() {},
  onSetAutoPlay: function onSetAutoPlay() {}
};

/***/ }),

/***/ "./resources/js/src/components/player-q/utils/index.js":
/*!*************************************************************!*\
  !*** ./resources/js/src/components/player-q/utils/index.js ***!
  \*************************************************************/
/*! exports provided: formatTime, findElPosition, getPointerPosition, BEZEL_KEYS, CC_FONT_COLORS, CC_FONT_SIZES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatTime", function() { return formatTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findElPosition", function() { return findElPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPointerPosition", function() { return getPointerPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BEZEL_KEYS", function() { return BEZEL_KEYS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CC_FONT_COLORS", function() { return CC_FONT_COLORS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CC_FONT_SIZES", function() { return CC_FONT_SIZES; });
// 
function pad(string) {
  return ('0' + string).slice(-2);
}

function formatTime(seconds) {
  var separator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ":";
  var date = new Date(seconds * 1000);
  var hh = date.getUTCHours();
  var mm = date.getUTCMinutes();
  var ss = pad(date.getUTCSeconds());
  if (hh) return hh + separator + pad(mm) + separator + ss; // `${hh}${separator}${pad(mm)}${separator}${ss}`;

  return mm + separator + ss; // `${mm}:${ss}`
} // FROM: video-react


function findElPosition(el) {
  var box;

  if (el.getBoundingClientRect && el.parentNode) {
    box = el.getBoundingClientRect();
  }

  if (!box) {
    return {
      left: 0,
      top: 0
    };
  }

  var docEl = document.documentElement;
  var body = document.body;
  var clientLeft = docEl.clientLeft || body.clientLeft || 0;
  var scrollLeft = window.pageXOffset || body.scrollLeft;
  var left = box.left + scrollLeft - clientLeft;
  var clientTop = docEl.clientTop || body.clientTop || 0;
  var scrollTop = window.pageYOffset || body.scrollTop;
  var top = box.top + scrollTop - clientTop; // Android sometimes returns slightly off decimal values, so need to round

  return {
    left: Math.round(left),
    top: Math.round(top),
    width: box.width // Q-CUSTOM

  };
}

function getPointerPosition(el, e) {
  var pos = {};
  var box = findElPosition(el);
  var boxW = el.offsetWidth;
  var boxH = el.offsetHeight;
  var boxY = box.top;
  var boxX = box.left;
  var pY = e.pageY;
  var pX = e.pageX;

  if (e.changedTouches) {
    pX = e.changedTouches[0].pageX;
    pY = e.changedTouches[0].pageY;
  }

  pos.y = Math.max(0, Math.min(1, (boxY - pY + boxH) / boxH));
  pos.x = Math.max(0, Math.min(1, (pX - boxX) / boxW));
  return pos;
} // Keyboard Shortcut:


var BEZEL_KEYS = ["ArrowRight", "ArrowLeft", "ArrowUp", "ArrowDown", "f", "i", "k", "m"];
var CC_FONT_COLORS = [{
  name: "White",
  hex: "#fff"
}, {
  name: "Yellow",
  hex: "#ffff00"
}, {
  name: "Green",
  hex: "#00ff00"
}, {
  name: "Cyan",
  hex: "#00ffff"
}, {
  name: "Blue",
  hex: "#0000ff"
}, {
  name: "Magenta",
  hex: "#ff00ff"
}, {
  name: "Red",
  hex: "#ff0000"
}];
var CC_FONT_SIZES = [50, 75, 100, 125, 150, 175, 200]; // 300, 400
// URL for copy:
// const 



/***/ }),

/***/ "./resources/js/src/components/q-ui-react/BgImage.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/BgImage.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BgImage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // useState
// import Btn from '../../components/q-ui-bootstrap/Btn';
// import { Cx } from '../../utils/Q';

/* function BgLoader({
	url, 
	// seconds = 1, 
	onLoad = () => {}, 
	onError = () => {}
}) {
  // let loaded = false;
  let img = new Image();

  // this will occur when the image is successfully loaded
  // no matter if seconds past
  img.onload = function(e){
    // loaded = true;
    // let div = document.getElementById("dk");
		// div.style.backgroundImage = "url('" + url + "')";
		// return url;
		onLoad(e);
		return;
	}
	img.onerror = function(e){
		onError(e);
		return;
	}
  img.src = url;

  // setTimeout(() => {
  //   // if when the time has passed image.onload had already occurred, run onLoad()
  //   if (loaded) onLoad();
  //   else onError();
  // }, seconds * 1000);
}*/

function BgImage(_ref) {
  var className = _ref.className,
      url = _ref.url,
      noRepeat = _ref.noRepeat,
      style = _ref.style,
      bgSize = _ref.bgSize,
      w = _ref.w,
      h = _ref.h,
      _ref$As = _ref.As,
      As = _ref$As === void 0 ? "div" : _ref$As,
      _ref$onLoad = _ref.onLoad,
      onLoad = _ref$onLoad === void 0 ? function () {} : _ref$onLoad,
      _ref$onError = _ref.onError,
      onError = _ref$onError === void 0 ? function () {} : _ref$onError,
      etc = _objectWithoutProperties(_ref, ["className", "url", "noRepeat", "style", "bgSize", "w", "h", "As", "onLoad", "onError"]);

  var elRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // const [err, setErr] = useState(false);

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // console.log('%cuseEffect in BgImage','color:yellow;');
    // function loadBg(){
    // 	BgLoader({
    // 		url, 
    // 		onLoad: () => {
    // 			console.log('onLoad bgLoad');
    // 		},
    // 		onError: () => {
    // 			console.log('onError bgLoad');
    // 			setErr(true);
    // 		}
    // 	});
    // }
    // loadBg();
    // BgLoader({
    // 	url, 
    // 	onLoad: (e) => {
    // 		// console.log('onLoad bgLoad');
    // 		onLoad(url, e);
    // 	},
    // 	onError: (e) => {
    // 		// console.log('onError bgLoad');
    // 		setErr(true);
    // 		onError(url, e);
    // 	}
    // });
    var img = new Image(); // this will occur when the image is successfully loaded, no matter if seconds past

    /* img.onload = function(e){
    	onLoad(url, elRef.current, e);
    	return null;
    }
    img.onerror = function(e){
    	// setErr(true);
    	onError(url, elRef.current, e);
    	return null;
    } */

    var loadEvt = function loadEvt(e) {
      onLoad(elRef.current, e); // img

      return;
    };

    var errEvt = function errEvt(e) {
      onError(elRef.current, e);
      return;
    };

    img.addEventListener("load", loadEvt); // , {once: true}

    img.addEventListener("error", errEvt);
    img.src = url;
    return function () {
      img.removeEventListener("load", loadEvt);
      img.removeEventListener("error", errEvt);
    }; // 
  }, [url]); // , onLoad, onError
  // console.log('err: ', err);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, _extends({}, etc, {
    ref: elRef,
    className: Q.Cx("bg-img", {
      "bg-no-repeat": noRepeat
    }, className),
    style: _objectSpread(_objectSpread({}, style), {}, {
      width: w,
      height: h,
      backgroundSize: bgSize,
      // `url("${err ? errImg : Q.newURL(url).href}")` 
      backgroundImage: "url(\"".concat(Q.newURL(url).href, "\")")
    })
  }));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/ContextMenu.js":
/*!***************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/ContextMenu.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_overlays_usePopper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-overlays/usePopper */ "./node_modules/react-overlays/esm/usePopper.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }


 // react-popper
// DEVS OPTION: for hide component when scroll
// import { useScroll } from 'ahooks';
// import debounce from 'lodash.debounce';
// import Btn from '../../components/q-ui-react/Btn';

var makeVirtualEl = function makeVirtualEl() {
  var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return {
    getBoundingClientRect: function getBoundingClientRect() {
      return {
        width: 0,
        height: 0,
        top: y,
        right: x,
        bottom: y,
        left: x
      };
    }
  };
};

function ContextMenu(_ref) {
  var dom = _ref.dom,
      children = _ref.children,
      component = _ref.component,
      popperConfig = _ref.popperConfig,
      className = _ref.className,
      _ref$esc = _ref.esc,
      esc = _ref$esc === void 0 ? true : _ref$esc,
      _ref$hideOnScroll = _ref.hideOnScroll,
      hideOnScroll = _ref$hideOnScroll === void 0 ? true : _ref$hideOnScroll,
      _ref$appendTo = _ref.appendTo,
      appendTo = _ref$appendTo === void 0 ? document.body : _ref$appendTo,
      _ref$onContextMenu = _ref.onContextMenu,
      onContextMenu = _ref$onContextMenu === void 0 ? Q.noop : _ref$onContextMenu,
      _ref$onEsc = _ref.onEsc,
      onEsc = _ref$onEsc === void 0 ? Q.noop : _ref$onEsc,
      _ref$onScrolls = _ref.onScrolls,
      onScrolls = _ref$onScrolls === void 0 ? Q.noop : _ref$onScrolls;
  // const scroll = useScroll(document, () => console.log('scroll: ', val));
  // const setPopperConfig = (targetFlip) => {
  // if(targetFlip){
  // return {
  // ...popperConfig, 
  // modifiers: [
  // {
  // name: 'flip',
  // options: {
  // boundary: targetFlip,
  // },
  // },
  // ],
  // }
  // }
  // return popperConfig;
  // };
  // const [popper, setPopper] = useState(setPopperConfig());
  var child = react__WEBPACK_IMPORTED_MODULE_0__["Children"].only(children);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      active = _useState2[0],
      setActive = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      clonedEl = _useState4[0],
      setClonedEl = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(makeVirtualEl()),
      _useState6 = _slicedToArray(_useState5, 2),
      virtualEl = _useState6[0],
      setVirtualEl = _useState6[1];

  var _usePopper = Object(react_overlays_usePopper__WEBPACK_IMPORTED_MODULE_2__["default"])(virtualEl, clonedEl, popperConfig),
      styles = _usePopper.styles,
      attributes = _usePopper.attributes; // popper


  var hide = function hide() {
    setActive(false);
    onContextMenu(false); // Q-CUSTOM: For ???
  };

  var onCtxMenu = function onCtxMenu(e) {
    if (e) e.preventDefault();
    var clientX = e.clientX,
        clientY = e.clientY,
        target = e.target;
    var rectEl;

    if (dom) {
      rectEl = target;
    } else {
      rectEl = makeVirtualEl(clientX, clientY);
    } // console.log('rectEl: ', rectEl);
    // setPopper(setPopperConfig(target));


    setVirtualEl(rectEl); // makeVirtualEl(clientX, clientY)

    setActive(true);
    onContextMenu(active, e); // Q-CUSTOM
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var onOutsideClick = function onOutsideClick(e) {
      if (clonedEl && !clonedEl.contains(e.target)) {
        hide();
        onContextMenu(active, e); // Q-CUSTOM
      }
    }; // Q-CUSTOM DEV OPTION: for hide component when scroll 
    // const onScrollDoc = debounce(() => {
    // if(active){
    // hide();
    // }
    // }, 50);


    var onScrollDoc = function onScrollDoc() {
      if (active) {
        hide();
        onScrolls();
      }
    };

    var onEscKey = function onEscKey(e) {
      // console.log(e);
      if (active && e.key === "Escape") {
        hide();
        onEsc(false);
      }
    };

    document.addEventListener('mousedown', onOutsideClick); // Q-CUSTOM DEV OPTION: for hide component when esc key

    if (esc && active) document.addEventListener('keydown', onEscKey); // Q-CUSTOM DEV OPTION: for hide component when scroll 

    if (hideOnScroll && active) document.addEventListener('scroll', onScrollDoc);
    return function () {
      document.removeEventListener('mousedown', onOutsideClick); // Q-CUSTOM DEV OPTION: for hide component when esc key

      if (esc) document.removeEventListener('keydown', onEscKey); // Q-CUSTOM DEV OPTION: for hide component when scroll

      if (hideOnScroll) document.removeEventListener('scroll', onScrollDoc);
    };
  }, [clonedEl, esc, hideOnScroll, active]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"])(child, _objectSpread(_objectSpread({}, child.props), {}, {
    onContextMenu: onCtxMenu
  })), active && /*#__PURE__*/react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.createPortal( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({
    ref: setClonedEl,
    style: _objectSpread({}, styles.popper)
  }, attributes.popper, {
    className: className
  }), component && component(hide, onCtxMenu, active)), appendTo // document.body
  ));
}
;

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Img.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Img.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Img; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // , { useState }
// import { Cx } from '../../utils/Q.js';
// errorText, errorDesc, onError, src, 

function Img(_ref) {
  var inRef = _ref.inRef,
      _ref$alt = _ref.alt,
      alt = _ref$alt === void 0 ? "" : _ref$alt,
      src = _ref.src,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? "lazy" : _ref$loading,
      _ref$drag = _ref.drag,
      drag = _ref$drag === void 0 ? false : _ref$drag,
      WrapAs = _ref.WrapAs,
      w = _ref.w,
      h = _ref.h,
      fluid = _ref.fluid,
      thumb = _ref.thumb,
      circle = _ref.circle,
      round = _ref.round,
      className = _ref.className,
      frameClass = _ref.frameClass,
      frame = _ref.frame,
      caption = _ref.caption,
      captionClass = _ref.captionClass,
      onLoad = _ref.onLoad,
      onError = _ref.onError,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "alt", "src", "loading", "drag", "WrapAs", "w", "h", "fluid", "thumb", "circle", "round", "className", "frameClass", "frame", "caption", "captionClass", "onLoad", "onError", "children"]);

  // const [load, setLoad] = useState(false);
  // const [err, setErr] = useState(null);
  // const support = 'loading' in HTMLImageElement.prototype;
  // const isLazy = SUPPORT_LOADING && loading === 'lazy';
  var bsFigure = frame === 'bs';
  var As = bsFigure ? "figure" : WrapAs ? WrapAs : "picture";

  var setCx = function setCx(et) {
    Q.setClass(et, "ava-loader", "remove"); // if(fluid || thumb) Q.setAttr(et, "height");
  };

  var Load = function Load(e) {
    // Q.setClass(e.target, "ava-loader", "remove");// OPTION: remove or Not
    setCx(e.target);
    if (onLoad) onLoad(e);
  }; // DEV: 


  var Error = function Error(e) {
    var et = e.target; // Q.setClass(et, "img-err");
    // Q.setAttr(et, {"aria-label":`&#xF03E;`});
    // setErr("&#xF03E;"); 

    setCx(et);
    et.src = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='95' viewBox='0 0 32 32'%3E%3Cg%3E%3Cpath d='M29,4H3A1,1,0,0,0,2,5V27a1,1,0,0,0,1,1H29a1,1,0,0,0,1-1V5A1,1,0,0,0,29,4ZM28,26H4V6H28Z'/%3E%3Cpath d='M18.29,12.29a1,1,0,0,1,1.42,0L26,18.59V9a1,1,0,0,0-1-1H7A1,1,0,0,0,6,9v1.58l7,7Z'/%3E%3Cpath d='M7,24H25a1,1,0,0,0,1-1V21.41l-7-7-5.29,5.3a1,1,0,0,1-1.42,0L6,13.42V23A1,1,0,0,0,7,24Z'/%3E%3C/g%3E%3C/svg%3E";
    if (onError) onError(e);
    return; // null
  };

  var img = function img() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", _extends({}, etc, {
      ref: inRef,
      loading: loading,
      alt: alt // OPTION
      ,
      width: w // isLazy && !w && !load ? 40 : w
      ,
      height: h // isLazy && !h && !load ? 40 : h
      ,
      src: Q.newURL(src).href // OPTION: Q.isDomain(src) ? src : Q.newURL(src).href
      ,
      className: Q.Cx("ava-loader", {
        // holder thumb| ava-loader
        'img-fluid': fluid,
        // fluid && !isLazy
        'img-thumbnail': thumb,
        'rounded-circle': circle,
        'rounded': round,
        'figure-img': bsFigure // 'fal img-err': err

      }, className),
      draggable: drag // !!drag 
      // aria-label={label || err} 
      ,
      onError: Error,
      onLoad: Load // OPTION: 
      // onContextMenu={e => {
      // Q.preventQ(e);
      // }}

      /* onLoad={e => {
      	if(isLazy){
      		setLoad(true);
      		let et = e.target;
      		// setAttr(et, 'width height');
      		setClass(et, 'isLoad');//  + (fluid ? ' img-fluid' : '')
      	}
      		onLoad(e);
      }} */

    }));
  };

  if (frame) {
    // const bsFigure = frame === 'bs';
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As // figure | div 
    // ref={frameRef} 
    , {
      className: // thumb
      Q.Cx("img-frame", {
        'figure': bsFigure
      }, frameClass) // draggable="false" // OPTION

    }, img(), bsFigure && caption && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
      className: Q.Cx("figure-caption", captionClass)
    }, caption), children);
  }

  return img();
} // Img.defaultProps = {
// loading: 'lazy',
// alt: '',
// onLoad: noop
// noDrag: true
// };

/* const error = e => {
	if(onError){
		onError(e);
	}else{
		// data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${this.props.alt}%3C/text%3E%3C/svg%3E
		e.target.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='45%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${errorText ? errorText : alt}%3C/text%3E%3Ctext x='50%25' y='55%25' fill='%23fff' dy='.5em' style='font-size:0.7rem;font-family:sans-serif'%3E${errorDesc ? errorDesc : 'Image not found'}%3C/text%3E%3C/svg%3E`;// "/img/imgError.svg";
		return null;// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
	}
   } */

/* const strNum = P.oneOfType([P.string, P.number]);

Img.propTypes = {
	frame: P.bool,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  alt: P.string,

  w: strNum,
  h: strNum,
	// src: P.string, // .isRequired

  fluid: P.bool,
  thumb: P.bool,
  circle: P.bool,
  round: P.bool,
  noDrag: P.bool,
	onError: P.func,
	onLoad: P.func
}; */

/***/ }),

/***/ "./resources/js/src/utils/clipboard/index.js":
/*!***************************************************!*\
  !*** ./resources/js/src/utils/clipboard/index.js ***!
  \***************************************************/
/*! exports provided: clipboardCopy, copyFn, pasteBlob, permissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clipboardCopy", function() { return clipboardCopy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyFn", function() { return copyFn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pasteBlob", function() { return pasteBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "permissions", function() { return permissions; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Copy to clipboard:
function clipboardCopy(target) {
  return new Promise(function (resolve, reject) {
    var clipboard = navigator.clipboard,
        txt;
    if (typeof target === 'string' && !target.tagName) txt = target;else if (target === null || target === void 0 ? void 0 : target.tagName) txt = target.textContent; // target.innerText
    else reject(new Error('Text or target DOM to copy is required.'));

    if (clipboard) {
      clipboard.writeText(txt).then(function () {
        return resolve(txt);
      })["catch"](function (e) {
        return reject(e);
      });
    } else {
      console.log('%cnavigator.clipboard NOT SUPPORT', 'color:yellow');
      copyFn(txt, {
        onOk: function onOk() {
          resolve(txt);
        },
        onErr: function onErr(e) {
          reject(e);
        }
      });
    }
  });
}

function copyFn(str, _ref) {
  var _ref$onOk = _ref.onOk,
      onOk = _ref$onOk === void 0 ? function () {} : _ref$onOk,
      _ref$onErr = _ref.onErr,
      onErr = _ref$onErr === void 0 ? function () {} : _ref$onErr;
  var DOC = document,
      el = DOC.createElement("textarea"),
      iOS = window.navigator.userAgent.match(/ipad|iphone/i);
  el.className = "sr-only sr-only-focusable";
  el.contentEditable = true; // needed for iOS >= 10

  el.readOnly = false; // needed for iOS >= 10

  el.value = str; // el.style.border = "0";
  // el.style.padding = "0";
  // el.style.margin = "0";
  // el.style.position = "absolute";
  // sets vertical scroll
  // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
  // el.style.top = `${yPosition}px`;

  DOC.body.appendChild(el);

  if (iOS) {
    var range = DOC.createRange();
    range.selectNodeContents(el);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    el.setSelectionRange(0, 999999);
  } else {
    el.focus({
      preventScroll: false
    });
    el.select();
  }

  var OK = DOC.execCommand("copy");

  if (OK) {
    onOk(str);
  } else {
    onErr('Failed to copy');
  }

  el.remove(); // DOC.body.removeChild(el);
} // 
// const pasteBlob = async (src) => {
// 	try{
// 		// const imgURL = '/images/generic/file.png';
// 		const data = await fetch(src);
// 		const blob = await data.blob();
// 		await navigator.clipboard.write([
// 			new ClipboardItem({
// 				[blob.type]: blob
// 			})
// 		]);
// 		console.log('Image copied.');
// 	}catch(e){
// 		console.error(e.name, e.message);
// 	}
// }
// async function pasteBlob(){
//   try{
// 		if(!navigator.clipboard.read) return false;
// 		const clipboardItems = await navigator.clipboard.read();
// 		// console.log('clipboardItems: ', clipboardItems);
// 		let data = false;
//     for(const item of clipboardItems){
//       for(const type of item.types){
// 				const blob = await item.getType(type);
// 				// console.log('blob: ', blob);
// 				if(blob.type.startsWith("image/")){
// 					// const objURL = window.URL.createObjectURL(blob);
// 					// console.log('objURL: ', objURL);
// 					data = blob;
// 				}
//       }
// 		}
// 		return data;
//   }catch(e){
// 		console.error(e.name, e.message);
// 		return false;
//   }
// }


function pasteBlob(_x) {
  return _pasteBlob.apply(this, arguments);
} // CHECK Permisson paste
// clipboard-write - granted by default


function _pasteBlob() {
  _pasteBlob = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
    var items, lng, i;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (e.clipboardData) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            items = e.clipboardData.items;

            if (items) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return");

          case 6:
            lng = items.length;
            i = 0;

          case 8:
            if (!(i < lng)) {
              _context.next = 15;
              break;
            }

            if (!(items[i].type.indexOf("image/") === -1)) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("continue", 12);

          case 11:
            return _context.abrupt("return", items[i].getAsFile());

          case 12:
            i++;
            _context.next = 8;
            break;

          case 15:
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log('Error pasteBlob e: ', _context.t0);
            return _context.abrupt("return", false);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));
  return _pasteBlob.apply(this, arguments);
}

function permissions() {
  return _permissions.apply(this, arguments);
}

function _permissions() {
  _permissions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var query;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return navigator.permissions.query({
              name: "clipboard-read",
              allowWithoutGesture: false
            });

          case 3:
            query = _context2.sent;
            // Will be 'granted', 'denied' or 'prompt':
            console.log(query.state); // Listen for changes to the permission state

            query.onchange = function () {
              console.log(query.state);
            };

            return _context2.abrupt("return", query);

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", false);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 9]]);
  }));
  return _permissions.apply(this, arguments);
}



/***/ })

}]);
//# sourceMappingURL=Devs~PostDetailPage~PostEdit.chunk.js.map