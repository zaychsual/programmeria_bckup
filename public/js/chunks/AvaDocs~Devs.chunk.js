(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["AvaDocs~Devs"],{

/***/ "./resources/js/src/components/q-ui-react/Ava.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Ava.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Ava; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useRef, useEffect }
// import { mapToCssModules } from '../q-react-bootstrap/utils';
// import { Cx, setClass, setAttr, isStr, cached } from '../../utils/Q.js'; // , hasClass
// https://codepen.io/andreaswik/pen/YjJqpK

var lightOrDark = Q.cached(function (color) {
  var r, g, b, hsp; // Check the format of the color, HEX or RGB?

  if (color.match(/^rgb/)) {
    // If HEX --> store the red, green, blue values in separate variables
    color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
    r = color[1];
    g = color[2];
    b = color[3];
  } else {
    // If RGB --> Convert it to HEX: http://gist.github.com/983661
    color = +("0x" + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));
    r = color >> 16;
    g = color >> 8 & 255;
    b = color & 255;
  } // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html


  hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b)); // Using the HSP value, determine whether the color is light or dark

  if (hsp > 127.5) return 'light';
  return 'dark';
}); // FROM https://github.com/chakra-ui/chakra-ui/blob/master/packages/chakra-ui/src/Avatar/index.js

var getInitials = function getInitials(name) {
  var no = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '?';
  // if(!name || !Q.isStr(name) || name.length < 1) return no;
  if (!name || !Q.isStr(name) || name === " " || name.length < 1) return no; // Destruct 

  var _name$split = name.split(" "),
      _name$split2 = _slicedToArray(_name$split, 2),
      first = _name$split2[0],
      last = _name$split2[1];

  if (first && last) {
    return "".concat(first[0]).concat(last[0]); // first.charAt(0)}${last.charAt(0)
  }

  return first[0]; // first.charAt(0)
}; // FROM https://github.com/segmentio/evergreen/blob/master/src/avatar/src/utils/getInitials.js
// function getInitials(name, fallback = '?'){
// if(!name || typeof name !== 'string') return fallback
// return name
// .replace(/\s+/, ' ')
// .split(' ') // Repeated spaces results in empty strings
// .slice(0, 2)
// .map(v => v && v[0].toUpperCase()) // Watch out for empty strings
// .join('')
// }
// FROM: chakra-ui


function str2Hex(str) {
  var no = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "5a6268";
  if (!str || str.length === 0) return no;
  var hash = 0,
      sl = str.length;

  for (var i = 0; i < sl; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
    hash = hash & hash;
  }

  var color = ""; // #

  for (var j = 0; j < 3; j++) {
    var val = hash >> j * 8 & 255;
    color += ("00" + val.toString(16)).substr(-2);
  }

  return color;
}

function Ava(_ref) {
  var inRef = _ref.inRef,
      _ref$w = _ref.w,
      w = _ref$w === void 0 ? 30 : _ref$w,
      _ref$h = _ref.h,
      h = _ref$h === void 0 ? 30 : _ref$h,
      src = _ref.src,
      _ref$alt = _ref.alt,
      alt = _ref$alt === void 0 ? " " : _ref$alt,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? "lazy" : _ref$loading,
      _ref$drag = _ref.drag,
      drag = _ref$drag === void 0 ? false : _ref$drag,
      className = _ref.className,
      wrapClass = _ref.wrapClass,
      wrapProps = _ref.wrapProps,
      bg = _ref.bg,
      circle = _ref.circle,
      round = _ref.round,
      thumb = _ref.thumb,
      contain = _ref.contain,
      onError = _ref.onError,
      onLoad = _ref.onLoad,
      etc = _objectWithoutProperties(_ref, ["inRef", "w", "h", "src", "alt", "loading", "drag", "className", "wrapClass", "wrapProps", "bg", "circle", "round", "thumb", "contain", "onError", "onLoad"]);

  // DEV OPTION: random color error image with initial alt
  // const randomColor = Math.floor(Math.random() * 16777215).toString(16);

  /* useEffect(() => {
  	// console.log('%cuseEffect in Ava','color:yellow;');
  }, []); */
  var removeLoadClass = function removeLoadClass(et) {
    Q.setClass(et.parentElement, 'ovhide ava-loader', 'remove'); // ovhide ava-loader
  };

  var Err = function Err(e) {
    var et = e.target;
    removeLoadClass(et);
    if (Q.hasClass(et, "text-dark")) setClass(et, "text-dark", "remove");
    if (Q.hasClass(et, "text-white")) setClass(et, "text-white", "remove"); // let setWidth = w ? w : h ? h : 50; // (et.offsetWidth || et.clientWidth);

    var fs = '--fs:calc(';

    if (isNaN(w)) {
      // setWidth
      var num = parseFloat(w);
      var unit = w.replace("" + num, ''); // `${num}`, '')

      fs += num + unit + " / 2.25);"; // 2.5 | `${num}${unit} / 2.25);`
    } else {
      fs += w + "px / 2.25);"; // 2.5 | `${w}px / 2.25);`
    } // const setAlt = et.alt || alt;// OPTION: if change alt (Directly in DOM)


    var trm = alt.trim();
    var color = bg ? bg.replace("#", "") : str2Hex(trm); // if(!parseFloat(et.getAttribute("width"))) et.width = setWidth; // OPTION

    Q.setAttr(et, {
      'aria-label': getInitials(trm),
      // alt | setAlt
      style: fs + "--bg:#" + color // alt | setAlt | randomColor

    });
    Q.setClass(et, lightOrDark(color) === "dark" ? "text-white" : "text-dark"); // DEV OPTION: Change src with svg
    // et.src = `data:image/svg+xml,%3Csvg width="100%25" height="100%25" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"%3E%3Cstyle%3E%0Asvg%7Bfont-family:sans-serif;font-size:1rem;text-anchor:middle%7D%0A%3C/style%3E%3Crect width="100%25" height="100%25" fill="%2355595c"%3E%3C/rect%3E%3Ctext x="50%25" y="50%25" fill="%23eceeef" dy=".35em"%3E${getInitials(alt)}%3C/text%3E%3C/svg%3E`;
    // FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
    // if(et.onerror) et.onerror = null;// DEV OPTION

    if (onError) onError(e);
    return; // null
  };

  var Load = function Load(e) {
    removeLoadClass(e.target);
    if (onLoad) onLoad(e);
  };

  var sameCx = {
    'rounded-circle': circle,
    'rounded': round
  }; // , cssModule

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, wrapProps, {
    className: // Q.Cx("img-frame ava-frame ava-loader", sameCx, frameClass)
    Q.Cx("img-frame ava-frame ovhide ava-loader", sameCx, wrapClass) // draggable={!!drag} // OPTION

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", _extends({}, etc, {
    ref: inRef,
    loading: loading // OPTIONS: native lazy load html
    ,
    width: w,
    height: h,
    src: Q.newURL(src).href // OPTION
    ,
    alt: alt,
    className: Q.Cx("ava", _objectSpread(_objectSpread({}, sameCx), {}, {
      // "rounded-pill": pill,
      // 'rounded-circle': circle,
      // 'rounded': round, 
      // 'img-fluid': fluid,
      'ava-thumb': thumb,
      // img-thumbnail
      'of-con': contain
    }), className),
    draggable: drag // !!drag
    ,
    onError: Err,
    onLoad: Load // onContextMenu={e => preventQ(e)}

  })));
} // Ava.defaultProps = {
// alt: ''
// };

/***/ })

}]);
//# sourceMappingURL=AvaDocs~Devs.chunk.js.map