(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PostEdit"],{

/***/ "./resources/js/src/components/q-ui-react/Placeholder.js":
/*!***************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Placeholder.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Placeholder; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { Fragment, useState, useEffect }
// import Btn from '../../components/q-ui-bootstrap/Btn';

function Placeholder(_ref) {
  var className = _ref.className,
      w = _ref.w,
      h = _ref.h,
      style = _ref.style,
      label = _ref.label,
      _ref$length = _ref.length,
      length = _ref$length === void 0 ? 1 : _ref$length,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "p" : _ref$type,
      etc = _objectWithoutProperties(_ref, ["className", "w", "h", "style", "label", "length", "type"]);

  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in Placeholder','color:yellow;');
  // }, []);
  // const As = "thumb form".includes(type) ? "div" : type; // type === "thumb" || type === "form"
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.toArray(Array.from({
    length: parseInt(length)
  }).map(function () {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, etc, {
      className: "holder ".concat(type).concat(className ? " ".concat(className) : ""),
      style: _objectSpread(_objectSpread({}, style), {}, {
        width: w,
        height: h
      }),
      "aria-label": label
    }));
  }));
}
/*
<Fragment>
	{Array.from({ length: parseInt(length) }).map((v, i) => 
		<div key={i} 
			{...etc}
			
			className={`holder ${type}${className ? ` ${className}` : ""}`}
			style={{
				...style, 
				width: w,
				height: h
			}} 
			
		/>
	)}
</Fragment>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/PostEdit.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/PostEdit.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PostEdit; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dropzone__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dropzone */ "./node_modules/react-dropzone/dist/es/index.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _parts_PostLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../parts/PostLayout */ "./resources/js/src/parts/PostLayout.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/q-ui-react/Img */ "./resources/js/src/components/q-ui-react/Img.js");
/* harmony import */ var _components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/q-ui-react/Placeholder */ "./resources/js/src/components/q-ui-react/Placeholder.js");
/* harmony import */ var _components_player_q_PlayerQ__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/player-q/PlayerQ */ "./resources/js/src/components/player-q/PlayerQ.js");
/* harmony import */ var _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../utils/file/fileRead */ "./resources/js/src/utils/file/fileRead.js");
/* harmony import */ var _utils_srt2vtt__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../utils/srt2vtt */ "./resources/js/src/utils/srt2vtt.js");
/* harmony import */ var _data_appData__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../data/appData */ "./resources/js/src/data/appData.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }


 // import { FormattedDateParts } from 'react-intl';
// import DOMPurify from 'dompurify';
// import { CircularProgressbar } from 'react-circular-progressbar';
// import 'react-circular-progressbar/dist/styles.css';

 // import PostDetailPage from './PostDetailPage';



 // import ModalQ from '../../../components/q-ui-react/ModalQ';





 // const EXTERNAL_VIDEO_KEY = "external-video";
// const EXTERNAL_IMG_KEY = "external-image";

function validateExternalUrl(val) {
  var urlReg = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; // /^((https?|ftp|file):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;

  var isUrl = urlReg.exec(val);

  if (isUrl) {
    var _isUrl$;

    var sUrl;

    if ((_isUrl$ = isUrl[0]) === null || _isUrl$ === void 0 ? void 0 : _isUrl$.startsWith("https://youtu.be/")) {
      sUrl = "https://www.youtube.com/embed" + isUrl[4].replace("/embed", "");
    } else {
      sUrl = isUrl[1] ? val : isUrl[4] ? "https://www.youtube.com/embed" + isUrl[4].replace("/embed", "") : 'https://' + isUrl[0];
    }

    return sUrl;
  } else {
    // const iframeReg =  /<iframe.*?src=\s*(["'])(.*?)\1/; //  /<iframe.*?src="(.*?)"/;
    var getSrc = /<iframe.*?src=\s*(["'])(.*?)\1/.exec(val);

    if (getSrc) {
      console.log(getSrc[2]);
      return getSrc[2];
    }

    return false;
  }
}

var DATE_NOW = function DATE_NOW() {
  return new Date(Date.now());
};

var DATE_OPTIONS = {
  weekday: "long",
  year: "numeric",
  month: "short",
  // long
  day: "numeric" // numeric | 2-digit

};
var INIT_DATE = DATE_NOW().toLocaleDateString(document.documentElement.lang, DATE_OPTIONS).split(",");
var INIT_DATA = {
  id: "MAKE_POST_INIT_DUMMY",
  // DATE_NOW().toLocaleDateString('en-GB', DATE_OPTIONS),
  // new Intl.DateTimeFormat('en-US', DATE_OPTIONS).format(DATE_NOW()), 
  created_at: INIT_DATE[1] + "," + INIT_DATE[2],
  title: "Make New Post",
  views: 0,
  like: 0,
  dislike: 0,
  mediaType: "video/mp4",
  // "video" | null
  media: {
    // poster: "",
    src: "/media/blank.mp4" // tracks: [
    // 	{kind:"subtitles", src:"/media/vtt/introduction-subtitle-en.vtt", label:"English", srcLang:"en", default:true},
    // 	{kind:"subtitles", src:"/media/vtt/introduction-subtitle-id.vtt", label:"Bahasa Indonesia", srcLang:"id"},
    // ]

  }
};
function PostEdit() {
  var _data$media2, _data$media3, _data$media4;

  // const fileRef = useRef(null);
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true),
      _useState2 = _slicedToArray(_useState, 2),
      load = _useState2[0],
      setLoad = _useState2[1]; // false


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(INIT_DATA),
      _useState4 = _slicedToArray(_useState3, 2),
      data = _useState4[0],
      setData = _useState4[1]; // const [metaData, setMetaData] = useState(null);


  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      externalMedia = _useState6[0],
      setExternalMedia = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      tmode = _useState8[0],
      setTmode = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      autoPlay = _useState10[0],
      setAutoPlay = _useState10[1]; // const [progressFile, setProgressFile] = useState(null);


  var onDrop = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (files) {
    console.log('onDrop files: ', files);
    var file = files[0]; // const blob = window.URL.createObjectURL(file);
    // window.URL.revokeObjectURL(this.src);

    if (file && (file.type.startsWith("video/") || file.type.startsWith("image/"))) {
      // let fSize = Q.bytes2Size(file.size);
      // console.log('file fSize: ', fSize);
      setData(_objectSpread(_objectSpread({}, data), {}, {
        id: Q.Qid(),
        mediaType: file.type,
        media: _objectSpread(_objectSpread({}, data.media), {}, {
          src: window.URL.createObjectURL(file)
        })
      })); // let fSize = Q.bytes2Size(file.size, false);
      // console.log('file fSize: ', Number(fSize));
      // if(Number(fSize) >= 1.5){
      // 	setData({
      // 		...data, 
      // 		id: Q.Qid(), 
      // 		mediaType: file.type,
      // 		media: {
      // 			...data.media,
      // 			src: window.URL.createObjectURL(file)
      // 		}
      // 	});
      // 	return;
      // }
      // let percent;
      // fileRead(file, {
      // 	onProgress: (e) => {
      // 		const { loaded, total } = e;
      // 		if (loaded && total) {
      // 			percent = Math.round((loaded / total) * 100);
      // 			console.log("Progress: ", percent);
      // 			setProgressFile(percent);
      // 		}
      // 	}
      // }).then(v => {
      // 	console.log('v: ', v);
      // 	let res = v.result;
      // 	if(res?.length <= 0){
      // 		res = window.URL.createObjectURL(file);
      // 	}
      // 	setData({
      // 		...data, 
      // 		id: Q.Qid(), 
      // 		mediaType: file.type,
      // 		media: {
      // 			...data.media,
      // 			src: res
      // 		}
      // 	});
      // 	if(res && percent === 100) setProgressFile(null);
      // }).catch(e => {
      // 	console.log(e);
      // });

      setExternalMedia(""); // Clear Insert URL
    } // setMetaData({
    // 	mime: file.type,
    // 	fileName: file.name,
    // 	// data: mediaUrl.split(',')[1] // imageUrl.substr(imageUrl.indexOf(',') + 1)
    // });

  }, []); // noDragEventsBubbling: true

  var _useDropzone = Object(react_dropzone__WEBPACK_IMPORTED_MODULE_1__["useDropzone"])({
    onDrop: onDrop,
    multiple: false,
    noClick: true,
    noKeyboard: true
  }),
      getRootProps = _useDropzone.getRootProps,
      getInputProps = _useDropzone.getInputProps,
      inputRef = _useDropzone.inputRef,
      isDragActive = _useDropzone.isDragActive; // useEffect(() => {
  // 	console.log('%cuseEffect in PostEdit','color:yellow;');
  // }, []);
  // CHECK Youtube video url: https://www.youtube.com/watch?v=VppULWpfJZo


  var onClickExternal = function onClickExternal() {
    var validVideoExt = validateExternalUrl(externalMedia);
    console.log('validVideoExt: ', validVideoExt); // console.log('data.media.src: ', data.media.src.startsWith('blob:'));

    if (validVideoExt || Q.isAbsoluteUrl(externalMedia)) {
      var _data$media, _data$media$src;

      // externalMedia.length > 0
      // Clean blob
      if ((_data$media = data.media) === null || _data$media === void 0 ? void 0 : (_data$media$src = _data$media.src) === null || _data$media$src === void 0 ? void 0 : _data$media$src.startsWith('blob:')) {
        // data.mediaType !== EXTERNAL_VIDEO_KEY
        window.URL.revokeObjectURL(data.media.src);
      }

      var mediaType = _data_appData__WEBPACK_IMPORTED_MODULE_11__["EXTERNAL_IMG_KEY"];

      if (validVideoExt && (validVideoExt.includes("youtube.com/") || validVideoExt.includes("youtu.be/"))) {
        mediaType = _data_appData__WEBPACK_IMPORTED_MODULE_11__["EXTERNAL_VIDEO_KEY"];
      }

      setData(_objectSpread(_objectSpread({}, data), {}, {
        id: Q.Qid(),
        mediaType: mediaType,
        media: _objectSpread(_objectSpread({}, data.media), {}, {
          src: validVideoExt || externalMedia
        })
      }));
      document.body.click();
    }
  };

  var onEnter = function onEnter(e) {
    if (e.target.value.length > 0 && e.key === "Enter") {
      onClickExternal();
    }
  };

  var renderAddMediaToool = function renderAddMediaToool(size) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
      size: size
    }, "Add media"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
      alignRight: true,
      className: "w-400"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: "dropdown-header"
    }, "Video / image URL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      as: "button",
      type: "button",
      className: "qi qi-upload q-mr" // id="btnUploadFile" 
      ,
      onClick: function onClick() {
        return inputRef.current.click();
      }
    }, "Upload file"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      className: "input-group input-group-sm py-2 px-3"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "search",
      className: "form-control",
      placeholder: "Insert URL" // id="extMediaInput"
      ,
      value: externalMedia,
      onChange: function onChange(e) {
        return setExternalMedia(e.target.value);
      },
      onKeyUp: onEnter
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "input-group-append"
    }, Q_appData.UA.browser.name === "Firefox" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
      As: "div",
      onClick: function onClick() {
        return setExternalMedia("");
      },
      kind: "light",
      className: "qi qi-close xx",
      hidden: externalMedia.length < 1
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
      As: "div",
      outline: true,
      onClick: onClickExternal
    }, "Insert")))));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, getRootProps(), {
    className: Q.Cx("position-relative post-edit", {
      "dropFile fullpage": isDragActive
    }),
    "aria-label": isDragActive ? "Drop file here" : undefined
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_parts_PostLayout__WEBPACK_IMPORTED_MODULE_3__["default"] // className="" 
  , {
    data: data,
    rowStyle: {
      minHeight: 'calc(100vh - 115px)'
    },
    append: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "p-2 bg-light border-top position-sticky b0 zi-3 mx-min15" // style={{ margin:'0 -15px -1rem' }} 

    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
      justify: "center",
      align: "center",
      className: "ml-1-next"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", getInputProps()), renderAddMediaToool("sm"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
      onClick: function onClick() {
        var setData = _objectSpread(_objectSpread({}, data), {}, {
          // created_at: new Date(Date.now()).toMysqlFormat()
          // created_at: new Intl.DateTimeFormat(document.documentElement.lang, {
          // 	weekday: 'long',
          // 	year: 'numeric',
          // 	month: 'numeric',
          // 	day: 'numeric',
          // 	hour: 'numeric',
          // 	minute: 'numeric',
          // 	second: 'numeric',
          // 	fractionalSecondDigits: 3,
          // 	hour12: true,
          // 	timeZone: 'UTC'
          // }).format(Date.UTC(Date.now()))
          created_at: Q.dateObj(Date.now()) // created_at: new Date(Date.now()).toISOString().slice(0, 19).replace('T', ' ')
          // created_at: new Date(Date.now()).toLocaleString('en-GB', { timeZone: 'UTC' })
          // created_at: <FormattedDateParts
          // 	value={new Date(Date.now())}
          // 	year="numeric"
          // 	month="long"
          // 	day="2-digit"
          // >
          // 	{parts => parts[0].value + parts[1].value + parts[2].value}
          // </FormattedDateParts>

        });

        console.log('setData: ', setData);
      },
      size: "sm"
    }, "Save")))
  }, (data === null || data === void 0 ? void 0 : (_data$media2 = data.media) === null || _data$media2 === void 0 ? void 0 : _data$media2.src) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-0 px-md-2 col-md-" + (tmode ? 12 : 8)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
    className: "mt-2 mr-3 position-absolute t0 r0 zi-5 ml-1-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    kind: "danger",
    className: "tip tipB qi qi-close",
    "aria-label": "Remove",
    onClick: function onClick() {
      setData(_objectSpread(_objectSpread({}, data), {}, {
        mediaType: null,
        media: {}
      }));
    }
  }), data.id !== "MAKE_POST_INIT_DUMMY" && data.mediaType.startsWith("video/") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    As: "label"
  }, "Add CC", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    hidden: true,
    onChange: function onChange(e) {
      var file = e.target.files[0];

      if (file && (file.type.startsWith("text/") || file.type === "")) {
        var src; // if(file.name.split(".").pop() === "srt"){
        // console.log("IS SRT");

        Object(_utils_file_fileRead__WEBPACK_IMPORTED_MODULE_9__["fileRead"])(file, {
          readAs: "Text"
        }).then(function (v) {
          // DOMPurify.sanitize(srt2vtt(v.result), {ALLOWED_TAGS: ['#text']})
          var vtt = file.name.split(".").pop() === "srt" ? Object(_utils_srt2vtt__WEBPACK_IMPORTED_MODULE_10__["default"])(v.result) : v.result;
          var blob = new Blob([vtt], {
            type: "text/vtt "
          }); // plain

          src = window.URL.createObjectURL(blob);
          console.log('v: ', v);
          console.log('vtt: ', vtt); // console.log('src: ', src);

          setLoad(false);
          setData(_objectSpread(_objectSpread({}, data), {}, {
            media: _objectSpread(_objectSpread({}, data.media), {}, {
              // src: dataPrev.src, 
              tracks: [{
                kind: "subtitles",
                src: src,
                label: "Indonesia",
                srcLang: "id",
                "default": true
              }]
            })
          }));
          setLoad(true);
        })["catch"](function (e) {
          console.log(e);
        }); // }
        // else{
        // 	console.log("IS VTT");
        // 	src = window.URL.createObjectURL(file);
        // 	setData({
        // 		...data, 
        // 		media: {
        // 			...data.media, 
        // 			tracks: [
        // 				{kind:"subtitles", src, label:"Bahasa Indonesia", srcLang:"id", default:true}
        // 			]
        // 		}
        // 	});
        // }
      }
    }
  })), renderAddMediaToool()), (load && (data === null || data === void 0 ? void 0 : data.mediaType.startsWith("video/")) || (data === null || data === void 0 ? void 0 : data.mediaType) === _data_appData__WEBPACK_IMPORTED_MODULE_11__["EXTERNAL_VIDEO_KEY"]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_player_q_PlayerQ__WEBPACK_IMPORTED_MODULE_8__["default"], {
    className: "bg-000 shadow-sm",
    config: {
      youtube: {
        playerVars: {
          // showinfo: 1,
          controls: 0,
          modestbranding: 1
        }
      }
    },
    url: data.media.src // fileConfig={data.media?.tracks ? { tracks: data.media.tracks } : undefined} 
    ,
    tracks: ((_data$media3 = data.media) === null || _data$media3 === void 0 ? void 0 : _data$media3.tracks) || undefined,
    poster: (_data$media4 = data.media) === null || _data$media4 === void 0 ? void 0 : _data$media4.poster,
    videoID: data.id,
    wrapStyle: {
      height: tmode ? 'calc(83vh - 48px)' : undefined
    } // 'calc(100vh - 242px)'
    ,
    enablePip: true,
    theaterMode: true,
    tmode: tmode,
    autoPlay: autoPlay,
    onTheaterMode: function onTheaterMode() {
      return setTmode(!tmode);
    },
    onSetAutoPlay: function onSetAutoPlay(c) {
      return setAutoPlay(c);
    }
  }), (load && (data === null || data === void 0 ? void 0 : data.mediaType.startsWith("image/")) || (data === null || data === void 0 ? void 0 : data.mediaType) === _data_appData__WEBPACK_IMPORTED_MODULE_11__["EXTERNAL_IMG_KEY"]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_6__["default"], {
    frame: true,
    WrapAs: "figure",
    fluid: true,
    frameClass: "mb-0 d-block",
    className: "w-100 bg-dark",
    style: {
      minHeight: 400
    },
    alt: data.title,
    src: data.media.src
  })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-8 px-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    className: "form-control-plaintext form-control-lg h-auto py-0 fw600 mt-3 mb-2",
    id: "postTitle",
    value: data === null || data === void 0 ? void 0 : data.title,
    onChange: function onChange(e) {
      setData(_objectSpread(_objectSpread({}, data), {}, {
        title: e.target.value
      }));
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
    wrap: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "fs-14"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("time", null, data === null || data === void 0 ? void 0 : data.created_at), " \u2022 ", data === null || data === void 0 ? void 0 : data.views, " views"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group mt-2 mt-lg-0 ml-0 ml-lg-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    kind: "light",
    className: "qi qi-thumbs-up"
  }, "0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    kind: "light",
    className: "qi qi-thumbs-down"
  }, "0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    kind: "light",
    className: "qi qi-share"
  }, "SHARE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
    kind: "light",
    className: "qi qi-plus"
  }, "SAVE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    bsPrefix: "btn-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    variant: "light",
    bsPrefix: "rounded-right qi qi-ellipsis-h"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "button",
    type: "button",
    className: "q-mr qi q-fw qi-flag"
  }, "Report"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "button",
    type: "button",
    className: "q-mr qi q-fw qi-file"
  }, "Open transcript"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-3 py-3 border-top"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-4 mt-1-next " + (tmode ? "mt-4" : "absolute-lg t0 r0")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_7__["default"], {
    type: "h3",
    className: "no-animate cauto pb-1"
  }), [1, 2, 3].map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: v,
      className: "media media-post"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_7__["default"], {
      w: 120,
      h: 65,
      className: "no-animate cauto mr-2"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "media-body lh-normal"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_7__["default"], {
      type: "h6",
      className: "no-animate cauto mb-1"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_7__["default"], {
      className: "no-animate cauto mb-0"
    })));
  }))));
}

/***/ }),

/***/ "./resources/js/src/parts/PostLayout.js":
/*!**********************************************!*\
  !*** ./resources/js/src/parts/PostLayout.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PostLayout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/q-ui-react/Placeholder */ "./resources/js/src/components/q-ui-react/Placeholder.js");
 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }




function PostLayout(_ref) {
  var className = _ref.className,
      data = _ref.data,
      rowStyle = _ref.rowStyle,
      append = _ref.append,
      children = _ref.children;
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in PostLayout','color:yellow;');
  // }, []);
  // pb-3
  //  + (tmode ? 12 : 8)
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Q.Cx("container pt-0 pt-md-3", className)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: rowStyle,
    className: "row mx-lg-n2 position-relative"
  }, data ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: data.title
  }), children) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "thumb",
    h: 370 // {screen.height / 2} // "55vh" 
    // className="card-img-top"
    // label="Post" 

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "h5",
    className: "w-50 my-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    w: "40%"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: "flex1 ml-5"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-2 col-md-4 mt-1-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: "h3",
    className: "pb-1"
  }), [1, 2, 3].map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: v,
      className: "media media-post"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
      w: 120,
      h: 65,
      className: "mr-2"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "media-body lh-normal"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
      type: "h6",
      className: "mb-1"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Placeholder__WEBPACK_IMPORTED_MODULE_3__["default"], {
      className: "mb-0"
    })));
  })))), append);
}
/*
<div className={Q.Cx("px-0 px-md-2", colMediaClass)}>

</div>
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/utils/srt2vtt.js":
/*!*******************************************!*\
  !*** ./resources/js/src/utils/srt2vtt.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return srt2vtt; });
// SRT to VTT
// https://github.com/silviapfeiffer/silviapfeiffer.github.io

/* function convert(val){
	// let input = document.getElementById("srt");
	// let output = document.getElementById("webvtt");
	// let srt = input.value;
	let webvtt = srt2webvtt(val);
	
	return webvtt;
	// output.innerHTML = "<textarea rows=20 cols=80>"
										 // + webvtt +
										 // "</textarea>";
} */
function convertSrtCue(caption) {
  // remove all html tags for security reasons
  //srt = srt.replace(/<[a-zA-Z\/][^>]*>/g, '');
  var cue = "";
  var s = caption.split(/\n/); // concatenate muilt-line string separated in array into one

  while (s.length > 3) {
    for (var i = 3; i < s.length; i++) {
      s[2] += "\n" + s[i];
    }

    s.splice(3, s.length - 3);
  }

  var line = 0; // detect identifier

  if (!s[0].match(/\d+:\d+:\d+/) && s[1].match(/\d+:\d+:\d+/)) {
    cue += s[0].match(/\w+/) + "\n";
    line += 1;
  } // get time strings


  if (s[line].match(/\d+:\d+:\d+/)) {
    // convert time string
    var m = s[1].match(/(\d+):(\d+):(\d+)(?:,(\d+))?\s*--?>\s*(\d+):(\d+):(\d+)(?:,(\d+))?/);

    if (m) {
      cue += m[1] + ":" + m[2] + ":" + m[3] + "." + m[4] + " --> " + m[5] + ":" + m[6] + ":" + m[7] + "." + m[8] + "\n";
      line += 1;
    } else {
      // Unrecognized timestring
      return "";
    }
  } else {
    // file format error or comment lines
    return "";
  } // get cue text


  if (s[line]) {
    cue += s[line] + "\n\n";
  }

  return cue;
} // srt2webvtt


function srt2vtt(val) {
  var tag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  // remove dos newlines
  var srt = val.replace(/\r+/g, ''); // trim white space start and end

  srt = srt.replace(/^\s+|\s+$/g, ''); // Q-CUSTOM

  if (!tag) {
    srt = srt.replace(/<[a-zA-Z\/][^>]*>/g, '');
  } // get cues


  var cuelist = srt.split('\n\n');
  var result = "";

  if (cuelist.length > 0) {
    result += "WEBVTT\n\n";

    for (var i = 0; i < cuelist.length; i = i + 1) {
      result += convertSrtCue(cuelist[i]);
    }
  }

  return result;
}

/***/ })

}]);
//# sourceMappingURL=PostEdit.chunk.js.map