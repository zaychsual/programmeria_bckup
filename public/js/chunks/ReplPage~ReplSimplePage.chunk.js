(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ReplPage~ReplSimplePage"],{

/***/ "./resources/js/src/apps/repl/parts/NavIde.js":
/*!****************************************************!*\
  !*** ./resources/js/src/apps/repl/parts/NavIde.js ***!
  \****************************************************/
/*! exports provided: NavIde */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavIde", function() { return NavIde; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
 // { useState, useEffect }
// import Flex from '../../../components/q-ui-react/Flex';
// import Loops from '../../../components/q-ui-react/Loops';

 // import BtnGroup from '../../../components/q-ui-react/BtnGroup';

function NavIde(_ref) {
  var theme = _ref.theme,
      className = _ref.className,
      btn = _ref.btn,
      active = _ref.active,
      children = _ref.children,
      onOpenSet = _ref.onOpenSet,
      onCloseTab = _ref.onCloseTab,
      _ref$tabs = _ref.tabs,
      tabs = _ref$tabs === void 0 ? [] : _ref$tabs,
      _ref$onClickTab = _ref.onClickTab,
      onClickTab = _ref$onClickTab === void 0 ? Q.noop : _ref$onClickTab;

  // const [data, setData] = useState();
  // useEffect(() => {
  // console.log('%cuseEffect in NavIde','color:yellow;');
  // }, []);
  var onWheelTab = function onWheelTab(e) {
    var et = e.target,
        c = '.ad-tabfile',
        s = e.deltaY > 1 ? 20 : -20; // console.log('onWheelTab deltaY s: ', e.deltaY, s);
    // et.scrollBy(s, 0)

    et.matches(c) ? et.scrollLeft += s : et.closest(c).scrollLeft += s;
  }; //  bg-strip


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
    className: Q.Cx("input-group input-group-sm flex-nowrap flexno ovhide zi-3 ad-ide-nav bg-" + theme, theme, className)
  }, (btn || onOpenSet) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-prepend mr-0"
  }, Q.isFunc(onOpenSet) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
    onClick: onOpenSet,
    kind: theme,
    className: "qi qi-cog",
    "aria-label": "Settings"
  }), btn), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    onWheel: onWheelTab //  btn-group-sm link-sm small ml-2
    ,
    className: "btn-group flex-nowrap flex1 mw-100 ml-1px-next q-scroll ad-tabfile"
  }, tabs.map(function (v, i) {
    return Q.isFunc(onCloseTab) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: Q.Cx("btn-group btn-group-sm flexno", {
        "active": active === i
      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
      onClick: function onClick(e) {
        return onClickTab(v, i, e);
      },
      kind: theme,
      className: "ad-tab-name",
      "aria-label": v.name
    }, v.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
      onClick: function onClick(e) {
        return onCloseTab(v, i, e);
      },
      kind: theme,
      className: "border-left-0 qi qi-close xx",
      "aria-label": "Close"
    })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_1__["default"], {
      key: i,
      onClick: function onClick(e) {
        return onClickTab(v, i, e);
      },
      kind: theme,
      size: "sm",
      active: active === i,
      className: "flexno ad-tab-name",
      "aria-label": v.name
    }, v.name);
  })), children);
}
/* NavIde.defaultProps = {
	tabs: [],
	// onOpenSet: Q.noop, // () => {}
	onClickTab: Q.noop
	// onCloseTab: noop // () => {}
}; */

/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/devs/react-split-pane-2/Pane.js":
/*!*********************************************************************!*\
  !*** ./resources/js/src/components/devs/react-split-pane-2/Pane.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SplitPane__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SplitPane */ "./resources/js/src/components/devs/react-split-pane-2/SplitPane.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // , { PureComponent }
// import P from 'prop-types';// PropTypes
// import prefixAll from 'inline-style-prefixer/static';

 // import { Cx } from '../../utils/Q';

function PaneStyle(_ref) {
  var split = _ref.split,
      initialSize = _ref.initialSize,
      size = _ref.size,
      minSize = _ref.minSize,
      maxSize = _ref.maxSize,
      resizersSize = _ref.resizersSize;
  var value = size || initialSize;
  var vertical = split === 'vertical';
  var styleProp = {
    minSize: vertical ? 'minWidth' : 'minHeight',
    maxSize: vertical ? 'maxWidth' : 'maxHeight',
    size: vertical ? 'width' : 'height'
  };
  var style = {// display: 'flex',
    // outline: 'none'
  };
  style[styleProp.minSize] = Object(_SplitPane__WEBPACK_IMPORTED_MODULE_1__["convertSizeToCssValue"])(minSize, resizersSize);
  style[styleProp.maxSize] = Object(_SplitPane__WEBPACK_IMPORTED_MODULE_1__["convertSizeToCssValue"])(maxSize, resizersSize);

  switch (Object(_SplitPane__WEBPACK_IMPORTED_MODULE_1__["getUnit"])(value)) {
    case 'ratio':
      style.flex = value;
      break;

    case '%':
    case 'px':
      style.flexGrow = 0;
      style[styleProp.size] = Object(_SplitPane__WEBPACK_IMPORTED_MODULE_1__["convertSizeToCssValue"])(value, resizersSize);
      break;

    default:
      break;
  }

  return style;
}

var Pane = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(Pane, _React$PureComponent);

  var _super = _createSuper(Pane);

  function Pane() {
    var _this;

    _classCallCheck(this, Pane);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "setRef", function (n) {
      return _this.props.innerRef(_this.props.index, n);
    });

    return _this;
  }

  _createClass(Pane, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          theme = _this$props.theme,
          className = _this$props.className,
          id = _this$props.id;
      var prefixedStyle = PaneStyle(this.props); // prefixAll(PaneStyle(this.props));

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        ref: this.setRef,
        id: id // Q-CUSTOM
        ,
        className: Q.Cx("Pane", theme, className) // 
        ,
        style: prefixedStyle
      }, children);
    }
  }]);

  return Pane;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.PureComponent); // Pane.propTypes = {
// children: P.node,
// innerRef: P.func,
// index: P.number,
// className: P.string,
// initialSize: P.oneOfType([P.string, P.number]),
// minSize: P.string,
// maxSize: P.string
// };


Pane.defaultProps = {
  initialSize: '1',
  split: 'vertical',
  minSize: '0',
  maxSize: '100%'
};
/* harmony default export */ __webpack_exports__["default"] = (Pane);

/***/ }),

/***/ "./resources/js/src/components/devs/react-split-pane-2/Resizer.js":
/*!************************************************************************!*\
  !*** ./resources/js/src/components/devs/react-split-pane-2/Resizer.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Resizer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

 // , { Component }
// import styled from 'styled-components';
// import { Cx } from '../../utils/Q';
// const Wrapper = styled.div`
// background: #000;
// opacity: 0.2;
// z-index: 1;
// box-sizing: border-box;
// background-clip: padding-box;
// `;
// :hover {
// transition: all 2s ease;
// }
// const HorizontalWrapper = styled(Wrapper)`
// height: 11px;
// margin: -5px 0;
// border-top: 5px solid rgba(255, 255, 255, 0);
// border-bottom: 5px solid rgba(255, 255, 255, 0);
// cursor: row-resize;
// width: 100%;
// :hover {
// border-top: 5px solid rgba(0, 0, 0, 0.5);
// border-bottom: 5px solid rgba(0, 0, 0, 0.5);
// }
// .disabled {
// cursor: not-allowed;
// }
// .disabled:hover {
// border-color: transparent;
// }
// `;
// const VerticalWrapper = styled(Wrapper)`
// width: 11px;
// margin: 0 -5px;
// border-left: 5px solid rgba(255, 255, 255, 0);
// border-right: 5px solid rgba(255, 255, 255, 0);
// cursor: col-resize;
// :hover {
// border-left: 5px solid rgba(0, 0, 0, 0.5);
// border-right: 5px solid rgba(0, 0, 0, 0.5);
// }
// .disabled {
// cursor: not-allowed;
// }
// .disabled:hover {
// border-color: transparent;
// }
// `;

var Resizer = /*#__PURE__*/function (_React$Component) {
  _inherits(Resizer, _React$Component);

  var _super = _createSuper(Resizer);

  function Resizer() {
    _classCallCheck(this, Resizer);

    return _super.apply(this, arguments);
  }

  _createClass(Resizer, [{
    key: "render",
    // componentDidMount(){
    // const {onDoubleClick} = this.props;
    // console.log(this.resizer);// 
    // if(onDoubleClick && this.resizer){
    // this.resizer.addEventListener('dblclick', onDoubleClick);
    // }
    // }
    value: function render() {
      var _this = this;

      var _this$props = this.props,
          index = _this$props.index,
          _this$props$split = _this$props.split,
          split = _this$props$split === void 0 ? 'vertical' : _this$props$split,
          active = _this$props.active,
          className = _this$props.className,
          _this$props$onClick = _this$props.onClick,
          _onClick = _this$props$onClick === void 0 ? function () {} : _this$props$onClick,
          _this$props$onDoubleC = _this$props.onDoubleClick,
          _onDoubleClick = _this$props$onDoubleC === void 0 ? function () {} : _this$props$onDoubleC,
          _this$props$onMouseDo = _this$props.onMouseDown,
          _onMouseDown = _this$props$onMouseDo === void 0 ? function () {} : _this$props$onMouseDo,
          _this$props$onTouchEn = _this$props.onTouchEnd,
          _onTouchEnd = _this$props$onTouchEn === void 0 ? function () {} : _this$props$onTouchEn,
          _this$props$onTouchSt = _this$props.onTouchStart,
          _onTouchStart = _this$props$onTouchSt === void 0 ? function () {} : _this$props$onTouchSt;

      var props = {
        ref: function ref(_) {
          return _this.resizer = _;
        },
        // 'data-attribute': split,
        // 'data-type': 'Resizer',
        onMouseDown: function onMouseDown(e) {
          return _onMouseDown(e, index);
        },
        onTouchStart: function onTouchStart(e) {
          e.preventDefault();

          _onTouchStart(e, index);
        },
        onTouchEnd: function onTouchEnd(e) {
          e.preventDefault();

          _onTouchEnd(e, index);
        },
        onClick: function onClick(e) {
          if (_onClick) {
            e.preventDefault();

            _onClick(e, index);
          }
        },
        onDoubleClick: function onDoubleClick(e) {
          if (_onDoubleClick) {
            e.preventDefault();

            _onDoubleClick(e, index);
          }
        }
      };
      return split === 'vertical' ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, props, {
        className: Q.Cx("Resizer vertical", className, {
          'active': active
        }),
        role: "presentation",
        tabIndex: "-1"
      })) // VerticalWrapper
      :
      /*#__PURE__*/
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, props, {
        className: Q.Cx("Resizer horizontal", className, {
          'active': active
        }),
        role: "presentation",
        tabIndex: "-1"
      })); // HorizontalWrapper
    }
  }]);

  return Resizer;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component); // export default Resizer;




/***/ }),

/***/ "./resources/js/src/components/devs/react-split-pane-2/SplitPane.js":
/*!**************************************************************************!*\
  !*** ./resources/js/src/components/devs/react-split-pane-2/SplitPane.js ***!
  \**************************************************************************/
/*! exports provided: getUnit, convertSizeToCssValue, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUnit", function() { return getUnit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertSizeToCssValue", function() { return convertSizeToCssValue; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Resizer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Resizer */ "./resources/js/src/components/devs/react-split-pane-2/Resizer.js");
/* harmony import */ var _Pane__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Pane */ "./resources/js/src/components/devs/react-split-pane-2/Pane.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // , { Component, cloneElement }
// import P from 'prop-types';// PropTypes
// import glamorous from 'glamorous';


 // import { Cx } from '../../utils/Q';// setClass

var DEFAULT_PANE_SIZE = '1';
var DEFAULT_PANE_MIN_SIZE = '0';
var DEFAULT_PANE_MAX_SIZE = '100%'; // const ColumnStyle = glamorous.div({
// display: 'flex',
// height: '100%',
// flexDirection: 'column',
// flex: 1,
// outline: 'none',
// overflow: 'hidden',
// userSelect: 'text'
// });
// const ColumnStyle = {
// display: 'flex',
// height: '100%',
// flexDirection: 'column',
// flex: 1,
// outline: 'none',
// overflow: 'hidden',
// userSelect: 'text'
// };
// const RowStyle = glamorous.div({
// display: 'flex',
// height: '100%',
// flexDirection: 'row',
// flex: 1,
// outline: 'none',
// overflow: 'hidden',
// userSelect: 'text'
// });
// const RowStyle = {
// display: 'flex',
// height: '100%',
// flexDirection: 'row',
// flex: 1,
// outline: 'none',
// overflow: 'hidden',
// userSelect: 'text'
// };

function convert(str, size) {
  var tokens = str.match(/([0-9]+)([px|%]*)/);
  var value = tokens[1];
  var unit = tokens[2];
  return toPx(value, unit, size);
}

function toPx(value) {
  var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'px';
  var size = arguments.length > 2 ? arguments[2] : undefined;

  switch (unit) {
    case '%':
      {
        return +(size * value / 100).toFixed(2);
      }

    default:
      {
        return +value;
      }
  }
}

function removeNullChildren(children) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.toArray(children).filter(function (c) {
    return c;
  });
}

function getUnit(size) {
  if (size.endsWith('px')) return 'px';
  if (size.endsWith('%')) return '%';
  return 'ratio';
}
function convertSizeToCssValue(value, resizersSize) {
  if (getUnit(value) !== '%') return value;
  if (!resizersSize) return value;
  var idx = value.search('%');
  var percent = value.slice(0, idx) / 100;
  if (percent === 0) return value;
  return "calc(".concat(value, " - ").concat(resizersSize, "px*").concat(percent, ")");
}

function convertToUnit(size, unit, containerSize) {
  switch (unit) {
    case '%':
      return "".concat((size / containerSize * 100).toFixed(2), "%");

    case 'px':
      return "".concat(size.toFixed(2), "px");

    case 'ratio':
      return (size * 100).toFixed(0);

    default:
      break;
  }
}

var SplitPane = /*#__PURE__*/function (_React$Component) {
  _inherits(SplitPane, _React$Component);

  var _super = _createSuper(SplitPane);

  function SplitPane(props) {
    var _this;

    _classCallCheck(this, SplitPane);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onMouseDown", function (e, resizerIndex) {
      if (e.button !== 0) return;
      e.preventDefault();

      _this.onDown(resizerIndex, e.clientX, e.clientY);
    });

    _defineProperty(_assertThisInitialized(_this), "onTouchStart", function (e, resizerIndex) {
      e.preventDefault();
      var _e$touches$ = e.touches[0],
          clientX = _e$touches$.clientX,
          clientY = _e$touches$.clientY;

      _this.onDown(resizerIndex, clientX, clientY);
    });

    _defineProperty(_assertThisInitialized(_this), "onDown", function (resizerIndex, clientX, clientY) {
      var _this$props = _this.props,
          allowResize = _this$props.allowResize,
          onResizeStart = _this$props.onResizeStart; // , split

      if (!allowResize) return;
      _this.resizerIndex = resizerIndex;
      _this.dimensionsSnapshot = _this.getDimensionsSnapshot(_this.props);
      _this.startClientX = clientX;
      _this.startClientY = clientY;
      document.addEventListener('mousemove', _this.onMouseMove);
      document.addEventListener('mouseup', _this.onMouseUp);
      document.addEventListener('touchmove', _this.onTouchMove);
      document.addEventListener('touchend', _this.onMouseUp);
      document.addEventListener('touchcancel', _this.onMouseUp); // setClass(document.body, this.cursor);
      // setClass(document.body, 'active');
      // setClass(e.target, 'active');

      _this.setState({
        active: true
      });

      if (onResizeStart) onResizeStart();
    });

    _defineProperty(_assertThisInitialized(_this), "onMouseMove", function (e) {
      e.preventDefault();

      _this.onMove(e.clientX, e.clientY);
    });

    _defineProperty(_assertThisInitialized(_this), "onTouchMove", function (e) {
      e.preventDefault();
      var _e$touches$2 = e.touches[0],
          clientX = _e$touches$2.clientX,
          clientY = _e$touches$2.clientY;

      _this.onMove(clientX, clientY);
    });

    _defineProperty(_assertThisInitialized(_this), "onMouseUp", function (e) {
      e.preventDefault();
      document.removeEventListener('mouseup', _this.onMouseUp);
      document.removeEventListener('mousemove', _this.onMouseMove);
      document.removeEventListener('touchmove', _this.onTouchMove);
      document.removeEventListener('touchend', _this.onMouseUp);
      document.addEventListener('touchcancel', _this.onMouseUp); // setClass(document.body, this.cursor, 'remove');
      // setClass(document.body, 'active', 'remove');
      // setClass(e.target, 'active', 'remove');

      _this.setState({
        active: false
      });

      if (_this.props.onResizeEnd) _this.props.onResizeEnd(_this.state.sizes);
    });

    _defineProperty(_assertThisInitialized(_this), "setPaneRef", function (idx, el) {
      if (!_this.paneElements) _this.paneElements = [];
      _this.paneElements[idx] = el;
    });

    _this.state = {
      sizes: _this.getPanePropSize(props),
      active: false // Q-CUSTOM

    }; // Q-CUSTOM
    // this.cursor = props.split === 'vertical' ? 'col-resize':'row-resize';
    // this.update = null;// false

    return _this;
  } // componentWillReceiveProps(nextProps) {
  // this.setState({sizes: this.getPanePropSize(nextProps)});
  // }

  /* shouldComponentUpdate(nextProps){// nextState
  	const {size} = this.props;// initialSize
  	if(size && size !== nextProps.size){
  		this.update = nextProps;
  		return true;
  	}
  	// else{
  		// this.update = null;
  		// return false;
  	// }
  	return false;
  }
  
  componentDidUpdate(prevProps){
  	// Typical usage (don't forget to compare props):
  	// if(this.props.userID !== prevProps.userID) {
  		// this.fetchData(this.props.userID);
  	// }
  	const {size} = this.props;
  	if(size && this.update){
  		this.setState({sizes: this.getPanePropSize(this.update)});
  	}
  	// if(size && size !== prevProps.size){
  		
  	// }
  } */


  _createClass(SplitPane, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener('mouseup', this.onMouseUp);
      document.removeEventListener('mousemove', this.onMouseMove);
      document.removeEventListener('touchmove', this.onTouchMove);
      document.removeEventListener('touchend', this.onMouseUp);
    }
  }, {
    key: "getDimensionsSnapshot",
    value: function getDimensionsSnapshot(props) {
      var split = props.split;
      var paneDimensions = this.getPaneDimensions();
      var splitPaneDimensions = this.splitPane.getBoundingClientRect();
      var minSizes = this.getPanePropMinMaxSize(props, 'minSize');
      var maxSizes = this.getPanePropMinMaxSize(props, 'maxSize');
      var resizersSize = this.getResizersSize(removeNullChildren(this.props.children));
      var splitPaneSizePx = split === 'vertical' ? splitPaneDimensions.width - resizersSize : splitPaneDimensions.height - resizersSize;
      var minSizesPx = minSizes.map(function (s) {
        return convert(s, splitPaneSizePx);
      });
      var maxSizesPx = maxSizes.map(function (s) {
        return convert(s, splitPaneSizePx);
      });
      var sizesPx = paneDimensions.map(function (d) {
        return split === 'vertical' ? d.width : d.height;
      });
      return {
        resizersSize: resizersSize,
        paneDimensions: paneDimensions,
        splitPaneSizePx: splitPaneSizePx,
        minSizesPx: minSizesPx,
        maxSizesPx: maxSizesPx,
        sizesPx: sizesPx
      };
    }
  }, {
    key: "getPanePropSize",
    value: function getPanePropSize(props) {
      return removeNullChildren(props.children).map(function (child) {
        var value = child.props['size'] || child.props['initialSize'];
        if (value === undefined) return DEFAULT_PANE_SIZE;
        return String(value);
      });
    }
  }, {
    key: "getPanePropMinMaxSize",
    value: function getPanePropMinMaxSize(props, key) {
      return removeNullChildren(props.children).map(function (child) {
        var value = child.props[key];
        if (value === undefined) return key === 'maxSize' ? DEFAULT_PANE_MAX_SIZE : DEFAULT_PANE_MIN_SIZE;
        return value;
      });
    }
  }, {
    key: "getPaneDimensions",
    value: function getPaneDimensions() {
      return this.paneElements.filter(function (el) {
        return el;
      }).map(function (el) {
        return el.getBoundingClientRect();
      });
    }
  }, {
    key: "getSizes",
    value: function getSizes() {
      return this.state.sizes;
    }
  }, {
    key: "onMove",
    value: function onMove(clientX, clientY) {
      var _this$props2 = this.props,
          split = _this$props2.split,
          onChange = _this$props2.onChange;
      var resizerIndex = this.resizerIndex;
      var _this$dimensionsSnaps = this.dimensionsSnapshot,
          sizesPx = _this$dimensionsSnaps.sizesPx,
          minSizesPx = _this$dimensionsSnaps.minSizesPx,
          maxSizesPx = _this$dimensionsSnaps.maxSizesPx,
          splitPaneSizePx = _this$dimensionsSnaps.splitPaneSizePx,
          paneDimensions = _this$dimensionsSnaps.paneDimensions;
      var sizeDim = split === 'vertical' ? 'width' : 'height';
      var primary = paneDimensions[resizerIndex];
      var secondary = paneDimensions[resizerIndex + 1];
      var maxSize = primary[sizeDim] + secondary[sizeDim];
      var primaryMinSizePx = minSizesPx[resizerIndex];
      var secondaryMinSizePx = minSizesPx[resizerIndex + 1];
      var primaryMaxSizePx = Math.min(maxSizesPx[resizerIndex], maxSize);
      var secondaryMaxSizePx = Math.min(maxSizesPx[resizerIndex + 1], maxSize);
      var moveOffset = split === 'vertical' ? this.startClientX - clientX : this.startClientY - clientY;
      var primarySizePx = primary[sizeDim] - moveOffset;
      var secondarySizePx = secondary[sizeDim] + moveOffset;
      var primaryHasReachedLimit = false;
      var secondaryHasReachedLimit = false;

      if (primarySizePx < primaryMinSizePx) {
        primarySizePx = primaryMinSizePx;
        primaryHasReachedLimit = true;
      } else if (primarySizePx > primaryMaxSizePx) {
        primarySizePx = primaryMaxSizePx;
        primaryHasReachedLimit = true;
      }

      if (secondarySizePx < secondaryMinSizePx) {
        secondarySizePx = secondaryMinSizePx;
        secondaryHasReachedLimit = true;
      } else if (secondarySizePx > secondaryMaxSizePx) {
        secondarySizePx = secondaryMaxSizePx;
        secondaryHasReachedLimit = true;
      }

      if (primaryHasReachedLimit) {
        secondarySizePx = primary[sizeDim] + secondary[sizeDim] - primarySizePx;
      } else if (secondaryHasReachedLimit) {
        primarySizePx = primary[sizeDim] + secondary[sizeDim] - secondarySizePx;
      }

      sizesPx[resizerIndex] = primarySizePx;
      sizesPx[resizerIndex + 1] = secondarySizePx;
      var sizes = this.getSizes().concat();
      var updateRatio;
      [primarySizePx, secondarySizePx].forEach(function (paneSize, idx) {
        var unit = getUnit(sizes[resizerIndex + idx]);

        if (unit !== 'ratio') {
          sizes[resizerIndex + idx] = convertToUnit(paneSize, unit, splitPaneSizePx);
        } else {
          updateRatio = true;
        }
      });

      if (updateRatio) {
        var ratioCount = 0;
        var lastRatioIdx;
        sizes = sizes.map(function (size, idx) {
          if (getUnit(size) === 'ratio') {
            ratioCount++;
            lastRatioIdx = idx;
            return convertToUnit(sizesPx[idx], 'ratio');
          }

          return size;
        });
        if (ratioCount === 1) sizes[lastRatioIdx] = '1';
      }

      onChange && onChange(sizes);
      this.setState({
        sizes: sizes
      });
    }
  }, {
    key: "getResizersSize",
    value: function getResizersSize(children) {
      return (children.length - 1) * this.props.resizerSize;
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          children = _this$props3.children,
          className = _this$props3.className,
          split = _this$props3.split,
          style = _this$props3.style,
          theme = _this$props3.theme,
          allowResize = _this$props3.allowResize,
          onDoubleClick = _this$props3.onDoubleClick,
          resizeClass = _this$props3.resizeClass; // 

      var notNullChildren = removeNullChildren(children); // this.props.children

      var sizes = this.getSizes();
      var resizersSize = this.getResizersSize(notNullChildren);
      var elements = notNullChildren.reduce(function (acc, child, idx) {
        var pane;
        var resizerIndex = idx - 1;
        var isPane = child.type === _Pane__WEBPACK_IMPORTED_MODULE_2__["default"];
        var paneProps = {
          index: idx,
          // 'data-type': 'Pane',
          split: split,
          key: "Pane-" + idx,
          innerRef: _this2.setPaneRef,
          resizersSize: resizersSize,
          size: sizes[idx]
        };

        if (isPane) {
          pane = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(child, paneProps);
        } else {
          pane = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Pane__WEBPACK_IMPORTED_MODULE_2__["default"], paneProps, child);
        }

        if (acc.length === 0 || !allowResize) {
          return [].concat(_toConsumableArray(acc), [pane]);
        } else {
          var resizer = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Resizer__WEBPACK_IMPORTED_MODULE_1__["default"], {
            index: resizerIndex,
            key: "Resizer-" + resizerIndex,
            split: split,
            active: _this2.state.active,
            onMouseDown: _this2.onMouseDown,
            onTouchStart: _this2.onTouchStart,
            onDoubleClick: onDoubleClick // Q-CUSTOM
            ,
            className: resizeClass // Q-CUSTOM

          });
          return [].concat(_toConsumableArray(acc), [resizer, pane]);
        }
      }, []); // const StyleComponent = split === 'vertical' ? RowStyle : ColumnStyle;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        // StyleComponent 
        // style={split === 'vertical' ? {...RowStyle, ...style} : {...ColumnStyle, ...style}} 
        style: style,
        className: Q.Cx("SplitPane", split, theme, className) // 
        // data-type="SplitPane" 
        // data-split={split} 
        // innerRef={el => {
        ,
        ref: function ref(n) {
          return _this2.splitPane = n;
        }
      }, elements);
    }
  }]);

  return SplitPane;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

SplitPane.defaultProps = {
  split: 'vertical',
  resizerSize: 1,
  allowResize: true,
  theme: "light"
}; // SplitPane.propTypes = {
// children: P.arrayOf(P.node).isRequired,
// className: P.string,
// split: P.oneOf(['vertical', 'horizontal']),
// resizerSize: P.number,
// onChange: P.func,
// onResizeStart: P.func,
// onResizeEnd: P.func
// };

/* harmony default export */ __webpack_exports__["default"] = (SplitPane);

/***/ }),

/***/ "./resources/js/src/data/monaco.js":
/*!*****************************************!*\
  !*** ./resources/js/src/data/monaco.js ***!
  \*****************************************/
/*! exports provided: MONACO_THEMES, MONACO_LANGS, MONACO_OPTIONS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MONACO_THEMES", function() { return MONACO_THEMES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MONACO_LANGS", function() { return MONACO_LANGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MONACO_OPTIONS", function() { return MONACO_OPTIONS; });
// SETUP
var MONACO_THEMES = ["dark", "light"]; // defaultThemes
// const monacoThemes = [
//   "Active4D",
//   "All Hallows Eve",
//   "Amy",
//   "Birds of Paradise",
//   "Blackboard",
//   "Brilliance Black",
//   "Brilliance Dull",
//   "Chrome DevTools",
//   "Clouds Midnight",
//   "Clouds",
//   "Cobalt",
//   "Dawn",
//   "Dominion Day",
//   "Dreamweaver",
//   "Eiffel",
//   "Espresso Libre",
//   "GitHub",
//   "IDLE",
//   "Katzenmilch",
//   "Kuroir Theme",
//   "LAZY",
//   "MagicWB (Amiga)",
//   "Merbivore Soft",
//   "Merbivore",
//   "Monokai Bright",
//   "Monokai",
//   "Night Owl",
//   "Oceanic Next",
//   "Pastels on Dark",
//   "Slush and Poppies",
//   "Solarized-dark",
//   "Solarized-light",
//   "SpaceCadet",
//   "Sunburst",
//   "Textmate (Mac Classic)",
//   "Tomorrow-Night-Blue",
//   "Tomorrow-Night-Bright",
//   "Tomorrow-Night-Eighties",
//   "Tomorrow-Night",
//   "Tomorrow",
//   "Twilight",
//   "Upstream Sunburst",
//   "Vibrant Ink",
//   "Xcode_default",
//   "Zenburnesque",
//   "iPlastic",
//   "idleFingers",
//   "krTheme",
//   "monoindustrial",
//   "themelist",
// ];
// monacoLangs

var MONACO_LANGS = [{
  id: 1,
  name: "apex",
  type: "text/x-java",
  ex: "cls"
}, // {id:2, name:"azcli", type:"", ex:""}, // ???
// {id:3, name:"bat", type:"application/bat", ex:"bat"},
{
  id: 4,
  name: "c",
  type: "text/x-csrc",
  ex: "c"
}, {
  id: 5,
  name: "clojure",
  type: "text/x-clojure",
  ex: "clj"
}, {
  id: 6,
  name: "coffeescript",
  type: "text/x-coffeescript",
  ex: "cson"
}, {
  id: 7,
  name: "cpp",
  type: "text/x-c++src",
  ex: "cpp"
}, {
  id: 8,
  name: "csharp",
  type: "text/x-csharp",
  ex: "cs"
}, // {id:9, name:"csp", type:"", ex:"csp"},
{
  id: 10,
  name: "css",
  type: "text/css",
  ex: "css"
}, // {id:11, name:"dockerfile", type:"text/x-dockerfile", ex:"dockerfile"},
// {id:12, name:"fsharp", type:"text/x-fsharp", ex:"fs"},
{
  id: 13,
  name: "go",
  type: "text/x-go",
  ex: "go"
}, {
  id: 14,
  name: "graphql",
  type: "text/plain",
  ex: "graphql"
}, // ex:"gql"
// {id:15, name:"handlebars", type:"", ex:"hbs"},
{
  id: 16,
  name: "html",
  type: "text/html",
  ex: "html"
}, {
  id: 17,
  name: "ini",
  type: "text/x-properties",
  ex: "ini"
}, {
  id: 18,
  name: "java",
  type: "text/x-java",
  ex: "java"
}, {
  id: 19,
  name: "javascript",
  type: "text/javascript",
  ex: "js"
}, {
  id: 20,
  name: "json",
  type: "application/json",
  ex: "json"
}, {
  id: 21,
  name: "kotlin",
  type: "text/x-kotlin",
  ex: "kt"
}, {
  id: 22,
  name: "less",
  type: "text/css",
  ex: "less"
}, // {id:23, name:"lua", type:"text/x-lua", ex:"lua"},
{
  id: 24,
  name: "markdown",
  type: "text/x-gfm",
  ex: "md"
}, // {id:25, name:"msdax", type:"", ex:""}, // ???
// {id:26, name:"mysql", type:"text/x-sql", ex:"sql"},
{
  id: 27,
  name: "objective-c",
  type: "text/x-objectivec",
  ex: "m"
}, {
  id: 28,
  name: "pascal",
  type: "text/x-pascal",
  ex: "pas"
}, {
  id: 29,
  name: "perl",
  type: "text/x-perl",
  ex: "pl"
}, // {id:30, name:"pgsql", type:"text/x-sql", ex:""}, // ???
{
  id: 31,
  name: "php",
  type: "application/x-httpd-php",
  ex: "php"
}, {
  id: 32,
  name: "plaintext",
  type: "text/plain",
  ex: "txt"
}, // {id:33, name:"postiats", type:"", ex:""}, // ???
// {id:34, name:"powerquery", type:"", ex:""}, // ???
{
  id: 35,
  name: "powershell",
  type: "application/x-powershell",
  ex: "ps1"
}, {
  id: 36,
  name: "pug",
  type: "text/x-pug",
  ex: "jade"
}, {
  id: 37,
  name: "python",
  type: "text/x-python",
  ex: "py"
}, {
  id: 38,
  name: "r",
  type: "text/x-rsrc",
  ex: "r"
}, // {id:39, name:"razor", type:"", ex:""}, // ???
// {id:40, name:"redis", type:"", ex:""}, // ???
// {id:41, name:"redshift", type:"", ex:""}, // ???
{
  id: 42,
  name: "ruby",
  type: "text/x-ruby",
  ex: "rb"
}, // {id:43, name:"rust", type:"", ex:""}, // ???
// {id:44, name:"sb", type:"", ex:""}, // ???
{
  id: 45,
  name: "scheme",
  type: "text/x-scheme",
  ex: "scm"
}, {
  id: 46,
  name: "scss",
  type: "text/x-scss",
  ex: "scss"
}, {
  id: 47,
  name: "shell",
  type: "text/x-sh",
  ex: "sh"
}, // {id:48, name:"sol", type:"", ex:""}, // ???
{
  id: 49,
  name: "sql",
  type: "",
  ex: "sql"
}, // type:"text/x-sql"
// {id:50, name:"st", type:"", ex:""}, // ???
{
  id: 51,
  name: "swift",
  type: "text/x-swift",
  ex: "swift"
}, // {id:52, name:"tcl", type:"", ex:""}, // ???
{
  id: 53,
  name: "typescript",
  type: "text/plain",
  ex: "ts"
}, {
  id: 54,
  name: "vb",
  type: "text/x-vb",
  ex: "vb"
}, {
  id: 55,
  name: "xml",
  type: "text/xml",
  ex: "xml"
}, {
  id: 56,
  name: "yaml",
  type: "text/x-yaml",
  ex: "yml"
}];
var MONACO_OPTIONS = {
  // Consolas, "Courier New", monospace
  fontFamily: 'Monaco, SFMono-Regular, Menlo, Consolas, "Liberation Mono", "Courier New", monospace',
  fontSize: 12,
  // 14
  tabSize: 2,
  // tabSize
  // dragAndDrop: false,
  minimap: {
    enabled: false // Defaults = true
    // side: minimapSide, // Default = right | left
    // showSlider: 'always', // Default = mouseover
    // renderCharacters: false, // Default =  true

  } // colorDecorators: false, // Css color preview
  // readOnly: true,
  // mouseWheelZoom: zoomIde

};
/** END: user define object */

/* function setDefineWindow(range, mon){
	// returning a static list of proposals, not even looking at the prefix (filtering is done by the Monaco editor),
	// here you could do a server side lookup
	// return [
		// {
			// label: '"lodash"',
			// kind: mon.languages.CompletionItemKind.Function,
			// documentation: "The Lodash library exported as Node.js modules.",
			// insertText: '"lodash": "*"',
			// range: range
		// },{
			// label: '"express"',
			// kind: mon.languages.CompletionItemKind.Function,
			// documentation: "Fast, unopinionated, minimalist web framework",
			// insertText: '"express": "*"',
			// range: range
		// }
	// ];

// !v.startsWith('Esm_') : For Es Module have store in window
// eslint-disable-next-line
	return window.getDefineObj().filter(v => {
		let wv = window[v];
		// // !['__core-js_shared__','__REACT_DEVTOOLS_APPEND_COMPONENT_STACK__','__REACT_DEVTOOLS_COMPONENT_FILTERS__','__VUE_DEVTOOLS_TOAST__'].includes(v)
		if(wv && !wv.toString().includes('[Command Line API]') && !(v.startsWith('__') || v.endsWith('__')) && !v.startsWith('Esm_')){
			return v;
		}
	}).map(v => ({
		label: v,
		documentation: window[v].toString(),
		insertText: v
	}));
	
	// return setWin.map(v => ({
		// label: v,
		// insertText: v
	// }));
} */

/** END: Get user define object */

 // setDefineWindow, monacoThemes, defaultThemes, monacoLangs

/***/ }),

/***/ "./resources/js/src/utils/string.js":
/*!******************************************!*\
  !*** ./resources/js/src/utils/string.js ***!
  \******************************************/
/*! exports provided: validFilename, toCapital, kebabToPascal, strInsertBefore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validFilename", function() { return validFilename; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toCapital", function() { return toCapital; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "kebabToPascal", function() { return kebabToPascal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "strInsertBefore", function() { return strInsertBefore; });
// String Utilities
// Validate
var windowsNames = function windowsNames() {
  return /^(con|prn|aux|nul|com[0-9]|lpt[0-9])$/i;
};

var filenameReservedRegex = function filenameReservedRegex() {
  return /[<>:"\/\\|?*\x00-\x1F]/g;
};

var validFilename = function validFilename(str) {
  if (!str || str.length > 255) {
    return false;
  }

  if (filenameReservedRegex().test(str) || windowsNames().test(str)) {
    return false;
  }

  if (/^\.\.?$/.test(str)) {
    return false;
  }

  return true;
};

var toCapital = function toCapital(s) {
  return s[0].toLocaleUpperCase() + s.slice(1);
};

function kebabToPascal(str) {
  if (str === null || str === void 0) return '';
  if (typeof str.toString !== 'function') return '';
  var input = str.toString().trim();
  if (input === '') return '';
  if (input.length === 1) return input.toLocaleUpperCase();
  var match = input.match(/[a-zA-Z0-9]+/g);
  if (match) return match.map(function (m) {
    return toCapital(m);
  }).join('');
  return input;
}
/*
function kebabToCamel(str){
  // let arr = str.split('-');
  // let capital = str.split('-').map((item, index) => index ? item.charAt(0).toUpperCase() + item.slice(1).toLowerCase() : item);
  // ^-- change here.
  // let capitalString = capital.join("");

  return str.split('-').map((item, index) => index ? item.charAt(0).toUpperCase() + item.slice(1).toLowerCase() : item).join("");
}
*/
// Insert String before some String

/** 
 * @USAGE :
 * strInsertBefore(setHtml (this is string to append), "This is string to before insert", "This is string to insert"); // 
 * 
*/


function strInsertBefore(str, strBefore, strInsert) {
  var n = str.lastIndexOf(strBefore);
  if (n < 0) return str;
  return str.substring(0, n) + strInsert + str.substring(n);
} // Get String in src img tag
// var str = "<img alt='' src='http://api.com/images/UID' /><br/>Some plain text<br/><a href='http://www.google.com'>http://www.google.com</a>";
// var regex = /<img.*?src='(.*?)'/; // /^<\s*img[^>]+src\s*=\s*(["'])(.*?)\1[^>]*>$/
// var src = regex.exec(str)[1];
// console.log(src);
// var im = 'im';
// var src2 = regex.exec(str)[1];




/***/ })

}]);
//# sourceMappingURL=ReplPage~ReplSimplePage.chunk.js.map