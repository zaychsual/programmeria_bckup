(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ReactPlayerDemoPage"],{

/***/ "./node_modules/css-loader/index.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/src/pages/admin/devs/react-player-demo/App.scss":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./resources/js/src/pages/admin/devs/react-player-demo/App.scss ***!
  \*********************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".player-wrapper {\n  width: 480px;\n  height: 270px;\n}\n\n.react-player {\n  margin-bottom: 10px;\n  background: rgba(0, 0, 0, 0.1);\n}\n\n.faded {\n  color: rgba(0, 0, 0, 0.5);\n}\n\n.footer {\n  margin: 20px;\n}", ""]);

// exports


/***/ }),

/***/ "./resources/js/src/pages/admin/devs/react-player-demo/App.scss":
/*!**********************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/react-player-demo/App.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader!../../../../../../../node_modules/postcss-loader/src??ref--9-2!../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!./App.scss */ "./node_modules/css-loader/index.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/src/pages/admin/devs/react-player-demo/App.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/react-player-demo/Duration.js":
/*!*************************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/react-player-demo/Duration.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Duration; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function Duration(_ref) {
  var className = _ref.className,
      seconds = _ref.seconds;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("time", {
    dateTime: "P".concat(Math.round(seconds), "S"),
    className: className
  }, format(seconds));
}

function format(seconds) {
  var date = new Date(seconds * 1000);
  var hh = date.getUTCHours();
  var mm = date.getUTCMinutes();
  var ss = pad(date.getUTCSeconds());

  if (hh) {
    return "".concat(hh, ":").concat(pad(mm), ":").concat(ss);
  }

  return "".concat(mm, ":").concat(ss);
}

function pad(string) {
  return ('0' + string).slice(-2);
}

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/react-player-demo/ReactPlayerDemoPage.js":
/*!************************************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/react-player-demo/ReactPlayerDemoPage.js ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ReactPlayerDemoPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! screenfull */ "./node_modules/screenfull/dist/screenfull.js");
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(screenfull__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_player_lazy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-player/lazy */ "./node_modules/react-player/lazy/index.js");
/* harmony import */ var react_player_lazy__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_player_lazy__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _App_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./App.scss */ "./resources/js/src/pages/admin/devs/react-player-demo/App.scss");
/* harmony import */ var _App_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_App_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _Duration__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Duration */ "./resources/js/src/pages/admin/devs/react-player-demo/Duration.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


 // import { hot } from 'react-hot-loader'


 // import './reset.scss';
// import './defaults.scss';
// import './range.scss';

 // import { version } from '../../package.json'
// import ReactPlayer from '../index';



var ReactPlayerDemoPage = /*#__PURE__*/function (_Component) {
  _inherits(ReactPlayerDemoPage, _Component);

  var _super = _createSuper(ReactPlayerDemoPage);

  function ReactPlayerDemoPage() {
    var _this;

    _classCallCheck(this, ReactPlayerDemoPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      url: null,
      pip: false,
      playing: true,
      controls: false,
      light: false,
      volume: 0.8,
      muted: false,
      played: 0,
      loaded: 0,
      duration: 0,
      playbackRate: 1.0,
      loop: false
    });

    _defineProperty(_assertThisInitialized(_this), "load", function (url) {
      _this.setState({
        url: url,
        played: 0,
        loaded: 0,
        pip: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handlePlayPause", function () {
      _this.setState({
        playing: !_this.state.playing
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleStop", function () {
      _this.setState({
        url: null,
        playing: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleToggleControls", function () {
      var url = _this.state.url;

      _this.setState({
        controls: !_this.state.controls,
        url: null
      }, function () {
        return _this.load(url);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleToggleLight", function () {
      _this.setState({
        light: !_this.state.light
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleToggleLoop", function () {
      _this.setState({
        loop: !_this.state.loop
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleVolumeChange", function (e) {
      _this.setState({
        volume: parseFloat(e.target.value)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleToggleMuted", function () {
      _this.setState({
        muted: !_this.state.muted
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleSetPlaybackRate", function (e) {
      _this.setState({
        playbackRate: parseFloat(e.target.value)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleTogglePIP", function () {
      _this.setState({
        pip: !_this.state.pip
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handlePlay", function () {
      console.log('onPlay');

      _this.setState({
        playing: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleEnablePIP", function () {
      console.log('onEnablePIP');

      _this.setState({
        pip: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleDisablePIP", function () {
      console.log('onDisablePIP');

      _this.setState({
        pip: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handlePause", function () {
      console.log('onPause');

      _this.setState({
        playing: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleSeekMouseDown", function (e) {
      _this.setState({
        seeking: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleSeekChange", function (e) {
      _this.setState({
        played: parseFloat(e.target.value)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleSeekMouseUp", function (e) {
      _this.setState({
        seeking: false
      });

      _this.player.seekTo(parseFloat(e.target.value));
    });

    _defineProperty(_assertThisInitialized(_this), "handleProgress", function (state) {
      console.log('onProgress', state); // We only want to update time slider if we are not currently seeking

      if (!_this.state.seeking) {
        _this.setState(state);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleEnded", function () {
      console.log('onEnded');

      _this.setState({
        playing: _this.state.loop
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleDuration", function (duration) {
      console.log('onDuration', duration);

      _this.setState({
        duration: duration
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleClickFullscreen", function () {
      screenfull__WEBPACK_IMPORTED_MODULE_2___default.a.request(Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(_this.player));
    });

    _defineProperty(_assertThisInitialized(_this), "renderLoadButton", function (url, label) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this.load(url);
        }
      }, label);
    });

    _defineProperty(_assertThisInitialized(_this), "ref", function (player) {
      _this.player = player;
    });

    return _this;
  }

  _createClass(ReactPlayerDemoPage, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          url = _this$state.url,
          playing = _this$state.playing,
          controls = _this$state.controls,
          light = _this$state.light,
          volume = _this$state.volume,
          muted = _this$state.muted,
          loop = _this$state.loop,
          played = _this$state.played,
          loaded = _this$state.loaded,
          duration = _this$state.duration,
          playbackRate = _this$state.playbackRate,
          pip = _this$state.pip;
      var SEPARATOR = ' · ';
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid py-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
        className: "col"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "ReactPlayer Demo"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "player-wrapper"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player_lazy__WEBPACK_IMPORTED_MODULE_3___default.a, {
        ref: this.ref,
        className: "react-player",
        width: "100%",
        height: "100%",
        url: url,
        pip: pip,
        playing: playing,
        controls: controls,
        light: light,
        loop: loop,
        playbackRate: playbackRate,
        volume: volume,
        muted: muted,
        onReady: function onReady() {
          return console.log('onReady');
        },
        onStart: function onStart() {
          return console.log('onStart');
        },
        onPlay: this.handlePlay,
        onEnablePIP: this.handleEnablePIP,
        onDisablePIP: this.handleDisablePIP,
        onPause: this.handlePause,
        onBuffer: function onBuffer() {
          return console.log('onBuffer');
        },
        onSeek: function onSeek(e) {
          return console.log('onSeek', e);
        },
        onEnded: this.handleEnded,
        onError: function onError(e) {
          return console.log('onError', e);
        },
        onProgress: this.handleProgress,
        onDuration: this.handleDuration
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "table-responsive"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("table", {
        className: "table",
        style: {
          tableLayout: 'fixed'
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Controls"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleStop
      }, "Stop"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handlePlayPause
      }, playing ? 'Pause' : 'Play'), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleClickFullscreen
      }, "Fullscreen"), light && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.player.showPreview();
        }
      }, "Show preview"), react_player_lazy__WEBPACK_IMPORTED_MODULE_3___default.a.canEnablePIP(url) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleTogglePIP
      }, pip ? 'Disable PiP' : 'Enable PiP'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Speed"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleSetPlaybackRate,
        value: 1
      }, "1x"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleSetPlaybackRate,
        value: 1.5
      }, "1.5x"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleSetPlaybackRate,
        value: 2
      }, "2x"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Seek"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        className: "custom-range",
        type: "range",
        min: 0,
        max: 0.999999,
        step: "any",
        value: played,
        onMouseDown: this.handleSeekMouseDown,
        onChange: this.handleSeekChange,
        onMouseUp: this.handleSeekMouseUp
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Volume"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "range",
        min: 0,
        max: 1,
        step: "any",
        value: volume,
        onChange: this.handleVolumeChange
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        htmlFor: "controls"
      }, "Controls")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        id: "controls",
        type: "checkbox",
        checked: controls,
        onChange: this.handleToggleControls
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, "\xA0 Requires player reload"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        htmlFor: "muted"
      }, "Muted")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        id: "muted",
        type: "checkbox",
        checked: muted,
        onChange: this.handleToggleMuted
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        htmlFor: "loop"
      }, "Loop")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        id: "loop",
        type: "checkbox",
        checked: loop,
        onChange: this.handleToggleLoop
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        htmlFor: "light"
      }, "Light mode")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        id: "light",
        type: "checkbox",
        checked: light,
        onChange: this.handleToggleLight
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Played"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("progress", {
        max: 1,
        value: played
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Loaded"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("progress", {
        max: 1,
        value: loaded
      }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
        className: "col"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "table-responsive"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("table", {
        className: "table",
        style: {
          tableLayout: 'fixed'
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "YouTube"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://www.youtube.com/watch?v=oUFJJNQGwhk', 'Test A'), this.renderLoadButton('https://www.youtube.com/watch?v=jNgP6d9HraI', 'Test B'), this.renderLoadButton('https://www.youtube.com/playlist?list=PLogRWNZ498ETeQNYrOlqikEML3bKJcdcx', 'Playlist'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "SoundCloud"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://soundcloud.com/miami-nights-1984/accelerated', 'Test A'), this.renderLoadButton('https://soundcloud.com/tycho/tycho-awake', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Facebook"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://www.facebook.com/facebook/videos/10153231379946729/', 'Test A'), this.renderLoadButton('https://www.facebook.com/FacebookDevelopers/videos/10152454700553553/', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Vimeo"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://vimeo.com/90509568', 'Test A'), this.renderLoadButton('https://vimeo.com/169599296', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Twitch"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://www.twitch.tv/videos/106400740', 'Test A'), this.renderLoadButton('https://www.twitch.tv/videos/12783852', 'Test B'), this.renderLoadButton('https://www.twitch.tv/kronovi', 'Test C'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Streamable"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://streamable.com/moo', 'Test A'), this.renderLoadButton('https://streamable.com/ifjh', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Wistia"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://home.wistia.com/medias/e4a27b971d', 'Test A'), this.renderLoadButton('https://home.wistia.com/medias/29b0fbf547', 'Test B'), this.renderLoadButton('https://home.wistia.com/medias/bq6epni33s', 'Test C'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "DailyMotion"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://www.dailymotion.com/video/x5e9eog', 'Test A'), this.renderLoadButton('https://www.dailymotion.com/video/x61xx3z', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Mixcloud"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://www.mixcloud.com/mixcloud/meet-the-curators/', 'Test A'), this.renderLoadButton('https://www.mixcloud.com/mixcloud/mixcloud-curates-4-mary-anne-hobbs-in-conversation-with-dan-deacon/', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Vidyard"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://video.vidyard.com/watch/YBvcF2BEfvKdowmfrRwk57', 'Test A'), this.renderLoadButton('https://video.vidyard.com/watch/BLXgYCDGfwU62vdMWybNVJ', 'Test B'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Files"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, this.renderLoadButton('https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/360/Big_Buck_Bunny_360_10s_1MB.mp4', 'mp4'), this.renderLoadButton('https://test-videos.co.uk/vids/bigbuckbunny/webm/vp8/360/Big_Buck_Bunny_360_10s_1MB.webm', 'webm'), this.renderLoadButton('https://filesamples.com/samples/video/ogv/sample_640x360.ogv', 'ogv'), this.renderLoadButton('https://storage.googleapis.com/media-session/elephants-dream/the-wires.mp3', 'mp3'), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), this.renderLoadButton('https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8', 'HLS (m3u8)'), this.renderLoadButton('http://dash.edgesuite.net/envivio/EnvivioDash3/manifest.mpd', 'DASH (mpd)'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "Custom URL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        ref: function ref(input) {
          _this2.urlInput = input;
        },
        type: "text",
        placeholder: "Enter URL"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.setState({
            url: _this2.urlInput.value
          });
        }
      }, "Load")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "State"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "table-responsive"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("table", {
        className: "table",
        style: {
          tableLayout: 'fixed'
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "url"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: !url ? 'faded' : ''
      }, (url instanceof Array ? 'Multiple' : url) || 'null')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "playing"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, playing ? 'true' : 'false')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "volume"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, volume.toFixed(3))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "played"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, played.toFixed(3))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "loaded"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, loaded.toFixed(3))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "duration"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Duration__WEBPACK_IMPORTED_MODULE_5__["default"], {
        seconds: duration
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "elapsed"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Duration__WEBPACK_IMPORTED_MODULE_5__["default"], {
        seconds: duration * played
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", null, "remaining"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Duration__WEBPACK_IMPORTED_MODULE_5__["default"], {
        seconds: duration * (1 - played)
      })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("footer", {
        className: "footer"
      }, SEPARATOR, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://github.com/CookPete/react-player"
      }, "GitHub"), SEPARATOR, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.npmjs.com/package/react-player"
      }, "npm")));
    }
  }]);

  return ReactPlayerDemoPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);



/***/ })

}]);
//# sourceMappingURL=ReactPlayerDemoPage.chunk.js.map