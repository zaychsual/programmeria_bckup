(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/src/components/antd/tree/AntdTree.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/antd/tree/AntdTree.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AntdTree; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_tree_style_index_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/tree/style/index.css */ "./node_modules/antd/lib/tree/style/index.css");
/* harmony import */ var antd_lib_tree_style_index_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_tree_style_index_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import 'antd/dist/antd.css';


 // import Btn from '../../components/q-ui-react/Btn';
// const { DirectoryTree } = Tree;

function AntdTree(_ref) {
  var _ref$data = _ref.data,
      data = _ref$data === void 0 ? [] : _ref$data,
      className = _ref.className,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? Q.noop : _ref$onClick,
      etc = _objectWithoutProperties(_ref, ["data", "className", "onClick"]);

  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in AntdTree','color:yellow;');
  // }, []);
  // NOT WORK???
  // const ClickTree = (selectedKeys, info) => { // SelectTree
  // 	console.log('ClickTree selectedKeys: ', selectedKeys);
  // 	console.log('ClickTree info: ', info);
  // }
  var Expand = function Expand(keys, data) {
    console.log('Expand keys: ', keys);
    console.log('Expand data: ', data);
    onClick(data);
  };

  var CtxMenu = function CtxMenu(data) {
    console.log('CtxMenu data: ', data);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Tree"].DirectoryTree, _extends({}, etc, {
    className: Q.Cx("no-caret-ico", className) // showLine 
    // multiple 
    ,
    selectable: false,
    draggable: true // blockNode 
    // switcherIcon={undefined} // <DownOutlined />
    // defaultExpandedKeys={} // ['0-0-0'] 
    // icon={(p) => <i className="fal fa-cog">{console.log(p)}</i>} 
    // onSelect={ClickTree} // SelectTree
    ,
    onExpand: Expand,
    onRightClick: CtxMenu,
    treeData: data
  }));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/devs/NumberField.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/devs/NumberField.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // useMemo

function NumberField(_ref) {
  var value = _ref.value,
      defaultValue = _ref.defaultValue,
      _ref$lang = _ref.lang,
      lang = _ref$lang === void 0 ? "en-GB" : _ref$lang,
      _ref$format = _ref.format,
      format = _ref$format === void 0 ? "decimal" : _ref$format,
      intl = _ref.intl,
      _ref$onChange = _ref.onChange,
      onChange = _ref$onChange === void 0 ? Q.noop : _ref$onChange,
      etc = _objectWithoutProperties(_ref, ["value", "defaultValue", "lang", "format", "intl", "onChange"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(value || defaultValue || ""),
      _useState2 = _slicedToArray(_useState, 2),
      val = _useState2[0],
      setVal = _useState2[1];

  var toCurrency = function toCurrency(v, lang, intlOps) {
    // const { lang, style, currency } = this.props; // maximumSignificantDigits
    // new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(number)
    // "sv-SE" | en-GB | "id-ID"
    // const formatter = new Intl.NumberFormat(lang, {
    //   style, // : "decimal", // decimal | currency
    // 	currency, // : "IDR", // SEK
    // 	maximumSignificantDigits
    // });
    // const intl = new Intl.NumberFormat(lang, intlOps);
    // return intl.format(v);
    var repVal = v.replace(/,|[a-z]|[A-Z]/g, ""); // .replace(/[a-z]|[A-Z]/g, "");
    // console.log('repVal: ', repVal);

    if (/[0-9]/g.test(repVal)) {
      return new Intl.NumberFormat(lang, intlOps).format(Number(repVal));
    }

    return "";
  };

  var Change = function Change(e) {
    var v = e.target.value;
    setVal(v);
    onChange(v, e);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({}, etc, {
    type: "text",
    inputMode: "numeric" // decimal
    ,
    value: toCurrency(val, lang, _objectSpread(_objectSpread({}, intl), {}, {
      style: format // currency, 
      // maximumSignificantDigits 

    })),
    onChange: Change
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["memo"])(NumberField));
/* 
export class Number extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // isEdit: false
      vals: props.value || props.defaultValue || ""
    };
  }

  // componentDidMount(){
  // 	console.log('%ccomponentDidMount in NumberField','color:yellow;');
  // }

  onChange = (e) => {
    const val = e.target.value;
    this.setState({ vals: val });
    this.props.onChange(val, e);
  };

  toCurrency(v, lang, intlOps) {
    // const { lang, style, currency } = this.props; // maximumSignificantDigits
    // new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(number)

    // "sv-SE" | en-GB | "id-ID"
    // const formatter = new Intl.NumberFormat(lang, {
    //   style, // : "decimal", // decimal | currency
    // 	currency, // : "IDR", // SEK
    // 	maximumSignificantDigits
    // });

    // const intl = new Intl.NumberFormat(lang, intlOps);
    // return intl.format(v);
    const repVal = v.replace(/,/g, "");

    if (/[0-9]/.test(repVal)) {
      return new Intl.NumberFormat(lang, intlOps).format(repVal); // val
    }

    return "";
  }

  render() {
    // value, defaultValue,
    const {
      lang,
      style,
      currency,
      maximumSignificantDigits,
      ...etc
    } = this.props; // className, name,

    return (
      <input
        {...etc}
        type="text"
        inputMode="numeric"
        value={this.toCurrency(this.state.vals, lang, {
          style,
          currency,
          maximumSignificantDigits
        })}
        onChange={this.onChange}
      />
    );
  }
}

Number.defaultProps = {
  // Intl Options:
  lang: "en-US", // en-GB
  style: "decimal", // decimal | currency
  // currency, // "USD" | "IDR"
  maximumSignificantDigits: 3,

  // value: "",
  onChange: () => {}
};
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Ava.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Ava.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Ava; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_darkOrLight__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/darkOrLight */ "./resources/js/src/utils/darkOrLight.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useRef, useEffect }
// import { mapToCssModules } from '../q-react-bootstrap/utils';
// import { Cx, setClass, setAttr, isStr, cached } from '../../utils/Q.js'; // , hasClass

 // FROM https://github.com/chakra-ui/chakra-ui/blob/master/packages/chakra-ui/src/Avatar/index.js

var getInitials = function getInitials(name) {
  var no = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '?';
  // if(!name || !Q.isStr(name) || name.length < 1) return no;
  if (!name || !Q.isStr(name) || name === " " || name.length < 1) return no; // Destruct 

  var _name$split = name.split(" "),
      _name$split2 = _slicedToArray(_name$split, 2),
      first = _name$split2[0],
      last = _name$split2[1];

  if (first && last) {
    return "".concat(first[0]).concat(last[0]); // first.charAt(0)}${last.charAt(0)
  }

  return first[0]; // first.charAt(0)
}; // FROM https://github.com/segmentio/evergreen/blob/master/src/avatar/src/utils/getInitials.js
// function getInitials(name, fallback = '?'){
// if(!name || typeof name !== 'string') return fallback
// return name
// .replace(/\s+/, ' ')
// .split(' ') // Repeated spaces results in empty strings
// .slice(0, 2)
// .map(v => v && v[0].toUpperCase()) // Watch out for empty strings
// .join('')
// }
// FROM: chakra-ui


function str2Hex(str) {
  var no = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "5a6268";
  if (!str || str.length === 0) return no;
  var hash = 0,
      sl = str.length;

  for (var i = 0; i < sl; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
    hash = hash & hash;
  }

  var color = ""; // #

  for (var j = 0; j < 3; j++) {
    var val = hash >> j * 8 & 255;
    color += ("00" + val.toString(16)).substr(-2);
  }

  return color;
}

function Ava(_ref) {
  var inRef = _ref.inRef,
      _ref$w = _ref.w,
      w = _ref$w === void 0 ? 30 : _ref$w,
      _ref$h = _ref.h,
      h = _ref$h === void 0 ? 30 : _ref$h,
      src = _ref.src,
      _ref$alt = _ref.alt,
      alt = _ref$alt === void 0 ? " " : _ref$alt,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? "lazy" : _ref$loading,
      _ref$drag = _ref.drag,
      drag = _ref$drag === void 0 ? false : _ref$drag,
      className = _ref.className,
      wrapClass = _ref.wrapClass,
      wrapProps = _ref.wrapProps,
      bg = _ref.bg,
      circle = _ref.circle,
      round = _ref.round,
      thumb = _ref.thumb,
      contain = _ref.contain,
      onError = _ref.onError,
      onLoad = _ref.onLoad,
      etc = _objectWithoutProperties(_ref, ["inRef", "w", "h", "src", "alt", "loading", "drag", "className", "wrapClass", "wrapProps", "bg", "circle", "round", "thumb", "contain", "onError", "onLoad"]);

  // DEV OPTION: random color error image with initial alt
  // const randomColor = Math.floor(Math.random() * 16777215).toString(16);

  /* useEffect(() => {
  	// console.log('%cuseEffect in Ava','color:yellow;');
  }, []); */
  var removeLoadClass = function removeLoadClass(et) {
    // Q.setClass(et.parentElement, 'ava-loader', 'remove');// ovhide ava-loader
    Q.setClass(et, 'ava-loader', 'remove');
  };

  var Err = function Err(e) {
    var et = e.target;
    removeLoadClass(et);
    if (Q.hasClass(et, "text-dark")) Q.setClass(et, "text-dark", "remove");
    if (Q.hasClass(et, "text-white")) Q.setClass(et, "text-white", "remove"); // let setWidth = w ? w : h ? h : 50; // (et.offsetWidth || et.clientWidth);

    var fs = '--fs:calc(';

    if (isNaN(w)) {
      // setWidth
      var num = parseFloat(w);
      var unit = w.replace("" + num, ''); // `${num}`, '')

      fs += num + unit + " / 2.25);"; // 2.5 | `${num}${unit} / 2.25);`
    } else {
      fs += w + "px / 2.25);"; // 2.5 | `${w}px / 2.25);`
    } // const setAlt = et.alt || alt;// OPTION: if change alt (Directly in DOM)


    var trm = alt.trim();
    var color = bg ? bg.replace("#", "") : str2Hex(trm); // if(!parseFloat(et.getAttribute("width"))) et.width = setWidth; // OPTION

    Q.setAttr(et, {
      'aria-label': getInitials(trm),
      // alt | setAlt
      style: fs + "--bg:#" + color // alt | setAlt | randomColor

    });
    Q.setClass(et, Object(_utils_darkOrLight__WEBPACK_IMPORTED_MODULE_1__["default"])(color) === "dark" ? "text-white" : "text-dark"); // DEV OPTION: Change src with svg
    // et.src = `data:image/svg+xml,%3Csvg width="100%25" height="100%25" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"%3E%3Cstyle%3E%0Asvg%7Bfont-family:sans-serif;font-size:1rem;text-anchor:middle%7D%0A%3C/style%3E%3Crect width="100%25" height="100%25" fill="%2355595c"%3E%3C/rect%3E%3Ctext x="50%25" y="50%25" fill="%23eceeef" dy=".35em"%3E${getInitials(alt)}%3C/text%3E%3C/svg%3E`;
    // FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
    // if(et.onerror) et.onerror = null;// DEV OPTION

    if (onError) onError(e);
    return; // null
  };

  var Load = function Load(e) {
    removeLoadClass(e.target);
    if (onLoad) onLoad(e);
  }; // const sameCx = {
  // 'rounded-circle': circle,
  // 'rounded': round
  // };// , cssModule


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, wrapProps, {
    className: // Q.Cx("img-frame ava-frame ava-loader", sameCx, frameClass)
    Q.Cx("img-frame ava-frame", {
      'img-thumbnail': thumb // 'rounded-circle': circle,
      // 'rounded': round

    }, wrapClass) // draggable={!!drag} // OPTION

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", _extends({}, etc, {
    ref: inRef,
    loading: loading // OPTIONS: native lazy load html
    ,
    width: w,
    height: h,
    src: Q.newURL(src).href // OPTION
    ,
    alt: alt,
    className: Q.Cx("ava ava-loader", {
      // ...sameCx,
      // "rounded-pill": pill, 
      'rounded': round,
      'rounded-circle': circle,
      // 'img-fluid': fluid,
      // 'ava-thumb': thumb, // img-thumbnail
      // 'img-thumbnail': thumb, 
      'of-con': contain
    }, className),
    draggable: drag // !!drag
    ,
    onError: Err,
    onLoad: Load // onContextMenu={e => preventQ(e)}

  })));
} // Ava.defaultProps = {
// alt: ''
// };

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Beforeunload.js":
/*!****************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Beforeunload.js ***!
  \****************************************************************/
/*! exports provided: usePrompt, Beforeunload */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "usePrompt", function() { return usePrompt; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Beforeunload", function() { return Beforeunload; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
 // React, 

 // import Btn from '../../components/q-ui-bootstrap/Btn';
// window.confirm("Do you really want to leave?")
// Are you sure you want to quit without saving your changes?

var usePrompt = function usePrompt(when) {
  var msg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["useHistory"])();
  var me = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var onWindowOrTabClose = function onWindowOrTabClose(e) {
    if (!when) {
      return;
    }

    if (typeof e == 'undefined') {
      e = window.event;
    }

    if (e) {
      e.returnValue = msg;
    }

    return msg;
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (when) {
      me.current = history.block(msg);
    } else {
      me.current = null;
    }

    window.addEventListener('beforeunload', onWindowOrTabClose);
    return function () {
      if (me.current) {
        me.current();
        me.current = null;
      }

      window.removeEventListener('beforeunload', onWindowOrTabClose);
    };
  }, [msg, when]);
};

function Beforeunload(_ref) {
  var when = _ref.when,
      _ref$msg = _ref.msg,
      msg = _ref$msg === void 0 ? "" : _ref$msg,
      _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children;
  usePrompt(when, msg);
  return children; //  || null
}

 // export default function Beforeunload({
//   // handlerRef, 
// 	// condition, 
// 	when, 
// 	message = '', // Are you sure you want to quit without saving your changes?
//   // onBeforeunload, 
//   children = null
// }){
//   const history = useHistory();
//   const me = useRef(null);
//   const onWindowOrTabClose = e => {
//     if (!when) {
//       return;
//     }
//     if (typeof e == 'undefined') {
//       e = window.event;
//     }
//     if (e) {
//       e.returnValue = message;
//     }
//     return message;
//   };
//   useEffect(() => {
//     // const fnBeforeunload = (e) => {
//     //   // let returnValue;
//     //   // if (handlerRef.current != null) {
//     //   //   returnValue = handlerRef.current(e);
//     //   // }
//     //   // // Chrome requires `returnValue` to be set.
//     //   // if (e.defaultPrevented) {
//     //   //   e.returnValue = '';
//     //   // }
//     //   // if (typeof returnValue === 'string') {
//     //   //   e.returnValue = returnValue;
//     //   //   return returnValue;
//     //   // }
//     //   // Cancel the event as stated by the standard.
//     //   e.preventDefault();
//     //   // Older browsers supported custom message
//     //   e.returnValue = '';
//     // };
//     // if(typeof condition === "boolean"){
//     //   if(condition) window.addEventListener('beforeunload', onWindowOrTabClose);// fnBeforeunload
//     //   else window.removeEventListener('beforeunload', onWindowOrTabClose);
//     // }
//     // else{
//     //   window.addEventListener('beforeunload', onWindowOrTabClose);
//     // }
//     // window.addEventListener('beforeunload', onWindowOrTabClose);
//     // return () => {
//     //   window.removeEventListener('beforeunload', onWindowOrTabClose);
// 		// };
//     if (when) {
//       me.current = history.block(message);
//     } else {
//       me.current = null;
//     }
//     window.addEventListener('beforeunload', onWindowOrTabClose);
//     return () => {
//       if (me.current) {
//         me.current();
//         me.current = null;
//       }
//       window.removeEventListener('beforeunload', onWindowOrTabClose);
// 		}
// 		// condition
//   }, [message, when]);// eslint-disable-line react-hooks/exhaustive-deps
//   return children;
// }

/*

*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Details.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Details.js ***!
  \***********************************************************/
/*! exports provided: Summary, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Summary", function() { return Summary; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Details; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import Btn from '../../components/q-ui-react/Btn';

var Summary = function Summary(_ref) {
  var _ref$As = _ref.As,
      As = _ref$As === void 0 ? "summary" : _ref$As,
      inRef = _ref.inRef,
      etc = _objectWithoutProperties(_ref, ["As", "inRef"]);

  // const KeyDown = e => {
  // let parent = e.target.parentElement;
  // let next = parent.nextElementSibling;
  // console.log('key: ', e.key);
  // console.log('next: ', next);
  // console.log('parent: ', parent);
  // console.log('next.role: ', Q.hasAttr(next, "role"));
  // let isRole = Q.hasAttr(next, "role");
  // if(e.key === "ArrowDown" && ((parent?.tagName === "DETAILS" && next?.tagName === "DETAILS") || (next && isRole) )){
  // console.log("focus: ", next.firstElementChild);
  // if(isRole){
  // next.focus();
  // }else{
  // next?.firstElementChild?.focus();// click
  // }
  // }
  // }
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, _extends({
    tabIndex: "0"
  }, etc));
};
function Details(_ref2) {
  var _extends2;

  var inRef = _ref2.inRef,
      className = _ref2.className,
      summary = _ref2.summary,
      summaryClass = _ref2.summaryClass,
      summaryProps = _ref2.summaryProps,
      groupClass = _ref2.groupClass,
      children = _ref2.children,
      _ref2$open = _ref2.open,
      open = _ref2$open === void 0 ? false : _ref2$open,
      renderOpen = _ref2.renderOpen,
      _ref2$onToggle = _ref2.onToggle,
      onToggle = _ref2$onToggle === void 0 ? Q.noop : _ref2$onToggle,
      etc = _objectWithoutProperties(_ref2, ["inRef", "className", "summary", "summaryClass", "summaryProps", "groupClass", "children", "open", "renderOpen", "onToggle"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(renderOpen),
      _useState2 = _slicedToArray(_useState, 2),
      one = _useState2[0],
      setOne = _useState2[1]; // false


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(open),
      _useState4 = _slicedToArray(_useState3, 2),
      isOpen = _useState4[0],
      seIsOpen = _useState4[1]; // useEffect(() => {
  // 	console.log('%cuseEffect in Details','color:yellow;');
  // }, []);


  var Toggle = function Toggle(e) {
    var et = e.target; // console.log("onToggle e: ", e.target.open);

    if (!one) setOne(true); // setTimeout(() => {
    // 	et.open ? Q.domQ(".detail-content", et)
    // }, 1);
    // Q.setAttr(et, {"aria-expanded": et.open});

    seIsOpen(et.open);
    onToggle(et.open, e);
  };
  /* const clickSummary = e => {
  // console.log('clickSummary e: ', e);
  onClickSummary(e);
  } */


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("details", _extends({}, etc, (_extends2 = {
    ref: inRef,
    open: open // open | isOpen
    // aria-expanded={isOpen} // OPTION: for accessibility
    ,
    className: Q.Cx("detail-q", className)
  }, _defineProperty(_extends2, "open", Boolean(one) ? one : open), _defineProperty(_extends2, "onToggle", Toggle), _extends2)), summary && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Summary, _extends({
    className: summaryClass
  }, summaryProps, {
    "aria-expanded": isOpen // OPTION: for accessibility

  }), summary), one && ( // , { "d-none": !open }
  summary ? // children | Q.Cx("detail-content", groupClass)
  children && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    role: "group",
    "aria-hidden": !isOpen,
    className: groupClass
  }, children) : children));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/IconPicker.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/IconPicker.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return IconPicker; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _researchgate_react_intersection_list__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @researchgate/react-intersection-list */ "./node_modules/@researchgate/react-intersection-list/lib/es/index.js");
/* harmony import */ var _Btn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _Flex__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _utils_number__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utils/number */ "./resources/js/src/utils/number.js");
/* harmony import */ var _utils_clipboard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../utils/clipboard */ "./resources/js/src/utils/clipboard/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // Fragment


 // import { useInViewport } from 'ahooks';



 // import Aroute from './Aroute';


 // import { APP_DOMAIN } from '../../data/appData';
// import { domQall, hasClass, setClass, toggleClass, noop, Qid } from '../../utils/Q';
// function InViewPort({ inRef, children }){
//   // const ref = useRef();
//   const inView = useInViewport(inRef);
//   return (
//     <Fragment>
//       {children && children(inView)}
//     </Fragment>
//   );
// };

function IconPicker(_ref) {
  var icons = _ref.icons,
      url = _ref.url,
      _ref$show = _ref.show,
      show = _ref$show === void 0 ? false : _ref$show,
      _ref$label = _ref.label,
      label = _ref$label === void 0 ? "Icons" : _ref$label,
      _ref$w = _ref.w,
      w = _ref$w === void 0 ? 290 : _ref$w,
      _ref$h = _ref.h,
      h = _ref$h === void 0 ? 340 : _ref$h,
      _ref$autoFocus = _ref.autoFocus,
      autoFocus = _ref$autoFocus === void 0 ? false : _ref$autoFocus,
      _ref$preview = _ref.preview,
      preview = _ref$preview === void 0 ? {
    name: "home",
    code: "e933"
  } : _ref$preview,
      _ref$cache = _ref.cache,
      cache = _ref$cache === void 0 ? true : _ref$cache,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? Q.noop : _ref$onClick,
      _ref$onHover = _ref.onHover,
      onHover = _ref$onHover === void 0 ? Q.noop : _ref$onHover;
  // const iconBoxRef = useRef(null);
  var findRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      load = _useState2[0],
      setLoad = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(icons),
      _useState4 = _slicedToArray(_useState3, 2),
      data = _useState4[0],
      setData = _useState4[1]; // false


  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState6 = _slicedToArray(_useState5, 2),
      meta = _useState6[0],
      setMeta = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(show),
      _useState8 = _slicedToArray(_useState7, 2),
      dd = _useState8[0],
      setDd = _useState8[1]; // false


  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(preview),
      _useState10 = _slicedToArray(_useState9, 2),
      view = _useState10[0],
      setView = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      findVal = _useState12[0],
      setFindVal = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      findArr = _useState14[0],
      setFindArr = _useState14[1]; // null
  // const [icoCache, setIcoCache] = useState(null);// DEV OPTION: Caching used


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // console.log('useEffect in IconPicker');// ,'color:yellow;'
    if (show) {
      // cacheIcon || 
      onGetSetData(); // null, cacheIcon
    } else {
      setLoad(true);
    }
  }, [show, onGetSetData]); // cache, icons, show

  var fetchIcon = function fetchIcon() {
    // cb
    // OPTION: Check url props
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(url).then(function (r) {
      // console.log('r: ', r);
      if (r.status === 200) {
        var _data = r.data;
        var metas = {
          name: _data.metadata.name,
          prefix: _data.preferences.fontPref.prefix
        };

        var dataIcon = _data.icons.map(function (v) {
          return {
            name: v.properties.name,
            code: Object(_utils_number__WEBPACK_IMPORTED_MODULE_6__["num2Hex"])(v.properties.code)
          };
        }); // const dataIcon = parseObj(data);


        setLoad(true);
        setMeta(metas);
        setData(dataIcon); // cb(metas);

        if (cache) {
          localStorage.setItem("icons", JSON.stringify({
            meta: metas,
            data: dataIcon
          }));
        }
      }
    })["catch"](function (e) {
      return console.log(e);
    });
  }; // const parseObj = (d) => {
  // 	const arr = d.icons.map(v => ({
  // 		name: v.properties.name,
  // 		code: num2Hex(v.properties.code)
  // 	})).filter((_, i) => i < 42);
  // 	// console.log('arr: ', arr);
  // 	return arr;
  // }


  var onGetSetData = function onGetSetData() {
    // iconCache
    if (cache) {
      var cacheIcon = localStorage.getItem("icons"); // qicoUsed

      if (cacheIcon) {
        var parseData = JSON.parse(cacheIcon);
        setLoad(true);
        setMeta(parseData.meta); // let dataIcon = parseData.data.filter((_, i) => i < 42);
        // console.log('parseData.data: ', parseData.data);
        // console.log('dataIcon: ', dataIcon);

        setData(parseData.data);
      } else {
        fetchIcon();
      }
    } else {
      fetchIcon(); // fetchIcon(r => {
      // 	if(cb) cb(r);
      // });
    }
  };

  var onHoverIcon = function onHoverIcon(v, e) {
    if (preview && view !== v) {
      setView(v); // ico
    }

    onHover(v, e); // ico, e
  };

  var onClickIcon = function onClickIcon(v, e) {
    // setDd(false);
    onClick(v, e); // OPTION: i, e

    /* if(isFunc(onClick)){
    	console.log(v);
    	// const ico = {name: v.name, code: v.code};// {name: v[0], code: v[1]}
    	onClick(v, i, e);// OPTION: i, e
    } */
    // DEV OPTION: Caching used
    // const ico = {name: v.name, code: v.code};

    /* if(cache){
    	// Frequently Used | Current Used
    	// const qicoUsed = localStorage.getItem('qicoUsed');
    	// console.log(icoCache);
    	if(icoCache !== null){
    		const obj = [ico, ...icoCache];
    		setIcoCache(obj);
    		localStorage.setItem('qicoUsed', JSON.stringify(obj));// emoji-mart.frequently
    	}else{
    		setIcoCache([ico]);
    		localStorage.setItem('qicoUsed', JSON.stringify([ico]));
    	}
    } */
  };

  var onFind = function onFind(e) {
    // const val = e.target ? ev.target.value : e;
    // const iconBox = iconBoxRef.current;
    // setFindVal(val);
    // setTimeout(() => {
    // 	let btnIcons = Q.domQall("button.icon-item", iconBox);
    // 	let NO = "d-none";
    // 	// 
    // 	btnIcons.forEach(el => {
    // 		if(el.dataset.name.includes(val)){// el.title.includes(val) | el.className.includes(val)
    // 			if(Q.hasClass(el, NO)) Q.setClass(el, NO, "remove");
    // 		}else{
    // 			Q.setClass(el, NO);
    // 		}
    // 	});
    // 	const getIcon = data.filter(v => v.name.includes(val)).length;
    // 	iconBox.classList.toggle("icoNotFound", getIcon < 1);
    // },900);
    var val = e.target.value;
    setFindVal(val);
    setTimeout(function () {
      var finds = data.filter(function (v) {
        return v.name.includes(val.toLowerCase());
      }); // console.log('finds: ', finds);

      setFindArr(finds);
    }, 900);
  };

  var onToggle = function onToggle(isOpen) {
    // , e, meta
    if (!data && isOpen) {
      onGetSetData();
    }

    if (dd && preview && view.name !== preview.name) {
      // console.log('if 1: ',dd);
      setView(preview);
    } // dd && findVal !== ''


    if (dd && findVal !== "") {
      // console.log('if 2: ',dd);
      setFindVal(""); // onFind("");

      setFindArr([]);
    } // Set focus 


    if (!dd && autoFocus && findRef.current) {
      setTimeout(function () {
        findRef.current.focus();
      }, 1);
    }

    setDd(isOpen);
  };

  var itemsRenderer = function itemsRenderer(items, ref) {
    var notFound = findVal.length > 0 && findLength() === 0;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
      wrap: true,
      content: "start",
      inRef: ref // iconBoxRef 
      ,
      className: Q.Cx("position-absolute position-full p-1 border bw-y1 ovyauto icons-box", {
        "icoNotFound": notFound
      }),
      "data-notfound": "NOT FOUND" // style={{
      // 	width: w,
      // 	maxHeight: maxH
      // }} 

    }, notFound ? [] : items);
  };

  var renderItem = function renderItem(index, key) {
    var v = findLength() > 0 ? findArr[index] : data[index]; // const v = data[index];

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      key: key,
      as: "button",
      type: "button",
      onMouseEnter: function onMouseEnter(e) {
        return onHoverIcon(v, e);
      },
      onClick: function onClick(e) {
        return onClickIcon({
          icon: _objectSpread(_objectSpread({}, v), {}, {
            className: pref() + meta.prefix + v.name
          }),
          key: key
        }, e);
      } // my-1
      ,
      className: "icon-item p-2 flex0 m-1 q-fw " + pref() + meta.prefix + v.name,
      "data-name": v.name // JSON.stringify({name: v[0], code: v[1]}, null, 2)
      ,
      title: "name: " + v.name + "\ncode: " + v.code
    });
  };

  var onCopy = function onCopy(e, t) {
    var et = e.target;
    Object(_utils_clipboard__WEBPACK_IMPORTED_MODULE_7__["clipboardCopy"])(t).then(function () {
      Q.setAttr(et, {
        "aria-label": "Copied!"
      });
      setTimeout(function () {
        return Q.setAttr(et, "aria-label");
      }, 999);
    })["catch"](function (e) {
      return console.log(e);
    });
  };

  var pref = function pref() {
    return meta.prefix.replace("-", " ");
  };

  var findLength = function findLength() {
    return findArr.length;
  }; //  group isOpen={dd} toggle={onToggle} | 


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    bsPrefix: "btn-group",
    show: dd,
    onToggle: onToggle
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, null, label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
    // renderOnMount 
    className: Q.Cx("p-0 ovhide icon-picker", {
      "w-200px": !data
    })
  }, data && load ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    dir: "column",
    align: "stretch",
    style: {
      width: w,
      // maxHeight: maxH
      height: h
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "d-flex m-0 p-1 flexno bg-strip"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    ref: findRef,
    autoFocus: autoFocus,
    onChange: onFind,
    value: findVal,
    className: "form-control form-control-sm",
    type: "search" // text
    ,
    placeholder: "Search"
  }), Q_appData.UA.browser.name === "Firefox" && findVal.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
    As: "b",
    size: "sm",
    kind: "flat",
    className: "qi qi-close xx",
    onClick: function onClick() {
      // if(findVal.length > 0) onFind("");// setFindVal | onFind
      // if(findVal.length > 0){
      setFindVal("");
      setFindArr([]); // }
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
    As: "b",
    size: "sm",
    kind: "flat",
    className: "qi qi-search" // className={"q-fw qi qi-" + (findVal.length > 0 ? "close xx":"search")} 
    // onClick={() => {
    // 	// if(findVal.length > 0) onFind("");// setFindVal | onFind
    // 	if(findVal.length > 0){
    // 		setFindVal("");
    // 		setFindArr([]);
    // 	}
    // }}

  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    dir: "column",
    align: "stretch",
    className: "flex1 h-100 position-relative wrap-icons"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_researchgate_react_intersection_list__WEBPACK_IMPORTED_MODULE_3__["default"], {
    awaitMore: findLength() > 0 ? findLength() > 0 : data.length > 0,
    pageSize: 70,
    itemCount: findLength() > 0 ? findLength() : data.length //  
    ,
    itemsRenderer: itemsRenderer,
    renderItem: renderItem // onIntersection={(size, pageSize) => {
    // 	console.log('onIntersection size: ', size);
    // 	console.log('onIntersection pageSize: ', pageSize);
    // }}

  })), preview &&
  /*#__PURE__*/
  // JSON.stringify(view, null, 2) //  style={{ width: w }}
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    wrap: true,
    align: "stretch",
    className: "flexno icon-view"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group btn-group-xs w-100 ip-btn-copy"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
    onClick: function onClick(e) {
      return onCopy(e, meta.prefix + view.name);
    },
    blur: true,
    kind: "light",
    className: "border-top-0 border-left-0 rounded-0 tip tipTL qi qi-copy"
  }, "Copy name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
    onClick: function onClick(e) {
      return onCopy(e, view.code);
    },
    blur: true,
    kind: "light",
    className: "border-top-0 rounded-0 tip tipTL qi qi-copy"
  }, "Copy code")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    justify: "center",
    align: "center",
    className: "flexno w-25 p-2 q-s22 " + pref() + meta.prefix + view.name
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    As: "pre",
    align: "center",
    className: "mb-0 p-2 small flex1 bg-light border-left q-scroll"
  }, "name: " + meta.prefix + view.name + "\ncode: " + view.code))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    dir: "column",
    align: "center",
    className: "text-center p-5 " + (load ? "i-load cwait" : "qi qi-ban ired fa-2x"),
    style: load ? {
      '--bg-i-load': '95px'
    } : undefined
  }, load ? "LOADING" : "ICONS NOT LOAD")));
} // export default React.memo(IconPicker, (prevProps, nextProps) => prevProps.icons === nextProps.icons);

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/InputQ.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/InputQ.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InputQ; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // className:

var FC = "form-control";
var CS = "custom-select";
function InputQ(_ref) {
  var _ref$WrapAs = _ref.WrapAs,
      WrapAs = _ref$WrapAs === void 0 ? "label" : _ref$WrapAs,
      _ref$As = _ref.As,
      As = _ref$As === void 0 ? "input" : _ref$As,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "text" : _ref$type,
      wrap = _ref.wrap,
      inRef = _ref.inRef,
      children = _ref.children,
      disabled = _ref.disabled,
      onChange = _ref.onChange,
      classNames = _ref.classNames,
      wrapProps = _ref.wrapProps,
      label = _ref.label,
      qSize = _ref.qSize,
      etc = _objectWithoutProperties(_ref, ["WrapAs", "As", "type", "wrap", "inRef", "children", "disabled", "onChange", "classNames", "wrapProps", "label", "qSize"]);

  // const [data, setData] = React.useState();
  // React.useEffect(() => {
  // 	console.log('%cuseEffect in Input','color:yellow;');
  // }, []);
  var isInput = As === "input";

  var Change = function Change(e) {
    if (disabled) {
      e.preventDefault();
      return;
    }

    if (onChange && !disabled) onChange(e);
  };

  var Input = function Input() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, _extends({}, etc, {
      ref: inRef,
      className: Q.Cx(isInput ? FC : CS, qSize && (isInput ? FC + "-" + qSize : CS + "-" + qSize) // isValid ? "is-valid" : "is-invalid"
      // [isValid ? "is-valid" : "is-invalid"] 
      ),
      type: isInput ? type : undefined,
      disabled: disabled,
      onChange: Change
    }));
  };

  if (wrap) {
    // children
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(WrapAs, _extends({}, wrapProps, {
      className: Q.Cx("q-input", classNames),
      "aria-label": label // htmlFor={WrapAs === "label" ? htmlFor : undefined} 

    }), Input(), children);
  }

  return Input();
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Range.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Range.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Range; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import Btn from '../../components/q-ui-react/Btn';

function Range(_ref) {
  var value = _ref.value,
      defaultValue = _ref.defaultValue,
      className = _ref.className,
      _ref$grab = _ref.grab,
      grab = _ref$grab === void 0 ? true : _ref$grab,
      _ref$onChange = _ref.onChange,
      onChange = _ref$onChange === void 0 ? Q.noop : _ref$onChange,
      etc = _objectWithoutProperties(_ref, ["value", "defaultValue", "className", "grab", "onChange"]);

  // let v = value || defaultValue || 0;
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(value || defaultValue || 0),
      _useState2 = _slicedToArray(_useState, 2),
      val = _useState2[0],
      setVal = _useState2[1]; // console.log('v: ', v);
  // useEffect(() => {
  //   console.log('v: ', v);
  // 	if(v){ // text.length > 0 && 
  //     setVal(v);
  //   }
  //   // console.log('txt: ', txt);
  //   // console.log('value: ', value);
  //   // console.log('defaultValue: ', defaultValue);
  // }, [v]); // value, defaultValue


  var Change = function Change(e) {
    setVal(e.target.value); // if(onChange) onChange(e);

    onChange(e);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({}, etc, {
    type: "range",
    className: Q.Cx("q-range", {
      "c-grab": grab
    }, className) // rounded-pill 
    // min={0} 
    // max={100} 
    ,
    value: value,
    defaultValue: defaultValue,
    style: {
      '--slider-val': val + '%'
    } // Number(e.target.value) 
    ,
    onChange: Change
  }));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Scroller.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Scroller.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Scroller; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Flex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _Btn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useInViewport } from 'ahooks';


 // const scrollOptions = {
// 	top: 0,
// 	left: 0, 
// 	behavior: "smooth"
// };

function Scroller(_ref) {
  var _ref$dir = _ref.dir,
      dir = _ref$dir === void 0 ? "row" : _ref$dir,
      _ref$customScroll = _ref.customScroll,
      customScroll = _ref$customScroll === void 0 ? true : _ref$customScroll,
      _ref$nowrap = _ref.nowrap,
      nowrap = _ref$nowrap === void 0 ? true : _ref$nowrap,
      _ref$ovx = _ref.ovx,
      ovx = _ref$ovx === void 0 ? true : _ref$ovx,
      _ref$scrollStep = _ref.scrollStep,
      scrollStep = _ref$scrollStep === void 0 ? 100 : _ref$scrollStep,
      _ref$behavior = _ref.behavior,
      behavior = _ref$behavior === void 0 ? "smooth" : _ref$behavior,
      _ref$onWheel = _ref.onWheel,
      onWheel = _ref$onWheel === void 0 ? function () {} : _ref$onWheel,
      _ref$onScrollPrev = _ref.onScrollPrev,
      onScrollPrev = _ref$onScrollPrev === void 0 ? function () {} : _ref$onScrollPrev,
      _ref$onScrollNext = _ref.onScrollNext,
      onScrollNext = _ref$onScrollNext === void 0 ? function () {} : _ref$onScrollNext,
      ovy = _ref.ovy,
      setScrollTo = _ref.setScrollTo,
      className = _ref.className,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["dir", "customScroll", "nowrap", "ovx", "scrollStep", "behavior", "onWheel", "onScrollPrev", "onScrollNext", "ovy", "setScrollTo", "className", "children"]);

  var scrollRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // const prevRef = useRef(null);
  // const nextRef = useRef(null);
  // const isViewPrev = useInViewport(prevRef);
  // const isViewNext = useInViewport(nextRef);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      endPrev = _useState2[0],
      setEndPrev = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      endNext = _useState4[0],
      setEndNext = _useState4[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useLayoutEffect"])(function () {
    // useEffect | useLayoutEffect
    // console.log('%cuseEffect / useLayoutEffect in Scroller','color:yellow;');
    var scroller = scrollRef.current;

    if (!setScrollTo && scroller.scrollLeft <= 0) {
      setEndPrev(true);
    }

    if (setScrollTo) {
      scroller.scrollBy(setScrollTo, 0);
    }
  }, [setScrollTo]);

  var Wheel = function Wheel(e) {
    // Not Run if native scroll with shift key
    if (!e.shiftKey) {
      // console.log(e.shiftKey); // ctrlKey
      // let et = e.target;
      // let scroller = scrollRef.current; // Q.hasClass(et, "scroller");
      // let pos = e.deltaY > 0 ? scrollStep : -scrollStep;
      // scroller.scrollBy(pos, 0);
      // let left = Math.ceil(scroller.scrollLeft);
      // let width = scroller.clientWidth;
      // let scrollWidth = scroller.scrollWidth;
      // if((left + width + 1) >= scrollWidth){
      // 	console.log('Wheel END NEXT');
      // 	setEndNext(true);
      // }else{
      // 	if(endNext) setEndNext(false);
      // }
      // if(left <= 0){
      // 	console.log('Wheel END PREV');
      // 	setEndPrev(true);
      // }else{
      // 	if(endPrev) setEndPrev(false);
      // }
      // if(scroller){
      // 	scroller.scrollBy(pos, 0);
      // }else{
      // 	et.closest(".scroller").scrollBy(pos, 0);
      // }
      if (e.deltaY > 0) {
        onScrollPrevNext(e, "next", "auto");
      } else {
        onScrollPrevNext(e, "prev", "auto");
      }
    }

    onWheel(e);
  };

  var Scroll = function Scroll(e) {
    e.stopPropagation();
    var et = e.target; // const scroller = scrollRef.current;
    // console.log('Scroll e.target: ', et);

    var left = Math.ceil(et.scrollLeft);
    var width = et.clientWidth;
    var scrollWidth = et.scrollWidth;

    if (left + width + 1 >= scrollWidth) {
      // console.log('Scroll END NEXT');
      setEndNext(true);
    } else {
      if (endNext) setEndNext(false);
    }

    if (left <= 0) {
      // console.log('Scroll END PREV');
      setEndPrev(true);
    } else {
      if (endPrev) setEndPrev(false);
    }
  };

  var onScrollPrevNext = function onScrollPrevNext(e) {
    var pos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "next";
    var b = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : behavior;
    if (e) e.stopPropagation(); // -scrollStep, 0

    var scroller = scrollRef.current;
    scroller.scrollBy({
      // ...scrollOptions, 
      top: 0,
      left: pos === "next" ? scrollStep : -scrollStep,
      behavior: b
    });
    var left = Math.ceil(scroller.scrollLeft);
    var width = scroller.clientWidth;
    var scrollWidth = scroller.scrollWidth; // // console.log('scrollLeft: ', left);// 
    // // console.log('clientWidth: ', width);
    // // console.log('scrollWidth: ', scrollWidth);
    // // console.log('rect: ', scroller.getBoundingClientRect());
    // // (scroller.scrollWidth - Math.ceil(scroller.scrollLeft)) >= scroller.clientWidth
    // // (Math.ceil(scroller.scrollLeft) + scroller.clientWidth) >= scroller.scrollWidth

    if (pos === "next") {
      if (left + width + 1 >= scrollWidth) {
        // console.log('onScrollPrevNext END NEXT');
        setEndNext(true);
        if (press) clearInterval(press);
      }

      if (endPrev) setEndPrev(false);
      onScrollNext(e);
    } else {
      if (left <= 0) {
        // console.log('onScrollPrevNext END PREV');
        setEndPrev(true);
        if (press) clearInterval(press);
      }

      if (endNext) setEndNext(false);
      onScrollPrev(e);
    }
  };

  var press = null;

  var onPressedPrev = function onPressedPrev() {
    press = setInterval(onPressPrev, 100);
  };

  var onPressPrev = function onPressPrev(e) {
    // console.log('onMouseDown to onPress e: ', e);
    onScrollPrevNext(e, "prev");
  };

  var onPressedNext = function onPressedNext() {
    press = setInterval(onPressNext, 100);
  };

  var onPressNext = function onPressNext(e) {
    // console.log('onMouseDown to onPress e: ', e);
    onScrollPrevNext(e, "next"); // 
  };

  var stopPress = function stopPress() {
    clearInterval(press); // console.log('onMouseUp to stopPress press: ', press);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: "wrap-scroller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_2__["default"], {
    className: "qi qi-chevron-left btnScrollPrev" // className={Q.Cx("qi qi-chevron-left btnScrollPrev", { "d-none":endPrev })} 
    ,
    disabled: endPrev,
    onClick: function onClick(e) {
      return onScrollPrevNext(e, "prev");
    },
    onMouseDown: onPressedPrev // onPress 
    ,
    onMouseUp: stopPress,
    onMouseLeave: stopPress
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Flex__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, etc, {
    inRef: scrollRef // OPTION
    ,
    dir: dir,
    nowrap: nowrap // justify="between" 
    ,
    className: Q.Cx("scroller", {
      "ovxauto": ovx,
      "ovyauto": ovy,
      "q-scroll": customScroll
    }, className) // scroll-x
    ,
    onWheel: Wheel,
    onScroll: Scroll
  }), children), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_2__["default"], {
    className: "qi qi-chevron-right btnScrollNext" // className={Q.Cx("qi qi-chevron-right btnScrollNext", { "d-none":endNext })} 
    ,
    disabled: endNext,
    onClick: function onClick(e) {
      return onScrollPrevNext(e);
    },
    onMouseDown: onPressedNext,
    onMouseUp: stopPress,
    onMouseLeave: stopPress
  }));
}
/*
<div 
	// {...etc} 
	// nav-scroller py-1 mb-2 w-100
	className="wrap-scroller" // 
>
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/Devs.js":
/*!***************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/Devs.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Devs; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! formik */ "./node_modules/formik/dist/formik.esm.js");
/* harmony import */ var yup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! yup */ "./node_modules/yup/es/index.js");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! html-to-image */ "./node_modules/html-to-image/lib/index.js");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(html_to_image__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/html2canvas.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_BgImage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/q-ui-react/BgImage */ "./resources/js/src/components/q-ui-react/BgImage.js");
/* harmony import */ var _components_q_ui_react_Ava__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../components/q-ui-react/Ava */ "./resources/js/src/components/q-ui-react/Ava.js");
/* harmony import */ var _components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../components/q-ui-react/Img */ "./resources/js/src/components/q-ui-react/Img.js");
/* harmony import */ var _components_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../components/q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
/* harmony import */ var _components_q_ui_react_InputGroup__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../components/q-ui-react/InputGroup */ "./resources/js/src/components/q-ui-react/InputGroup.js");
/* harmony import */ var _components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../components/q-ui-react/InputQ */ "./resources/js/src/components/q-ui-react/InputQ.js");
/* harmony import */ var _components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../components/q-ui-react/ModalQ */ "./resources/js/src/components/q-ui-react/ModalQ.js");
/* harmony import */ var _components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../components/q-ui-react/Textarea */ "./resources/js/src/components/q-ui-react/Textarea.js");
/* harmony import */ var _components_q_ui_react_Range__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../components/q-ui-react/Range */ "./resources/js/src/components/q-ui-react/Range.js");
/* harmony import */ var _components_q_ui_react_Details__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../components/q-ui-react/Details */ "./resources/js/src/components/q-ui-react/Details.js");
/* harmony import */ var _components_q_ui_react_Scroller__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../components/q-ui-react/Scroller */ "./resources/js/src/components/q-ui-react/Scroller.js");
/* harmony import */ var _components_q_ui_react_Beforeunload__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../components/q-ui-react/Beforeunload */ "./resources/js/src/components/q-ui-react/Beforeunload.js");
/* harmony import */ var _components_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../components/react-confirm/util/confirm */ "./resources/js/src/components/react-confirm/util/confirm.js");
/* harmony import */ var _components_PageLoader__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../../components/PageLoader */ "./resources/js/src/components/PageLoader.js");
/* harmony import */ var _components_player_q_PlayerQ__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../../components/player-q/PlayerQ */ "./resources/js/src/components/player-q/PlayerQ.js");
/* harmony import */ var _apps_image_editor_ImgEditor__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../../apps/image-editor/ImgEditor */ "./resources/js/src/apps/image-editor/ImgEditor.js");
/* harmony import */ var _components_antd_tree_AntdTree__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../../components/antd/tree/AntdTree */ "./resources/js/src/components/antd/tree/AntdTree.js");
/* harmony import */ var _components_q_ui_react_IconPicker__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../../components/q-ui-react/IconPicker */ "./resources/js/src/components/q-ui-react/IconPicker.js");
/* harmony import */ var _components_devs_NumberField__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../../components/devs/NumberField */ "./resources/js/src/components/devs/NumberField.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // useEffect, useRef, 
// import axios from 'axios';
// import MediaQuery from 'react-responsive';



 // , toJpeg, toBlob, toPixelData, toSvg

 // import List from '@researchgate/react-intersection-list';

 // DEVS Components:
// import Flex from '../../../components/q-ui-react/Flex';








 // import Sticky from '../../../components/q-ui-react/Sticky';






 // , confirmComplex






 // ../../../css-modules/Button.module.css
// import btnStles from './Button.module.css';// scss
// END DEVS Components:
// FROM: https://blueprintjs.com/docs/#timezone/timezone-picker
// import * as moment from "moment-timezone";
// function getTimezoneMetadata(timezone, date = new Date()){
// 	const timestamp = date.getTime();
// 	const zone = moment.tz.zone(timezone);
// 	const zonedDate = moment.tz(timestamp, timezone);
// 	const offset = zonedDate.utcOffset();
// 	const offsetAsString = zonedDate.format("Z");
// 	// Only include abbreviations that are not just a repeat of the offset:
// 	// moment-timezone's `abbr` falls back to the time offset if a zone doesn't have an abbr.
// 	const abbr = zone.abbr(timestamp);
// 	const abbreviation = ABBR_REGEX.test(abbr) ? abbr : undefined;
// 	return {
// 			abbreviation,
// 			offset,
// 			offsetAsString,
// 			population: zone.population,
// 			timezone,
// 	};
// }

var FILE_SIZE = 160 * 1024;
var SUPPORTED_FORMATS = ["application/javascript", "text/javascript", "image/jpg", "image/jpeg", "image/gif", "image/png"];
var INIT_TEXTAREA_VAL = "lorem\nipsum\nsit\namet\ndolor"; // console.log('btnStles: ', btnStles);

function Devs() {
  var _formik$values, _formik$values$userfi;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      pageLoad = _useState2[0],
      setPageLoad = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      progress = _useState4[0],
      setProgress = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      numVal = _useState6[0],
      setNumVal = _useState6[1]; // const [inputNum, setInputNum] = useState([""]);


  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(Q.lang()),
      _useState8 = _slicedToArray(_useState7, 2),
      lang = _useState8[0],
      setLang = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState10 = _slicedToArray(_useState9, 2),
      draw = _useState10[0],
      setDraw = _useState10[1]; // const [bcState, setBcState] = useState(true);


  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      modal = _useState12[0],
      setModal = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState14 = _slicedToArray(_useState13, 2),
      cvs = _useState14[0],
      setCvs = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("change me"),
      _useState16 = _slicedToArray(_useState15, 2),
      textareaVal = _useState16[0],
      setTextareaVal = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState18 = _slicedToArray(_useState17, 2),
      sliderVal = _useState18[0],
      setSliderVal = _useState18[1];

  var dom2Draw = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null); // useEffect(() => {
  // 	console.log('%cuseEffect in Devs','color:yellow;');
  // 	// 'https://jspm.dev/@babel/core' | 
  // 	importShim('/storage/js/esm/clipboardApi.js').then(m => {
  // 		console.log(m);
  // 	}).catch(e => console.log(e));
  // }, []);
  // useEffect(() => {
  // 	// Q.domQ('#btnToggleFullscreenPage').click();
  // 	// bcApi.onmessage = (e) => {
  // 	// }
  // 	const onBc = (e) => {
  // 		console.log(e);
  // 		if(e.data === "logout") window.location.replace("/login");
  // 		// if(e.data === "closeBC"){
  // 		// 	setBcState(false);
  // 		// 	bcApi.removeEventListener('message', onBc);
  // 		// }
  // 		// if(e.data === "openBC"){
  // 		// 	setBcState(true);
  // 		// 	bcApi.addEventListener('message', onBc);
  // 		// }
  // 	}
  // 	bcApi.addEventListener('message', onBc);
  // 	return () => bcApi.removeEventListener('message', onBc);
  // }, []);

  var onConfirm = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var options;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              //  
              options = {
                // type: "toast",
                // title: "Title",
                // toastInfoText: "11 min ago",
                // icon: <i className="fab fa-react mr-2" />, 
                size: "sm",
                bodyClass: "text-center",
                btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next",
                // backdrop: false, 
                modalProps: {
                  // size: "sm",
                  // contentClassName: "HELL", 
                  // centered: true, 
                  // modalClassName: "modal-right", // up | down | left | right
                  returnFocusAfterClose: false,
                  onOpened: function onOpened() {
                    console.log('onOpened');
                  },
                  onClosed: function onClosed() {
                    console.log('onClosed');
                  }
                }
              };
              _context.next = 3;
              return Object(_components_react_confirm_util_confirm__WEBPACK_IMPORTED_MODULE_20__["confirm"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Are your sure?"), options);

            case 3:
              if (!_context.sent) {
                _context.next = 7;
                break;
              }

              console.log('YES');
              _context.next = 8;
              break;

            case 7:
              console.log('NO');

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function onConfirm() {
      return _ref.apply(this, arguments);
    };
  }(); // const handleOnClickComplex = () => {
  // 	confirmComplex({ message: 'hello' }).then(({ button, input }) => {
  // 		console.log('proceed! pressed:' + button + ' input:' + input);
  // 	}, () => {
  // 		console.log('cancel!');
  // 	});
  // }
  // const setLoads = (id, bool = true) => {
  // 	setBtnLoads(btnLoads.map((v, i) => i === id ? {...v, load: bool} : v));
  // }


  var validationSchema = yup__WEBPACK_IMPORTED_MODULE_3__["object"]().shape({
    userfile: yup__WEBPACK_IMPORTED_MODULE_3__["mixed"]().required("File is required").test("fileSize", "File too large", function (value) {
      return value && value.size <= FILE_SIZE;
    }).test("fileFormat", "Unsupported Format", function (value) {
      return value && SUPPORTED_FORMATS.includes(value.type);
    })
  });
  var formik = Object(formik__WEBPACK_IMPORTED_MODULE_2__["useFormik"])({
    initialValues: {
      userfile: null
    },
    validationSchema: validationSchema,
    // validateOnBlur: true,
    onSubmit: function onSubmit(values, _ref2) {
      var setSubmitting = _ref2.setSubmitting;
      // , { setFieldError, setSubmitting, setStatus,  }
      console.log('values: ', values);
      setSubmitting(true);
      var formData = new FormData();
      formData.append("userfile", values.userfile, values.userfile.name); // console.log('file: ',file);
      // console.log('formData: ',formData.get('userfile'));

      fetch("/api/upload/public/js/esm/core", {
        method: 'POST',
        // *GET, POST, PUT, DELETE, etc.
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // headers: {
        // 	// 'Content-Type': 'application/json'
        // 	// 'multipart/form-data' | application/x-www-form-urlencoded
        // 	'Content-Type': "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2), 
        // },
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: formData // formData // JSON.stringify(data) // body data type must match "Content-Type" header

      }).then(function (r) {
        return r.json();
      }) // json | text
      .then(function (res) {
        console.log("%cfetch res: ", 'color:yellow', res); // alert('Success upload file: '+ file.name + ' to path: ' + res.data);
      })["catch"](function (e) {
        console.log(e);
      });
    }
  });

  var onChangeFile = function onChangeFile(e, formik) {
    e.preventDefault();
    var file = e.target.files[0];
    console.log('file: ', file);

    if (file) {
      var reader = new FileReader();

      reader.onloadend = function () {
        return formik.setFieldValue("userfile", file);
      }; // setFileName(file.name)


      reader.readAsDataURL(file); // formik.setFieldValue("userfile", file);
    }
  };

  var onModal = function onModal() {
    setModal(!modal);
    if (cvs && !modal) setCvs(null);
  }; // const itemsRenderer = (items, ref) => (
  //   <ul className="list-group ovyauto" ref={ref} style={{ maxHeight: 200 }}>
  //     {items}
  //   </ul>
  // );
  // const itemRenderer = (index, key) => <li key={key} className="list-group-item">{index}</li>;
  // console.log('formik: ',formik);


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container-fluid py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_6__["default"], {
    title: "Devs"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"] // cssModule={btnStles.error}
  , null, "Btn"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "<IconPicker />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_IconPicker__WEBPACK_IMPORTED_MODULE_25__["default"], {
    autoFocus: true,
    label: "Icon Picker" // fa-regular-400
    ,
    url: "/storage/fonts/q-icon-v1.0/selection.json",
    onClick: function onClick(ico) {
      // , e
      console.log('onClick ico: ', ico); // console.log('onClick e: ' , e);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "<Details />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Default"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Details__WEBPACK_IMPORTED_MODULE_17__["default"], {
    open: true,
    summary: "System Requirements" // className="tree-q"

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Requires a computer running an operating system. The computer must have some memory and ideally some kind of long-term storage. An input device as well as some form of output device is recommended.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "No render children on mount"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Details__WEBPACK_IMPORTED_MODULE_17__["default"] // open 
  , {
    renderOpen: false,
    summary: "System Requirements" // className="tree-q"

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Requires a computer running an operating system. The computer must have some memory and ideally some kind of long-term storage. An input device as well as some form of output device is recommended.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "<ImgEditor /> with react-cropper - <Cropper />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_apps_image_editor_ImgEditor__WEBPACK_IMPORTED_MODULE_23__["default"] // pasteFromClipboard={false} 
  , {
    wrapClass: "bg-light" // https://raw.githubusercontent.com/roadmanfong/react-cropper/master/example/img/child.jpg
    // data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhMVFhUXFhcWGBcXFxUXFRcXFRcXFhcVFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAPkAygMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAQIDBAYHAAj/xAA+EAABAwIDBQYFAgQFBAMAAAABAAIRAyEEBTESQVFhcQYigZGxwRMyodHwI1IUQuHxBzNygsJTc5KiNDVi/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAIDAQQF/8QAJxEAAgICAwACAgEFAQAAAAAAAAECEQMhEjFBIjITUUIEI2FxgRT/2gAMAwEAAhEDEQA/AOpAJYXgEsKJ0CQlhKlQAkJYSwlQYJC8lXkANcFFfeqOa5/QofM7vftFz5BZPG9t6rp+GwNbxdc/RK8kYjKDZuS+FEcW3e5s8JErk2Y9pqzpmo93IGw8rIQ7GVXXvHGVn5b8N/HXp2+nim/uHmFL8YcQuBVMwey+0ev5orGX9sa7DZ7iOFz5FOm34I0kd32kpXPMg7aS9ratQQddoAbp1W5wma0ao7jwVtmUWh0TSeSkYZCfC2wor24JWxKm2UmyiwoSEhToSQlNGwkIT4SQgBkJE4hJCAJQEq8lQaeheSpUGCLyVVMdjmU2kuItdAEteu1gLnEAC5JXOO1HbsmWUXQ2YkHvH/du6C6Edse1j67ixpimNAN/MlZzB0do3F/QKUpX/orGJdoPfUO3UPdF4lJWxG0YGm7+yrZhjQO6LAWVjKsPbaOpufsovS5Mqu6RIWhoDj/f+iq1S99924f0Ct4h224/tG7j4KOrtTEEuO4XgcLIiwkgfVYxl3d4/T6oe/MXSQ3u8reoR9nZ/EVbu7o4G3ordPsiZvB9VVZIrsm8cn0ZEY5+vm038kRwOMdIdTcWvGlyCI4FaOr2MDm8Cs3m+RVcOQ6ZE63TxywlpCSxTjs6h2H7YGoRQxAipo1255Hut2HHgvnPD4p4Ic3UGbE2I3grt3YrPRiqAM/qNs8e/imENAlSr0LTBqQp0JEGjYSJ6QhADCkToXoQBIvBR/FCVtUIAkXlC/EgKpWx/BAEmY4vYba54LmnbHMXuPw5vqdbcFqcwxpuZ8Vz7M3bTjxN1DJLdF8cfQNsi+/nxPBTuOww7nO4a9BzUopXA/OZVbGEkk6NFufmlW2O1SKOHobThtak2HAbyeceqPh8NI8/zoheXwXl37W28bBFGtuAdNT+dB9UmV2zcapE2EpWk9T1OiMZbhNm5+Y3PsOgUWBw8gTvufYIvSZCi5WWiizQpKw2kkoBX6NGbrErHZXNNUsZgWvaQ4Ag7kaNFV6lNa1RidnKc8yR2HcXMks9E/sb2iOHxLToDZw0kHiuhY7ChwIIkEQVy7Ncu+HVLRq02PLcurFPkqZyZYcXaPomjVDmhwMggEdCnrN9hMZ8TC05N2iCtKuhO0c7VMavJyRaYNSQnJFgDUickQaUSVVxGJjeqdbMm6SEPxWJWDWWq2MJ3qB2O3IXUrkprqyVs1IfnWJAETuusk90y7joiWbVyRE6oaGzA3flvJcrduzpgqQjGWJ3n8KH5i4QGjSR4onUd3Z4jyCE1ILgeYjqSB7poGT6JsAyGvPF0eSIYZm08DiQPdUMM6KbObijOQiSXncT9kmT1jRDNNsRy/PsiNNqG0qslWv41jdXCeCii/SCdAIlQcguEx1MmNtvmjWHqM12gfFUihZMmcFWrBWxVbxUGIe1M0KmUagWF7b0QypTf+6W+IuPdbXE4um3VwHisT/iTVaaNEtMzVsRya6VuJfJC5vqafsBjdmG7jZdDC4t2QxkEAm9j/Vdhy+vtMBXRifaOTKvSykSrysSESJyRBo1InJFgHLcfLbkGVUpY13FGce+SgmLY2dLp/yr1B+N+FmnjAbGxSVaipvpGJFwoRWNxuU8kVxuI8JPlTIcW6SmsFvzjHoE3EvgbXAJ2DE7A5yfC3uVw+HZ6NzB27h/YfVDMWAC3gCPMkfZEK5lxP5+aIPm9WNnrtew91TGvBMj0WKhinS/OC0OQECiTy+pus3nDu7T6T9AjmTv/RA3n2BS5F8TYfYnZWc6wOyN7t/h909r8I2z3X5uMlBs5w2JiKbe6LkyhGEygOguc4ukEyDFjJHQi2qaGNNXZuTI06SNm7CUXAOY9wG4w7Z5d42VrL21KTgHOt6qPB4Ciyk00tplQA7QG0aRkk7Ja83sYkK0abnUxMAg2gyIvokyKtWPjbfaNHSeSJQPO8RUJ2GmBvP9kay8zT8Fnu0ODqPaWsJbOpGvnuCSO2NJ0gcxlNpmoXO4zMfRDu2dKmaFN1M2+IDEyPldfyCc7IaGw4VGPD4bsupl5O0Bdx23EX4IRXyquzDB1RxLfiWaeYd3p/NV1KKTTs5pSk1TiW8oqbL2kaQPUfddg7N4raaBP91xbCOtO9vpK6d2TxNmndCSLqZk18TdBeTaZsnLrOU8kSryAEKRR16oaFQOKKeONy6MckjnVOuS4yZsqlZ0lXH0g3RUHFQytN6L400tkmGqwYKjxdIQSFXrPi6ne+WTxCS9D1sE5o/uxxICs4V0B3INb4mT7hDcyq99o/LwAre1DOp9BAUGtFk9kNWp3SeMn6wPRBMc/bMdGoji6to5e91Uy+ltVG9QT5yqw1slPei3nfzNbwAH0lG8mEbA5T6IJiG7dT/d9BYI1hXQ4ch9lLJ9Ui2NfKzV0aYcNE9uV0zfZUOBqWCL4czZSiyzQ2ll7Bu91DjqceVkYp04QPOq3e2R+2U8uhIrZZyt3dVllEOkIflrzYIpTEOvCVDNFZ+Cb+0IR2owm1hngDSHf+JBP0laeqwaoLnzooVSf+m70Kb0WtHM6dmu/wBI89qPYroPY100xyv9Fz3FPlro3lo8yT7rovYmnHd//JT/AMkRf1ZvME6WhWVRy11oV5dkejjl2eTajoEpyH46rJgJ4R5OhJOkQ1HF5U4wSkwVCBJVtUnkp1EWMb2zkuOdCHlW8zcqcrifZ2LoqY0pcNV7pb+XTcaFSpVe+7gPZFaC9lOv3sQBw9r+yvYhwlo6eqrUGTWe7h7pMS8zP5uUntoqtWDalWQ49fU/dEcoGy0vOp0QrDM2nFu6T7WRyi2SG8L/AGVJ6VCQ3slwdGL+HifwnwU9Z3eBGkwnU9Z3Cw6nU+ClNK4HD1XM3s6YoLZXiZstBhaqyuHZBkI5gnEpGVD3xybIRmmFf8TbYZBaARwibjjqpHY0M1I8VGMyad8rewX+AZh8Jimu2iQ4HSBs7J89FoMDSeCS902gDnvuo/45kRteTZ9012ZNlbRtMu/GIsVm+3WODMK4b3lrB4mT9AUX+Nt3GiwP+JWL79GlwDnnxOy30cmxLlKiOaXGAKwVWXNnSZ8tPVdO7InvjmFyrKhous9j23HgPuqtf3EQv4GwwQ16q8q2HGvMqyF0xOSQys+AShUyZV3MH2hRUqfcJXVj+Mb/AGSlt0ebjTwTv47kq1CntGFP/BFNKONOmKnI5dmpuFSY5Ws0d3lSBXnPs70QZg6BzQrDAjaJ4K1m1aC2OMqJtMQTuJnwTPURVuZ57tkc3H8KrYmbfmqZjq129QfqPZWfhyWu3AfVSqlZVu3RBhKAYOpnqVbpvgxvP0UNSx2t4BDfcp+Ao7RuYGpKWTvY0VQUwLhIJjZEnoALlSYOuKku4oV2jxgp0vhiz6ggC9qYN+hP34IZk+ZGnDToljibjyGWVKfE3uEaiGBOy4cECwGYNdcFF6VYcVFovZczXBU6tyBI36HzCFNwrWWI37yT6oxRqSrDKLTqtTZsZOIOo4Ci4WIF5sYHXVO/hKUwwTz/AKouzKKRvsDyCf8AwobYCE47ykLLMhcg7XYr4uNqkaNIpj/aL/8AttLp/aPMBQoOfvgho4uOi45QkuJNySZPNWwRq5HBnlbSD2SUZI5XXVex1O09SubZPTgDmurdkKUMHRZHczJ6iaSmII/NynUQ1ClXUjlYMxrpcrTmxT8FTq3f4q/WHd8F0z0oolHtlHBfMiaGYL5kTlLm+xsOjimPrNLgAVWcVDVP6o6J9YwLrlyRqVI6YStWwVmLpKkqu/THMg+qp4l0uCt1h+na8X8tVuRaSMxPbYPqsLgBvmPqiddwYBPQD1KhyylLTUdZokjiVBiMRJ2tnoDeAueW3R0R0rFpsc8yd+72RUVWUWy+NrVrBx4kqrTn4e3MSJtb6oS+pNOeBI+s+6WuRvKiPNS6s4vdcn8AHLcreFy7aYLXUOFv0WiyNuyb6Kk58Y0hccE5WwA+jVpXEwr2C7QEWdZat+HpuFxF481nc67PFjrBTjOMtSKuLj9Q1ludA77cUdoZg3iuWOwlRp7s+EgotldKu4xtOPXQeKJY12mbHI+mjqDMzEahR4jN2BsucABvJQLA5eBTc58mATqdwXL6uMc593E3OpJ8BwRjx8/RcmVQ8NX2szT452hOwJDRxnVxCzWBp/Uog8/pBJgqMOZzlVTqNEauVh7K6ckLrvZmlFOeg8ly/s/RlwC65lNLZpMHKfNLh22Znei07d1UhTHp66UcwK/n8URrfKUOf8/iiNX5fBdGT+JOPoJa6NE/4zuK9QZJhX/4RqrOcYvYiTZwgumqlzJ0NjioqX+Ym498ujgFxyVzOiLqAOZd11dbpCrUWXJVpjZF0mR7KY1SK+LxIa0N3Tp09ghbqxMlFsdRtfVCqtKAVONFZWSYLGdwsJ006FRUjIc3x+6pAkHopXVNkh35BTuG9E1PWx9Go5joBWkyrGk/M0dR9lnqo8Qr+AeRpfkbH7FTyJNFMbaYcrVySdk66azPut5haQrUmuc0SWieu/6ysZlTQCHG/EHct1goAtouV90dSKjMnozdgKIswlJosxo8AnFt0jwdFqMYPzUxh6pH7H+hXDaDZcF3fOKc0XN4tI8wuL5VhTtnatsWPhaFfBKlI580blEI4gQ0NTcM79SOGiixNeXJ2FOr0Vo3Vm+yRoBBG/TqV1HBO7jegXI8hxjXMA3hdMyDHCpTEG4sUn9PKpNMzOvQuQlamynNXYcgMxYh6vTLPBVswbcFSYd8sK6JbgmTWm0VcL8yKoVhvmRVZn7CHR8/PbFRVniSTzVvH/NwhBMZmEfJBjfw8N6R9jroLiloAiWGwkNlxAmY4noFmssrPDTXqlzr91p3ndZFMsrOqA1XmXExbQNtLW8tPJceW9nVja0PxdHUjXnp4oJjRutzWnqskQAhNbAaud66qcJU9lZqzNuF5+ybXZoJRHEUgNPrr4AKrVaTceW9dSlZzOIyg+IB04o5haQcO6gLKuo8FbwOMLDpbgkyRb6HxzSezVYcECeHotnkdaWgclisFmTHNI0tfxWoyUjYbHOPBcUrXZ2LfRpgV6FXZU0n6/dXKYWpgC84dAhc27QU/hTsCDUMnrpA+pXSM70lc87RUy5wIWwfyCcbiZmq0zG+wVwuDAGneIPqpDQgF5HJvuUPrzHOV1r5HI1xC+FxWyQRoVr8gz51IhwNtD9iufYOttDZ3i4RGhXcw2Bg66kKU8e9djRna30dzy7PadYTICMU3iLFcLwOakEbPdP0K2+RdpnAgOt6FNHI19hJY/0bvFslqpYdxEhWsHig9shP+Gu2GT40cso7KOHadpFFFsc17Z5onLkwiqPnDEYw4mqGus0/KPODA+YzuuhxwpfPwmVO7qdkuMgSR3RDfNQsbYuDiC2DpGpiQesK7lzTtACoWFxEuO1AvqY3pHroZb7FzJz2U20zIIbca3P4fNXsnr/pDlM9ZMKTtJljWNJ70wYgWkXg8Ah+Vk/DDBqRJ6lQbUoF4pxn/wADjcxLASBYb+fDmUIxOPqPMuMckzE4mGi1hOwNJ4uKpzuSxxpbGc2ycXnhxKg2e94qzTdALTx+ibgmbTwOfpBHoU3SZj7RG4MHeIkb41B+ycymNQdofUdRuSPGw82ndyI3hSPw5ZFSmZY63Np/aUGBHAUGu5H83rQZbjXMsROzeBYwguXw7kd8flkWY+CHa7Jv0K5pq2dMXSC7O2NH5XsqDyI666K5T7b4OBNWLHVj93gsP2goBri5umtt3MLNVnyfRWhhjJWRnnlFnTMz7a4LZIFRzjFtljvUgBZI5yKzxstIF9dfosw4J+AxPw3gnQqn/mila7EX9XNun0bPMsOO40bmz5IFmOGiOU+aMUsZLmze39fsl/hyT+GFzwbiXklIztHDOHehWqWKINzfqI+4TcbULnOuSG7p8vEqh8Yzc+HDxXSly7OZvi9Gow+NIsHQeOvlKv4fHudqQ7r3XfngsnQrOB49f7K/hyXmWyCNeClKCRSMmzqXZvNbAXka9FtMO7aAIXGMkxbheTYwOa6r2dxO1Sa7jqmxTp0Jkj6FyF7YTwV5dFkT5fwNEipSbIIeWGD8pBMd5psYv5a70YwGHNTEii2AHExtRAEnS94aNBwVHLs2YHuEBjdktY8CajI+UzeTOsQn4HA16j2lg2gCP1GkWAPzXO6+vikd+lI14E+0OYh79gg/DaDSDv5iGd0OIgfzTzglC8JQLXAayABGlv7hP7RVYxFWBHeIi15El1pFyZUJq7NNlQaTBmLOBuRebgqbj8VQ6l8nZFndRrq3cMgACNwI3KClQM6FJhKMukG8k9UWqWEEeIWt8VQJW7KNdthZT4KmWvYd0xO69jPA3Kno4baP5+FHKOWtFJ4JuRLf9Q0IKjLIloqoXszub0tioevrBPqq+HrFktsWnUHQ+O4opmsPqu5AX8AVRrVmMgFsxrdNB2khZabZZyuo3a7ht9R15Iu94tzt+eCzOHrN29qm0t5SY8yr1TGuMQIt+FZOLs2ElRNj6m3ScCNCY8DH2WccfdEHYmzh1Hghj3WXRiVI58rtkaa9spy8VYgWstxpa4NO7QrU0cV/LxCxbmyiWCxpIDXGCNCoZcfqOjFk8ZcbRPw3jfLj5f0Q5jYcAeCO4YFzXSIc255jQkKrjMNZrx0PqpxlTaKONpMny/CbTXO5mB6eqM4DLtkERclC8DiPhyOP90VyzNgXk7hPoFDJy8LQpHsXSFMtgm38oGpXROwlUuoDatfTmsKysHuBOhv0K2PZDHNFJxf3YcSfAxP0TYnumJlRtmlOWLwXbhtbEijRZtNmC6+7hy5rYSupOzmaPmfB5TFI1qpIaDAa27nEu2bu+Vgkb7+8tXOXta2nSmk1moa6S473Od4ndv3qLKseynTftRUdUMlrxLBB+Yk3Lukab1BiK1MUw0Q5xMlwERqYB3wtq3sLSWiehVNaq34svmATPfEmAS60gc03tNhfhvAAhkd3iY1J4qLKcSGVmOdtENIiCLHjpom5/Sc2u/bmSZuZMG4uFlfNG38GTYQtAB3mN0xr9wr5eNx+hHqhDiQ2Bc281bpUe7cy7mbDoEkl6Ug/A7lGIaTBMn88CpM2xRiPb3WTfgXSNh1+RP0hPf8AGYQ17zB4klT/ABK7TH/I/UEKVWQ517WVB1AvJOhRnK6YOpt9EXqsAG7y91n5ODB4+SM1hsLsCXfnndJiKkAnj9Ai9RgPHlv/AKIPjqN726lbF8nsyS4qkUWPk9QVBXEEhPrkCwUBK64o5ZsReSpEwgqcOaavIAv4HM30iDG03gdY4SjGFrseDsGWn+U/M12ojisxKfTcQZFipTxJ9FYZWuw9XFun4VBl1WHuG8/WVTbjXGzr+qnw7gSAeocNRy5hTcGlsqsib0GKeLLS5vECOW4ojTxj3sFOe7O7fvM8RICDGod8E8RqtB2Yw7XVAXfKL/0XPJF0zoXYrKGUqe3sgOdqeXVapZ7L6xqOhvdYwfgRttMwLrph0c0uz5YrNio8CYaTu3Angm02gz3mjrtewKnp0g0sJMucQSLgAWmfCVXBbtXEjru69Fciy1RdSpvDnONSIIDRstnmXCSJ4DxUmcV/iv8AiRsl24kkkDQgRYKi2mXHutMcpKK4fLHDD1KjmwRBbbvRa4vIF+G5TlSdspG2mkUKT4ME33ga+J3L1UnjE7gq7LOPMD+qstINzuQ0C6C2QUW7Uv05n2T+0+ZUz+k2HHedzeQ5rPVsSdAbaKu0rFit8mDy0uKCNHEhrYBcDx2j9Antxv7nv/OaGylCpwRPmwi/MLQ3zJM+UlVX1vFQyvLVFIHJscXyklIvJhR7RzXk1KgBV5IlWAKEqavAoAeCnhx3EqIFOBQAXy3HmYfBHHet92QwPxtt9MnuQCBqeh3FcvpPhdY/wT+XEci30UZ4ovZaGWXRv8pYwsGx8vDS4/dz6oiqOIwZDviUzDv5h/K7rz5pf4up/wBM+YQtGs+Z8spue57toDYpucXO6FoHUzZOrkOa01A5roaAQ0Q5oAiRIvEXVZ3+SP8AX/xKuZl/l0un/Fif0RdGlyTZw+CrvaRVDxMxsxAA2TtamTMa8kDzrNzVYAwFjD84nV8b41bA38OQVnBf/Ar/AOsetNB2/wCW/qz1cpxiuTb/AGUlJ8Ul+gXtkEKw9/dsq+K3dPcpWafnBXogmICnQmpwWmCgpwKjTkAOXki8gBwSpoTggBV5IvFACyllNSoAWV5IV5YAspQU1KtAkaV03/BfNmtrVMO63xBtNPNmrfK/gVzALUf4d/8A2OG/7n/Fyx9Grs+h0kJUikVP/9k=
    // /img/iron_man.jpg
    // src="/img/iron_man.jpg" 
    ,
    onSave: function onSave(val) {
      console.log('ImgEditor onSave val: ', val);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "<Scroller />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Scroller__WEBPACK_IMPORTED_MODULE_18__["default"], {
    className: "mx-1",
    setScrollTo: 1000
  }, Array.from({
    length: 20
  }).map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "btn-group" + (i === 7 ? " active" : "")
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
      kind: "light"
    }, "Dummy - ", i + 1), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
      kind: "light",
      className: "qi qi-close xx"
    }));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Custom input range - ", "<Range />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Uncontrolled:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Range__WEBPACK_IMPORTED_MODULE_16__["default"], {
    className: "rounded-pill mb-3",
    min: 0,
    max: 100,
    defaultValue: 50 // onChange={e => setSliderVal(e.target.value)} 

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Controlled:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Range__WEBPACK_IMPORTED_MODULE_16__["default"], {
    className: "rounded-pill mb-3",
    min: 0,
    max: 100,
    value: sliderVal,
    onChange: function onChange(e) {
      return setSliderVal(e.target.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Without cursor grab:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Range__WEBPACK_IMPORTED_MODULE_16__["default"], {
    grab: false,
    className: "rounded-pill",
    min: 0,
    max: 100,
    defaultValue: 20
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "progress"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "q-progress mb-2",
    role: "progressbar",
    "aria-valuemin": "0",
    "aria-valuemax": "100",
    "aria-valuenow": progress,
    "aria-label": progress + "%",
    style: {
      "--val": progress + "%"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "range",
    className: "custom-range cpoin" // q-range
    // style={{ "--val": "50%" }}
    ,
    min: "0" // max="5"
    ,
    value: progress,
    onChange: function onChange(e) {
      return setProgress(e.target.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<PlayerQ />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_player_q_PlayerQ__WEBPACK_IMPORTED_MODULE_22__["default"] // /media/video_360.mp4
  // https://www.youtube.com/watch?v=ysz5S6PUM-U
  // https://youtu.be/6Ovj_XK6yqc
  , {
    url: "/media/video_360.mp4"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Antd <Tree /> = <AntdTree />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_antd_tree_AntdTree__WEBPACK_IMPORTED_MODULE_24__["default"], {
    data: [{
      title: 'Folder 1',
      key: '0-0',
      children: [{
        title: 'app.js',
        key: '0-0-0',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-js"
        })
      }, {
        title: 'Component.jsx',
        key: '0-0-1',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-react"
        })
      }, {
        title: 'style.css',
        key: '0-0-3',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-css"
        }) // css3 | css3-alt

      }, {
        title: 'index.html',
        key: '0-0-4',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-html"
        })
      }, {
        title: 'package.json',
        key: '0-0-5',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-json"
        })
      }, {
        title: 'routes.php',
        key: '0-0-6',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-php"
        })
      }, {
        title: 'apps.sql',
        key: '0-0-7',
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
          className: "qi i-color qi-sql"
        })
      }, {
        title: '.env',
        key: '0-0-8',
        isLeaf: true
      }]
    }, {
      title: 'Folder 2',
      key: '0-1',
      children: [{
        title: 'leaf 1-0',
        key: '0-1-0',
        isLeaf: true
      }, {
        title: 'leaf 1-1',
        key: '0-1-1',
        isLeaf: true
      }]
    }]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<Textarea />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_15__["default"], {
    defaultValue: INIT_TEXTAREA_VAL
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<Beforeunload />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Beforeunload__WEBPACK_IMPORTED_MODULE_19__["Beforeunload"], {
    when: textareaVal !== "change me" // INIT_TEXTAREA_VAL  
    ,
    msg: "CLOSE ME...!!!"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_15__["default"], {
    value: textareaVal,
    onChange: function onChange(e) {
      return setTextareaVal(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Facebook link"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    // https://l.facebook.com/l.php?u=https%3A%2F%2F
    // https://davidwalsh.name/cookiestore?fbclid=IwAR3qY5lGkyGucp4aubMvdUJyiLniUX7ul_zOpaSX8JkMghLZLRdjOmDgWn0
    href: "https://davidwalsh.name/cookiestore/?plink=123" // onClick={e => {
    // 	let et = e.target;
    // 	// let href = et.href;
    // }}
    ,
    target: "_blank",
    rel: "nofollow noopener",
    onMouseEnter: function onMouseEnter(e) {
      var et = e.target;
      var href = et.href;

      if (href.startsWith(Q.baseURL)) {
        // decodeURI | decodeURIComponent
        et.href = decodeURIComponent(href.replace(Q.baseURL + '/blank/?u=', ''));
      }
    },
    onClick: function onClick(e) {
      e.preventDefault();
      var href = Q.baseURL + '/blank/?u=' + encodeURIComponent(e.target.href); // let data = JSON.stringify({
      // 	name: "Husein",
      // 	job: "Programmer"
      // });
      // navigator.sendBeacon('https://reqres.in/api/users', data);
      // axios.post('https://reqres.in/api/users', {
      // 	name: "Husein",
      // 	job: "Programmer"
      // }).then(r => {
      // 	console.log(r)
      // }).catch(e => {
      // 	console.log(e)
      // });
      // let formData = new FormData();
      // formData.append("name", "Husein");
      // formData.append("job", "Programmer");
      // fetch('https://reqres.in/api/users', {
      // 	method:'POST', // *GET, POST, PUT, DELETE, etc.
      // 	// cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      // 	mode: 'no-cors',
      // 	// headers: {
      // 	// 	// 'Content-Type': 'application/x-www-form-urlencoded'
      // 	// 	Accept: "application/json, text/plain, */*",
      // 	// 	'Content-Type': 'application/json;charset=utf-8',
      // 	// 	// 'multipart/form-data' | application/x-www-form-urlencoded
      // 	// 	// 'Content-Type': "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2), 
      // 	// },
      // 	// referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      // 	body: formData // JSON.stringify(data) // body data type must match "Content-Type" header
      // })
      // .then(r => r.text())
      // .then(json => console.log(json))
      // .catch(e => {
      // 	console.log(e);
      // });

      window.open(href, '_blank');
    },
    onContextMenu: function onContextMenu(e) {
      var et = e.target;
      var href = et.href;

      if (!href.startsWith(Q.baseURL)) {
        et.href = Q.baseURL + '/blank/?u=' + encodeURIComponent(href);
      }
    }
  }, "Link to blank"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<Ava />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-2-next"
  }, [{
    src: "/img/avatar_man.png",
    alt: "Avatar Man",
    circle: true
  }, {
    src: "/img/users/angelina-jolie.jpg",
    alt: "Bootstrap Thumbnail",
    thumb: true
  }, // , wrapClass:"tip tipT"
  {
    src: "/img/no_img.png",
    alt: "Brad Pitt",
    round: true
  }, {
    src: "/img/no_img.png",
    alt: "Bootstrap Thumbnail",
    thumb: true
  }, {
    src: "/img/no_img.png",
    alt: "Set Background",
    bg: "#000000"
  }, {
    src: "/img/no_img.png",
    alt: "Mike Portnoy",
    circle: true
  } // , bg:"#e3f2fd"
  ].map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Ava__WEBPACK_IMPORTED_MODULE_9__["default"], {
      key: i // thumb 
      ,
      round: v.round,
      bg: v.bg // fluid 
      ,
      circle: v.circle,
      thumb: v.thumb,
      wrapClass: "tip tipT" // {v.wrapClass} 
      ,
      wrapProps: {
        "aria-label": v.alt
      },
      src: v.src,
      alt: v.alt,
      w: 70 // null
      ,
      h: 70
    });
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Capture Web Page / DOM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onClick: function onClick() {
      html2canvas__WEBPACK_IMPORTED_MODULE_5___default()(document.documentElement).then(function (canvas) {
        console.log('canvas: ', canvas);
        setModal(true); // setCvs(canvas);

        setTimeout(function () {
          Q.domQ('#frameCvs').appendChild(canvas);
        }, 1000);
      });
    }
  }, "html2canvas"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onClick: function onClick() {
      Object(html_to_image__WEBPACK_IMPORTED_MODULE_4__["toPng"])(document.documentElement).then(function (dataUrl) {
        // var img = new Image();
        // img.src = dataUrl;
        // document.body.appendChild(img);
        setDraw(dataUrl); // saveAs(dataUrl, 'dom2img.png');
        // Q.setAttr(dom2Draw.current, "style");

        setModal(true);
      })["catch"](function (e) {
        console.log('oops, something went wrong!', e);
      });
    }
  }, "html-to-image"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_14__["default"], {
    open: modal,
    toggle: onModal,
    title: "html2canvas result" // scrollable 
    ,
    position: "modal-full",
    bodyClass: "p-0",
    body: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      id: "frameCvs"
    }, draw && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      className: "img-fluid",
      src: draw,
      alt: "Draw from DOM"
    }))
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<Img />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-2-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_10__["default"], {
    thumb: true,
    w: 100,
    h: 100,
    alt: "Angelina Jolie",
    src: "/img/users/angelina-jolie.jpg" // drag={null} 

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_10__["default"], {
    thumb: true // drag 
    ,
    w: 100,
    h: 100,
    alt: "Brad Pitt",
    src: "/img/users/not.jpg"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Upload Api"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    // noValidate 
    onSubmit: formik.handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("fieldset", {
    disabled: formik.isSubmitting
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "input-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "custom-file"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    name: "userfile",
    type: "file",
    className: "custom-file-input",
    id: "customFile",
    onChange: function onChange(e) {
      return onChangeFile(e, formik);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "custom-file-label"
  }, (formik === null || formik === void 0 ? void 0 : (_formik$values = formik.values) === null || _formik$values === void 0 ? void 0 : (_formik$values$userfi = _formik$values.userfile) === null || _formik$values$userfi === void 0 ? void 0 : _formik$values$userfi.name) || "Choose file")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "input-group-append"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    kind: "light",
    className: "qi qi-close" // disabled={!(formik?.values?.userfile)} 
    ,
    type: "reset",
    onClick: function onClick() {
      var _formik$values2;

      if (formik === null || formik === void 0 ? void 0 : (_formik$values2 = formik.values) === null || _formik$values2 === void 0 ? void 0 : _formik$values2.userfile) formik.resetForm({});
    }
  }))), formik.touched.userfile && formik.errors.userfile && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "invalid-feedback d-block"
  }, formik.errors.userfile), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    type: "submit",
    className: "mt-2"
  }, "UPLOAD"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Dynamic import ES Module"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onClick: function onClick(e) {
      var et = e.target; // copy2Clipboard | https://programmeria.com/js/esm/clipboardApi.js
      // /storage/js/esm/clipboardApi.js

      importShim('/esm/core/clipboard/clipboardApi.js').then(function (m) {
        console.log(m);

        if (m) {
          // window.clipboardApi = m.default;
          // "STRING COPY..."
          m["default"](et).then(function (ok) {
            console.log('SUCCESS COPY ok: ', ok);
          })["catch"](function (e) {
            return console.log(e);
          });
        }
      })["catch"](function (e) {
        return console.log(e);
      });
    }
  }, "Copy ME"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "CSS Helper: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "i-load"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "i-load p-3 position-relative"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "i-load p-3 position-relative",
    style: {
      '--bg-i-load': '70px'
    }
  }, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Html input types:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-3-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "color",
    type: "color",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "number",
    type: "number",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "search",
    type: "search",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "tel",
    type: "tel",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "url",
    type: "url",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "date",
    type: "date",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "month",
    type: "month",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "time",
    type: "time",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_13__["default"], {
    wrap: true,
    label: "week",
    type: "week",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "html2canvas"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    ref: dom2Draw,
    className: "card"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "card-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
    className: "custom-select",
    value: lang,
    onChange: function onChange(e) {
      var val = e.target.value;
      document.documentElement.lang = val;
      setLang(val);
    }
  }, [{
    lang: "en",
    txt: "English"
  }, {
    lang: "id",
    txt: "Indonesia"
  }].map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
      key: i,
      value: v.lang
    }, v.txt);
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    kind: "info",
    className: "qi qi-book",
    onClick: function onClick() {
      console.log('Q.lang(): ', Q.lang());
      console.log('draw: ', draw);
    }
  }, " ", "GET LANG: ", lang))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "react-confirm"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onClick: onConfirm
  }, "Confirm"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<InputGroup />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputGroup__WEBPACK_IMPORTED_MODULE_12__["default"], {
    className: "mb-3",
    prepend: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "input-group-text"
    }, "Rp. ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "text",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputGroup__WEBPACK_IMPORTED_MODULE_12__["default"], {
    className: "mb-3",
    append: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
      As: "div",
      kind: "light",
      className: "qi qi-search"
    })
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "text",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputGroup__WEBPACK_IMPORTED_MODULE_12__["default"], {
    className: "mb-3",
    prepend: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "input-group-text"
    }, "*"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "input-group-text"
    }, "Rp. "))
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "text",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_InputGroup__WEBPACK_IMPORTED_MODULE_12__["default"], {
    prepend: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "input-group-text"
    }, "First and last name")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "text",
    className: "form-control"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "text",
    className: "form-control"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<NumberField />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "d-block"
  }, "Number:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_devs_NumberField__WEBPACK_IMPORTED_MODULE_26__["default"], {
    className: "form-control",
    value: numVal,
    onChange: function onChange(val) {
      return setNumVal(val);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "d-block"
  }, "Default input type=\"number\"", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "form-control",
    type: "number"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<SpinLoader />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_11__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<PageLoader />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onClick: function onClick() {
      setPageLoad(true);
      setTimeout(function () {
        return setPageLoad(false);
      }, 5000);
    }
  }, "Try Load Page"), pageLoad && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_PageLoader__WEBPACK_IMPORTED_MODULE_21__["default"], {
    progress: true // top 
    ,
    bottom: true,
    w: Q.UA.platform.type === "mobile" ? null : "calc(100% - 220px)",
    left: Q.UA.platform.type === "mobile" // className="bg-white" 

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "<BgImage />"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_BgImage__WEBPACK_IMPORTED_MODULE_8__["default"], {
    noRepeat: true,
    bgSize: "contain" // auto | contain | cover | revert 
    ,
    className: "bg-light mb-3",
    url: "/img/iron_man.jpg",
    w: "55vw" // 715 
    ,
    h: 400 // style={{width:200, height:200}} 
    // onLoad={(el, e) => {
    //   console.log('onLoad BgImage el: ', el);
    //   // console.log('onLoad BgImage e: ', e);
    // }}
    // onError={(el, e) => {
    //   // console.log('onError BgImage el: ', el);
    //   // console.log('onError BgImage e: ', e);
    // }}

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h6", null, "Error background image"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_q_ui_react_BgImage__WEBPACK_IMPORTED_MODULE_8__["default"], {
    noRepeat: true,
    As: "section" // bgSize="auto" // auto | contain | cover | revert 
    ,
    className: "mt-2 bg-info",
    url: "/img-not-available.png",
    w: 100,
    h: 100 // style={{width:200, height:200}} 
    // onLoad={(el, e) => {
    //   // console.log('onLoad BgImage e: ', e);
    // }}
    ,
    onError: function onError(el, e) {
      // console.log('onError BgImage url: ', el);
      console.log('onError BgImage e: ', e);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", null));
}
/*
<React.Fragment>
	Devs
</React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/utils/number.js":
/*!******************************************!*\
  !*** ./resources/js/src/utils/number.js ***!
  \******************************************/
/*! exports provided: num2Hex */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "num2Hex", function() { return num2Hex; });
// Number methods:
function num2Hex(n) {
  return n.toString(16);
} // function hex2Num(n){
// return parseInt(n, 16);
// }




/***/ })

}]);
//# sourceMappingURL=Devs.chunk.js.map