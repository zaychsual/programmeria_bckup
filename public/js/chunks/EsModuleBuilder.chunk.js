(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["EsModuleBuilder"],{

/***/ "./resources/js/src/components/q-ui-react/Textarea.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Textarea.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Textarea; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { Cx } from '../../utils/Q';

function Textarea(_ref) {
  var h = _ref.h,
      value = _ref.value,
      defaultValue = _ref.defaultValue,
      className = _ref.className,
      style = _ref.style,
      onChange = _ref.onChange,
      _ref$bs = _ref.bs,
      bs = _ref$bs === void 0 ? "form-control" : _ref$bs,
      etc = _objectWithoutProperties(_ref, ["h", "value", "defaultValue", "className", "style", "onChange", "bs"]);

  var elRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      txt = _useState2[0],
      setTxt = _useState2[1]; // "" | value || defaultValue || 


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(h),
      _useState4 = _slicedToArray(_useState3, 2),
      height = _useState4[0],
      setHeight = _useState4[1]; // const [parentHeight, setParentHeight] = useState("auto");
  // useEffect(() => {
  //   let el = elRef.current;
  //   if(value || defaultValue){
  //     setHeight(el.scrollHeight + 2);
  //   }
  // }, [value, defaultValue]);


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // setParentHeight(`${elRef.current.scrollHeight}px`);
    var el = elRef.current; // console.log('clientHeight: ', el.clientHeight);
    // console.log('scrollHeight: ', el.scrollHeight);

    if ((value || defaultValue) && el && el.clientHeight !== el.scrollHeight) {
      // text.length > 0 && 
      setHeight(el.scrollHeight + 2); // (el.scrollHeight + 2) + "px"
    } // if(txt || (value || defaultValue)){
    //   setHeight(el.scrollHeight + 2);
    // }
    // console.log('txt: ', txt);
    // console.log('value: ', value);
    // console.log('defaultValue: ', defaultValue);

  }, [txt, value, defaultValue]);

  var Change = function Change(e) {
    setHeight(h); // null | "auto"
    // setParentHeight(`${elRef.current.scrollHeight}px`);

    setTxt(e.target.value); // setHeight(elRef.current.scrollHeight + "px");

    if (onChange) onChange(e);
  }; // const Keyup = (e) => {
  //   let et = e.target;
  //   // setTimeout(() => {
  //     // et.style.height = 'auto';// ;padding:0
  //     // for box-sizing other than "content-box" use:
  //     // et.style.cssText = '-moz-box-sizing:content-box';
  //     let scrl = et.scrollHeight;
  //     // et.value.length === 0
  //     if(et.clientHeight !== scrl){//  && e.keyCode === 8
  //       // et.style.height = h; // 'height:' + h + 'px';
  //       setHeight((scrl + 2) + "px");// setHeight(h);
  //     }
  //     // else{
  //     //   // et.style.height = scrl + 'px';
  //     //   setHeight((scrl + 2) + "px");
  //     // }
  //     // et.classList.remove("ovhide");
  //   // }, 1);
  //   if(onKeyUp) onKeyUp(e);
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", _extends({}, etc, {
    ref: elRef,
    value: value,
    defaultValue: defaultValue,
    className: Q.Cx(bs, className),
    style: _objectSpread(_objectSpread({}, style), {}, {
      height: height
    }) // onKeyUp={Keyup} 
    ,
    onChange: Change
  }));
}
;
/*
<div
  style={{
    minHeight: parentHeight,
  }}
>
    
</div>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/EsModuleBuilder.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/EsModuleBuilder.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EsModuleBuilder; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Textarea */ "./resources/js/src/components/q-ui-react/Textarea.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_live_code_Compiler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/live-code/Compiler */ "./resources/js/src/components/live-code/Compiler.js");
/* harmony import */ var _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../utils/file/fileRead */ "./resources/js/src/utils/file/fileRead.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

 // , { useState, useEffect, useRef, } 

 // import Flex from '../../../components/q-ui-react/Flex';



 // Compiler, 

 // import Ava from '../../../components/q-ui-react/Ava';
// import SpinLoader from '../../../components/q-ui-react/SpinLoader';
// import PageLoader from '../../../components/PageLoader';

var EsModuleBuilder = /*#__PURE__*/function (_React$Component) {
  _inherits(EsModuleBuilder, _React$Component);

  var _super = _createSuper(EsModuleBuilder);

  function EsModuleBuilder(props) {
    var _this;

    _classCallCheck(this, EsModuleBuilder);

    _this = _super.call(this, props);
    _this.state = {
      loadBabel: false,
      fnName: "",
      code: "",
      exportDefault: false,
      Module: null
    };
    return _this;
  }

  _createClass(EsModuleBuilder, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      console.log('%ccomponentDidMount in EsModuleBuilder', 'color:yellow;');
      Q.getScript({
        src: "/storage/app_modules/@babel-standalone/babel.min.js",
        "data-js": "Babel"
      }).then(function () {
        if (window.Babel) {
          // window.define = windowDefine;
          _this2.setState({
            loadBabel: true
          });
        } else {
          // If failed load use jspm.io cdn
          importShim("https://jspm.dev/@babel/standalone").then(function (m) {
            // window.Babel = m.default;// store Babel to window
            // window.define = windowDefine;
            _this2.setState({
              loadBabel: true
            });
          })["catch"](function (e) {
            return console.log('importShim catch: ', e);
          });
        }
      })["catch"](function (e) {
        console.log('catch e: ', e);
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$state = this.state,
          loadBabel = _this$state.loadBabel,
          fnName = _this$state.fnName,
          code = _this$state.code,
          exportDefault = _this$state.exportDefault,
          Module = _this$state.Module;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid py-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
        title: "Es Module Builder"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "Es Module Builder"), loadBabel ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
        noValidate: true,
        onSubmit: function onSubmit(e) {
          var et = e.target;
          Q.preventQ(e);

          if (et.checkValidity()) {
            // const transformCode = generateEl(code, {}, (err) => {
            // 	console.log('generateEl err: ', err);
            // });
            // console.log('transformCode: ', transformCode);
            // transformCode(window.Babel, code, {}).then(mod => {
            // 	console.log('transformCode mod: ', mod);
            // }).catch(e => console.log(e));
            var transformCode = Object(_components_live_code_Compiler__WEBPACK_IMPORTED_MODULE_4__["evalCode"])(window.Babel, code.replace(/export|default/gm, "").trim());
            var resCode = "export " + (exportDefault ? "default " : "") + transformCode; // Q.isObj(transformCode)

            var blob = new Blob([resCode], {
              type: 'text/javascript'
            }); // let blobCode = window.URL.createObjectURL(blob);
            // , { readAs: "Text" }

            Object(_utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__["fileRead"])(blob).then(function (r) {
              console.log('r: ', r);
              console.log('r.result: ', r.result);
              importShim(r.result).then(function (m) {
                var modName = Object.keys(m);
                console.log('importShim m: ', m);
                console.log('importShim modName: ', modName);

                if (modName && modName.length > 0) {
                  if (m["default"]) {
                    // console.log('if modName: ', modName);
                    // console.log('if m.default: ', m.default);
                    _this3.setState({
                      Module: m["default"]
                    });
                  } else {
                    // console.log('else modName: ', modName);
                    // console.log('else m: ', m);
                    _this3.setState({
                      Module: m[modName[0]]
                    });
                  } // Clear blob from memory
                  // window.URL.revokeObjectURL(blobCode);

                } else {
                  console.log('module Not Found...!!!');
                }
              })["catch"](function (e) {
                return console.log(e);
              });
            })["catch"](function (e) {
              return console.log(e);
            });
            console.log('transformCode: ', transformCode);
            console.log('resCode: ', resCode); // console.log('blobCode: ', blobCode);

            Q.setClass(et, "was-validated", "remove");
          } else {
            Q.setClass(et, "was-validated");
          }
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        htmlFor: "fnName"
      }, "Function Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "text",
        className: "form-control",
        id: "fnName" // required 
        ,
        value: fnName,
        onChange: function onChange(e) {
          return _this3.setState({
            fnName: e.target.value
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        htmlFor: "code"
      }, "Code"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_2__["default"], {
        rows: "9",
        id: "code",
        required: true,
        value: code,
        onChange: function onChange(e) {
          return _this3.setState({
            code: e.target.value
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "custom-control custom-checkbox"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "checkbox",
        className: "custom-control-input",
        id: "exportDefault",
        checked: exportDefault,
        onChange: function onChange(e) {
          return _this3.setState({
            exportDefault: e.target.checked
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        className: "custom-control-label",
        htmlFor: "exportDefault"
      }, "Export default"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        type: "submit"
      }, "Submit")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "my-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", null, "Module result:"), Module ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        onClick: function onClick() {
          // window.URL.revokeObjectURL(Module);
          _this3.setState({
            Module: null
          });
        }
      }, "Remove"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Module, null)) : null)) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "LOADING"));
    }
  }]);

  return EsModuleBuilder;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);
/*
<React.Fragment></React.Fragment>
*/




/***/ })

}]);
//# sourceMappingURL=EsModuleBuilder.chunk.js.map