(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Settings"],{

/***/ "./node_modules/@babel/runtime/helpers/esm/createClass.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/createClass.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _createClass; });
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Form.js":
/*!********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Form.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Form; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import {findDOMNode} from "react-dom";
// import { Cx, toggleClass } from '../../utils/Q';
// wrapClass, upload, id, encType

function Form(_ref) {
  var inRef = _ref.inRef,
      className = _ref.className,
      fieldsetClass = _ref.fieldsetClass,
      noValidate = _ref.noValidate,
      valid = _ref.valid,
      disabled = _ref.disabled,
      onSubmit = _ref.onSubmit,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "className", "fieldsetClass", "noValidate", "valid", "disabled", "onSubmit", "children"]);

  // React.useEffect(() => {
  // console.log('%cuseEffect in Form','color:yellow;');
  // console.log(getEl());
  // console.log(findDOMNode(Form));
  // }, []);

  /*const getEl = () => { // getElement
  	try{
  		// using findDOMNode for two reasons:
  		// 1. cloning to insert a ref is unwieldy and not performant.
  		// 2. ensure that we resolve to an actual DOM node (instead of any JSX ref instance).
  		return findDOMNode(this);
  	}catch{
  		return null;// swallow error if findDOMNode is run on unmounted component.
  	}
  }*/
  var Submit = function Submit(e) {
    if (disabled) return;
    var et = e.target; // Prevent submit form if use xhr

    if (noValidate) {
      e.preventDefault();
      e.stopPropagation(); // console.log(!Q.isBool(valid));
      // if(!Q.isBool(valid)) 

      if (!Q.isBool(valid)) {
        et.classList.toggle("was-validated", !et.checkValidity());
      }
    }

    if (onSubmit && !disabled) onSubmit(e, et.checkValidity()); // onSubmit to props
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", _extends({}, etc, {
    ref: inRef,
    className: Q.Cx('form-q', {
      "disabled": disabled,
      // Q.isBool(valid) //  && !isValid
      "was-validated": noValidate && valid && Object.keys(valid).length > 0
    }, className) // enctype values: (OPTION)
    // 1. application/x-www-form-urlencoded (Default)
    // 2. multipart/form-data (form upload)
    // 3. text/plain (Spaces are converted to "+" symbols, but no special characters are encoded)
    // encType={upload ? 'multipart/form-data' : encType} 
    //data-reset={reset} 
    ,
    noValidate: noValidate // noValidate ? true : undefined
    ,
    onSubmit: Submit
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
    disabled: disabled,
    className: fieldsetClass
  }, children));
}

/***/ }),

/***/ "./resources/js/src/pages/admin/Settings.js":
/*!**************************************************!*\
  !*** ./resources/js/src/pages/admin/Settings.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Settings; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var yup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! yup */ "./node_modules/yup/es/index.js");
/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! formik */ "./node_modules/formik/dist/formik.esm.js");
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/q-ui-react/Form */ "./resources/js/src/components/q-ui-react/Form.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect } 


 // useFormik | Formik


 // import InputQ from '../../components/q-ui-react/InputQ';

 // import Loops from '../../components/q-ui-react/Loops';
// import { confirm } from '../../components/react-confirm/util/confirm';// , confirmComplex

var FormGroup = function FormGroup(_ref) {
  var label = _ref.label,
      id = _ref.id,
      iSize = _ref.iSize,
      className = _ref.className,
      wrapClass = _ref.wrapClass,
      formText = _ref.formText,
      formTextClass = _ref.formTextClass,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "text" : _ref$type,
      children = _ref.children,
      field = _ref.field,
      form = _ref.form,
      etc = _objectWithoutProperties(_ref, ["label", "id", "iSize", "className", "wrapClass", "formText", "formTextClass", "type", "children", "field", "form"]);

  // console.log('field: ', field);
  // console.log('form: ', form);
  // console.log('meta: ', meta);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group" + (wrapClass ? " " + wrapClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: id
  }, label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({}, field, etc, {
    type: type,
    className: Q.Cx("form-control", _defineProperty({}, " form-control-" + iSize, iSize), className),
    id: id
  })), formText && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    className: "form-text" + (formTextClass ? " " + formTextClass : "")
  }, formText), form && (form === null || form === void 0 ? void 0 : form.touched[id]) && (form === null || form === void 0 ? void 0 : form.errors[id]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "invalid-feedback"
  }, form.errors[id]));
};

var Schema = yup__WEBPACK_IMPORTED_MODULE_1__["object"]().shape({
  appName: yup__WEBPACK_IMPORTED_MODULE_1__["string"]() // .min(5, "Minimum 5 symbols")
  // .max(50, "Maximum 50 symbols")
  .required("App Name is required"),
  // .required(
  //   intl.formatMessage({
  //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
  //   })
  // ),
  email: yup__WEBPACK_IMPORTED_MODULE_1__["string"]().email("Wrong email format").required("Email is required"),
  theme: yup__WEBPACK_IMPORTED_MODULE_1__["string"]().required("Theme is required")
});
var initValues = {
  appName: "Programmeria",
  email: "",
  theme: "#e3f2fd"
};
function Settings() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(initValues),
      _useState2 = _slicedToArray(_useState, 2),
      vals = _useState2[0],
      setVals = _useState2[1]; // useEffect(() => {
  // 	console.log('%cuseEffect in Settings','color:yellow;');
  // }, []);


  var onSave = function onSave(e, valid, formik) {
    // , valid
    // console.log('onSave e: ', e);
    console.log('onSave valid: ', valid);
    console.log('onSave formik: ', formik);

    if (valid) {
      formik.handleSubmit();
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_3__["default"], {
    title: "Settings"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
    className: "hr-h hr-left mb-4"
  }, "Settings"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row flex-row-reverse"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card bg-light shadow-sm"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card-header"
  }, "Header"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card-body"
  }, "Info"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(formik__WEBPACK_IMPORTED_MODULE_2__["Formik"], {
    initialValues: vals // initValues 
    ,
    validationSchema: Schema,
    onSubmit: function onSubmit(values, fn) {
      fn.setSubmitting(true);
      setTimeout(function () {
        console.log("values: ", values); // JSON.stringify(values, null, 2)

        setVals(values);
        fn.setSubmitting(false);
      }, 3000);
    }
  }, function (formik) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_4__["default"], {
      noValidate: true,
      valid: formik.touched,
      className: "card shadow-sm" + (formik.isSubmitting ? " i-load" : "") // style={{ '--iload-size': formik.isSubmitting ? "70" : undefined }}
      ,
      disabled: formik.isSubmitting,
      onSubmit: function onSubmit(e, valid) {
        return onSave(e, valid, formik);
      },
      onReset: formik.handleReset
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "card-header bg-light py-2 position-sticky t48 zi-1"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
      type: "reset",
      kind: "dark"
    }, "Reset"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_5__["default"], {
      type: "submit"
    }, "Save")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "card-body"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(formik__WEBPACK_IMPORTED_MODULE_2__["Field"], {
      id: "appName",
      name: "appName",
      label: "App Name",
      required: true,
      component: FormGroup
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(formik__WEBPACK_IMPORTED_MODULE_2__["Field"], {
      id: "email",
      name: "email",
      label: "Email",
      type: "email",
      required: true,
      component: FormGroup
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(formik__WEBPACK_IMPORTED_MODULE_2__["Field"], {
      id: "theme",
      name: "theme",
      label: "Theme",
      type: "color",
      className: "p-1 cpoin",
      required: true,
      component: FormGroup
    })));
  }))));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=Settings.chunk.js.map