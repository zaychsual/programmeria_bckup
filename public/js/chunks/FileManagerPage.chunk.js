(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["FileManagerPage"],{

/***/ "./resources/js/src/apps/file-manager/FileManager.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/apps/file-manager/FileManager.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FileManager; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
/* harmony import */ var _components_browser_RouterMemory__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/browser/RouterMemory */ "./resources/js/src/components/browser/RouterMemory.js");
/* harmony import */ var _parts_HistoryEntries__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./parts/HistoryEntries */ "./resources/js/src/apps/file-manager/parts/HistoryEntries.js");
/* harmony import */ var _context_FileManagerContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./context/FileManagerContext */ "./resources/js/src/apps/file-manager/context/FileManagerContext.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useState, useRef, useEffect }

 // , useHistory, useLocation, Redirect, 




 // BtnRoute
// import PageLoader from '../../components/PageLoader';



 // const INIT_PATHS = [{ pathname:"/", label:"root", exact:true }];

function App(_ref) {
  var _ref$data = _ref.data,
      data = _ref$data === void 0 ? INIT_PATHS : _ref$data,
      routes = _ref.routes,
      className = _ref.className;

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_FileManagerContext__WEBPACK_IMPORTED_MODULE_8__["FileManagerContext"]),
      themeState = _useContext.themeState,
      viewState = _useContext.viewState,
      setAppState = _useContext.setAppState; // const [themes, setThemes] = useState(themeState); // themeValue()
  // const [views, setViews] = useState(viewState);
  // console.log('themeState: ', themeState);
  // console.log('views: ', views);


  var inputTypeRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      paths = _useState2[0],
      setPaths = _useState2[1]; // NOT FIX: data | null


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(data),
      _useState4 = _slicedToArray(_useState3, 2),
      breadcrumbs = _useState4[0],
      setBreadcrumbs = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState6 = _slicedToArray(_useState5, 2),
      initIndex = _useState6[0],
      setInitIndex = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState8 = _slicedToArray(_useState7, 2),
      routers = _useState8[0],
      setRouters = _useState8[1]; // NOT FIX: routes | null


  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState10 = _slicedToArray(_useState9, 2),
      pathType = _useState10[0],
      setPathType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      openDownshift = _useState12[0],
      setOpenDownshift = _useState12[1]; // For downshift


  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState14 = _slicedToArray(_useState13, 2),
      ctxMenu = _useState14[0],
      setCtxMenu = _useState14[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // console.log('%cuseEffect in FileManager','color:yellow', theme);
    // setRouters(routes);
    if (data && routes) {
      setRouters(routes);
      setPaths(data);
      setInitIndex(data.length - 1);
    } // setInitIndex(data.length - 1);

  }, [data, routes]); // e = if use form | val = onKeyUp input

  var onEnterPath = function onEnterPath(val) {
    // Q.preventQ(e);
    if (val.length > 0) {
      // pathType | val
      var pathArr = val.split("/");
      pathArr.shift();
      var parsePath = pathArr.map(function (v, i) {
        var _data$i;

        return {
          label: ((_data$i = data[i]) === null || _data$i === void 0 ? void 0 : _data$i.label) || "",
          pathname: "/" + v // exact: data[i]?.exact

        };
      }); // let parseRoutes = parsePath.map((v, i) => ({ ...v, component: DIRS[i] }));
      // parseRoutes.push(OBJ_404);

      console.log('onEnterPath val: ', val);
      console.log('onEnterPath pathArr: ', pathArr); // /programmeria/public

      console.log('onEnterPath parsePath: ', parsePath); // console.log('onEnterPath parseRoutes: ', parseRoutes);
      // inputTypeRef.current.blur();

      setPathType(null);
      setBreadcrumbs(parsePath); // setPaths
      // setRouters(parseRoutes);
      // setTimeout(() => {
      // setRouters(parseRoutes);
      // setTimeout(() => setInitIndex(pathArr.length - 1), 9);
      // }, 9);
    }
  };

  var onSetEditPath = function onSetEditPath(downshift) {
    // data[initIndex].pathname | data[0].pathname | paths
    setPathType(breadcrumbs.map(function (v) {
      return v.pathname;
    }).join(""));
    setTimeout(function () {
      var _inputTypeRef$current;

      (_inputTypeRef$current = inputTypeRef.current) === null || _inputTypeRef$current === void 0 ? void 0 : _inputTypeRef$current.select();
      if (Q.isBool(downshift)) setOpenDownshift(true);
    }, 9);
  };

  var getEntries = function getEntries(entries) {
    // return entries.reduce((acc, cur) => {
    //   const x = acc.find(v => v.pathname === cur.pathname);
    //   if(!x){
    //     return acc.concat([cur]);
    //   }
    //   return acc;
    // }, []);
    var path = "";
    return entries.reduce(function (acc, cur, curIndex, arr) {
      var x = acc.find(function (v) {
        return v.pathname === cur.pathname;
      });

      if (!x) {
        var _arr2;

        path += ((_arr2 = arr[curIndex - 1]) === null || _arr2 === void 0 ? void 0 : _arr2.pathname) || "";
        return acc.concat([_objectSpread(_objectSpread({}, cur), {}, {
          pathname: path + cur.pathname
        })]);
      }

      return acc;
    }, []);
  };

  var dark2light = themeState === "dark" ? "light" : "dark";

  if (!paths && !routers) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "LOADING");
  }

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_browser_RouterMemory__WEBPACK_IMPORTED_MODULE_6__["default"] // key={initIndex} // DEV OPTION: For re-render when change path typing
  , {
    initialEntries: paths // ["/", "/public", { pathname: "/img" }] 
    ,
    initialIndex: initIndex // 2
    // keyLength={2} 

  }, function (_ref2) {
    var history = _ref2.history,
        location = _ref2.location;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
      nowrap: true,
      dir: "column",
      className: Q.Cx("file-manager", {
        "bg-dark text-light": themeState === "dark"
      }, className)
    }, console.log('history: ', history), console.log('location: ', location), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
      dir: "row",
      className: "flexno position-relative ml-1-next"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "btn-group btn-group-sm"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      kind: dark2light,
      outline: true,
      className: "tip tipBL qi qi-arrow-left",
      disabled: !history.canGo(-1),
      "aria-label": "Back",
      onClick: history.goBack
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      kind: dark2light,
      outline: true,
      className: "qi qi-arrow-right tip tipBL",
      disabled: !history.canGo(1),
      "aria-label": "Forward",
      onClick: history.goForward
    })), Q.isStr(pathType) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_parts_HistoryEntries__WEBPACK_IMPORTED_MODULE_7__["default"], {
      open: openDownshift,
      entries: getEntries(history.entries),
      downshiftProps: {
        onChange: function onChange(selection) {
          onEnterPath(selection.pathname);
          setOpenDownshift(false);
        }
      },
      dropdownProps: {
        // as: "form", 
        // noValidate: true, 
        // onSubmit: e => onEnterPath(e), 
        className: "d-flex flex1"
      },
      dropdownToggleProps: {
        as: "div",
        bsPrefix: "w-100"
      },
      dropdownMenuProps: {
        className: "dd-sm py-1 w-100"
      },
      inputProps: {
        ref: inputTypeRef,
        className: "form-control form-control-sm shadow-none",
        type: "search",
        autoFocus: true,
        spellCheck: false,
        required: true,
        onBlur: function onBlur() {
          setPathType(null);
          setOpenDownshift(false);
        },
        onKeyDown: function onKeyDown(e) {
          return e.stopPropagation();
        } // onKeyUp: e => {
        //   e.stopPropagation();
        //   if(e.key === "Enter"){
        //     setPathType(e.target.value);
        //     // onEnterPath(e.target.value);
        //   }
        //   // console.log('onKeyUp key: ', e.key);
        //   // console.log('onKeyUp value: ', e.target.value);
        // },
        // onChange: e => setPathType(e.target.value) 

      }
    }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
      "aria-label": "breadcrumb",
      className: "flex1 breadcrumb-path"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ol", {
      className: Q.Cx("breadcrumb flex-nowrap p-0 mb-0 border", {
        "bg-dark text-light": themeState === "dark",
        "bg-white": themeState === "light"
      })
    }, breadcrumbs.map(function (v) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: v.pathname,
        className: "breadcrumb-item"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_5__["default"], {
        noNewTab: true,
        size: "sm",
        exact: v.exact,
        strict: v.strict,
        to: v.pathname // btn={dark2light} outline 
        ,
        btn: "flat" // kind={dark2light} 
        ,
        className: "px-1 border-0 zi-1" //  position-relative
        ,
        onContextMenu: function onContextMenu(e) {
          Q.preventQ(e);
          setCtxMenu(v.pathname);
        }
      }, v.label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
        className: "position-absolute r0" // inset-0 
        ,
        show: ctxMenu === v.pathname,
        onToggle: function onToggle() {
          return setCtxMenu(null);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
        as: "div",
        bsPrefix: "btn btn-sm px-1 border-0 position-relative zi--1 shadow-none"
      }, v.label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
        className: Q.Cx("dd-sm py-1", {
          "dropdown-menu-dark": themeState === "dark"
        }),
        onClick: Q.preventQ
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
        as: "div"
      }, "Copy address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
        as: "div",
        onClick: onSetEditPath
      }, "Edit address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
        as: "div"
      }, "Delete history"))));
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      onClick: onSetEditPath,
      className: "breadcrumb-item before-no flex1 cpoin"
    }, " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      size: "sm",
      kind: dark2light,
      outline: true,
      className: "dropdown-toggle",
      title: "Previous locations",
      onClick: function onClick() {
        return onSetEditPath(true);
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
      alignRight: true
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
      size: "sm",
      variant: "outline-" + dark2light,
      bsPrefix: "qi qi-bars",
      title: "Options"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
      className: Q.Cx("dd-sm py-1", {
        "dropdown-menu-dark": themeState === "dark"
      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: "dropdown-header"
    }, "View as"), ["grid", "list", "details"].map(function (v, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, _extends({
        key: i
      }, Q.DD_BTN, {
        className: "text-capitalize",
        active: viewState === v,
        onClick: function onClick() {
          return setAppState({
            view: v
          });
        } // onChangeAppState

      }), v);
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      className: "border-" + dark2light
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: "dropdown-header"
    }, "Theme"), ["dark", "light"].map(function (v, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, _extends({
        key: i
      }, Q.DD_BTN, {
        className: "text-capitalize",
        active: themeState === v,
        onClick: function onClick() {
          return setAppState({
            theme: v
          });
        }
      }), v);
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      className: "input-group input-group-sm w-auto"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "search",
      className: "form-control shadow-none",
      placeholder: "Search"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "input-group-append"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      As: "div",
      kind: dark2light,
      outline: true,
      className: "qi qi-search"
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
      dir: "column",
      className: "flex-auto mnh-100"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"] // fallback={<PageLoader bottom left={isMobile} w={isMobile ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />}
    , {
      fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "LOADING")
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null, Array.isArray(routers) ? routers.map(function (v, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        key: i,
        exact: v.exact,
        strict: v.strict,
        path: v.pathname,
        component: v.component
      });
    }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      exact: true,
      path: "/"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "This folder is empty."))))));
  });
} // data, routes, className


function FileManager(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_context_FileManagerContext__WEBPACK_IMPORTED_MODULE_8__["FileManagerProvider"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(App, props));
}

/***/ }),

/***/ "./resources/js/src/apps/file-manager/context/FileManagerContext.js":
/*!**************************************************************************!*\
  !*** ./resources/js/src/apps/file-manager/context/FileManagerContext.js ***!
  \**************************************************************************/
/*! exports provided: FileManagerContext, FileManagerProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileManagerContext", function() { return FileManagerContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileManagerProvider", function() { return FileManagerProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }


var FileManagerContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();
var Provider = FileManagerContext.Provider;

var FileManagerProvider = function FileManagerProvider(_ref) {
  var children = _ref.children;

  // const theme = localStorage.getItem("theme");
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("light"),
      _useState2 = _slicedToArray(_useState, 2),
      themeState = _useState2[0],
      setThemeState = _useState2[1]; // theme || "light"


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("grid"),
      _useState4 = _slicedToArray(_useState3, 2),
      viewState = _useState4[0],
      setViewState = _useState4[1];

  var setApp = function setApp(_ref2) {
    var theme = _ref2.theme,
        view = _ref2.view;
    // localStorage.setItem("theme", theme);
    // setThemeState({ theme, view });
    if (theme) setThemeState(theme);
    if (view) setViewState(view);
  }; // const themeValue = () => {
  // if(themeState) return themeState;
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Provider, {
    value: {
      themeState: themeState,
      // themeValue, 
      viewState: viewState,
      setAppState: function setAppState(val) {
        return setApp(val);
      }
    }
  }, children);
};



/***/ }),

/***/ "./resources/js/src/apps/file-manager/parts/Dir.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/apps/file-manager/parts/Dir.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Dir; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
/* harmony import */ var _components_q_ui_react_Table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/q-ui-react/Table */ "./resources/js/src/components/q-ui-react/Table.js");
/* harmony import */ var _context_FileManagerContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../context/FileManagerContext */ "./resources/js/src/apps/file-manager/context/FileManagerContext.js");
 // , { useState }

 // Switch, Route, useHistory, useLocation, Redirect, 
// import Dropdown from 'react-bootstrap/Dropdown';




function Dir(_ref) {
  var data = _ref.data,
      className = _ref.className,
      _ref$As = _ref.As,
      As = _ref$As === void 0 ? "section" : _ref$As,
      _ref$emptyTxt = _ref.emptyTxt,
      emptyTxt = _ref$emptyTxt === void 0 ? "This folder is empty." : _ref$emptyTxt;

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_FileManagerContext__WEBPACK_IMPORTED_MODULE_4__["FileManagerContext"]),
      themeState = _useContext.themeState,
      viewState = _useContext.viewState; // "grid", "list", "details"


  var history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["useHistory"])(); // console.log('themeState: ', themeState);
  // OPTION list view: column-count, column-gap

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, {
    className: Q.Cx({
      "p-3": viewState !== "details"
    }, className)
  }, Array.isArray(data) ? ["grid", "list"].includes(viewState) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Q.Cx({
      "row": viewState === "grid",
      "card-columns text-truncate": viewState === "list"
    })
  }, data.map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: Q.Cx({
        "col flex-grow-0": viewState === "grid" // "px-3": viewState === "list", 

      })
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_2__["default"], {
      noNewTab: true,
      to: v.path,
      btn: "flat",
      size: "sm" // listGroup={viewState === "details"} 
      ,
      className: Q.Cx("i-color qi qi-" + (v.type === "file" ? v.ext : "folder"), {
        "mb-2 btn-app qi-3x truncate-line mxh-no": viewState === "grid",
        "text-left text-truncate mw-100 q-mr": viewState === "list"
      }),
      title: "Date created: " + v.created_at + "\nFolders: " + v.name,
      draggable: true
    }, v.name));
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Table__WEBPACK_IMPORTED_MODULE_3__["default"], {
    responsive: true,
    responsiveStyle: {
      height: 'calc(100vh - 97px)'
    },
    responsiveClass: "border-top-0",
    fixThead: true,
    customScroll: true,
    hover: true,
    sm: true,
    border: "less",
    kind: themeState,
    thead: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, ["Name", "Date modified", "Type", "Size"].map(function (v, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        key: i,
        scope: "col"
      }, v);
    })),
    tbody: data.map(function (v, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
        key: i,
        className: "cpoin",
        onClick: function onClick() {
          return history.push(v.path);
        },
        draggable: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        title: v.name,
        className: "i-color q-mr qi qi-" + (v.type === "file" ? v.ext : "folder")
      }, v.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        title: v.create_at
      }, v.create_at), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        title: v.type
      }, v.type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        title: v.size
      }, v.size));
    })
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Q.Cx({
      "p-3": viewState === "details"
    })
  }, emptyTxt));
}

/***/ }),

/***/ "./resources/js/src/apps/file-manager/parts/HistoryEntries.js":
/*!********************************************************************!*\
  !*** ./resources/js/src/apps/file-manager/parts/HistoryEntries.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistoryEntries; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var downshift__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! downshift */ "./node_modules/downshift/dist/downshift.esm.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




function HistoryEntries(_ref) {
  var entries = _ref.entries,
      open = _ref.open,
      downshiftProps = _ref.downshiftProps,
      downshiftMenuProps = _ref.downshiftMenuProps,
      dropdownProps = _ref.dropdownProps,
      dropdownToggleProps = _ref.dropdownToggleProps,
      dropdownMenuProps = _ref.dropdownMenuProps,
      inputProps = _ref.inputProps;
  if (!entries) return null; // let entries = history.entries;

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(downshift__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, downshiftProps, {
    // onChange={selection => {
    // alert(selection ? `You selected ${selection.value}` : 'Selection Cleared')
    // }}
    itemToString: function itemToString(item) {
      return item ? item.pathname : "";
    }
  }), function (_ref2) {
    var getRootProps = _ref2.getRootProps,
        getInputProps = _ref2.getInputProps,
        getItemProps = _ref2.getItemProps,
        getMenuProps = _ref2.getMenuProps,
        isOpen = _ref2.isOpen,
        inputValue = _ref2.inputValue,
        highlightedIndex = _ref2.highlightedIndex,
        selectedItem = _ref2.selectedItem;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({}, getRootProps(_objectSpread({}, dropdownProps), {
      suppressRefError: true
    }), {
      show: open || isOpen
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, dropdownToggleProps, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", getInputProps(_objectSpread({}, inputProps)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", getMenuProps(_objectSpread({}, downshiftMenuProps)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, dropdownMenuProps, entries.filter(function (item) {
      return !inputValue || item.pathname.includes(inputValue);
    }).map(function (item, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, _extends({}, Q.DD_BTN, getItemProps({
        key: item.pathname,
        index: index,
        item: item,
        active: selectedItem === item,
        // || highlightedIndex === index
        className: highlightedIndex === index && "highlighted" // style: {
        // backgroundColor:
        // highlightedIndex === index ? 'lightgray' : 'white',
        // fontWeight: selectedItem === item ? 'bold' : 'normal',
        // },

      })), item.pathname);
    }))));
  });
} // const HistoryList = ({ history, onClick = Q.noop }) => {
// 	let entries = history.entries;
// 	if(entries){
// 		console.log('entries: ',entries);
// 		// console.log('window.history: ', window.history);
// 		if(entries.length > 1){
// 			return (
// 				history.entries.map((v, i) => 
// 					<Dropdown.Item key={i} onClick={() => onClick(v)} as="button" type="button">{v.pathname}</Dropdown.Item>
// 				)
// 			);
// 		}
// 		return null;
// 	}
// }
// const renderDataList = (history) => {
// 	const entries = history.entries;
// 	if(entries && entries.length > 1){
// 		const noDup = [...(new Set(entries.map(v => v.pathname)))];
// 		console.log('entries: ', entries);
// 		console.log('noDup: ', noDup);
// 		// console.log('window.history: ', window.history);
// 		// [...(new Set(MONACO_LANGS.map(v => v.type)))];
// 		// [...(new Set(entries.map(v => <option key={v.key} value={v.pathname} />)))];
// 		return (
// 			<datalist id={INPUT_ID}>
// 				{noDup.map((v, i) => <option key={i} value={v} />)}
// 			</datalist>
// 		);
// 	}
// }

/* <Dropdown // className="position-absolute inset-0" 
	alignRight 
	show={ddPrevLocations} 
	onToggle={(open) => {
		setDdPrevLocations(open);
		if(open) onSetEditPath();
	}}
>
	<Dropdown.Toggle size="sm" variant="light" title="Previous locations">
		
	</Dropdown.Toggle>
	<Dropdown.Menu className="py-1 dd-sm dd-prevlocs"
		popperConfig={{
			modifiers: [{
				name: "offset",
				options: {
					offset: [-30, 0]
				}
			}]
		}}
	>
		<HistoryList history={history} onClick={(path) => setPathType(path.pathname)} />
	</Dropdown.Menu>
</Dropdown> */

/***/ }),

/***/ "./resources/js/src/components/browser/RouterMemory.js":
/*!*************************************************************!*\
  !*** ./resources/js/src/components/browser/RouterMemory.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RouterMemory; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function RouterMemory(_ref) {
  var children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["children"]);

  // OPTIONS: ???
  // const getConfirm = (msg, cb) => {
  // cb(window.confirm(msg));
  // }
  // ???
  // const createPath = (location) => {
  // return location.pathname + location.search;
  // }	
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["MemoryRouter"], etc, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    render: function render(_ref2) {
      var history = _ref2.history,
          location = _ref2.location;
      return children({
        history: history,
        location: location
      });
    }
  }));
}

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Table.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Table.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Table; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // { useState, useEffect }
// import ResizeSensor from './ResizeSensor';

function Table(_ref) {
  var _ref$responsive = _ref.responsive,
      responsive = _ref$responsive === void 0 ? true : _ref$responsive,
      responsiveClass = _ref.responsiveClass,
      responsiveSize = _ref.responsiveSize,
      responsiveStyle = _ref.responsiveStyle,
      border = _ref.border,
      hover = _ref.hover,
      strip = _ref.strip,
      sm = _ref.sm,
      kind = _ref.kind,
      layout = _ref.layout,
      thead = _ref.thead,
      tbody = _ref.tbody,
      tfoot = _ref.tfoot,
      caption = _ref.caption,
      className = _ref.className,
      theadClass = _ref.theadClass,
      tbodyClass = _ref.tbodyClass,
      tfootClass = _ref.tfootClass,
      captionClass = _ref.captionClass,
      fixThead = _ref.fixThead,
      _ref$customScroll = _ref.customScroll,
      customScroll = _ref$customScroll === void 0 ? true : _ref$customScroll;

  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in Table','color:yellow;');
  // }, []);
  var Tb = function Tb() {
    var _Q$Cx;

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("table", {
      className: Q.Cx("table", (_Q$Cx = {}, _defineProperty(_Q$Cx, "table-border" + (border === true ? "ed" : "less"), border), _defineProperty(_Q$Cx, "table-striped", strip), _defineProperty(_Q$Cx, "table-hover", hover), _defineProperty(_Q$Cx, "table-sm", sm), _defineProperty(_Q$Cx, "table-" + kind, kind), _defineProperty(_Q$Cx, "tb-" + layout, layout), _Q$Cx), className)
    }, caption && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("caption", {
      className: captionClass
    }, caption), thead && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", {
      className: theadClass
    }, thead), tbody && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", {
      className: tbodyClass
    }, tbody), tfoot && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tfoot", {
      className: tfootClass
    }, tfoot));
  }; // const Tbl = () => 
  // 	fixThead ? 
  // 		<ResizeSensor 
  // 			// className="" 
  // 			// wait={1000} 
  // 			onResizeEnd={(s) => {
  // 				console.log('onResizeEnd size: ', s);
  // 			}}
  // 		>
  // 			{Tb()}
  // 		</ResizeSensor>
  // 	: Tb();


  if (responsive || fixThead) {
    var _Q$Cx2;

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: Q.Cx((_Q$Cx2 = {}, _defineProperty(_Q$Cx2, "table-responsive" + (responsiveSize ? "-" + responsiveSize : ""), responsive), _defineProperty(_Q$Cx2, "ovauto thead-fix", fixThead), _defineProperty(_Q$Cx2, "q-scroll ovscroll-auto", customScroll), _Q$Cx2), responsiveClass),
      style: responsiveStyle // DEV OPTION: For fixed thead

    }, Tb());
  }

  return Tb();
}

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/FileManagerPage.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/FileManagerPage.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FileManagerPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
/* harmony import */ var _apps_file_manager_FileManager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../apps/file-manager/FileManager */ "./resources/js/src/apps/file-manager/FileManager.js");
/* harmony import */ var _apps_file_manager_parts_Dir__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../apps/file-manager/parts/Dir */ "./resources/js/src/apps/file-manager/parts/Dir.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // , { useState, useEffect, useRef, } 


 // BtnRoute




function RootDir() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_file_manager_parts_Dir__WEBPACK_IMPORTED_MODULE_4__["default"], {
    emptyTxt: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "programmeria"), " directory is empty.")
  });
}

function PublicDir() {
  // Public directory
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_file_manager_parts_Dir__WEBPACK_IMPORTED_MODULE_4__["default"], null);
}

var DATE_DUMMY = new Date();

function ImgDir() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_file_manager_parts_Dir__WEBPACK_IMPORTED_MODULE_4__["default"], {
    data: [// File folder
    {
      type: "folder",
      name: "FinanSys Flow Landing Page BG long text test data users",
      path: "/users",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "bg",
      path: "/bg",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "icons",
      path: "/icons",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "svg",
      path: "/programmeria/public/img/svg",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "logos",
      path: "/programmeria/public/img/logos",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "dummy",
      path: "/programmeria/public/img/dummy",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "sprite",
      path: "/programmeria/public/img/sprite",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "posts",
      path: "/programmeria/public/img/posts",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "avatar",
      path: "/programmeria/public/img/avatar",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "reports",
      path: "/programmeria/public/img/reports",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "poster",
      path: "/programmeria/public/img/poster",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "ads",
      path: "/programmeria/public/img/ads",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "banner",
      path: "/programmeria/public/img/banner",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "etc",
      path: "/programmeria/public/img/etc",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "misc",
      path: "/programmeria/public/img/misc",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "library",
      path: "/programmeria/public/img/library",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "thumbs",
      path: "/programmeria/public/img/thumbs",
      created_at: DATE_DUMMY
    }, {
      type: "folder",
      name: "articles",
      path: "/programmeria/public/img/articles",
      created_at: DATE_DUMMY
    }, {
      type: "file",
      name: "angelina-jolie.jpg",
      path: "/public/img/users/angelina-jolie.jpg",
      created_at: DATE_DUMMY,
      size: "145 KB",
      ext: "jpg"
    }, {
      type: "file",
      name: "husein.jpg",
      path: "/public/img/users/husein.jpg",
      created_at: DATE_DUMMY,
      size: "182 KB",
      ext: "jpg"
    }, {
      type: "file",
      name: "favicon.ico",
      path: "/public/favicon.ico",
      created_at: DATE_DUMMY,
      size: "1.12 KB",
      ext: "ico"
    }, {
      type: "file",
      name: "favicon-offline.png",
      path: "/public/favicon-offline.png",
      created_at: DATE_DUMMY,
      size: "624 B",
      ext: "png"
    }, {
      type: "file",
      name: "Q.js",
      path: "/public/js/Q.js",
      created_at: DATE_DUMMY,
      size: "27 KB",
      ext: "js"
    }, {
      type: "file",
      name: "app.js",
      path: "/public/css/app.css",
      created_at: DATE_DUMMY,
      size: "72 KB",
      ext: "css"
    }]
  });
}

function Dir404() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Not Found");
} // const INPUT_ID = "dir-url-" + Q.Qid();
// ["/", "/public", "/img"]


var INIT_PATHS = [{
  pathname: "/programmeria",
  label: "programmeria",
  exact: true
}, {
  pathname: "/public",
  label: "public"
}, {
  pathname: "/img",
  label: "img"
} // { pathname: "*" }, 
];
var DIRS = [RootDir, PublicDir, ImgDir // Dir404, 
];
var OBJ_404 = {
  pathname: "*",
  component: Dir404
};
var ROUTES = INIT_PATHS.map(function (v, i) {
  return _objectSpread(_objectSpread({}, v), {}, {
    component: DIRS[i]
  });
}); // ROUTES.push(OBJ_404);

function FileManagerPage() {
  // const [pageLoad, setPageLoad] = useState(false);
  // useEffect(() => {
  // console.log('%cuseEffect in FileManagerPage','color:yellow;');
  // }, []);
  // div className="container-fluid p-2"
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid d-flex flex-column flex-auto mnh-100 p-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "File Manager"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_file_manager_FileManager__WEBPACK_IMPORTED_MODULE_3__["default"] // theme="dark" 
  // view="list" 
  , {
    className: "flex-auto mnh-100 border rounded shadow-sm",
    data: INIT_PATHS,
    routes: ROUTES
  }));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=FileManagerPage.chunk.js.map