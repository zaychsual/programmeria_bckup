(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ImgMapPage"],{

/***/ "./resources/js/src/components/img-map/ImgMap.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/img-map/ImgMap.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ImgMap; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _q_ui_react_Img__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../q-ui-react/Img */ "./resources/js/src/components/q-ui-react/Img.js");
/* harmony import */ var _q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/file/fileRead */ "./resources/js/src/utils/file/fileRead.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

 // import Immutable from 'Immu'





var TOOLS = [{
  label: "Polygon"
}, {
  label: "Rectangle"
}, {
  label: "Circle"
}, {
  label: "Select"
}, {
  label: "Delete"
} // {label:"Test"},
];

var ImgMap = /*#__PURE__*/function (_React$Component) {
  _inherits(ImgMap, _React$Component);

  var _super = _createSuper(ImgMap);

  function ImgMap(props) {
    var _this;

    _classCallCheck(this, ImgMap);

    _this = _super.call(this, props);
    _this.state = {
      files: null,
      tool: null
    };
    return _this;
  } // componentDidMount(() => {
  // 	console.log('%ccomponentDidMount in ImgMap','color:yellow;');
  // }
  // 


  _createClass(ImgMap, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          files = _this$state.files,
          tool = _this$state.tool;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], {
        dir: "row",
        style: {
          minHeight: 'calc(100vh - 48px)'
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-9 d-grid place-center bg-strip",
        style: {
          minHeight: 'calc(100vh - 48px)'
        }
      }, files && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_3__["default"], {
        frame: true,
        fluid: true,
        WrapAs: "div",
        alt: files.file.name,
        src: files.result
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "position-absolute position-full w-100 h-100 svg-map" // width="200" 
        // height="200" 
        ,
        xmlns: "http://www.w3.org/2000/svg"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-3 bg-light",
        style: {
          minHeight: 'calc(100vh - 48px)'
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "btn-group-vertical w-100"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"], {
        bsPrefix: "btn-group"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Toggle, {
        variant: "light",
        className: "text-left"
      }, "Insert File"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Menu, {
        className: "w-400"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Item, {
        as: "label",
        className: "qi qi-upload"
      }, "Upload", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "file",
        accept: ".jpg,.JPG,.jpeg,.JPEG,.png,.PNG,.gif,.GIF,.bmp,.BMP,.svg,.SVG,.webp,.WEBP",
        hidden: true,
        onChange: function onChange(e) {
          var file = e.target.files[0];

          if (file) {
            console.log(file);
            Object(_utils_file_fileRead__WEBPACK_IMPORTED_MODULE_5__["fileRead"])(file).then(function (r) {
              console.log(r);

              _this2.setState({
                files: r
              });
            })["catch"](function (e) {
              return console.log(e);
            });
          }
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
        className: "dropdown-header"
      }, "Insert URL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        className: "input-group input-group-sm px-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "search",
        className: "form-control"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "input-group-append"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
        As: "b",
        kind: "light",
        className: "qi qi-check"
      }))))), TOOLS.map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
          key: i,
          onClick: function onClick() {
            return _this2.setState({
              tool: v.label
            });
          },
          blur: true,
          kind: "light",
          className: "text-left",
          active: tool === v.label
        }, v.label);
      }))));
    }
  }]);

  return ImgMap;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);
/*
<svg 
	className="img-fluid" 
	// width="200" 
	// height="200" 
	xmlns="http://www.w3.org/2000/svg" 
>       
	<image className="" href={files.result} 
		// height="200" 
		// width="200" 
	/>
</svg>

<React.Fragment></React.Fragment>
*/




/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Img.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Img.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Img; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // , { useState }
// import { Cx } from '../../utils/Q.js';
// errorText, errorDesc, onError, src, 

function Img(_ref) {
  var inRef = _ref.inRef,
      _ref$alt = _ref.alt,
      alt = _ref$alt === void 0 ? "" : _ref$alt,
      src = _ref.src,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? "lazy" : _ref$loading,
      _ref$drag = _ref.drag,
      drag = _ref$drag === void 0 ? false : _ref$drag,
      WrapAs = _ref.WrapAs,
      w = _ref.w,
      h = _ref.h,
      fluid = _ref.fluid,
      thumb = _ref.thumb,
      circle = _ref.circle,
      round = _ref.round,
      className = _ref.className,
      frameClass = _ref.frameClass,
      frame = _ref.frame,
      caption = _ref.caption,
      captionClass = _ref.captionClass,
      onLoad = _ref.onLoad,
      onError = _ref.onError,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "alt", "src", "loading", "drag", "WrapAs", "w", "h", "fluid", "thumb", "circle", "round", "className", "frameClass", "frame", "caption", "captionClass", "onLoad", "onError", "children"]);

  // const [load, setLoad] = useState(false);
  // const [err, setErr] = useState(null);
  // const support = 'loading' in HTMLImageElement.prototype;
  // const isLazy = SUPPORT_LOADING && loading === 'lazy';
  var bsFigure = frame === 'bs';
  var As = bsFigure ? "figure" : WrapAs ? WrapAs : "picture";

  var setCx = function setCx(et) {
    Q.setClass(et, "ava-loader", "remove"); // if(fluid || thumb) Q.setAttr(et, "height");
  };

  var Load = function Load(e) {
    // Q.setClass(e.target, "ava-loader", "remove");// OPTION: remove or Not
    setCx(e.target);
    if (onLoad) onLoad(e);
  }; // DEV: 


  var Error = function Error(e) {
    var et = e.target; // Q.setClass(et, "img-err");
    // Q.setAttr(et, {"aria-label":`&#xF03E;`});
    // setErr("&#xF03E;"); 

    setCx(et);
    et.src = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='95' viewBox='0 0 32 32'%3E%3Cg%3E%3Cpath d='M29,4H3A1,1,0,0,0,2,5V27a1,1,0,0,0,1,1H29a1,1,0,0,0,1-1V5A1,1,0,0,0,29,4ZM28,26H4V6H28Z'/%3E%3Cpath d='M18.29,12.29a1,1,0,0,1,1.42,0L26,18.59V9a1,1,0,0,0-1-1H7A1,1,0,0,0,6,9v1.58l7,7Z'/%3E%3Cpath d='M7,24H25a1,1,0,0,0,1-1V21.41l-7-7-5.29,5.3a1,1,0,0,1-1.42,0L6,13.42V23A1,1,0,0,0,7,24Z'/%3E%3C/g%3E%3C/svg%3E";
    if (onError) onError(e);
    return; // null
  };

  var img = function img() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", _extends({}, etc, {
      ref: inRef,
      loading: loading,
      alt: alt // OPTION
      ,
      width: w // isLazy && !w && !load ? 40 : w
      ,
      height: h // isLazy && !h && !load ? 40 : h
      ,
      src: Q.newURL(src).href // OPTION: Q.isDomain(src) ? src : Q.newURL(src).href
      ,
      className: Q.Cx("ava-loader", {
        // holder thumb| ava-loader
        'img-fluid': fluid,
        // fluid && !isLazy
        'img-thumbnail': thumb,
        'rounded-circle': circle,
        'rounded': round,
        'figure-img': bsFigure // 'fal img-err': err

      }, className),
      draggable: drag // !!drag 
      // aria-label={label || err} 
      ,
      onError: Error,
      onLoad: Load // OPTION: 
      // onContextMenu={e => {
      // Q.preventQ(e);
      // }}

      /* onLoad={e => {
      	if(isLazy){
      		setLoad(true);
      		let et = e.target;
      		// setAttr(et, 'width height');
      		setClass(et, 'isLoad');//  + (fluid ? ' img-fluid' : '')
      	}
      		onLoad(e);
      }} */

    }));
  };

  if (frame) {
    // const bsFigure = frame === 'bs';
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As // figure | div 
    // ref={frameRef} 
    , {
      className: // thumb
      Q.Cx("img-frame", {
        'figure': bsFigure
      }, frameClass) // draggable="false" // OPTION

    }, img(), bsFigure && caption && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
      className: Q.Cx("figure-caption", captionClass)
    }, caption), children);
  }

  return img();
} // Img.defaultProps = {
// loading: 'lazy',
// alt: '',
// onLoad: noop
// noDrag: true
// };

/* const error = e => {
	if(onError){
		onError(e);
	}else{
		// data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${this.props.alt}%3C/text%3E%3C/svg%3E
		e.target.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='45%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${errorText ? errorText : alt}%3C/text%3E%3Ctext x='50%25' y='55%25' fill='%23fff' dy='.5em' style='font-size:0.7rem;font-family:sans-serif'%3E${errorDesc ? errorDesc : 'Image not found'}%3C/text%3E%3C/svg%3E`;// "/img/imgError.svg";
		return null;// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
	}
   } */

/* const strNum = P.oneOfType([P.string, P.number]);

Img.propTypes = {
	frame: P.bool,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  alt: P.string,

  w: strNum,
  h: strNum,
	// src: P.string, // .isRequired

  fluid: P.bool,
  thumb: P.bool,
  circle: P.bool,
  round: P.bool,
  noDrag: P.bool,
	onError: P.func,
	onLoad: P.func
}; */

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/ImgMapPage.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/ImgMapPage.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ImgMapPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_img_map_ImgMap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/img-map/ImgMap */ "./resources/js/src/components/img-map/ImgMap.js");
 // , { useState, useEffect, useRef, } 


 // DEVS Components:
// import SpinLoader from '../../../components/q-ui-react/SpinLoader';
// END DEVS Components:

function ImgMapPage() {
  // const [pageLoad, setPageLoad] = useState(false);
  // useEffect(() => {
  // console.log('%cuseEffect in ImgMapPage','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid mh-full-navmain"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Img Map"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_img_map_ImgMap__WEBPACK_IMPORTED_MODULE_2__["default"], null));
}
/*
<h1>Img Map</h1>

<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=ImgMapPage.chunk.js.map