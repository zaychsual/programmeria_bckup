(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["TableDocs"],{

/***/ "./resources/js/src/components/q-ui-react/Table.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Table.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Table; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // { useState, useEffect }
// import ResizeSensor from './ResizeSensor';

function Table(_ref) {
  var _ref$responsive = _ref.responsive,
      responsive = _ref$responsive === void 0 ? true : _ref$responsive,
      responsiveClass = _ref.responsiveClass,
      responsiveSize = _ref.responsiveSize,
      responsiveStyle = _ref.responsiveStyle,
      border = _ref.border,
      hover = _ref.hover,
      strip = _ref.strip,
      sm = _ref.sm,
      kind = _ref.kind,
      layout = _ref.layout,
      thead = _ref.thead,
      tbody = _ref.tbody,
      tfoot = _ref.tfoot,
      caption = _ref.caption,
      className = _ref.className,
      theadClass = _ref.theadClass,
      tbodyClass = _ref.tbodyClass,
      tfootClass = _ref.tfootClass,
      captionClass = _ref.captionClass,
      fixThead = _ref.fixThead,
      _ref$customScroll = _ref.customScroll,
      customScroll = _ref$customScroll === void 0 ? true : _ref$customScroll;

  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in Table','color:yellow;');
  // }, []);
  var Tb = function Tb() {
    var _Q$Cx;

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("table", {
      className: Q.Cx("table", (_Q$Cx = {}, _defineProperty(_Q$Cx, "table-border" + (border === true ? "ed" : "less"), border), _defineProperty(_Q$Cx, "table-striped", strip), _defineProperty(_Q$Cx, "table-hover", hover), _defineProperty(_Q$Cx, "table-sm", sm), _defineProperty(_Q$Cx, "table-" + kind, kind), _defineProperty(_Q$Cx, "tb-" + layout, layout), _Q$Cx), className)
    }, caption && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("caption", {
      className: captionClass
    }, caption), thead && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", {
      className: theadClass
    }, thead), tbody && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", {
      className: tbodyClass
    }, tbody), tfoot && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tfoot", {
      className: tfootClass
    }, tfoot));
  }; // const Tbl = () => 
  // 	fixThead ? 
  // 		<ResizeSensor 
  // 			// className="" 
  // 			// wait={1000} 
  // 			onResizeEnd={(s) => {
  // 				console.log('onResizeEnd size: ', s);
  // 			}}
  // 		>
  // 			{Tb()}
  // 		</ResizeSensor>
  // 	: Tb();


  if (responsive || fixThead) {
    var _Q$Cx2;

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: Q.Cx((_Q$Cx2 = {}, _defineProperty(_Q$Cx2, "table-responsive" + (responsiveSize ? "-" + responsiveSize : ""), responsive), _defineProperty(_Q$Cx2, "ovauto thead-fix", fixThead), _defineProperty(_Q$Cx2, "q-scroll ovscroll-auto", customScroll), _Q$Cx2), responsiveClass),
      style: responsiveStyle // DEV OPTION: For fixed thead

    }, Tb());
  }

  return Tb();
}

/***/ }),

/***/ "./resources/js/src/pages/admin/docs/q-ui-react/TableDocs.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/pages/admin/docs/q-ui-react/TableDocs.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TableDocs; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DocsPage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../DocsPage */ "./resources/js/src/pages/admin/docs/DocsPage.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../components/q-ui-react/Table */ "./resources/js/src/components/q-ui-react/Table.js");
 // , { useState, useEffect, Fragment }




function TableDocs() {
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in TableDocs','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocsPage__WEBPACK_IMPORTED_MODULE_1__["default"] // title="Table" 
  , {
    api: "/storage/json/docs/components/q-ui-react/Table.json" // Table | Btn
    ,
    scope: {
      useState: react__WEBPACK_IMPORTED_MODULE_0__["useState"],
      Table: _components_q_ui_react_Table__WEBPACK_IMPORTED_MODULE_3__["default"],
      Flex: _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"]
    }
  });
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=TableDocs.chunk.js.map