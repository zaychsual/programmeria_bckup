(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ArouteDocs~AvaDocs~BtnDocs~LoopsPage~ModalPage~TableDocs"],{

/***/ "./resources/js/src/components/live-code/LiveCode.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/live-code/LiveCode.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LiveCode; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_split_pane__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../react-split-pane */ "./resources/js/src/components/react-split-pane/index.js");
/* harmony import */ var _q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _q_ui_react_Switch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../q-ui-react/Switch */ "./resources/js/src/components/q-ui-react/Switch.js");
/* harmony import */ var _browser_Browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../browser/Browser */ "./resources/js/src/components/browser/Browser.js");
/* harmony import */ var _Compiler__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Compiler */ "./resources/js/src/components/live-code/Compiler.js");
/* harmony import */ var _monaco_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../monaco-react */ "./resources/js/src/components/monaco-react/index.js");
/* harmony import */ var _monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../monaco-react/ide/onDidMount */ "./resources/js/src/components/monaco-react/ide/onDidMount.js");
/* harmony import */ var _utils_clipboard_clipboardApi__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../utils/clipboard/clipboardApi */ "./resources/js/src/utils/clipboard/clipboardApi.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import loadable from '@loadable/component';
// import { useInView } from '../react-intersection-observer';// InView




 // import UctrlAlert from '../../components/q-react-bootstrap/UctrlAlert';// OPTION



 // {ControlledEditor} | Editor | IdeCtrl

 // import { ResizeSensor } from '../blueprintjs/components/resize-sensor/resizeSensor';
// import ResizeSensor from '../q-ui-react/ResizeSensor';

 // import copy from '../../utils/clipboard/copy-to-clipboard';
// import {xhrScript} from '../../utils/xhrScript';

/* const Bs = loadable.lib(() => import('@babel/standalone'));
// import BabelStandalone from '@babel/standalone';
console.log('Bs: ', Bs);

function BabelStandalone({ date }) {
	// ({ default: b })
  return (
    <div>
			<Bs fallback={<div>Loading Babel...</div>}>
				{m => {
					console.log('render BabelStandalone: ', b);
				}}
			</Bs>
    </div>
  )
} */
// import { LazyLoadComponent } from '../../components/react-lazy-load-image-component';// trackWindowScroll

/** NOT FIX ISSUE: can't render component twice if Babel load with getScript */

var LiveCode = /*#__PURE__*/function (_React$Component) {
  _inherits(LiveCode, _React$Component);

  var _super = _createSuper(LiveCode);

  function LiveCode(props) {
    var _this;

    _classCallCheck(this, LiveCode);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onGetBabel", function () {
      // async 
      // const { code, onRender } = this.props;
      if (window.Babel) {
        // console.log('%cBabel available: ', 'color:orange', window.Babel);
        _this.setBabel();
      } else {
        var windowDefine = window.define;

        if (window.monaco && windowDefine) {
          window.define = null; // delete window.define;
          // window.AMDLoader = null;
        } // await 


        Q.getScript({
          src: "/storage/app_modules/@babel-standalone/babel.min.js",
          async: false,
          "data-js": "Babel"
        }).then(function () {
          if (window.Babel) {
            // console.log('%cif Babel: ', 'color:orange', window.Babel);
            _this.setBabel();

            window.define = windowDefine;
          } else {
            console.log('%celse Babel: ', 'color:orange', window.Babel); // @babel/standalone | @babel/core

            importShim("https://jspm.dev/@babel/standalone").then(function (m) {
              console.log('importShim: ', m);
              window.Babel = m["default"]; // store Babel to window

              window.define = windowDefine;

              _this.setBabel();
            })["catch"](function (e) {
              return console.log('importShim catch: ', e);
            });
          }
        })["catch"](function (e) {
          return console.warn(e);
        }); // NOT RUN...!!!
      }
    });

    _defineProperty(_assertThisInitialized(_this), "setBabel", function () {
      var _this$props = _this.props,
          code = _this$props.code,
          onRender = _this$props.onRender;

      _this.setState({
        codeAuto: code,
        codeNoAuto: code
      }, function () {
        return onRender();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onDidChangeEditor", function (v, editor) {
      if (!_this.state.editorReady) {
        _this.setState({
          editorReady: true
        });

        _this.ideRef.current = editor;
        Object(_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_8__["default"])(v, editor); // , {required:true, onMount: noop}
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onCopy", function (to, e) {
      var et = e.target;
      Object(_utils_clipboard_clipboardApi__WEBPACK_IMPORTED_MODULE_9__["Copy"])(to, {
        onOk: function onOk(t) {
          if (et) {
            var pTitle = Q.getAttr(et, 'aria-label'); // et.title;

            Q.setAttr(et, {
              'aria-label': 'Copied'
            }); // et.title = 'Copied';

            setTimeout(function () {
              return Q.setAttr(et, {
                'aria-label': pTitle
              });
            }, 1000); // et.title = pTitle
          }
        },
        onErr: function onErr(e) {
          return console.warn('Error copy: ', e);
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "renderLiveCode", function () {
      var _this$state = _this.state,
          codeAuto = _this$state.codeAuto,
          codeNoAuto = _this$state.codeNoAuto,
          autoRun = _this$state.autoRun;

      if (codeAuto) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Compiler__WEBPACK_IMPORTED_MODULE_6__["default"], {
          babel: window.Babel,
          code: autoRun ? codeAuto : codeNoAuto,
          scope: _this.props.scope,
          setError: function setError(e) {
            // if(e){
            // this.setState({ err: e.replace('/file.tsx: ', '') });
            // }else{
            // this.setState({ err: null });
            // }
            _this.setState({
              err: e ? e.replace('/file.tsx: ', '') : null
            });
          } // preview={err !== null ? ErrBabel : null} // ???

        });
      }
    });

    _this.state = {
      // themes: props.theme,
      pos: "vertical",
      ideOpen: props.openIde,
      // false |  props.openIde
      autoRun: props.auto,
      codeAuto: undefined,
      // undefined | props.code
      codeNoAuto: undefined,
      // undefined | props.code
      err: null,
      editorReady: false,
      maxSize1: document.body.clientWidth - 320 // 800
      // maxSize2: 290

    };
    _this.ideRef = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createRef"])();
    _this.liveRef = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createRef"])();
    return _this;
  }

  _createClass(LiveCode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // async 
      // console.log('%ccomponentDidMount in LiveCode:', 'color:yellow');
      this.onGetBabel();
      /* const {code, onRender} = this.props;
      if(window.Babel){
      	this.setState({
      		codeAuto: code,
      		codeNoAuto: code
      	}, () => onRender());
      }else{
      	Qrequirejs(['/storage/app_modules/@babel-standalone/babel.min.js'],(Babel) => {
      		console.log('Babel set', Babel);
      		this.setState({
      			codeAuto: code,
      			codeNoAuto: code
      		}, () => onRender());
      	});
      } */

      /* if(window.Babel){//  && renderCount < 1
      	this.setState({
      		codeAuto: code,
      		codeNoAuto: code
      	}, () => onRender());
      }else{
      	xhrScript("/storage/app_modules/@babel-standalone/babel.min.js", "Babel", 
      		(fn, v) => {
      			// console.log('xhrScript: ', v);
      			// console.log('xhrScript: ', fn);
      			this.setState({
      				codeAuto: code,
      				codeNoAuto: code
      			}, () => onRender());
      		},
      		(e) => console.warn(e)
      	);
      } */
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      // auto, code, inRef
      var _this$props2 = this.props,
          scope = _this$props2.scope,
          h = _this$props2.h,
          minH = _this$props2.minH,
          maxH = _this$props2.maxH,
          className = _this$props2.className,
          viewClass = _this$props2.viewClass,
          liveBoxClass = _this$props2.liveBoxClass,
          browser = _this$props2.browser,
          _onChange = _this$props2.onChange; // themes, maxSize2

      var _this$state2 = this.state,
          editorReady = _this$state2.editorReady,
          ideOpen = _this$state2.ideOpen,
          pos = _this$state2.pos,
          autoRun = _this$state2.autoRun,
          codeAuto = _this$state2.codeAuto,
          codeNoAuto = _this$state2.codeNoAuto,
          err = _this$state2.err,
          maxSize1 = _this$state2.maxSize1; // <LazyLoadComponent>

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"] // inRef={inRef} 
      , {
        dir: "column",
        className: Q.Cx("liveCode", className)
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], {
        className: "p-1 bg-light border border-bottom-0 ml-1-next"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        outline: true,
        onClick: function onClick() {
          return _this2.setState({
            ideOpen: !ideOpen
          });
        },
        size: "sm",
        className: "tip tipBL qi qi-eye" + (ideOpen ? "-slash" : ""),
        "aria-label": "Show/Hide Code"
      }), editorReady && ideOpen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        outline: true,
        onClick: function onClick(e) {
          return _this2.onCopy(codeAuto, e);
        },
        size: "sm",
        className: "tip tipBL qi qi-copy",
        "aria-label": "Copy Code"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Switch__WEBPACK_IMPORTED_MODULE_4__["default"] // checked={!autoRun} // disabled={!ideOpen} 
      , {
        className: "btn btn-flat btn-sm border-secondary pl-5 pr-3 tip tipBL",
        parentProps: {
          "aria-label": "Auto Run"
        },
        label: "Auto" // Auto Run
        ,
        defaultChecked: autoRun,
        onChange: function onChange(e) {
          return _this2.setState({
            autoRun: e.target.checked
          });
        }
      }), !autoRun && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        onClick: function onClick() {
          return _this2.setState({
            codeNoAuto: codeAuto
          });
        },
        size: "sm",
        className: "tip tipBL qi qi-play",
        "aria-label": "Run"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        ref: this.liveRef,
        tabIndex: "-1",
        style: {
          height: h,
          minHeight: minH,
          maxHeight: maxH
        },
        className: Q.Cx("position-relative liveBox", liveBoxClass),
        title: "Resize"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_react_split_pane__WEBPACK_IMPORTED_MODULE_1__["default"], {
        split: pos // "horizontal" // vertical
        ,
        minSize: 50,
        maxSize: maxSize1 // 1000 | 300
        // defaultSize={100}
        // defaultSize="100%" // 50%
        ,
        size: ideOpen ? "50%" : "100%",
        allowResize: ideOpen,
        className: "border position-absolute pb-4" // className={Q.Cx("border position-absolute", themes)} 
        ,
        pane1Class: "compilerCode",
        pane2Class: "ovhide bg-dark" // pane2Class={`ovhide bg-${themes}`} 
        ,
        parentProps: {
          title: ""
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: Q.Cx("bg-white h-100 ovyscroll q-scroll ovscroll-auto liveView", viewClass)
      }, browser ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_browser_Browser__WEBPACK_IMPORTED_MODULE_5__["default"], {
        type: null
      }, this.renderLiveCode()) : this.renderLiveCode()), ideOpen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_react_split_pane__WEBPACK_IMPORTED_MODULE_1__["default"], {
        split: "horizontal",
        allowResize: err !== null,
        size: err !== null ? "50%" : "100%" // maxSize={maxSize2} // 290
        ,
        pane1Class: err !== null ? "mxh-80" : undefined // resizerClass={err === null ? 'd-none':''} 
        ,
        pane2Class: "bg-white ovhide"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "w-100 live-app"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_monaco_react__WEBPACK_IMPORTED_MODULE_7__["ControlledEditor"] // height="calc(100% - 31px)" // calc(100vh - 75px) 
      // className={Cx({'d-none': tools !== 'Compiler'})} 
      , {
        editorDidMount: this.onDidChangeEditor,
        value: codeAuto,
        onChange: function onChange(e, codeAuto) {
          _this2.setState({
            codeAuto: codeAuto
          });

          _onChange(codeAuto); // OPTION

        } // language={compilerLang} // ideLang | 'javascript'
        // theme={themes} 
        ,
        options: {
          fontSize: 14,
          // fontSize
          tabSize: 2,
          // tabSize
          minimap: {
            enabled: false
          }
        }
      })), err && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("pre", {
        className: "alert alert-danger mb-0 border-0 rounded-0 h-100 q-scroll ovscroll-auto"
      }, err))))); // </LazyLoadComponent>
    }
  }]);

  return LiveCode;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);


LiveCode.defaultProps = {
  // theme: 'dark', // light | dark
  scope: {},
  h: 150,
  // 300
  minH: 150,
  // 300
  // maxH: 300,
  // code: '',
  openIde: false,
  auto: false,
  onRender: Q.noop,
  onChange: Q.noop // OPTION

}; // export default trackWindowScroll(LiveCode);
// export default React.memo(LiveCode, (prevProps, nextProps) => prevProps.code === nextProps.code);

/*
</React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Form.js":
/*!********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Form.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Form; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import {findDOMNode} from "react-dom";
// import { Cx, toggleClass } from '../../utils/Q';
// wrapClass, upload, id, encType

function Form(_ref) {
  var inRef = _ref.inRef,
      className = _ref.className,
      fieldsetClass = _ref.fieldsetClass,
      noValidate = _ref.noValidate,
      valid = _ref.valid,
      disabled = _ref.disabled,
      onSubmit = _ref.onSubmit,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "className", "fieldsetClass", "noValidate", "valid", "disabled", "onSubmit", "children"]);

  // React.useEffect(() => {
  // console.log('%cuseEffect in Form','color:yellow;');
  // console.log(getEl());
  // console.log(findDOMNode(Form));
  // }, []);

  /*const getEl = () => { // getElement
  	try{
  		// using findDOMNode for two reasons:
  		// 1. cloning to insert a ref is unwieldy and not performant.
  		// 2. ensure that we resolve to an actual DOM node (instead of any JSX ref instance).
  		return findDOMNode(this);
  	}catch{
  		return null;// swallow error if findDOMNode is run on unmounted component.
  	}
  }*/
  var Submit = function Submit(e) {
    if (disabled) return;
    var et = e.target; // Prevent submit form if use xhr

    if (noValidate) {
      e.preventDefault();
      e.stopPropagation(); // console.log(!Q.isBool(valid));
      // if(!Q.isBool(valid)) 

      if (!Q.isBool(valid)) {
        et.classList.toggle("was-validated", !et.checkValidity());
      }
    }

    if (onSubmit && !disabled) onSubmit(e, et.checkValidity()); // onSubmit to props
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", _extends({}, etc, {
    ref: inRef,
    className: Q.Cx('form-q', {
      "disabled": disabled,
      // Q.isBool(valid) //  && !isValid
      "was-validated": noValidate && valid && Object.keys(valid).length > 0
    }, className) // enctype values: (OPTION)
    // 1. application/x-www-form-urlencoded (Default)
    // 2. multipart/form-data (form upload)
    // 3. text/plain (Spaces are converted to "+" symbols, but no special characters are encoded)
    // encType={upload ? 'multipart/form-data' : encType} 
    //data-reset={reset} 
    ,
    noValidate: noValidate // noValidate ? true : undefined
    ,
    onSubmit: Submit
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
    disabled: disabled,
    className: fieldsetClass
  }, children));
}

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Switch.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Switch.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Switch; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import {Cx} from '../utils/Q';

function Switch(_ref) {
  var _ref$As = _ref.As,
      As = _ref$As === void 0 ? 'label' : _ref$As,
      className = _ref.className,
      id = _ref.id,
      label = _ref.label,
      parentProps = _ref.parentProps,
      etc = _objectWithoutProperties(_ref, ["As", "className", "id", "label", "parentProps"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, _extends({}, parentProps, {
    className: Q.Cx("custom-control custom-switch", className)
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({}, etc, {
    type: "checkbox",
    className: "custom-control-input",
    id: id
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As === 'label' && !id ? 'div' : 'label', {
    className: "custom-control-label",
    htmlFor: As !== 'label' && id ? id : undefined
  }, label));
} // <label className="custom-control-label" htmlFor={id}>{label}</label>
// Switch.defaultProps = {
// As: 'label'
// };

/***/ }),

/***/ "./resources/js/src/pages/admin/docs/DocsPage.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/pages/admin/docs/DocsPage.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocsPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_live_code_LiveCode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/live-code/LiveCode */ "./resources/js/src/components/live-code/LiveCode.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/q-ui-react/Form */ "./resources/js/src/components/q-ui-react/Form.js");
/* harmony import */ var _components_monaco_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components/monaco-react */ "./resources/js/src/components/monaco-react/index.js");
/* harmony import */ var _components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/monaco-react/ide/onDidMount */ "./resources/js/src/components/monaco-react/ide/onDidMount.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // 

 // import RouteParams from '../../../parts/RouteParams';

 // import MainContent from '../../../parts/MainContent';
// import { InView } from '../../../components-dev/react-intersection-observer';// useInView | InView

 // LiveCode | Ngoding

 // import Modal from '../../../components/q-react-bootstrap/Modal';
// import ModalHeader from '../../../components/q-react-bootstrap/ModalHeader';

 // Loops

 // {ControlledEditor} | Editor

 // import { LazyLoadComponent, trackWindowScroll } from '../../../components/react-lazy-load-image-component';
// import Ava from '../../../components/q-ui-react/Ava';

/** NOT FIX: hack load babel */

var DocsPage = /*#__PURE__*/function (_Component) {
  _inherits(DocsPage, _Component);

  var _super = _createSuper(DocsPage);

  function DocsPage(props) {
    var _this;

    _classCallCheck(this, DocsPage);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onAdd", function (e) {
      var et = e.target;

      if (et.checkValidity()) {
        var addVal = _this.state.addVal; // docFirst, docs
        // console.log('OK', this.addIdeRef.current());

        var newDoc = {
          openIde: true,
          label: _this.addTitleRef.current.value,
          code: addVal // this.addIdeRef.current()

        };

        _this.setState(function (s) {
          return {
            newDocs: [newDoc].concat(_toConsumableArray(s.newDocs)),
            modal: false // addVal:""

          };
        }); // this.setState({modal: false})

        /* if(docFirst){
        	this.setState(s => ({
        		docs: [docFirst, ...s.docs],
        		docFirst: newDoc,
        		modal: false
        	}));
        }
        else{
        	this.setState(s => ({
        		docs: [newDoc, ...s.docs],
        		modal: false
        	}));
        } */

      }
    });

    _defineProperty(_assertThisInitialized(_this), "onModal", function () {
      return _this.setState(function (s) {
        return {
          modal: !s.modal
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "ideDidMount", function (v, editor) {
      Object(_components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_7__["default"])(v, editor, true); // {required:true, onMount: noop}

      /* if(!this.state.editorReady){
        this.setState({editorReady: true});
        this.addIdeRef.current = editor;// editor | v
        
        if(editor){ // this.ideMain.current
      // OPTION DEV: set attribute required in monaco textarea
      const ta = domQ("textarea", editor.getDomNode());
      if(ta) setAttr(ta, {required:''});
      
      // Get bind editor contextmenu FROM https://github.com/Microsoft/monaco-editor/issues/484
      editor.onContextMenu(e => {
      	let ctxMenu = domQ(".monaco-menu-container", editor.getDomNode());// this.ideMain.current
      	if(ctxMenu){
      		// window.outerHeight
              let ee = e.event,
                  ch = ctxMenu.clientHeight,
                  cw = ctxMenu.clientWidth,
                  cs = ctxMenu.style;
      		const posY = (ee.posy + ch) > window.innerHeight ? ee.posy - ch : ee.posy;
      		// window.outerWidth
      		const posX = (ee.posx + cw) > document.body.clientWidth ? ee.posx - cw : ee.posx;
      
      		cs.position = "fixed";// OPTIONS: set in css internal
      		cs.top = Math.max(0, Math.floor(posY)) + "px";
      		cs.left = Math.max(0, Math.floor(posX)) + "px";
      	}
      });
        }
      } */
    });

    _defineProperty(_assertThisInitialized(_this), "onSaveAll", function () {
      var _this$state = _this.state,
          docFirst = _this$state.docFirst,
          docs = _this$state.docs,
          newDocs = _this$state.newDocs;
      var newDocsOk = newDocs.map(function (v) {
        return {
          label: v.label,
          code: v.code
        };
      });
      var mergeDoc = docFirst ? [].concat(_toConsumableArray(newDocsOk), [docFirst], _toConsumableArray(docs)) : [].concat(_toConsumableArray(newDocsOk), _toConsumableArray(docs)); // console.log('newDocsOk: ', newDocsOk);

      console.log(JSON.stringify(mergeDoc, null, 2));
    });

    _this.state = {
      docFirst: null,
      docsLoad: false,
      docs: null,
      // [] | null
      modal: false,
      // editorReady: false,
      addVal: '',
      newDocs: []
    };
    _this.addTitleRef = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef(); // this.addIdeRef = React.createRef();

    return _this;
  }

  _createClass(DocsPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var api = this.props.api; // console.log('%ccomponentDidMount in DocsPage','color:yellow;', this.props.location);
      // Get data docs: '/storage/json/docs/components/q-ui-react/Btn.json'

      if (api) {
        axios__WEBPACK_IMPORTED_MODULE_1___default()(api).then(function (v) {
          var docs = v.data; // console.log('docs: ',docs);

          if (v.status === 200 && Q.isObj(docs)) {
            //  && Array.isArray(docs)
            if (window.Babel) {
              // console.log('window.Babel: ',window.Babel);
              _this2.setState({
                docs: docs,
                docsLoad: true
              });
            } else {
              // console.log('No Babel');
              _this2.setState({
                docFirst: docs.demos[0]
              }, function () {
                _this2.setState({
                  docs: _objectSpread(_objectSpread({}, docs), {}, {
                    demos: docs.demos.filter(function (v, i) {
                      return i !== 0;
                    })
                  })
                });
              });
            }
          }
        })["catch"](function (e) {
          return console.log(e);
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          scope = _this$props.scope,
          browser = _this$props.browser; // title, 

      var _this$state2 = this.state,
          docFirst = _this$state2.docFirst,
          docsLoad = _this$state2.docsLoad,
          docs = _this$state2.docs,
          newDocs = _this$state2.newDocs,
          modal = _this$state2.modal,
          addVal = _this$state2.addVal;
      var isNewDocs = newDocs.length > 0; // <RouteParams path="/components">

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container py-3 mb-5 mt-3-next"
      }, docsLoad && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_2__["default"], {
        title: docs.title
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        className: "h4"
      }, docs.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "lead"
      }, docs.description)), isNewDocs && newDocs.map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          key: i + v.label + '-newDocs'
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", null, v.label, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
          onClick: function onClick() {
            return _this3.setState({
              newDocs: newDocs.filter(function (v2, i2) {
                return i !== i2;
              })
            });
          },
          size: "xs",
          kind: "danger",
          className: "float-right q q-close",
          tip: "Remove / cancel"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_live_code_LiveCode__WEBPACK_IMPORTED_MODULE_3__["default"], {
          browser: browser,
          openIde: v.openIde,
          className: "shadow-sm",
          liveBoxClass: "ovauto resize-native resize-v qi qi-arrows-v",
          viewClass: "w-100",
          h: v.height ? v.height : null // '70vh'
          ,
          scope: _objectSpread(_objectSpread({}, scope), {}, {
            Btn: _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"],
            Form: _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_5__["default"]
          }),
          code: v.code
        }));
      }), docFirst && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", null, docFirst.label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_live_code_LiveCode__WEBPACK_IMPORTED_MODULE_3__["default"], {
        browser: browser,
        className: "shadow-sm",
        liveBoxClass: "ovauto resize-native resize-v qi qi-arrows-v",
        viewClass: "w-100",
        h: docFirst.height ? docFirst.height : null // '70vh'
        ,
        scope: _objectSpread(_objectSpread({}, scope), {}, {
          Btn: _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"],
          Form: _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_5__["default"]
        }) // 
        ,
        code: docFirst.code,
        onRender: function onRender() {
          return _this3.setState({
            docsLoad: true
          });
        }
      })), docsLoad && docs && docs.demos.map(function (v, i) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          key: i + v.label + '-docs'
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", null, v.label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_live_code_LiveCode__WEBPACK_IMPORTED_MODULE_3__["default"] // LiveCode | Ngoding
        // theme="dark" 
        , {
          browser: browser,
          className: "shadow-sm",
          liveBoxClass: "ovauto resize-native resize-v qi qi-arrows-v",
          viewClass: "w-100",
          h: v.height ? v.height : null // '70vh'
          ,
          scope: _objectSpread(_objectSpread({}, scope), {}, {
            Btn: _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"],
            Form: _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_5__["default"]
          }),
          code: v.code // className="mb-3" 
          // viewClass="p-3" 

        }));
      }));
    }
  }]);

  return DocsPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);
/* <Modal unmountOnClose={false} isOpen={modal} size="xl" toggle={this.onModal}
				onClosed={() => {
					if(addVal.length > 0) this.setState({addVal: ''})
				}}
			>
				<ModalHeader As="h6" toggle={this.onModal} modalTitle="Add item" />
				<div className="modal-body p-0">
					<Form noValidate onSubmit={this.onAdd}>
						<div className="p-2">
							<input ref={this.addTitleRef} className="form-control" type="text" placeholder="Label / title" required />
						</div>
						
						<ControlledEditor 
							height="70vh" 
							// language="javascript" 
							// className="mx-min15" 
							value={addVal} 
							onChange={(e, addVal) => this.setState({addVal})} 
							editorDidMount={this.ideDidMount} 
							options={{
								fontSize: 14,
								tabSize: 2, // tabSize
								// dragAndDrop: false,
								minimap: {
									enabled: false, // Defaults = true
									// side: minimapSide, // Default = right | left
									// showSlider: 'always', // Default = mouseover
									// renderCharacters: false, // Default =  true
								},
								// colorDecorators: false, // Css color preview
								// readOnly: true,
								// mouseWheelZoom: zoomIde
							}}
						/>
						
						<button type="submit" id="btnSubmitDoc" hidden />
					</Form>
				</div>
				
				<div className="modal-footer">
					<Btn>Cancel</Btn>
					<Btn As="label" kind="primary" htmlFor="btnSubmitDoc">Save</Btn>
				</div>
			</Modal> */

/*
								<React.Fragment>
									<h5>{v.label}</h5>
									<LiveCode 
										// theme="dark" 
										scope={{Btn}} 
										code={v.code} 
										viewClass="p-3" 
										onRender={() => {
											console.log('onRender 1');
											// this.setState({code1: });
										}}
									/>
								</React.Fragment>
								
								<InView 
									triggerOnce 
									threshold={1} 
								>
									{({ inView, ref, entry }) => 
										<div ref={ref}>
											{inView && 
												<React.Fragment>
													<h5>{v.label} - {inView + ''}</h5>
													<LiveCode // inRef={ref} 
														// theme="dark" 
														scope={{Btn}} 
														code={v.code} 
														viewClass="p-3" 
														onRender={() => {
															console.log('onRender: ', i);
															// this.setState({code2: })
														}}
													/>
													<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
												</React.Fragment>
											}
										</div>
									}
								</InView>
								
								<InView // as="div" 
									triggerOnce 
									threshold={1} 
									onChange={(inView, entry) => console.log('Inview: ', inView)}
								>
									<React.Fragment>
										<h5>{v.label}</h5>
										<LiveCode // inRef={ref} 
											// theme="dark" 
											scope={{Btn}} 
											code={v.code} 
											viewClass="p-3" 
											onRender={() => {
												console.log('onRender: ', i);
												// this.setState({code2: })
											}}
										/>
										<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
									</React.Fragment>
								</InView>
								


<React.Fragment></React.Fragment>
*/




/***/ }),

/***/ "./resources/js/src/utils/clipboard/clipboardApi.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/utils/clipboard/clipboardApi.js ***!
  \**********************************************************/
/*! exports provided: Copy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Copy", function() { return Copy; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import Import from '../../component-etc/route/Import';
// == Copy ==
function Copy(_x) {
  return _Copy.apply(this, arguments);
} // For npm


function _Copy() {
  _Copy = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(target) {
    var _ref,
        _ref$onOk,
        onOk,
        _ref$onErr,
        onErr,
        txt,
        _args = arguments;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _ref = _args.length > 1 && _args[1] !== undefined ? _args[1] : {}, _ref$onOk = _ref.onOk, onOk = _ref$onOk === void 0 ? function () {} : _ref$onOk, _ref$onErr = _ref.onErr, onErr = _ref$onErr === void 0 ? function () {} : _ref$onErr;

            if (!navigator.clipboard) {
              _context.next = 9;
              break;
            }

            if (target) {
              _context.next = 5;
              break;
            }

            console.warn('Text to copy or target DOM is required.');
            return _context.abrupt("return");

          case 5:
            if (typeof target === 'string' && !target.tagName) txt = target;else txt = target.textContent; // target.innerText.trim()
            // console.log(txt.replace(/\n/gm, ''));

            navigator.clipboard.writeText(txt).then(function () {
              return onOk(txt);
            })["catch"](function (err) {
              return onErr(err);
            });
            _context.next = 10;
            break;

          case 9:
            // const m = await Import('/storage/esm/copySelection.js');
            // console.log(m);
            console.log('%cnavigator.clipboard NOT SUPPORT', 'color:yellow');

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _Copy.apply(this, arguments);
}



/***/ })

}]);
//# sourceMappingURL=ArouteDocs~AvaDocs~BtnDocs~LoopsPage~ModalPage~TableDocs.chunk.js.map