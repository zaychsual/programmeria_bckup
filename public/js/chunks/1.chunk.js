(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/@react-dnd/invariant/dist/invariant.esm.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@react-dnd/invariant/dist/invariant.esm.js ***!
  \*****************************************************************/
/*! exports provided: invariant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "invariant", function() { return invariant; });
/**
 * Use invariant() to assert state which your program assumes to be true.
 *
 * Provide sprintf-style format (only %s is supported) and arguments
 * to provide information about what broke and what you were
 * expecting.
 *
 * The invariant message will be stripped in production, but the invariant
 * will remain to ensure logic does not differ in production.
 */
function invariant(condition, format) {
  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  if (true) {
    if (format === undefined) {
      throw new Error('invariant requires an error message argument');
    }
  }

  if (!condition) {
    var error;

    if (format === undefined) {
      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
    } else {
      var argIndex = 0;
      error = new Error(format.replace(/%s/g, function () {
        return args[argIndex++];
      }));
      error.name = 'Invariant Violation';
    }

    error.framesToPop = 1; // we don't care about invariant's own frame

    throw error;
  }
}


//# sourceMappingURL=invariant.esm.js.map


/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/OptionsReader.js":
/*!************************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/OptionsReader.js ***!
  \************************************************************************/
/*! exports provided: OptionsReader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OptionsReader", function() { return OptionsReader; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var OptionsReader = /*#__PURE__*/function () {
  function OptionsReader(incoming, context) {
    var _this = this;

    _classCallCheck(this, OptionsReader);

    this.enableTouchEvents = true;
    this.enableMouseEvents = false;
    this.enableKeyboardEvents = false;
    this.ignoreContextMenu = false;
    this.enableHoverOutsideTarget = false;
    this.touchSlop = 0;
    this.scrollAngleRanges = undefined;
    this.context = context;
    this.delayTouchStart = incoming.delayTouchStart || incoming.delay || 0;
    this.delayMouseStart = incoming.delayMouseStart || incoming.delay || 0;
    Object.keys(incoming).forEach(function (key) {
      if (incoming[key] != null) {
        ;
        _this[key] = incoming[key];
      }
    });
  }

  _createClass(OptionsReader, [{
    key: "window",
    get: function get() {
      if (this.context && this.context.window) {
        return this.context.window;
      } else if (typeof window !== 'undefined') {
        return window;
      }

      return undefined;
    }
  }, {
    key: "document",
    get: function get() {
      if (this.window) {
        return this.window.document;
      }

      return undefined;
    }
  }]);

  return OptionsReader;
}();

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/TouchBackendImpl.js":
/*!***************************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/TouchBackendImpl.js ***!
  \***************************************************************************/
/*! exports provided: TouchBackendImpl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TouchBackendImpl", function() { return TouchBackendImpl; });
/* harmony import */ var _react_dnd_invariant__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @react-dnd/invariant */ "./node_modules/@react-dnd/invariant/dist/invariant.esm.js");
/* harmony import */ var _interfaces__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces */ "./node_modules/react-dnd-touch-backend/dist/esm/interfaces.js");
/* harmony import */ var _utils_predicates__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/predicates */ "./node_modules/react-dnd-touch-backend/dist/esm/utils/predicates.js");
/* harmony import */ var _utils_offsets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils/offsets */ "./node_modules/react-dnd-touch-backend/dist/esm/utils/offsets.js");
/* harmony import */ var _utils_math__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utils/math */ "./node_modules/react-dnd-touch-backend/dist/esm/utils/math.js");
/* harmony import */ var _utils_supportsPassive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils/supportsPassive */ "./node_modules/react-dnd-touch-backend/dist/esm/utils/supportsPassive.js");
/* harmony import */ var _OptionsReader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./OptionsReader */ "./node_modules/react-dnd-touch-backend/dist/esm/OptionsReader.js");
var _eventNames;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var eventNames = (_eventNames = {}, _defineProperty(_eventNames, _interfaces__WEBPACK_IMPORTED_MODULE_1__["ListenerType"].mouse, {
  start: 'mousedown',
  move: 'mousemove',
  end: 'mouseup',
  contextmenu: 'contextmenu'
}), _defineProperty(_eventNames, _interfaces__WEBPACK_IMPORTED_MODULE_1__["ListenerType"].touch, {
  start: 'touchstart',
  move: 'touchmove',
  end: 'touchend'
}), _defineProperty(_eventNames, _interfaces__WEBPACK_IMPORTED_MODULE_1__["ListenerType"].keyboard, {
  keydown: 'keydown'
}), _eventNames);
var TouchBackendImpl = /*#__PURE__*/function () {
  function TouchBackendImpl(manager, context, options) {
    var _this = this;

    _classCallCheck(this, TouchBackendImpl);

    this.getSourceClientOffset = function (sourceId) {
      var element = _this.sourceNodes.get(sourceId);

      return element && Object(_utils_offsets__WEBPACK_IMPORTED_MODULE_3__["getNodeClientOffset"])(element);
    };

    this.handleTopMoveStartCapture = function (e) {
      if (!Object(_utils_predicates__WEBPACK_IMPORTED_MODULE_2__["eventShouldStartDrag"])(e)) {
        return;
      }

      _this.moveStartSourceIds = [];
    };

    this.handleMoveStart = function (sourceId) {
      // Just because we received an event doesn't necessarily mean we need to collect drag sources.
      // We only collect start collecting drag sources on touch and left mouse events.
      if (Array.isArray(_this.moveStartSourceIds)) {
        _this.moveStartSourceIds.unshift(sourceId);
      }
    };

    this.handleTopMoveStart = function (e) {
      if (!Object(_utils_predicates__WEBPACK_IMPORTED_MODULE_2__["eventShouldStartDrag"])(e)) {
        return;
      } // Don't prematurely preventDefault() here since it might:
      // 1. Mess up scrolling
      // 2. Mess up long tap (which brings up context menu)
      // 3. If there's an anchor link as a child, tap won't be triggered on link


      var clientOffset = Object(_utils_offsets__WEBPACK_IMPORTED_MODULE_3__["getEventClientOffset"])(e);

      if (clientOffset) {
        if (Object(_utils_predicates__WEBPACK_IMPORTED_MODULE_2__["isTouchEvent"])(e)) {
          _this.lastTargetTouchFallback = e.targetTouches[0];
        }

        _this._mouseClientOffset = clientOffset;
      }

      _this.waitingForDelay = false;
    };

    this.handleTopMoveStartDelay = function (e) {
      if (!Object(_utils_predicates__WEBPACK_IMPORTED_MODULE_2__["eventShouldStartDrag"])(e)) {
        return;
      }

      var delay = e.type === eventNames.touch.start ? _this.options.delayTouchStart : _this.options.delayMouseStart;
      _this.timeout = setTimeout(_this.handleTopMoveStart.bind(_this, e), delay);
      _this.waitingForDelay = true;
    };

    this.handleTopMoveCapture = function () {
      _this.dragOverTargetIds = [];
    };

    this.handleMove = function (_evt, targetId) {
      if (_this.dragOverTargetIds) {
        _this.dragOverTargetIds.unshift(targetId);
      }
    };

    this.handleTopMove = function (e) {
      if (_this.timeout) {
        clearTimeout(_this.timeout);
      }

      if (!_this.document || _this.waitingForDelay) {
        return;
      }

      var moveStartSourceIds = _this.moveStartSourceIds,
          dragOverTargetIds = _this.dragOverTargetIds;
      var enableHoverOutsideTarget = _this.options.enableHoverOutsideTarget;
      var clientOffset = Object(_utils_offsets__WEBPACK_IMPORTED_MODULE_3__["getEventClientOffset"])(e, _this.lastTargetTouchFallback);

      if (!clientOffset) {
        return;
      } // If the touch move started as a scroll, or is is between the scroll angles


      if (_this._isScrolling || !_this.monitor.isDragging() && Object(_utils_math__WEBPACK_IMPORTED_MODULE_4__["inAngleRanges"])(_this._mouseClientOffset.x || 0, _this._mouseClientOffset.y || 0, clientOffset.x, clientOffset.y, _this.options.scrollAngleRanges)) {
        _this._isScrolling = true;
        return;
      } // If we're not dragging and we've moved a little, that counts as a drag start


      if (!_this.monitor.isDragging() && // eslint-disable-next-line no-prototype-builtins
      _this._mouseClientOffset.hasOwnProperty('x') && moveStartSourceIds && Object(_utils_math__WEBPACK_IMPORTED_MODULE_4__["distance"])(_this._mouseClientOffset.x || 0, _this._mouseClientOffset.y || 0, clientOffset.x, clientOffset.y) > (_this.options.touchSlop ? _this.options.touchSlop : 0)) {
        _this.moveStartSourceIds = undefined;

        _this.actions.beginDrag(moveStartSourceIds, {
          clientOffset: _this._mouseClientOffset,
          getSourceClientOffset: _this.getSourceClientOffset,
          publishSource: false
        });
      }

      if (!_this.monitor.isDragging()) {
        return;
      }

      var sourceNode = _this.sourceNodes.get(_this.monitor.getSourceId());

      _this.installSourceNodeRemovalObserver(sourceNode);

      _this.actions.publishDragSource();

      if (e.cancelable) e.preventDefault(); // Get the node elements of the hovered DropTargets

      var dragOverTargetNodes = (dragOverTargetIds || []).map(function (key) {
        return _this.targetNodes.get(key);
      }).filter(function (e) {
        return !!e;
      }); // Get the a ordered list of nodes that are touched by

      var elementsAtPoint = _this.options.getDropTargetElementsAtPoint ? _this.options.getDropTargetElementsAtPoint(clientOffset.x, clientOffset.y, dragOverTargetNodes) : _this.document.elementsFromPoint(clientOffset.x, clientOffset.y); // Extend list with parents that are not receiving elementsFromPoint events (size 0 elements and svg groups)

      var elementsAtPointExtended = [];

      for (var nodeId in elementsAtPoint) {
        // eslint-disable-next-line no-prototype-builtins
        if (!elementsAtPoint.hasOwnProperty(nodeId)) {
          continue;
        }

        var currentNode = elementsAtPoint[nodeId];
        elementsAtPointExtended.push(currentNode);

        while (currentNode) {
          currentNode = currentNode.parentElement;

          if (currentNode && elementsAtPointExtended.indexOf(currentNode) === -1) {
            elementsAtPointExtended.push(currentNode);
          }
        }
      }

      var orderedDragOverTargetIds = elementsAtPointExtended // Filter off nodes that arent a hovered DropTargets nodes
      .filter(function (node) {
        return dragOverTargetNodes.indexOf(node) > -1;
      }) // Map back the nodes elements to targetIds
      .map(function (node) {
        return _this._getDropTargetId(node);
      }) // Filter off possible null rows
      .filter(function (node) {
        return !!node;
      }).filter(function (id, index, ids) {
        return ids.indexOf(id) === index;
      }); // Invoke hover for drop targets when source node is still over and pointer is outside

      if (enableHoverOutsideTarget) {
        for (var targetId in _this.targetNodes) {
          var targetNode = _this.targetNodes.get(targetId);

          if (sourceNode && targetNode && targetNode.contains(sourceNode) && orderedDragOverTargetIds.indexOf(targetId) === -1) {
            orderedDragOverTargetIds.unshift(targetId);
            break;
          }
        }
      } // Reverse order because dnd-core reverse it before calling the DropTarget drop methods


      orderedDragOverTargetIds.reverse();

      _this.actions.hover(orderedDragOverTargetIds, {
        clientOffset: clientOffset
      });
    };
    /**
     *
     * visible for testing
     */


    this._getDropTargetId = function (node) {
      var keys = _this.targetNodes.keys();

      var next = keys.next();

      while (next.done === false) {
        var targetId = next.value;

        if (node === _this.targetNodes.get(targetId)) {
          return targetId;
        } else {
          next = keys.next();
        }
      }

      return undefined;
    };

    this.handleTopMoveEndCapture = function (e) {
      _this._isScrolling = false;
      _this.lastTargetTouchFallback = undefined;

      if (!Object(_utils_predicates__WEBPACK_IMPORTED_MODULE_2__["eventShouldEndDrag"])(e)) {
        return;
      }

      if (!_this.monitor.isDragging() || _this.monitor.didDrop()) {
        _this.moveStartSourceIds = undefined;
        return;
      }

      if (e.cancelable) e.preventDefault();
      _this._mouseClientOffset = {};

      _this.uninstallSourceNodeRemovalObserver();

      _this.actions.drop();

      _this.actions.endDrag();
    };

    this.handleCancelOnEscape = function (e) {
      if (e.key === 'Escape' && _this.monitor.isDragging()) {
        _this._mouseClientOffset = {};

        _this.uninstallSourceNodeRemovalObserver();

        _this.actions.endDrag();
      }
    };

    this.options = new _OptionsReader__WEBPACK_IMPORTED_MODULE_6__["OptionsReader"](options, context);
    this.actions = manager.getActions();
    this.monitor = manager.getMonitor();
    this.sourceNodes = new Map();
    this.sourcePreviewNodes = new Map();
    this.sourcePreviewNodeOptions = new Map();
    this.targetNodes = new Map();
    this.listenerTypes = [];
    this._mouseClientOffset = {};
    this._isScrolling = false;

    if (this.options.enableMouseEvents) {
      this.listenerTypes.push(_interfaces__WEBPACK_IMPORTED_MODULE_1__["ListenerType"].mouse);
    }

    if (this.options.enableTouchEvents) {
      this.listenerTypes.push(_interfaces__WEBPACK_IMPORTED_MODULE_1__["ListenerType"].touch);
    }

    if (this.options.enableKeyboardEvents) {
      this.listenerTypes.push(_interfaces__WEBPACK_IMPORTED_MODULE_1__["ListenerType"].keyboard);
    }
  }
  /**
   * Generate profiling statistics for the HTML5Backend.
   */


  _createClass(TouchBackendImpl, [{
    key: "profile",
    value: function profile() {
      var _this$dragOverTargetI;

      return {
        sourceNodes: this.sourceNodes.size,
        sourcePreviewNodes: this.sourcePreviewNodes.size,
        sourcePreviewNodeOptions: this.sourcePreviewNodeOptions.size,
        targetNodes: this.targetNodes.size,
        dragOverTargetIds: ((_this$dragOverTargetI = this.dragOverTargetIds) === null || _this$dragOverTargetI === void 0 ? void 0 : _this$dragOverTargetI.length) || 0
      };
    } // public for test

  }, {
    key: "setup",
    value: function setup() {
      if (!this.window) {
        return;
      }

      Object(_react_dnd_invariant__WEBPACK_IMPORTED_MODULE_0__["invariant"])(!TouchBackendImpl.isSetUp, 'Cannot have two Touch backends at the same time.');
      TouchBackendImpl.isSetUp = true;
      this.addEventListener(this.window, 'start', this.getTopMoveStartHandler());
      this.addEventListener(this.window, 'start', this.handleTopMoveStartCapture, true);
      this.addEventListener(this.window, 'move', this.handleTopMove);
      this.addEventListener(this.window, 'move', this.handleTopMoveCapture, true);
      this.addEventListener(this.window, 'end', this.handleTopMoveEndCapture, true);

      if (this.options.enableMouseEvents && !this.options.ignoreContextMenu) {
        this.addEventListener(this.window, 'contextmenu', this.handleTopMoveEndCapture);
      }

      if (this.options.enableKeyboardEvents) {
        this.addEventListener(this.window, 'keydown', this.handleCancelOnEscape, true);
      }
    }
  }, {
    key: "teardown",
    value: function teardown() {
      if (!this.window) {
        return;
      }

      TouchBackendImpl.isSetUp = false;
      this._mouseClientOffset = {};
      this.removeEventListener(this.window, 'start', this.handleTopMoveStartCapture, true);
      this.removeEventListener(this.window, 'start', this.handleTopMoveStart);
      this.removeEventListener(this.window, 'move', this.handleTopMoveCapture, true);
      this.removeEventListener(this.window, 'move', this.handleTopMove);
      this.removeEventListener(this.window, 'end', this.handleTopMoveEndCapture, true);

      if (this.options.enableMouseEvents && !this.options.ignoreContextMenu) {
        this.removeEventListener(this.window, 'contextmenu', this.handleTopMoveEndCapture);
      }

      if (this.options.enableKeyboardEvents) {
        this.removeEventListener(this.window, 'keydown', this.handleCancelOnEscape, true);
      }

      this.uninstallSourceNodeRemovalObserver();
    }
  }, {
    key: "addEventListener",
    value: function addEventListener(subject, event, handler, capture) {
      var options = _utils_supportsPassive__WEBPACK_IMPORTED_MODULE_5__["supportsPassive"] ? {
        capture: capture,
        passive: false
      } : capture;
      this.listenerTypes.forEach(function (listenerType) {
        var evt = eventNames[listenerType][event];

        if (evt) {
          subject.addEventListener(evt, handler, options);
        }
      });
    }
  }, {
    key: "removeEventListener",
    value: function removeEventListener(subject, event, handler, capture) {
      var options = _utils_supportsPassive__WEBPACK_IMPORTED_MODULE_5__["supportsPassive"] ? {
        capture: capture,
        passive: false
      } : capture;
      this.listenerTypes.forEach(function (listenerType) {
        var evt = eventNames[listenerType][event];

        if (evt) {
          subject.removeEventListener(evt, handler, options);
        }
      });
    }
  }, {
    key: "connectDragSource",
    value: function connectDragSource(sourceId, node) {
      var _this2 = this;

      var handleMoveStart = this.handleMoveStart.bind(this, sourceId);
      this.sourceNodes.set(sourceId, node);
      this.addEventListener(node, 'start', handleMoveStart);
      return function () {
        _this2.sourceNodes.delete(sourceId);

        _this2.removeEventListener(node, 'start', handleMoveStart);
      };
    }
  }, {
    key: "connectDragPreview",
    value: function connectDragPreview(sourceId, node, options) {
      var _this3 = this;

      this.sourcePreviewNodeOptions.set(sourceId, options);
      this.sourcePreviewNodes.set(sourceId, node);
      return function () {
        _this3.sourcePreviewNodes.delete(sourceId);

        _this3.sourcePreviewNodeOptions.delete(sourceId);
      };
    }
  }, {
    key: "connectDropTarget",
    value: function connectDropTarget(targetId, node) {
      var _this4 = this;

      if (!this.document) {
        return function () {
          /* noop */
        };
      }

      var handleMove = function handleMove(e) {
        if (!_this4.document || !_this4.monitor.isDragging()) {
          return;
        }

        var coords;
        /**
         * Grab the coordinates for the current mouse/touch position
         */

        switch (e.type) {
          case eventNames.mouse.move:
            coords = {
              x: e.clientX,
              y: e.clientY
            };
            break;

          case eventNames.touch.move:
            coords = {
              x: e.touches[0].clientX,
              y: e.touches[0].clientY
            };
            break;
        }
        /**
         * Use the coordinates to grab the element the drag ended on.
         * If the element is the same as the target node (or any of it's children) then we have hit a drop target and can handle the move.
         */


        var droppedOn = coords != null ? _this4.document.elementFromPoint(coords.x, coords.y) : undefined;
        var childMatch = droppedOn && node.contains(droppedOn);

        if (droppedOn === node || childMatch) {
          return _this4.handleMove(e, targetId);
        }
      };
      /**
       * Attaching the event listener to the body so that touchmove will work while dragging over multiple target elements.
       */


      this.addEventListener(this.document.body, 'move', handleMove);
      this.targetNodes.set(targetId, node);
      return function () {
        if (_this4.document) {
          _this4.targetNodes.delete(targetId);

          _this4.removeEventListener(_this4.document.body, 'move', handleMove);
        }
      };
    }
  }, {
    key: "getTopMoveStartHandler",
    value: function getTopMoveStartHandler() {
      if (!this.options.delayTouchStart && !this.options.delayMouseStart) {
        return this.handleTopMoveStart;
      }

      return this.handleTopMoveStartDelay;
    }
  }, {
    key: "installSourceNodeRemovalObserver",
    value: function installSourceNodeRemovalObserver(node) {
      var _this5 = this;

      this.uninstallSourceNodeRemovalObserver();
      this.draggedSourceNode = node;
      this.draggedSourceNodeRemovalObserver = new MutationObserver(function () {
        if (node && !node.parentElement) {
          _this5.resurrectSourceNode();

          _this5.uninstallSourceNodeRemovalObserver();
        }
      });

      if (!node || !node.parentElement) {
        return;
      }

      this.draggedSourceNodeRemovalObserver.observe(node.parentElement, {
        childList: true
      });
    }
  }, {
    key: "resurrectSourceNode",
    value: function resurrectSourceNode() {
      if (this.document && this.draggedSourceNode) {
        this.draggedSourceNode.style.display = 'none';
        this.draggedSourceNode.removeAttribute('data-reactid');
        this.document.body.appendChild(this.draggedSourceNode);
      }
    }
  }, {
    key: "uninstallSourceNodeRemovalObserver",
    value: function uninstallSourceNodeRemovalObserver() {
      if (this.draggedSourceNodeRemovalObserver) {
        this.draggedSourceNodeRemovalObserver.disconnect();
      }

      this.draggedSourceNodeRemovalObserver = undefined;
      this.draggedSourceNode = undefined;
    }
  }, {
    key: "window",
    get: function get() {
      return this.options.window;
    } // public for test

  }, {
    key: "document",
    get: function get() {
      if (this.window) {
        return this.window.document;
      }

      return undefined;
    }
  }]);

  return TouchBackendImpl;
}();

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/index.js ***!
  \****************************************************************/
/*! exports provided: TouchBackendImpl, TouchBackend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TouchBackend", function() { return TouchBackend; });
/* harmony import */ var _TouchBackendImpl__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TouchBackendImpl */ "./node_modules/react-dnd-touch-backend/dist/esm/TouchBackendImpl.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TouchBackendImpl", function() { return _TouchBackendImpl__WEBPACK_IMPORTED_MODULE_0__["TouchBackendImpl"]; });



var TouchBackend = function createBackend(manager) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  return new _TouchBackendImpl__WEBPACK_IMPORTED_MODULE_0__["TouchBackendImpl"](manager, context, options);
};

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/interfaces.js":
/*!*********************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/interfaces.js ***!
  \*********************************************************************/
/*! exports provided: ListenerType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListenerType", function() { return ListenerType; });
var ListenerType;

(function (ListenerType) {
  ListenerType["mouse"] = "mouse";
  ListenerType["touch"] = "touch";
  ListenerType["keyboard"] = "keyboard";
})(ListenerType || (ListenerType = {}));

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/utils/math.js":
/*!*********************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/utils/math.js ***!
  \*********************************************************************/
/*! exports provided: distance, inAngleRanges */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "distance", function() { return distance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "inAngleRanges", function() { return inAngleRanges; });
function distance(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow(Math.abs(x2 - x1), 2) + Math.pow(Math.abs(y2 - y1), 2));
}
function inAngleRanges(x1, y1, x2, y2, angleRanges) {
  if (!angleRanges) {
    return false;
  }

  var angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI + 180;

  for (var i = 0; i < angleRanges.length; ++i) {
    if ((angleRanges[i].start == null || angle >= angleRanges[i].start) && (angleRanges[i].end == null || angle <= angleRanges[i].end)) {
      return true;
    }
  }

  return false;
}

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/utils/offsets.js":
/*!************************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/utils/offsets.js ***!
  \************************************************************************/
/*! exports provided: getNodeClientOffset, getEventClientTouchOffset, getEventClientOffset */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getNodeClientOffset", function() { return getNodeClientOffset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEventClientTouchOffset", function() { return getEventClientTouchOffset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEventClientOffset", function() { return getEventClientOffset; });
/* harmony import */ var _predicates__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./predicates */ "./node_modules/react-dnd-touch-backend/dist/esm/utils/predicates.js");

var ELEMENT_NODE = 1;
function getNodeClientOffset(node) {
  var el = node.nodeType === ELEMENT_NODE ? node : node.parentElement;

  if (!el) {
    return undefined;
  }

  var _el$getBoundingClient = el.getBoundingClientRect(),
      top = _el$getBoundingClient.top,
      left = _el$getBoundingClient.left;

  return {
    x: left,
    y: top
  };
}
function getEventClientTouchOffset(e, lastTargetTouchFallback) {
  if (e.targetTouches.length === 1) {
    return getEventClientOffset(e.targetTouches[0]);
  } else if (lastTargetTouchFallback && e.touches.length === 1) {
    if (e.touches[0].target === lastTargetTouchFallback.target) {
      return getEventClientOffset(e.touches[0]);
    }
  }
}
function getEventClientOffset(e, lastTargetTouchFallback) {
  if (Object(_predicates__WEBPACK_IMPORTED_MODULE_0__["isTouchEvent"])(e)) {
    return getEventClientTouchOffset(e, lastTargetTouchFallback);
  } else {
    return {
      x: e.clientX,
      y: e.clientY
    };
  }
}

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/utils/predicates.js":
/*!***************************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/utils/predicates.js ***!
  \***************************************************************************/
/*! exports provided: eventShouldStartDrag, eventShouldEndDrag, isTouchEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventShouldStartDrag", function() { return eventShouldStartDrag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventShouldEndDrag", function() { return eventShouldEndDrag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isTouchEvent", function() { return isTouchEvent; });
// Used for MouseEvent.buttons (note the s on the end).
var MouseButtons = {
  Left: 1,
  Right: 2,
  Center: 4
}; // Used for e.button (note the lack of an s on the end).

var MouseButton = {
  Left: 0,
  Center: 1,
  Right: 2
};
/**
 * Only touch events and mouse events where the left button is pressed should initiate a drag.
 * @param {MouseEvent | TouchEvent} e The event
 */

function eventShouldStartDrag(e) {
  // For touch events, button will be undefined. If e.button is defined,
  // then it should be MouseButton.Left.
  return e.button === undefined || e.button === MouseButton.Left;
}
/**
 * Only touch events and mouse events where the left mouse button is no longer held should end a drag.
 * It's possible the user mouse downs with the left mouse button, then mouse down and ups with the right mouse button.
 * We don't want releasing the right mouse button to end the drag.
 * @param {MouseEvent | TouchEvent} e The event
 */

function eventShouldEndDrag(e) {
  // Touch events will have buttons be undefined, while mouse events will have e.buttons's left button
  // bit field unset if the left mouse button has been released
  return e.buttons === undefined || (e.buttons & MouseButtons.Left) === 0;
}
function isTouchEvent(e) {
  return !!e.targetTouches;
}

/***/ }),

/***/ "./node_modules/react-dnd-touch-backend/dist/esm/utils/supportsPassive.js":
/*!********************************************************************************!*\
  !*** ./node_modules/react-dnd-touch-backend/dist/esm/utils/supportsPassive.js ***!
  \********************************************************************************/
/*! exports provided: supportsPassive */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsPassive", function() { return supportsPassive; });
var supportsPassive = function () {
  // simular to jQuery's test
  var supported = false;

  try {
    addEventListener('test', function () {// do nothing
    }, Object.defineProperty({}, 'passive', {
      get: function get() {
        supported = true;
        return true;
      }
    }));
  } catch (e) {// do nothing
  }

  return supported;
}();

/***/ })

}]);
//# sourceMappingURL=1.chunk.js.map