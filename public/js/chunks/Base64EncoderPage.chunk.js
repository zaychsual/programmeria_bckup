(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Base64EncoderPage"],{

/***/ "./resources/js/src/apps/Base64Encoder.js":
/*!************************************************!*\
  !*** ./resources/js/src/apps/Base64Encoder.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Base64Encoder; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/q-ui-react/Textarea */ "./resources/js/src/components/q-ui-react/Textarea.js");
/* harmony import */ var _components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/q-ui-react/Img */ "./resources/js/src/components/q-ui-react/Img.js");
/* harmony import */ var _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utils/file/fileRead */ "./resources/js/src/utils/file/fileRead.js");
/* harmony import */ var _utils_clipboard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils/clipboard */ "./resources/js/src/utils/clipboard/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // , { createRef }









var DATE_OPTIONS = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
};
var SYMBOLS = /[\r\n%#()<>?\[\\\]^`{|}]/g;
var EXM_SVG = "<svg>\n\t<circle r=\"50\" cx=\"50\" cy=\"50\" fill=\"tomato\"/>\n\t<circle r=\"41\" cx=\"47\" cy=\"50\" fill=\"orange\"/>\n\t<circle r=\"33\" cx=\"48\" cy=\"53\" fill=\"gold\"/>\n\t<circle r=\"25\" cx=\"49\" cy=\"51\" fill=\"yellowgreen\"/>\n\t<circle r=\"17\" cx=\"52\" cy=\"50\" fill=\"lightseagreen\"/>\n\t<circle r=\"9\" cx=\"55\" cy=\"48\" fill=\"teal\"/>\n</svg>";
var TB_SIZE = {
  tabSize: 2
};

var Base64Encoder = /*#__PURE__*/function (_React$Component) {
  _inherits(Base64Encoder, _React$Component);

  var _super = _createSuper(Base64Encoder);

  function Base64Encoder(props) {
    var _this;

    _classCallCheck(this, Base64Encoder);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "encodeSVG", function (data) {
      var externalQuotesValue = _this.state.externalQuotesValue; // Use single quotes instead of double to avoid encoding.

      if (externalQuotesValue === "\"") {
        data = data.replace(/"/g, '\'');
      } else {
        data = data.replace(/'/g, '"');
      }

      data = data.replace(/>\s{1,}</g, "><");
      data = data.replace(/\s{2,}/g, " ");
      return data.replace(SYMBOLS, encodeURIComponent);
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeInit", function (e) {
      var val = e.target.value;

      _this.setState({
        initTextarea: val,
        resultTextarea: val.length > 0 ? _this.encodeSVG(val) : val
      }, function () {
        _this.getResults();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getResults", function () {
      var _this$state = _this.state,
          initTextarea = _this$state.initTextarea,
          externalQuotesValue = _this$state.externalQuotesValue;

      if (initTextarea.length < 1) {
        _this.setState({
          resultCssTextarea: "",
          bgSvg: null
        });

        return;
      } // let namespaced = this.addNameSpace(initTextarea);


      var escaped = _this.encodeSVG(_this.addNameSpace(initTextarea)); // background-image: 
      // let bg = escaped ? `url(${externalQuotesValue}data:image/svg+xml,${escaped}${externalQuotesValue})` : "";


      _this.setState({
        // bgSvg: bg, 
        resultTextarea: escaped // resultCssTextarea: bg

      }, function () {
        if (escaped) {
          var bgSvg = "url(".concat(externalQuotesValue, "data:image/svg+xml,").concat(escaped).concat(externalQuotesValue, ")");

          _this.setState({
            bgSvg: bgSvg,
            resultCssTextarea: bgSvg
          });
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onBackground", function (bg) {
      return _this.setState({
        bg: bg
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onSetQuotes", function (q) {
      if (_this.state.externalQuotesValue === q) return;

      _this.setState({
        externalQuotesValue: q
      }, function () {
        _this.getResults();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onExample", function () {
      _this.setState({
        initTextarea: EXM_SVG // resultTextarea: this.encodeSVG(svg)

      }, function () {
        _this.getResults();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onCopy", function (e, t) {
      var et = e.target;
      Object(_utils_clipboard__WEBPACK_IMPORTED_MODULE_8__["clipboardCopy"])(t).then(function () {
        Q.setAttr(et, {
          "aria-label": "Copied!"
        });
        setTimeout(function () {
          return Q.setAttr(et, "aria-label");
        }, 999);
      })["catch"](function (e) {
        return console.log(e);
      });
    });

    _this.state = {
      initTextarea: "",
      resultTextarea: "",
      resultCssTextarea: "",
      externalQuotesValue: "'",
      // 'single'
      bg: null,
      bgSvg: null,
      files: null // For bitmap

    }; // this.symbols = /[\r\n%#()<>?\[\\\]^`{|}]/g;

    return _this;
  }

  _createClass(Base64Encoder, [{
    key: "addNameSpace",
    value: function addNameSpace(data) {
      if (data.indexOf('http://www.w3.org/2000/svg') < 0) {
        data = data.replace(/<svg/g, "<svg xmlns=\"http://www.w3.org/2000/svg\"");
      }

      return data.trim();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state2 = this.state,
          initTextarea = _this$state2.initTextarea,
          resultTextarea = _this$state2.resultTextarea,
          resultCssTextarea = _this$state2.resultCssTextarea,
          externalQuotesValue = _this$state2.externalQuotesValue,
          bgSvg = _this$state2.bgSvg,
          bg = _this$state2.bg,
          files = _this$state2.files; // const { pos, tipTxt } = this.props;
      // <div className="container-fluid py-3">

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-white p-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
        className: "hr-h"
      }, "URL-encoder for image"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_1__["default"].Container, {
        mountOnEnter: true,
        id: "base64-tools",
        defaultActiveKey: "bitmap"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_2__["default"], {
        variant: "pills"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_2__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_2__["default"].Link, {
        as: "button",
        type: "button",
        eventKey: "bitmap",
        className: "btn"
      }, "Bitmap")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_2__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_2__["default"].Link, {
        as: "button",
        type: "button",
        eventKey: "svg",
        className: "btn"
      }, "Svg / vector"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_1__["default"].Content, {
        className: "mt-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_1__["default"].Pane, {
        eventKey: "bitmap"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Bitmap"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "custom-file mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "file",
        className: "custom-file-input",
        id: "file",
        accept: ".jpg,.JPG,.jpeg,.JPEG,.png,.PNG,.gif,.GIF,.bmp,.BMP,.svg,.SVG,.webp,.WEBP",
        onChange: function onChange(e) {
          var file = e.target.files[0];

          if (file) {
            console.log(file);
            Object(_utils_file_fileRead__WEBPACK_IMPORTED_MODULE_7__["fileRead"])(file).then(function (r) {
              console.log(r);

              _this2.setState({
                files: r
              });
            })["catch"](function (e) {
              return console.log(e);
            });
          }
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        className: "custom-file-label",
        htmlFor: "file"
      }, files ? files.file.name : "Choose file")), files &&
      /*#__PURE__*/
      // Q.dateObj(file.lastModified)
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Result"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_6__["default"], {
        thumb: true,
        alt: files.file.name,
        src: files.result
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "alert alert-info pre-wrap mt-2"
      }, "File name\t\t\t: ".concat(files.file.name, "\nFile size\t\t\t: ").concat(Q.bytes2Size(files.file.size), "\nFile type\t\t\t: ").concat(files.file.type, "\nLast modified\t\t: ").concat(new Date(files.file.lastModified).toLocaleDateString("id-ID", DATE_OPTIONS), " \n")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "form-group mt-23"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
        As: "label",
        htmlFor: "base64text",
        align: "center"
      }, "Base64", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        onClick: function onClick(e) {
          return _this2.onCopy(e, files.result);
        },
        size: "xs",
        className: "tip tipTR ml-auto"
      }, "Copy")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", {
        className: "form-control form-control-sm bg-white",
        id: "base64text",
        readOnly: true,
        rows: 9,
        value: files.result
      }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_1__["default"].Pane, {
        eventKey: "svg"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Svg / vector"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
        className: "py-2 ml-1-next"
      }, "External quotes:", " ", [{
        q: "'",
        t: "Single"
      }, {
        q: "\"",
        t: "Double"
      }].map(function (v) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
          key: v.t,
          outline: true,
          size: "xs",
          kind: "info",
          onClick: function onClick() {
            return _this2.onSetQuotes(v.q);
          },
          active: externalQuotesValue === v.q,
          title: v.t + " quotes"
        }, v.t);
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-6 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
        justify: "between",
        className: "mb-1"
      }, "Insert SVG code: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        onClick: this.onExample,
        size: "xs",
        kind: "info"
      }, "Example")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_5__["default"], {
        rows: "6",
        style: TB_SIZE,
        spellCheck: "false",
        value: initTextarea,
        onChange: this.onChangeInit
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_4__["default"], {
        justify: "between",
        className: "mt-3 mb-1"
      }, "Preview:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        className: "btn btn-xs btn-fff",
        type: "color",
        title: "Change background color",
        onChange: function onChange(e) {
          return _this2.onBackground(e.target.value);
        }
      }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
        onClick: function onClick() {
          return _this2.onBackground(null);
        },
        size: "xs",
        kind: "fff"
      }, "Transparent"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "rounded svg2url-view",
        style: {
          backgroundColor: bg
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-no-repeat ovauto resize-native resize-v qi qi-arrows-v",
        style: {
          backgroundImage: bgSvg,
          height: 156
        }
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-6 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "mb-1"
      }, "Take encoded:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_5__["default"], {
        style: TB_SIZE,
        readOnly: true,
        rows: "6",
        value: resultTextarea
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "mt-3 mb-1"
      }, "Ready for CSS:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_5__["default"], {
        style: TB_SIZE,
        readOnly: true,
        rows: "6",
        value: resultCssTextarea
      }))))))));
    }
  }]);

  return Base64Encoder;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);



/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Img.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Img.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Img; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // , { useState }
// import { Cx } from '../../utils/Q.js';
// errorText, errorDesc, onError, src, 

function Img(_ref) {
  var inRef = _ref.inRef,
      _ref$alt = _ref.alt,
      alt = _ref$alt === void 0 ? "" : _ref$alt,
      src = _ref.src,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? "lazy" : _ref$loading,
      _ref$drag = _ref.drag,
      drag = _ref$drag === void 0 ? false : _ref$drag,
      WrapAs = _ref.WrapAs,
      w = _ref.w,
      h = _ref.h,
      fluid = _ref.fluid,
      thumb = _ref.thumb,
      circle = _ref.circle,
      round = _ref.round,
      className = _ref.className,
      frameClass = _ref.frameClass,
      frame = _ref.frame,
      caption = _ref.caption,
      captionClass = _ref.captionClass,
      onLoad = _ref.onLoad,
      onError = _ref.onError,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "alt", "src", "loading", "drag", "WrapAs", "w", "h", "fluid", "thumb", "circle", "round", "className", "frameClass", "frame", "caption", "captionClass", "onLoad", "onError", "children"]);

  // const [load, setLoad] = useState(false);
  // const [err, setErr] = useState(null);
  // const support = 'loading' in HTMLImageElement.prototype;
  // const isLazy = SUPPORT_LOADING && loading === 'lazy';
  var bsFigure = frame === 'bs';
  var As = bsFigure ? "figure" : WrapAs ? WrapAs : "picture";

  var setCx = function setCx(et) {
    Q.setClass(et, "ava-loader", "remove"); // if(fluid || thumb) Q.setAttr(et, "height");
  };

  var Load = function Load(e) {
    // Q.setClass(e.target, "ava-loader", "remove");// OPTION: remove or Not
    setCx(e.target);
    if (onLoad) onLoad(e);
  }; // DEV: 


  var Error = function Error(e) {
    var et = e.target; // Q.setClass(et, "img-err");
    // Q.setAttr(et, {"aria-label":`&#xF03E;`});
    // setErr("&#xF03E;"); 

    setCx(et);
    et.src = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='95' viewBox='0 0 32 32'%3E%3Cg%3E%3Cpath d='M29,4H3A1,1,0,0,0,2,5V27a1,1,0,0,0,1,1H29a1,1,0,0,0,1-1V5A1,1,0,0,0,29,4ZM28,26H4V6H28Z'/%3E%3Cpath d='M18.29,12.29a1,1,0,0,1,1.42,0L26,18.59V9a1,1,0,0,0-1-1H7A1,1,0,0,0,6,9v1.58l7,7Z'/%3E%3Cpath d='M7,24H25a1,1,0,0,0,1-1V21.41l-7-7-5.29,5.3a1,1,0,0,1-1.42,0L6,13.42V23A1,1,0,0,0,7,24Z'/%3E%3C/g%3E%3C/svg%3E";
    if (onError) onError(e);
    return; // null
  };

  var img = function img() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", _extends({}, etc, {
      ref: inRef,
      loading: loading,
      alt: alt // OPTION
      ,
      width: w // isLazy && !w && !load ? 40 : w
      ,
      height: h // isLazy && !h && !load ? 40 : h
      ,
      src: Q.newURL(src).href // OPTION: Q.isDomain(src) ? src : Q.newURL(src).href
      ,
      className: Q.Cx("ava-loader", {
        // holder thumb| ava-loader
        'img-fluid': fluid,
        // fluid && !isLazy
        'img-thumbnail': thumb,
        'rounded-circle': circle,
        'rounded': round,
        'figure-img': bsFigure // 'fal img-err': err

      }, className),
      draggable: drag // !!drag 
      // aria-label={label || err} 
      ,
      onError: Error,
      onLoad: Load // OPTION: 
      // onContextMenu={e => {
      // Q.preventQ(e);
      // }}

      /* onLoad={e => {
      	if(isLazy){
      		setLoad(true);
      		let et = e.target;
      		// setAttr(et, 'width height');
      		setClass(et, 'isLoad');//  + (fluid ? ' img-fluid' : '')
      	}
      		onLoad(e);
      }} */

    }));
  };

  if (frame) {
    // const bsFigure = frame === 'bs';
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As // figure | div 
    // ref={frameRef} 
    , {
      className: // thumb
      Q.Cx("img-frame", {
        'figure': bsFigure
      }, frameClass) // draggable="false" // OPTION

    }, img(), bsFigure && caption && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
      className: Q.Cx("figure-caption", captionClass)
    }, caption), children);
  }

  return img();
} // Img.defaultProps = {
// loading: 'lazy',
// alt: '',
// onLoad: noop
// noDrag: true
// };

/* const error = e => {
	if(onError){
		onError(e);
	}else{
		// data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${this.props.alt}%3C/text%3E%3C/svg%3E
		e.target.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='45%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${errorText ? errorText : alt}%3C/text%3E%3Ctext x='50%25' y='55%25' fill='%23fff' dy='.5em' style='font-size:0.7rem;font-family:sans-serif'%3E${errorDesc ? errorDesc : 'Image not found'}%3C/text%3E%3C/svg%3E`;// "/img/imgError.svg";
		return null;// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
	}
   } */

/* const strNum = P.oneOfType([P.string, P.number]);

Img.propTypes = {
	frame: P.bool,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  alt: P.string,

  w: strNum,
  h: strNum,
	// src: P.string, // .isRequired

  fluid: P.bool,
  thumb: P.bool,
  circle: P.bool,
  round: P.bool,
  noDrag: P.bool,
	onError: P.func,
	onLoad: P.func
}; */

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Textarea.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Textarea.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Textarea; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { Cx } from '../../utils/Q';

function Textarea(_ref) {
  var h = _ref.h,
      value = _ref.value,
      defaultValue = _ref.defaultValue,
      className = _ref.className,
      style = _ref.style,
      onChange = _ref.onChange,
      _ref$bs = _ref.bs,
      bs = _ref$bs === void 0 ? "form-control" : _ref$bs,
      etc = _objectWithoutProperties(_ref, ["h", "value", "defaultValue", "className", "style", "onChange", "bs"]);

  var elRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      txt = _useState2[0],
      setTxt = _useState2[1]; // "" | value || defaultValue || 


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(h),
      _useState4 = _slicedToArray(_useState3, 2),
      height = _useState4[0],
      setHeight = _useState4[1]; // const [parentHeight, setParentHeight] = useState("auto");
  // useEffect(() => {
  //   let el = elRef.current;
  //   if(value || defaultValue){
  //     setHeight(el.scrollHeight + 2);
  //   }
  // }, [value, defaultValue]);


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // setParentHeight(`${elRef.current.scrollHeight}px`);
    var el = elRef.current; // console.log('clientHeight: ', el.clientHeight);
    // console.log('scrollHeight: ', el.scrollHeight);

    if ((value || defaultValue) && el && el.clientHeight !== el.scrollHeight) {
      // text.length > 0 && 
      setHeight(el.scrollHeight + 2); // (el.scrollHeight + 2) + "px"
    } // if(txt || (value || defaultValue)){
    //   setHeight(el.scrollHeight + 2);
    // }
    // console.log('txt: ', txt);
    // console.log('value: ', value);
    // console.log('defaultValue: ', defaultValue);

  }, [txt, value, defaultValue]);

  var Change = function Change(e) {
    setHeight(h); // null | "auto"
    // setParentHeight(`${elRef.current.scrollHeight}px`);

    setTxt(e.target.value); // setHeight(elRef.current.scrollHeight + "px");

    if (onChange) onChange(e);
  }; // const Keyup = (e) => {
  //   let et = e.target;
  //   // setTimeout(() => {
  //     // et.style.height = 'auto';// ;padding:0
  //     // for box-sizing other than "content-box" use:
  //     // et.style.cssText = '-moz-box-sizing:content-box';
  //     let scrl = et.scrollHeight;
  //     // et.value.length === 0
  //     if(et.clientHeight !== scrl){//  && e.keyCode === 8
  //       // et.style.height = h; // 'height:' + h + 'px';
  //       setHeight((scrl + 2) + "px");// setHeight(h);
  //     }
  //     // else{
  //     //   // et.style.height = scrl + 'px';
  //     //   setHeight((scrl + 2) + "px");
  //     // }
  //     // et.classList.remove("ovhide");
  //   // }, 1);
  //   if(onKeyUp) onKeyUp(e);
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", _extends({}, etc, {
    ref: elRef,
    value: value,
    defaultValue: defaultValue,
    className: Q.Cx(bs, className),
    style: _objectSpread(_objectSpread({}, style), {}, {
      height: height
    }) // onKeyUp={Keyup} 
    ,
    onChange: Change
  }));
}
;
/*
<div
  style={{
    minHeight: parentHeight,
  }}
>
    
</div>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/tools/Base64EncoderPage.js":
/*!*****************************************************************!*\
  !*** ./resources/js/src/pages/admin/tools/Base64EncoderPage.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Base64EncoderPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _apps_Base64Encoder__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../apps/Base64Encoder */ "./resources/js/src/apps/Base64Encoder.js");


 // SvgToUrl

function Base64EncoderPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Base64 Encoder"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_Base64Encoder__WEBPACK_IMPORTED_MODULE_2__["default"], null));
}

/***/ }),

/***/ "./resources/js/src/utils/clipboard/index.js":
/*!***************************************************!*\
  !*** ./resources/js/src/utils/clipboard/index.js ***!
  \***************************************************/
/*! exports provided: clipboardCopy, copyFn, pasteBlob, permissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clipboardCopy", function() { return clipboardCopy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyFn", function() { return copyFn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pasteBlob", function() { return pasteBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "permissions", function() { return permissions; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Copy to clipboard:
function clipboardCopy(target) {
  return new Promise(function (resolve, reject) {
    var clipboard = navigator.clipboard,
        txt;
    if (typeof target === 'string' && !target.tagName) txt = target;else if (target === null || target === void 0 ? void 0 : target.tagName) txt = target.textContent; // target.innerText
    else reject(new Error('Text or target DOM to copy is required.'));

    if (clipboard) {
      clipboard.writeText(txt).then(function () {
        return resolve(txt);
      })["catch"](function (e) {
        return reject(e);
      });
    } else {
      console.log('%cnavigator.clipboard NOT SUPPORT', 'color:yellow');
      copyFn(txt, {
        onOk: function onOk() {
          resolve(txt);
        },
        onErr: function onErr(e) {
          reject(e);
        }
      });
    }
  });
}

function copyFn(str, _ref) {
  var _ref$onOk = _ref.onOk,
      onOk = _ref$onOk === void 0 ? function () {} : _ref$onOk,
      _ref$onErr = _ref.onErr,
      onErr = _ref$onErr === void 0 ? function () {} : _ref$onErr;
  var DOC = document,
      el = DOC.createElement("textarea"),
      iOS = window.navigator.userAgent.match(/ipad|iphone/i);
  el.className = "sr-only sr-only-focusable";
  el.contentEditable = true; // needed for iOS >= 10

  el.readOnly = false; // needed for iOS >= 10

  el.value = str; // el.style.border = "0";
  // el.style.padding = "0";
  // el.style.margin = "0";
  // el.style.position = "absolute";
  // sets vertical scroll
  // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
  // el.style.top = `${yPosition}px`;

  DOC.body.appendChild(el);

  if (iOS) {
    var range = DOC.createRange();
    range.selectNodeContents(el);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    el.setSelectionRange(0, 999999);
  } else {
    el.focus({
      preventScroll: false
    });
    el.select();
  }

  var OK = DOC.execCommand("copy");

  if (OK) {
    onOk(str);
  } else {
    onErr('Failed to copy');
  }

  el.remove(); // DOC.body.removeChild(el);
} // 
// const pasteBlob = async (src) => {
// 	try{
// 		// const imgURL = '/images/generic/file.png';
// 		const data = await fetch(src);
// 		const blob = await data.blob();
// 		await navigator.clipboard.write([
// 			new ClipboardItem({
// 				[blob.type]: blob
// 			})
// 		]);
// 		console.log('Image copied.');
// 	}catch(e){
// 		console.error(e.name, e.message);
// 	}
// }
// async function pasteBlob(){
//   try{
// 		if(!navigator.clipboard.read) return false;
// 		const clipboardItems = await navigator.clipboard.read();
// 		// console.log('clipboardItems: ', clipboardItems);
// 		let data = false;
//     for(const item of clipboardItems){
//       for(const type of item.types){
// 				const blob = await item.getType(type);
// 				// console.log('blob: ', blob);
// 				if(blob.type.startsWith("image/")){
// 					// const objURL = window.URL.createObjectURL(blob);
// 					// console.log('objURL: ', objURL);
// 					data = blob;
// 				}
//       }
// 		}
// 		return data;
//   }catch(e){
// 		console.error(e.name, e.message);
// 		return false;
//   }
// }


function pasteBlob(_x) {
  return _pasteBlob.apply(this, arguments);
} // CHECK Permisson paste
// clipboard-write - granted by default


function _pasteBlob() {
  _pasteBlob = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
    var items, lng, i;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (e.clipboardData) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            items = e.clipboardData.items;

            if (items) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return");

          case 6:
            lng = items.length;
            i = 0;

          case 8:
            if (!(i < lng)) {
              _context.next = 15;
              break;
            }

            if (!(items[i].type.indexOf("image/") === -1)) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("continue", 12);

          case 11:
            return _context.abrupt("return", items[i].getAsFile());

          case 12:
            i++;
            _context.next = 8;
            break;

          case 15:
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log('Error pasteBlob e: ', _context.t0);
            return _context.abrupt("return", false);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));
  return _pasteBlob.apply(this, arguments);
}

function permissions() {
  return _permissions.apply(this, arguments);
}

function _permissions() {
  _permissions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var query;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return navigator.permissions.query({
              name: "clipboard-read",
              allowWithoutGesture: false
            });

          case 3:
            query = _context2.sent;
            // Will be 'granted', 'denied' or 'prompt':
            console.log(query.state); // Listen for changes to the permission state

            query.onchange = function () {
              console.log(query.state);
            };

            return _context2.abrupt("return", query);

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", false);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 9]]);
  }));
  return _permissions.apply(this, arguments);
}



/***/ })

}]);
//# sourceMappingURL=Base64EncoderPage.chunk.js.map