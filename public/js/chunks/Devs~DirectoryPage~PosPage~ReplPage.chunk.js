(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs~DirectoryPage~PosPage~ReplPage"],{

/***/ "./resources/js/src/components/react-confirm/components/Confirmation.js":
/*!******************************************************************************!*\
  !*** ./resources/js/src/components/react-confirm/components/Confirmation.js ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap_es_Modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap/es/Modal */ "./node_modules/reactstrap/es/Modal.js");
/* harmony import */ var reactstrap_es_ModalHeader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap/es/ModalHeader */ "./node_modules/reactstrap/es/ModalHeader.js");
/* harmony import */ var _q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _confirmable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../confirmable */ "./resources/js/src/components/react-confirm/confirmable.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

 // import PropTypes from 'prop-types';
// import Modal from 'react-bootstrap/Modal';
// import Button from 'react-bootstrap/Button';



 // import { confirmable } from 'react-confirm';



var Confirmation = function Confirmation(_ref) {
  var _ref$okLabel = _ref.okLabel,
      okLabel = _ref$okLabel === void 0 ? "OK" : _ref$okLabel,
      _ref$cancelLabel = _ref.cancelLabel,
      cancelLabel = _ref$cancelLabel === void 0 ? "CANCEL" : _ref$cancelLabel,
      title = _ref.title,
      text = _ref.text,
      show = _ref.show,
      proceed = _ref.proceed,
      _ref$esc = _ref.esc,
      esc = _ref$esc === void 0 ? false : _ref$esc,
      size = _ref.size,
      toastInfoText = _ref.toastInfoText,
      icon = _ref.icon,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "modal" : _ref$type,
      bodyClass = _ref.bodyClass,
      btnsWrapClass = _ref.btnsWrapClass,
      backdrop = _ref.backdrop,
      modalProps = _ref.modalProps;
  var isModal = type === "modal";
  var bodyCx = {
    className: Q.Cx(type + "-body", bodyClass)
  };

  var Btns = function Btns() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: Q.Cx(btnsWrapClass)
    }, cancelLabel && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      onClick: function onClick() {
        return proceed(false);
      },
      size: "sm",
      kind: "danger"
    }, cancelLabel), " ", okLabel && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      onClick: function onClick() {
        return proceed(true);
      },
      size: "sm"
    }, okLabel));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap_es_Modal__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, modalProps, {
    isOpen: show // toggle={() => proceed(false)} 
    ,
    backdrop: esc ? true : Q.isBool(backdrop) ? backdrop : "static",
    keyboard: esc // size={type === "toast" ? "sm" : size ? size : null} 
    ,
    size: size // !isModal ? "bg-transparent border-0" : ""
    ,
    contentClassName: Q.Cx({
      "bg-transparent border-0": !isModal
    }, modalProps.contentClassName)
  }), isModal && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap_es_ModalHeader__WEBPACK_IMPORTED_MODULE_2__["default"], {
    toggle: function toggle() {
      return proceed(false);
    }
  }, title), isModal ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", bodyCx, text) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "toast o-1 mb-0 mx-auto",
    role: "alert",
    "aria-live": "assertive",
    "aria-atomic": "true"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "toast-header"
  }, icon, title && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "d-flex align-items-center ml-auto ml-2-next"
  }, toastInfoText && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", null, toastInfoText), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "close mb-1",
    "aria-label": "Close",
    onClick: function onClick() {
      return proceed(false);
    }
  }, "\xD7"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", bodyCx, text, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Btns, null)))), isModal && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "modal-footer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Btns, null)));
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_confirmable__WEBPACK_IMPORTED_MODULE_4__["default"])(Confirmation)); // <div className="static-modal">
// </div>

/* Confirmation.propTypes = {
  okLabbel: PropTypes.string,
  cancelLabel: PropTypes.string,
  title: PropTypes.string,
  confirmation: PropTypes.string,
  show: PropTypes.bool,
  proceed: PropTypes.func,     // called when ok button is clicked.
  enableEscape: PropTypes.bool,
}; */

/***/ }),

/***/ "./resources/js/src/components/react-confirm/confirmable.js":
/*!******************************************************************!*\
  !*** ./resources/js/src/components/react-confirm/confirmable.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// import React from 'react';
var confirmable = function confirmable(El) {
  return /*#__PURE__*/function (_React$Component) {
    _inherits(_class, _React$Component);

    var _super = _createSuper(_class);

    function _class(props) {
      var _this;

      _classCallCheck(this, _class);

      _this = _super.call(this, props);
      _this.state = {
        show: true
      };
      Q.bindFuncs.call(_assertThisInitialized(_this), ['dismiss', 'cancel', 'proceed']);
      return _this;
    }

    _createClass(_class, [{
      key: "dismiss",
      value: function dismiss() {
        var _this2 = this;

        this.setState({
          show: false
        }, function () {
          return _this2.props.dispose();
        });
      }
    }, {
      key: "cancel",
      value: function cancel(v) {
        var _this3 = this;

        this.setState({
          show: false
        }, function () {
          return _this3.props.reject(v);
        });
      }
    }, {
      key: "proceed",
      value: function proceed(v) {
        var _this4 = this;

        this.setState({
          show: false
        }, function () {
          return _this4.props.resolve(v);
        });
      }
    }, {
      key: "render",
      value: function render() {
        return /*#__PURE__*/React.createElement(El, _extends({
          proceed: this.proceed,
          cancel: this.cancel,
          dismiss: this.dismiss,
          show: this.state.show
        }, this.props));
      }
    }]);

    return _class;
  }(React.Component);
}; // <Component proceed={::this.proceed} cancel={::this.cancel} dismiss={::this.dismiss} show={this.state.show} {...this.props}/>


/* harmony default export */ __webpack_exports__["default"] = (confirmable);

/***/ }),

/***/ "./resources/js/src/components/react-confirm/createConfirmation.js":
/*!*************************************************************************!*\
  !*** ./resources/js/src/components/react-confirm/createConfirmation.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// import React from 'react';
// import ReactDOM from 'react-dom';
var createConfirmation = function createConfirmation(Component) {
  var unmountDelay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 950;
  var mountingNode = arguments.length > 2 ? arguments[2] : undefined;
  // 1000
  return function (props) {
    var body = document.body;
    var wrap = (mountingNode || body).appendChild(Q.makeEl("div")); // document.createElement('div')

    var promise = new Promise(function (resolve, reject) {
      try {
        ReactDOM.render( /*#__PURE__*/React.createElement(Component, _extends({
          reject: reject,
          resolve: resolve,
          dispose: dispose
        }, props)), wrap);
      } catch (e) {
        console.error(e);
        throw e;
      }
    });

    function dispose() {
      setTimeout(function () {
        ReactDOM.unmountComponentAtNode(wrap);
        setTimeout(function () {
          if (body.contains(wrap)) {
            body.removeChild(wrap);
          }
        });
      }, unmountDelay);
    }

    return promise.then(function (result) {
      dispose();
      return result;
    }, function (result) {
      dispose();
      return Promise.reject(result);
    });
  };
};

/* harmony default export */ __webpack_exports__["default"] = (createConfirmation);

/***/ }),

/***/ "./resources/js/src/components/react-confirm/util/confirm.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/components/react-confirm/util/confirm.js ***!
  \*******************************************************************/
/*! exports provided: confirm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirm", function() { return confirm; });
/* harmony import */ var _components_Confirmation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Confirmation */ "./resources/js/src/components/react-confirm/components/Confirmation.js");
/* harmony import */ var _createConfirmation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../createConfirmation */ "./resources/js/src/components/react-confirm/createConfirmation.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import ComplexConfirmation from '../components/ComplexConfirmation';
// import { createConfirmation } from 'react-confirm';


var defaultConfirmation = Object(_createConfirmation__WEBPACK_IMPORTED_MODULE_1__["default"])(_components_Confirmation__WEBPACK_IMPORTED_MODULE_0__["default"]);
function confirm(text) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  // confirmation, options = {}
  return defaultConfirmation(_objectSpread({
    text: text
  }, options));
} // export const confirmComplex = createConfirmation(ComplexConfirmation);

/***/ })

}]);
//# sourceMappingURL=Devs~DirectoryPage~PosPage~ReplPage.chunk.js.map