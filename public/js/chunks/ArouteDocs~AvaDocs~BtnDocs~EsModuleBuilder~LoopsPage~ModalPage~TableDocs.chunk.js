(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ArouteDocs~AvaDocs~BtnDocs~EsModuleBuilder~LoopsPage~ModalPage~TableDocs"],{

/***/ "./resources/js/src/components/live-code/Compiler.js":
/*!***********************************************************!*\
  !*** ./resources/js/src/components/live-code/Compiler.js ***!
  \***********************************************************/
/*! exports provided: evalCode, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "evalCode", function() { return evalCode; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

 // 
// import presetReact from '@babel/preset-react';// OPTION

var errorBoundary = function errorBoundary(El, errorCb) {
  var ErrorBoundary = /*#__PURE__*/function (_React$Component) {
    _inherits(ErrorBoundary, _React$Component);

    var _super = _createSuper(ErrorBoundary);

    function ErrorBoundary() {
      _classCallCheck(this, ErrorBoundary);

      return _super.apply(this, arguments);
    }

    _createClass(ErrorBoundary, [{
      key: "componentDidCatch",
      value: function componentDidCatch(error) {
        console.log('ErrorBoundary error: ', error);
        errorCb(error);
      }
    }, {
      key: "render",
      value: function render() {
        if (typeof El === 'undefined') return null;
        return typeof El === 'function' ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(El, null) : El;
      }
    }]);

    return ErrorBoundary;
  }(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

  return ErrorBoundary;
}; // const RenderCode = ({code, scope = {}}) => {


var evalCode = function evalCode(babel, code) {
  var scope = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var presets = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
  // if(window.Babel){ // window.buble && code && code.length > 0
  // const parseCode = buble.transform(code.trim());
  // const parseCode = window.Babel.transform(code, {
  var parseCode = babel.transform(code, {
    // ast: true,
    // code: false,
    // retainLines: true,
    // compact: true, // OPTION ISSUE: CAN'T REPLACE function _extends()
    minified: true,
    // OK
    inputSourceMap: false,
    sourceMaps: false,
    // TS preset needs this and it doesn't seem to matter when TS preset is not used, so let's keep it here?
    filename: 'file.tsx',
    comments: false,
    presets: ['es2017', 'react', 'typescript', 'flow'].concat(_toConsumableArray(presets)),
    // ,'stage-2' | 'env',
    // presets: presets ? [presetReact, ...presets] : [presetReact],
    plugins: [['proposal-class-properties'] // , { "loose": true }
    // 'transform-modules-amd',
    // 'transform-modules-commonjs',
    // 'transform-modules-umd',
    ] // modules:['amd','umd','cjs']

  });
  var resCode = parseCode ? parseCode.code : '';
  var scopeKeys = Object.keys(scope);
  var scopeVals = Object.values(scope); // eslint-disable-next-line

  var res = _construct(Function, ['React'].concat(_toConsumableArray(scopeKeys), ["return ".concat(resCode)])); // eslint-disable-next-line


  return res.apply(void 0, [react__WEBPACK_IMPORTED_MODULE_0___default.a].concat(_toConsumableArray(scopeVals))); // }
  // return null;// OPTION
};

var generateEl = function generateEl(babel, code, scope, presets, errorCb) {
  return errorBoundary(evalCode(babel, code, scope, presets), errorCb);
};

var parseCode = function parseCode(babel, code, scope, presets, setOutput, setError) {
  try {
    var component = generateEl(babel, code, scope, presets, function (err) {
      // console.log('%ctry component err: ', LOG_DEV, err);
      setError(err.toString());
    });
    setOutput({
      component: component
    });
    setError(null);
  } catch (err) {
    // console.log('%ccatch err: ', LOG_DEV, err);
    // setOutput({ component: code });// Q_CUSTOM (DEV)
    setError(err.toString());
  }
}; // const transformCode = async (babel, code, scope, presets) => { // , setOutput, setError
// try{
// const component = await generateEl(babel, code, scope, presets, (err) => {
// return err.toString();
// });
// return { component };
// }
// catch(err){
// return err.toString();
// }
// };
// Output, Error
// code, Placeholder, minHeight,


var Compiler = function Compiler(_ref) {
  var babel = _ref.babel,
      code = _ref.code,
      scope = _ref.scope,
      presets = _ref.presets,
      setError = _ref.setError,
      preview = _ref.preview,
      previewClass = _ref.previewClass;

  var _React$useState = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState({
    component: null
  }),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      output = _React$useState2[0],
      setOutput = _React$useState2[1]; // const [error, setError] = React.useState(null);// { where: '', msg: null }


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (babel) parseCode(babel, code, scope, presets, setOutput, setError);
  }, [babel, code, scope, presets, setError]);
  var El = output.component;
  var Preview = preview;
  return El ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(El, null) : Preview ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Preview, {
    className: previewClass
  }) : null;
};

 // transformCode

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["memo"])(Compiler, function (prevProps, nextProps) {
  return prevProps.code === nextProps.code;
})); // export default Compiler;
// <React.Fragment></React.Fragment>

/***/ })

}]);
//# sourceMappingURL=ArouteDocs~AvaDocs~BtnDocs~EsModuleBuilder~LoopsPage~ModalPage~TableDocs.chunk.js.map