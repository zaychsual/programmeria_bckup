(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["LoopsPage"],{

/***/ "./resources/js/src/components/q-ui-react/Loops.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Loops.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

/** Loops React Component with render props & auto key 
@usage: 
	<Loops 
		data={chartSetting.xAxis}
	>
		{(v, i) => (
			<li>{v.xaxisname} <Btn kind="danger" size="xs" onClick={() => this.onChartRemove('xAxis', i, v)}>X</Btn></li>
		)}
	</Loops> */

var Loops = function Loops(_ref) {
  var _ref$data = _ref.data,
      data = _ref$data === void 0 ? [] : _ref$data,
      children = _ref.children;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.toArray(data.map(function (v, i) {
    return children && children(v, i);
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Loops); // function uniq(l = 3){
// 	let a = new Uint32Array(l), //  Int8Array | Uint8Array | Int16Array | Uint16Array | Int32Array | Uint32Array
// 			b = window.crypto.getRandomValues(a);
// 	return b.join("-");
// }
// const Loops = ({data = [], children}) => {
// 	return React.Children.toArray(
// 		data.map((v, i) => children && children(v, i))
// 	);
// }
// export default Loops;

/***/ }),

/***/ "./resources/js/src/pages/admin/docs/q-ui-react/LoopsPage.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/pages/admin/docs/q-ui-react/LoopsPage.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LoopsPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DocsPage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../DocsPage */ "./resources/js/src/pages/admin/docs/DocsPage.js");
/* harmony import */ var _components_q_ui_react_Loops__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../components/q-ui-react/Loops */ "./resources/js/src/components/q-ui-react/Loops.js");
 // , { useState, useEffect, Fragment }

 // import Flex from '../../../../components/q-ui-react/Flex';


function LoopsPage() {
  // const [data, setData] = useState();
  // useEffect(() => {
  // 	console.log('%cuseEffect in LoopsPage','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocsPage__WEBPACK_IMPORTED_MODULE_1__["default"], {
    api: "/storage/json/docs/components/q-ui-react/Loops.json",
    scope: {
      useState: react__WEBPACK_IMPORTED_MODULE_0__["useState"],
      Loops: _components_q_ui_react_Loops__WEBPACK_IMPORTED_MODULE_2__["default"]
    }
  });
}

/***/ })

}]);
//# sourceMappingURL=LoopsPage.chunk.js.map