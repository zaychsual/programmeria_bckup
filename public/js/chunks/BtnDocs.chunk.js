(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BtnDocs"],{

/***/ "./resources/js/src/pages/admin/docs/q-ui-react/BtnDocs.js":
/*!*****************************************************************!*\
  !*** ./resources/js/src/pages/admin/docs/q-ui-react/BtnDocs.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Btn; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DocsPage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../DocsPage */ "./resources/js/src/pages/admin/docs/DocsPage.js");
 // , { useState, useEffect }


function Btn() {
  // const [data, setData] = useState();
  // useEffect(() => {
  // console.log('%cuseEffect in Btn','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocsPage__WEBPACK_IMPORTED_MODULE_1__["default"], {
    api: "/storage/json/docs/components/q-ui-react/Btn.json"
  });
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=BtnDocs.chunk.js.map