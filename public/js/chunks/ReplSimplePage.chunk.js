(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ReplSimplePage"],{

/***/ "./resources/js/src/apps/repl/ReplSimple.js":
/*!**************************************************!*\
  !*** ./resources/js/src/apps/repl/ReplSimple.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ReplSimple; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var _components_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/q-ui-react/SpinLoader */ "./resources/js/src/components/q-ui-react/SpinLoader.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/q-ui-react/ModalQ */ "./resources/js/src/components/q-ui-react/ModalQ.js");
/* harmony import */ var _components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/devs/react-split-pane-2/SplitPane */ "./resources/js/src/components/devs/react-split-pane-2/SplitPane.js");
/* harmony import */ var _components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/devs/react-split-pane-2/Pane */ "./resources/js/src/components/devs/react-split-pane-2/Pane.js");
/* harmony import */ var _parts_NavIde__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./parts/NavIde */ "./resources/js/src/apps/repl/parts/NavIde.js");
/* harmony import */ var _components_monaco_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/monaco-react */ "./resources/js/src/components/monaco-react/index.js");
/* harmony import */ var _components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/monaco-react/ide/onDidMount */ "./resources/js/src/components/monaco-react/ide/onDidMount.js");
/* harmony import */ var _data_monaco__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../data/monaco */ "./resources/js/src/data/monaco.js");
/* harmony import */ var _components_browser_Browser__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/browser/Browser */ "./resources/js/src/components/browser/Browser.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



 // Loader




/** NOTES: MOVE TO q-ui-react */
// import Switch from '../../ui-frameworks/q-react-bootstrap/Switch';


 // parts:

 // , monaco

 // @monaco-editor/react | {ControlledEditor} | Editor | DiffEditor | IdeCtrl


 // ../../config/monaco | , setDefineWindow

 // const calcPxWithPercent = (calc, persen = 50) => Math.round(persen * calc / 100);
// const DUMMY_TABS_IDE = [
// {name:"app.js"},{name:"index.js"},{name:"config.js"},{name:"data.js"},{name:"components.js"},
// {name:"utils.js"},{name:"default.js"},{name:"settings.js"},{name:"jquery.js"},{name:"custom.js"},
// {name:"react.js"},{name:"vue.js"},{name:"angular.js"},{name:"lodash.js"},{name:"bootstrap.js"},
// ];
// const ACCEPT_EXT = [...MONACO_LANGS.map(v => "."+v.ex), '.sass','.jsx','.tsx'];
// const ACCEPT_MIME = [...(new Set(MONACO_LANGS.map(v => v.type)))]; // Array.from(new Set(MONACO_LANGS.map(v => v.type)))
// const INIT_BABEL_PRESETS = ['es2015','es2015-loose','es2016','es2017','stage-0','stage-1','stage-2','stage-3','react','flow','typescript'];

var MONACOLANGS = _toConsumableArray(new Set(_data_monaco__WEBPACK_IMPORTED_MODULE_11__["MONACO_LANGS"].map(function (v) {
  return v.name;
})));

var ReplSimple = /*#__PURE__*/function (_React$Component) {
  _inherits(ReplSimple, _React$Component);

  var _super = _createSuper(ReplSimple);

  function ReplSimple(props) {
    var _this;

    _classCallCheck(this, ReplSimple);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onDidChangeEditor", function (v, editor) {
      // console.log('editor: ',editor);
      if (!_this.state.editorReady) {
        _this.setState({
          editorReady: true
        }); // this.ideMain.current = editor;// v | editor


        Object(_components_monaco_react_ide_onDidMount__WEBPACK_IMPORTED_MODULE_10__["default"])(v, editor); // console.log('monaco: ', window.monaco);

        /* window.monaco.languages.registerCompletionItemProvider('javascript', {
        	provideCompletionItems: (model, position) => {
        		// find out if we are completing a property in the 'dependencies' object.
        		// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
        		// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
        		// if(!match){
        			// return { suggestions: [] };
        		// }
        		
        		// let word = model.getWordUntilPosition(position);
        		// let range = {
        			// startLineNumber: position.lineNumber,
        			// endLineNumber: position.lineNumber,
        			// startColumn: word.startColumn,
        			// endColumn: word.endColumn
        		// };
        		return {
        			suggestions: setDefineWindow() // range
        		};
        	}
        }); */
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onModal", function () {
      return _this.setState(function (s) {
        return {
          modal: !s.modal
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleConsole", function () {
      _this.setState(function (s) {
        // (state, props)
        if (s.openConsole) {
          var pane = Q.domQ('.paneConsole');
          var h = pane.style.height;
          var def = 'calc(25% - 0.25px)';

          if (h !== def) {
            // console.log('CHANGE height');
            pane.style.height = def;
          }
        }

        return {
          openConsole: !s.openConsole
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onResetSize", function (e) {
      var pl = e.target.previousElementSibling; // .clientWidth

      var w = "calc(50% - 0.5px)"; // console.log('onDoubleClick');

      if (pl.style.width !== w) pl.style.width = w;
    });

    _this.state = {
      // presets: props.presets,
      editorReady: false,
      theme: props.theme,
      openConsole: false,
      modal: false,
      exCssVal: ""
    };
    return _this;
  } // componentDidMount(){
  // 	console.log('%ccomponentDidMount in Repl','color:yellow;');
  // 	// const { presets } = this.props;// const {babelPreset} = this.props; // state
  // 	// if(presets.includes('react')){
  // 	// 	Q.getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js","data-js":"Babel"})
  // 	// 	.then(v => {
  // 	// 		console.log(v);
  // 	// 	}).catch(e => {
  // 	// 		console.log(e);
  // 	// 	});
  // 	// }
  // }


  _createClass(ReplSimple, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          type = _this$props.type,
          className = _this$props.className,
          frameProps = _this$props.frameProps,
          frameRefNative = _this$props.frameRefNative,
          addStyleParent = _this$props.addStyleParent,
          onLoadIframe = _this$props.onLoadIframe,
          html = _this$props.html,
          stylesheet = _this$props.stylesheet,
          script = _this$props.script,
          autoRun = _this$props.autoRun,
          onClickRun = _this$props.onClickRun,
          tabs = _this$props.tabs,
          tabActive = _this$props.tabActive,
          onCloseTab = _this$props.onCloseTab,
          onClickTab = _this$props.onClickTab,
          lang = _this$props.lang,
          code = _this$props.code,
          onChangeCode = _this$props.onChangeCode,
          onChangeLang = _this$props.onChangeLang,
          onChangeAutoRun = _this$props.onChangeAutoRun; // presets, 

      var _this$state = this.state,
          theme = _this$state.theme,
          editorReady = _this$state.editorReady,
          openConsole = _this$state.openConsole,
          modal = _this$state.modal,
          exCssVal = _this$state.exCssVal;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__["default"], {
        dir: "row",
        className: Q.Cx("repl", className)
      }, !editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_SpinLoader__WEBPACK_IMPORTED_MODULE_2__["default"], {
        className: "position-absolute bg-" + theme,
        bg: "/icon/android-icon-36x36.png"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_6__["default"] // className={"bg-ide-" + theme} // style={{}} 
      , {
        onDoubleClick: this.onResetSize
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_7__["default"], {
        initialSize: "50%",
        minSize: "9%",
        maxSize: "90%",
        className: "ovhide bg-ide-" + theme
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "position-absolute position-full w-100 h-100"
      }, editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_parts_NavIde__WEBPACK_IMPORTED_MODULE_8__["NavIde"], {
        theme: theme // className=" px-3" 
        ,
        tabs: tabs // [] | DUMMY_TABS_IDE 
        ,
        active: tabActive,
        btn: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
          kind: theme,
          className: "qi qi-play" // onClick={() => this.onCompile(mode, code)} 
          ,
          onClick: onClickRun,
          disabled: autoRun // autoRun || !online
          ,
          "aria-label": "Run Compiler"
        }),
        onClickTab: onClickTab,
        onCloseTab: onCloseTab,
        onOpenSet: function onOpenSet() {
          _this2.setState({
            modal: true
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_monaco_react__WEBPACK_IMPORTED_MODULE_9__["ControlledEditor"], {
        loading: false,
        height: "calc(100% - 31px)" // calc(100vh - 75px) 
        // className={Q.Cx({'d-none': tools !== 'Compiler'})} 
        ,
        editorDidMount: this.onDidChangeEditor,
        value: code,
        onChange: function onChange(e, code) {
          return onChangeCode(e, code);
        } // (e, code) => this.setState({code})
        ,
        language: lang // ideLang | 'javascript'
        ,
        theme: theme,
        options: _data_monaco__WEBPACK_IMPORTED_MODULE_11__["MONACO_OPTIONS"]
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_devs_react_split_pane_2_SplitPane__WEBPACK_IMPORTED_MODULE_6__["default"], {
        split: "horizontal",
        allowResize: openConsole
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_7__["default"], {
        className: "flex-column"
      }, editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_browser_Browser__WEBPACK_IMPORTED_MODULE_12__["default"] // historyList={true} 
      , {
        className: "position-absolute position-full",
        type: type // DEFAULT = "native" | "component"
        ,
        addStyleParent: addStyleParent,
        theme: theme,
        frameProps: frameProps,
        frameRefNative: frameRefNative,
        html: html,
        stylesheet: stylesheet,
        script: script,
        append: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"], {
          className: "input-group-append"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Toggle, {
          variant: theme,
          bsPrefix: "rounded-0 qi qi-ellipsis-v tip tipBR",
          "aria-label": "More"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Menu, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Item, {
          as: "button",
          type: "button",
          onClick: this.onToggleConsole
        }, openConsole ? "Close" : "Open", " console"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Item, {
          as: "button",
          type: "button"
        }, "Open in new window"))),
        onLoad: function onLoad(e) {
          return onLoadIframe(e, script);
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_devs_react_split_pane_2_Pane__WEBPACK_IMPORTED_MODULE_7__["default"], {
        initialSize: "25%" // size={openConsole ? "25%":"0"}
        ,
        minSize: "20px",
        maxSize: "90%",
        className: "flex-column paneConsole"
      }, openConsole && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__["default"], {
        dir: "column",
        className: Q.Cx("h-100 ovhide", {
          'bg-white': theme !== 'dark'
        }) // Q.Cx("h-100 ovhide", {'bg-white':tm === 'light'}) | "h-100 ovhide bg-" + tm
        ,
        style: {
          backgroundColor: theme === 'dark' ? '#242424' : null
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__["default"], {
        className: "flexno border-bottom bg-" + theme
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
        kind: theme,
        size: "xs",
        className: "border-0 rounded-0 qi qi-ban",
        "aria-label": "Clear console",
        onClick: function onClick() {
          // if(logs.length > 0){
          // setLogs([]);
          console.clear(); // }
        } // disabled={logs.length < 1} 

      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
        onClick: this.onToggleConsole // () => this.setState({openConsole: false})
        ,
        kind: theme,
        size: "xs",
        className: "border-0 rounded-0 ml-auto qi qi-close xx",
        "aria-label": "Close"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "text-white"
      }, "Console etc"))))), editorReady && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_5__["default"], {
        open: modal,
        toggle: this.onModal,
        scrollable: true,
        modalClassName: "modalReplSetting" // size={modalTitle === 'Settings' ? 'lg':'xl'} // "xl" 
        ,
        size: "lg" // position="modal-up" 
        ,
        headProps: {
          tag: "h6"
        },
        title: "Settings",
        bodyClass: "p-0",
        body: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_3__["default"], {
          className: "py-2"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "col"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
          className: "fset",
          disabled: false
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("legend", null, "Editor"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
          className: "d-block"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
          className: "qi qi-info q-mr tip tipTL chelp",
          "aria-label": "Info"
        }), " Theme", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
          className: "custom-select custom-select-sm mt-1 txt-cap",
          value: theme,
          onChange: function onChange(e) {
            return _this2.setState({
              theme: e.target.value
            });
          }
        }, _data_monaco__WEBPACK_IMPORTED_MODULE_11__["MONACO_THEMES"].map(function (v, i) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
            key: i,
            value: v
          }, v);
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
          className: "d-block"
        }, "Language ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "qi qi-info float-right tip tipTR",
          "aria-label": "Info"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
          className: "custom-select custom-select-sm mt-1 txt-cap",
          value: lang,
          onChange: onChangeLang
        }, MONACOLANGS.map(function (v, i) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
            key: i,
            value: v
          }, v);
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
          className: "custom-control custom-switch"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
          onChange: onChangeAutoRun,
          checked: autoRun,
          className: "custom-control-input",
          type: "checkbox"
        }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "custom-control-label"
        }, "Auto Run")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "col" // onSubmit={e => et.checkValidity()}

        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
          className: "fset",
          disabled: false
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("legend", null, "External Source"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
          className: "d-block"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
          className: "qi qi-info q-mr tip tipTL chelp",
          "aria-label": "Info"
        }), " CSS", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
          className: "form-control form-control-sm mt-1",
          onChange: function onChange(e) {
            return _this2.setState({
              exCssVal: e.target.value
            });
          }
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
          className: "d-block"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
          className: "qi qi-info q-mr tip tipTL chelp",
          "aria-label": "Info"
        }), " JavaScript", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
          className: "form-control form-control-sm mt-1"
        }))))),
        foot: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
          size: "sm",
          kind: "dark",
          onClick: this.onModal
        }, "Close"), ' ', /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
          size: "sm" // kind="primary"
          ,
          onClick: function onClick() {
            // this.onModal();
            if (exCssVal.length > 0) {}
          }
        }, "Save"))
      }));
    }
  }]);

  return ReplSimple;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);


ReplSimple.defaultProps = {
  // presets: [], // babelPreset
  type: "native",
  // component | native
  tabs: [],
  // Editor:
  theme: "dark",
  // dark | light
  lang: "javascript",
  // 
  code: "",
  autoRun: false,
  onLoadIframe: Q.noop,
  onChangeCode: Q.noop,
  // onChangeTheme: Q.noop, 
  onChangeLang: Q.noop // onChangeAutoRun: Q.noop, 

};
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/pages/admin/devs/ReplSimplePage.js":
/*!*************************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/ReplSimplePage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ReplSimplePage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _apps_repl_ReplSimple__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../apps/repl/ReplSimple */ "./resources/js/src/apps/repl/ReplSimple.js");
/* harmony import */ var _utils_string__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../utils/string */ "./resources/js/src/utils/string.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }



 // INIT TABS CODE:

var TABS = [// "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n  Launch demo modal\n</button>\n\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        Modal content\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n      </div>\n    </div>\n  </div>\n</div>"
{
  id: "HTML",
  lang: "html",
  name: "HTML",
  code: "<!doctype html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\t\t<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n\t\t<title>Programmeria</title>\n\t\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" crossorigin=\"anonymous\">\n\t</head>\n\t<body>\n\t\t<!-- Button trigger modal -->\n\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n\t\t\tLaunch demo modal\n\t\t</button>\n\n\t\t<!-- Modal -->\n\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n\t\t\t<div class=\"modal-dialog\">\n\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\tModal content\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<script src=\"https://unpkg.com/jquery@3.5.1/dist/jquery.min.js\" crossorigin=\"anonymous\"></script>\n\t\t<script src=\"https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js\" crossorigin=\"anonymous\"></script>\n\t</body>\n<html>"
}, //{id:"HTML", lang:"html", name:"HTML", code:"<!doctype html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\t\t<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n\t\t<title>Programmeria</title>\n\t\t\n\t</head>\n\t<body>\n\t\t<!-- Button trigger modal -->\n\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n\t\t\tLaunch demo modal\n\t\t</button>\n\n\t\t<!-- Modal -->\n\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n\t\t\t<div class=\"modal-dialog\">\n\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\tModal content\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</body>\n<html>"},
{
  id: "CSS",
  lang: "css",
  name: "CSS",
  code: "body{\n\tbackground-color: lightblue;\n}"
}, {
  id: "JS",
  lang: "javascript",
  name: "JS",
  code: "console.log('This JS man', 6+1);\n$('.btn').trigger('click');"
}];

var ReplSimplePage = /*#__PURE__*/function (_Component) {
  _inherits(ReplSimplePage, _Component);

  var _super = _createSuper(ReplSimplePage);

  function ReplSimplePage(props) {
    var _this;

    _classCallCheck(this, ReplSimplePage);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "setTab", function (v, i) {
      _this.setState({
        tabActive: i,
        lang: v.lang,
        code: v.code
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onTab", function (v, i, e) {
      var tabActive = _this.state.tabActive; // tabs, code, autoRun

      if (i === tabActive) return; // console.log('onClickTab v: ',v);

      var _this$state = _this.state,
          tabs = _this$state.tabs,
          code = _this$state.code;
      var activeTab = tabs[tabActive]; // console.log('onClickTab activeTab.code: ', activeTab.code);

      if (activeTab.code !== code) {
        _this.setState(function (s) {
          var tabs = s.tabs.map(function (t) {
            if (activeTab.id === t.id) {
              return _objectSpread(_objectSpread({}, t), {}, {
                code: code
              });
            }

            return t;
          });
          var htmlCode = s.autoRun ? _this.addScriptInline(tabs) : s.htmlCode;
          return {
            tabs: tabs,
            htmlCode: htmlCode
          };
        }, function () {
          _this.setTab(v, i); // NOT FIX: Add css code to tag style in iframe with click Reload Button
          // if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();

        });
      } else {
        _this.setTab(v, i);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onLoadIframe", function (e, script, tabs) {
      // , tag, code
      // let et = e.target;
      // let doc = et.contentDocument;
      setTimeout(function () {// console.log('onLoadIframe: ', tabs[0].code.length - 14);

        /* getScript({src:"/js/libs/plnkr-runtime.js"}, doc.body).then((v) => {
        	console.log("window.PlnkrRuntime: ", et.contentWindow.PlnkrRuntime);
        	getScript({innerHTML:`const files = {
        			"package.json": JSON.stringify({
        					dependencies: {
        						jquery: '3.5.1',
        						bootstrap: '4.5.0',
        					},
        			}),
        			"index.js": "console.log('OKEH: ', window)"
        		};
        				const host = {
        			getCanonicalPath(path) {
        				if(files[path]) return path;
        				if(files[path + '.js']) return path + '.js';
        				return Promise.reject(new Error('File not found '+ path));
        			},
        			getFileContents(canonicalPath){
        				return files[canonicalPath];
        			}
        		};
        		
        		(async () => {
        			const runtime = new PlnkrRuntime.Runtime({host}),
        						body = document.body;
        			body.classList.remove('isLoad','isErr');
        					await runtime.import('./index.js').then((v) => {
        				body.classList.add('isLoad');
        			}).catch(e => {
        				console.warn('ERROR Compile: ',e);
        				body.classList.add('isLoad','isErr');
        			});
        		})();				
        	`}, doc.body);
        })
        .catch(e => console.log('ERROR load plnkr-runtime.js: ', e)); */
        // getScript({tag:"style", innerHTML:tabs[1].code}, doc.head);// MUST CHECK IF ADD CSS CODE IN TABS

        /* getScript({src:"/js/libs/Qrequire.js"}, doc.body)
        .then((v) => {
        	// let el = makeEl(tag);
        	// el.innerHTML = code;// this.state.tabs[1].code;
        	// if(tag === "style") doc.head.appendChild(el);
        	// else doc.body.appendChild(el);
        	
        	getScript({tag:"style", innerHTML:tabs[1].code}, doc.head);// .then(v => console.log(v))
        	
        	console.log('Qrequirejs v: ', v);// et.contentWindow.Qrequirejs
        	if(et.contentWindow.Qrequirejs){
        		console.log('Qrequirejs isAvailable');
        	}
        	
        	getScript({innerHTML:tabs[2].code}, doc.body);
        })
        .catch(e => console.log('ERROR load Qrequire.js: ', e)); */
      }, 9);
    });

    _this.state = {
      tabs: [].concat(TABS),
      tabActive: 0,
      lang: "html",
      // HTML Mode
      code: TABS[0].code,
      // HTML Mode & Code
      htmlCode: "",
      autoRun: false // script: [
      // "https://unpkg.com/jquery@3.5.1/dist/jquery.min.js", 
      // "https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"
      // ]

    }; // this.iframe = React.createRef();

    return _this;
  }

  _createClass(ReplSimplePage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      // console.log('%ccomponentDidMount in ReplSimplePage','color:yellow;');
      // const {tabs} = this.state;
      this.setState(function (s) {
        return {
          htmlCode: _this2.addScriptInline(s.tabs)
        };
      });
    }
  }, {
    key: "addScriptInline",
    value: function addScriptInline(tabs) {
      // const {tabs} = this.state;// script
      // let blob = new Blob([tabs[1].code], {type: 'text/css'});
      // let objUrl = window.URL.createObjectURL(blob);
      // console.log('blob: ', blob);
      // console.log('objUrl: ', objUrl);
      // const addScripts = script.map(v => `<script src="${v}" crossorigin="anonymous"></script>`);
      // HTML
      var setHtml = tabs[0].code; // DEV OPTION: Insert history.js for manipulate iframe history API
      // setHtml = strInsertBefore(setHtml, "</head>", historyApi);
      // CSS

      if (tabs[1] && tabs[1].code.length) setHtml = Object(_utils_string__WEBPACK_IMPORTED_MODULE_3__["strInsertBefore"])(setHtml, "</head>", "<style>".concat(tabs[1].code, "</style>")); // Minify Html & Css

      setHtml = setHtml.replace(/\n|\t/gm, ''); // JS

      var jsIframe = "";
      if (tabs[2] && tabs[2].code.length) setHtml = Object(_utils_string__WEBPACK_IMPORTED_MODULE_3__["strInsertBefore"])(setHtml, "</body>", "".concat(jsIframe, "<script>").concat(tabs[2].code, "</script>")); // addScripts.join('\n\t') + '\n'
      // console.log('setHtml: ', setHtml.replace(/\n|\t/gm,''));

      return setHtml; // setHtml.replace(/\n|\t/gm,'');// setHtml
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      // html, css
      var _this$state2 = this.state,
          lang = _this$state2.lang,
          code = _this$state2.code,
          htmlCode = _this$state2.htmlCode,
          tabs = _this$state2.tabs,
          tabActive = _this$state2.tabActive,
          autoRun = _this$state2.autoRun; // Repl Native Mode (Html, Css, Js)

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "w-100 mh-full-navmain position-relative"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
        title: "Repl Simple"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_repl_ReplSimple__WEBPACK_IMPORTED_MODULE_2__["default"], {
        className: "w-100 h-100 position-absolute position-full" // theme="light" // dark | light
        // presets={['esm']} 
        // type="component" // DEFAULT = native | component 
        ,
        frameProps: {
          className: "repl-web-programming" // title: "Test title",

        } // frameRefNative={this.iframe} 
        ,
        addStyleParent: false // 
        ,
        html: htmlCode // tabs[0].code | html | lang === "html" ? code : "" | `<h1>OKEH</h1>`
        // stylesheet={[
        // "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
        // ]}

        /* script={[
        	// "https://code.jquery.com/jquery-3.5.1.min.js"
        	"https://unpkg.com/jquery@3.5.1/dist/jquery.min.js", 
        	"https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"
        ]}  */
        // onLoadIframe={(e, script) => {
        // 	let history = createBrowserHistory({
        // 		window: e.target.contentWindow // iframe.contentWindow
        // 	});
        // 	console.log('onLoadIframe e.target.contentWindow: ', e.target.contentWindow);
        // 	console.log('onLoadIframe history: ', history);
        // 	// this.onLoadIframe(e, script, tabs);
        // 	// this.onLoadIframe(e, 'style', tabs[1].code);// ADD CSS
        // 	// this.onLoadIframe(e, 'script', tabs[2].code);// ADD JS
        // }}
        ,
        autoRun: autoRun,
        lang: lang // "html" 
        ,
        code: code,
        onChangeCode: function onChangeCode(e, code) {
          _this3.setState({
            code: code
          }, function () {// const { autoRun } = this.state;
            // console.log('onChangeCode autoRun: ', autoRun);
            // if(autoRun){
            // 	const activeTab = tabs[tabActive];
            // 	// // NOT FIX: Add css code to tag style in iframe with click Reload Button
            // 	// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();
            // 	// this.addScriptInline();
            // 	if(activeTab.code !== code){
            // 		this.setState(s => {
            // 			const tabs = s.tabs.map(t => { 
            // 				if(activeTab.id === t.id){
            // 					return {...t, code};
            // 				}
            // 				return t;
            // 			});
            // 			const htmlCode = s.autoRun ? this.addScriptInline(tabs) : s.htmlCode;
            // 			return {tabs, htmlCode};
            // 		}, () => {
            // 			// this.setTab(v, i);
            // 			// NOT FIX: Add css code to tag style in iframe with click Reload Button
            // 			// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();
            // 		});
            // 	}
            // 	// else{
            // 	// 	this.setTab(v, i);
            // 	// }
            // }
          });
        },
        onChangeLang: function onChangeLang(e) {
          return _this3.setState({
            lang: e.target.value
          });
        },
        onChangeAutoRun: function onChangeAutoRun(e) {
          return _this3.setState({
            autoRun: e.target.checked
          });
        },
        onClickRun: function onClickRun(e) {
          if (!autoRun) {
            // NOT FIX:
            // this.setState(s => ({
            // htmlCode: this.addScriptInline(s.tabs)
            // }));
            _this3.setState(function (s) {
              var tabs = s.tabs.map(function (t) {
                if (s.tabs[tabActive].id === t.id) {
                  return _objectSpread(_objectSpread({}, t), {}, {
                    code: code
                  });
                }

                return t;
              }); // const htmlCode = autoRun ?  : s.htmlCode;

              return {
                tabs: tabs,
                htmlCode: _this3.addScriptInline(tabs)
              };
            });
          }
        },
        tabs: tabs,
        tabActive: tabActive,
        onClickTab: this.onTab // onCloseTab={(v, i, e) => {
        // console.log('onCloseTab v: ',v);
        // console.log('onCloseTab i: ',i);
        // }} 

      }));
    }
  }]);

  return ReplSimplePage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);
/*
<React.Fragment></React.Fragment>
*/




/***/ })

}]);
//# sourceMappingURL=ReplSimplePage.chunk.js.map