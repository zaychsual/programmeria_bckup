(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["MatterCSS"],{

/***/ "./resources/js/src/pages/admin/devs/MatterCSS.js":
/*!********************************************************!*\
  !*** ./resources/js/src/pages/admin/devs/MatterCSS.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MatterCSS; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
 // , { useState, useEffect, useRef, } 


 // DEVS Components:
// import BgImage from '../../../components/q-ui-react/BgImage';
// import Ava from '../../../components/q-ui-react/Ava';
// import SpinLoader from '../../../components/q-ui-react/SpinLoader';
// import InputGroup from '../../../components/q-ui-react/InputGroup';
// import ScrollTo from '../../../components/q-ui-react/ScrollTo';
// import PageLoader from '../../../components/PageLoader';
// import NumberField from '../../../components/devs/NumberField';
// END DEVS Components:

function MatterCSS() {
  // const [pageLoad, setPageLoad] = useState(false);
  // const [progress, setProgress] = useState(0);
  // const [numVal, setNumVal] = useState("");
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    console.log('%cuseEffect in MatterCSS', 'color:yellow;'); // Q.getScript({
    // 	tag:"link", 
    // 	rel:"stylesheet", 
    // 	href:"https://res.cloudinary.com/finnhvman/raw/upload/matter/matter-0.2.2.min.css"
    // }, "head")
    // 	.then(v => {
    // 		console.log(v);
    // 	}).catch(e => {
    // 		console.log(e);
    // 	});
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Matter CSS"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "Matter CSS - Pure CSS Material"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "pure-material-slider"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "range",
    min: 0,
    max: 100
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Slider")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
    className: "hr-h"
  }, "Progress- Circular"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("progress", {
    className: "pure-material-progress-circular cwait"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
    className: "hr-h mb-3"
  }, "Progress- Linear"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_2__["default"], {
    dir: "column",
    align: "baseline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("progress", {
    className: "pure-material-progress-linear",
    value: "50",
    max: "100"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("progress", {
    className: "pure-material-progress-linear mt-4"
  })));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=MatterCSS.chunk.js.map