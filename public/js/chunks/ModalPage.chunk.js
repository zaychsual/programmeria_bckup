(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ModalPage"],{

/***/ "./resources/js/src/pages/admin/docs/q-ui-react/ModalPage.js":
/*!*******************************************************************!*\
  !*** ./resources/js/src/pages/admin/docs/q-ui-react/ModalPage.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ModalPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DocsPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../DocsPage */ "./resources/js/src/pages/admin/docs/DocsPage.js");
 // , { useState, useEffect, Fragment }

 // , ModalBody, ModalFooter

 // import Flex from '../../../../components/q-ui-react/Flex';
// import Btn from '../../../../components/q-ui-react/Btn';

function ModalPage() {
  // const [modalTop, setModalTop] = useState(false);
  // useEffect(() => {
  // 	console.log('%cuseEffect in ModalPage','color:yellow;');
  // }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocsPage__WEBPACK_IMPORTED_MODULE_2__["default"], {
    api: "/storage/json/docs/components/q-ui-react/Modal.json",
    scope: {
      useState: react__WEBPACK_IMPORTED_MODULE_0__["useState"],
      Modal: reactstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"],
      ModalHeader: reactstrap__WEBPACK_IMPORTED_MODULE_1__["ModalHeader"]
    }
  });
}

/***/ })

}]);
//# sourceMappingURL=ModalPage.chunk.js.map