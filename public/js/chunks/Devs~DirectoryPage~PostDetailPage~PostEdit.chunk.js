(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs~DirectoryPage~PostDetailPage~PostEdit"],{

/***/ "./resources/js/src/components/q-ui-react/ContextMenu.js":
/*!***************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/ContextMenu.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_overlays_usePopper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-overlays/usePopper */ "./node_modules/react-overlays/esm/usePopper.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }


 // react-popper
// DEVS OPTION: for hide component when scroll
// import { useScroll } from 'ahooks';
// import debounce from 'lodash.debounce';
// import Btn from '../../components/q-ui-react/Btn';

var makeVirtualEl = function makeVirtualEl() {
  var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return {
    getBoundingClientRect: function getBoundingClientRect() {
      return {
        width: 0,
        height: 0,
        top: y,
        right: x,
        bottom: y,
        left: x
      };
    }
  };
};

function ContextMenu(_ref) {
  var children = _ref.children,
      component = _ref.component,
      popperConfig = _ref.popperConfig,
      className = _ref.className,
      _ref$esc = _ref.esc,
      esc = _ref$esc === void 0 ? true : _ref$esc,
      _ref$hideOnScroll = _ref.hideOnScroll,
      hideOnScroll = _ref$hideOnScroll === void 0 ? true : _ref$hideOnScroll,
      _ref$appendTo = _ref.appendTo,
      appendTo = _ref$appendTo === void 0 ? document.body : _ref$appendTo,
      _ref$onContextMenu = _ref.onContextMenu,
      onContextMenu = _ref$onContextMenu === void 0 ? Q.noop : _ref$onContextMenu,
      _ref$onEsc = _ref.onEsc,
      onEsc = _ref$onEsc === void 0 ? Q.noop : _ref$onEsc,
      _ref$onScrolls = _ref.onScrolls,
      onScrolls = _ref$onScrolls === void 0 ? Q.noop : _ref$onScrolls;
  // const scroll = useScroll(document, () => console.log('scroll: ', val));
  var child = react__WEBPACK_IMPORTED_MODULE_0__["Children"].only(children);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      active = _useState2[0],
      setActive = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      clonedEl = _useState4[0],
      setClonedEl = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(makeVirtualEl()),
      _useState6 = _slicedToArray(_useState5, 2),
      virtualEl = _useState6[0],
      setVirtualEl = _useState6[1];

  var _usePopper = Object(react_overlays_usePopper__WEBPACK_IMPORTED_MODULE_2__["default"])(virtualEl, clonedEl, popperConfig),
      styles = _usePopper.styles,
      attributes = _usePopper.attributes;

  var hide = function hide() {
    setActive(false);
    onContextMenu(false); // Q-CUSTOM: For ???
  };

  var onCtxMenu = function onCtxMenu(e) {
    if (e) e.preventDefault();
    var clientX = e.clientX,
        clientY = e.clientY;
    setVirtualEl(makeVirtualEl(clientX, clientY));
    setActive(true);
    onContextMenu(active, e); // Q-CUSTOM
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var onOutsideClick = function onOutsideClick(e) {
      if (clonedEl && !clonedEl.contains(e.target)) {
        hide();
        onContextMenu(active, e); // Q-CUSTOM
      }
    }; // Q-CUSTOM DEV OPTION: for hide component when scroll 
    // const onScrollDoc = debounce(() => {
    // if(active){
    // hide();
    // }
    // }, 50);


    var onScrollDoc = function onScrollDoc() {
      if (active) {
        hide();
        onScrolls();
      }
    };

    var onEscKey = function onEscKey(e) {
      // console.log(e);
      if (active && e.key === "Escape") {
        hide();
        onEsc(false);
      }
    };

    document.addEventListener('mousedown', onOutsideClick); // Q-CUSTOM DEV OPTION: for hide component when esc key

    if (esc && active) document.addEventListener('keydown', onEscKey); // Q-CUSTOM DEV OPTION: for hide component when scroll 

    if (hideOnScroll && active) document.addEventListener('scroll', onScrollDoc);
    return function () {
      document.removeEventListener('mousedown', onOutsideClick); // Q-CUSTOM DEV OPTION: for hide component when esc key

      if (esc) document.removeEventListener('keydown', onEscKey); // Q-CUSTOM DEV OPTION: for hide component when scroll

      if (hideOnScroll) document.removeEventListener('scroll', onScrollDoc);
    };
  }, [clonedEl, esc, hideOnScroll, active]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"])(child, _objectSpread(_objectSpread({}, child.props), {}, {
    onContextMenu: onCtxMenu
  })), active && /*#__PURE__*/react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.createPortal( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({
    ref: setClonedEl,
    style: _objectSpread({}, styles.popper)
  }, attributes.popper, {
    className: className
  }), component(hide, onCtxMenu, active)), appendTo // document.body
  ));
}
;

/***/ }),

/***/ "./resources/js/src/utils/clipboard/index.js":
/*!***************************************************!*\
  !*** ./resources/js/src/utils/clipboard/index.js ***!
  \***************************************************/
/*! exports provided: clipboardCopy, copyFn, pasteBlob, permissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clipboardCopy", function() { return clipboardCopy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyFn", function() { return copyFn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pasteBlob", function() { return pasteBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "permissions", function() { return permissions; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Copy to clipboard:
function clipboardCopy(target) {
  return new Promise(function (resolve, reject) {
    var clipboard = navigator.clipboard,
        txt;
    if (typeof target === 'string' && !target.tagName) txt = target;else if (target === null || target === void 0 ? void 0 : target.tagName) txt = target.textContent; // target.innerText
    else reject(new Error('Text or target DOM to copy is required.'));

    if (clipboard) {
      clipboard.writeText(txt).then(function () {
        return resolve(txt);
      })["catch"](function (e) {
        return reject(e);
      });
    } else {
      console.log('%cnavigator.clipboard NOT SUPPORT', 'color:yellow');
      copyFn(txt, {
        onOk: function onOk() {
          resolve(txt);
        },
        onErr: function onErr(e) {
          reject(e);
        }
      });
    }
  });
}

function copyFn(str, _ref) {
  var _ref$onOk = _ref.onOk,
      onOk = _ref$onOk === void 0 ? function () {} : _ref$onOk,
      _ref$onErr = _ref.onErr,
      onErr = _ref$onErr === void 0 ? function () {} : _ref$onErr;
  var DOC = document,
      el = DOC.createElement("textarea"),
      iOS = window.navigator.userAgent.match(/ipad|iphone/i);
  el.className = "sr-only sr-only-focusable";
  el.contentEditable = true; // needed for iOS >= 10

  el.readOnly = false; // needed for iOS >= 10

  el.value = str; // el.style.border = "0";
  // el.style.padding = "0";
  // el.style.margin = "0";
  // el.style.position = "absolute";
  // sets vertical scroll
  // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
  // el.style.top = `${yPosition}px`;

  DOC.body.appendChild(el);

  if (iOS) {
    var range = DOC.createRange();
    range.selectNodeContents(el);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    el.setSelectionRange(0, 999999);
  } else {
    el.focus({
      preventScroll: false
    });
    el.select();
  }

  var OK = DOC.execCommand("copy");

  if (OK) {
    onOk(str);
  } else {
    onErr('Failed to copy');
  }

  el.remove(); // DOC.body.removeChild(el);
} // 
// const pasteBlob = async (src) => {
// 	try{
// 		// const imgURL = '/images/generic/file.png';
// 		const data = await fetch(src);
// 		const blob = await data.blob();
// 		await navigator.clipboard.write([
// 			new ClipboardItem({
// 				[blob.type]: blob
// 			})
// 		]);
// 		console.log('Image copied.');
// 	}catch(e){
// 		console.error(e.name, e.message);
// 	}
// }
// async function pasteBlob(){
//   try{
// 		if(!navigator.clipboard.read) return false;
// 		const clipboardItems = await navigator.clipboard.read();
// 		// console.log('clipboardItems: ', clipboardItems);
// 		let data = false;
//     for(const item of clipboardItems){
//       for(const type of item.types){
// 				const blob = await item.getType(type);
// 				// console.log('blob: ', blob);
// 				if(blob.type.startsWith("image/")){
// 					// const objURL = window.URL.createObjectURL(blob);
// 					// console.log('objURL: ', objURL);
// 					data = blob;
// 				}
//       }
// 		}
// 		return data;
//   }catch(e){
// 		console.error(e.name, e.message);
// 		return false;
//   }
// }


function pasteBlob(_x) {
  return _pasteBlob.apply(this, arguments);
} // CHECK Permisson paste
// clipboard-write - granted by default


function _pasteBlob() {
  _pasteBlob = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
    var items, lng, i;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (e.clipboardData) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            items = e.clipboardData.items;

            if (items) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return");

          case 6:
            lng = items.length;
            i = 0;

          case 8:
            if (!(i < lng)) {
              _context.next = 15;
              break;
            }

            if (!(items[i].type.indexOf("image/") === -1)) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("continue", 12);

          case 11:
            return _context.abrupt("return", items[i].getAsFile());

          case 12:
            i++;
            _context.next = 8;
            break;

          case 15:
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log('Error pasteBlob e: ', _context.t0);
            return _context.abrupt("return", false);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));
  return _pasteBlob.apply(this, arguments);
}

function permissions() {
  return _permissions.apply(this, arguments);
}

function _permissions() {
  _permissions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var query;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return navigator.permissions.query({
              name: "clipboard-read",
              allowWithoutGesture: false
            });

          case 3:
            query = _context2.sent;
            // Will be 'granted', 'denied' or 'prompt':
            console.log(query.state); // Listen for changes to the permission state

            query.onchange = function () {
              console.log(query.state);
            };

            return _context2.abrupt("return", query);

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", false);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 9]]);
  }));
  return _permissions.apply(this, arguments);
}



/***/ })

}]);
//# sourceMappingURL=Devs~DirectoryPage~PostDetailPage~PostEdit.chunk.js.map