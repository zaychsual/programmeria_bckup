(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["About"],{

/***/ "./resources/js/src/pages/public/About.js":
/*!************************************************!*\
  !*** ./resources/js/src/pages/public/About.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return About; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
 // { useState, useEffect }
// import { useHistory } from 'react-router-dom';

 // import Btn from '../../components/q-ui-bootstrap/Btn';

function About() {
  // let history = useHistory();
  // const [data, setData] = useState();
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // bcApi.onmessage = (e) => {
    // }
    var onBc = function onBc(e) {
      console.log(e);

      if (e.data === "logout") {
        window.location.replace("/login"); // window.history.replaceState(null, null, "/login");
        // history.replace("/");
      } // if(e.data === "closeBC"){
      // 	setBcState(false);
      // 	bcApi.removeEventListener('message', onBc);
      // }
      // if(e.data === "openBC"){
      // 	setBcState(true);
      // 	bcApi.addEventListener('message', onBc);
      // }

    };

    bcApi.addEventListener('message', onBc);
    return function () {
      console.log('useEffect removeEventListener');
      bcApi.removeEventListener('message', onBc);
    };
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "jumbotron jumbotron-fluid text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "About"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "display-4"
  }, "About Programmeria"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lead"
  }, "Programmeria is web app"));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=About.chunk.js.map