(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs~PostEdit"],{

/***/ "./resources/js/src/utils/file/fileRead.js":
/*!*************************************************!*\
  !*** ./resources/js/src/utils/file/fileRead.js ***!
  \*************************************************/
/*! exports provided: isDir, fileRead, SUPPORT_DIR */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isDir", function() { return isDir; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fileRead", function() { return fileRead; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SUPPORT_DIR", function() { return SUPPORT_DIR; });
// import attrAccept from './attr-accept';
// window.Directory || !('webkitdirectory' in document.createElement('input') && 'webkitGetAsEntry' in DataTransferItem.prototype)
// 'webkitdirectory' in document.createElement('input') && 'webkitGetAsEntry' in DataTransferItem.prototype;
var SUPPORT_DIR = ('webkitGetAsEntry' in DataTransferItem.prototype); // const noop = () => {};
// == Check Directory (From bootstrap-fileinput) ==

function isDir(items) {
  // checkDir
  var i,
      item,
      len = items ? items.length : 0,
      folders = 0;

  if (len > 0 && items[0].webkitGetAsEntry()) {
    for (i = 0; i < len; i++) {
      item = items[i].webkitGetAsEntry();

      if (item && item.isDirectory) {
        folders++;
      }
    }
  }

  return folders;
} // https://joji.me/en-us/blog/processing-huge-files-using-filereader-readasarraybuffer-in-web-browser/
// let file = document.getElementById('file').files[0];
// let fr = new FileReader();
// let CHUNK_SIZE = 10 * 1024;
// let startTime, endTime;
// let reverse = false;
// fr.onload = function () {
// let buffer = new Uint8Array(fr.result);
// let timeReg = /\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}/;
// for (let i = reverse ? buffer.length - 1 : 0; reverse ? i > -1 : i < buffer.length; reverse ? i-- : i++) {
// if (buffer[i] === 10) {
// let snippet = new TextDecoder('utf-8').decode(buffer.slice(i + 1, i + 20));
// if (timeReg.exec(snippet)) {
// if (!reverse) {
// startTime = snippet;
// reverse = true;
// seek();
// } else {
// endTime = snippet;
// alert(`Log time range: ${startTime} ~ ${endTime}`);
// }
// break;
// }
// }
// }
// }
// seek();
// function seek() {
// let start = reverse ? file.size - CHUNK_SIZE : 0;
// let end = reverse ? file.size : CHUNK_SIZE;
// let slice = file.slice(start, end);
// fr.readAsArrayBuffer(slice);
// }
// == FileReader API ==


function fileRead(file) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$readAs = _ref.readAs,
      readAs = _ref$readAs === void 0 ? "DataURL" : _ref$readAs,
      onProgress = _ref.onProgress,
      onLoad = _ref.onLoad;

  if (file) {
    var reader = new FileReader(); // let CHUNK_SIZE = 10 * 1024;
    // let startTime, endTime;
    // let reverse = false;
    // 1.161.111.475
    // FileReader.EMPTY | FileReader.DONE | FileReader.LOADING

    return new Promise(function (resolve, reject) {
      /* reader.onerror = (e) => {
      	reader.abort();
      	// onError(e, reader.readyState);
      	reject(e);// new DOMException("Problem parsing file.")
      }; */
      reader.addEventListener('error', function (e) {
        reader.abort();
        reject(e);
      }); // reader.onloadstart = (e) => { // event.type
      // onLoadStart(e, reader.readyState);
      // };
      // reader.onprogress = (e) => { // event.type
      // onProgress(e, reader.readyState);
      // };

      if (onProgress) {
        reader.addEventListener('progress', onProgress);
      }

      reader.addEventListener('load', function (e) {
        if (onLoad) onLoad(reader, e);
        resolve({
          file: file,
          result: reader.result
        });
      });
      /* reader.onload = () => {
      	// console.log('onload: ', reader.readyState);
      	// let buffer = new Uint8Array(reader.result);
      	// let timeReg = /\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}/;
      	// let bufLength = buffer.length;
      	// for (let i = reverse ? bufLength - 1 : 0; reverse ? i > -1 : i < bufLength; reverse ? i-- : i++) {
      		// if (buffer[i] === 10) {
      			// let snippet = new TextDecoder('utf-8').decode(buffer.slice(i + 1, i + 20));
      			// if (timeReg.exec(snippet)) {
      				// if (!reverse) {
      					// startTime = snippet;
      					// reverse = true;
      					// seek();
      				// } else {
      					// endTime = snippet;
      					// console.log(`Log time range: ${startTime} ~ ${endTime}`);
      				// }
      				// break;
      			// }
      		// }
      	// }
      	
      	resolve({
      		file: file,
      		result: reader.result
      	});// ...data
      }; */
      // reader.onloadend = (e) => { // event.type
      // onLoadedEnd(e, reader.readyState);
      // };

      /* reader.onabort = (e) => { // event.type
      	// onAbort(e, reader.readyState);
      	console.log(reader.error.message);
      	reject(e, reader.error);
      }; */

      reader.addEventListener('abort', function () {
        reject(e, reader.error);
      }); // seek();
      // function seek(){
      // let start = reverse ? file.size - CHUNK_SIZE : 0;
      // let end = reverse ? file.size : CHUNK_SIZE;
      // let slice = file.slice(start, end);
      // reader["readAs" + readAs](slice);// fr.readAsArrayBuffer(slice);
      // }

      reader["readAs" + readAs](file);
    });
  }
}
/* function fileRead(e, {
	accept = '.txt', //
  readAs = 'arrayBuffer',
  // onEmpty = noop,
  onValidate = noop,
  onLoadStart = noop,
  onProgress = noop,
  onLoad = noop,
  onLoadedEnd = noop,
  onAbort = noop,
  onError = noop
} = {}){
  if(!window.FileReader) return;

	let input = e.dataTransfer || e.target,
      files = input ? input.files : e;

  if(!files.length){
    // onEmpty(e);
    return;
  }else{
    for(let i = 0; i < files.length; i++){
      let f = files[i];

			// console.log(f);
			// CHECK file type to Validate
			let isAccept = attrAccept({
			    name: f.name,
			    type: f.type
			}, accept);// 'application/json, video/*'

			if(!isAccept){
				onValidate(f.name, accept, isAccept);
				return;
			}

      let reader = new FileReader();

      reader.onloadstart = ((file) => {
         return (ev) => onLoadStart(e, ev, file);// if(onLoadStart) return (ev) => onLoadStart(e, ev, file);
      })(f);

      reader.onprogress = ((file) => {
        return (ev) => onProgress(e, ev, file);
      })(f);

      reader.onload = ((file) => {
        return (ev) => onLoad(e, ev, file, files);
      })(f);

      reader.onloadend = ((file) => {
        return (ev) => onLoadedEnd(e, ev, file);
      })(f);

      reader.onabort = ((file) => {
        return (ev) => onAbort(e, ev, file);
      })(f);

      reader.onerror = ((file) => {
        return (ev) => onError(e, ev, file);
      })(f);

      // Choose type to Read
      switch(readAs){
        case 'arrayBuffer':
          reader.readAsArrayBuffer(f);
          break;
        case 'binaryString':
          reader.readAsBinaryString(f);
          break;
        case 'dataUrl':
          reader.readAsDataURL(f);
          break;
        case 'text':
          reader.readAsText(f);
          break;
        default:
          break;
      }
    }
  }
} */
// https://github.com/GoogleChrome/chrome-app-samples/blob/master/samples/filesystem-access/js/app.js,
// https://blog.shovonhasan.com/using-promises-with-filereader/
// function readAsText(file, ){
// const reader = new FileReader();
// return new Promise((resolve, reject) => {
// reader.onerror = () => {
// reader.abort();
// reject(new DOMException("Problem parsing file."));
// };
// reader.onload = () => {
// resolve(reader.result);
// };
// reader.readAsText(file);
// });
// }


 // readAsText, loopFiles, scanDir
// == DEV NEW ==
// function traverseDirectory(entry, cb){
// 	let reader = entry.createReader();
// 	// Resolved when the entire directory is traversed
// 	return new Promise((resolve_directory) => {
// 		let iteration_attempts = [];
// 		(function read_entries() {
// 			// According to the FileSystem API spec, readEntries() must be called until it calls the callback with an empty array.  Seriously??
// 			reader.readEntries((entries) => {
// 				if(!entries.length){
// 					resolve_directory(Promise.all(iteration_attempts));// Done iterating this particular directory
// 				}else{
// 					// Add a list of promises for each directory entry.  If the entry is itself a directory, then that promise won't resolve until it is fully traversed.
// 					iteration_attempts.push(Promise.all(entries.map((entry) => {
// 						if(entry.isFile){
// 							return entry;// DO SOMETHING WITH FILES
// 							// return cb(entry);
// 						}else{
// 							return traverseDirectory(entry);// DO SOMETHING WITH DIRECTORIES
// 							// traverseDirectory(entry);
// 						}
// 					})));
// 					read_entries();// Try calling readEntries() again for the same dir, according to spec
// 				}
// 			});// , this.errorHandler
// 		})();
// 	});
// }
// == Get Files in Directory (From upload component antd) ==
// function loopFiles(item, cb){
// const dirReader = item.createReader();
// let fileList = [];
// function sequence(){
// dirReader.readEntries((entries) => {
// const entryList = Array.prototype.slice.apply(entries);
// fileList = fileList.concat(entryList);
// const isFinish = !entryList.length;// Check if all the file has been viewed
// if(isFinish){
// cb(fileList);// callback
// }else{
// sequence();
// }
// });
// }
// sequence();
// }
// == Read Directory onDrop event ==
// files, callback, isAccepted

/* const scanDir = (files, callback, isAccepted) => {
  const _scanDir = (item, path) => {
    path = path || '';
    if(item.isFile){
      item.file((file) => {
        //if(isAccepted(file)){
          // https://github.com/ant-design/ant-design/issues/16426
          if(item.fullPath && !file.webkitRelativePath){
            Object.defineProperties(file,{
              webkitRelativePath: {
                writable: true,
              },
            });
            file.webkitRelativePath = item.fullPath.replace(/^\//, '');
            Object.defineProperties(file,{
              webkitRelativePath: {
                writable: false,
              },
            });
          }
          callback([file]);// callback
        //}
      });
    }else if(item.isDirectory){
      loopFiles(item, (entries) => {
        entries.forEach((entryItem) => {
          _scanDir(entryItem, `${path}${item.name}/`);
        });
      });
    }
  };
  for(let file of files){
    _scanDir(file.webkitGetAsEntry());
  }
}; */

/***/ })

}]);
//# sourceMappingURL=Devs~PostEdit.chunk.js.map