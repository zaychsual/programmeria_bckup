(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ManifestGeneratorPage"],{

/***/ "./resources/js/src/apps/ManifestGenerator.js":
/*!****************************************************!*\
  !*** ./resources/js/src/apps/ManifestGenerator.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ManifestGenerator; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var react_colorful__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-colorful */ "./node_modules/react-colorful/dist/index.module.js");
/* harmony import */ var image_blob_reduce__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! image-blob-reduce */ "./node_modules/image-blob-reduce/dist/image-blob-reduce.esm.mjs");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/q-ui-react/Textarea */ "./resources/js/src/components/q-ui-react/Textarea.js");
/* harmony import */ var _components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/q-ui-react/Img */ "./resources/js/src/components/q-ui-react/Img.js");
/* harmony import */ var _components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/q-ui-react/ModalQ */ "./resources/js/src/components/q-ui-react/ModalQ.js");
/* harmony import */ var _apps_image_editor_ImgEditor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../apps/image-editor/ImgEditor */ "./resources/js/src/apps/image-editor/ImgEditor.js");
/* harmony import */ var _utils_darkOrLight__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utils/darkOrLight */ "./resources/js/src/utils/darkOrLight.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // , { createRef }

 // import Nav from 'react-bootstrap/Nav';

 // RgbaStringColorPicker, RgbaColorPicker


 // import Flex from '../components/q-ui-react/Flex';




 // import { fileRead } from '../utils/file/fileRead';
// import { clipboardCopy } from '../utils/clipboard';

 // import ImgBlob from '../components/libraries/ImgBlob';
// const SIZES = [16, 32, 36, 48, 57, 60, 70, 72, 76, 96, 114, 120, 144, 150, 152, 180, 192, 310];

var SIZES = [{
  size: 16,
  ext: "ico"
}, {
  size: 16
}, {
  size: 32
}, {
  size: 36
}, {
  size: 48
}, {
  size: 57
}, {
  size: 60
}, {
  size: 70
}, {
  size: 72
}, {
  size: 76
}, {
  size: 96
}, {
  size: 114
}, {
  size: 120
}, {
  size: 144
}, {
  size: 150
}, {
  size: 152
}, {
  size: 180
}, {
  size: 192
}, {
  size: 310
}];

function setSrcDoc(_ref) {
  var title = _ref.title,
      theme = _ref.theme,
      body = _ref.body;
  return "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n<meta charset=\"utf-8\">\n<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no\">\n<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"default\">\n<meta name=\"mobile-web-app-capable\" content=\"yes\">\n<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n<meta name=\"msapplication-TileColor\" content=\"".concat(theme, "\">\n<meta name=\"theme-color\" content=\"").concat(theme, "\">\n<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n<title>").concat(title, "</title>\n<style>\nbody{margin:0;padding:1rem}\n.icon{max-width:100%;height:auto}\n</style>\n</head>\n<body>\n").concat(body, "\n</body></html>\n");
} // const initialValues = {
// 	name: "", 
// 	short_name: "", 
// 	description: "", 
// 	display: "", // standalone
// 	theme_color: "", 
// 	background_color: "", 
// 	start_url: "./", 
// 	scope: "/", 
// };


function makeManifestJson(imgArr, _ref2) {
  var name = _ref2.name,
      short_name = _ref2.short_name,
      description = _ref2.description,
      display = _ref2.display,
      theme_color = _ref2.theme_color,
      background_color = _ref2.background_color,
      start_url = _ref2.start_url,
      scope = _ref2.scope,
      zipFolder = _ref2.zipFolder,
      jsonTab = _ref2.jsonTab;
  return JSON.stringify({
    name: name,
    short_name: short_name,
    description: description,
    display: display,
    theme_color: theme_color,
    background_color: background_color,
    // gcm_sender_id: "122867383838", 
    start_url: start_url,
    scope: scope,
    icons: imgArr.map(function (v) {
      return {
        src: zipFolder + "/" + v.name,
        type: v.blob.type,
        sizes: v.size + "x" + v.size // density: "0.75"

      };
    }) // .filter(f => !f.ext)
    // related_applications: [
    // 	{
    // 		platform: "play",
    // 		id: "org.telegram.messenger",
    // 		url: "https://telegram.org/dl/android?ref=webmanifest"
    // 	}, {
    // 		platform: "itunes",
    // 		url: "https://telegram.org/dl/ios?ref=webmanifest"
    // 	}
    // ]

  }, null, jsonTab);
}

function makeBrowserConfigXml(_ref3) {
  var path = _ref3.path,
      imgSrc = _ref3.imgSrc,
      tileColor = _ref3.tileColor;
  return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<browserconfig>\n\t<msapplication>\n\t\t<tile>\n\t\t\t".concat(imgSrc.map(function (v) {
    return "<square".concat(v.size, "x").concat(v.size, "logo src=\"/").concat(path, "/").concat(v.name, "\"/>");
  }).join("\n\t\t\t"), "\n\t\t\t<TileColor>").concat(tileColor, "</TileColor>\n\t\t</tile>\n\t</msapplication>\n</browserconfig>");
}

function ManifestGenerator(_ref4) {
  var className = _ref4.className,
      prepend = _ref4.prepend;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      name = _useState2[0],
      setName = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      short_name = _useState4[0],
      setShortName = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      description = _useState6[0],
      setDescription = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("standalone"),
      _useState8 = _slicedToArray(_useState7, 2),
      display = _useState8[0],
      setDisplay = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("./"),
      _useState10 = _slicedToArray(_useState9, 2),
      start_url = _useState10[0],
      setStartUrl = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("/"),
      _useState12 = _slicedToArray(_useState11, 2),
      scope = _useState12[0],
      setScope = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("#ffffff"),
      _useState14 = _slicedToArray(_useState13, 2),
      theme_color = _useState14[0],
      setTheme = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("#ffffff"),
      _useState16 = _slicedToArray(_useState15, 2),
      background_color = _useState16[0],
      setBgColor = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState18 = _slicedToArray(_useState17, 2),
      themePick = _useState18[0],
      setThemePick = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      bgPick = _useState20[0],
      setBgPick = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("icons"),
      _useState22 = _slicedToArray(_useState21, 2),
      zipFolder = _useState22[0],
      setZipFolder = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(2),
      _useState24 = _slicedToArray(_useState23, 2),
      jsonTab = _useState24[0],
      setJsonTab = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState26 = _slicedToArray(_useState25, 2),
      fileData = _useState26[0],
      setFileData = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState28 = _slicedToArray(_useState27, 2),
      img = _useState28[0],
      setImg = _useState28[1]; // null
  // const [imgOri, setImgOri] = useState(null);


  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState30 = _slicedToArray(_useState29, 2),
      modal = _useState30[0],
      setModal = _useState30[1];

  var onToggleTheme = function onToggleTheme(see, key) {
    var docActive = document.activeElement;

    if (themePick && docActive.dataset.color === key) {
      setThemePick(true);
    } else {
      setThemePick(see);
    }
  };

  var onToggleBg = function onToggleBg(see, key) {
    var docActive = document.activeElement;

    if (bgPick && docActive.dataset.color === key) {
      setBgPick(true);
    } else {
      setBgPick(see);
    }
  };

  var isDark = function isDark(c) {
    return Object(_utils_darkOrLight__WEBPACK_IMPORTED_MODULE_9__["default"])(c.replace("#", ""));
  };

  var onZip = function onZip(zip) {
    var imgNoFavicon = img.filter(function (v) {
      return !v.ext;
    });
    var manifest = makeManifestJson(imgNoFavicon, {
      name: name,
      short_name: short_name,
      description: description,
      display: display,
      start_url: start_url,
      scope: scope,
      theme_color: theme_color,
      background_color: background_color,
      zipFolder: zipFolder,
      jsonTab: jsonTab
    });
    var browserConfig = makeBrowserConfigXml({
      path: zipFolder,
      imgSrc: imgNoFavicon.filter(function (v) {
        return [70, 150, 310].includes(v.size);
      }),
      tileColor: theme_color
    });
    var indexHtml = setSrcDoc({
      title: name,
      theme: theme_color,
      body: imgNoFavicon.map(function (v) {
        return "<img src=\"./".concat(v.name, "\" alt=\"").concat(v.name, "\" class=\"icon\"/>");
      }).join("\n")
    });
    zip.file(zipFolder + "/manifest.json", manifest);
    zip.file(zipFolder + "/browserconfig.xml", browserConfig);
    zip.file(zipFolder + "/index.html", indexHtml);
    img.forEach(function (v) {
      zip.file(zipFolder + "/" + v.name, v.blob); // "icons/" + v.name, v.blob
    });
    zip.generateAsync({
      type: "blob"
    }).then(function (blob) {
      console.log("zip: ", blob);
      var a = Q.makeEl('a');
      a.href = URL.createObjectURL(blob);
      a.rel = "noopener";
      a.download = zipFolder;
      setTimeout(function () {
        return a.click();
      }, 1);
      setTimeout(function () {
        URL.revokeObjectURL(a.href);
        a = null; // OPTION
      }, 4E4); // 40s
    });
  };

  var onSave = function onSave() {
    if (window.JSZip) {
      onZip(new JSZip());
    } else {
      Q.getScript({
        src: "/js/libs/jszip/jszip.min.js",
        "data-js": "JSZip"
      }).then(function () {
        if (window.JSZip) {
          onZip(new JSZip());
        } else {
          console.log("%cjszip failed", "color:yellow");
        }
      })["catch"](function (e) {
        return console.log(e);
      });
    }
  };

  var onSaveIcons = function onSaveIcons(val) {
    console.log('ImgEditor onSave val: ', val);
    var blob = val.blob,
        file = val.file;

    var _file$name$replace$sp = file.name.replace(/ /g, "-").split("."),
        _file$name$replace$sp2 = _slicedToArray(_file$name$replace$sp, 2),
        fname = _file$name$replace$sp2[0],
        ext = _file$name$replace$sp2[1];

    var files = new File([blob], file.name, {
      lastModified: Date.now(),
      type: file.type
    });
    var arr = [];
    SIZES.forEach(function (v) {
      Object(image_blob_reduce__WEBPACK_IMPORTED_MODULE_3__["default"])().toBlob(files, {
        max: v.size,
        unsharpAmount: 80,
        unsharpRadius: 0.6,
        unsharpThreshold: 2
      }).then(function (blob) {
        arr.push(_objectSpread(_objectSpread({}, v), {}, {
          blob: blob,
          // size: v, 
          // name: fname + "-" + v.size + "x" + v.size + "." + (v.ext || ext.toLowerCase())
          name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext.toLowerCase()
        }));
      });
    }); // Promise.all(SIZES.forEach(v => {
    // 	ImageBlobReduce().toBlob(files, {
    // 		max: v.size, 
    // 		unsharpAmount: 80, 
    // 		unsharpRadius: 0.6,
    // 		unsharpThreshold: 2
    // 	}).then(blob => {
    // 		// arr.push({ 
    // 		// 	...v, 
    // 		// 	blob, 
    // 		// 	// size: v, 
    // 		// 	// name: fname + "-" + v.size + "x" + v.size + "." + (v.ext || ext.toLowerCase())
    // 		// 	name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext.toLowerCase()
    // 		// });
    // 		setImg({ 
    // 			...v, 
    // 			blob, 
    // 			// size: v, 
    // 			// name: fname + "-" + v.size + "x" + v.size + "." + (v.ext || ext.toLowerCase())
    // 			name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext.toLowerCase()
    // 		});
    // 	});
    // }));

    setFileData(file);
    setTimeout(function () {
      // const copyArr = [ ...arr ];
      // const arrSort = arr.sort((a, b) => (a.size > b.size) ? 1 : -1);
      console.log('arr: ', arr); // console.log('arrSort: ', arrSort);

      setImg(arr.sort(function (a, b) {
        return a.size > b.size ? 1 : -1;
      })); // setImgOri(URL.createObjectURL(file));

      setModal(false);
    }, 3000);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Q.Cx("manifest-generator", className)
  }, prepend, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_ModalQ__WEBPACK_IMPORTED_MODULE_7__["default"], {
    open: modal // toggle
    ,
    size: "xl" // lg 
    ,
    contentClassName: "border-0 ovhide",
    position: "center",
    close: false,
    bodyClass: "p-0",
    body: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_image_editor_ImgEditor__WEBPACK_IMPORTED_MODULE_8__["default"] // pasteFromClipboard={false} 
    , {
      h: 450 // wrapClass="rounded" 
      ,
      captureImage: false,
      captureScreen: false // https://raw.githubusercontent.com/roadmanfong/react-cropper/master/example/img/child.jpg
      // data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhMVFhUXFhcWGBcXFxUXFRcXFRcXFhcVFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAPkAygMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAQIDBAYHAAj/xAA+EAABAwIDBQYFAgQFBAMAAAABAAIRAyEEBTESQVFhcQYigZGxwRMyodHwI1IUQuHxBzNygsJTc5KiNDVi/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAIDAQQF/8QAJxEAAgICAwACAgEFAQAAAAAAAAECEQMhEjFBIjITUUIEI2FxgRT/2gAMAwEAAhEDEQA/AOpAJYXgEsKJ0CQlhKlQAkJYSwlQYJC8lXkANcFFfeqOa5/QofM7vftFz5BZPG9t6rp+GwNbxdc/RK8kYjKDZuS+FEcW3e5s8JErk2Y9pqzpmo93IGw8rIQ7GVXXvHGVn5b8N/HXp2+nim/uHmFL8YcQuBVMwey+0ev5orGX9sa7DZ7iOFz5FOm34I0kd32kpXPMg7aS9ratQQddoAbp1W5wma0ao7jwVtmUWh0TSeSkYZCfC2wor24JWxKm2UmyiwoSEhToSQlNGwkIT4SQgBkJE4hJCAJQEq8lQaeheSpUGCLyVVMdjmU2kuItdAEteu1gLnEAC5JXOO1HbsmWUXQ2YkHvH/du6C6Edse1j67ixpimNAN/MlZzB0do3F/QKUpX/orGJdoPfUO3UPdF4lJWxG0YGm7+yrZhjQO6LAWVjKsPbaOpufsovS5Mqu6RIWhoDj/f+iq1S99924f0Ct4h224/tG7j4KOrtTEEuO4XgcLIiwkgfVYxl3d4/T6oe/MXSQ3u8reoR9nZ/EVbu7o4G3ordPsiZvB9VVZIrsm8cn0ZEY5+vm038kRwOMdIdTcWvGlyCI4FaOr2MDm8Cs3m+RVcOQ6ZE63TxywlpCSxTjs6h2H7YGoRQxAipo1255Hut2HHgvnPD4p4Ic3UGbE2I3grt3YrPRiqAM/qNs8e/imENAlSr0LTBqQp0JEGjYSJ6QhADCkToXoQBIvBR/FCVtUIAkXlC/EgKpWx/BAEmY4vYba54LmnbHMXuPw5vqdbcFqcwxpuZ8Vz7M3bTjxN1DJLdF8cfQNsi+/nxPBTuOww7nO4a9BzUopXA/OZVbGEkk6NFufmlW2O1SKOHobThtak2HAbyeceqPh8NI8/zoheXwXl37W28bBFGtuAdNT+dB9UmV2zcapE2EpWk9T1OiMZbhNm5+Y3PsOgUWBw8gTvufYIvSZCi5WWiizQpKw2kkoBX6NGbrErHZXNNUsZgWvaQ4Ag7kaNFV6lNa1RidnKc8yR2HcXMks9E/sb2iOHxLToDZw0kHiuhY7ChwIIkEQVy7Ncu+HVLRq02PLcurFPkqZyZYcXaPomjVDmhwMggEdCnrN9hMZ8TC05N2iCtKuhO0c7VMavJyRaYNSQnJFgDUickQaUSVVxGJjeqdbMm6SEPxWJWDWWq2MJ3qB2O3IXUrkprqyVs1IfnWJAETuusk90y7joiWbVyRE6oaGzA3flvJcrduzpgqQjGWJ3n8KH5i4QGjSR4onUd3Z4jyCE1ILgeYjqSB7poGT6JsAyGvPF0eSIYZm08DiQPdUMM6KbObijOQiSXncT9kmT1jRDNNsRy/PsiNNqG0qslWv41jdXCeCii/SCdAIlQcguEx1MmNtvmjWHqM12gfFUihZMmcFWrBWxVbxUGIe1M0KmUagWF7b0QypTf+6W+IuPdbXE4um3VwHisT/iTVaaNEtMzVsRya6VuJfJC5vqafsBjdmG7jZdDC4t2QxkEAm9j/Vdhy+vtMBXRifaOTKvSykSrysSESJyRBo1InJFgHLcfLbkGVUpY13FGce+SgmLY2dLp/yr1B+N+FmnjAbGxSVaipvpGJFwoRWNxuU8kVxuI8JPlTIcW6SmsFvzjHoE3EvgbXAJ2DE7A5yfC3uVw+HZ6NzB27h/YfVDMWAC3gCPMkfZEK5lxP5+aIPm9WNnrtew91TGvBMj0WKhinS/OC0OQECiTy+pus3nDu7T6T9AjmTv/RA3n2BS5F8TYfYnZWc6wOyN7t/h909r8I2z3X5uMlBs5w2JiKbe6LkyhGEygOguc4ukEyDFjJHQi2qaGNNXZuTI06SNm7CUXAOY9wG4w7Z5d42VrL21KTgHOt6qPB4Ciyk00tplQA7QG0aRkk7Ja83sYkK0abnUxMAg2gyIvokyKtWPjbfaNHSeSJQPO8RUJ2GmBvP9kay8zT8Fnu0ODqPaWsJbOpGvnuCSO2NJ0gcxlNpmoXO4zMfRDu2dKmaFN1M2+IDEyPldfyCc7IaGw4VGPD4bsupl5O0Bdx23EX4IRXyquzDB1RxLfiWaeYd3p/NV1KKTTs5pSk1TiW8oqbL2kaQPUfddg7N4raaBP91xbCOtO9vpK6d2TxNmndCSLqZk18TdBeTaZsnLrOU8kSryAEKRR16oaFQOKKeONy6MckjnVOuS4yZsqlZ0lXH0g3RUHFQytN6L400tkmGqwYKjxdIQSFXrPi6ne+WTxCS9D1sE5o/uxxICs4V0B3INb4mT7hDcyq99o/LwAre1DOp9BAUGtFk9kNWp3SeMn6wPRBMc/bMdGoji6to5e91Uy+ltVG9QT5yqw1slPei3nfzNbwAH0lG8mEbA5T6IJiG7dT/d9BYI1hXQ4ch9lLJ9Ui2NfKzV0aYcNE9uV0zfZUOBqWCL4czZSiyzQ2ll7Bu91DjqceVkYp04QPOq3e2R+2U8uhIrZZyt3dVllEOkIflrzYIpTEOvCVDNFZ+Cb+0IR2owm1hngDSHf+JBP0laeqwaoLnzooVSf+m70Kb0WtHM6dmu/wBI89qPYroPY100xyv9Fz3FPlro3lo8yT7rovYmnHd//JT/AMkRf1ZvME6WhWVRy11oV5dkejjl2eTajoEpyH46rJgJ4R5OhJOkQ1HF5U4wSkwVCBJVtUnkp1EWMb2zkuOdCHlW8zcqcrifZ2LoqY0pcNV7pb+XTcaFSpVe+7gPZFaC9lOv3sQBw9r+yvYhwlo6eqrUGTWe7h7pMS8zP5uUntoqtWDalWQ49fU/dEcoGy0vOp0QrDM2nFu6T7WRyi2SG8L/AGVJ6VCQ3slwdGL+HifwnwU9Z3eBGkwnU9Z3Cw6nU+ClNK4HD1XM3s6YoLZXiZstBhaqyuHZBkI5gnEpGVD3xybIRmmFf8TbYZBaARwibjjqpHY0M1I8VGMyad8rewX+AZh8Jimu2iQ4HSBs7J89FoMDSeCS902gDnvuo/45kRteTZ9012ZNlbRtMu/GIsVm+3WODMK4b3lrB4mT9AUX+Nt3GiwP+JWL79GlwDnnxOy30cmxLlKiOaXGAKwVWXNnSZ8tPVdO7InvjmFyrKhous9j23HgPuqtf3EQv4GwwQ16q8q2HGvMqyF0xOSQys+AShUyZV3MH2hRUqfcJXVj+Mb/AGSlt0ebjTwTv47kq1CntGFP/BFNKONOmKnI5dmpuFSY5Ws0d3lSBXnPs70QZg6BzQrDAjaJ4K1m1aC2OMqJtMQTuJnwTPURVuZ57tkc3H8KrYmbfmqZjq129QfqPZWfhyWu3AfVSqlZVu3RBhKAYOpnqVbpvgxvP0UNSx2t4BDfcp+Ao7RuYGpKWTvY0VQUwLhIJjZEnoALlSYOuKku4oV2jxgp0vhiz6ggC9qYN+hP34IZk+ZGnDToljibjyGWVKfE3uEaiGBOy4cECwGYNdcFF6VYcVFovZczXBU6tyBI36HzCFNwrWWI37yT6oxRqSrDKLTqtTZsZOIOo4Ci4WIF5sYHXVO/hKUwwTz/AKouzKKRvsDyCf8AwobYCE47ykLLMhcg7XYr4uNqkaNIpj/aL/8AttLp/aPMBQoOfvgho4uOi45QkuJNySZPNWwRq5HBnlbSD2SUZI5XXVex1O09SubZPTgDmurdkKUMHRZHczJ6iaSmII/NynUQ1ClXUjlYMxrpcrTmxT8FTq3f4q/WHd8F0z0oolHtlHBfMiaGYL5kTlLm+xsOjimPrNLgAVWcVDVP6o6J9YwLrlyRqVI6YStWwVmLpKkqu/THMg+qp4l0uCt1h+na8X8tVuRaSMxPbYPqsLgBvmPqiddwYBPQD1KhyylLTUdZokjiVBiMRJ2tnoDeAueW3R0R0rFpsc8yd+72RUVWUWy+NrVrBx4kqrTn4e3MSJtb6oS+pNOeBI+s+6WuRvKiPNS6s4vdcn8AHLcreFy7aYLXUOFv0WiyNuyb6Kk58Y0hccE5WwA+jVpXEwr2C7QEWdZat+HpuFxF481nc67PFjrBTjOMtSKuLj9Q1ludA77cUdoZg3iuWOwlRp7s+EgotldKu4xtOPXQeKJY12mbHI+mjqDMzEahR4jN2BsucABvJQLA5eBTc58mATqdwXL6uMc593E3OpJ8BwRjx8/RcmVQ8NX2szT452hOwJDRxnVxCzWBp/Uog8/pBJgqMOZzlVTqNEauVh7K6ckLrvZmlFOeg8ly/s/RlwC65lNLZpMHKfNLh22Znei07d1UhTHp66UcwK/n8URrfKUOf8/iiNX5fBdGT+JOPoJa6NE/4zuK9QZJhX/4RqrOcYvYiTZwgumqlzJ0NjioqX+Ym498ujgFxyVzOiLqAOZd11dbpCrUWXJVpjZF0mR7KY1SK+LxIa0N3Tp09ghbqxMlFsdRtfVCqtKAVONFZWSYLGdwsJ006FRUjIc3x+6pAkHopXVNkh35BTuG9E1PWx9Go5joBWkyrGk/M0dR9lnqo8Qr+AeRpfkbH7FTyJNFMbaYcrVySdk66azPut5haQrUmuc0SWieu/6ysZlTQCHG/EHct1goAtouV90dSKjMnozdgKIswlJosxo8AnFt0jwdFqMYPzUxh6pH7H+hXDaDZcF3fOKc0XN4tI8wuL5VhTtnatsWPhaFfBKlI580blEI4gQ0NTcM79SOGiixNeXJ2FOr0Vo3Vm+yRoBBG/TqV1HBO7jegXI8hxjXMA3hdMyDHCpTEG4sUn9PKpNMzOvQuQlamynNXYcgMxYh6vTLPBVswbcFSYd8sK6JbgmTWm0VcL8yKoVhvmRVZn7CHR8/PbFRVniSTzVvH/NwhBMZmEfJBjfw8N6R9jroLiloAiWGwkNlxAmY4noFmssrPDTXqlzr91p3ndZFMsrOqA1XmXExbQNtLW8tPJceW9nVja0PxdHUjXnp4oJjRutzWnqskQAhNbAaud66qcJU9lZqzNuF5+ybXZoJRHEUgNPrr4AKrVaTceW9dSlZzOIyg+IB04o5haQcO6gLKuo8FbwOMLDpbgkyRb6HxzSezVYcECeHotnkdaWgclisFmTHNI0tfxWoyUjYbHOPBcUrXZ2LfRpgV6FXZU0n6/dXKYWpgC84dAhc27QU/hTsCDUMnrpA+pXSM70lc87RUy5wIWwfyCcbiZmq0zG+wVwuDAGneIPqpDQgF5HJvuUPrzHOV1r5HI1xC+FxWyQRoVr8gz51IhwNtD9iufYOttDZ3i4RGhXcw2Bg66kKU8e9djRna30dzy7PadYTICMU3iLFcLwOakEbPdP0K2+RdpnAgOt6FNHI19hJY/0bvFslqpYdxEhWsHig9shP+Gu2GT40cso7KOHadpFFFsc17Z5onLkwiqPnDEYw4mqGus0/KPODA+YzuuhxwpfPwmVO7qdkuMgSR3RDfNQsbYuDiC2DpGpiQesK7lzTtACoWFxEuO1AvqY3pHroZb7FzJz2U20zIIbca3P4fNXsnr/pDlM9ZMKTtJljWNJ70wYgWkXg8Ah+Vk/DDBqRJ6lQbUoF4pxn/wADjcxLASBYb+fDmUIxOPqPMuMckzE4mGi1hOwNJ4uKpzuSxxpbGc2ycXnhxKg2e94qzTdALTx+ibgmbTwOfpBHoU3SZj7RG4MHeIkb41B+ycymNQdofUdRuSPGw82ndyI3hSPw5ZFSmZY63Np/aUGBHAUGu5H83rQZbjXMsROzeBYwguXw7kd8flkWY+CHa7Jv0K5pq2dMXSC7O2NH5XsqDyI666K5T7b4OBNWLHVj93gsP2goBri5umtt3MLNVnyfRWhhjJWRnnlFnTMz7a4LZIFRzjFtljvUgBZI5yKzxstIF9dfosw4J+AxPw3gnQqn/mila7EX9XNun0bPMsOO40bmz5IFmOGiOU+aMUsZLmze39fsl/hyT+GFzwbiXklIztHDOHehWqWKINzfqI+4TcbULnOuSG7p8vEqh8Yzc+HDxXSly7OZvi9Gow+NIsHQeOvlKv4fHudqQ7r3XfngsnQrOB49f7K/hyXmWyCNeClKCRSMmzqXZvNbAXka9FtMO7aAIXGMkxbheTYwOa6r2dxO1Sa7jqmxTp0Jkj6FyF7YTwV5dFkT5fwNEipSbIIeWGD8pBMd5psYv5a70YwGHNTEii2AHExtRAEnS94aNBwVHLs2YHuEBjdktY8CajI+UzeTOsQn4HA16j2lg2gCP1GkWAPzXO6+vikd+lI14E+0OYh79gg/DaDSDv5iGd0OIgfzTzglC8JQLXAayABGlv7hP7RVYxFWBHeIi15El1pFyZUJq7NNlQaTBmLOBuRebgqbj8VQ6l8nZFndRrq3cMgACNwI3KClQM6FJhKMukG8k9UWqWEEeIWt8VQJW7KNdthZT4KmWvYd0xO69jPA3Kno4baP5+FHKOWtFJ4JuRLf9Q0IKjLIloqoXszub0tioevrBPqq+HrFktsWnUHQ+O4opmsPqu5AX8AVRrVmMgFsxrdNB2khZabZZyuo3a7ht9R15Iu94tzt+eCzOHrN29qm0t5SY8yr1TGuMQIt+FZOLs2ElRNj6m3ScCNCY8DH2WccfdEHYmzh1Hghj3WXRiVI58rtkaa9spy8VYgWstxpa4NO7QrU0cV/LxCxbmyiWCxpIDXGCNCoZcfqOjFk8ZcbRPw3jfLj5f0Q5jYcAeCO4YFzXSIc255jQkKrjMNZrx0PqpxlTaKONpMny/CbTXO5mB6eqM4DLtkERclC8DiPhyOP90VyzNgXk7hPoFDJy8LQpHsXSFMtgm38oGpXROwlUuoDatfTmsKysHuBOhv0K2PZDHNFJxf3YcSfAxP0TYnumJlRtmlOWLwXbhtbEijRZtNmC6+7hy5rYSupOzmaPmfB5TFI1qpIaDAa27nEu2bu+Vgkb7+8tXOXta2nSmk1moa6S473Od4ndv3qLKseynTftRUdUMlrxLBB+Yk3Lukab1BiK1MUw0Q5xMlwERqYB3wtq3sLSWiehVNaq34svmATPfEmAS60gc03tNhfhvAAhkd3iY1J4qLKcSGVmOdtENIiCLHjpom5/Sc2u/bmSZuZMG4uFlfNG38GTYQtAB3mN0xr9wr5eNx+hHqhDiQ2Bc281bpUe7cy7mbDoEkl6Ug/A7lGIaTBMn88CpM2xRiPb3WTfgXSNh1+RP0hPf8AGYQ17zB4klT/ABK7TH/I/UEKVWQ517WVB1AvJOhRnK6YOpt9EXqsAG7y91n5ODB4+SM1hsLsCXfnndJiKkAnj9Ai9RgPHlv/AKIPjqN726lbF8nsyS4qkUWPk9QVBXEEhPrkCwUBK64o5ZsReSpEwgqcOaavIAv4HM30iDG03gdY4SjGFrseDsGWn+U/M12ojisxKfTcQZFipTxJ9FYZWuw9XFun4VBl1WHuG8/WVTbjXGzr+qnw7gSAeocNRy5hTcGlsqsib0GKeLLS5vECOW4ojTxj3sFOe7O7fvM8RICDGod8E8RqtB2Yw7XVAXfKL/0XPJF0zoXYrKGUqe3sgOdqeXVapZ7L6xqOhvdYwfgRttMwLrph0c0uz5YrNio8CYaTu3Angm02gz3mjrtewKnp0g0sJMucQSLgAWmfCVXBbtXEjru69Fciy1RdSpvDnONSIIDRstnmXCSJ4DxUmcV/iv8AiRsl24kkkDQgRYKi2mXHutMcpKK4fLHDD1KjmwRBbbvRa4vIF+G5TlSdspG2mkUKT4ME33ga+J3L1UnjE7gq7LOPMD+qstINzuQ0C6C2QUW7Uv05n2T+0+ZUz+k2HHedzeQ5rPVsSdAbaKu0rFit8mDy0uKCNHEhrYBcDx2j9Antxv7nv/OaGylCpwRPmwi/MLQ3zJM+UlVX1vFQyvLVFIHJscXyklIvJhR7RzXk1KgBV5IlWAKEqavAoAeCnhx3EqIFOBQAXy3HmYfBHHet92QwPxtt9MnuQCBqeh3FcvpPhdY/wT+XEci30UZ4ovZaGWXRv8pYwsGx8vDS4/dz6oiqOIwZDviUzDv5h/K7rz5pf4up/wBM+YQtGs+Z8spue57toDYpucXO6FoHUzZOrkOa01A5roaAQ0Q5oAiRIvEXVZ3+SP8AX/xKuZl/l0un/Fif0RdGlyTZw+CrvaRVDxMxsxAA2TtamTMa8kDzrNzVYAwFjD84nV8b41bA38OQVnBf/Ar/AOsetNB2/wCW/qz1cpxiuTb/AGUlJ8Ul+gXtkEKw9/dsq+K3dPcpWafnBXogmICnQmpwWmCgpwKjTkAOXki8gBwSpoTggBV5IvFACyllNSoAWV5IV5YAspQU1KtAkaV03/BfNmtrVMO63xBtNPNmrfK/gVzALUf4d/8A2OG/7n/Fyx9Grs+h0kJUikVP/9k=
      // /img/iron_man.jpg
      // src="/img/iron_man.jpg" 
      ,
      onSave: onSaveIcons
    }),
    foot: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
      kind: "dark",
      size: "sm",
      onClick: function onClick() {
        return setModal(false);
      }
    }, "Cancel")
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card shadow-sm position-sticky",
    style: {
      top: 60
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "icons_folder"
  }, "Icons folder name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    className: "form-control form-control-sm",
    type: "text",
    id: "icons_folder",
    value: zipFolder,
    onChange: function onChange(e) {
      return setZipFolder(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "jsonTab"
  }, "manifest.json tab size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
    className: "custom-select custom-select-sm",
    id: "jsonTab",
    value: jsonTab,
    onChange: function onChange(e) {
      return setJsonTab(e.target.value);
    }
  }, [2, 4, 6, 8].map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      key: i,
      value: v
    }, v);
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: 0
  }, "Minify"))), img.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
    onClick: onSave
  }, "Download")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-9"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-6 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "name"
  }, "App name ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("sup", {
    className: "text-danger font-weight-bold"
  }, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    className: "form-control",
    type: "text",
    id: "name",
    value: name,
    onChange: function onChange(e) {
      return setName(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-6 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "short_name"
  }, "Short name ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("sup", {
    className: "text-danger font-weight-bold"
  }, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    className: "form-control",
    type: "text",
    id: "short_name",
    value: short_name,
    onChange: function onChange(e) {
      return setShortName(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-12 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "description"
  }, "Description"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Textarea__WEBPACK_IMPORTED_MODULE_5__["default"], {
    id: "description" //  type="text"
    ,
    value: description,
    onChange: function onChange(e) {
      return setDescription(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-4 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "display"
  }, "Display"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
    className: "custom-select",
    id: "display",
    value: display,
    onChange: function onChange(e) {
      return setDisplay(e.target.value);
    }
  }, ["browser", "standalone", "minimal-ui", "fullscreen"].map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      key: i,
      value: v
    }, v);
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-4 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "start_url"
  }, "Start url ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("sup", {
    className: "text-danger font-weight-bold"
  }, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    className: "form-control",
    type: "text",
    id: "start_url",
    value: start_url,
    onChange: function onChange(e) {
      return setStartUrl(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-4 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "scope"
  }, "Scope"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    className: "form-control",
    type: "text",
    id: "scope",
    value: scope,
    onChange: function onChange(e) {
      return setScope(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-6 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "theme_color"
  }, "Theme Color"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"], {
    show: themePick,
    onToggle: function onToggle(isOpen, e, m) {
      return onToggleTheme(isOpen, "theme_color");
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Toggle, {
    as: "div",
    bsPrefix: "wrap-input-color input-group",
    tabIndex: "0",
    "data-color": "theme_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "input-group-prepend mb-0",
    htmlFor: "theme_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-text"
  }, "#")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_colorful__WEBPACK_IMPORTED_MODULE_2__["HexColorInput"], {
    color: theme_color,
    onChange: setTheme,
    type: "text",
    "data-color": "theme_color" // ={theme_color} 
    // inputMode="search" // 
    ,
    id: "theme_color",
    className: Q.Cx("form-control text-monospace", _defineProperty({}, "text-" + (isDark(theme_color) === "dark" ? "white" : "dark"), theme_color !== "")),
    style: {
      backgroundColor: theme_color // caretColor: color !== "" && isDark(theme_color) === "light" ? "#000" : undefined // "#fff"

    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Menu, {
    className: "p-1",
    onContextMenu: Q.preventQ
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_colorful__WEBPACK_IMPORTED_MODULE_2__["HexColorPicker"], {
    color: theme_color,
    onChange: setTheme
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-6 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "background_color"
  }, "Background Color"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"], {
    show: bgPick,
    onToggle: function onToggle(isOpen, e, m) {
      return onToggleBg(isOpen, "background_color");
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Toggle, {
    as: "div",
    bsPrefix: "wrap-input-color input-group",
    tabIndex: "0",
    "data-color": "background_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "input-group-prepend mb-0",
    htmlFor: "background_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-text"
  }, "#")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_colorful__WEBPACK_IMPORTED_MODULE_2__["HexColorInput"], {
    color: background_color,
    onChange: setBgColor,
    type: "text",
    "data-color": "background_color" // inputMode="search" // 
    ,
    id: "background_color",
    className: Q.Cx("form-control text-monospace", _defineProperty({}, "text-" + (isDark(background_color) === "dark" ? "white" : "dark"), background_color !== "")),
    style: {
      backgroundColor: background_color // caretColor: color !== "" && isDark(background_color) === "light" ? "#000" : undefined // "#fff"

    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_1__["default"].Menu, {
    className: "p-1",
    onContextMenu: Q.preventQ
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_colorful__WEBPACK_IMPORTED_MODULE_2__["HexColorPicker"], {
    color: background_color,
    onChange: setBgColor
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-12 form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "icons"
  }, "Icons ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("sup", {
    className: "text-danger font-weight-bold"
  }, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_4__["default"], {
    block: true,
    outline: true,
    kind: "secondary",
    className: "text-left",
    id: "icons",
    title: fileData === null || fileData === void 0 ? void 0 : fileData.name,
    onClick: function onClick() {
      return setModal(true);
    }
  }, (fileData === null || fileData === void 0 ? void 0 : fileData.name) || "Choose file")), img.length > 0 &&
  /*#__PURE__*/
  //  ml-2-next
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card-columns text-center"
  }, img.filter(function (v) {
    return !v.ext;
  }).map(function (v, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: "card shadow-sm"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Img__WEBPACK_IMPORTED_MODULE_6__["default"], {
      frame: "bs",
      frameClass: "card-body p-2 mb-0",
      className: "flexno",
      fluid: true,
      src: v.blob && URL.createObjectURL(v.blob),
      alt: fileData === null || fileData === void 0 ? void 0 : fileData.name //  + " - " + v.size
      ,
      caption: v.size + "x" + v.size,
      onLoad: function onLoad(e) {
        URL.revokeObjectURL(e.target.src);
      }
    }));
  })))))));
}
/*
<div className="input-group">
	<div className="custom-file text-truncate">
		<input type="file" className="custom-file-input" id="inputile" 
			onChange={onChangeIcon} 
		/>
		<label className="custom-file-label" htmlFor="inputile">
			{fileData?.name || "Choose file"}
		</label>
	</div>
</div>

{(imgOri && img.length > 0) && 
	<div className="col-md-12">
		<div className="row mb-3">
			<div className="col">
				<Img fluid src={imgOri} alt={fileData?.name} 
					onLoad={e => {
						URL.revokeObjectURL(e.target.src)
					}}
				/>
			</div>
			
			<div className="col ml-2-next">
				{img.map((v, i) => 
					<Img key={i} fluid src={URL.createObjectURL(v.blob)} alt={fileData?.name + " - " + v.size} 
						onLoad={e => {
							URL.revokeObjectURL(e.target.src)
						}}
					/>
				)}
			</div>
		</div>
	</div>
}
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Img.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Img.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Img; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // , { useState }
// import { Cx } from '../../utils/Q.js';
// errorText, errorDesc, onError, src, 

function Img(_ref) {
  var inRef = _ref.inRef,
      _ref$alt = _ref.alt,
      alt = _ref$alt === void 0 ? "" : _ref$alt,
      src = _ref.src,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? "lazy" : _ref$loading,
      _ref$drag = _ref.drag,
      drag = _ref$drag === void 0 ? false : _ref$drag,
      WrapAs = _ref.WrapAs,
      w = _ref.w,
      h = _ref.h,
      fluid = _ref.fluid,
      thumb = _ref.thumb,
      circle = _ref.circle,
      round = _ref.round,
      className = _ref.className,
      frameClass = _ref.frameClass,
      frame = _ref.frame,
      caption = _ref.caption,
      captionClass = _ref.captionClass,
      onLoad = _ref.onLoad,
      onError = _ref.onError,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "alt", "src", "loading", "drag", "WrapAs", "w", "h", "fluid", "thumb", "circle", "round", "className", "frameClass", "frame", "caption", "captionClass", "onLoad", "onError", "children"]);

  // const [load, setLoad] = useState(false);
  // const [err, setErr] = useState(null);
  // const support = 'loading' in HTMLImageElement.prototype;
  // const isLazy = SUPPORT_LOADING && loading === 'lazy';
  var bsFigure = frame === 'bs';
  var As = bsFigure ? "figure" : WrapAs ? WrapAs : "picture";

  var setCx = function setCx(et) {
    Q.setClass(et, "ava-loader", "remove"); // if(fluid || thumb) Q.setAttr(et, "height");
  };

  var Load = function Load(e) {
    // Q.setClass(e.target, "ava-loader", "remove");// OPTION: remove or Not
    setCx(e.target);
    if (onLoad) onLoad(e);
  }; // DEV: 


  var Error = function Error(e) {
    var et = e.target; // Q.setClass(et, "img-err");
    // Q.setAttr(et, {"aria-label":`&#xF03E;`});
    // setErr("&#xF03E;"); 

    setCx(et);
    et.src = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='95' viewBox='0 0 32 32'%3E%3Cg%3E%3Cpath d='M29,4H3A1,1,0,0,0,2,5V27a1,1,0,0,0,1,1H29a1,1,0,0,0,1-1V5A1,1,0,0,0,29,4ZM28,26H4V6H28Z'/%3E%3Cpath d='M18.29,12.29a1,1,0,0,1,1.42,0L26,18.59V9a1,1,0,0,0-1-1H7A1,1,0,0,0,6,9v1.58l7,7Z'/%3E%3Cpath d='M7,24H25a1,1,0,0,0,1-1V21.41l-7-7-5.29,5.3a1,1,0,0,1-1.42,0L6,13.42V23A1,1,0,0,0,7,24Z'/%3E%3C/g%3E%3C/svg%3E";
    if (onError) onError(e);
    return; // null
  };

  var img = function img() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", _extends({}, etc, {
      ref: inRef,
      loading: loading,
      alt: alt // OPTION
      ,
      width: w // isLazy && !w && !load ? 40 : w
      ,
      height: h // isLazy && !h && !load ? 40 : h
      ,
      src: Q.newURL(src).href // OPTION: Q.isDomain(src) ? src : Q.newURL(src).href
      ,
      className: Q.Cx("ava-loader", {
        // holder thumb| ava-loader
        'img-fluid': fluid,
        // fluid && !isLazy
        'img-thumbnail': thumb,
        'rounded-circle': circle,
        'rounded': round,
        'figure-img': bsFigure // 'fal img-err': err

      }, className),
      draggable: drag // !!drag 
      // aria-label={label || err} 
      ,
      onError: Error,
      onLoad: Load // OPTION: 
      // onContextMenu={e => {
      // Q.preventQ(e);
      // }}

      /* onLoad={e => {
      	if(isLazy){
      		setLoad(true);
      		let et = e.target;
      		// setAttr(et, 'width height');
      		setClass(et, 'isLoad');//  + (fluid ? ' img-fluid' : '')
      	}
      		onLoad(e);
      }} */

    }));
  };

  if (frame) {
    // const bsFigure = frame === 'bs';
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As // figure | div 
    // ref={frameRef} 
    , {
      className: // thumb
      Q.Cx("img-frame", {
        'figure': bsFigure
      }, frameClass) // draggable="false" // OPTION

    }, img(), bsFigure && caption && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
      className: Q.Cx("figure-caption", captionClass)
    }, caption), children);
  }

  return img();
} // Img.defaultProps = {
// loading: 'lazy',
// alt: '',
// onLoad: noop
// noDrag: true
// };

/* const error = e => {
	if(onError){
		onError(e);
	}else{
		// data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='50%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${this.props.alt}%3C/text%3E%3C/svg%3E
		e.target.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' style='text-anchor:middle'%3E%3Crect width='100%25' height='100%25' fill='%23868e96'%3E%3C/rect%3E%3Ctext x='50%25' y='45%25' fill='%23fff' dy='.3em' style='font-family:sans-serif'%3E${errorText ? errorText : alt}%3C/text%3E%3Ctext x='50%25' y='55%25' fill='%23fff' dy='.5em' style='font-size:0.7rem;font-family:sans-serif'%3E${errorDesc ? errorDesc : 'Image not found'}%3C/text%3E%3C/svg%3E`;// "/img/imgError.svg";
		return null;// FROM MDN https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
	}
   } */

/* const strNum = P.oneOfType([P.string, P.number]);

Img.propTypes = {
	frame: P.bool,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  alt: P.string,

  w: strNum,
  h: strNum,
	// src: P.string, // .isRequired

  fluid: P.bool,
  thumb: P.bool,
  circle: P.bool,
  round: P.bool,
  noDrag: P.bool,
	onError: P.func,
	onLoad: P.func
}; */

/***/ }),

/***/ "./resources/js/src/pages/admin/tools/ManifestGeneratorPage.js":
/*!*********************************************************************!*\
  !*** ./resources/js/src/pages/admin/tools/ManifestGeneratorPage.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ManifestGeneratorPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _apps_ManifestGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../apps/ManifestGenerator */ "./resources/js/src/apps/ManifestGenerator.js");


 // SvgToUrl

function ManifestGeneratorPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "Manifest Generator"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_apps_ManifestGenerator__WEBPACK_IMPORTED_MODULE_2__["default"], {
    className: "p-3" // title="Manifest Generator" 
    ,
    prepend: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      className: "h4"
    }, "Manifest Generator")
  }));
}

/***/ }),

/***/ "./resources/js/src/utils/clipboard/index.js":
/*!***************************************************!*\
  !*** ./resources/js/src/utils/clipboard/index.js ***!
  \***************************************************/
/*! exports provided: clipboardCopy, copyFn, pasteBlob, permissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clipboardCopy", function() { return clipboardCopy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyFn", function() { return copyFn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pasteBlob", function() { return pasteBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "permissions", function() { return permissions; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Copy to clipboard:
function clipboardCopy(target) {
  return new Promise(function (resolve, reject) {
    var clipboard = navigator.clipboard,
        txt;
    if (typeof target === 'string' && !target.tagName) txt = target;else if (target === null || target === void 0 ? void 0 : target.tagName) txt = target.textContent; // target.innerText
    else reject(new Error('Text or target DOM to copy is required.'));

    if (clipboard) {
      clipboard.writeText(txt).then(function () {
        return resolve(txt);
      })["catch"](function (e) {
        return reject(e);
      });
    } else {
      console.log('%cnavigator.clipboard NOT SUPPORT', 'color:yellow');
      copyFn(txt, {
        onOk: function onOk() {
          resolve(txt);
        },
        onErr: function onErr(e) {
          reject(e);
        }
      });
    }
  });
}

function copyFn(str, _ref) {
  var _ref$onOk = _ref.onOk,
      onOk = _ref$onOk === void 0 ? function () {} : _ref$onOk,
      _ref$onErr = _ref.onErr,
      onErr = _ref$onErr === void 0 ? function () {} : _ref$onErr;
  var DOC = document,
      el = DOC.createElement("textarea"),
      iOS = window.navigator.userAgent.match(/ipad|iphone/i);
  el.className = "sr-only sr-only-focusable";
  el.contentEditable = true; // needed for iOS >= 10

  el.readOnly = false; // needed for iOS >= 10

  el.value = str; // el.style.border = "0";
  // el.style.padding = "0";
  // el.style.margin = "0";
  // el.style.position = "absolute";
  // sets vertical scroll
  // let yPosition = window.pageYOffset || DOC.documentElement.scrollTop;
  // el.style.top = `${yPosition}px`;

  DOC.body.appendChild(el);

  if (iOS) {
    var range = DOC.createRange();
    range.selectNodeContents(el);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    el.setSelectionRange(0, 999999);
  } else {
    el.focus({
      preventScroll: false
    });
    el.select();
  }

  var OK = DOC.execCommand("copy");

  if (OK) {
    onOk(str);
  } else {
    onErr('Failed to copy');
  }

  el.remove(); // DOC.body.removeChild(el);
} // 
// const pasteBlob = async (src) => {
// 	try{
// 		// const imgURL = '/images/generic/file.png';
// 		const data = await fetch(src);
// 		const blob = await data.blob();
// 		await navigator.clipboard.write([
// 			new ClipboardItem({
// 				[blob.type]: blob
// 			})
// 		]);
// 		console.log('Image copied.');
// 	}catch(e){
// 		console.error(e.name, e.message);
// 	}
// }
// async function pasteBlob(){
//   try{
// 		if(!navigator.clipboard.read) return false;
// 		const clipboardItems = await navigator.clipboard.read();
// 		// console.log('clipboardItems: ', clipboardItems);
// 		let data = false;
//     for(const item of clipboardItems){
//       for(const type of item.types){
// 				const blob = await item.getType(type);
// 				// console.log('blob: ', blob);
// 				if(blob.type.startsWith("image/")){
// 					// const objURL = window.URL.createObjectURL(blob);
// 					// console.log('objURL: ', objURL);
// 					data = blob;
// 				}
//       }
// 		}
// 		return data;
//   }catch(e){
// 		console.error(e.name, e.message);
// 		return false;
//   }
// }


function pasteBlob(_x) {
  return _pasteBlob.apply(this, arguments);
} // CHECK Permisson paste
// clipboard-write - granted by default


function _pasteBlob() {
  _pasteBlob = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
    var items, lng, i;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (e.clipboardData) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            items = e.clipboardData.items;

            if (items) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return");

          case 6:
            lng = items.length;
            i = 0;

          case 8:
            if (!(i < lng)) {
              _context.next = 15;
              break;
            }

            if (!(items[i].type.indexOf("image/") === -1)) {
              _context.next = 11;
              break;
            }

            return _context.abrupt("continue", 12);

          case 11:
            return _context.abrupt("return", items[i].getAsFile());

          case 12:
            i++;
            _context.next = 8;
            break;

          case 15:
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log('Error pasteBlob e: ', _context.t0);
            return _context.abrupt("return", false);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));
  return _pasteBlob.apply(this, arguments);
}

function permissions() {
  return _permissions.apply(this, arguments);
}

function _permissions() {
  _permissions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var query;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return navigator.permissions.query({
              name: "clipboard-read",
              allowWithoutGesture: false
            });

          case 3:
            query = _context2.sent;
            // Will be 'granted', 'denied' or 'prompt':
            console.log(query.state); // Listen for changes to the permission state

            query.onchange = function () {
              console.log(query.state);
            };

            return _context2.abrupt("return", query);

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", false);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 9]]);
  }));
  return _permissions.apply(this, arguments);
}



/***/ })

}]);
//# sourceMappingURL=ManifestGeneratorPage.chunk.js.map