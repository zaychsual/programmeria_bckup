(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Devs~ManifestGeneratorPage"],{

/***/ "./resources/js/src/apps/image-editor/ImgEditor.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/apps/image-editor/ImgEditor.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ImgEditor; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dropzone__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dropzone */ "./node_modules/react-dropzone/dist/es/index.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var react_cropper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-cropper */ "./node_modules/react-cropper/dist/react-cropper.es.js");
/* harmony import */ var cropperjs_dist_cropper_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! cropperjs/dist/cropper.css */ "./node_modules/cropperjs/dist/cropper.css");
/* harmony import */ var cropperjs_dist_cropper_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(cropperjs_dist_cropper_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _utils_file_fileRead__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../utils/file/fileRead */ "./resources/js/src/utils/file/fileRead.js");
/* harmony import */ var _utils_img_captureVideoFrame__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../utils/img/captureVideoFrame */ "./resources/js/src/utils/img/captureVideoFrame.js");
/* harmony import */ var _utils_clipboard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../utils/clipboard */ "./resources/js/src/utils/clipboard/index.js");
/* harmony import */ var _utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils/img/screenCaptureAPI */ "./resources/js/src/utils/img/screenCaptureAPI.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }


 // import CropperJS from 'cropperjs';





 // DEVS: Screen Capture API (CHANGE TO ES Module)



 // displayMediaOptions, userMediaOptions

var SM_DEVICE = Q_appData.UA.platform.type === "mobile" || screen.width <= 480;
var VIDEO_ID = Q.Qid();
var ASPECT_RATIO = [{
  txt: "16 : 9",
  val: 16 / 9
}, {
  txt: "4 : 3",
  val: 4 / 3
}, {
  txt: "1 : 1",
  val: 1 / 1
}, {
  txt: "Free",
  val: NaN
}]; // console.log('screenCaptureAPI: ', screenCaptureAPI);

function ImgEditor(_ref) {
  var src = _ref.src,
      _ref$aspectRatio = _ref.aspectRatio,
      aspectRatio = _ref$aspectRatio === void 0 ? 1 / 1 : _ref$aspectRatio,
      _ref$guides = _ref.guides,
      guides = _ref$guides === void 0 ? false : _ref$guides,
      _ref$rotate = _ref.rotate,
      rotate = _ref$rotate === void 0 ? 0 : _ref$rotate,
      _ref$flipX = _ref.flipX,
      flipX = _ref$flipX === void 0 ? 1 : _ref$flipX,
      _ref$flipY = _ref.flipY,
      flipY = _ref$flipY === void 0 ? 1 : _ref$flipY,
      wrapClass = _ref.wrapClass,
      croppperClass = _ref.croppperClass,
      cropperStyle = _ref.cropperStyle,
      _ref$pasteFromClipboa = _ref.pasteFromClipboard,
      pasteFromClipboard = _ref$pasteFromClipboa === void 0 ? true : _ref$pasteFromClipboa,
      w = _ref.w,
      _ref$h = _ref.h,
      h = _ref$h === void 0 ? 500 : _ref$h,
      _ref$captureImage = _ref.captureImage,
      captureImage = _ref$captureImage === void 0 ? true : _ref$captureImage,
      _ref$captureScreen = _ref.captureScreen,
      captureScreen = _ref$captureScreen === void 0 ? true : _ref$captureScreen,
      _ref$onSave = _ref.onSave,
      onSave = _ref$onSave === void 0 ? Q.noop : _ref$onSave;

  // let CropperJS = null;
  var styles = _objectSpread(_objectSpread({}, cropperStyle), {}, {
    height: h,
    width: w // "100%"

  });

  var cropperRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // DEVS: Screen Capture API

  var videoRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      onceCapture = _useState2[0],
      setOnceCapture = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isCapture = _useState4[0],
      setIsCapture = _useState4[1]; // const [imgCapture, setImgCapture] = useState(null);
  // END DEVS: Screen Capture API


  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(src),
      _useState6 = _slicedToArray(_useState5, 2),
      imgSrc = _useState6[0],
      setImgSrc = _useState6[1]; // src ? Q.newURL(src).href : null


  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      externalMedia = _useState8[0],
      setExternalMedia = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      once = _useState10[0],
      setOnce = _useState10[1]; // const [tools, setTools] = useState(false);


  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState12 = _slicedToArray(_useState11, 2),
      fname = _useState12[0],
      setFname = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(".png"),
      _useState14 = _slicedToArray(_useState13, 2),
      ext = _useState14[0],
      setExt = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      flipCapture = _useState16[0],
      setFlipCapture = _useState16[1]; // const [enableCrop, setEnableCrop] = useState(enable);
  // const [reload, setReload] = useState(true);
  // const [guide, setGuide] = useState(guides);


  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(aspectRatio),
      _useState18 = _slicedToArray(_useState17, 2),
      ratio = _useState18[0],
      setRatio = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(rotate),
      _useState20 = _slicedToArray(_useState19, 2),
      rotates = _useState20[0],
      setRotate = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(flipX),
      _useState22 = _slicedToArray(_useState21, 2),
      fX = _useState22[0],
      setFx = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(flipY),
      _useState24 = _slicedToArray(_useState23, 2),
      fY = _useState24[0],
      setFy = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(),
      _useState26 = _slicedToArray(_useState25, 2),
      dataCropper = _useState26[0],
      setDataCropper = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(),
      _useState28 = _slicedToArray(_useState27, 2),
      dataFile = _useState28[0],
      setDataFile = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(),
      _useState30 = _slicedToArray(_useState29, 2),
      result = _useState30[0],
      setResult = _useState30[1];

  var onDrop = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (files) {
    var file = files[0]; // const blob = window.URL.createObjectURL(file);
    // window.URL.revokeObjectURL(this.src);

    console.log('onDrop files: ', files); // setImgSrc(window.URL.createObjectURL(file));
    // setFname(file.name);

    if (file) {
      // let percent;
      Object(_utils_file_fileRead__WEBPACK_IMPORTED_MODULE_7__["fileRead"])(file, {// onProgress: (e) => {
        // 	const { loaded, total } = e;
        // 	if (loaded && total) {
        // 		percent = Math.round((loaded / total) * 100);
        // 		console.log("Progress: ", percent);
        // 		setProgressFile(percent);
        // 	}
        // }
      }).then(function (v) {
        // console.log('v: ', v);
        // let res = v.result;
        // if(res?.length <= 0){
        // 	res = window.URL.createObjectURL(file);
        // } 
        setDataFile(file);
        setImgSrc(v.result);
        setFname(file.name);
        setExt(Q.fileName(file.name).ext); // if(res && percent === 100) setProgressFile(null);
      })["catch"](function (e) {
        console.log(e);
      });
    }
  }, []); // inputRef

  var _useDropzone = Object(react_dropzone__WEBPACK_IMPORTED_MODULE_1__["useDropzone"])({
    onDrop: onDrop,
    accept: "image/*",
    // noDragEventsBubbling: true,
    multiple: false,
    noClick: true,
    noKeyboard: true
  }),
      getRootProps = _useDropzone.getRootProps,
      getInputProps = _useDropzone.getInputProps,
      isDragActive = _useDropzone.isDragActive;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (pasteFromClipboard) {
      var pasteFn = function pasteFn(e) {
        // const docActive = document.activeElement;
        // const editorActive = Q.hasClass(docActive, "imgEditor") ? docActive : docActive.closest(".imgEditor");
        // console.log('docActive: ', docActive);
        // console.log('editorActive: ', editorActive);
        // // NOTE: CHECK SUPPORT
        Object(_utils_clipboard__WEBPACK_IMPORTED_MODULE_9__["pasteBlob"])(e).then(function (data) {
          // If there's an image, display it in the canvas
          if (data) {
            // editorActive && data
            // let canvas = document.getElementById("canvas");
            // let ctx = canvas.getContext('2d');
            // Create an image to render the blob on the canvas
            // let img = new Image();
            // Once the image loads, render the img on the canvas

            /* img.onload = function(){
            	// Update dimensions of the canvas with the dimensions of the image
            	canvas.width = this.width;
            	canvas.height = this.height;
            						// Draw the image
            	ctx.drawImage(img, 0, 0);
            }; */
            // Crossbrowser support for URL
            var url = window.URL || window.webkitURL;

            var _ext = data.type.replace("image/", "");

            setImgSrc(url.createObjectURL(data));
            setFname("external-img-" + Date.now() + "." + _ext); // + extSet

            setExt(_ext); // img.src = blob;
          }
        })["catch"](function (e) {
          console.log('Error call pasteBlob e: ', e);
        });
      };

      window.addEventListener('paste', pasteFn); // document

      return function () {
        window.removeEventListener('paste', pasteFn); // document
      };
    }
  }, [pasteFromClipboard]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (once) {
      if (imgSrc && !imgSrc.startsWith("data:image/") && !imgSrc.startsWith("blob:http")) {
        var names = Q.fileName(imgSrc);
        setFname(names.fullname); // imgSrc.split("/").pop()

        setExt(names.ext);
      }
    }
  }, [once]); // , imgSrc
  // useEffect(() => {
  // 	getCropper().setDefaults({ aspectRatio: ratio, rotateTo: rotate, guides: !guide });
  // }, [guide]);
  // useEffect(() => {
  // 	getCropper().enable(enableCrop);
  // }, [enableCrop]);

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (imgSrc) getCropper().rotateTo(rotates);
  }, [imgSrc, rotates]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (imgSrc) getCropper().setAspectRatio(ratio);
  }, [imgSrc, ratio]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (imgSrc) getCropper().scale(fX, fY);
  }, [imgSrc, fX]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (imgSrc) getCropper().scale(fX, fY);
  }, [imgSrc, fY]);

  var getCropper = function getCropper() {
    var _cropperRef$current;

    return cropperRef === null || cropperRef === void 0 ? void 0 : (_cropperRef$current = cropperRef.current) === null || _cropperRef$current === void 0 ? void 0 : _cropperRef$current.cropper;
  }; // const onCrop = (e) => {
  //   // const imgEl = cropperRef?.current;
  // 	// const cropper = imgEl?.cropper;
  // 	console.log('onCrop e: ', e);
  // 	console.log(getCropper()); // getCropper().getCroppedCanvas().toDataURL()
  // 	setDataCrop(getCropper());
  // };
  // const onSetGuide = () => {
  // 	// setReload(false);
  // 	setGuide(!guide);
  // 	// setTimeout(() => setReload(true), 1);
  // 	// setTimeout(() => {
  // 	// 	let cropper = getCropper();
  // 	// 	// let cropBoxData = cropper.getCropBoxData();
  // 	// 	// let canvasData = cropper.getCanvasData();
  // 	// 	// cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
  //   //   cropper.destroy();
  //   //   new CropperJS(cropperRef.current, { guides: guide });// cropperRef.current = 
  // 	// }, 1);
  // }


  var onFlipX = function onFlipX() {
    fX > 0 ? setFx(-1) : setFx(1);
  };

  var onFlipY = function onFlipY() {
    fY > 0 ? setFy(-1) : setFy(1);
  }; // DEVS: Screen Capture API


  var onCapture = function onCapture() {
    if (!onceCapture) setOnceCapture(true);
    setTimeout(function () {
      var vid = videoRef.current;

      if (vid) {
        Object(_utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["getDisplayMedia"])(vid).then(function (data) {
          console.log('onCapture data: ', data);
          vid.srcObject = data; // dumpOptionsInfo(vid);

          var vidTrack = vid.srcObject.getVideoTracks()[0];
          console.log(JSON.stringify(vidTrack.getSettings(), null, 2)); // vidTrack.applyConstraints({
          // 	cursor: "never", // https://github.com/web-platform-tests/wpt/issues/16206
          // 	// width,
          // 	// height,
          // 	// aspectRatio: 1.33,
          // 	// resizeMode: "crop-and-scale"
          // });
          // vidTrack.contentHint = "motion";
          // vidTrack.cursor = "never";
          // if stop sharing (Chrome)

          vidTrack.onended = function () {
            onStopCapture();
          };

          setIsCapture(true);
        })["catch"](function (e) {
          console.log("Error: ", e);
          return null;
        });
      }
    }, 9);
  };

  var onStopCapture = function onStopCapture() {
    Object(_utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["stopCapture"])(videoRef.current);
    setIsCapture(false);
    if (flipCapture) setFlipCapture(false); // Reset flip
  };

  var onSaveImg = function onSaveImg() {
    var vid = videoRef.current;
    var frame = Object(_utils_img_captureVideoFrame__WEBPACK_IMPORTED_MODULE_8__["default"])(vid, {
      ext: "png",
      flip: flipCapture
    }); // console.log('frame: ', frame);

    Object(_utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["stopCapture"])(vid);
    setIsCapture(false);
    setImgSrc(frame.uri);
    setFname("capture-screen-" + Date.now()); // null | Q.Qid()

    setExt("png");
    if (flipCapture) setFlipCapture(false); // Reset flip
  };

  var onSaveEditFilename = function onSaveEditFilename() {
    var name = Q.fileName(fname).name;

    if (name.length < 1) {
      setFname("Untitled-" + Date.now() + "." + ext);
    }
  };

  var onGetUserMedia = function onGetUserMedia() {
    // MUST STRICT CHECK DEVICE: window.screen.width > 480
    // console.log(SM_DEVICE);
    if (!SM_DEVICE && _utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["getUserMediaAPI"]) {
      if (!onceCapture) setOnceCapture(true);
      if (!flipCapture) setFlipCapture(true); // Set flip

      setTimeout(function () {
        var options = {
          // constraints
          video: true // audio: true

        };
        var vid = videoRef.current; // navigator.mediaDevices.getUserMedia(options)

        Object(_utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["getUserMedia"])(options).then(function (stream) {
          console.log('getUserMedia stream: ', stream);
          vid.srcObject = stream;
          setIsCapture(true);
        })["catch"](function (e) {
          console.log('Error getUserMedia: ', e);
        });
      }, 9);
    }
  };

  var onClickExternal = function onClickExternal() {
    var file = Q.fileName(externalMedia);
    var isExt = ["png", "jpg", "jpeg", "svg", "gif"].includes(file.ext.toLowerCase()); // console.log('onClickExternal file: ', file);
    // console.log('onClickExternal isExt: ', isExt);

    if (Q.isAbsoluteUrl(externalMedia) && isExt || externalMedia.startsWith("data:image/")) {
      var extSet = isExt ? file.ext : externalMedia.split(';')[0].slice(5).replace("image/", ""); // console.log('onClickExternal extSet: ', extSet);

      setImgSrc(externalMedia);
      setFname(isExt ? file.fullname : "external-img-" + Date.now() + "." + extSet);
      setExt(extSet);
      setExternalMedia(""); // Reset input value

      document.body.click();
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Q.Cx("imgEditor", wrapClass),
    tabIndex: "0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    wrap: true,
    className: "p-2 ml-1-next bg-strip"
  }, fname &&
  /*#__PURE__*/
  // py-1 px-2 bg-light rounded
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group btn-group-sm"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    bsPrefix: "btn-group mxw-27vw tip tipBL",
    "aria-label": "Edit",
    onToggle: function onToggle(open) {
      if (!open) onSaveEditFilename();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    size: "sm",
    variant: "light",
    bsPrefix: "text-truncate",
    title: fname
  }, fname), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
    className: "w-400 p-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "input-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    className: "form-control",
    type: "text",
    value: Q.fileName(fname).name,
    onChange: function onChange(e) {
      var val = e.target.value;
      setFname(val.length > 0 ? val + "." + ext : val);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-append"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-text"
  }, ext), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    onClick: onSaveEditFilename,
    as: "button",
    type: "button",
    bsPrefix: "btn btn-primary"
  }, "Save"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: function onClick() {
      setFname(null);
      setImgSrc(null);
    },
    kind: "light",
    className: "flexno qi qi-close xx"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_5__["default"], {
    wrap: true,
    align: "center",
    className: "ml-md-auto ml-1-next"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    size: "sm",
    disabled: !dataCropper,
    onClick: function onClick() {
      if (dataCropper) {
        // typeof dataCrop !== "undefined"
        // console.log('SAVE dataCropper: ', dataCropper);// getCropper().getCroppedCanvas().toDataURL()
        // setDataCropper(null);
        var cropCanvas = dataCropper.getCroppedCanvas();
        cropCanvas.toBlob(function (blob) {
          // console.log('blob: ', blob);
          var res = {
            blob: blob,
            // : URL.createObjectURL(blob), 
            url: cropCanvas.toDataURL(),
            file: dataFile
          };
          setResult(res);
          onSave(res);
        });
      }
    }
  }, "Save"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    size: "sm",
    variant: "light"
  }, "Add image"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
    className: "w-400"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "label",
    role: "button"
  }, "Insert File", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({
    hidden: true
  }, getInputProps()))), captureImage && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
    as: "label",
    role: "button",
    onClick: onGetUserMedia
  }, "Capture image", window.screen.width <= 480 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({
    hidden: true,
    capture: true
  }, getInputProps()))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "input-group input-group-sm py-2 px-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    className: "form-control",
    placeholder: "Insert URL" // id="extMediaInput"
    ,
    value: externalMedia,
    onChange: function onChange(e) {
      return setExternalMedia(e.target.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "input-group-append"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    As: "div",
    onClick: function onClick() {
      return setExternalMedia("");
    },
    kind: "light",
    className: "qi qi-close xx",
    hidden: externalMedia.length < 1
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    As: "div",
    outline: true,
    onClick: onClickExternal
  }, "Insert"))))), captureScreen && _utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["screenCaptureAPI"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: onCapture,
    size: "sm",
    kind: "light",
    className: "qi qi-desktop",
    disabled: isCapture
  }, "Capture screen"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
    className: "ml-1-next",
    disabled: !imgSrc
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group btn-group-sm"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    kind: "light",
    className: "tip tipB qi qi-arrows-alt",
    "aria-label": "Move"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    kind: "light",
    className: "tip tipB qi qi-crop",
    "aria-label": "Crop"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group btn-group-sm"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: function onClick() {
      return setRotate(rotates - 45);
    },
    kind: "light",
    className: "tip tipB qi qi-undo",
    "aria-label": "Rotate -45"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: function onClick() {
      return setRotate(rotates + 45);
    },
    kind: "light",
    className: "tip tipB qi qi-redo",
    "aria-label": "Rotate 45"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "btn-group btn-group-sm"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: onFlipX,
    kind: "light",
    className: "tip tipB qi qi-arrows-h",
    "aria-label": "Flip X"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: onFlipY,
    kind: "light",
    className: "tip tipB qi qi-arrows-v",
    "aria-label": "Flip Y"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"], {
    alignRight: true,
    bsPrefix: "btn-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Toggle, {
    size: "sm",
    variant: "light"
  }, "Aspect ratio"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Menu, {
    className: "mnw-auto text-right"
  }, ASPECT_RATIO.map(function (v) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].Item, {
      key: v.txt,
      onClick: function onClick() {
        return setRatio(v.val);
      },
      active: v.val === ratio,
      as: "button",
      type: "button"
    }, v.txt);
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", _extends({}, getRootProps(), {
    className: Q.Cx("position-relative", {
      "dropFile": isDragActive || !imgSrc
    }),
    "aria-label": isDragActive || !imgSrc ? "Drop file here" : undefined
  }), imgSrc ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_cropper__WEBPACK_IMPORTED_MODULE_3__["default"], {
    ref: cropperRef,
    className: croppperClass // Q.Cx("px-0 col", croppperClass)
    // Cropper.js options 
    // enable={enableCrop} 
    ,
    aspectRatio: ratio // initialAspectRatio={16 / 9} 
    ,
    guides: guides // zoom={} 
    // onInitialized={c => {
    // 	// console.log('onInitialized c: ', c);
    // 	// CropperJS = c;
    // 	console.log('onInitialized imgSrc: ', imgSrc);
    // 	if(!imgSrc.includes("data:image/") || !imgSrc.startsWith("blob:http:")){
    // 		console.log('onInitialized: ');
    // 		let name = imgSrc.split("/");
    // 		setFname(name[name.length - 1]);
    // 	}
    // }} 
    ,
    viewMode: 1,
    minCropBoxHeight: 10,
    minCropBoxWidth: 10,
    autoCropArea: 1,
    checkOrientation: false // https://github.com/fengyuanchen/cropperjs/issues/671
    ,
    onInitialized: function onInitialized(instance) {
      setDataCropper(instance);
    },
    ready: function ready() {
      // e
      // console.log('ready once: ',once);
      // console.log('ready imgSrc.startsWith: ', imgSrc.startsWith("data:image/"));
      if (!once) setOnce(true); // if(imgSrc && (!imgSrc.startsWith("data:image/") && !imgSrc.startsWith("blob:http"))){
      // 	console.log('=============== ready imgSrc: ===============', imgSrc);
      // 	let name = imgSrc.split("/");
      // 	setFname(name[name.length - 1]);
      // }
    } // preview={} 
    // cropstart={} 
    // cropmove={} 
    // crop={onCrop} 
    // cropend={onCrop} 
    ,
    rotateTo: rotate // scaleX={fX} 
    // scaleY={fY} 
    ,
    src: result ? result.url : imgSrc // imgSrc
    ,
    style: styles
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cropper-bg",
    style: styles
  }), (_utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["screenCaptureAPI"] || _utils_img_screenCaptureAPI__WEBPACK_IMPORTED_MODULE_10__["getUserMediaAPI"]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, onceCapture && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    //  + (isCapture ? "":" d-none")
    className: "embed-responsive embed-responsive-16by9 position-absolute position-full zi-5 bg-dark playerQ touch-no",
    hidden: !isCapture,
    style: {
      height: h
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("video", {
    autoPlay: true,
    ref: videoRef,
    className: Q.Cx("embed-responsive-item point-no", {
      "mirror-q": flipCapture
    }),
    id: "videoCapture-" + VIDEO_ID // "videoCapture" 
    ,
    tabIndex: "-1"
  }), isCapture && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "p-2 position-absolute b0 l0 r0 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: onStopCapture,
    kind: "danger",
    className: "qi qi-close"
  }, "Cancel Capture"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_6__["default"], {
    onClick: onSaveImg,
    className: "qi qi-save"
  }, "Save capture"))))));
}
/*
			<div style={{ height: h }}>
				{reload && }
			</div>

<Flex dir="column" className={Q.Cx("ovhide position-relative imgEditor", wrapClass)}>

			<div tabIndex={tools ? undefined : -1} 
				className={Q.Cx("bg-white position-absolute t0 b0 r0 col-3 crop-tool", { "toolOpen": tools })}
			>
				<Btn As="div" onClick={() => setTools(!tools)} size="sm" kind="light" 
					className="round-right-0 position-absolute zi-1 crop-toggle-tool fas fa-sort i-r90" 
				/>

				<div className="p-2 bg-strip">
					Options
				</div>

				<div className="crop-options">

				</div>

				<Btn>Save</Btn>
			</div>

<div className="position-absolute t0 b0 zi-1 crop-toggle-tool">
	<Btn onClick={() => setTools(!tools)} size="sm" kind="light" className="round-right-0 fas fa-sort i-r90" />
</div>

<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Textarea.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Textarea.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Textarea; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { Cx } from '../../utils/Q';

function Textarea(_ref) {
  var h = _ref.h,
      value = _ref.value,
      defaultValue = _ref.defaultValue,
      className = _ref.className,
      style = _ref.style,
      onChange = _ref.onChange,
      _ref$bs = _ref.bs,
      bs = _ref$bs === void 0 ? "form-control" : _ref$bs,
      etc = _objectWithoutProperties(_ref, ["h", "value", "defaultValue", "className", "style", "onChange", "bs"]);

  var elRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      txt = _useState2[0],
      setTxt = _useState2[1]; // "" | value || defaultValue || 


  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(h),
      _useState4 = _slicedToArray(_useState3, 2),
      height = _useState4[0],
      setHeight = _useState4[1]; // const [parentHeight, setParentHeight] = useState("auto");
  // useEffect(() => {
  //   let el = elRef.current;
  //   if(value || defaultValue){
  //     setHeight(el.scrollHeight + 2);
  //   }
  // }, [value, defaultValue]);


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // setParentHeight(`${elRef.current.scrollHeight}px`);
    var el = elRef.current; // console.log('clientHeight: ', el.clientHeight);
    // console.log('scrollHeight: ', el.scrollHeight);

    if ((value || defaultValue) && el && el.clientHeight !== el.scrollHeight) {
      // text.length > 0 && 
      setHeight(el.scrollHeight + 2); // (el.scrollHeight + 2) + "px"
    } // if(txt || (value || defaultValue)){
    //   setHeight(el.scrollHeight + 2);
    // }
    // console.log('txt: ', txt);
    // console.log('value: ', value);
    // console.log('defaultValue: ', defaultValue);

  }, [txt, value, defaultValue]);

  var Change = function Change(e) {
    setHeight(h); // null | "auto"
    // setParentHeight(`${elRef.current.scrollHeight}px`);

    setTxt(e.target.value); // setHeight(elRef.current.scrollHeight + "px");

    if (onChange) onChange(e);
  }; // const Keyup = (e) => {
  //   let et = e.target;
  //   // setTimeout(() => {
  //     // et.style.height = 'auto';// ;padding:0
  //     // for box-sizing other than "content-box" use:
  //     // et.style.cssText = '-moz-box-sizing:content-box';
  //     let scrl = et.scrollHeight;
  //     // et.value.length === 0
  //     if(et.clientHeight !== scrl){//  && e.keyCode === 8
  //       // et.style.height = h; // 'height:' + h + 'px';
  //       setHeight((scrl + 2) + "px");// setHeight(h);
  //     }
  //     // else{
  //     //   // et.style.height = scrl + 'px';
  //     //   setHeight((scrl + 2) + "px");
  //     // }
  //     // et.classList.remove("ovhide");
  //   // }, 1);
  //   if(onKeyUp) onKeyUp(e);
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", _extends({}, etc, {
    ref: elRef,
    value: value,
    defaultValue: defaultValue,
    className: Q.Cx(bs, className),
    style: _objectSpread(_objectSpread({}, style), {}, {
      height: height
    }) // onKeyUp={Keyup} 
    ,
    onChange: Change
  }));
}
;
/*
<div
  style={{
    minHeight: parentHeight,
  }}
>
    
</div>
*/

/***/ }),

/***/ "./resources/js/src/utils/darkOrLight.js":
/*!***********************************************!*\
  !*** ./resources/js/src/utils/darkOrLight.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// 
// https://codepen.io/andreaswik/pen/YjJqpK
var darkOrLight = Q.cached(function (color) {
  var r, g, b, hsp; // Check the format of the color, HEX or RGB?

  if (color.match(/^rgb/)) {
    // If HEX --> store the red, green, blue values in separate variables
    color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
    r = color[1];
    g = color[2];
    b = color[3];
  } else {
    // If RGB --> Convert it to HEX: http://gist.github.com/983661
    color = +("0x" + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));
    r = color >> 16;
    g = color >> 8 & 255;
    b = color & 255;
  } // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html


  hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b)); // Using the HSP value, determine whether the color is light or dark

  if (hsp > 127.5) return 'light';
  return 'dark';
});
/* harmony default export */ __webpack_exports__["default"] = (darkOrLight);

/***/ }),

/***/ "./resources/js/src/utils/img/captureVideoFrame.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/utils/img/captureVideoFrame.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return captureVideoFrame; });
// https://github.com/ilkkao/capture-video-frame
function captureVideoFrame(video) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$ext = _ref.ext,
      ext = _ref$ext === void 0 ? "jpeg" : _ref$ext,
      _ref$flip = _ref.flip,
      flip = _ref$flip === void 0 ? false : _ref$flip,
      _ref$quality = _ref.quality,
      quality = _ref$quality === void 0 ? 0.92 : _ref$quality;

  if (Q.isStr(video)) {
    // typeof video === 'string'
    video = Q.domQ(video); // document.getElementById(video);
  } // format = format || 'jpeg';
  // quality = quality || 0.92;


  if (!video || ext !== 'png' && ext !== 'jpeg') {
    return false;
  } // canvas


  var cvs = Q.makeEl("canvas"); // document.createElement("CANVAS");

  cvs.width = video.videoWidth;
  cvs.height = video.videoHeight;
  var ctx = cvs.getContext('2d'); // Flip image:

  if (flip) {
    ctx.setTransform(-1, 0, 0, 1, cvs.width, 0);
  }

  ctx.drawImage(video, 0, 0);
  var type = 'image/' + ext;
  var uri = cvs.toDataURL(type, quality); // dataUri
  // let data = uri.split(',')[1];
  // let type = uri.split(';')[0].slice(5);// mimeType

  var bytes = window.atob(uri.split(',')[1]); // data

  var bl = bytes.length;
  var buf = new ArrayBuffer(bl); // bytes.length

  var arr = new Uint8Array(buf);

  for (var i = 0; i < bl; i++) {
    arr[i] = bytes.charCodeAt(i);
  }

  var blob = new Blob([arr], {
    type: type
  });
  return {
    blob: blob,
    uri: uri,
    ext: ext
  };
}

/***/ }),

/***/ "./resources/js/src/utils/img/screenCaptureAPI.js":
/*!********************************************************!*\
  !*** ./resources/js/src/utils/img/screenCaptureAPI.js ***!
  \********************************************************/
/*! exports provided: screenCaptureAPI, getUserMediaAPI, displayMediaOptions, userMediaOptions, getDisplayMedia, getUserMedia, stopCapture */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "screenCaptureAPI", function() { return screenCaptureAPI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserMediaAPI", function() { return getUserMediaAPI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "displayMediaOptions", function() { return displayMediaOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userMediaOptions", function() { return userMediaOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDisplayMedia", function() { return getDisplayMedia; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserMedia", function() { return getUserMedia; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stopCapture", function() { return stopCapture; });
var _navigator, _navigator$mediaDevic, _navigator2, _navigator2$mediaDevi;

// Screen Capture API
var screenCaptureAPI = (_navigator = navigator) === null || _navigator === void 0 ? void 0 : (_navigator$mediaDevic = _navigator.mediaDevices) === null || _navigator$mediaDevic === void 0 ? void 0 : _navigator$mediaDevic.getDisplayMedia; // For detect / checking support

var getUserMediaAPI = (_navigator2 = navigator) === null || _navigator2 === void 0 ? void 0 : (_navigator2$mediaDevi = _navigator2.mediaDevices) === null || _navigator2$mediaDevi === void 0 ? void 0 : _navigator2$mediaDevi.getUserMedia;
/* {
  "aspectRatio": 1.7777777777777777,
  "deviceId": "screen:1:0",
  "frameRate": 30,
  "height": 720,
  "resizeMode": "crop-and-scale",
  "width": 1280,
  "cursor": "always",
  "displaySurface": "monitor",
  "logicalSurface": true
} */

var displayMediaOptions = {
  video: {
    cursor: "never" // always | motion | never | ["motion", "always"]
    // cursor: {
    // exact: "none"
    // }

  },
  audio: false
};
var userMediaOptions = {
  // constraints
  video: true // audio: false

};

function getDisplayMedia() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : displayMediaOptions;
  // User
  // let captureStream = null;
  return navigator.mediaDevices.getDisplayMedia(options); // reader["readAs" + readAs](file);
  // return navigator.mediaDevices[`get${type}Media`](mediaOptions);
}

function getUserMedia() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : userMediaOptions;
  return navigator.mediaDevices.getUserMedia(options);
}

function stopCapture(video) {
  // evt
  var tracks = video.srcObject.getTracks();
  tracks.forEach(function (track) {
    return track.stop();
  });
  video.srcObject = null;
} // function dumpOptionsInfo(video){ // , cb
// 	const videoTrack = video.srcObject.getVideoTracks()[0];
// 	const trackSettings = JSON.stringify(videoTrack.getSettings(), null, 2);
// 	const getConstraints = JSON.stringify(videoTrack.getConstraints(), null, 2);
// 	console.log("%cvideoTrack:","color:yellow", videoTrack);
// 	console.log("%cTrack settings:","color:yellow", trackSettings);
// 	console.log("%cTrack constraints:","color:yellow", getConstraints);
// 	// cb({ trackSettings, getConstraints });
// }




/***/ })

}]);
//# sourceMappingURL=Devs~ManifestGeneratorPage.chunk.js.map