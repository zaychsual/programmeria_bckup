(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Register"],{

/***/ "./resources/js/src/pages/public/Register.js":
/*!***************************************************!*\
  !*** ./resources/js/src/pages/public/Register.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Register; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var yup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! yup */ "./node_modules/yup/es/index.js");
/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! formik */ "./node_modules/formik/dist/formik.esm.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _context_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../context/AuthContext */ "./resources/js/src/context/AuthContext.js");
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/q-ui-react/Form */ "./resources/js/src/components/q-ui-react/Form.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../components/q-ui-react/InputQ */ "./resources/js/src/components/q-ui-react/InputQ.js");
/* harmony import */ var _components_q_ui_react_Password__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/q-ui-react/Password */ "./resources/js/src/components/q-ui-react/Password.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // { , useEffect }


 // useFormik | Formik


 // useHistory, 
// import Popover from 'antd/lib/popover';
// import FormCheck from 'react-bootstrap/FormCheck';








 // import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';

var CARD_CLASS = "card p-3 w-350px w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";
var initialValues = {
  email: "",
  // eve.holt@reqres.in
  username: "",
  password: "" // pistol
  // confirmPassword: "", 
  // term: false

};
function Register() {
  // let history = useHistory();
  var auth = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_AuthContext__WEBPACK_IMPORTED_MODULE_5__["AuthContext"]);
  var isLog = auth.localAuth(); // const [isLog, setLog] = useState(auth.localAuth());

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      ok = _useState2[0],
      setOk = _useState2[1]; //  | false
  // useEffect(() => {
  // 	console.log('%cuseEffect in Register','color:yellow;');
  // }, []);


  var validationSchema = yup__WEBPACK_IMPORTED_MODULE_1__["object"]().shape({
    email: yup__WEBPACK_IMPORTED_MODULE_1__["string"]() // .label('Email')
    .email("Wrong email format") // .max(50, "Maximum 50 symbols")
    .required("Email is required"),
    username: yup__WEBPACK_IMPORTED_MODULE_1__["string"]().min(3, "Minimum 3 symbols").max(55, "Maximum 55 symbols").required("Username is required"),
    password: yup__WEBPACK_IMPORTED_MODULE_1__["string"]().min(5, "Minimum 5 symbols").required("Password is required") // confirmPassword: Yup.string()
    // 	// 
    // 	// .min(5, "Minimum 5 symbols")
    // 	// .required()
    // 	// .required('Confirm Password must same'),
    // 	.test('passwords-match', 'Password must match', v => v === Yup.ref("password")),
    // term: Yup.bool().oneOf([true], 'Terms & Conditions must be accepted')

  });
  var formik = Object(formik__WEBPACK_IMPORTED_MODULE_2__["useFormik"])({
    initialValues: initialValues,
    validationSchema: validationSchema,
    // () => Schema
    onSubmit: function onSubmit(values, _ref) {
      var setSubmitting = _ref.setSubmitting;
      // setStatus, 
      console.log('values: ', values); // console.log('setStatus: ',setStatus);
      // setLoad(true);
      // setTimeout(() => {
      // https://reqres.in/api/register

      axios__WEBPACK_IMPORTED_MODULE_3___default.a.post('/api/register', {
        email: values.email,
        name: values.username,
        password: values.password // level: 2 // 1 = member, 2 = admin

      }).then(function (r) {
        var _r$data;

        console.log('r: ', r); // setSubmitting(false);

        if (r.status === 200 && ((_r$data = r.data) === null || _r$data === void 0 ? void 0 : _r$data.access_token)) {
          // token
          // console.log('r.data: ', r.data);
          // sessionStorage.setItem('auth', JSON.stringify(r.data));
          // history.replace("/");// Redirect to Home
          setOk(true);
        } // else{
        // 	setLoad(false);
        // }

      })["catch"](function (e) {
        console.log('e: ', e);
        setSubmitting(false); // setLoad(false);
      }); // }, 9);
    }
  }); // console.log('formik: ', formik);
  // console.log('formik getFieldProps: ', formik.getFieldProps("email"));

  if (isLog) return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Redirect"], {
    to: "/"
  });

  if (ok) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__["default"], {
      dir: "column",
      justify: "center",
      align: "center",
      className: "h-full-nav"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "Register Success"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__["default"], {
      to: "/login",
      btn: "primary"
    }, "Login"));
  } // d-grid place-center


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__["default"], {
    dir: "column",
    justify: "center",
    align: "center",
    className: "container-fluid py-3 mh-full-navmain"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_6__["default"], {
    title: "Register"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: CARD_CLASS
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "h4 hr-h mb-3"
  }, "Register"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_7__["default"], {
    noValidate: true,
    valid: formik.touched // className={"position-relative" + (load ? " i-load bg-size-90px-after cwait" : "")}  
    // className={formik.isValid ? undefined : "was-validated"} 
    // className={
    // 	Q.Cx({
    // 		"was-validated": Object.keys(formik.touched).length > 0, 
    // 		"cwait": load
    // 	})
    // } 
    ,
    disabled: formik.isSubmitting // isProgress
    ,
    onSubmit: formik.handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_11__["default"], {
    wrap: true,
    label: "Email",
    type: "email",
    name: "email",
    required: true // pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 
    ,
    value: formik.values.email,
    onChange: formik.handleChange // {...formik.getFieldProps("email")} 

  }, formik.touched.email && formik.errors.email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "invalid-feedback"
  }, formik.errors.email)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_11__["default"], {
    wrap: true,
    label: "Username",
    name: "username",
    required: true,
    pattern: ".{3,}",
    spellCheck: false,
    value: formik.values.username,
    onChange: formik.handleChange
  }, formik.touched.username && formik.errors.username && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "invalid-feedback"
  }, formik.errors.username)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Password__WEBPACK_IMPORTED_MODULE_12__["default"], {
    className: "mb-0",
    label: "Password",
    type: "password",
    name: "password",
    required: true // minLength={5} 
    ,
    pattern: ".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 
    ,
    value: formik.values.password,
    onChange: formik.handleChange
  }), formik.touched.password && formik.errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "invalid-feedback d-block"
  }, formik.errors.password), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__["default"], {
    align: "start",
    className: "mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "small mr-2"
  }, "By register, you agree to our Terms, Data Policy and Cookies Policy.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__["default"], {
    to: "/terms-and-conditions",
    kind: "dark" // secondary
    ,
    className: "d-inline-block mt-2 fa fa-info-circle" // 
    ,
    disabled: formik.isSubmitting,
    tabIndex: "-1"
  }, " Read terms & conditions")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
    type: "submit"
  }, "Register")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: CARD_CLASS + " d-block mt-2 text-center text-sm"
  }, "Have an account? ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__["default"], {
    to: "/login"
  }, "Login")));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=Register.chunk.js.map