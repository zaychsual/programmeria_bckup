(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Login"],{

/***/ "./resources/js/src/pages/public/Login.js":
/*!************************************************!*\
  !*** ./resources/js/src/pages/public/Login.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Login; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var yup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! yup */ "./node_modules/yup/es/index.js");
/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! formik */ "./node_modules/formik/dist/formik.esm.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _context_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../context/AuthContext */ "./resources/js/src/context/AuthContext.js");
/* harmony import */ var _components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/q-ui-react/Head */ "./resources/js/src/components/q-ui-react/Head.js");
/* harmony import */ var _components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/q-ui-react/Form */ "./resources/js/src/components/q-ui-react/Form.js");
/* harmony import */ var _components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/q-ui-react/Flex */ "./resources/js/src/components/q-ui-react/Flex.js");
/* harmony import */ var _components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/q-ui-react/Aroute */ "./resources/js/src/components/q-ui-react/Aroute.js");
/* harmony import */ var _components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/q-ui-react/Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
/* harmony import */ var _components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../components/q-ui-react/InputQ */ "./resources/js/src/components/q-ui-react/InputQ.js");
/* harmony import */ var _components_q_ui_react_Password__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/q-ui-react/Password */ "./resources/js/src/components/q-ui-react/Password.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // useEffect, Fragment


 // useFormik | Formik


 // useHistory, 
// import Modal from 'antd/es/modal';
// import { Checkbox } from 'antd';

 // import Cookie from '../../utils/cookie';






 // import InputGroup from '../../components/q-ui-react/InputGroup';

 // import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';
// "card p-3 w-25 w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";

var CARD_CLASS = "card p-3 w-350px w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";
var initialValues = {
  email: "",
  // eve.holt@reqres.in
  password: "",
  remember: false
};

function toggleFocus(on) {
  // Disabled / Enable all focused element
  Q.domQall(Q.FOCUSABLE).forEach(function (n) {
    return on ? Q.setAttr(n, "tabindex") : n.tabIndex = "-1";
  });
}

var expiresAt = 60 * 24;
function Login() {
  var auth = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_AuthContext__WEBPACK_IMPORTED_MODULE_5__["AuthContext"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(),
      _useState2 = _slicedToArray(_useState, 2),
      loginError = _useState2[0],
      setLoginError = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(auth.localAuth()),
      _useState4 = _slicedToArray(_useState3, 2),
      isLog = _useState4[0],
      setIsLog = _useState4[1]; // auth.isAuthenticated() | false
  // const [load, setLoad] = useState(false);
  // const [seePass, setSeePass] = useState(false);
  // useEffect(() => {
  // 	// console.log('%cuseEffect in Login','color:yellow;', isLog);
  // 	auth.isAuthenticated().then(v => {
  // 		// console.log(v);
  // 		setIsLog(v);
  // 	}).catch(e => console.log(e));
  // }, []);
  // let history = useHistory();


  var validationSchema = yup__WEBPACK_IMPORTED_MODULE_1__["object"]().shape({
    email: yup__WEBPACK_IMPORTED_MODULE_1__["string"]().email("Wrong email format") // .max(50, "Maximum 50 symbols")
    .required("Email is required"),
    // .required(
    //   intl.formatMessage({
    //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
    //   })
    // ),
    password: yup__WEBPACK_IMPORTED_MODULE_1__["string"]().min(5, "Minimum 5 symbols") // .max(50, "Maximum 50 symbols")
    .required("Password is required")
  }); // const onLogin = async credentials => {
  // 	try {
  // 		// setLoginLoading(true);
  // 		const req = await axios.post("/api/login", credentials);// { data }
  // 		console.log('req: ', req);
  // 		if(req.status === 200){
  // 			if(req.data?.user){
  // 				auth.setAuthState(req.data);
  // 				// setLoginSuccess(data.message);
  // 				setLoginError(null);
  // 				setTimeout(() => {
  // 					setIsLog(true);
  // 				}, 700);
  // 			}
  // 			else{
  // 				console.log('message: ', req.data.message);
  // 				setLoginError(req.data.message);
  // 				toggleFocus(1);
  // 				Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
  // 			}
  // 		}
  // 	} catch (e) {
  // 		// setLoginLoading(false);
  // 		const { data } = e.response;
  // 		setLoginError(data.message);
  // 		// setLoginSuccess(null);
  // 	}
  // }

  var formik = Object(formik__WEBPACK_IMPORTED_MODULE_2__["useFormik"])({
    initialValues: initialValues,
    validationSchema: validationSchema,
    // () => Schema
    onSubmit: function onSubmit(values, _ref) {
      var setSubmitting = _ref.setSubmitting;
      // setStatus, 
      // setSubmitting(true);
      console.log('values: ', values); // Disabled all focused element

      toggleFocus();
      Q.setClass(Q.domQ('#QloadStartUp'), 'd-none', 'remove'); // setLoad(true);
      // onLogin(values);
      // setTimeout(() => {
      // login | https://reqres.in/api/login

      axios__WEBPACK_IMPORTED_MODULE_3___default.a.post('/api/login', values).then(function (r) {
        console.log('r: ', r);
        setSubmitting(false);

        if (r.status === 200) {
          var _r$data;

          if ((_r$data = r.data) === null || _r$data === void 0 ? void 0 : _r$data.user) {
            console.log('r.data: ', r.data); // sessionStorage.setItem('auth', JSON.stringify(r.data));
            // history.replace("/");// Redirect to Home

            if (values.remember) {
              var date = new Date();
              date.setTime(date.getTime() + expiresAt * 60 * 1000); // Cookie.set("access_token", r.data.token, { path: "/", expires: date });

              auth.setAuthState(_objectSpread(_objectSpread({}, r.data), {}, {
                expired: date
              })); // r.data
            } else {
              // Cookie.set("access_token", r.data.token, { path: "/" });
              auth.setAuthState(r.data);
            } // auth.setAuthState(r.data);
            // setLoginSuccess(data.message);


            setLoginError(null); // setTimeout(() => {

            setIsLog(true); // }, 700);
          } else {
            // setLoad(false);
            // toggleFocus(1);
            // Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
            console.log('message: ', r.data.message);
            setLoginError(r.data.message);
          }
        }

        toggleFocus(1);
        Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
      })["catch"](function (e) {
        console.log('e: ', e);
        toggleFocus(1);
        Q.setClass(Q.domQ('#QloadStartUp'), 'd-none'); // setLoad(false);

        setSubmitting(false);
      }); // }, 2000);
    }
  }); // console.log('formik: ', formik);
  // console.log('isAuthenticated: ', auth.isAuthenticated());

  if (isLog) return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Redirect"], {
    to: "/"
  }); // d-grid place-center

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__["default"], {
    dir: "column",
    justify: "center",
    align: "center",
    className: "container-fluid py-3 mh-full-navmain"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Head__WEBPACK_IMPORTED_MODULE_6__["default"], {
    title: "Login"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: CARD_CLASS
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "h4 hr-h mb-3"
  }, "Login"), loginError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "alert alert-danger"
  }, loginError), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Form__WEBPACK_IMPORTED_MODULE_7__["default"], {
    noValidate: true,
    valid: formik.touched // className={load ? "cwait":undefined} 
    // className={formik.isValid ? undefined:"was-validated"} 
    // className={formik.isSubmitting ? "i-load" : undefined} 
    // fieldsetClass="text-center" 
    ,
    disabled: formik.isSubmitting // isProgress
    ,
    onSubmit: formik.handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_InputQ__WEBPACK_IMPORTED_MODULE_11__["default"], {
    wrap: true,
    label: "Email",
    type: "email",
    name: "email",
    required: true // pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 
    ,
    value: formik.values.email,
    onChange: formik.handleChange // {...formik.getFieldProps("email")} 

  }, formik.touched.email && formik.errors.email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "invalid-feedback"
  }, formik.errors.email)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Password__WEBPACK_IMPORTED_MODULE_12__["default"], {
    className: "mb-0",
    label: "Password",
    type: "password",
    name: "password",
    required: true // minLength={5} 
    ,
    pattern: ".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 
    ,
    value: formik.values.password,
    onChange: formik.handleChange
  }), formik.touched.password && formik.errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "invalid-feedback d-block"
  }, formik.errors.password), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Flex__WEBPACK_IMPORTED_MODULE_8__["default"], {
    justify: "between",
    align: "center",
    className: "my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "custom-control custom-checkbox"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    onChange: formik.handleChange,
    type: "checkbox",
    className: "custom-control-input",
    id: "remember"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "custom-control-label text-sm",
    htmlFor: "remember"
  }, "Remember me")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Btn__WEBPACK_IMPORTED_MODULE_10__["default"], {
    type: "submit"
  }, "Login")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__["default"], {
    to: "/forgot-password",
    kind: "secondary",
    className: "text-sm fa fa-question-circle" // disabled={formik.isSubmitting} 

  }, " Forgot password?"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: CARD_CLASS + " d-block mt-2 text-center text-sm"
  }, "Don't have account? ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_q_ui_react_Aroute__WEBPACK_IMPORTED_MODULE_9__["default"], {
    to: "/register"
  }, "Register")));
}
/*
<Fragment></Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=Login.chunk.js.map