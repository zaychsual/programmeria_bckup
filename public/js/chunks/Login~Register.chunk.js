(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Login~Register"],{

/***/ "./resources/js/src/components/q-ui-react/Form.js":
/*!********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Form.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Form; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import {findDOMNode} from "react-dom";
// import { Cx, toggleClass } from '../../utils/Q';
// wrapClass, upload, id, encType

function Form(_ref) {
  var inRef = _ref.inRef,
      className = _ref.className,
      fieldsetClass = _ref.fieldsetClass,
      noValidate = _ref.noValidate,
      valid = _ref.valid,
      disabled = _ref.disabled,
      onSubmit = _ref.onSubmit,
      children = _ref.children,
      etc = _objectWithoutProperties(_ref, ["inRef", "className", "fieldsetClass", "noValidate", "valid", "disabled", "onSubmit", "children"]);

  // React.useEffect(() => {
  // console.log('%cuseEffect in Form','color:yellow;');
  // console.log(getEl());
  // console.log(findDOMNode(Form));
  // }, []);

  /*const getEl = () => { // getElement
  	try{
  		// using findDOMNode for two reasons:
  		// 1. cloning to insert a ref is unwieldy and not performant.
  		// 2. ensure that we resolve to an actual DOM node (instead of any JSX ref instance).
  		return findDOMNode(this);
  	}catch{
  		return null;// swallow error if findDOMNode is run on unmounted component.
  	}
  }*/
  var Submit = function Submit(e) {
    if (disabled) return;
    var et = e.target; // Prevent submit form if use xhr

    if (noValidate) {
      e.preventDefault();
      e.stopPropagation(); // console.log(!Q.isBool(valid));
      // if(!Q.isBool(valid)) 

      if (!Q.isBool(valid)) {
        et.classList.toggle("was-validated", !et.checkValidity());
      }
    }

    if (onSubmit && !disabled) onSubmit(e, et.checkValidity()); // onSubmit to props
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", _extends({}, etc, {
    ref: inRef,
    className: Q.Cx('form-q', {
      "disabled": disabled,
      // Q.isBool(valid) //  && !isValid
      "was-validated": noValidate && valid && Object.keys(valid).length > 0
    }, className) // enctype values: (OPTION)
    // 1. application/x-www-form-urlencoded (Default)
    // 2. multipart/form-data (form upload)
    // 3. text/plain (Spaces are converted to "+" symbols, but no special characters are encoded)
    // encType={upload ? 'multipart/form-data' : encType} 
    //data-reset={reset} 
    ,
    noValidate: noValidate // noValidate ? true : undefined
    ,
    onSubmit: Submit
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("fieldset", {
    disabled: disabled,
    className: fieldsetClass
  }, children));
}

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/InputQ.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/InputQ.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InputQ; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // className:

var FC = "form-control";
var CS = "custom-select";
function InputQ(_ref) {
  var _ref$WrapAs = _ref.WrapAs,
      WrapAs = _ref$WrapAs === void 0 ? "label" : _ref$WrapAs,
      _ref$As = _ref.As,
      As = _ref$As === void 0 ? "input" : _ref$As,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "text" : _ref$type,
      wrap = _ref.wrap,
      inRef = _ref.inRef,
      children = _ref.children,
      disabled = _ref.disabled,
      onChange = _ref.onChange,
      classNames = _ref.classNames,
      wrapProps = _ref.wrapProps,
      label = _ref.label,
      qSize = _ref.qSize,
      etc = _objectWithoutProperties(_ref, ["WrapAs", "As", "type", "wrap", "inRef", "children", "disabled", "onChange", "classNames", "wrapProps", "label", "qSize"]);

  // const [data, setData] = React.useState();
  // React.useEffect(() => {
  // 	console.log('%cuseEffect in Input','color:yellow;');
  // }, []);
  var isInput = As === "input";

  var Change = function Change(e) {
    if (disabled) {
      e.preventDefault();
      return;
    }

    if (onChange && !disabled) onChange(e);
  };

  var Input = function Input() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(As, _extends({}, etc, {
      ref: inRef,
      className: Q.Cx(isInput ? FC : CS, qSize && (isInput ? FC + "-" + qSize : CS + "-" + qSize) // isValid ? "is-valid" : "is-invalid"
      // [isValid ? "is-valid" : "is-invalid"] 
      ),
      type: isInput ? type : undefined,
      disabled: disabled,
      onChange: Change
    }));
  };

  if (wrap) {
    // children
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(WrapAs, _extends({}, wrapProps, {
      className: Q.Cx("q-input", classNames),
      "aria-label": label // htmlFor={WrapAs === "label" ? htmlFor : undefined} 

    }), Input(), children);
  }

  return Input();
}
/*
<React.Fragment></React.Fragment>
*/

/***/ }),

/***/ "./resources/js/src/components/q-ui-react/Password.js":
/*!************************************************************!*\
  !*** ./resources/js/src/components/q-ui-react/Password.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Password; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _InputGroup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./InputGroup */ "./resources/js/src/components/q-ui-react/InputGroup.js");
/* harmony import */ var _InputQ__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./InputQ */ "./resources/js/src/components/q-ui-react/InputQ.js");
/* harmony import */ var _Btn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Btn */ "./resources/js/src/components/q-ui-react/Btn.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // { useState, useEffect }




function Password(_ref) {
  var className = _ref.className,
      label = _ref.label,
      etc = _objectWithoutProperties(_ref, ["className", "label"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(),
      _useState2 = _slicedToArray(_useState, 2),
      see = _useState2[0],
      setSee = _useState2[1]; // useEffect(() => {
  // 	console.log('%cuseEffect in Password','color:yellow;');
  // }, []);


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: Q.Cx("q-input", className),
    "aria-label": label
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_InputGroup__WEBPACK_IMPORTED_MODULE_1__["default"], {
    As: "div",
    append: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Btn__WEBPACK_IMPORTED_MODULE_3__["default"], {
      As: "div",
      kind: "light",
      tabIndex: "0",
      className: "tip tipTR qi q-fw qi-eye" + (see ? "-slash" : ""),
      "aria-label": (see ? "Hide" : "Show") + " Password",
      onClick: function onClick() {
        return setSee(!see);
      }
    })
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_InputQ__WEBPACK_IMPORTED_MODULE_2__["default"] // wrap 
  , _extends({}, etc, {
    label: label // "Password" 
    ,
    type: see ? "text" : "password" // name="password" 
    // required 
    // minLength={5} 
    // pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 
    // OPTION: From Gmail, toggle or default render
    // spellCheck={see ? false : undefined} 
    // autoComplete={see ? "off" : undefined} 
    // autoCapitalize={see ? "off" : undefined} 
    ,
    spellCheck: false,
    autoComplete: "off",
    autoCapitalize: "off" // value={formik.values.password} 
    // onChange={formik.handleChange}

  }))));
}
/*
<React.Fragment></React.Fragment>
*/

/***/ })

}]);
//# sourceMappingURL=Login~Register.chunk.js.map