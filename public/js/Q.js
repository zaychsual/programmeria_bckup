// 
window.URL =  window.URL || window.webkitURL;

// 
let Q_appData = {
	UA: bowser.parse(navigator.userAgent),
	// cached(fn){
	// 	let cache = Object.create(null);
	// 	return (function cachedFn(s){
	// 		let hit = cache[s];
	// 		return hit || (cache[s] = fn(s))
	// 	})
	// }, 
};// null

const Q = Object.freeze({
	/** Check support native lazy loading image, iframe */
	// const SUPPORT_LOADING = 'loading' in HTMLImageElement.prototype;
	baseURL: location.origin,
	// UA: bowser.parse(navigator.userAgent),
	lang(){
		let l = navigator.language;
		return l[0] + l[1];
	},
	setUpDOM(){
		// Q.UA
		// let html = document.documentElement;
		// let cls = Q_appData.UA.platform.type + " " + Q_appData.UA.os.name + " " + Q_appData.UA.browser.name;
		// Q.setClass(html, cls, cls.includes(html.className) ? "remove" : "add");

		// Q.setClass(html, Q_appData.UA.platform.type + " " + Q_appData.UA.os.name + " " + Q_appData.UA.browser.name);
		
		Q.setAttr(document.documentElement, {
			"data-platform": Q_appData.UA.platform.type, 
			"data-os": Q_appData.UA.os.name,
			"data-browser": Q_appData.UA.browser.name, 

			"data-os-v": Q_appData.UA.os.versionName || Q_appData.UA.os.version,
			"data-browser-v": Q_appData.UA.browser.version
		});
	},
	/** GET favicon offline file */
	// faviconOffline(){
	// 	return localStorage.getItem('favicon-offline');
	// }, 

	PAGE_FULL_NAV:'calc(100vh - 48px)', 
	FOCUSABLE: 'a[href], area[href], input:not([disabled]):not([type="hidden"]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), object, embed, [tabindex="0"], audio[controls], video[controls], [contenteditable="true"]', 
	DD_BTN: {
		as:"button", type:"button"
	},
	// cookie(k, options, fn = 'get'){
	// 	let cookie = new UniversalCookie();
	// 	return cookie[fn](k);
	// },

	// https://medium.com/javascript-in-plain-english/you-must-understand-these-14-javasript-functions-1f4fa1c620e2
	/* function cached(fn){
	// Create an object to store the results returned after each function execution.
		const cache = Object.create(null);
		// Returns the wrapped function
		return function cachedFn(str){
			// If the cache is not hit, the function will be executed
			if(!cache[str]){
				let result = fn(str);
				// Store the result of the function execution in the cache
				cache[str] = result;
			}
			return cache[str]
		}
	} */

	/** FROM: Vue.js */
	cached(fn){
		let cache = Object.create(null);
		return (function cachedFn(s){
			let hit = cache[s];
			return hit || (cache[s] = fn(s))
		})
	}, 
	/** === Type checking === */
	isStr(v){
		return typeof v === 'string' || v instanceof String;
	}, 
	isObj(v){
		return v && typeof v === 'object' && v.constructor === Object;
	}, 
	isFunc(v){
		return typeof v === 'function';/* v && typeof v === 'function' */
	}, 
	isBool(v){
		return typeof v === "boolean";
	}, 
	/** Q classnames */
	Cx(){
		let hasOwn = {}.hasOwnProperty,
				c = [],
				alength = arguments.length;
		for(let i = 0; i < alength; i++){
			let arg = arguments[i];
			if(!arg) continue;

			/* let argType = typeof arg; */
			if(Q.isStr(arg) || typeof arg === 'number'){
				c.push(arg);
			}else if(Array.isArray(arg) && arg.length){
				let inr = Cx.apply(null, arg);
				if(inr) c.push(inr);
			}else if(Q.isObj(arg)){
				for(let k in arg){
					if(hasOwn.call(arg, k) && arg[k]) c.push(k);
				}
			}
		}
		return c.length > 0 ? c.join(' ') : undefined;
	}, 
	/** FROM: blueprint.js utils - Safely invoke the function with the given arguments, if it is indeed a
	 * function, and return its value. Otherwise, return undefined */
	/* function safeInvoke(fn, ...args){
		if(isFunc(fn)){
			return fn(...args);
		}
		return undefined;
	} */
	/** === END Type checking === */

	/** == dom-q.js === */
	domQ(q, dom = document){
		return dom.querySelector(q);
	}, 
	/** DEV OPTIONS: only this / return array with check length */
	domQall(q, dom = document){
		return dom.querySelectorAll(q);
	}, 
	/** USAGE:
		add = setClass(element, "btn active");
		remove = setClass(element, "btn active", 'remove'); */
	setClass(el, c, fn = 'add'){
		let cls = c.split(" ");
		el.classList[fn](...cls);
	}, 
	// toggleClass(el, c, cek){
	//   el.classList.toggle(c, cek);
	// }, 
	replaceClass(el, o, n){
		hasClass(el, o) ? el.classList.replace(o, n) : el.classList.replace(n, o);
	}, 
	hasClass(el, c){
		return el.classList.contains(c);
	}, 
	hasAttr(el, a){
		return el.hasAttribute(a);
	}, 
	getAttr(el, a){
		return el.getAttribute(a);
	}, 
	/**
		@el : Element / node
		@attr : attribute name & value (Object)
	*/
	setAttr(el, attr){
		if(el){
			if(Q.isObj(attr)) Object.entries(attr).forEach(v => el.setAttribute(v[0], v[1]));
			else if(Q.isStr(attr)) attr.split(" ").forEach(v => el.removeAttribute(v));
			else console.warn('setAttr() : params 2 required Object to add / string to remove, To remove several attributes, separate the attribute names with a space.');
		}else{
			console.warn('setAttr() : params 1 required DOM');
		}
	},
	makeEl(t){
		return document.createElement(t);
	}, 
	/** === END dom-q.js === */

	/** === Generate id === */
	// Qid(){
	// 	function S4(){
	// 		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
	// 	}
	// 	return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + Date.now())
	// }, 
	// OPTION
	// 1.0e+9
	Qid(l = 4){
		// let a = new Uint32Array(l); //  Int8Array | Uint8Array | Int16Array | Uint16Array | Int32Array | Uint32Array
		return String.fromCharCode(97 + Number(l)) + "-" + window.crypto.getRandomValues(new Uint32Array(l)).join("-");
	}, 
	/** === END Generate id === */

	/** No Action */
	preventQ(e){
		e.preventDefault();
		e.stopPropagation();
	}, 
	noop(){}, 
	/** END No Action */

	/** Bind multiple component methods:
		* @param {this} context
		* @param {Array} functions
		constructor(){
			...
			bindFuncs.call(this,['onClick','onModal']);
		} */
	bindFuncs(fns){
		fns.forEach(f => (this[f] = this[f].bind(this)));
	}, 

	/** reactstrap utils */
	omit(obj, omitKeys){
		let res = {};
		Object.keys(obj).forEach(k => {
			if(omitKeys.indexOf(k) === -1) res[k] = obj[k];
		});
		return res;
	}, 
	// mapToCssModules(className = "", cssModule){ // className = "", cssModule = globalCssModule
	// 	if(!cssModule) return className;
	// 	return className.split(" ").map(c => cssModule[c] || c).join(" ");
	// },
	// isReactRefObj(target){
	// 	// typeof target === "object" | this.isObj(target)
	// 	if(target && typeof target === "object"){
	// 		return "current" in target;
	// 	}
	// 	return false;
	// },
	/** END reactstrap utils */
	/* Check absolute URL */
	isAbsoluteUrl(url){
		try{
			new URL(url);
			return true;// OPTION: return new URL(url);
		}catch(err){
			return false;
		}
	},
	newURL(path = "", url = window.location.origin){
		// if(isAbsoluteUrl(url)) return url + path;
		// return new URL(path, url).href; // OPTION: only return string URL in key href
	
		if(Q.isAbsoluteUrl(url)) return new URL(path, url); // OPTION: only return string URL in key href
	},
	urlSearch(url = window.location.search){// k = 'id', url = window.location.search
		return new URLSearchParams(url);// .get(k);
	},
	// isDomain(s){
  // 	let regexp = /^(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
	// 	if(regexp.test(s)){
	// 		return true;
	// 	}
	// 	return false;
	// }, 

	// getCookie(name){
	// 	// try{
	// 	// 	return document.cookie.split('; ').find(row => row.startsWith(name)).split('=')[1];
	// 	// }catch(e){
	// 	// 	return false;
	// 	// }
	// 	let c = document?.cookie.match(new RegExp("(?:^|; )" + name?.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	// 	return c ? decodeURIComponent(c[1]) : null;
	// }, 

	getScript(obj, to = "body"){
		return new Promise((resolve, reject) => {
			// let srcCache = Q.cached(url => url.src || url.href);
			// console.log('srcCache: ', srcCache(obj));

			let el = Q.isStr(to) ? document[to] : to, 
					urlSrc = Q.newURL(obj.src || obj.href).href, // Q.newURL(srcCache(obj)).href
					// As = "stylesheet preload".includes(obj.rel) && (urlSrc.endsWith('.css') || obj.type === "text/css" || obj.as === "style") ? 'link' : 'script',
					tag = obj.tag ? obj.tag : 'script';
			
			/* MAKE SURE dom available */
			if(Q.domQ(`${tag}[${tag === 'link' ? 'href':'src'}="${urlSrc}"]`, el)){
				if(Q.isFunc(obj.callbackReady)) obj.callbackReady();// Callback if script is ready in DOM
				return;
				// resolve(); // (urlSrc)
			}
			
			let dom = Q.makeEl(tag);// document.createElement(tag);
			function loadend(){
				dom.onerror = dom.onload = null;
			}
			dom.onload = function(e){
				loadend();
				resolve({e, dom});
				window.onerror = null;/* OPTION */
			}
			dom.onerror = function(){
				loadend();
				dom.remove();
				reject(new Error('Failed to load '+ urlSrc));
			}

			window.onerror = function(msg, url, line, column, errObj){
				let str = msg.toLowerCase(),
						scriptErr = "script error";
				if(str.indexOf(scriptErr) > -1){
					dom.remove();
					reject({
						msg: 'Script Error: See browser console for detail',
						path: urlSrc /* OPTION */
					});
				}else{
					dom.remove();
					reject({
						msg, url, line, column,
						errorObj: JSON.stringify(errObj),
						path: urlSrc /* OPTION */
					});
				}
				window.onerror = null;
				return false;
			}
			
			/* Set more valid attributes to script tag */
			if(obj.src) obj.src = urlSrc;
			if(obj.href) obj.href = urlSrc;
			Q.setAttr(dom, Q.omit(obj, ['tag','callbackReady','async','crossOrigin','innerText','innerHTML']));// , 'crossOrigin', 'src','text','onerror','onload'
			/* // Available attributes script tag
			{
				type: "",
				noModule: false,
				async: true,
				defer: false,
				crossOrigin: null,
				text: "",
				referrerPolicy: null,
				event: "",
				integrity: ""
			} */
			
			// let inHTML = obj.innerHTML;/* !obj.innerText && !obj.innerHTML */
			/* if(obj.innerText) dom.innerText = obj.innerText; */
			// if(inHTML) dom.innerHTML = inHTML;
			
			if(tag !== 'link' && !Q.isBool(obj.async)) dom.async = 1;
			if(!obj.crossOrigin || tag !== 'style') dom.crossOrigin = "anonymous";
			el.appendChild(dom);
		});
	}, 

	// loadJs(url){
  //   let s = Q.makeEl("script");
  //   s.src = url;
  //   document.body.appendChild(s); // head
  //   return new Promise((resolve, reject) => {
	// 		s.addEventListener("load", () => {
	// 			resolve(s);
	// 		});
	// 		s.addEventListener("error", () => {
	// 			reject(new Error("Can not load: " + url));
	// 		});
  //   });
	// }, 

	// importJS: Q_appData.cached(url => Q.loadJs(url)), // return 

// HTMLElement.prototype.click = function(e){
// 	// this.addEventListener(e, cb);
// 	this.click();
// 	return this;
// }

// DEVS: Date to mysql timestamp format
// FROM: https://stackoverflow.com/questions/5129624/convert-js-date-time-to-mysql-datetime
// function twoDigits(d){
// 	if(0 <= d && d < 10) return "0" + d.toString();
// 	if(-10 < d && d < 0) return "-0" + (-1*d).toString();
// 	return d.toString();
// }
// Date.prototype.toMysqlFormat = function(){
// 	return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
// };

	dateObj(d){
		let dt = new Date(d);
		return {
			year: dt.getFullYear(),
			month: dt.getMonth(),
			date: dt.getDate(),
			day: dt.getDay(),
			hour: dt.getHours(),
			minute: dt.getMinutes(),
			seconds: dt.getSeconds(),
			milliseconds: dt.getMilliseconds()
		}
	}, 

	fileName(t){
		// .replace(/.png|.jpg|.svg|.gif|.jpeg|.PNG/,"")
		if(Q.isStr(t)){
			let fullname = t.split("/").pop(),
					ext = fullname.split('.').pop();

			return {
				name: fullname.replace(`.${ext}` ,""), 
				fullname,
				ext
			}
		}
	}, 

	str2slug(str = ""){
    str = str.replace(/^\s+|\s+$/g, '').toLowerCase(); // trim
  
    // remove accents, swap ñ for n, etc
    let from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    let to   = "aaaaeeeeiiiioooouuuunc------";
    for(let i=0, l=from.length ; i<l ; i++){
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
	}, 
	// https://gist.github.com/lanqy/5193417
	// https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
	bytes2Size(bytes, separator = ""){ // , postFix = ''
    if(bytes){
			let sizes = ['Bytes','KB','MB','GB','TB','PB','EB','ZB','YB'];
			let i = Math.min(parseInt(Math.floor(Math.log(bytes) / Math.log(1024)).toString(), 10), sizes.length - 1);
			// return `${(bytes / (1024 ** i)).toFixed(i ? 1 : 0)}${separator}${sizes[i]}${postFix}`;
			let no = separator === false;
			return `${(bytes / (1024 ** i)).toFixed(i ? 1 : 0)}${no ? "" : separator}${no ? "" : sizes[i]}`;// ${postFix}
    }
    return "n/a";
	}, 
	formikValidClass(formik, fieldname){
		let touch = formik.touched[fieldname];
		let err = formik.errors[fieldname];
		if(touch && err){
			return " is-invalid";
		}
		if(touch && !err){
			return " is-valid";
		}
		return "";
	},
	/* https://kentcdodds.com/blog/replace-axios-with-a-simple-custom-fetch-wrapper */
	// fetch(api, {body, ...config} = {}){
	// 	const headers = {'Content-Type': 'application/json'}
	// 	const ops = {
	// 		method: body ? 'POST' : 'GET',
	// 		...config,
	// 		headers: {
	// 			...headers,
	// 			...config.headers,
	// 		},
	// 	}
	// 	if(body){
	// 		ops.body = JSON.stringify(body)
	// 	}
	// 	// `${process.env.REACT_APP_API_URL}/${endpoint}`
	// 	return window
	// 		.fetch(Q.newURL(api).href, ops)
	// 		.then(async res => {
	// 			const data = await res.json();
	// 			if(res.ok){
	// 				return data;
	// 			}else{
	// 				return Promise.reject(data);
	// 			}
	// 		})
	// },

	// FOR check mobile device
	// isMobile(){
	//   return !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	// }

	/** Polyfill (Edge) */
	// typeof HTMLDetailsElement == "undefined" && getScripts({src:"/js/libs/polyfill/details.js","data-js":"details"}).then(e => console.log('+ details polyfill')).catch(e => console.warn(e));

	// !"srcdoc" in document.createElement("iframe") && getScripts({src:"/js/libs/polyfill/srcdoc.js","data-js":"srcdoc"}).then(v => console.log('+ srcdoc polyfill')).catch(e => console.warn(e));
	/** END Polyfill (Edge) */

	// function isSupportLocaleDateString(date){
		// try {
			// date.toLocaleDateString('i');// new Date().toLocaleDateString('i');
		// }catch(e){
			// return e.name === 'RangeError';
		// }
		// return false;
	// }

	// function isValidLang(lang){
	// 	if(!window.Intl) return;
	// 	try {
	// 		let supportLocOf = Intl.DateTimeFormat.supportedLocalesOf(lang, {localeMatcher:'lookup'});// lookup | best fit
	// 		return supportLocOf && Intl.getCanonicalLocales(lang) ? supportLocOf.length : false;
	// 	}catch(e){
	// 		console.warn(e.message);// expected output: RangeError: invalid language tag: lang
	//		return false;
	// 	}
	// }

	// function dateIntl(dt, options, lang = 'en'){
	// 	if(!dt || !isValidLang(lang)) return;// && !isSupportLocaleDateString(dt)

	// 	// console.log('isValidLang: ',!isValidLang(lang));
	// 	try {
	// 		let setOptions = {
	// 			weekday:'long', // long | short | narrow
	// 			year:'numeric', // numeric | 2-digit
	// 			month:'long', // numeric | 2-digit | long | short | narrow
	// 			day:'numeric', // numeric | 2-digit
	// 			// hour: '', // numeric | 2-digit
	// 			// minute: '', // numeric | 2-digit
	// 			// second: '', // numeric | 2-digit
	// 			...options
	// 		};
	// 		// console.log(setOptions);

	// 		return new Intl.DateTimeFormat(lang, setOptions).format(dt);
	// 	}catch(e){
	// 		console.warn(e.message);
	// 	}
	// }

	/* FROM ContentEditable.js */
	/* function normalizeHtml(str){
		return str && str.replace(/&nbsp;|\u202F|\u00A0/g, ' ');
	}
	function replaceCaret(el){
		// Place the caret at the end of the element
		let target = document.createTextNode('');
		el.appendChild(target);
		// do not move caret if element was not focused
		let isTargetFocused = document.activeElement === el;
		if(target !== null && target.nodeValue !== null && isTargetFocused){
			let sel = window.getSelection();
			if(sel !== null){
				let range = document.createRange();
				range.setStart(target, target.nodeValue.length);
				range.collapse(true);
				sel.removeAllRanges();
				sel.addRange(range);
			}
			if(el instanceof HTMLElement) el.focus();
		}
	} */

	/* function dateToStr(dt){
		let date = new Date(dt);
		return date.toDateString();
	} */

	/** Check html tagName **/
	/* function isTag(t){ // tagCheck
		// let c = document.createElement(t),
				// b = c.toString() !== "[object HTMLUnknownElement]"
		// return {valid:b, el:c};
		return document.createElement(t).toString() !== "[object HTMLUnknownElement]";
	} */

	/** Babel Helpers:  */
	// function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }
	// function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

	// function _interopRequireDefault(obj){
		// return obj && obj.__esModule ? obj : {default: obj};
	// }
	/** END Babel Helpers:  */

	// __esModule: true
});/** END Q */

// Object.freeze(Q);

