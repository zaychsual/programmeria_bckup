/** Details Element Polyfill 2.4.0 Copyright © 2019 Javan Makhmali 
https://github.com/javan/details-element-polyfill */
(function(){
"use strict";
  var element = document.createElement("details");
  var elementIsNative = typeof HTMLDetailsElement != "undefined" && element instanceof HTMLDetailsElement;
  var support = {
    open:"open" in element || elementIsNative,
    toggle:"ontoggle" in element
  };
  // var styles = '\ndetails,summary{display:block;}\ndetails:not([open]) > *:not(summary){display:none;}\nsummary::before{content:"►";padding-right:0.3rem;font-size:0.6rem;cursor:default;}[open] > summary::before{content:"▼";}';
  var styles = `details,summary{display:block}
details:not([open]) > *:not(summary){display:none}
summary:not(.q)::before{
	content:"►";
	padding-right:.3rem;
	font-size:.6rem;
	cursor:default
}
[open] > summary:not(.q)::before{content:"▼"}`;
	var _ref = [], 
			forEach = _ref.forEach, 
			slice = _ref.slice;
  if(!support.open){
    polyfillStyles();
    polyfillProperties();
    polyfillToggle();
    polyfillAccessibility();
  }
  if(support.open && !support.toggle){
    polyfillToggleEvent();
  }
  function polyfillStyles(){
    document.head.insertAdjacentHTML('afterbegin','<style id="cssDetailsPolyfill">'+styles+'</style>');
  }
  function polyfillProperties(){
    var prototype = document.createElement('details').constructor.prototype;
    var setAttribute = prototype.setAttribute, 
				removeAttribute = prototype.removeAttribute;
    var open = Object.getOwnPropertyDescriptor(prototype,"open");
    Object.defineProperties(prototype,{
      open:{
        get:function get(){
          if(this.tagName == "DETAILS"){
            return this.hasAttribute("open");
          }else{
            if(open && open.get){
              return open.get.call(this);
            }
          }
        },
        set:function set(val){
          if(this.tagName == "DETAILS"){
            return val ? this.setAttribute("open","") : this.removeAttribute("open");
          }else{
            if(open && open.set){
              return open.set.call(this,val);
            }
          }
        }
      },
      setAttribute:{
        value:function value(name, _value){
          var _this = this;
          var call = function call(){
            return setAttribute.call(_this, name, _value);
          };
          if(name == "open" && this.tagName == "DETAILS"){
            var wasOpen = this.hasAttribute("open");
            var result = call();
            if(!wasOpen){
              var summary = this.querySelector("summary");
              if(summary) summary.setAttribute("aria-expanded",true);
              triggerToggle(this);
            }
            return result;
          }
          return call();
        }
      },
      removeAttribute: {
        value:function value(name){
          var _this2 = this;
          var call = function call(){
            return removeAttribute.call(_this2,name);
          };
          if(name == "open" && this.tagName == "DETAILS"){
            var wasOpen = this.hasAttribute("open");
            var result = call();
            if(wasOpen){
              var summary = this.querySelector("summary");
              if(summary) summary.setAttribute("aria-expanded", false);
              triggerToggle(this);
            }
            return result;
          }
          return call();
        }
      }
    });
  }
  function polyfillToggle(){
    onTogglingTrigger(function(el){
      el.hasAttribute("open") ? el.removeAttribute("open") : el.setAttribute("open","");
    });
  }
  function polyfillToggleEvent(){
    if(window.MutationObserver){
      new MutationObserver(function(mutations){
        forEach.call(mutations,function(mutation){
          var target = mutation.target, attributeName = mutation.attributeName;
          if(target.tagName == "DETAILS" && attributeName == "open"){
            triggerToggle(target);
          }
        });
      }).observe(document.documentElement,{
        attributes:true,
        subtree:true
      });
    }else{
      onTogglingTrigger(function(el){
        var wasOpen = el.getAttribute("open");
        setTimeout(function(){
          var isOpen = el.getAttribute("open");
          if(wasOpen != isOpen){
            triggerToggle(el);
          }
        }, 1);
      });
    }
  }
  function polyfillAccessibility(){
    setAccessibilityAttributes(document);
    if(window.MutationObserver){
      new MutationObserver(function(mutations){
        forEach.call(mutations,function(mutation){
          forEach.call(mutation.addedNodes,setAccessibilityAttributes);
        });
      }).observe(document.documentElement,{
        subtree:true,
        childList:true
      });
    }else{
      document.addEventListener("DOMNodeInserted",function(e){
        setAccessibilityAttributes(e.target);
      });
    }
  }
  function setAccessibilityAttributes(root){
    findElementsWithTagName(root, "SUMMARY").forEach(function(summary){
      var details = findClosestElementWithTagName(summary, "DETAILS");
      summary.setAttribute("aria-expanded", details.hasAttribute("open"));
      if(!summary.hasAttribute("tabindex")) summary.setAttribute("tabindex","0");
      if(!summary.hasAttribute("role")) summary.setAttribute("role","button");
    });
  }
  function eventIsSignificant(e){
    return !(e.defaultPrevented || e.ctrlKey || e.metaKey || e.shiftKey || e.target.isContentEditable);
  }
  function onTogglingTrigger(cb){
    addEventListener("click",function(e){
      if(eventIsSignificant(e)){
        if(e.which <= 1){
          var el = findClosestElementWithTagName(e.target,"SUMMARY");
          if(el && el.parentNode && el.parentNode.tagName == "DETAILS"){
            cb(el.parentNode);
          }
        }
      }
    },false);
    addEventListener("keydown",function(e){
      if(eventIsSignificant(e)){
        if(e.keyCode == 13 || e.keyCode == 32){
          var el = findClosestElementWithTagName(e.target,"SUMMARY");
          if(el && el.parentNode && el.parentNode.tagName == "DETAILS"){
            cb(el.parentNode);
            e.preventDefault();
          }
        }
      }
    },false);
  }
  function triggerToggle(el){
    var ev = document.createEvent("Event");
    ev.initEvent("toggle",false,false);
    el.dispatchEvent(ev);
  }
  function findElementsWithTagName(root,tagName){
    return (root.tagName == tagName ? [ root ] : []).concat(typeof root.getElementsByTagName == "function" ? slice.call(root.getElementsByTagName(tagName)) : []);
  }
  function findClosestElementWithTagName(el,tagName){
    if(typeof el.closest == "function"){
      return el.closest(tagName);
    }else{
      while (el){
        if(el.tagName == tagName) return el;
        else el = el.parentNode;
      }
    }
  }
})();
