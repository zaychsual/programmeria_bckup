/** https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign */
Object.defineProperty(Object,"assign",{
	enumerable:false,
	writable:true,
	configurable:true,
	value:function assign(target){
		'use strict';
		if(target === null || target === undefined){
			throw new TypeError('Cannot convert undefined or null to object');
		}
		var to = Object(target);
		for(var i=1; i<arguments.length; i++){
			var nextSrc = arguments[i];
			if(nextSrc !== null && nextSrc !== undefined){ 
				for(var nextKey in nextSrc){
					if(Object.prototype.hasOwnProperty.call(nextSrc,nextKey)){
						to[nextKey] = nextSrc[nextKey];
					}
				}
			}
		}
		return to;
	}
});