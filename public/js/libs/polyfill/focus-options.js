/** focus - focusOptions - preventScroll polyfill - https://github.com/calvellido/focus-options-polyfill */
(function(){
  if(typeof window === "undefined" || typeof document === "undefined" || typeof HTMLElement === "undefined") return;

  var supportsPreventScrollOption = false;
  try {
    var focusEl = document.createElement("div");
    focusEl.addEventListener("focus",function(e){
			e.preventDefault();
			e.stopPropagation();
		},true);
    focusEl.focus(
      Object.defineProperty({},"preventScroll",{
        get:function(){
          supportsPreventScrollOption = true;
        }
      })
    );
  }catch(e){}

  if(HTMLElement.prototype.nativeFocus === undefined && !supportsPreventScrollOption){
    HTMLElement.prototype.nativeFocus = HTMLElement.prototype.focus;

    var calcScrollableEls = function(el){
      var parent = el.parentNode,
					scrollableEls = [],
					rootScrollEl = document.scrollingElement || document.documentElement;

      while (parent && parent !== rootScrollEl){
        if(parent.offsetHeight < parent.scrollHeight || parent.offsetWidth < parent.scrollWidth){
          scrollableEls.push([
            parent,
            parent.scrollTop,
            parent.scrollLeft
          ]);
        }
        parent = parent.parentNode;
      }
      parent = rootScrollEl;
      scrollableEls.push([parent, parent.scrollTop, parent.scrollLeft]);
      return scrollableEls;
    };

    var restoreScrollPosition = function(scrollableEls){
      for(var i = 0; i < scrollableEls.length; i++){
        scrollableEls[i][0].scrollTop = scrollableEls[i][1];
        scrollableEls[i][0].scrollLeft = scrollableEls[i][2];
      }
      scrollableEls = [];
    };

    var patchedFocus = function(args){
      if(args && args.preventScroll){
        var evScrollableElements = calcScrollableEls(this);
        this.nativeFocus();
        if(typeof setTimeout === 'function'){
          setTimeout(function(){
            restoreScrollPosition(evScrollableElements);
          },0);
        }else{
          restoreScrollPosition(evScrollableElements);          
        }
      }else{
        this.nativeFocus();
      }
    };
    HTMLElement.prototype.focus = patchedFocus;
  }
})();