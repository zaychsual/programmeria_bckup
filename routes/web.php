<?php
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\EsmController;
use App\Http\Controllers\LinkController;
// use Illuminate\Http\Request;// DEV OPTION

// use hisorange\BrowserDetect\Parser as Browser;
use Jenssegers\Agent\Agent;

/*--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great! */

// ORI
// Route::get('/',function(){
//     return view('welcome');
// });

// Route::get('/iconmaker',function(){
// 	return view('iconmaker');
// });

// Es Module CDN:
Route::get('/esm/{filepath}', [EsmController::class, 'get'])->where('filepath', '^(.+)\/([^\/]+)$');

Route::get('/blank', [LinkController::class, 'open']);

// Admin App (NOT FIX: check again)
Route::prefix('admin')->group(function(){
  Route::get('{uri?}',function(){
    $agent = new Agent();

    if($agent->isRobot()){
      abort(404);
    }
    
    $content = view('admin');
    return response($content)->withHeaders([
      'Accept-Ranges' => 'bytes',
      'Access-Control-Allow-Origin' => '*',
      'X-Frame-Options' => 'SAMEORIGIN',
      // 'Vary' => 'Accept-Encoding',
      'X-Robots-Tag' => 'none,noarchive',
      'X-Powered-By' => 'Programmeria', // REQUIRE
      'X-XSS-Protection' => '1; mode=block'
      // 'Feature-Policy' => "display-capture 'self'"
    ]);

    // return view('admin');

    // return response($content)->header('Access-Control-Allow-Origin', '*');
  })->where('uri', '(.*)');
});

Route::get('/{uri?}',function(){
  $agent = new Agent();

  if($agent->isRobot()){
    // Facebook Bot:
    // facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)
    // facebookexternalhit/1.1
    // Facebot
    $r = $agent->robot();
    // $fb = $r === "Facebookexternalhit" || $r === "Facebot";
    return "BOT: " . $r;
  }
  
  return view('welcome');
})->where('uri', '(.*)');

// Front App (NOT FIX: check again)
/* Route::get('/{uri?}',function(Request $request){
  if(!$request->is('api/*')){ // !$request->is('api') ||
    return view('welcome');
    // $content = view('welcome');
    // return response($content)->header('Access-Control-Allow-Origin', '*');
  }
  return 'NOT FOUND';
})->where('uri', '(.*)'); */

//
// DEV Package Manager -
// Route::get('/package-manager', 'PackageManagerCtrl@index');
// Route::post('package-manager/install', 'PackageManagerCtrl@install');

