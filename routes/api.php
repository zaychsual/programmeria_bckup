<?php 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UploadController;
// use App\Http\Controllers\Api\EsmController;// OPTION: Es Module CDN here

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout']);
Route::post('/islogin', [AuthController::class, 'islogin']);

// Q-CUSTOM:
Route::post('/upload/{path}', [UploadController::class, 'save'])->where('path', '^(.+)\/([^\/]+)$');

// OPTION: Es Module CDN here
// Route::get('/esm/{filepath}', [EsmController::class, 'get'])->where('filepath', '^(.+)\/([^\/]+)$');

// Route::get('/getauth', [AuthController::class, 'getauth']);

// Route::get('/orders', function (Request $request) {
// 	$token = $request->user()->token();
//     if ($token) {
//         return json_encode(['user' => "TOKEN OK"]);
//     }
//     else{
//     	return json_encode(['user' => "TOKEN FAILED"]);
//     }
// });


