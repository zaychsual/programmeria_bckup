<?php 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
// use App\Models\User;

class EsmController extends Controller{
    // public function __construct(){
    //     $this->middleware('auth');
    // }

    public function get(Request $request, $filepath){ // , Response $res
        // $validate = $request->validate([
        //     'userfile' => 'required'
        // ]);

        $res = $request->headers->get('Accept');
        if($res !== "*/*"){
            // return response(['error' => 'NOT FOUND']);
            abort(404);
        }

        // dd($request->headers->get('Accept'));// Accept | accept

        // $file = response()->download(storage_path($filename), null, [], null);
        $file = Storage::disk('esm')->get($filepath);// WORK
        // $file = Storage::get("public/js/esm/" . $filepath);// WORK: Without set filesystem.php
 
        // return Storage::disk('esm')->download($filepath, null, [
        //     'Content-Type' => 'application/javascript',
        //     'Access-Control-Allow-Origin' => '*',
        //     'Content-Disposition' => 'inline'
        // ])->deleteFileAfterSend(true);// , null

        // CHECK: response function can support response code http
        return response($file, 200)->withHeaders([
            'Content-Type' => 'text/javascript', // application/javascript
            // 'Accept-Ranges' => 'bytes', // OPTION
            'Access-Control-Allow-Origin' => '*', // REQUIRE
            'Content-Disposition' => 'inline'
            // 'X-Frame-Options' => 'SAMEORIGIN', // OPTION
            // 'Vary' => 'Accept-Encoding', // OPTION
            // 'X-Powered-By' => 'Programmeria' // REQUIRE
        ]);

        // WORK: must -> use Illuminate\Http\Response;
        // return (new Response($file, 200))->withHeaders([
        //     'Content-Type' => 'application/javascript',
        //     'Accept-Ranges' => 'bytes', // OPTION
        //     'Access-Control-Allow-Origin' => '*', // REQUIRE
        //     'X-Frame-Options' => 'SAMEORIGIN', // OPTION
        //     'Vary' => 'Accept-Encoding', // OPTION
        //     'X-Powered-By' => 'Programmeria' // REQUIRE
        // ]);
        // ->header('Content-Type', 'application/javascript');
    }
}
