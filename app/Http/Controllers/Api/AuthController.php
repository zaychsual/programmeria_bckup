<?php 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\User;

class AuthController extends Controller{
    public function register(Request $request){
        $validatedData = $request->validate([
            'name'     => 'required|max:55',
            'email'    => 'email|required|unique:users',
            'password' => 'required|min:5' // required|confirmed
        ]);
        $validatedData['password'] = bcrypt($request->password);
        $user = User::create($validatedData);

        $token = $user->createToken('authToken')->accessToken;
        
        return response(['user' => $user, 'access_token' => $token]); // access_token | token
    }

    public function login(Request $request){
        $loginData = $request->validate([
            'email'    => 'email|required',
            'password' => 'required|min:5'
        ]);

        if(!auth()->attempt($loginData)){
            return response(['message' => 'Invalid Credentials']);
        }

        $token = auth()->user()->createToken('authToken')->accessToken;

        // ->withCookie('color', 'blue');

        // access_token | token
        // return response(['user' => auth()->user(), 'access_token' => $token]);
        return response(['user' => auth()->user(), 'access_token' => $token])->cookie(
            'access_token', $token, time() - 3600, "/", "localhost", false, true
        );
    }

    public function logout(){
        // $token = $request->user()->token();
        // $token->revoke();
        // $response = ['message' => 'You have been successfully logged out!'];
        // return response($response, 200);

        // \Cookie::queue(\Cookie::forget('test_token'));
        // return response(['ok' => true]);

        $cookie = \Cookie::forget('access_token');
        return response(['ok' => true])->withCookie($cookie);
    }

    public function islogin(){
        $val = Cookie::get('access_token');
        return response(['value' => $val]);

        // if($val) return response(['value' => $val]);
        // return response(['err' => 'Not found']);
    }

    // public function getauth(Request $request){
    //     $token = $request->user()->token();
    //     if ($token) { // $request->user()->tokenCan('authToken')
    //         return json_encode(['user' => "TOKEN OK"]);// auth()->user(), 'token' => $token
    //     }else{
    //         return json_encode(['user' => "TOKEN FAILED"]);
    //     }
    // }
}
