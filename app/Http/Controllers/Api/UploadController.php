<?php 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage;
// use App\Models\User;

class UploadController extends Controller{
    public function save(Request $request, $path){
        // $validate = $request->validate([
        //     'userfile' => 'required'
        // ]);

        // $validatedData['password'] = bcrypt($request->password);
        // $user = User::create($validatedData);

        // $token = $user->createToken('authToken')->accessToken;
        
        // return response(['user' => $user, 'access_token' => $token]); // access_token | token

        // Storage::put($file->getClientOriginalName(), $file);

        // $file = $request->file('userfile')->store('/public/fonts');// 'font' | '/','public'
        // file_get_contents();

        // if($file){ // $request->has('userfile')
        //     // $path = $request->file('userfile')->store('font');
        //     $file->store('/');
        // }

        $file = $request->file('userfile');
        $store = $file->storeAs($path, $file->getClientOriginalName());// '/public/fonts'

        if($store) return response(['data' => $store]);

        return response(['error' => 'Error upload']);
        // return $store;
    }
}
