import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React, { lazy } from 'react';// { Suspense }
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
// import { lazy as lazyLoadable } from '@loadable/component';// loadable
import MediaQuery from 'react-responsive';

// import 'antd/dist/antd.css';

import { APP_NAME } from './src/data/appData';

import AsideAdmin from './src/parts/admin/AsideAdmin';
import NavAdmin from './src/parts/admin/NavAdmin';
import RouteLazy from './src/components/RouteLazy'; // ErrorBoundary | RouteLazy
import PageLoader from './src/components/PageLoader';
import Flex from './src/components/q-ui-react/Flex';
// import Loops from './src/components/q-ui-react/Loops';
import ScrollTo from './src/components/q-ui-react/ScrollTo';
// import RouteParams from './src/parts/RouteParams';
// import DynamicImport from './src/parts/DynamicImport';

import Ide from './src/apps/ide/Ide';

// PAGES:
import HomeAdmin from './src/pages/admin/HomeAdmin';
// import DocsPage from './src/pages/admin/docs/DocsPage';
import NotFound from './src/pages/public/NotFound';

// ==================================================
// DEV OPTION: Store to window object / global
window.React = React;
window.ReactDOM = ReactDOM;
// END DEV OPTION: Store to window object / global
// ==================================================

const IdePage = () => <Ide />;

// lazy:
// const NotFound = lazy(() => import(/* webpackChunkName: "NotFound" */'./src/pages/public/NotFound'));
const Settings = lazy(() => import(/* webpackChunkName: "private/Settings" */'./src/pages/admin/Settings'));

// Components:
// storage/js/chunk/pv/adm/docs/
const ArouteDocs = lazy(() => import(/* webpackChunkName: "private/ArouteDocs" */'./src/pages/admin/docs/q-ui-react/ArouteDocs'));
const AvaDocs = lazy(() => import(/* webpackChunkName: "private/AvaDocs" */'./src/pages/admin/docs/q-ui-react/AvaDocs'));
const BtnDocs = lazy(() => import(/* webpackChunkName: "private/BtnDocs" */'./src/pages/admin/docs/q-ui-react/BtnDocs'));
const LoopsPage = lazy(() => import(/* webpackChunkName: "private/LoopsPage" */'./src/pages/admin/docs/q-ui-react/LoopsPage'));
const ModalPage = lazy(() => import(/* webpackChunkName: "private/ModalPage" */'./src/pages/admin/docs/q-ui-react/ModalPage'));
const TableDocs = lazy(() => import(/* webpackChunkName: "private/TableDocs" */'./src/pages/admin/docs/q-ui-react/TableDocs'));
// Documentation:
const Laravel = lazy(() => import(/* webpackChunkName: "private/Laravel" */'./src/pages/Laravel'));
// Develop:
// storage/js/chunk/pv/adm/
const Devs = lazy(() => import(/* webpackChunkName: "private/Devs" */'./src/pages/admin/devs/Devs'))
const MatterCSS = lazy(() => import(/* webpackChunkName: "private/MatterCSS" */'./src/pages/admin/devs/MatterCSS'));
// const Devs = loadable(() => import(/* webpackChunkName: "storage/js/chunk/pv/adm/Devs" */'./src/pages/private/Devs'));
const PosPage = lazy(() => import(/* webpackChunkName: "private/PosPage" */'./src/pages/private/PosPage'));
// END lazy
// END PAGES

const BASENAME = "/admin";// Admin url basename

const MENUS = [
	// {label:"Testing", href:"/admin/testing", icon:"cogs"},
	{label:"Home", to:"/", icon:"home", exact:true, strict:true}, 
	{label:"Settings", to:"/settings", icon:"cog"}, 
	{label:"Articles", to:"/articles", icon:"edit"}, 
	{label:"Projects", to:"/projects", icon:"list"}, 
	{label:"Components", icon:"code", 
    menus: [
			{label:"Aroute", to:"/components/aroute", icon:"code"}, 
			{label:"Ava", to:"/components/ava", icon:"code"}, 
			{label:"BgImage", to:"/components/bg-image", icon:"code"}, 
			{label:"Btn", to:"/components/btn", icon:"code"}, 
			{label:"Flex", to:"/components/flex", icon:"code"},
			{label:"Form", to:"/components/form", icon:"code"},
			{label:"Head", to:"/components/head", icon:"code"},
			{label:"Iframe", to:"/components/iframe", icon:"code"},
			{label:"Img", to:"/components/img", icon:"code"},
			{label:"InputGroup", to:"/components/input-group", icon:"code"},
			{label:"InputQ", to:"/components/input-q", icon:"code"},
			{label:"Loops", to:"/components/loops", icon:"code"},
			{label:"Modal", to:"/components/modal", icon:"code"},
			{label:"NetWork", to:"/components/network", icon:"code"},
			{label:"ResizeSensor", to:"/components/resize-sensor", icon:"code"},
			{label:"RouteImport", to:"/components/route-import", icon:"code"},
			{label:"Script", to:"/components/script", icon:"code"},
			{label:"ScrollTo", to:"/components/scroll-to", icon:"code"},
			{label:"SpinLoader", to:"/components/spin-loader", icon:"code"},
			{label:"Table", to:"/components/table", icon:"code"}
    ]
	},
	{label:"Documentation", icon:"book", 
    menus: [
			{label:"JS Utilities", to:"/documentation/js-utilities", icon:"js fab"}, 
			{label:"Laravel", to:"/documentation/laravel", icon:"laravel fab"}, 
    ]
  },
  {label:"Develop", icon:"code", 
    menus: [
			{label:"Devs", to:"/devs/all", icon:"cogs"}, 
			{label:"Matter CSS", to:"/devs/matter-css", icon:"browser"}, 
			{label:"Point Of Sales App", to:"/devs/pos-app", icon:"cash-register"},
			{label:"Integrated Development Environment", to:"/devs/ide", icon:"code"},
    ]
	}, 
	{label:"Experiments", to:"/experiments", icon:"flask"}, 
  // {label:"About", href:"/about", icon:"info-circle"}, 
  // {label:"Contact", href:"/contact", icon:"phone"}, 
	// {label:"Bug", href:"/bug", icon:"bug"}, 
  // {label:"More", icon:"bars", 
  //   menus: [
  //     {label:"Setup"},
  //     {label:"Heart Broken", icon:"heart-broken"},
  //     {label:"Infos", href:"/admin/infos"},
  //   ]
  // }, 
];

// const SM_DEVICE = 'ontouchstart' in window || (navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0) || (Q.isMobile() && screen.width < 480);
// const SM_DEVICE = Q.UA.platform.type === "mobile" || screen.width < 480; // Q_UA.platform.type === "mobile" && screen.width < 480;

// const Loader = () => (
// 	<PageLoader bottom left={SM_DEVICE} w={SM_DEVICE ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />
// );

// const ErrMsg = ()  => (
// 	<div role="alert" 
// 		className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
// 	>&#9888;{` Something went wrong. 

// ${navigator.onLine ? 'Server error.':'Your internet connection is offline.'}`}</div>
// );

class Root extends React.Component{
	constructor(props){
		super(props);

		// SETUP DOM 
		//if(!document.documentElement?.lang?.length > 0) document.documentElement.lang = Q.lang();

		Q.setClass(document.documentElement, Q.UA.platform.type + " " + Q.UA.os.name + " " + Q.UA.browser.name);

		Q.setAttr(document.documentElement, {
			"data-os-v": Q.UA.os.versionName || Q.UA.os.version,
			"data-browser-v": Q.UA.browser.version
		});
		
		// if(SM_DEVICE) Q.setClass(document.body, 'isMobile');
		// END SETUP DOM

		this.state = {
			asideMin: false, 
		};
	}

	componentDidMount(){
		// const { history } = this.props;
		// console.log('componentDidMount in Admin window.history: ', window.history);

		// if ('scrollRestoration' in window.history) {
		// 	// Back off, browser, I got this...
		// 	window.history.scrollRestoration = 'manual';
		// }

		// console.log('componentDidMount in Admin window.history after: ', window.history);

		Q.setClass(Q.domQ('#QloadStartUp'),'d-none');// Hide Loader Splash / start up

    ['dragover','drop'].forEach(v => window.addEventListener(v, Q.preventQ));// Prevent Drag & Drop File
		
		window.addEventListener('keydown', this.onCtrlSaveNPrint);// DEV OPTION: for prevent ctrl + S, ctrl + P
		
		// ['online','offline'].forEach(v => window.addEventListener(v, this.onConnection));
		
		// Store favicon offline file
		// if(!FAV_OFF){
		// 	localStorage.setItem('favicon-offline', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACN0lEQVR42pRTTUhUURT+zn13xtEoG5KMyNpILloVuBjIFHMRFfZDtGtTZFItIyJw126gKDSKJKKd1CINU6ZVGES1CAYKsgRNF85o/oymvjvv3tOZGTfxBrHDO++dd+53/s8FOwZziV3h67jxd2Y0tZrLLrNzbAPjRF5cXsr0OXaNzA4lLmJBzjkQhAixuezP1/Ga+jZangN/eAOa+gEUwLvrgcQJoHoHjPnTF41tvQjGSsGsGFU8xxZnxyecWWPu7WJuq2Ruwr/cWsHcc4OtWeG11aW0BK4Su1IG8zNjQ/F43TG61Q58TmFDOtQKlxwEK/1CKe88nHUHnQ3YPbwZjjqWZp6eCOndvetsrRV2R1RuYfIOL8yCXt7HZon6H8HOT7M0qFNFdGUzfRwG8v6mHcAG0CMD5KxtU7Gq+BZMfsd/k9iQ59VoJgLlTXlQsgPwdPkzmYCMj7S/ksvF6hq2UTnQ+LfigpSjYO9+cWKzirQesonjgI6EUT3vgWfpsF6yCppOWyKVUlpXdiNeC3fmWhiYnQIyv8I9bL8CXV3ryTL3kjEG1vrPFdOFSNc50KfhDXvHBxIwd99apaMDUt1ZcSDjY4rmg9WUhmrWT25D9T+WIk0obXvyMoKrSUte9Kv0vkW082R8I/dCdoIoAue6pa6OYDHj9OBT5Y1+AXbugb+vAfbwqaBi+y4ZCb+S63NJ8HNUMPP99QUqNJuLUouIneL0qChr1g9m5DUi/ED+3xU1BWt5/gowAIJUQNn6yq1DAAAAAElFTkSuQmCC');
		// }
		
		// const Qworker = new Worker('/storage/src/worker/tryWorker.js');
		
		// Qworker.onmessage = (e) => {
			// if(e && e.data){
				// console.log('Qworker: ',e.data);
			// }
    // };
		
		// Qworker.addEventListener('message', (e) => {
			// if(e && e.data){
				// console.log('Qworker: ',e.data);
			// }
		// }, false);
		
		// Qworker.postMessage('babel');
		
		// import(/*webpackIgnore:true*/'https://dev.jspm.io/react-dom').then(m => {
			// console.log(m);
			// localStorage.setItem('modules', m.default);
		// }).catch(e => console.error(e));
		
		// https://dev.jspm.io/react-router-dom | https://unpkg.com/react-router-dom?module
		// import(/*webpackIgnore:true*/'https://dev.jspm.io/reactstrap').then(v => {
			// console.log(v);
		// }).catch(e => console.error(e));

		// Store scroll position before reload page (Chrome Only)
		// if(Q.UA.browser.name === "Chrome"){
		// 	let scroll = Number(sessionStorage.getItem('scroll'));
		// 	if(scroll && document.body.offsetHeight > scroll){
		// 		window.scrollTo(0, scroll);
		// 	}

		// 	window.addEventListener("unload", (e) => {
		// 		//  localStorage.removeItem('persist:programmeria');// console.log('unload ', e);
		// 		sessionStorage.setItem('scroll', window.scrollY);
		// 	});
		// }
		// END Store scroll position before reload page (Chrome Only)

    // window.onbeforeunload = function(e){
    //  let sb = navigator.sendBeacon(
    //    'https://reqres.in/api/users',
    //    JSON.stringify({
		// 		name: 'Husein',
		// 		job: 'Programmer'
		// 	})
    //  );
    //  console.log('sb: ', sb);
     // empty return is required to not show popup
    //  return;
		// }
		
		// fetch('https://jsonplaceholder.typicode.com/posts', {
		// 	method: 'POST',
		// 	body: JSON.stringify({
		// 		title: 'foo',
		// 		body: 'bar',
		// 		userId: 1
		// 	}),
		// 	headers: {
		// 		"Content-type": "application/json; charset=UTF-8"
		// 	}
		// })
		// .then(response => response.json())
		// .then(json => console.log(json));
		
		// console.log('%cProgrammeria App Designer','color:#666;font-family:sans-serif;letter-spacing:2px;font-size:26px;font-weight:700;text-shadow:1px 1px 0 #A0E7FE,2px 2px 1px rgba(0,0,0,.3)');
		// console.log('%cDevelopment by: https://programmeria.com','font-size:15px');
	}

	componentDidUpdate(prevProps){
		const { location } = this.props;
		// console.log('componentDidUpdate in Admin prevProps: ',prevProps);
		// console.log('componentDidUpdate in Admin location: ',location);
		if(location.pathname !== prevProps.location.pathname){ //   && !location.state | && !location.pathname.includes('/detail/')
      window.scrollTo(0, 0);
      // DEV to set previous url FOR Not Found page
      // this.setState({backUrl: prevProps.location.pathname});
		}
		
		// FOR close dropdown-menu with dropdown-item react-router NavLink / Link component
		let btn = document.activeElement;
		if(Q.getAttr(btn, 'aria-expanded') === 'true'){ // hasClass(btn, "ddRoute") ||
			btn.click();
			btn.blur();
		}
	}
	
// NOT FIX:
	// onConnection = e => {
	// 	// console.log('onConnection e: ', e);
	// 	const rel = domQ('link[rel="shortcut icon"]');
	// 	const href = getAttr(rel, 'href');
	// 	let fav = '/favicon.ico';
	// 	if(navigator.onLine){
	// 		if(href !== fav) rel.href = fav;
	// 	}else{
	// 		if(href === fav) rel.href = FAV_OFF ? FAV_OFF : '/favicon-offline.png';
	// 	}
	// }
	
// DEV OPTION: Prevent ctrl + S, ctrl + P
  onCtrlSaveNPrint = e => {
    e.stopPropagation();// OPTION

    let kc = e.keyCode;
    let ctrl = e.ctrlKey;

    // 80 = P
    if(((ctrl && kc === 80) && !Q.hasClass(document.documentElement, 'print-on')) || ctrl && kc === 83){
      e.preventDefault();
    }
    // 83 = S,
    // if(ctrl && kc === 83){
      // e.preventDefault();
    // }
  }

  onToggleAside = () => {
    if(this.state.asideMin){
      this.setState({asideMin: false});
      document.body.classList.toggle('aside-min');
    }
  }

	render(){
		// const { location } = this.props;
		const { asideMin } = this.state;

		return (
			<MediaQuery 
				maxDeviceWidth={767} // 768 | minDeviceWidth 
				// device={{ deviceWidth: 1600 }} 
			>
				{matches => {
					document.body.classList.toggle("isMobile", matches);

					return (
						<Flex dir="row" className="layout">
							<AsideAdmin 
								isMobile={matches} // SM_DEVICE 
								basename={BASENAME} 
								headText={APP_NAME} // "Programmeria" 
								asideMin={asideMin} 
								menus={MENUS} 
								onToggleAside={this.onToggleAside} 
							/>

							<div className="flex-auto pancake-stack">
								<NavAdmin 
									isMobile={matches} // SM_DEVICE 
									asideMin={asideMin} 
									onToggleAside={() => this.setState({asideMin: !asideMin})} 
								/>

								<main>
									<React.Suspense 
										fallback={<PageLoader bottom left={matches} w={matches ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />}
									>
										<Switch>
											<Route exact strict path="/">
												<HomeAdmin />
											</Route>

											{/* <Loops 
												data={[
													{ p:"/settings", c: Settings }, // component
											
													{ p:"/components/table", c: TableDocs },
													{ p:"/components/btn", c: BtnDocs },
											
													{ p:"/documentation/laravel", c: Laravel },
											
													{ p:"/devs/all", c: Devs },
													{ p:"/devs/matter-css", c: MatterCSS },
													{ p:"/devs/pos-app", c: PosPage }, 

													{ p:"*", c: NotFound }
												]}
											>
												{v => <RouteLazy path={v.p} component={v.c} />}
											</Loops> */}

											<RouteLazy path="/settings" component={Settings} 
												// errorMsg={<ErrMsg />} src/pages/private/
											/>

											{/* <DocsPage /> */}

											<RouteLazy path="/components/Aroute" component={ArouteDocs} />
											<RouteLazy path="/components/ava" component={AvaDocs} />
											<RouteLazy path="/components/btn" component={BtnDocs} />
											<RouteLazy path="/components/loops" component={LoopsPage} />
											<RouteLazy path="/components/modal" component={ModalPage} />
											<RouteLazy path="/components/table" component={TableDocs} />
											

											<RouteLazy path="/documentation/laravel" component={Laravel} />

											<RouteLazy path="/devs/all" component={Devs} />
											<RouteLazy path="/devs/matter-css" component={MatterCSS} />
											<RouteLazy path="/devs/pos-app" component={PosPage} />

											<RouteLazy path="/devs/ide" component={IdePage} />

											{/* DEVS ONLY */}
											<Route path="/devs/all" component={Devs} />
											<Route path="/devs/pos-app" component={PosPage} />
											{/* END DEVS ONLY */}

											<Route>{/*  path="*" */}
												<NotFound />
											</Route>

											{/* <ErrorBoundary
												errorMsg={<ErrMsg />} 
												path="*" 
												component={NotFound}
											/> */}
										</Switch>
									</React.Suspense>
								</main>
							</div>

							{/* NOT FIX: */}
							<ScrollTo 
								className="position-fixed zi-2 tip tipL fal fa-arrow-up" 
								style={{ bottom:9, right:9 }} 
								aria-label="Back To top" 
								// onClick={(scroll, e) => {
								// 	console.log('onClick scroll: ', scroll);
								// }}
							/>
						</Flex>
					)
				}}
			</MediaQuery>
		);
	}
}

const Admin = () => (
	<BrowserRouter basename={BASENAME}>
		<LastLocationProvider>
			<Route component={Root} />
		</LastLocationProvider>
	</BrowserRouter>
);

ReactDOM.render(<Admin />, document.getElementById('root'));

// if(document.getElementById('root')){
  // ReactDOM.render(<Example />, document.getElementById('root'));
// }
