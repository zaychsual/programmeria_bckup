// require('./bootstrap');

import 'react-app-polyfill/ie11'; // OPTION
import 'react-app-polyfill/stable';
import React, { lazy, Fragment, Suspense, useContext } from 'react';
import ReactDOM from 'react-dom';
// import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';// , Redirect
// import Drawer from 'antd/es/drawer';

// import 'antd/dist/antd.css';
// AUTH:
import { AuthProvider, AuthContext } from './src/context/AuthContext';
import { FetchProvider } from './src/context/FetchContext';

import NavApp from './src/parts/app/NavApp';
import InputGroup from './src/components/q-ui-react/InputGroup';
import Btn from './src/components/q-ui-react/Btn';
import ModalQ from './src/components/q-ui-react/ModalQ';
import ErrorBoundary from './src/components/ErrorBoundary';
import PageLoader from './src/components/PageLoader';

// PAGES:
import Home from './src/pages/Home';
// import Login from './src/pages/public/Login';

import NotFound from './src/pages/public/NotFound';
// END PAGES

// ==================================================
// DEV OPTION: Store to window object / global
window.React = React;
window.ReactDOM = ReactDOM;
// END DEV OPTION: Store to window object / global
// ==================================================

// lazy:
// import RouteImport from './src/components/q-ui-react/RouteImport';

// import Laravel from './src/pages/Laravel';
// storage/js/chunk/pv/
// storage/js/chunk/guest/
const Login = lazy(() => import(/* webpackChunkName: "Login" */'./src/pages/public/Login'));
// storage/js/chunk/guest/
const Register = lazy(() => import(/* webpackChunkName: "Register" */'./src/pages/public/Register'));
// END lazy
// END PAGES

const SM_DEVICE = Q.UA.platform.type === "mobile" || screen.width < 480; // Q.UA.platform.type === "mobile" && screen.width < 480;

// const UnauthenticatedRoutes = () => (
//   <Switch>
//     <Route exact path="/">
//       <Home />
//     </Route>
//     <Route path="/login">
//       <Login />
//     </Route>
//     <Route path="/register">
//       <Register />
//     </Route>
//     <Route path="*">
//       <NotFound />
//     </Route>
//   </Switch>
// );

// const AuthenticatedRoute = ({ children, ...etc }) => {
//   const auth = useContext(AuthContext);
//   return (
//     <Route 
//       {...etc}
//       render={() => auth.isAuthenticated() ? children : <NotFound />} // <Redirect to="/" />
//     />
//   );
// };

// const AdminRoute = ({ children, ...rest }) => {
//   const auth = useContext(AuthContext);
//   return (
//     <Route
//       {...rest}
//       render={() =>
//         auth.isAuthenticated() && auth.isAdmin() ? (
//           <AppShell>{children}</AppShell>
//         ) : (
//           <Redirect to="/" />
//         )
//       }
//     ></Route>
//   );
// };

class Root extends React.Component{
  constructor(props){
    super(props);

    // SETUP DOM
    let html = document.documentElement;
    Q.setClass(html, Q.UA.platform.type + " " + Q.UA.os.name + " " + Q.UA.browser.name);

		Q.setAttr(html, {
			"data-os-v": Q.UA.os.versionName || Q.UA.os.version,
			"data-browser-v": Q.UA.browser.version
		});
		
    if(SM_DEVICE) Q.setClass(document.body, 'isMobile');
    // END SETUP DOM

    // DEV OPTION: back to home if from /search
    if(props.location.pathname === '/search') props.history.replace('/'); 

    // this.state = {
      // lang: Q.lang(), 
      // auth: sessionStorage.getItem('auth'),
    // };

    this.findRef = React.createRef();
  }

  componentDidMount(){
    // console.log('auth: ',this.state.auth);
		Q.setClass(Q.domQ('#QloadStartUp'),'d-none');// Hide Loader Splash / start up

    ['dragover', 'drop'].forEach(v => window.addEventListener(v, Q.preventQ));// Prevent Html Drop File in window app

		// window.addEventListener('keydown', this.onCtrlSaveNPrint);// DEV OPTION: for prevent ctrl + S, ctrl + P

    // https://jsonplaceholder.typicode.com/posts | 
		// fetch('https://reqres.in/api/login', {
		// 	method: 'POST',
		// 	body: JSON.stringify({
    //     email: "eve.holt@reqres.in",
    //     password: "cityslicka" // null
    //   }),
		// 	headers: {
		// 		"Content-type": "application/json; charset=UTF-8"
		// 	}
		// })
		// .then(response => response.json())
		// .then(json => console.log(json));

    // console.log('%cWelcome to Programmeria', 'color:#666;font-family:sans-serif;letter-spacing:2px;font-size:26px;font-weight:700;text-shadow:1px 1px 0 #E1E9FF,2px 2px 1px rgba(0,0,0,.3)');
    // console.log('React version: ', React.version);
  }

  componentDidUpdate(prevProps){
    const { location } = this.props;
  	if(location.pathname !== prevProps.location.pathname){ //   && !location.state | && !location.pathname.includes('/detail/')
      window.scrollTo(0,0);
    }
    // FOR close dropdown-menu with dropdown-item react-router NavLink / Link component
  	let btn = document.activeElement;
  	if(Q.getAttr(btn, 'aria-expanded') === 'true'){
  		btn.click();
  		btn.blur();
  	}
  }

// DEV OPTION: Prevent ctrl + S, ctrl + P
  // onCtrlSaveNPrint = e => {
  //   e.stopPropagation();// OPTION

  //   let kc = e.keyCode;
  //   let ctrl = e.ctrlKey;

  //   // 80 = P
  //   if(((ctrl && kc === 80) && !Q.hasClass(document.documentElement, 'print-on')) || ctrl && kc === 83){
  //     e.preventDefault();
  //   }
  //   // 83 = S,
  //   // if(ctrl && kc === 83){
  //     // e.preventDefault();
  //   // }
  // }

  onClickSearch = () => {
    const { history, location } = this.props;
    history.push({
      pathname: "/search", // ${i.id}
      state: { searchMain: location }
    });
  }

  render(){
    // const {  } = this.state;
    const { history } = this.props; // location, 
    // const searchMain = location.state && location.state.searchMain;

    // console.log('cookie: ', UniversalCookie.get("programmeria_session"));

// <div className="app">
    return (
      <Fragment>
        <NavApp 
          isMobile={SM_DEVICE} 
          onClickSearch={this.onClickSearch}
        />

        <main>
          <Suspense fallback={<PageLoader bottom left className="w-100" />}>
            <ErrorBoundary>
              <Switch>
                {/* <AuthenticatedRoute path="/laravel">
                  <Laravel />
                </AuthenticatedRoute> */}

                {/* <UnauthenticatedRoutes /> */}

                <Route exact strict path="/" component={Home} />

                {/* <Route path="/laravel" component={Laravel} /> */}

                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />

                {/* <RouteImport path="/laravel" module="/js/esm/Laravel.js" /> */}

                <Route path="/search">
                  {/* <Drawer 
                    className="drawer-full searchMainDrawer" 
                    placement="top" 
                    zIndex={1051} 
                    // drawerStyle={{ height: '100vh' }} 
                    headerStyle={{ padding: 0 }}
                    // bodyStyle={{ padding:0 }} 
                    title={
                      <InputGroup 
                        As="div" // 
                        role="form" 
                        size="lg" 
                        // noValidate 
                        prepend={
                          <Btn onClick={() => history.goBack()} kind="fff" className="px-2 rounded-0 fal fa-arrow-left" />
                        }
                      >
                        <input type="text" placeholder="Search" 
                          ref={this.findRef} 
                          className="form-control rounded-0"  
                          
                        />
                      </InputGroup>
                    } 
                    closable={false} 
                    onClose={() => history.goBack()} 
                    afterVisibleChange={(open) => {
                      if(open) this.findRef.current.focus();
                    }}
                    visible={true} // searchMain ? true:false
                  >
                    <div>
                      <p>Seacrh Results</p>
                    </div>
                  </Drawer> */}

                  <ModalQ 
                    backdrop="static" 
                    keyboard={false} 
                    unmountOnClose={false} // OPTION
                    open={true} 
                    // size="sm" // sm | lg | xl
                    toggle={() => history.goBack()} // onToggleAside
                    scrollable 
                    // className="asideModalApp" // mx-0 | modal-full-width 
                    contentClassName="vh-100" // {modalPosition === "modal-full" ? "vh-100" : undefined} 

                    position="modal-full" // {modalPosition} 
                    closeHead={false}  
                    // headClass="headClass" 
                    head={
                      <InputGroup 
                        As="div" // 
                        role="form" 
                        size="lg" 
                        // noValidate 
                        prepend={
                          <Btn onClick={() => history.goBack()} kind="fff" className="px-2 rounded-0 fal fa-arrow-left" />
                        }
                      >
                        <input type="text" placeholder="Search" 
                          ref={this.findRef} 
                          className="form-control rounded-0"  
                          
                        />
                      </InputGroup>
                      // <div className="modal-header p-0 border-0">
                      //   <Btn onClick={() => setOpenDrawer(!openDrawer)} kind="fff" size="lg" className="h-100 fal fa-times btnToogleAsideMain" aria-label="Close"  />
                      // </div>
                    }
                    bodyClass="p-0" 
                    body={
                      <div>
                        <p>Seacrh Results</p>
                      </div>
                    } 
                    // foot={
                    // 	<Btn onClick={toggleModal}>Close</Btn>
                    // }
                  />
                </Route>

                <Route path="*">
                  <NotFound />
                </Route>
              </Switch>

              
            </ErrorBoundary>
          </Suspense>
        </main>
      </Fragment>
    );
  }
}

const App = () => (
  <BrowserRouter>
    <AuthProvider>
      <FetchProvider>
        <Route component={Root} />
      </FetchProvider>
    </AuthProvider>
  </BrowserRouter>
);

ReactDOM.render(<App />, document.getElementById('root'));

// <React.StrictMode>
// <App />
// </React.StrictMode>

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

// serviceWorker.unregister();
