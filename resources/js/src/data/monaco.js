// SETUP
const MONACO_THEMES = ["dark", "light"];// defaultThemes

// const monacoThemes = [
//   "Active4D",
//   "All Hallows Eve",
//   "Amy",
//   "Birds of Paradise",
//   "Blackboard",
//   "Brilliance Black",
//   "Brilliance Dull",
//   "Chrome DevTools",
//   "Clouds Midnight",
//   "Clouds",
//   "Cobalt",
//   "Dawn",
//   "Dominion Day",
//   "Dreamweaver",
//   "Eiffel",
//   "Espresso Libre",
//   "GitHub",
//   "IDLE",
//   "Katzenmilch",
//   "Kuroir Theme",
//   "LAZY",
//   "MagicWB (Amiga)",
//   "Merbivore Soft",
//   "Merbivore",
//   "Monokai Bright",
//   "Monokai",
//   "Night Owl",
//   "Oceanic Next",
//   "Pastels on Dark",
//   "Slush and Poppies",
//   "Solarized-dark",
//   "Solarized-light",
//   "SpaceCadet",
//   "Sunburst",
//   "Textmate (Mac Classic)",
//   "Tomorrow-Night-Blue",
//   "Tomorrow-Night-Bright",
//   "Tomorrow-Night-Eighties",
//   "Tomorrow-Night",
//   "Tomorrow",
//   "Twilight",
//   "Upstream Sunburst",
//   "Vibrant Ink",
//   "Xcode_default",
//   "Zenburnesque",
//   "iPlastic",
//   "idleFingers",
//   "krTheme",
//   "monoindustrial",
//   "themelist",
// ];

// monacoLangs
const MONACO_LANGS = [
  {id:1, name:"apex", type:"text/x-java", ex:"cls"},
  // {id:2, name:"azcli", type:"", ex:""}, // ???
  // {id:3, name:"bat", type:"application/bat", ex:"bat"},
  {id:4, name:"c", type:"text/x-csrc", ex:"c"},
  {id:5, name:"clojure", type:"text/x-clojure", ex:"clj"},
  {id:6, name:"coffeescript", type:"text/x-coffeescript", ex:"cson"},
  {id:7, name:"cpp", type:"text/x-c++src", ex:"cpp"},
  {id:8, name:"csharp", type:"text/x-csharp", ex:"cs"},
  // {id:9, name:"csp", type:"", ex:"csp"},
  {id:10, name:"css", type:"text/css", ex:"css"},
  // {id:11, name:"dockerfile", type:"text/x-dockerfile", ex:"dockerfile"},
  // {id:12, name:"fsharp", type:"text/x-fsharp", ex:"fs"},
  {id:13, name:"go", type:"text/x-go", ex:"go"},
  {id:14, name:"graphql", type:"text/plain", ex:"graphql"}, // ex:"gql"
  // {id:15, name:"handlebars", type:"", ex:"hbs"},
  {id:16, name:"html", type:"text/html", ex:"html"},
  {id:17, name:"ini", type:"text/x-properties", ex:"ini"},
  {id:18, name:"java", type:"text/x-java", ex:"java"},
  {id:19, name:"javascript", type:"text/javascript", ex:"js"},
  {id:20, name:"json", type:"application/json", ex:"json"},
  {id:21, name:"kotlin", type:"text/x-kotlin", ex:"kt"},
  {id:22, name:"less", type:"text/css", ex:"less"},
  // {id:23, name:"lua", type:"text/x-lua", ex:"lua"},
  {id:24, name:"markdown", type:"text/x-gfm", ex:"md"},
  // {id:25, name:"msdax", type:"", ex:""}, // ???
  // {id:26, name:"mysql", type:"text/x-sql", ex:"sql"},
  {id:27, name:"objective-c", type:"text/x-objectivec", ex:"m"},
  {id:28, name:"pascal", type:"text/x-pascal", ex:"pas"},
  {id:29, name:"perl", type:"text/x-perl", ex:"pl"},
  // {id:30, name:"pgsql", type:"text/x-sql", ex:""}, // ???
  {id:31, name:"php", type:"application/x-httpd-php", ex:"php"},
  {id:32, name:"plaintext", type:"text/plain", ex:"txt"},
  // {id:33, name:"postiats", type:"", ex:""}, // ???
  // {id:34, name:"powerquery", type:"", ex:""}, // ???
  {id:35, name:"powershell", type:"application/x-powershell", ex:"ps1"},
  {id:36, name:"pug", type:"text/x-pug", ex:"jade"},
  {id:37, name:"python", type:"text/x-python", ex:"py"},
  {id:38, name:"r", type:"text/x-rsrc", ex:"r"},
  // {id:39, name:"razor", type:"", ex:""}, // ???
  // {id:40, name:"redis", type:"", ex:""}, // ???
  // {id:41, name:"redshift", type:"", ex:""}, // ???
  {id:42, name:"ruby", type:"text/x-ruby", ex:"rb"},
  // {id:43, name:"rust", type:"", ex:""}, // ???
  // {id:44, name:"sb", type:"", ex:""}, // ???
  {id:45, name:"scheme", type:"text/x-scheme", ex:"scm"},
  {id:46, name:"scss", type:"text/x-scss", ex:"scss"},
  {id:47, name:"shell", type:"text/x-sh", ex:"sh"},
  // {id:48, name:"sol", type:"", ex:""}, // ???
  {id:49, name:"sql", type:"", ex:"sql"}, // type:"text/x-sql"
  // {id:50, name:"st", type:"", ex:""}, // ???
  {id:51, name:"swift", type:"text/x-swift", ex:"swift"},
  // {id:52, name:"tcl", type:"", ex:""}, // ???
  {id:53, name:"typescript", type:"text/plain", ex:"ts"},
  {id:54, name:"vb", type:"text/x-vb", ex:"vb"},
  {id:55, name:"xml", type:"text/xml", ex:"xml"},
  {id:56, name:"yaml", type:"text/x-yaml", ex:"yml"}
];

const MONACO_OPTIONS = {
	// Consolas, "Courier New", monospace
	fontFamily: 'Monaco, SFMono-Regular, Menlo, Consolas, "Liberation Mono", "Courier New", monospace',
	fontSize: 12, // 14
	tabSize: 2, // tabSize
	// dragAndDrop: false,
	minimap: {
		enabled: false, // Defaults = true
		// side: minimapSide, // Default = right | left
		// showSlider: 'always', // Default = mouseover
		// renderCharacters: false, // Default =  true
	},
	// colorDecorators: false, // Css color preview
	// readOnly: true,
	// mouseWheelZoom: zoomIde
};

/** END: user define object */
/* function setDefineWindow(range, mon){
	// returning a static list of proposals, not even looking at the prefix (filtering is done by the Monaco editor),
	// here you could do a server side lookup
	// return [
		// {
			// label: '"lodash"',
			// kind: mon.languages.CompletionItemKind.Function,
			// documentation: "The Lodash library exported as Node.js modules.",
			// insertText: '"lodash": "*"',
			// range: range
		// },{
			// label: '"express"',
			// kind: mon.languages.CompletionItemKind.Function,
			// documentation: "Fast, unopinionated, minimalist web framework",
			// insertText: '"express": "*"',
			// range: range
		// }
	// ];

// !v.startsWith('Esm_') : For Es Module have store in window
// eslint-disable-next-line
	return window.getDefineObj().filter(v => {
		let wv = window[v];
		// // !['__core-js_shared__','__REACT_DEVTOOLS_APPEND_COMPONENT_STACK__','__REACT_DEVTOOLS_COMPONENT_FILTERS__','__VUE_DEVTOOLS_TOAST__'].includes(v)
		if(wv && !wv.toString().includes('[Command Line API]') && !(v.startsWith('__') || v.endsWith('__')) && !v.startsWith('Esm_')){
			return v;
		}
	}).map(v => ({
		label: v,
		documentation: window[v].toString(),
		insertText: v
	}));
	
	// return setWin.map(v => ({
		// label: v,
		// insertText: v
	// }));
} */
/** END: Get user define object */

export { MONACO_THEMES, MONACO_LANGS, MONACO_OPTIONS };// setDefineWindow, monacoThemes, defaultThemes, monacoLangs
