// https://www.robinwieruch.de/react-usestate-callback
import React from 'react';// {useState, useEffect, useLayoutEffect}

// const useStateCb = (iState, cb) => {
const useStateCb = (iState, cb, condition) => {
  const [state, setState] = React.useState(iState);

  React.useEffect(() => {
		cb(state, condition);// if(isFunc(cb)) 
	}, [state, cb, condition]);

  return [state, setState];
};

const useStateCbInstant = (iState, cb) => {
  const [state, setState] = React.useState(iState);

  React.useLayoutEffect(() => cb(state), [state, cb]);

  return [state, setState];
};

export {useStateCbInstant}; // useStateCallbackInstant

export default useStateCb; // useStateCallback

/** USAGE:
import useStateCb from './utils/hooks/useStateCb';

const App = () => {
  const [count, setCount] = useStateCb(0, count => {
    if (count > 1) {
      console.log('Threshold of over 1 reached.');
    } else {
      console.log('No threshold reached.');
    }
  });
  return (
    <div>
      <p>{count}</p>
      <button type="button" onClick={() => setCount(count + 1)}>
        Increase
      </button>
    </div>
  );
};
*/