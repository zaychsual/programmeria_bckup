import React from 'react';
// https://usehooks.com/useEventListener/

export default function useEvent(eventName, handler, el = window){
  // Create a ref that stores handler
  const savedHandler = React.useRef();
  
  // Update ref.current value if handler changes.
  // This allows our effect below to always get latest handler ...
  // ... without us needing to pass it in effect deps array ...
  // ... and potentially cause effect to re-run every render.
  React.useEffect(() => {
    savedHandler.current = handler;
  }, [handler]);

  React.useEffect(
    () => {
      // Make sure element supports addEventListener
      // On 
      const isSupported = el && el.addEventListener;
      if(!isSupported) return;
      
      // Create event listener that calls handler function stored in ref
      const eventListener = event => savedHandler.current(event);
      
      // Add event listener
      el.addEventListener(eventName, eventListener);
      
      // Remove event listener on cleanup
      return () => {
        el.removeEventListener(eventName, eventListener);
      };
    },
    [eventName, el] // Re-run if eventName or element changes
  );
};