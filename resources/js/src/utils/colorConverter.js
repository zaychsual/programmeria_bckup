// 
// RGBA to HEX = https://jsfiddle.net/Mottie/xcqpF/1/light/
function rgb2hex(rgb, hash){
  rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
  return (rgb && rgb.length === 4) ? (hash ? "#" : "") +
   ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
   ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
   ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
}

// HEX to RGB = https://convertingcolors.com/blog/article/convert_hex_to_rgb_with_javascript.html
// function hex2Rgb(hex){
//   if(hex){
//     let aRgbHex = hex.match(/.{1,2}/g);
//     let rgb = [
//       parseInt(aRgbHex[0], 16),
//       parseInt(aRgbHex[1], 16),
//       parseInt(aRgbHex[2], 16)
//     ];
//     return {
//       arr: rgb, 
//       rgb: rgb.join(',')
//     };
//   }
//   return false;
// }

// https://gist.github.com/comficker/871d378c535854c1c460f7867a191a5a#file-hex2rgb-js
const hex2Rgb = str => {
  const RGB_HEX = /^#?(?:([\da-f]{3})[\da-f]?|([\da-f]{6})(?:[\da-f]{2})?)$/i;
  const [ , short, long ] = String(str).match(RGB_HEX) || [];

  let arr;
  if (long) {
    const value = Number.parseInt(long, 16);
    arr = [ value >> 16, value >> 8 & 0xFF, value & 0xFF ];

    const [ r, g, b ] = arr;
    return {
      arr, 
      obj: { r, g, b }, 
      rgb: arr.join(',')
    };
  } else if (short) {
    arr = Array.from(short, s => Number.parseInt(s, 16)).map(n => (n << 4) | n);
    const [ r, g, b ] = arr;

    return {
      arr, 
      obj: { r, g, b }, 
      rgb: arr.join(',')
    };
  }
};

export {
  rgb2hex, 
  hex2Rgb, 
}
