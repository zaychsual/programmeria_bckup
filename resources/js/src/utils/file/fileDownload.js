// === File Download ====
// https://github.com/kennethjiang/js-file-download/blob/master/file-download.js

import {isStr, setAttr} from '../Q';// ../../utils

export default function fileDownload(data, filename, mime, bom){
	if(data){
		let A = document.createElement('a');

		function setLink(link){
			setAttr(A, {
				display:'none', 
				href:link, 
				download:filename
			});
			// A.style.display = 'none';
			// A.href = blobURL;
			// A.setAttribute('download', filename);

			// Safari thinks _blank anchor are pop ups. We only want to set _blank
			// target if the browser does not support the HTML5 download attribute.
			// This allows you to download files in desktop safari if pop up blocking is enabled.
			if(typeof A.download === 'undefined') setAttr(A, {target:'_blank'}); // A.setAttribute('target', '_blank');

			document.body.appendChild(A);
			A.click();
		}
		
		if(isStr(data)){
			setLink(data);
			document.body.removeChild(A);
		}else{
			let blobData = (typeof bom !== 'undefined') ? [bom, data] : [data];
			let blob = new Blob(blobData, {type: mime || 'application/octet-stream'});
			
			console.log('blobData: ', blobData);
			console.log('blob: ', blob);
			
			if(typeof window.navigator.msSaveBlob !== 'undefined'){
				// IE workaround for "HTML7007: One or more blob URLs were 
				// revoked by closing the blob for which they were created. 
				// These URLs will no longer resolve as the data backing the URL has been freed."
				window.navigator.msSaveBlob(blob, filename);
			}
			else{
				let blobURL = window.URL.createObjectURL(blob);
				
				setLink(blobURL);

				// Fixes "webkit blob resource error 1"
				setTimeout(() => {
					document.body.removeChild(A);
					window.URL.revokeObjectURL(blobURL);
				}, 1)
			}
		}
	}
}
