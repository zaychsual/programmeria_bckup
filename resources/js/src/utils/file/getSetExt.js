// ====== Get File Extension ======
// OPTION 1
// function getFileExtension1(filename) {
//   return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
// }

// OPTION 2
// function getFileExtension2(filename) {
//   return filename.split('.').pop();
// }

// OPTION 3
// function getFileExtension3(filename) {
//   return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
// }

/** @fname = filename to get extension (String)
    @bool = return boolean (Boolean / Number = 1 For true) */

function getExt(fname, bool){
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

/** fname = file name */
function setExt(fname){
	// let ext = f.split('.').pop().toLowerCase();
	let ext = getExt(fname).toLowerCase();
	switch(ext){
		case 'sass':
			return 'scss';
		case 'jsx':
			return 'js';
		case 'tsx':
			return 'ts';
		case 'bas':
		case 'cls':
		case 'frm':
		case 'frx':
		case 'vba':
		case 'vbhtml':
		case 'vbs':
			return 'vb';
		default:
			return ext;
	}
}

export { getExt, setExt }