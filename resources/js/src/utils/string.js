// String Utilities

// Validate
const windowsNames = () => (/^(con|prn|aux|nul|com[0-9]|lpt[0-9])$/i);
const filenameReservedRegex = () => (/[<>:"\/\\|?*\x00-\x1F]/g);

const validFilename = str => {
	if (!str || str.length > 255) {
		return false;
	}

	if (filenameReservedRegex().test(str) || windowsNames().test(str)) {
		return false;
	}

	if (/^\.\.?$/.test(str)) {
		return false;
	}

	return true;
};

const toCapital = s => s[0].toLocaleUpperCase() + s.slice(1);

function kebabToPascal(str){
  if(str === null || str === void 0) return '';
  if(typeof str.toString !== 'function') return '';

  let input = str.toString().trim();
  if(input === '') return '';
  if(input.length === 1) return input.toLocaleUpperCase();

  let match = input.match(/[a-zA-Z0-9]+/g);
  if(match) return match.map(m => toCapital(m)).join('');

  return input;
}

/*
function kebabToCamel(str){
  // let arr = str.split('-');
  // let capital = str.split('-').map((item, index) => index ? item.charAt(0).toUpperCase() + item.slice(1).toLowerCase() : item);
  // ^-- change here.
  // let capitalString = capital.join("");

  return str.split('-').map((item, index) => index ? item.charAt(0).toUpperCase() + item.slice(1).toLowerCase() : item).join("");
}
*/

// Insert String before some String
/** 
 * @USAGE :
 * strInsertBefore(setHtml (this is string to append), "This is string to before insert", "This is string to insert"); // 
 * 
*/
function strInsertBefore(str, strBefore, strInsert){
	let n = str.lastIndexOf(strBefore);
	if(n < 0) return str;
	return str.substring(0,n) + strInsert + str.substring(n);    
}

// Get String in src img tag
// var str = "<img alt='' src='http://api.com/images/UID' /><br/>Some plain text<br/><a href='http://www.google.com'>http://www.google.com</a>";

// var regex = /<img.*?src='(.*?)'/; // /^<\s*img[^>]+src\s*=\s*(["'])(.*?)\1[^>]*>$/
// var src = regex.exec(str)[1];

// console.log(src);

// var im = 'im';
// var src2 = regex.exec(str)[1];

export {
	validFilename, 
  toCapital, 
  kebabToPascal, 
  strInsertBefore, 

};