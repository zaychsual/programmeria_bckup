// ====== CssToReact - https://github.com/staxmanade/CssToReact ======

// Transform implementation or originally thanks to
// https://github.com/raphamorim/native-css
import cssParser from 'css';

function transformRules(self, rules, result){
	rules.forEach(rule => {
		let obj = {};
		if(rule.type === 'media'){
			let name = mediaNameGenerator(rule.media);
			// let media = result[name] = result[name] || {
				// "__exp__": rule.media
			// };
			// "__expression__": rule.media
			
			let media = result[name] = result[name] || {};
			// console.log(media);
			
			transformRules(self, rule.rules, media);
		}
		else if(rule.type === 'rule'){
			rule.declarations.forEach(declaration => {
				if(declaration.type === 'declaration'){
					// let cleanProperty = cleanPropertyName(declaration.property);
					obj[declaration.property] = declaration.value; // obj[cleanProperty] = declaration.value;
				}
			});
			
			/* rule.selectors.forEach(selector => {
				console.log(selector.trim());
				// let name = nameGenerator(selector.trim());
				result[selector] = obj; // result[name] = obj;
			}); */
			
			result[rule.selectors.join(',\n')] = obj; // rule.selectors
		}
	});
}

// turn things like 'align-items' into 'alignItems'
// const cleanPropertyName = (name) => {
	// name = name.replace(/(-.)/g, (v) => {
		// return v[1].toUpperCase();
	// });
  // return name;
// }

const mediaNameGenerator = (name) => {
  return '@media ' + name;
};

/* const nameGenerator = (name) => {
	name = name.replace(/\s\s+/g, ' ');
	// name = name.replace(/[^a-zA-Z0-9]/g, '_');
	name = name.replace(/^_+/g, '');
	name = name.replace(/_+$/g, '');

	return name;
}; */

export function transform(inputCssText){
  if(!inputCssText){
    throw new Error('missing css text to transform');
  }

  // If the input "css" doesn't wrap it with a css class (raw styles)
  // we need to wrap it with a style so the css parser doesn't choke.
  let bootstrapWithCssClass = false;
  if(inputCssText.indexOf("{") === -1){
    bootstrapWithCssClass = true;
    inputCssText = `.bootstrapWithCssClass { ${inputCssText} }`;
  }

  let css = cssParser.parse(inputCssText);
  let result = {};
  transformRules(this, css.stylesheet.rules, result);

  // Don't expose the implementation detail of our wrapped css class.
  if(bootstrapWithCssClass){
    result = result.bootstrapWithCssClass;
  }

  return result;
}