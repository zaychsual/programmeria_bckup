import React from 'react';
import axios from 'axios';
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';
import Barcode from 'react-barcode';

import NetWork from '../components/q-ui-react/NetWork';
import Form from '../components/q-ui-react/Form';
import Btn from '../components/q-ui-react/Btn';
import { confirm } from '../components/react-confirm/util/confirm';// , confirmComplex

const EXPORT_TO = [
  {txt:"Excel", fn:() => console.log('Excel')},
  {txt:"Pdf", fn:() => console.log('Pdf')},
  {txt:"Word", fn:() => console.log('Word')},
];

export class Pos extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      loadingFirst: 0,
      tab: "Sales", 
      brand: "",
      type: "",
      price: "",
      code: "",
      qty: 0, // ""

      barcode: null,
      printBarcode: false,
      products: [],
    };

  }

  componentDidMount(){
    console.log('%ccomponentDidMount in Pos','color:yellow;');
  }

  onSave = e => {
    let et = e.target;
    if(et.checkValidity()){
      const { products, brand, type, price, code, qty, loadingFirst } = this.state;

      // DEV GENERATE BARCODE string
      let d = Date.now().toString().split("");
      d.slice(d.pop());
      const generateCode = d.join("");
      // const generateCode = Math.round(Math.random() * 10000000000000);

      const checkCode = products.find(v => v.code === code);
      // console.log('checkCode: ',checkCode);

      if(checkCode){
        alert(`Product Code, must unique`);
      }
      else{
        const newData = {
          id: products.length + 1,
          barcode: generateCode, 
          brand, type, price, 
          code, qty
        };

        if(loadingFirst < 1){
          this.setState({
            brand: "",
            type: "",
            price: "",
            code: "",
            qty: 0,
            barcode: generateCode
          }, () => this.onTab("Products", newData));
        }else{
          this.setState(s => ({
            brand: "",
            type: "",
            price: "",
            code: "",
            qty: 0,
            barcode: generateCode,
            products: [...s.products, newData]
          }));
        }
      }
    }
  }

  deleteProduct = async (id) => { //  
		const options = {
			type: "toast",
			// title: "Title",
			// toastInfoText: "11 min ago",
			// icon: <i className="fab fa-react mr-2" />, 

			bodyClass: "text-center bold", 
			btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next", 

			modalProps: {
				centered: true, 
				// modalClassName: "modal-right", // up | down | left | right
				// returnFocusAfterClose: false, 
			}
		};

		if(await confirm("Do you Want to delete these product?", options)){
			this.setState(s => ({ products: s.products.filter(v => v.id !== id) }))
		}else{
			console.log('NO');
		}
	}

  toggleBarcode = (barcode, code) => {
    this.setState({
      printBarcode: true,
      barcode, 
      code
    });
  }

  onTab = (tab, add) => {
    const { url } = this.props;
    const { loadingFirst } = this.state;

    if(loadingFirst < 1 && tab === "Products" && url){
      // if(url){
        axios.get(url).then(r => {
          console.log(r);
          if(r.status === 200){
            const setData = add ? [...r.data, add] : r.data;
            this.setState({
              products: setData, 
              loadingFirst: 1 // loadingFirst + 1
            }, () => {
              // Set Focus if click search button in nav tab first open Products Tab
              if(!add && document.activeElement.tagName === "LABEL") this.findRef.focus();
            });
          }
        }).catch(e => console.log(e));
      // }
    }

    this.setState({ tab });
  }

  render(){
    const {className} = this.props;
    const { tab, brand, type, price, code, qty, barcode, products, printBarcode } = this.state;

    return (
      <NetWork>
        {network => (
          <Tab.Container 
            mountOnEnter 
            id="pos-tabs" 
            // defaultActiveKey="Sales" 
            activeKey={tab} 
            onSelect={k => this.onTab(k)}
          >
            <div className={Q.Cx("card shadow-sm pos-app", className)}>
              <div className="card-header d-flex align-items-center">
                <h6 className="mb-0">Point Of Sales - Connection: {`${network.online}`}</h6>

                <Nav variant="tabs" className="ml-auto link-sm nav-card-head">
                  {/* <Nav.Link eventKey="Products" as="label" htmlFor="findProductInput" 
                    role="button" className="tab-noactive mb-0 px-2 fal fa-search" 
                    tabIndex="0" 
                    title="Search Product"
                  /> */}
                  <Nav.Link eventKey="Sales" as="button" type="button">Sales</Nav.Link>
                  <Nav.Link eventKey="Products" as="button" type="button">Products</Nav.Link>
                  <Nav.Link eventKey="addProduct" as="button" type="button">Add Product</Nav.Link>

                  <div className="d-flex flex-row align-items-center ml-1">
                    <Nav.Link eventKey="Products" as="label" 
                      // className="tab-noactive mb-0 px-2 fal fa-search"
                      bsPrefix="btn btn-sm btn-flat fal fa-search tip tipL" 
                      htmlFor="findProductInput" 
                      role="button" 
                      tabIndex="0" 
                      aria-label="Search Product"
                    />
                    <Btn size="sm" kind="flat" className="fal fa-cog tip tipL" aria-label="Settings" />
                    <Btn size="sm" kind="flat" className="fal fa-info-circle tip tipL" aria-label="Info" />
                  </div>
                </Nav>
              </div>

              <Tab.Content className="card-body">
                <Tab.Pane eventKey="Sales">
                  <Form noValidate className="col-lg-8 mx-auto py-2">
                    <button className="btn btn-block btn-lg btn-outline-dark fal fa-barcode-read q-mr" type="button"> Scan Barcode</button>
                    <h5 className="hr-h my-3">OR</h5>

                    <div className="card p-4 shadow-sm">
                      <h6 className="hr-h mb-4">Input Product</h6>
                      
                      <div className="form-group row">
                        <label htmlFor="inputCode" className="col-sm-3 col-form-label col-form-label-sm">
                          Product Code
                        </label>
                        <div className="col-sm-9">
                          <input type="text" className="form-control form-control-sm" id="inputCode" 
                            required 
                            // value={brand} 
                            // onChange={e => this.setState({brand: e.target.value})}
                          />
                          <div className="invalid-feedback">Product Code is Required</div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label htmlFor="inputQuantity" className="col-sm-3 col-form-label col-form-label-sm">
                          Quantity
                        </label>
                        <div className="col-sm-9">
                          <input type="number" className="form-control form-control-sm" id="inputQuantity" 
                            required 
                            min={0} 
                            // value={qty} 
                            // onChange={e => this.setState({qty: e.target.value})}
                          />
                          <div className="invalid-feedback">Quantity is Required</div>
                        </div>
                      </div>

                      {/* MORE CUSTOM */}

                      <div className="text-right">
                        <Btn type="submit">Submit</Btn>
                      </div>
                    </div>
                  </Form>
                </Tab.Pane>

                <Tab.Pane eventKey="Products">
                  <div className="nav mb-2 ml-1-next">
                    <Dropdown className="ml-auto">
                      <Dropdown.Toggle size="sm" variant="info" disabled={products.length < 1}>Export to</Dropdown.Toggle>
                      <Dropdown.Menu>
                        {EXPORT_TO.map((v, i) => 
                          <Dropdown.Item key={i} onClick={v.fn} as="button" type="button">{v.txt}</Dropdown.Item>
                        )}
                      </Dropdown.Menu>
                    </Dropdown>

                    <label className="input-group input-group-sm w-auto mb-0" role="group">
                      <input ref={n => this.findRef = n} 
                        disabled={products.length < 1} 
                        type="text" 
                        className="form-control" 
                        placeholder="Search" 
                        id="findProductInput" 
                      />
                      <div className="input-group-append">
                        <Btn As="div" outline kind="secondary" className="fal fa-search" tabIndex="0" />
                      </div>
                    </label>
                  </div>

                  <div className="table-responsive">
                    <table className="table table-sm table-bordered table-striped table-hover">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Code</th>
                          <th>Barcode</th>
                          <th>Brand</th>
                          <th>Type</th>
                          <th>Price</th>
                          <th>Qty</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {products.map((v, i) => 
                          <tr key={i}>
                            <td>{v.id}</td>
                            <td>{v.code}</td>
                            <td>{v.barcode}</td>
                            <td>{v.brand}</td>
                            <td>{v.type}</td>
                            <td>{v.price}</td>
                            <td>{v.qty}</td>
                            <td>
                              <Btn As="div" onClick={() => this.deleteProduct(v.id)} size="xs" kind="danger" className="fa fa-trash" role="button" title="Delete" />{" "}
                              <Btn As="div" size="xs" kind="info" className="fa fa-pencil" role="button" title="Edit" />{" "}
                              <Btn As="div" outline size="xs" kind="dark" onClick={() => this.toggleBarcode(v.barcode, v.code)} className="fa fa-barcode" role="button" title="Show Barcode" />
                            </td>
                          </tr>
                        )}
                      </tbody>

                    </table>

                    {products.length < 1 && <div className="alert alert-info mb-0">NO DATA</div>}
                  </div>
                </Tab.Pane>

                <Tab.Pane eventKey="addProduct" className="col-lg-8 mx-auto py-2">
                  <h6 className="hr-h mb-4">Add Product</h6>

                  <Form noValidate onSubmit={this.onSave}>
                    <div className="form-group row">
                      <label htmlFor="productName" className="col-md-3 col-form-label col-form-label-sm">
                        Product Name
                      </label>
                      <div className="col-md-9">
                        <input type="text" className="form-control form-control-sm" id="productName" 
                          required 
                          value={brand} 
                          onChange={e => this.setState({brand: e.target.value})}
                        />
                        <div className="invalid-feedback">Product Name is Required</div>
                      </div>
                    </div>

                    <div className="form-group row">
                      <label htmlFor="productType" className="col-md-3 col-form-label col-form-label-sm">
                        Product Type
                      </label>
                      <div className="col-md-9">
                        <input type="text" className="form-control form-control-sm" id="productType" 
                          required 
                          value={type} 
                          onChange={e => this.setState({type: e.target.value})}
                        />
                        <div className="invalid-feedback">Product Type is Required</div>
                      </div>
                    </div>

                    <div className="form-group row">
                      <label htmlFor="productPrice" className="col-md-3 col-form-label col-form-label-sm">
                        Product Price
                      </label>
                      <div className="col-md-9">
                        <input type="text" className="form-control form-control-sm" id="productPrice" 
                          required 
                          value={price} 
                          onChange={e => this.setState({price: e.target.value})}
                        />
                        <div className="invalid-feedback">Product Price is Required</div>
                      </div>
                    </div>

                    <div className="form-group row">
                      <label htmlFor="productCode" className="col-md-3 col-form-label col-form-label-sm">
                        Product Code (SKU)
                      </label>
                      <div className="col-md-9">
                        <input type="text" className="form-control form-control-sm" id="productCode" 
                          required 
                          value={code} 
                          onChange={e => this.setState({code: e.target.value})}
                        />
                        <div className="invalid-feedback">Product Code is Required</div>
                      </div>
                    </div>

                    <div className="form-group row">
                      <label htmlFor="Quantity" className="col-md-3 col-form-label col-form-label-sm">
                        Quantity
                      </label>
                      <div className="col-md-9">
                        <input type="number" className="form-control form-control-sm" id="Quantity" 
                          // required 
                          min={0} 
                          value={qty} 
                          onChange={e => this.setState({qty: e.target.value})}
                        />
                        {/* <div className="invalid-feedback">Quantity is Required</div> */}
                      </div>
                    </div>

                    <div className="text-right">
                      <Btn type="submit">Save</Btn>
                    </div>
                  </Form>
                </Tab.Pane>
              </Tab.Content>

              <Modal 
                show={printBarcode} 
                onHide={() => this.setState({printBarcode: false})}
              >
                <Modal.Header closeButton>
                  <h6 className="modal-title">Barcode Result</h6>
                </Modal.Header>

                <Modal.Body>
                  <div className="d-inline-block p-2 border">
                    <h6 className="mb-0 text-center">{code}</h6>
                    {barcode && 
                      <Barcode 
                        value={barcode} // "123456789012" //  
                        format="EAN13" // CODE128 | EAN13 | CODE39
                        // lineColor="red"
                        // textMargin={-10}
                      />
                    }
                  </div>
                </Modal.Body>
              </Modal>
            </div>
          </Tab.Container>
        )}
      </NetWork>
    );
  }
}

/*
            {[
              {label:"Product Name", id:"productName", val:brand, fn:e => this.setState({brand: e.target.value})},
            ].map((v, i) => 
              <div key={i} className="form-group row">
                <label htmlFor="productName" className="col-sm-2 col-form-label col-form-label-sm">
                  {v.label}
                </label>
                <div className="col-sm-10">
                  <input type="text" className="form-control form-control-sm" id={v.id} 
                    required 
                    value={v.brand} 
                    onChange={v.fn}
                  />
                  <div className="invalid-feedback">Is Required</div>
                </div>
              </div>
            )}
*/