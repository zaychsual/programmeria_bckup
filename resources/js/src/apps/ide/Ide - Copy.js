import React from 'react';

import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import SplitPane from '../../components/react-split-pane';
import { ControlledEditor } from '../../components/monaco-react';// {ControlledEditor} | Editor | IdeCtrl
import onDidMount from '../../components/monaco-react/ide/onDidMount';
import Browser from '../../components/browser/Browser';

import { FolderTree } from './parts/FolderTree';

const setCodeHtml = ({ code }) => `<!DOCTYPE html><html lang="en"><head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
<meta name="theme-color" content="#e3f2fd">
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" href="/favicon.ico">
<title>Programmeria</title>
</head><body class="body-iframe">
<div id="root"></div>

${code ? `<script id="runPlnkr">${code}</script>` : ""}
<script id="windowMsg" src="/js/libs/q/winMsg.js"></script>
</body></html>`;

// <script src="/js/libs/@plnkr-runtime.js" data-js="PlnkrRuntime"></script>

const plnkrSet = `(async function(){
  const host = {
    getCanonicalPath(path){
      if(files[path]) return path;
      if(files[path + ".js"]) return path + ".js";
      return Promise.reject(new Error("File not found " + path));
    },
    getFileContents(canonicalPath){
      return files[canonicalPath];
    }
  };
  
  const runtime = new PlnkrRuntime.Runtime({ host });
  const { markup } = await runtime.import('./index.js');
}());`;

export default class Ide extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      editorReady: false,
      code: "", 
      codeHtml: setCodeHtml({ code:"" })
    };
		this.ideRef = React.createRef();
		this.iframeRef = React.createRef();
  }

  componentDidMount(){
    console.log('%ccomponentDidMount in Ide','color:yellow;');

    // importShim('https://dev.jspm.io/@plnkr/runtime')
    // .then(m => {
    //   console.log('m: ', m);// .default
    // })
    // .then(PlnkrRuntime => {
    //   // Let your imagination go wild
    //   console.log('PlnkrRuntime: ', PlnkrRuntime);
    // });
  }

  onDidChangeEditor = (v, editor) => {
    if(!this.state.editorReady){
      this.ideRef.current = editor;// editor | v
      
      onDidMount(v, editor);// , true
      
			this.setState({ editorReady: true }, () => {
				setTimeout(() => {
					let frame = this.iframeRef.current;// e.target.contentWindow;// contentWindow | contentDocument
					// console.log(frame);
					
					if(frame){
						let win = frame.contentWindow;
						
						console.log('onLoad win: ', win);
						console.log('onLoad window.parent: ', window.parent);
						
						let windowDefine = window.define;
						if(window.monaco && windowDefine){
							window.define = null;
						}
						
						Q.getScript({ src:"/js/libs/@plnkr-runtime.js", "data-js":"PlnkrRuntime" })
						.then(v => {
							if(window.PlnkrRuntime){
								console.log('then window.PlnkrRuntime', window.PlnkrRuntime);
								
								// window.define = windowDefine;
								win.PlnkrRuntime = window.PlnkrRuntime; // window.parent.PlnkrRuntime;
							
								console.log('onLoad after add getScript win: ', win);
							}
						})
						.catch(e => {
							console.log(e);
						});
					}
				}, 1500);
			});
    }
  }

  render(){
    const { className } = this.props;
    const { editorReady, code, codeHtml } = this.state;

    return (
      <Flex dir="column" className={Q.Cx("position-relative h-100 ide", className)}>
        <Flex className="p-1 bg-light border border-bottom-0 ml-1-next">
          <Btn size="sm"
            onClick={() => {
              let code = `
              const files = {
                'package.json': JSON.stringify({
                  dependencies: {
                    react: '16.x',
                    'react-dom': '16.x',
                  },
                }),
                'index.js': \`
                  import React from 'react';
                  import ReactDOM from 'react-dom';
              
                  import Hello from './Hello';
              
                  ReactDOM.render(<Hello name="React" />, document.getElementById('root'));
                \`,
                'Hello.js': \`
                  import React, { Component } from 'react';
              
                  export default class Hello extends Component {
                    render() {
                      return <h1>Hello {this.props.name}</h1>;
                    }
                  }
                \`,
              };`;

              code += plnkrSet;
              // const setCode = setCodeHtml({ code });
              // this.setState({ codeHtml: setCode });//  + plnkrSet
							
							this.iframeRef.current.contentWindow.postMessage(code, "*");
							// return false;
            }}
          >Run</Btn>
        </Flex>

        <Flex className="h-100">
          <div className="flexno position-relative ide-aside" style={{width:200}}>
            <div className="position-absolute position-full ovyauto q-scroll">
              {/* {Array.from({length:30}).map((v, i) => <p key={i}>DUMMY - {i + 1}</p>)} */}

              <FolderTree 

              />
            </div>
          </div>

          <Flex className="position-relative flex-auto">
            <SplitPane
              split="vertical" // {pos} // "horizontal" // vertical
              minSize={50}
              maxSize={document.body.clientWidth - 250} // 1000 | 300
              // defaultSize={100}
              defaultSize="50%" // 
              // size={ideOpen ? "50%" : "100%"} 
              // allowResize={ideOpen} 
              className="border position-absolute" 
              // className={Q.Cx("border position-absolute", themes)} 
              pane1Class="ideEditor" 
              pane2Class="ovhide bg-dark" 
              // pane2Class={`ovhide bg-${themes}`} 
            >
              <ControlledEditor
                // height="calc(100% - 31px)" // calc(100vh - 75px) 
                // className={Q.Cx({'d-none': tools !== 'Compiler'})} 
                editorDidMount={this.onDidChangeEditor} 
                value={code} 
                onChange={(e, code) => {
                  this.setState({ code });
                  // onChange(codeAuto);// OPTION
                }} 
                // language={compilerLang} // ideLang | 'javascript'
                // theme={themes} 
                options={{
                  fontSize: 14, // fontSize
                  tabSize: 2, // tabSize
                  minimap: {
                    enabled: false
                  }
                }}
              />

              {editorReady && 
                <Browser 
									frameRefNative={this.iframeRef} 
                  className="h-100" 
                  html={codeHtml}

                  // htmlBody="<div id='root'></div>" 
                  onLoad={e => {
										/* // this.iframeRef.current.contentWindow
										let win = e.target.contentWindow;// contentWindow | contentDocument
										// console.log(window.parent.document.getElementById('root'));
										console.log('onLoad win: ', win);
										console.log('onLoad window.parent: ', window.parent);
										
										let windowDefine = window.define;
										if(window.monaco && windowDefine){
											window.define = null;
											// delete window.define;
											// window.AMDLoader = null;
										}
										
										Q.getScript({ src:"/js/libs/@plnkr-runtime.js", "data-js":"PlnkrRuntime" })
										.then(v => {
											console.log('then window.PlnkrRuntime', window.PlnkrRuntime);
											
											window.define = windowDefine;
											win.PlnkrRuntime = window.PlnkrRuntime; // window.parent.PlnkrRuntime;
										
											console.log('onLoad after add getScript win: ', win);
										})
										.catch(e => {
											console.log(e);
										}); */
										
										//   let doc = e.target.contentDocument;
										//   console.log('onLoad Browser doc: ', doc);
										//   if(doc){
										//     const plnkr = Q.makeEl('script');
										//     plnkr.innerHTML = ``;

										//     doc.body.appendChild(plnkr);
										//   }
                  }}
                />
              }
            </SplitPane>
          </Flex>
        </Flex>
      </Flex>
    );
  }
}

/*
<React.Fragment></React.Fragment>
*/
