import React from 'react';
import Downshift from 'downshift';
import Dropdown from 'react-bootstrap/Dropdown';

export default function HistoryEntries({
	entries, // history, 
	// location, 
	open, 
	downshiftProps, 
	// downshiftPropsEtc, 
	downshiftMenuProps, 
	dropdownProps, 
	dropdownToggleProps, 
	dropdownMenuProps, 
	inputProps, 
}){
	if(!entries) return null;
	
	// let entries = history.entries;

	return (
		<Downshift 
			{...downshiftProps} 
			// onChange={selection => {
				// alert(selection ? `You selected ${selection.value}` : 'Selection Cleared')
			// }}
			itemToString={item => (item ? item.pathname : "")}
		>
			{({
				// downshiftPropsEtc, // Error???
				getRootProps, 
				getInputProps,
				getItemProps,
				// getLabelProps,
				getMenuProps, 
				isOpen,
				inputValue,
				highlightedIndex,
				selectedItem, 
			}) => (
				<Dropdown 
					{...getRootProps({ ...dropdownProps }, { 
							suppressRefError: true 
						}
					)} 

					show={open || isOpen} 
				>
					<Dropdown.Toggle {...dropdownToggleProps}>
						<input {...getInputProps({ ...inputProps })} />
					</Dropdown.Toggle>

					<div {...getMenuProps({ ...downshiftMenuProps })}>
						<Dropdown.Menu {...dropdownMenuProps}>
							{entries
								.filter(item => !inputValue || item.pathname.includes(inputValue))
								.map((item, index) => (										
									<Dropdown.Item 
										{...Q.DD_BTN} 

										{...getItemProps({
											key: item.pathname,
											index,
											item, 
											active: selectedItem === item, // || highlightedIndex === index
											className: highlightedIndex === index && "highlighted"
											// style: {
												// backgroundColor:
													// highlightedIndex === index ? 'lightgray' : 'white',
												// fontWeight: selectedItem === item ? 'bold' : 'normal',
											// },
										})}
									>
										{item.pathname}
									</Dropdown.Item>
								))
							}
						</Dropdown.Menu>
					</div>
				</Dropdown>
			)}
		</Downshift>
	);
}
	// const HistoryList = ({ history, onClick = Q.noop }) => {
	// 	let entries = history.entries;
	// 	if(entries){
	// 		console.log('entries: ',entries);
	// 		// console.log('window.history: ', window.history);
			
	// 		if(entries.length > 1){
	// 			return (
	// 				history.entries.map((v, i) => 
	// 					<Dropdown.Item key={i} onClick={() => onClick(v)} as="button" type="button">{v.pathname}</Dropdown.Item>
	// 				)
	// 			);
	// 		}
	// 		return null;
	// 	}
	// }

	// const renderDataList = (history) => {
	// 	const entries = history.entries;
	// 	if(entries && entries.length > 1){
	// 		const noDup = [...(new Set(entries.map(v => v.pathname)))];
	// 		console.log('entries: ', entries);
	// 		console.log('noDup: ', noDup);
	// 		// console.log('window.history: ', window.history);
	// 		// [...(new Set(MONACO_LANGS.map(v => v.type)))];
	// 		// [...(new Set(entries.map(v => <option key={v.key} value={v.pathname} />)))];
	// 		return (
	// 			<datalist id={INPUT_ID}>
	// 				{noDup.map((v, i) => <option key={i} value={v} />)}
	// 			</datalist>
	// 		);
	// 	}
	// }

/* <Dropdown // className="position-absolute inset-0" 
	alignRight 
	show={ddPrevLocations} 
	onToggle={(open) => {
		setDdPrevLocations(open);
		if(open) onSetEditPath();
	}}
>
	<Dropdown.Toggle size="sm" variant="light" title="Previous locations">
		
	</Dropdown.Toggle>
	<Dropdown.Menu className="py-1 dd-sm dd-prevlocs"
		popperConfig={{
			modifiers: [{
				name: "offset",
				options: {
					offset: [-30, 0]
				}
			}]
		}}
	>
		<HistoryList history={history} onClick={(path) => setPathType(path.pathname)} />
	</Dropdown.Menu>
</Dropdown> */	