import React, { useContext } from 'react';// , { useState }
import { useHistory } from 'react-router-dom';// Switch, Route, useHistory, useLocation, Redirect, 
// import Dropdown from 'react-bootstrap/Dropdown';

import Aroute from '../../../components/q-ui-react/Aroute';
import Table from '../../../components/q-ui-react/Table';

import { FileManagerContext } from '../context/FileManagerContext';

export default function Dir({
	data, 
	className, 
	As = "section", 
	emptyTxt = "This folder is empty."
}){
	const { themeState, viewState } = useContext(FileManagerContext);// "grid", "list", "details"
	const history = useHistory();
	// console.log('themeState: ', themeState);
	 
// OPTION list view: column-count, column-gap
	return (
		<As className={Q.Cx({ "p-3": viewState !== "details" }, className)}>			
			{Array.isArray(data) ? 
				["grid","list"].includes(viewState) ? 
					<div 
						className={
							Q.Cx({
								"row": viewState === "grid", 
								"card-columns text-truncate": viewState === "list"
							})
						}
					>
						{data.map((v, i) => 
							<div key={i} 
								className={
									Q.Cx({
										"col flex-grow-0": viewState === "grid", 
										// "px-3": viewState === "list", 
									})
								}
							>
								<Aroute 
									noNewTab 
									to={v.path} 
									btn="flat" 
									size="sm" 
									// listGroup={viewState === "details"} 
									className={
										Q.Cx("i-color qi qi-" + (v.type === "file" ? v.ext : "folder"), {
											"mb-2 btn-app qi-3x truncate-line mxh-no": viewState === "grid", 
											"text-left text-truncate mw-100 q-mr": viewState === "list"
										})
									}
									title={"Date created: " + v.created_at + "\nFolders: " + v.name} 
									draggable={true} 
								>
									{v.name}
								</Aroute>
							</div>
						)}
					</div>
					: 
					<Table 
						responsive 
						responsiveStyle={{ height: 'calc(100vh - 97px)' }} 
						responsiveClass="border-top-0"
						fixThead 
						customScroll 
						hover 
						sm 
						border="less" 
						kind={themeState} 
						thead={
							<tr>
								{["Name", "Date modified", "Type", "Size"].map((v, i) => 
									<th key={i} scope="col">{v}</th>
								)}
							</tr>
						}
						tbody={
							data.map((v, i) => 
								<tr key={i} 
									className="cpoin" 
									onClick={() => history.push(v.path)} 
									draggable={true} 
								>
									<td title={v.name} 
										className={"i-color q-mr qi qi-" + (v.type === "file" ? v.ext : "folder")}
									>
										{v.name}
									</td>
									<td title={v.create_at}>{v.create_at}</td>
									<td title={v.type}>{v.type}</td>
									<td title={v.size}>
										{v.size}
									</td>
								</tr>
							)
						}
					/>
				:
				<div className={Q.Cx({ "p-3": viewState === "details" })}>{emptyTxt}</div>
			}
		</As>
	);
}


	