import React, { createContext, useState } from 'react';

const FileManagerContext = createContext();
const { Provider } = FileManagerContext;

const FileManagerProvider = ({ children }) => {
	// const theme = localStorage.getItem("theme");
	
  const [themeState, setThemeState] = useState("light"); // theme || "light"
	const [viewState, setViewState] = useState("grid");
	
	const setApp = ({ theme, view }) => {
		// localStorage.setItem("theme", theme);
		
    // setThemeState({ theme, view });
		if(theme) setThemeState(theme);
		if(view) setViewState(view);
  };
	
	// const themeValue = () => {
    // if(themeState) return themeState;
  // }
	
  return (
    <Provider
      value={{
        themeState, 
				// themeValue, 
				viewState, 
				setAppState: val => setApp(val), 
      }}
    >
      {children}
    </Provider>
  );
};

export { FileManagerContext, FileManagerProvider };