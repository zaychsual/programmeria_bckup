import React, { useContext, useRef, useState, useEffect, Suspense } from 'react';// { useState, useRef, useEffect }
import { Switch, Route } from 'react-router-dom';// , useHistory, useLocation, Redirect, 
import Dropdown from 'react-bootstrap/Dropdown';

import Btn from '../../components/q-ui-react/Btn';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute'; // BtnRoute
// import PageLoader from '../../components/PageLoader';
import RouterMemory from '../../components/browser/RouterMemory';

import HistoryEntries from './parts/HistoryEntries';
import { FileManagerProvider, FileManagerContext } from './context/FileManagerContext';

// const INIT_PATHS = [{ pathname:"/", label:"root", exact:true }];

function App({
  data = INIT_PATHS, 
  routes, 
  className, 
}){
  const { themeState, viewState, setAppState } = useContext(FileManagerContext);

  // const [themes, setThemes] = useState(themeState); // themeValue()
  // const [views, setViews] = useState(viewState);
  // console.log('themeState: ', themeState);
  // console.log('views: ', views);

	const inputTypeRef = useRef(null);
  
  const [paths, setPaths] = useState(null);// NOT FIX: data | null
  const [breadcrumbs, setBreadcrumbs] = useState(data);
	const [initIndex, setInitIndex] = useState(0);
	const [routers, setRouters] = useState(null);// NOT FIX: routes | null
  const [pathType, setPathType] = useState(null);
  const [openDownshift, setOpenDownshift] = useState(false);// For downshift
	const [ctxMenu, setCtxMenu] = useState(null);

  useEffect(() => {
    // console.log('%cuseEffect in FileManager','color:yellow', theme);
    // setRouters(routes);

    if(data && routes){
      setRouters(routes);
      setPaths(data);
      setInitIndex(data.length - 1);
    }

    // setInitIndex(data.length - 1);

  }, [data, routes]);

// e = if use form | val = onKeyUp input
	const onEnterPath = val => {
		// Q.preventQ(e);
		if(val.length > 0){ // pathType | val
			let pathArr = val.split("/");
			pathArr.shift();

			const parsePath = pathArr.map((v, i) => ({ 
				label: data[i]?.label || "", 
				pathname: "/" + v, 
				// exact: data[i]?.exact
			}));

			// let parseRoutes = parsePath.map((v, i) => ({ ...v, component: DIRS[i] }));
			// parseRoutes.push(OBJ_404);

			console.log('onEnterPath val: ', val);
			console.log('onEnterPath pathArr: ', pathArr);// /programmeria/public
			console.log('onEnterPath parsePath: ', parsePath);
			// console.log('onEnterPath parseRoutes: ', parseRoutes);
			// inputTypeRef.current.blur();
			
			setPathType(null);
			setBreadcrumbs(parsePath); // setPaths
			// setRouters(parseRoutes);

			// setTimeout(() => {
				// setRouters(parseRoutes);
				// setTimeout(() => setInitIndex(pathArr.length - 1), 9);
			// }, 9);
		}
	}

	const onSetEditPath = (downshift) => {
    // data[initIndex].pathname | data[0].pathname | paths
		setPathType(breadcrumbs.map(v => v.pathname).join(""));
		setTimeout(() => {
      inputTypeRef.current?.select();
      if(Q.isBool(downshift)) setOpenDownshift(true);
    }, 9);
  }

  const getEntries = (entries) => {
    // return entries.reduce((acc, cur) => {
    //   const x = acc.find(v => v.pathname === cur.pathname);
    //   if(!x){
    //     return acc.concat([cur]);
    //   }
    //   return acc;
    // }, []);

    let path = "";
    return entries.reduce((acc, cur, curIndex, arr) => {
      const x = acc.find(v => v.pathname === cur.pathname);
      if(!x){
        path += arr[curIndex - 1]?.pathname || "";
        return acc.concat([{ ...cur, pathname: path + cur.pathname }]);
      }
      return acc;
    }, []);
  }

  const dark2light = themeState === "dark" ? "light" : "dark";

  if(!paths && !routers){
    return (
      <div>LOADING</div>
    )
  }

	return (
    <RouterMemory 
      // key={initIndex} // DEV OPTION: For re-render when change path typing
      initialEntries={paths} // ["/", "/public", { pathname: "/img" }] 
      initialIndex={initIndex} // 2
      // keyLength={2} 
    >
      {({ history, location }) => (
        <Flex nowrap dir="column" 
          className={
            Q.Cx("file-manager", {
              "bg-dark text-light": themeState === "dark",
            }, className)
          }
        >
          {console.log('history: ', history)}
          {console.log('location: ', location)}
          {/* {console.log('entries: ', getEntries(history.entries))} */}

          <Flex dir="row" className="flexno position-relative ml-1-next">
            <div className="btn-group btn-group-sm">
              <Btn kind={dark2light} outline 
                className="tip tipBL qi qi-arrow-left" 
                disabled={!history.canGo(-1)} 
                aria-label="Back" 
                onClick={history.goBack} 
              />
              <Btn kind={dark2light} outline 
                className="qi qi-arrow-right tip tipBL" 
                disabled={!history.canGo(1)} 
                aria-label="Forward" 
                onClick={history.goForward} 
              />
            </div>

            {Q.isStr(pathType) ? 
              <HistoryEntries 
                open={openDownshift} 
                entries={getEntries(history.entries)} 
                downshiftProps={{
                  onChange: selection => {
                    onEnterPath(selection.pathname);
                    setOpenDownshift(false);
                  }
                }}
                dropdownProps={{
                  // as: "form", 
                  // noValidate: true, 
                  // onSubmit: e => onEnterPath(e), 
                  className: "d-flex flex1"
                }}
                dropdownToggleProps={{ 
                  as: "div",
                  bsPrefix: "w-100" 
                }} 
                dropdownMenuProps={{ 
                  className: "dd-sm py-1 w-100" 
                }} 
                inputProps={{ 
                  ref: inputTypeRef, 
                  className: "form-control form-control-sm shadow-none", 
                  type: "search", 
                  autoFocus: true, 
                  spellCheck: false, 
                  required: true, 
                  onBlur: () => {
                    setPathType(null);
                    setOpenDownshift(false);
                  }, 
                  onKeyDown: e => e.stopPropagation(), 
                  // onKeyUp: e => {
                  //   e.stopPropagation();
                  //   if(e.key === "Enter"){
                  //     setPathType(e.target.value);
                  //     // onEnterPath(e.target.value);
                  //   }
                  //   // console.log('onKeyUp key: ', e.key);
                  //   // console.log('onKeyUp value: ', e.target.value);
                  // },
                  // onChange: e => setPathType(e.target.value) 
                }} 
              />
              : 
              <nav aria-label="breadcrumb" className="flex1 breadcrumb-path">
                <ol 
                  className={
                    Q.Cx("breadcrumb flex-nowrap p-0 mb-0 border", { 
                      "bg-dark text-light": themeState === "dark", 
                      "bg-white": themeState === "light" 
                    })
                  }
                >
                  {breadcrumbs.map((v) => 
                    <li key={v.pathname} className="breadcrumb-item">
                      <Aroute noNewTab size="sm" 
                        exact={v.exact} 
                        strict={v.strict} 
                        to={v.pathname} 
                        // btn={dark2light} outline 
                        btn="flat" 
                        // kind={dark2light} 
                        className="px-1 border-0 zi-1" //  position-relative
                        onContextMenu={e => {
                          Q.preventQ(e);
                          setCtxMenu(v.pathname);
                        }}
                      >
                        {v.label}
                      </Aroute>

                      <Dropdown className="position-absolute r0" // inset-0 
                        show={ctxMenu === v.pathname} 
                        onToggle={() => setCtxMenu(null)}
                      >
                        <Dropdown.Toggle as="div" bsPrefix="btn btn-sm px-1 border-0 position-relative zi--1 shadow-none">{v.label}</Dropdown.Toggle>
                        <Dropdown.Menu 
                          className={Q.Cx("dd-sm py-1", { "dropdown-menu-dark": themeState === "dark" })} 
                          onClick={Q.preventQ}
                        >
                          <Dropdown.Item as="div">Copy address</Dropdown.Item>
                          <Dropdown.Item as="div" onClick={onSetEditPath}>Edit address</Dropdown.Item>
                          <Dropdown.Item as="div">Delete history</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    </li>
                  )}
                  
                  <li onClick={onSetEditPath} className="breadcrumb-item before-no flex1 cpoin"> </li>
                  {/* <li className="breadcrumb-item before-no">
                    <Btn onClick={onSetEditPath} size="sm" kind="flat" className="dropdown-toggle" />
                  </li> */}
                </ol>

                {/* <div className="dropdown-menu dd-sm w-100 show">
                  <button className="dropdown-item" type="button">Action</button>
                </div> */}
              </nav>
            }

            {/* DEVS OPTION: Downshift history entries here */}

            <Btn size="sm" kind={dark2light} outline className="dropdown-toggle" title="Previous locations" 
              onClick={() => onSetEditPath(true)} 
            />

            <Dropdown alignRight>
              <Dropdown.Toggle size="sm" variant={"outline-" + dark2light} bsPrefix="qi qi-bars" title="Options" />
              <Dropdown.Menu 
                className={Q.Cx("dd-sm py-1", { "dropdown-menu-dark": themeState === "dark" })} 
              >
                <h6 className="dropdown-header">View as</h6>
                {["grid", "list", "details"].map((v, i) => 
                  <Dropdown.Item key={i} {...Q.DD_BTN} className="text-capitalize" 
                    active={viewState === v} 
                    onClick={() => setAppState({ view: v })} // onChangeAppState
                  >
                    {v}
                  </Dropdown.Item>
                )}

                <hr className={"border-" + (dark2light)} />
                
                <h6 className="dropdown-header">Theme</h6>
                {["dark", "light"].map((v, i) => 
                  <Dropdown.Item key={i} {...Q.DD_BTN} className="text-capitalize" 
                    active={themeState === v} 
                    onClick={() => setAppState({ theme: v })}
                  >
                    {v}
                  </Dropdown.Item>
                )}
              </Dropdown.Menu>
            </Dropdown>

            <label className="input-group input-group-sm w-auto">
              <input type="search" className="form-control shadow-none" placeholder="Search" />
              <div className="input-group-append">
                <Btn As="div" kind={dark2light} outline className="qi qi-search" />
              </div>
            </label>
          </Flex>
          
          <Flex dir="column" className="flex-auto mnh-100">
            <Suspense 
              // fallback={<PageLoader bottom left={isMobile} w={isMobile ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />}
              fallback={<div>LOADING</div>}
            >
              <Switch>
                {Array.isArray(routers) ? 
                  routers.map((v, i) => 
                    <Route key={i} exact={v.exact} strict={v.strict} path={v.pathname} component={v.component} />
                  )
                  :
                  <Route exact path="/">
                    <div>This folder is empty.</div>
                  </Route>
                }
              </Switch>
            </Suspense>
          </Flex>
        </Flex>
      )}
    </RouterMemory>
	);
}

// data, routes, className
export default function FileManager(props){
  return (
    <FileManagerProvider>
      <App {...props} />
    </FileManagerProvider>
  )
}