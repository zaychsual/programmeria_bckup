import React from 'react';

import Flex from '../../components/q-ui-react/Flex';
import SpinLoader from '../../components/q-ui-react/SpinLoader';// Loader
import SplitPane from '../../components/devs/react-split-pane-2/SplitPane';
import Pane from "../../components/devs/react-split-pane-2/Pane";

// editorReady
export default function ReplWrap({ 
	ready, 
	theme, 
	firstContent, 
	children, 
	bg = "/icon/android-icon-36x36.png", 
	onResetSize = Q.noop,
}){
	const ResetSize = e => {
		let pl = e.target.previousElementSibling;// .clientWidth
		let w = "calc(50% - 0.5px)";
		// console.log('onDoubleClick');
		if(pl.style.width !== w) pl.style.width = w;
		
		onResetSize(e);
	}
	
	return (
		<Flex dir="row" className={Q.Cx("repl", className)}>
			{!ready && <SpinLoader className={"position-absolute bg-" + theme} bg={bg} />}
			
			<SplitPane // className={"bg-ide-" + theme} // style={{}} 
				onDoubleClick={ResetSize} 
			>
				<Pane 
					initialSize="50%" 
					minSize="9%" 
					maxSize="90%" 
					className={"ovhide bg-ide-" + theme} 
				>
					<div className="position-absolute position-full w-100 h-100">
						{firstContent}
					</div>
				</Pane>
			
			</SplitPane>
			
			{children}
		</Flex>
	);
}