import React from 'react';

const errorBoundary = (El, errorCb) => {
	class ErrorBoundary extends React.Component{
		componentDidCatch(error){
			// console.log('ErrorBoundary error: ',error);
			errorCb(error);
		}
		render(){
			if(typeof El === 'undefined') return null;
			return typeof El === 'function' ? React.createElement(El, null) : El;
		}
	}
	return ErrorBoundary;
};

// const RenderCode = ({code, scope = {}}) => {
const evalCode = (code, scope = {}) => {
	// if(code && code.length > 0){ // window.buble && 
		/* {
			objectAssign: "Object.assign"
		} */
		// const parseCode = buble.transform(code.trim());
		const parseCode = window.Babel.transform(code, {
			// ast: true,
			// code: false,
			// retainLines: true,
			// compact: true, // OPTION ISSUE: CAN'T REPLACE function _extends()
			// minified: true, // OK
			// inputSourceMap: false,
			// sourceMaps: false,
			filename: 'file.tsx',
			comments:false,
			presets: ['es2017','react','typescript','flow'], // ,'stage-2' | 'env',
			plugins: [
				['proposal-class-properties'], // , { "loose": true }
				// 'transform-modules-amd',
				// 'transform-modules-commonjs',
				// 'transform-modules-umd',
			],
			// modules:['amd','umd','cjs']
		});
		
		const resultCode = parseCode ? parseCode.code : '';
		// console.log('parseCode: ',parseCode);
		
		const scopeKeys = Object.keys(scope);
		const scopeValues = Object.values(scope);
		const res = new Function('React', ...scopeKeys, `return ${resultCode}`);
		// const ResCall = res(React, ...scopeValues);
		// console.log('ResCall: ', ResCall);
		return res(React, ...scopeValues);// return React.createElement(ResCall); // <ResCall />
	// }
	// return null;
};

const generateEl = (code, scope, errorCb) => {
  return errorBoundary(evalCode(code, scope), errorCb);
};

// const parseCode = (code, scope, setOutput, setError) => {
	// try{
		// const component = generateEl(code, scope, (error) => {
			// setError(error.toString());
		// });
		// setOutput({ component });
		// setError(null);
	// }
	// catch(error){
		// setError(error.toString());
	// }
// };

// Output, Error
const Compiler = ({ scope, Output, Error, className }) => { // code, Placeholder, minHeight
  // const [output, setOutput] = React.useState({component: null});
	// const [error, setError] = React.useState(null);// { where: '', msg: null }

  // React.useEffect(() => {
    // parseCode(code, scope, setOutput, setError);
  // }, [code]);

  // const Element = output.component;
  // const Placeholder = placeholder;

	// {Element ? (<Element />) : Placeholder ? (<Placeholder height={minHeight || 32} />) : null}
	// Output ? <Output /> : Error ? <pre className="alert alert-danger mb-0 rounded-0">{Error}</pre> : null;
			// {Output && <Output.component />}
  return (
		<React.Fragment>
			{Output && <Output />}
			
			{Error && <pre className="alert alert-danger mb-0 rounded-0 vh-100">{Error}</pre>}
		</React.Fragment>
	);
};

export { generateEl };

export default Compiler;
// export default React.memo(Compiler, (prevProps, nextProps) => prevProps.code === nextProps.code);

