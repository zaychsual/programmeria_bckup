import React from 'react';// { useState, useEffect }

// import Flex from '../../../components/q-ui-react/Flex';
// import Loops from '../../../components/q-ui-react/Loops';
import Btn from '../../../components/q-ui-react/Btn';
// import BtnGroup from '../../../components/q-ui-react/BtnGroup';

export function NavIde({ 
	theme, className, btn, active, children, onOpenSet, onCloseTab, 
	tabs = [], 
	onClickTab = Q.noop, 
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
		// console.log('%cuseEffect in NavIde','color:yellow;');
	// }, []);
	
	const onWheelTab = e => {
		let et = e.target,
				c = '.ad-tabfile',
				s = e.deltaY > 1 ? 20 : -20;
		// console.log('onWheelTab deltaY s: ', e.deltaY, s);
		// et.scrollBy(s, 0)
		et.matches(c) ? et.scrollLeft += s : et.closest(c).scrollLeft += s;
	}

//  bg-strip
	return (
		<nav className={Q.Cx("input-group input-group-sm flex-nowrap flexno ovhide zi-3 ad-ide-nav bg-" + theme, theme, className)}>
			{(btn || onOpenSet) && 
				<div className="input-group-prepend mr-0">
					{Q.isFunc(onOpenSet) && <Btn onClick={onOpenSet} kind={theme} className="qi qi-cog" aria-label="Settings" />}
					
					{btn} 
				</div>
			}
			
			<div onWheel={onWheelTab} 
				//  btn-group-sm link-sm small ml-2
				className="btn-group flex-nowrap flex1 mw-100 ml-1px-next q-scroll ad-tabfile"
			>
				{tabs.map((v, i) => (
					Q.isFunc(onCloseTab) ? 
						<div key={i} className={Q.Cx("btn-group btn-group-sm flexno", { "active": active === i })}>
							<Btn onClick={e => onClickTab(v, i, e)} kind={theme} className="ad-tab-name" aria-label={v.name}>{v.name}</Btn>
							<Btn onClick={e => onCloseTab(v, i, e)} kind={theme} className="border-left-0 qi qi-close xx" aria-label="Close" />
						</div>
						: 
						<Btn key={i} onClick={e => onClickTab(v, i, e)} 
							kind={theme} size="sm" 
							active={active === i} 
							className="flexno ad-tab-name" 
							aria-label={v.name}
						>
							{v.name}
						</Btn>
				))}
			</div>
			
			{children}
		</nav>
	);
}

/* NavIde.defaultProps = {
	tabs: [],
	// onOpenSet: Q.noop, // () => {}
	onClickTab: Q.noop
	// onCloseTab: noop // () => {}
}; */

/*
<React.Fragment></React.Fragment>
*/
