import React from 'react';

import SpinLoader from '../../ui-frameworks/q-ui-react/SpinLoader';// Loader
import Flex from '../../ui-frameworks/q-ui-react/Flex';

import Btn from '../../ui-frameworks/q-react-bootstrap/Btn';
import Modal from '../../ui-frameworks/q-react-bootstrap/Modal';
import ModalHeader from '../../ui-frameworks/q-react-bootstrap/ModalHeader';

import SplitPane from '../../components-dev/react-split-pane-2/SplitPane';
import Pane from "../../components-dev/react-split-pane-2/Pane";

// parts:
import {NavIde} from './parts/NavIde';
// , monaco
import {ControlledEditor} from '../../components/monaco-react';// @monaco-editor/react | {ControlledEditor} | Editor | DiffEditor | IdeCtrl
import onDidMount from '../../components/monaco-react/ide/onDidMount';
// import {setDefineWindow} from '../../config/monaco';

import {Cx, domQ} from '../../utils/Q';
import ImportEsm from '../../utils/load/ImportEsm';// ImportEsm, Import
import getScript from '../../utils/load/getScript';
import {copyStyles} from '../../utils/css/copyStyles';

// const calcPxWithPercent = (calc, persen = 50) => Math.round(persen * calc / 100);

const DUMMY_TABS_IDE = [
	{name:"app.js"},{name:"index.js"},{name:"config.js"},{name:"data.js"},{name:"components.js"},
	{name:"utils.js"},{name:"default.js"},{name:"settings.js"},{name:"jquery.js"},{name:"custom.js"},
	{name:"react.js"},{name:"vue.js"},{name:"angular.js"},{name:"lodash.js"},{name:"bootstrap.js"},
];

const setSrcDoc = ({title, css, js, body}) => `<!DOCTYPE html><html lang="en"><head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="theme-color" content="#000000">
<meta name="mobile-web-app-capable" content="yes" />
<title>${title}</title>${css ? css.map(v => `<link rel="stylesheet" href="${v}" crossorigin="anonymous">`) : ""}${js ? js.map(v => `<script src="${v}" crossorigin="anonymous"></script>`) : ""}
</head><body>${body ? body : ""}</body></html>`;

export default class Repl extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			theme: 'dark',// dark | light
			editorReady: false,
			compilerLang: 'javascript',
			
			presets: props.presets,
			
			code: '',
			openConsole: false,
			modal: false,
			
			iframeType: props.type,
    };
		
  }

  componentDidMount(){
		console.log('%ccomponentDidMount in Repl','color:yellow;');
		const {presets} = this.state;// const {babelPreset} = this.props;
		
		if(presets.includes('react')){
			getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js","data-js":"Babel"}).then(v => {
				console.log(v);
			}).catch(e => {
				console.log(e);
			});
		}
		
  }
	
  onDidChangeEditor = (v, editor) => {
		// console.log('v: ',v);
    // console.log('editor: ',editor);
    if(!this.state.editorReady){
      this.setState({editorReady:true});
      // this.ideMain.current = editor;// v | editor
			
			onDidMount(v, editor);
			// console.log('monaco: ', window.monaco);
			
			/* window.monaco.languages.registerCompletionItemProvider('javascript', {
				provideCompletionItems: (model, position) => {
					// find out if we are completing a property in the 'dependencies' object.
					// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
					// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
					// if(!match){
						// return { suggestions: [] };
					// }
					
					// let word = model.getWordUntilPosition(position);
					// let range = {
						// startLineNumber: position.lineNumber,
						// endLineNumber: position.lineNumber,
						// startColumn: word.startColumn,
						// endColumn: word.endColumn
					// };
					return {
						suggestions: setDefineWindow() // range
					};
				}
			}); */
    }
	}
	
	onModal = () => this.setState(s => ({
		modal: !s.modal
	}));
	
	onToggleConsole = () => {		
		this.setState(s => {// (state, props)
			if(s.openConsole){
				let pane = domQ('.paneConsole');
				let h = pane.style.height;
				let def = 'calc(25% - 0.25px)';
				
				if(h !== def){
					console.log('CHANGE height');
					pane.style.height = def;
				}
			}
			
			return {
				openConsole: !s.openConsole
			};
		});
	}
	
	setFrameProps = () => {
		const {frameProps, addStyleParent, onLoad, onMakeCss} = this.props;
		return {
			...frameProps,
			className: Cx("iframe-app", frameProps?.className), // frameProps && frameProps.className
			title: frameProps?.title ?? "Programmeria", // "Programmeria",
			// ambient-light-sensor; camera; hid; vr; 
			allow: "accelerometer; encrypted-media; geolocation; gyroscope; microphone; midi; payment; usb; xr-spatial-tracking",
			// allow-autoplay 
			sandbox: "allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts", 
			onLoad: e => {
				const et = e.target;
				if(onLoad) onLoad(e);
				
				/* if(et.contentWindow){
					console.log('onLoad iframe Browser', et.contentWindow);
					et.contentWindow.addEventListener('popstate', (e) => {
						preventQ(e);
						console.log('Browser iframe popstate', e);
						// e.state is equal to the data-attribute of the last image we clicked
						// window.history.pushState(null, null, window.location.href);
					});
				} */
				
				/* Add style from parent window */
				if(addStyleParent){
					// setTimeout(() => copyStyles(document, et.contentDocument, onMakeCss(e)), 9);
					setTimeout(() => copyStyles(document, et.contentDocument, onMakeCss?.(e)), 9);
					// console.log('onLoad iframe after call copyStyles(): ');// ,e.target.contentDocument
				}
			}
		}
	}

  render(){
		const {className, aside, frameRefNative, frameProps, css, js} = this.props;
		// presets
		const {theme, editorReady, compilerLang, iframeType, code, openConsole, modal} = this.state;
	
		// <Btn onClick={this.onModal}>Copy</Btn>
    return (
			<Flex 
				dir="row" 
				className={Cx("w-100 h-100 repl", className)} 
				// style={{
					// height:'50vh'
				// }}
			>
				{!editorReady && <SpinLoader className={"position-absolute bg-" + theme} bg="/icon/programmeria-36x36.png" />}
			
				{aside && 
					<Flex As="aside"
						style={{
							width:200,
						}}
					>
					
					</Flex>
				}
				
        <SplitPane 
					// className={"bg-ide-" + theme} 
					// style={{}} 
					onDoubleClick={(e) => {
						let pl = e.target.previousElementSibling;// .clientWidth
						let w = "calc(50% - 0.5px)";
						// console.log('onDoubleClick');
						// console.log(pl.style.width);
						if(pl.style.width !== w) pl.style.width = w;
					}}
				>
          <Pane 
						initialSize="50%" 
						minSize="9%" 
						maxSize="90%" 
						className={"ovhide bg-ide-" + theme} 
					>
						<div className="absolute position-full w-100 h-100">
							{editorReady && 
								<NavIde 
									theme={theme} 
									// className=" px-3" 
									tabs={DUMMY_TABS_IDE} // [] | DUMMY_TABS_IDE
									btn={
										<Btn kind={theme} className="q q-play" 
											// onClick={() => this.onCompile(mode, code)} 
											// onClick={() => this.onRun(code)} 
											// disabled={code.length <= 0} // autoRun || !online
											tip="Run Compiler" />
									}
									onCloseTab={(v, i, e) => {
										console.log('onCloseTab v: ',v);
										console.log('onCloseTab i: ',i);
									}}
									onOpenSet={() => {
										this.setState({modal: true})
									}} 
								>
									
								</NavIde>
							}
							
							<ControlledEditor 
								loading={false} 
								height="calc(100% - 31px)" // calc(100vh - 75px) 
								// className={Cx({'d-none': tools !== 'Compiler'})} 
								editorDidMount={this.onDidChangeEditor} 
								value={code}
								onChange={(e, code) => this.setState({code})} 
								language={compilerLang} // ideLang | 'javascript'
								theme={theme} 
								options={{
									fontSize: 14, // fontSize
									tabSize: 2, // tabSize
									// dragAndDrop: false,
									minimap: {
										enabled: false, // minimapEnabled, // Defaults = true
										// side: minimapSide, // Default = right | left
									},
									// mouseWheelZoom: zoomIde
								}}
							/>
						</div>
					</Pane>
					
          <SplitPane split="horizontal" allowResize={openConsole}>
            <Pane 
							// initialSize="75%" 
							// size={openConsole ? "75%":"100%"} 
							// minSize="10%" 
							// maxSize="90%"
							className="flex-column"
						>
							<nav>
								<Btn onClick={this.onToggleConsole}>Console</Btn>
							</nav>
							
							{/* presets.includes('esm') */}
							{iframeType === "native" ? // eslint-disable-next-line
								<iframe 
									{...this.setFrameProps()} 
									ref={frameRefNative} // DEV OPTION
									srcDoc={
										setSrcDoc({
											title: frameProps?.title ?? "Programmeria",
											css: css,
											js: js, 
											
										})
									}
								/>
								: 
								<ImportEsm  
									module="/storage/esm/react-frame-component/ReactFrameComponent.js" 
								>
									{({Module, Err}) => 
										<React.Fragment>
											{Err ? 
												<div>Error load Module</div>
												: 
												<Module.default
													{...this.setFrameProps()} 
													
													contentDidMount={(child) => {
														// setLoad(true);
														// console.log('contentDidMount Frame in Browser child: ', child.props.value.document);
														// DEV OPTION if problem in firefox
														// if(addStyleParent){
															// copyStyles(document, child.props.value.document, onMakeCss(child));
															// console.log('%conLoad iframe after call copyStyles(): ',LOG_DEV);// ,e.target.contentDocument
														// }
														
														// if(onMount) onMount(child, history);// OPTIONS: {history, location}
													}} 
													// OPTION: get componentDidUpdate. prevProps, prevState, snapshot
													contentDidUpdate={(child) => {
														// if(loadCount > 0 && !load){
															// setLoad(true);
															// console.log('contentDidUpdate: ');
														// }
														
														// if(onUpdate) onUpdate(child);
													}} 
												>
													<Module.FrameContextConsumer>
														{
															// Callback is invoked with iframe's window and document instances
															({document, window}) => {
																return <div>ReactFrameNih</div>
															}
														}
													</Module.FrameContextConsumer>
												</Module.default>
											}
										</React.Fragment>
									}
								</ImportEsm>
							}
						</Pane>
						
						<Pane 
							initialSize="25%" // size={openConsole ? "25%":"0"}
							minSize="20px" 
							maxSize="90%" 
							className="flex-column paneConsole" 
						>
							{openConsole && 
								<Flex dir="column" 
									className={Cx("h-100 ovhide", {'bg-white':theme !== 'dark'})} // Cx("h-100 ovhide", {'bg-white':tm === 'light'}) | "h-100 ovhide bg-" + tm
									style={{backgroundColor:theme === 'dark' ? '#242424':null}} 
								>
									<Flex className={"flexno border-bottom bg-" + theme}>
										<Btn kind={theme} size="xs" className="border-0 rounded-0 q q-ban" tip="Clear console" 
											onClick={() => {
												// if(logs.length > 0){
													// setLogs([]);
													// console.clear();
												// }
											}} 
											// disabled={logs.length < 1} 
										/>
										
										<Btn onClick={this.onToggleConsole} // () => this.setState({openConsole: false})
											kind={theme} size="xs" className="border-0 rounded-0 ml-auto q q-close xx" tip="Close" />
									</Flex>
									
									<div className="text-white">
										Console etc
									</div>
								</Flex>
							}
						</Pane>
          </SplitPane>
        </SplitPane>
				
				<Modal 
					isOpen={modal} 
					toggle={this.onModal} 
					modalClassName="modalReplSetting" 
					// size={modalTitle === 'Settings' ? 'lg':'xl'} // "xl" 
					size="lg" 
				>
					<ModalHeader As="h6" toggle={this.onModal} modalTitle="Settings">
						
					</ModalHeader>
					
					<div className="modal-body">
						Setting Content
					</div>
				</Modal>
			</Flex>
    );
  }
}

Repl.defaultProps = {
	presets: [], // babelPreset
	type: "native", // component | native
	
};

/*
<React.Fragment></React.Fragment>
*/
