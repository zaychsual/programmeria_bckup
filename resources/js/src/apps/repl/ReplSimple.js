import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

import SpinLoader from '../../components/q-ui-react/SpinLoader';// Loader
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import ModalQ from '../../components/q-ui-react/ModalQ';

/** NOTES: MOVE TO q-ui-react */
// import Switch from '../../ui-frameworks/q-react-bootstrap/Switch';

import SplitPane from '../../components/devs/react-split-pane-2/SplitPane';
import Pane from "../../components/devs/react-split-pane-2/Pane";

// parts:
import { NavIde } from './parts/NavIde';
// , monaco
import { ControlledEditor } from '../../components/monaco-react';// @monaco-editor/react | {ControlledEditor} | Editor | DiffEditor | IdeCtrl
import onDidMount from '../../components/monaco-react/ide/onDidMount';
import { MONACO_THEMES, MONACO_LANGS, MONACO_OPTIONS } from '../../data/monaco';// ../../config/monaco | , setDefineWindow

import Browser from '../../components/browser/Browser';

// const calcPxWithPercent = (calc, persen = 50) => Math.round(persen * calc / 100);

// const DUMMY_TABS_IDE = [
	// {name:"app.js"},{name:"index.js"},{name:"config.js"},{name:"data.js"},{name:"components.js"},
	// {name:"utils.js"},{name:"default.js"},{name:"settings.js"},{name:"jquery.js"},{name:"custom.js"},
	// {name:"react.js"},{name:"vue.js"},{name:"angular.js"},{name:"lodash.js"},{name:"bootstrap.js"},
// ];

// const ACCEPT_EXT = [...MONACO_LANGS.map(v => "."+v.ex), '.sass','.jsx','.tsx'];
// const ACCEPT_MIME = [...(new Set(MONACO_LANGS.map(v => v.type)))]; // Array.from(new Set(MONACO_LANGS.map(v => v.type)))
// const INIT_BABEL_PRESETS = ['es2015','es2015-loose','es2016','es2017','stage-0','stage-1','stage-2','stage-3','react','flow','typescript'];
const MONACOLANGS = [ ...(new Set(MONACO_LANGS.map(v => v.name) )) ];

export default class ReplSimple extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			// presets: props.presets,
			editorReady: false,
			theme: props.theme, 
			
			openConsole: false,
			modal: false,
			
			exCssVal: "",
    };
  }

  // componentDidMount(){
	// 	console.log('%ccomponentDidMount in Repl','color:yellow;');
	// 	// const { presets } = this.props;// const {babelPreset} = this.props; // state
		
	// 	// if(presets.includes('react')){
	// 	// 	Q.getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js","data-js":"Babel"})
	// 	// 	.then(v => {
	// 	// 		console.log(v);
	// 	// 	}).catch(e => {
	// 	// 		console.log(e);
	// 	// 	});
	// 	// }
  // }
	
  onDidChangeEditor = (v, editor) => {
    // console.log('editor: ',editor);
    if(!this.state.editorReady){
      this.setState({editorReady:true});
      // this.ideMain.current = editor;// v | editor
			
			onDidMount(v, editor);
			// console.log('monaco: ', window.monaco);
			
			/* window.monaco.languages.registerCompletionItemProvider('javascript', {
				provideCompletionItems: (model, position) => {
					// find out if we are completing a property in the 'dependencies' object.
					// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
					// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
					// if(!match){
						// return { suggestions: [] };
					// }
					
					// let word = model.getWordUntilPosition(position);
					// let range = {
						// startLineNumber: position.lineNumber,
						// endLineNumber: position.lineNumber,
						// startColumn: word.startColumn,
						// endColumn: word.endColumn
					// };
					return {
						suggestions: setDefineWindow() // range
					};
				}
			}); */
    }
	}
	
	onModal = () => this.setState(s => ({
		modal: !s.modal
	}));
	
	onToggleConsole = () => {		
		this.setState(s => {// (state, props)
			if(s.openConsole){
				let pane = Q.domQ('.paneConsole');
				let h = pane.style.height;
				let def = 'calc(25% - 0.25px)';
				
				if(h !== def){
					// console.log('CHANGE height');
					pane.style.height = def;
				}
			}
			return {openConsole: !s.openConsole};
		});
	}
	
	onResetSize = e => {
		let pl = e.target.previousElementSibling;// .clientWidth
		let w = "calc(50% - 0.5px)";
		// console.log('onDoubleClick');
		if(pl.style.width !== w) pl.style.width = w;
	}

  render(){
		const {
			// mode, // DEV: Repl mode
			type, className, // aside, 
			frameProps, frameRefNative, addStyleParent, onLoadIframe, 
			// Compiler type & iframe:
			html, stylesheet, script, autoRun, onClickRun, 
			// Tab:
			tabs, tabActive, onCloseTab, onClickTab, 
			// Edior: theme, onChangeTheme
			lang, code, onChangeCode, onChangeLang, onChangeAutoRun, 
		} = this.props;

		// presets, 
		const { 
			theme, editorReady, openConsole, modal, 
			exCssVal, 
			// info, files, dependencies, externalSrc 
		} = this.state;
	
    return (
			<Flex dir="row" className={Q.Cx("repl", className)}>
				{!editorReady && <SpinLoader className={"position-absolute bg-" + theme} bg="/icon/android-icon-36x36.png" />}

				<SplitPane // className={"bg-ide-" + theme} // style={{}} 
					onDoubleClick={this.onResetSize} 
				>
					<Pane 
						initialSize="50%" 
						minSize="9%" 
						maxSize="90%" 
						className={"ovhide bg-ide-" + theme} 
					>
						<div className="position-absolute position-full w-100 h-100">
							{editorReady && 
								<NavIde 
									theme={theme} 
									// className=" px-3" 
									tabs={tabs} // [] | DUMMY_TABS_IDE 
									active={tabActive} 
									btn={
										<Btn kind={theme} className="qi qi-play" 
											// onClick={() => this.onCompile(mode, code)} 
											onClick={onClickRun} 
											disabled={autoRun} // autoRun || !online
											aria-label="Run Compiler" />
									} 
									onClickTab={onClickTab} 
									onCloseTab={onCloseTab} 
									onOpenSet={() => {
										this.setState({modal: true})
									}} 
								>
									
								</NavIde>
							}
							
							<ControlledEditor 
								loading={false} 
								height="calc(100% - 31px)" // calc(100vh - 75px) 
								// className={Q.Cx({'d-none': tools !== 'Compiler'})} 
								editorDidMount={this.onDidChangeEditor} 
								value={code}
								onChange={(e, code) => onChangeCode(e, code)} // (e, code) => this.setState({code})
								language={lang} // ideLang | 'javascript'
								theme={theme} 
								options={MONACO_OPTIONS} 
							/>
						</div>
					</Pane>
					
					<SplitPane split="horizontal" allowResize={openConsole}>
						<Pane className="flex-column">
							{editorReady && 
								<Browser 
									// historyList={true} 
									className="position-absolute position-full" 
									type={type} // DEFAULT = "native" | "component"
									addStyleParent={addStyleParent} 
									theme={theme} 
									frameProps={frameProps} 
									frameRefNative={frameRefNative} 
									html={html} 
									
									stylesheet={stylesheet} 
									script={script} 
									append={
										<Dropdown className="input-group-append">
											<Dropdown.Toggle variant={theme} bsPrefix="rounded-0 qi qi-ellipsis-v tip tipBR" aria-label="More" />
											<Dropdown.Menu>
												<Dropdown.Item as="button" type="button" onClick={this.onToggleConsole}>{openConsole ? "Close":"Open"} console</Dropdown.Item>
												<Dropdown.Item as="button" type="button">Open in new window</Dropdown.Item>
											</Dropdown.Menu>
										</Dropdown>
									}
									onLoad={e => onLoadIframe(e, script)} 
								>
									{/* {({document, window, frameRef}) => 
										<div>This is ReactFrame</div>
									} */}
								</Browser>
							}
						</Pane>
						
						<Pane 
							initialSize="25%" // size={openConsole ? "25%":"0"}
							minSize="20px" 
							maxSize="90%" 
							className="flex-column paneConsole" 
						>
							{openConsole && 
								<Flex dir="column" 
									className={Q.Cx("h-100 ovhide", {'bg-white':theme !== 'dark'})} // Q.Cx("h-100 ovhide", {'bg-white':tm === 'light'}) | "h-100 ovhide bg-" + tm
									style={{backgroundColor:theme === 'dark' ? '#242424':null}} 
								>
									<Flex className={"flexno border-bottom bg-" + theme}>
										<Btn kind={theme} size="xs" className="border-0 rounded-0 qi qi-ban" aria-label="Clear console" 
											onClick={() => {
												// if(logs.length > 0){
													// setLogs([]);
													console.clear();
												// }
											}} 
											// disabled={logs.length < 1} 
										/>
										
										<Btn onClick={this.onToggleConsole} // () => this.setState({openConsole: false})
											kind={theme} size="xs" className="border-0 rounded-0 ml-auto qi qi-close xx" aria-label="Close" />
									</Flex>
									
									<div className="text-white">
										Console etc
									</div>
								</Flex>
							}
						</Pane>
					</SplitPane>
				</SplitPane>
				
				{editorReady && 
					<ModalQ 
						open={modal} 
						toggle={this.onModal} 
						scrollable 
						modalClassName="modalReplSetting" 
						// size={modalTitle === 'Settings' ? 'lg':'xl'} // "xl" 
						size="lg" 
						// position="modal-up" 
						headProps={{
							tag: "h6"
						}}
						title="Settings" 
						bodyClass="p-0"
						body={
							<Flex className="py-2">
								<div className="col">
									<fieldset className="fset" disabled={false}>
										<legend>Editor</legend>
										<label className="d-block">
											<b className="qi qi-info q-mr tip tipTL chelp" aria-label="Info" /> Theme 
											{/*<select onChange={e => this.setState({theme: e.target.value})} value={theme} // disabled={!online} 
												className="custom-select custom-select-sm mt-1 txt-cap">
												<Loops data={["dark","light"]}>
													{(v,i) => <option value={v}>{v}</option>}
												</Loops>
											</select>*/}
											
											<select className="custom-select custom-select-sm mt-1 txt-cap" 
												value={theme} 
												onChange={e => this.setState({theme: e.target.value})} 
											>
												{MONACO_THEMES.map((v,i) => <option key={i} value={v}>{v}</option>)}
											</select>
										</label>
										
										<label className="d-block">
											Language <div className="qi qi-info float-right tip tipTR" aria-label="Info" />
											<select className="custom-select custom-select-sm mt-1 txt-cap"
												value={lang} 
												onChange={onChangeLang} 
											>
												{MONACOLANGS.map((v,i) => <option key={i} value={v}>{v}</option>)}
											</select>
										</label>
										
										{/* <Switch onChange={onChangeAutoRun} checked={autoRun} 
											// disabled={!online} 
											label="Auto Run" 
										/> */}

										<label className="custom-control custom-switch">
											<input onChange={onChangeAutoRun} checked={autoRun} className="custom-control-input" type="checkbox" />
											<div className="custom-control-label">Auto Run</div>
										</label>
										
									{/* <label className="d-block tip tipTL" aria-label="Info">
										Mode <b className="q q-plus"></b>
										<select onChange={e => this.setState({mode: e.target.value})} value={mode} disabled={!online} 
											className="custom-select custom-select-sm mt-1 txt-cap">
											<Loops data={["app","component","scss"]}>
												{(v,i) => <option value={v}>{v}</option>}
											</Loops>
										</select>
									</label> */}
									</fieldset>
								</div>
								
								<div className="col" // onSubmit={e => et.checkValidity()}
								>
									<fieldset className="fset" disabled={false}>
										<legend>External Source</legend>
										<label className="d-block">
											<b className="qi qi-info q-mr tip tipTL chelp" aria-label="Info" /> CSS 
											<input className="form-control form-control-sm mt-1" 
												onChange={e => this.setState({exCssVal: e.target.value})} 
											/>
										</label>
										
										<label className="d-block">
											<b className="qi qi-info q-mr tip tipTL chelp" aria-label="Info" /> JavaScript  
											<input className="form-control form-control-sm mt-1" />
										</label>
									</fieldset>
								
									{/* <h6>App compiler settings</h6> */}
									
									{/* <h6>Component compiler settings</h6> */}
									
									{/* <fieldset className="fset" disabled={false}>
										<legend>Scss compiler settings</legend>
										<label className="d-block tip tipTL" aria-label="Info">
											Style <b className="qi q-plus"></b>
											<select className="custom-select custom-select-sm mt-1">
												<Loops data={[
													{t:"2 Spaces", v:"  "},
													{t:"4 Spaces", v:"  "},
													{t:"Tab", v:"   "}
												]}>
													{(v,i) => <option value={v.v}>{v.t}</option>}
												</Loops>
											</select>
										</label>
									</fieldset> */}
								</div>
							</Flex>
						}
						foot={
							<>
								<Btn size="sm" kind="dark" onClick={this.onModal}>Close</Btn>{' '}
								<Btn size="sm" // kind="primary"
									onClick={() => {
										// this.onModal();
										if(exCssVal.length > 0){
											
										}
									}}
								>Save</Btn>
							</>
						}
					/>
				}
			</Flex>
    );
  }
}

ReplSimple.defaultProps = {
	// presets: [], // babelPreset
	type: "native", // component | native
	tabs: [], 
	// Editor:
	theme: "dark",// dark | light
	lang: "javascript",	// 
	code: "", 
	autoRun: false, 
	onLoadIframe: Q.noop, 
	onChangeCode: Q.noop, 
	// onChangeTheme: Q.noop, 
	onChangeLang: Q.noop, 
	// onChangeAutoRun: Q.noop, 
};

/*
<React.Fragment></React.Fragment>
*/
