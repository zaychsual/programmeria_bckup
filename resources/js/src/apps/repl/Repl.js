import React, { createRef } from 'react';
import axios from 'axios';
import Collapse from 'react-bootstrap/Collapse';
import Dropdown from 'react-bootstrap/Dropdown';
// import { Hook, Console, Decode } from 'console-feed';

import SpinLoader from '../../components/q-ui-react/SpinLoader';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import ModalQ from '../../components/q-ui-react/ModalQ';

/** NOTES: MOVE TO q-ui-react */
// import Switch from '../../ui-frameworks/q-react-bootstrap/Switch';

import SplitPane from '../../components/devs/react-split-pane-2/SplitPane';
import Pane from "../../components/devs/react-split-pane-2/Pane";
import AntdTree from '../../components/antd/tree/AntdTree';

// parts:
import { NavIde } from './parts/NavIde';
import { ControlledEditor } from '../../components/monaco-react';// @monaco-editor/react | {ControlledEditor} | Editor | DiffEditor | IdeCtrl
import onDidMount from '../../components/monaco-react/ide/onDidMount';
import { MONACO_THEMES, MONACO_OPTIONS } from '../../data/monaco';// MONACO_LANGS , setDefineWindow
import Browser from '../../components/browser/Browser';
import NewWindow from '../../components/react-new-window/NewWindow';

// import { permissions } from '../../utils/clipboard';
import { confirm } from '../../components/react-confirm/util/confirm';

// const ACCEPT_EXT = [...MONACO_LANGS.map(v => "."+v.ex), '.sass','.jsx','.tsx'];
// const ACCEPT_MIME = [...(new Set(MONACO_LANGS.map(v => v.type)))]; // Array.from(new Set(MONACO_LANGS.map(v => v.type)))
// const INIT_BABEL_PRESETS = ['es2015','es2015-loose','es2016','es2017','stage-0','stage-1','stage-2','stage-3','react','flow','typescript'];

const COMPILE_MODE = ["bundler", "js", "scss"];//  | "app", "component", "scss"
const ACCEPT_MIME = ["text/html","text/css","text/scss","text/javascript","text/plain","application/json"];

// const ROLLUP_OPTIONS = {
// 	format: "umd", // amd, cjs, es / esm, iife, umd, system
// 	name: 'Qapp',
// 	version: "1",
// 	amd: {
// 		id: "", // define: ''
// 	},
// 	globals: {
// 		"react": "React",
// 		"react-dom": "ReactDOM"
// 	},
// 	// compact: true
// };

const ASIDE_ID = "repl-aside-" + Q.Qid();// 3
const CONSOLE_ID = "repl-console-" + Q.Qid();
const HEIGHT_1 = "calc(100% - 31px)";// For height Editor & Browser Component

export default class Repl extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			autoCompile: props.autoRun, 

			// presets: props.presets,
			editorReady: false,
			lang: props.mode === "bundler" || props.mode === "js" ? "javascript" : "scss", 
			code: "", // Main editor value

			// Compiler mode:
			// js = JavaScript compiler & bundling
			// scss = Scss to Css 
			modeCompiler: props.mode, // OPTIONS IN: COMPILE_MODE
			theme: props.theme, 
			
			openConsole: false,
			modal: false,
			
			exCssVal: "", 

			// Aside Collapse:
			info: true,
			files: true,
			dependencies: true,
			externalSrc: false,

			// Directory tree data:
			modules: [],
			tabLeft: "Project", 
			tabRight: props.mode === "bundler" ? "Compiler Result":"browser", 
			// logs: [], // For <Console />
			// paste: null, 
			newWindow: false, 
    };
		
		this.ideMain = createRef();
		this.fileRef = createRef();
		this.newWin = null;
  }

  componentDidMount(){
		console.log('%ccomponentDidMount in Repl','color:yellow;');
		const { projectSrc } = this.props;// presets
		// if(presets.includes('react')){
		// }

		const setData =() => {
			if(Q.isStr(projectSrc)){
				axios.get(projectSrc).then(r => {
					console.log('Xhr r: ', r);
					const data = r.data;
					if(r.status === 200 && data){
						const children = data.modules.map((v, i) => ({ 
							title: v.name, // REGEX forder / file name /^[\w.-]+$/.test(".")
							key: v.name + i, 
							icon: <i className={"i-color qi qi-" + Q.fileName(v.name).ext} />
						}));

						console.log('children: ', children);

						const modules = [{ 
							...data,
							key: data.id, 
							children
						}];

						console.log('modules: ', modules);
						loadJsZip();
						this.setState({ modules, libsOk: true });

						// Hook(window.console, log => {
						// 	this.setState(({ logs }) => ({ logs: [...logs, Decode(log)] }))
						// });
					}
				}).catch(e => {
					console.log('Error xhr: ', e);
				})
			}else{
				loadJsZip();
				this.setState({ libsOk: true });

				// Hook(window.console, log => {
				// 	this.setState(({ logs }) => ({ logs: [...logs, Decode(log)] }))
				// });
			}
		}

		const loadJsZip = () => {
			Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"JSZip"}).then(() => {
				if(window.JSZip){
					this.setState({ zip: true });
				}
				else console.log("%cjszip failed", "color:yellow");
			}).catch(e => console.log(e));
		}

		if(window.Babel && !window.rollup){
			let windowDefine = window.define;
			if(window.monaco && windowDefine){
				window.define = null;
			}

			Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js", "data-js":"rollup"}).then(() => {
				if(window.rollup){
					window.define = windowDefine;
					setData();
				}
			}).catch(e => {
				console.log('catch e: ', e);
			});
		}
		else if(window.Babel && window.rollup){
			setData();
		}else{
			// Load rollup & babel
			let windowDefine = window.define;
			if(window.monaco && windowDefine){
				window.define = null;
			}

			Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js", "data-js":"rollup"}).then(() => {
				if(window.rollup){
					Q.getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"}).then(() => {
						if(window.Babel){
							window.define = windowDefine;
							setData();
							// loadJsZip();
						}else{
							// If failed load use jspm.io cdn
							importShim("https://jspm.dev/@babel/standalone").then(m => {
								window.Babel = m.default;// store Babel to window
								window.define = windowDefine;
								
								// loadJsZip();
								setData();
							}).catch(e => console.log('importShim catch: ', e));
						}
					}).catch(e => {
						console.log('catch e: ', e);
					});
				}
			}).catch(e => {
				console.log('catch e: ', e);
			});
		}

		// if(navigator.clipboard.readText){
		// 	console.log('navigator.clipboard.readText support');

		// 	permissions().then(r => {
		// 		console.log("permissions navigator query r: ", r);
		// 		this.setState({ paste: r.state });
		// 	}).catch(e => {
		// 		console.log("Errror navigator query e: ", e);
		// 	});
		// }else{
		// 	console.log('Clipboard readText function not support');
		// }
	}
	
  onDidChangeEditor = (v, editor) => {
		const { editorReady } = this.state;// paste

    if(!editorReady){
      this.setState({ editorReady: true });
      this.ideMain.current = editor;// v | editor
			
			onDidMount(v, editor);
			// console.log('onDidChangeEditor editor: ', editor);

			[
				{id:"replDependencies", label:"Add dependencies", fn: () => this.setState({ modal: true })},
				// {id:"replExportToZip", label:"Export to zip", fn: () => console.log('Export to zip')}, 
				{id:"replUploadfiles", label:"Upload files", 
					fn: () => {
						console.log('Upload files');
						this.fileRef.current.click();
					}
				}, 
				{id:"replNewFile", label:"New file", fn: () => console.log('New file')}, 
				// {id:"replNewDirectory", label:"New directory", fn: () => console.log('New directory')}
			].forEach(v => {
				editor.addAction({
					id: v.id,
					label: v.label,
					// keybindings: [v.KeyMod.CtrlCmd | v.KeyCode.KEY_V],
					contextMenuGroupId: "replMonacoCtxMenu", // 9_cutcopypaste | 1_modification
					run: (editor) => {
						// console.log("Add your custom pasting( code here", editor.getPosition());
						v.fn();
					}
				});
			});

			// if(paste){
			// 	editor.addAction({
			// 		id: "replPaste",
			// 		label: "Paste",
			// 		// keybindings: [v.KeyMod.CtrlCmd | v.KeyCode.KEY_V],
			// 		contextMenuGroupId: "9_cutcopypaste",
			// 		run: (editor) => {
			// 			console.log("Add your custom pasting( code here", editor.getPosition());
			// 			// this.onPasteEditor();
			// 		}
			// 	});
			// }
			// OPTION: Add define / custom window object
			/* window.monaco.languages.registerCompletionItemProvider('javascript', {
				provideCompletionItems: (model, position) => {
					// find out if we are completing a property in the 'dependencies' object.
					// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
					// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
					// if(!match){
						// return { suggestions: [] };
					// }
					
					// let word = model.getWordUntilPosition(position);
					// let range = {
						// startLineNumber: position.lineNumber,
						// endLineNumber: position.lineNumber,
						// startColumn: word.startColumn,
						// endColumn: word.endColumn
					// };
					return {
						suggestions: setDefineWindow() // range
					};
				}
			}); */
    }
	}

	// onPasteEditor = (pos = 0) => {
	// 	let readTxt = navigator.clipboard.readText;
	// 	if(readTxt){
	// 		navigator.clipboard.readText().then(txt => {
	// 			console.log(txt);
	// 			this.setState({ code: txt });
	// 		})
	// 		.catch(e => {
	// 			console.log("Errror navigator readText e: ", e);
	// 			this.onConfirm();
	// 		});
	// 	}
	// 	// else{
	// 	// 	let paste = document.execCommand("paste", false);// , sValue
	// 	// 	console.log('paste: ', paste);
	// 	// }
	// }
	
	onModal = () => this.setState(s => ({ modal: !s.modal }));
	
	onToggleConsole = () => {		
		this.setState(s => {
			if(s.openConsole){
				let pane = Q.domQ("#" + CONSOLE_ID);
				let h = pane.style.height;
				let def = "calc(25% - 0.25px)";
				
				if(h !== def){
					pane.style.height = def;
				}
			}
			return { openConsole: !s.openConsole };
		});
	}
	
	onResetSize = e => {
		let pl = e.target.previousElementSibling;
		let w = "calc(50% - 0.5px)";
		if(pl.style.width !== w) pl.style.width = w;
	}

	onTabLeft = (tab) => {
		let aside = Q.domQ('#' + ASIDE_ID);
		if(aside && aside.clientWidth < 150){
			aside.style.width = "220px";
		}

		this.setState({ tabLeft: tab });
	}

	onLoadIframe = e => {
		const { onLoadIframe } =  this.props;// script

		if(Q.isFunc(onLoadIframe)) onLoadIframe(e); // script
	}

	onClickRun = () => {
		const { onRun } = this.props;

		console.log('onClickRun: ');

		if(Q.isFunc(onRun)) onRun();
	}

	onConfirm = async () => { //  
		const options = {
			type: "toast",
			title: "Title",
			// toastInfoText: "11 min ago",
			// icon: <i className="fab fa-react mr-2" />, 

			size: "sm",  
			bodyClass: "text-center", 
			btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next", 
			// backdrop: false, 
			modalProps: {
				// size: "sm",
				// contentClassName: "HELL", 
				centered: true, 
				// modalClassName: "modal-right", // up | down | left | right
				returnFocusAfterClose: false, 
				// onOpened: () => {
				// 	console.log('onOpened');
				// },
				// onClosed: () => {
				// 	console.log('onClosed');
				// }
			}
		};

		if(await confirm(<h6>Are your sure?</h6>, options)){
			console.log('YES');
		}else{
			console.log('NO');
		}
	}

	renderBtnCollapse = (state, txt) => {
		let s = this.state[state];
		return (
			<Btn block blur size="sm" kind={this.state.theme} active={s} 
				className={"text-left bw-x1 rounded-0 qi qi-chevron-" + (s ? "down":"right")} 
				onClick={() => this.setState({ [state]: !s })}
			>
				{txt}
			</Btn>
		)
	}

  render(){
		const {
			// type, addStyleParent, frameProps, frameRefNative, script
			className, aside,  
			// Compiler type & iframe: html, 
			stylesheet, 
			// Tab: tabs, onClickTab, tabActive, 
			onCloseTab,   
		} = this.props;

		// presets, 
		const { 
			lang, code, 
			modeCompiler, // Repl mode
			autoCompile, 
			theme, libsOk, editorReady, openConsole, modal, exCssVal, 
			info, files, dependencies, externalSrc, modules, tabLeft, tabRight, 
			newWindow, // paste, logs, 
		} = this.state;

		const textWhite = { "text-white": theme === "dark" };
		const tipWhite = { "tip-white": theme === "dark" };
		const iconInfoClass = Q.Cx("float-right qi qi-info q-mr chelp tip tipTR", tipWhite);
	
    return (
			<Flex dir="row" className={Q.Cx("repl bg-ide-" + theme, textWhite, className)}>
				{libsOk ? 
					<>
						<div className={"btn-group-vertical justify-content-start zi-4 bg-" + (theme === "dark" ? "secondary" : "light")}>
							{
								[
									{t:"Project", i:"box"}, 
									{t:"Settings", i:"cog"}
								].map(v => 
									<Btn key={v.t} blur kind={theme} 
										onClick={() => this.onTabLeft(v.t)} 
										active={tabLeft === v.t}  
										className={Q.Cx("flexno border-0 rounded-0 tip tipR qi qi-" + v.i, tipWhite)} 
										aria-label={v.t}  
									/>
								)
							}
						</div>

						<SplitPane theme={theme} split="vertical" resizeClass="l7">
							{aside && // (aside && editorReady) NOTE: Don't change aside props with state in use component
								<Pane id={ASIDE_ID} 
									initialSize="220px" // 270 | 200
									minSize="0px" // 150px | 170
									maxSize="400px" 
									className={"flex-column ovyauto q-scroll pb-3 repl-aside bg-ide-" + theme} 
								>
									<Collapse timeout={1} in={tabLeft === "Project"}>
										<div>
											<Flex dir="column" className="border-top-next">
												<div>
													{this.renderBtnCollapse("info", "Info")}

													<Collapse timeout={90} in={info}>
														<div>
															<div className="p-2">
																<p className="pre-wrap small">{`Project name: 
Programmeria

Create by: 
Husein`}</p>

																{/* {paste && <Btn onClick={this.onPasteEditor}>Paste Dev</Btn>} */}
															</div>
														</div>
													</Collapse>
												</div>

												<div>
													{this.renderBtnCollapse("files", "Files")} 

													<Collapse timeout={90} in={files}>
														<div>
															<div className="btn-group btn-group-xs">
																{
																	[
																		{t:"Export to zip", i:"zip"}, 
																		{t:"Upload files", i:"upload", el: <input ref={this.fileRef} type="file" accept={ACCEPT_MIME} hidden />}, 
																		{t:"New file", i:"file"}, 
																		{t:"New directory", i:"folder"} 
																	].map(v => 
																		<Btn key={v.t} As={v.el ? "label":"button"} kind={theme} 
																			className={Q.Cx("tip tipTL q-fw qi qi-" + v.i, { "btnFile": v.el }, tipWhite)} 
																			aria-label={v.t}
																		>
																			{v.el}
																		</Btn>
																	)
																}
															</div>
															<AntdTree 
																data={modules} 
																// autoExpandParent 
																// defaultExpandParent 
																defaultExpandedKeys={['main.js']} // 
																className={Q.Cx("repl-directory bg-" + theme + " tree-" + theme, textWhite)} 
															/>
														</div>
													</Collapse>
												</div>

												<div>
													{this.renderBtnCollapse("dependencies", "Dependencies")} 

													<Collapse timeout={90} in={dependencies}>
														<div>
															<label className="input-group input-group-xs">
																<input className="form-control border-0 rounded-0" placeholder="Search dependencies" type="text" />
																<div className="input-group-append">
																	<Btn As="div" kind={theme} className="border-0 qi qi-search" />
																	<Btn onClick={this.onModal} kind={theme} className={Q.Cx("qi qi-window-maximize border-0 rounded-0 tip tipTR", tipWhite)} aria-label="Package manager" />
																</div>
															</label>

															{/* <div className="list-group list-group-flush"></div> */}
															<nav className="nav flex-column small">
																<Flex className="nav-link p-0 border-0">
																	<input className={"form-control form-control-sm bg-transparent border-0 rounded-0 text-" + theme} type="text" 
																		readOnly 
																		value="react"
																	/>
																	<div className="btn-group btn-group-sm">
																		<Btn kind={theme} className="qi qi-close border-0 rounded-0" />
																	</div>
																</Flex>

																<a className="nav-link" href="/">Link</a>
															</nav>
														</div>
													</Collapse>
												</div>

												<div>
													{this.renderBtnCollapse("externalSrc", "External src")} 

													<Collapse mountOnEnter timeout={90} in={externalSrc}>
														<div>
															<fieldset disabled={false}>
																<label className="input-group input-group-sm mb-2">
																	<div className="input-group-prepend">
																		<div className="input-group-text bg-transparent rounded-0 border-left-0 px-1 q-fw i-color qi qi-css" />
																	</div>
																	<input className="form-control form-control-sm rounded-0 border-right-0" placeholder="CSS" 
																		value={exCssVal} 
																		onChange={e => this.setState({ exCssVal: e.target.value })} 
																	/>
																</label>

																<label className="input-group input-group-sm">
																	<div className="input-group-prepend">
																		<div className="input-group-text bg-transparent rounded-0 border-left-0 px-1 q-fw i-color qi qi-js" />
																	</div>
																	<input className="form-control form-control-sm rounded-0 border-right-0" placeholder="JavaScript" 
																		// value={exJsVal} 
																		// onChange={e => this.setState({ exJsVal: e.target.value })} 
																	/>
																</label>
															</fieldset>
														</div>
													</Collapse>
												</div>
											</Flex>
										</div>
									</Collapse>

									<Collapse mountOnEnter timeout={1} in={tabLeft === "Settings"}>
										<div className="p-2 mt-2-next">
											<h6 className="hr-h">Settings</h6>
											<fieldset className="fset small" disabled={false}>
												<legend>Editor</legend>
												<label className="d-block">
													Theme <b className={iconInfoClass} aria-label="Info" />													
													<select className="custom-select custom-select-sm mt-1 text-capitalize" 
														value={theme} 
														onChange={e => this.setState({ theme: e.target.value })} 
													>
														{MONACO_THEMES.map((v,i) => <option key={i} value={v}>{v}</option>)}
													</select>
												</label>
												
												{/* <label className="d-block">
													Language <b className={iconInfoClass} aria-label="Info" />
													<select className="custom-select custom-select-sm mt-1 text-capitalize"
														value={lang} 
														onChange={e => this.setState({ lang: e.target.value })} 
													>
														{MONACOLANGS.map((v,i) => <option key={i} value={v}>{v}</option>)}
													</select>
												</label> */}
											</fieldset>

											<fieldset className="fset small" disabled={false}>
												<legend>Compiler</legend>												
												<label className="d-block">
													Mode <b className={iconInfoClass} aria-label="Info" />
													<select className="custom-select custom-select-sm mt-1 text-capitalize" 
														// disabled={!online} 
														value={modeCompiler} 
														onChange={e => this.setState({ modeCompiler: e.target.value })} 
													>
														{COMPILE_MODE.map(v => <option key={v} value={v}>{v}</option>)}
													</select>
												</label>

												{/* <Switch onChange={} checked={autoCompile} // disabled={!online} 
													label="Auto Run" 
												/> */}

												<label className="custom-control custom-switch">
													<input onChange={e => this.setState({ autoCompile: e.target.checked })} checked={autoCompile} className="custom-control-input" type="checkbox" />
													<div className="custom-control-label">Auto Run</div>
												</label>
											</fieldset>
											
											{/* <h6>App compiler settings</h6> */}
											
											{/* <h6>Component compiler settings</h6> */}
											
											{/* <fieldset className="fset small" disabled={false}>
												<legend>Scss compiler settings</legend>
												<label className="d-block">
													Style <b className="q q-plus"></b>
													<select className="custom-select custom-select-sm mt-1">
														{[
															{t:"2 Spaces", v:"  "},
															{t:"4 Spaces", v:"  "},
															{t:"Tab", v:"   "}
														].map((v,i) => <option key={i} value={v.v}>{v.t}</option>)}
													</select>
												</label>
											</fieldset> */}
										</div>
									</Collapse>
								</Pane>
							}

							<SplitPane theme={theme} onDoubleClick={this.onResetSize}>
								<Pane initialSize="50%" minSize="9%" maxSize="90%" className={"ovhide bg-ide-" + theme}>
									<div className="position-absolute position-full w-100 h-100">
										{editorReady && 
											<NavIde 
												theme={theme} 
												// tabs={tabs} 
												// active={tabActive} 
												// onClickTab={onClickTab} 
												onCloseTab={onCloseTab} 
												// onOpenSet={() => {
												// 	this.setState({modal: true})
												// }} 
											>
												<Btn size="sm" kind={theme} className={Q.Cx("tip tipL far fa-play", tipWhite)} 
													// onClick={() => this.onCompile(modeCompiler, code)} 
													onClick={this.onClickRun} 
													disabled={autoCompile} // autoCompile || !online
													aria-label="Run Compiler" 
												/>
											</NavIde>
										}
										
										<ControlledEditor 
											loading={false} 
											className="monacoFixedCtxMenu replEditorMain"
											height={HEIGHT_1} 
											editorDidMount={this.onDidChangeEditor} 
											value={code} 
											onChange={(e, code) => this.setState({ code })} // onChangeCode(e, code) | this.setState({code})
											language={lang} 
											theme={theme} 
											options={MONACO_OPTIONS} 
										/>
									</div>
								</Pane>
								
								<SplitPane theme={theme} split="horizontal" allowResize={openConsole}>
									<Pane className="flex-column">
										{editorReady && 
											<>
												<nav className="btn-group btn-group-sm">
													{
														[
															{s:"browser", i:"browser"}, 
															{s:"Compiler Result", i:"code"} 
														].map(v => 
														<Btn key={v.s} blur className={"py-1 border-transparent rounded-0 qi qi-" + v.i} 
															onClick={() => this.setState({ tabRight: v.s })} 
															active={tabRight === v.s} kind={theme} 
														>
															{v.s}
														</Btn>
													)}
												</nav>

												{
													[
														{
															s: "Compiler Result", 
															el: <div>
																		<Flex wrap align="center" className="flexno small pl-2 ml-1-next">
																			Format : 
																			<div className="btn-group btn-group-sm ml-auto">
																				{[
																					{m:"amd", long:"Asynchronous Module Definition"},
																					{m:"cjs", long:"CommonJS"},
																					{m:"esm", long:"Es Module"},
																					{m:"iife", long:"Immediately Invoked Function Expression"},
																					{m:"umd", long:"Universal Module Definition"},
																					{m:"system", long:"SystemJS"},
																				].map((v, i) => 
																					<Btn key={i} As="b" outline kind={theme === "dark" ? "light":"dark"} 
																						className={Q.Cx("rounded-0 bw-x1 tip tipB", tipWhite)} 
																						aria-label={v.long} 
																						// active={options.format === v} 
																						// onClick={() => this.onClickBundleFormat(v)} 
																					>
																						{v.m}
																						{/* <a href="/" className={iconInfoClass} aria-label={"About " + v.long} /> */}
																					</Btn>
																				)}
																				
																				<Dropdown bsPrefix="btn-group">
																					<Dropdown.Toggle size="sm" variant={"outline-" + (theme === "dark" ? "light":"dark")} className="rounded-0" />
																					<Dropdown.Menu className="fs-14">
																						<Dropdown.Item as="button" type="button">Copy</Dropdown.Item>
																						<Dropdown.Item as="button" type="button">Download</Dropdown.Item>
																					</Dropdown.Menu>
																				</Dropdown>
																				{/* <Btn size="sm" blur outline kind={theme === "dark" ? "light":"dark"} className="rounded-0 bw-x1" 
																					// onClick={this.onCopy} 
																				>Copy</Btn> */}
																			</div>
																		</Flex>

																		<ControlledEditor 
																			loading={false} 
																			height="calc(100% - 70px)" // {HEIGHT_1} 
																			className="position-absolute b0 l0 r0" 
																			// value={output.length > 0 ? output[0].output[0].code : ""} 
																			// onChange={(e, value) => this.setState({code: value})} 
																			// language="javascript" 
																			theme={theme} 
																			options={MONACO_OPTIONS} 
																		/>
																	</div>
														}, {
															s: "browser", 
															el: <Browser 
																		style={{ height: HEIGHT_1 }} 
																		className="position-absolute b0 l0 r0" // position-full
																		// type={type} // DEFAULT = "native" | "component" 
																		// addStyleParent={addStyleParent} 
																		theme={theme} 
																		frameProps={{
																			name: "iframeBrowser"
																			// className: "repl-web-programming",
																			// title: "Test title",
																		}} 
																		// frameRefNative={frameRefNative} 
																		// html={html} 
																		
																		stylesheet={stylesheet} 
																		// script={script} 
																		append={
																			<Dropdown className="input-group-append">
																				<Dropdown.Toggle variant={theme} bsPrefix="rounded-0 qi qi-ellipsis-v tip tipBR" aria-label="More" />
																				<Dropdown.Menu className="fs-14">
																					<Dropdown.Item onClick={() => window.open("/", "_blank")} as="button" type="button">Open in new tab</Dropdown.Item>
																					<Dropdown.Item as="button" type="button"
																						onClick={() => {
																							if(newWindow){
																								if(this.newWin) this.newWin.focus();
																							}else{
																								this.setState({ newWindow: true });
																							}
																						}}
																					>
																						Open in new window{/* {newWindow ? "Close":"Open in"} new window */}
																					</Dropdown.Item>
																					<hr/>
																					<Dropdown.Item as="button" type="button" onClick={this.onToggleConsole}>{openConsole ? "Close":"Open"} console</Dropdown.Item>
																				</Dropdown.Menu>
																			</Dropdown>
																		}
																		onLoad={this.onLoadIframe} 
																	/>
														}
													].map(v => 
														<Collapse key={v.s} mountOnEnter timeout={1} in={tabRight === v.s}>
															<div>{v.el}</div>
														</Collapse>
													)
												}
											</>
										}
									</Pane>
									
									<Pane initialSize="25%" minSize="20px" maxSize="85%" 
										// size={openConsole ? "25%":"0"} 
										id={CONSOLE_ID} 
										className="flex-column paneConsole" 
									>
										{openConsole && 
											<Flex dir="column" 
												className={Q.Cx("h-100 ovhide", { "bg-dark text-white": theme === "dark" })}
												// className={Q.Cx("h-100 ovhide", { "bg-white": theme !== "dark" })} // Q.Cx("h-100 ovhide", {'bg-white':tm === 'light'}) | "h-100 ovhide bg-" + tm
												// style={{ backgroundColor: theme === "dark" ? "#242424":null }} 
											>
												<Flex className="flexno border-bottom">
													<Btn kind={theme} size="xs" className="border-0 rounded-0 qi qi-ban" title="Clear console" 
														// onClick={() => {
														// 	if(logs.length > 0){
														// 		this.setState({ logs: [] });
														// 		console.clear();
														// 		// console.log("");
														// 	}
														// }} 
														// disabled={logs.length < 1} 
													/>
													
													<Btn onClick={this.onToggleConsole} kind={theme} size="xs" className="border-0 rounded-0 ml-auto qi qi-close xx" title="Close" />
												</Flex>
												
												<Flex dir="column" grow="1" className="repl-logs">
													{/* <Console logs={logs} variant={theme} />
													
													<ControlledEditor 
														loading={false} 
														height={30} // calc(100% - 70px)
														className="flexno" // position-absolute b0 l0 r0
														// value={} 
														// onChange={(e, value) => this.setState({code: value})} 
														theme={theme} 
														options={{
															...MONACO_OPTIONS, 
															contextmenu: false
														}} 
													/> */}
												</Flex>
											</Flex>
										}
									</Pane>
								</SplitPane>
							</SplitPane>
						</SplitPane>
						
						{editorReady && 
							<>
								<ModalQ scrollable 
									open={modal} 
									toggle={this.onModal}  
									modalClassName="modalRepl" 
									contentClassName={Q.Cx("bg-" + theme, textWhite)} 
									// size={modalTitle === 'Settings' ? 'lg':'xl'} // "xl" 
									size="lg" 
									position="modal-down" // up 
									headProps={{
										tag: "h6"
									}}
									title="Package Manager" 
									bodyClass="p-0"
									body={
										<div>
											{Array.from({length:50}).map((v, i) => <p key={i}>DUMMY - {i + 1}</p>)}
										</div>
									}
									// foot={
									// 	<>
									// 		<Btn size="sm" onClick={this.onModal}>Close</Btn>
									// 	</>
									// }
								/>

								{newWindow && 
									<NewWindow 
										// features={null} 
										url={"/"} 
										// url={newWindow} // "/admin/devs/post-edit" 
										// center="parent" // screen 
										// name="_blank" 
										title="Programmeria" 
										onBlock={() => {
											console.log('onBlock: ');
										}}
										onUnload={() => {
											console.log('onUnload: ');
											if(newWindow){
												console.log('onUnload setState: ');
												this.setState({ newWindow: false });
											}
										}}
										onOpen={(win) => {
											console.log('onOpen win: ', win);
											this.newWin = win;
										}}
									>
										<h1>Hi 👋</h1>
									</NewWindow>
								}
							</>
						}
					</>
					: 
					<SpinLoader className={"position-absolute bg-" + theme} bg="/icon/android-icon-36x36.png" />
				}
			</Flex>
    );
  }
}

Repl.defaultProps = {
	presets: [], // babelPreset
	mode: "bundler", 
	// Editor:
	theme: "dark",// dark | light
	// 
	autoRun: false, 
	onLoadIframe: Q.noop, 
	onChangeCode: Q.noop, 
};

/*
<React.Fragment></React.Fragment>
*/
