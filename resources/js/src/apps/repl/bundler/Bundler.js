import React, { createRef } from 'react';
import json from '@rollup/plugin-json';
import dataUri from '@rollup/plugin-data-uri';

import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';

import BabelStandalone from '../../../components/libraries/BabelStandalone';
import RollupBrowser from '../../../components/libraries/RollupBrowser';
import { dirname, resolve } from './utils/path';
import { supportsInput } from './utils/rollupVersion';
import { ControlledEditor } from '../../../components/monaco-react';
import onDidMount from '../../../components/monaco-react/ide/onDidMount';
import { MONACO_OPTIONS } from '../../../data/monaco';
import { clipboardCopy } from '../../../utils/clipboard';

const ROLLUP_OPTIONS = {
	format: 'umd', // amd, cjs, es / esm, iife, umd, system
	name: 'Qapp',
	version: "1",
	amd: {
		id: "", // define: ''
	},
	globals: {
		"react": "React",
		"react-dom": "ReactDOM"
	},
	// compact: true
};

// DEVS: Handle monaco error
class ErrorBoundary extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			hasError: false
		};
  }

  static getDerivedStateFromError(){// error
    return { hasError: true };
  }

  componentDidCatch(err, info){
		console.log('componentDidCatch err: ', err);
		console.log('componentDidCatch info: ', info);
  }

  render(){
    if(this.state.hasError){
      return <h1>Something went wrong.</h1>;// You can render any custom fallback UI
    }
    return this.props.children; 
  }
}

export default class Bundler extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			editorReady: false, 
			code: "",// string code to <ControlledEditor />
			// files: [],
			activeFile: null,
			libsOk: false, 
      output: [],
      options: { ...ROLLUP_OPTIONS },
      codeSplitting: false,
      selectedProject: undefined,// BEFORE: selectedExample, OPTIONS:  '' | null | undefined | '00'
      selectedProjectModules: [],// BEFORE: selectedExampleModules
			moduleTitle: '',
      modules: [],
      warnings: [],
      // input: '', // null // ???
      error: null,
			modulesBundle: null,
			errApp: null,
    };
		
		this.ideMain = createRef();
  }

  componentDidMount(){
		console.log('%ccomponentDidMount in Bundler','color:yellow;');
		
		/* if(window.Babel && !window.rollup){
			Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js","data-js":"rollup"}).then(() => {
				if(window.rollup){
					Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
						if(window.JSZip) this.setState({ libsOk: true });
						else console.log("%cjszip failed", "color:yellow");
					}).catch(e => console.log(e));
				}
			}).catch(e => {
				console.log('catch e: ', e);
			});
		}
		else if(window.Babel && window.rollup){
			this.setState({ libsOk: true });
		}else{
			Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js","data-js":"rollup"}).then(() => {
				if(window.rollup){
					Q.getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js","data-js":"Babel"}).then(() => {
						if(window.Babel){
							Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
								if(window.JSZip) this.setState({ libsOk: true });
								else console.log("%cjszip failed", "color:yellow");
							}).catch(e => console.log(e));
						}
					}).catch(e => {
						console.log('catch e: ', e);
					});
				}
			}).catch(e => {
				console.log('catch e: ', e);
			});

			// importShim("https://jspm.dev/jszip").then(m => {
			// 	console.log("importShim m: ", m);
			// }).catch(e => console.log("Error importShim e: ", e));
		} */

		// if(window.rollup){
		// 	this.setState({ libsOk: true });
		// }else{
		// 	Q.getScript({src:"/storage/app_modules/rollup/dist/rollup.browser-1.29.1.js","data-js":"rollup"}).then(() => {
		// 		if(window.rollup){
		// 			Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
		// 				if(window.JSZip) this.setState({ libsOk: true });
		// 				else console.log("%cjszip failed", "color:yellow");
		// 			}).catch(e => console.log(e));
		// 		}
		// 	}).catch(e => {
		// 		console.log('catch e: ', e);
		// 	});
		// }

		if(window.JSZip){
			this.setState({ libsOk: true });
		}else{
			Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"jszip"}).then(() => {
				if(window.JSZip) this.setState({ libsOk: true });
				else console.log("%cjszip failed", "color:yellow");
			}).catch(e => console.log(e));
		}
		
		window.addEventListener('error', this.winErr);// DEVS: Handle monaco error
  }
	
	componentWillUnmount(){
		// DEVS: Handle monaco error
		window.removeEventListener('error', this.winErr);
		if(Q.hasClass(document.body, 'monacoErrorNotFix')) Q.setClass(document.body, 'monacoErrorNotFix', 'remove');
	}
	
// DEVS: Handle monaco error
	winErr = (msg) => {// , url, lineNo, colNo, errObj
		console.log('window error msg: ', msg);
		if(msg){
			Q.setClass(document.body, 'monacoErrorNotFix hasErr');// OPTION hide react dev error iframe
		
			setTimeout(() => {
				let xErr = Q.domQ('iframe[style]', document.body);// span[title="Click or press Escape to dismiss."]
				// console.log('xErr: ', xErr);
				// if(xErr) xErr.remove();// xErr.click();
				
				if(xErr && xErr.contentDocument){
					// setTimeout(() => {
						// console.log('xErr.contentDocument: ', xErr.contentDocument);
						let x = Q.domQ('span[title="Click or press Escape to dismiss."]', xErr.contentDocument);
						if(x) x.click();
					// },150);
				}
				Q.setClass(document.body, 'hasErr', 'remove');
			}, 1500);// 5000
			
			this.setState({errApp: msg}, () => {
				// window.alert(msg.message);
			});
		}
	}

	bundleFinal = () => {
		const { modules, activeFile } = this.state;// code, options, selectedProjectModules, 
		const editCodes = modules.map(v => {		
			// if(activeFile && activeFile.name === v.name){//  && code.length > 0
				// return {
					// ...v,
					// code: this.babelTransform(this.state.code) // activeFile.code
				// }
			// }
			const setCode = activeFile && activeFile.name === v.name ? this.state.code : v.code;
			return {
				...v,
				code: this.babelTransform(setCode) // v.code | MUST Check babel transform status
			}
		});
		
		console.log('modules: ', modules);
		console.log('editCodes: ', editCodes);
		
		this.setState({
			// modules: editCodes, // data.modules
			selectedProjectModules: editCodes, // selectModule
			modulesBundle: editCodes
		}, () => {
			this.bundle();// Run rollup bundle
		})
	}
	
	babelTransform = (code) => {
		if(window.Babel){
			try{
				const parseCode = window.Babel.transform(code, {
					// ast: true,
					// code: false,
					// retainLines: true,
					// compact: true, // OPTION ISSUE: CAN'T REPLACE function _extends()
					// minified: true, // OK
					// inputSourceMap: false,
					// sourceMaps: false,
					filename: 'file.tsx',
					comments:false,
					presets: ['es2017','react','typescript','flow'], // ,'stage-2' | 'env',
					plugins: [
						['proposal-class-properties'], // , { "loose": true }
						// 'transform-modules-amd',
						// 'transform-modules-commonjs',
						// 'transform-modules-umd',
					],
					// modules:['amd','umd','cjs']
				});

				const resultCode = parseCode ? parseCode.code : "";
				const strExtendsFn = 'function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }';
				// const strExtendsFn = 'function _extends(){_extends=Object.assign||function(target){for(var i=1;i<arguments.length;i++){var source=arguments[i];for(var key in source){if(Object.prototype.hasOwnProperty.call(source,key)){target[key]=source[key];}}}return target;};return _extends.apply(this,arguments);}';
				
				const replaceExtends = resultCode.replace(strExtendsFn, "");
				const ext2ObjectAssign = replaceExtends.replace(/_extends/gm, 'Object.assign').trim();//
				
				console.log('%cresultCode: ', 'color:yellow', resultCode);
				console.log('%creplaceExtends: ', 'color:yellow', replaceExtends);
				console.log('%cext2ObjectAssign: ', 'color:yellow', ext2ObjectAssign);
				
				return ext2ObjectAssign;
			}
			catch(e){
				console.warn(e);
				return null;
			}
		}else{
			console.log('%cwindow.Babel NOT AVAILABLE: ', 'color:yellow');
		}
	}
	
	bundle = async () => {
		if(window.rollup){
			// rollup, warnings, modules
			const { options, selectedProject, selectedProjectModules, codeSplitting, modulesBundle } = this.state;
			// console.log(`running Rollup version %c${window.rollup.VERSION}`, 'color:yellow;');
			if(selectedProject && selectedProjectModules.length){
				// if(modules.length !== selectedProjectModules.length || selectedProjectModules.some((module, i) => {
				if(modulesBundle.length !== selectedProjectModules.length || selectedProjectModules.some((module, i) => {
					const currentModule = modulesBundle[i];// const currentModule = modules[i];
					return currentModule.name !== module.name || currentModule.code !== module.code || currentModule.isEntry !== module.isEntry;
				})){
					this.setState({
						selectedProject: undefined, // null | ''
						selectedProjectModules: []
					})
				}
			}

			// updateUrl();
			let moduleById = {};
			// modulesBundle | modules
			modulesBundle.forEach(module => {
				moduleById[module.name] = module;
			});

			// warnings = [];
			const inputOptions = {
				plugins: [
					{
						resolveId(importee, importer){
							if(!importer) return importee;
							if(importee[0] !== '.') return false;

							let resolved = resolve(dirname(importer), importee).replace(/^\.\//, '');
							if(resolved in moduleById) return resolved;

							resolved += '.js';
							if(resolved in moduleById) return resolved;

							throw new Error("Could not resolve " + importee + " from " + importer);// throw new Error(`Could not resolve '${importee}' from '${importer}'`);
						},
						load: function(id){
							return moduleById[id].code;
						}
					},
					json(),
					dataUri()
				],
				onwarn (warning){
					// this.setState({warnings: [...warnings, warning]}); // warnings.push(warning);
					console.group(warning.loc ? warning.loc.file : "");
					console.warn(warning.message);

					if(warning.frame){
						console.log(warning.frame);
					}
					if(warning.url){
						console.log("See " + warning.url + " for more information");
					}
					console.groupEnd();
				}
			};
			
			if(codeSplitting){
				inputOptions.input = modulesBundle.filter((module, index) => index === 0 || module.isEntry).map(module => module.name);
			}else{
				inputOptions[supportsInput(window.rollup.VERSION) ? 'input' : 'entry'] = 'main.js';
			}

			try {
				const generated = await (await window.rollup.rollup(inputOptions)).generate(options);
				if(codeSplitting){
					this.setState({
						output: generated.output,
						error: null
					})
				}else{
					this.setState({
						output: [generated],
						error: null
					})
				}
			}catch(err){
				// error = err;
				this.setState({
					error: err
				}, () => {
					const { error } =  this.state;
					if(error.frame) console.log(error.frame);
					setTimeout(() => { throw error });
				});
			}
		}else{
			console.log('%cwindow.rollup NOT load: ', 'color:yellow');
		}
  }
  
  loadCodes = () => {// updateSelectedExample
    const { selectedProject } = this.state;
		if(selectedProject){
			fetch(`/DUMMY/rollup/${selectedProject}/${selectedProject}.json`).then(r => r.json()).then(data => {
				// modules = example.modules;
				// selectedProjectModules = modules.map(module => ({...module}))
				const selectModule = data.modules.map(module => ({...module}));
				console.log('data: ', data);
				console.log('modules: ', data.modules);
				console.log('selectModule: ', selectModule);
				this.setState({
					moduleTitle: data.title,
					modules: data.modules.map(v => ({ ...v, id: v.name + "-" + Q.Qid(3) })), // data.modules
					selectedProjectModules: selectModule
				})
			});
		}
	}	

	addFile = () => {
		const { modules } = this.state;
		let ID = "Untitled-" + Number(modules.length + 1);
		const QID = ID + "-" + Q.Qid(3);

		this.setState(() => {
			return {
				modules: [
					...modules, 
					{
						name: ID,
						code: "",
						isEntry: false,
						// For editable & focus input filename
						edit: true,
						id: QID
					}
				]
			}
		}, () => {
			setTimeout(() => Q.domQ("#" + QID)?.focus(), 9);
		})
	}

	onClickFile = (e, v) => {
		e.stopPropagation();
		if(!v.edit){
			this.setState({
				code: v.code,
				activeFile: v
			}, () => {
				this.ideMain?.current?.focus();
			});
		}
	}

	onChangeFileName = (e, v) => {
		if(v.edit){
			let name = e.target.value;
			this.setState(s => ({
				modules: s.modules.map(v2 => ( v2.name === v.name ? { ...v2, name } : v2 ))
			}))
		}
	}

	onBlurFile = (v) => {
		if(v.edit){
			this.setState(s => ({
				modules: s.modules.map(v2 => ( v2.name === v.name ? { ...v2, edit: false } : v2 ))
			}))
		}
	}

	onClickEdit = (e, v) => {
		e.stopPropagation();
		this.setState(s => ({
			modules: s.modules.map(d => ( d.name === v.name ? { ...d, edit: true } : d ))
		}));
	}

	onRemoveFile = (e, v) => {
		e.stopPropagation();
		this.setState(s => ({
			modules: s.modules.filter(v2 => v2.name !== v.name)
		}), () => {
			if(this.state.activeFile) this.setState({ activeFile: null });
		})
	}

	onRemoveImport = () => {
		const { options, output } = this.state;
		if(options.format === "esm" || options.format === "es"){
			const removeImport = () => {
				if(output.length > 0){
					const finalCode = output[0].output[0].code;
					return finalCode.replace(/import\s+?(?:(?:(?:[\w*\s{},]*)\s+from\s+?)|)(?:(?:".*?")|(?:'.*?'))[\s]*?(?:;|$|)/gm, "");
				}
			}
			
			console.log('removeImport(): ', removeImport());
			// this.setState(s => ({
				// output: [
					// {
						// ...s.output, 
						// code: s.output[0].output[0].code.replace(/import\s+?(?:(?:(?:[\w*\s{},]*)\s+from\s+?)|)(?:(?:".*?")|(?:'.*?'))[\s]*?(?:;|$|)/gm, '')
					// }
				// ]
			// }), () => {
				// console.log('this.state.output: ', this.state.output);
			// });
		}
		// output.length > 0 ? output[0].output[0].code : ''
		// console.log('output: ', output);
	}

	onZip = () => {
		const { modules, moduleTitle } = this.state;

		if(JSZip &&  modules.length){
			let zip = new JSZip();

			// console.log(moduleTitle);
			modules.forEach(v => {
				zip.file(moduleTitle + "/" + v.name, v.code);
			});

			zip.generateAsync({type:"blob"}).then((blob) => {
				console.log("zip: ", blob);

				let url = window.URL || window.webkitURL;
				let data = url.createObjectURL(blob);
				// console.log("objUrl: ", data);

				let a = Q.makeEl('a');
				a.href = data;
				a.rel = "noopener";
				a.download = moduleTitle;

				setTimeout(() => a.click(), 1);
				setTimeout(() => url.revokeObjectURL(a.href), 4E4); // 40s
			});
		}
	}

	onChangeEditor = (e, code) => {
		this.setState({ code }, () => {
			const { modules, activeFile } = this.state;
			
			// console.log('onChange modules: ', modules);
			// console.log('onChange activeFile: ', activeFile);
			
			if(activeFile && modules.length > 0){
				// console.log('edit Modules: ');
				this.setState({ modules: modules.map(v => ( v.id === activeFile.id ? { ...v, code } : v )) });
			}
		});
	}

	onClickBundleFormat = (v) => {
		this.setState(s => ({
			options: { ...s.options, format: v } 
		}), () => {
			if(this.state.modules.length > 0) this.bundleFinal();
		})
	}

	onCopy = () => {
		const { output } = this.state;
		// let code = output.length > 0 ? output[0].output[0].code : "";
		
		if(output.length > 0){
			clipboardCopy(output[0].output[0].code).then(t => console.log('onOk: ')).catch(e => console.log(e));
		}
	}

  onDidChangeEditor = (v, editor) => {
    // console.log('editor: ',editor);
    if(!this.state.editorReady){
      this.setState({ editorReady: true });
      this.ideMain.current = editor;// v | editor
			
			onDidMount(v, editor);
			// console.log('monaco: ', window.monaco);
			
			/* window.monaco.languages.registerCompletionItemProvider('javascript', {
				provideCompletionItems: (model, position) => {
					// find out if we are completing a property in the 'dependencies' object.
					// var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
					// var match = textUntilPosition.match(/"dependencies"\s*:\s*\{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*([^"]*)?$/);
					// if(!match){
						// return { suggestions: [] };
					// }
					
					// let word = model.getWordUntilPosition(position);
					// let range = {
						// startLineNumber: position.lineNumber,
						// endLineNumber: position.lineNumber,
						// startColumn: word.startColumn,
						// endColumn: word.endColumn
					// };
					return {
						suggestions: setDefineWindow() // range
					};
				}
			}); */
    }
	}
	
  render(){
		// selectedProjectModules, modulesBundle, activeFile, 
		const { libsOk, code, modules, moduleTitle, options, output, selectedProject } = this.state;

    return (
			<ErrorBoundary>
				<BabelStandalone load>
					{(Babel) => {
						if(Babel){
							if(!window.Babel){
								window.Babel = Babel;
							}

							return (
								<RollupBrowser load>
									{rollup => {
										if(rollup){
											if(!window.rollup){
												window.rollup = rollup;
											}

											return (
												<Flex dir="row" className="mh-full-navmain appBox">
													<Flex dir="column" className="flexno sticky asideApp" style={{width:190}}>
														<small>Projects</small>
														<select className="custom-select custom-select-sm rounded-0 bw-y1" 
															value={selectedProject} 
															onChange={e => this.setState({ selectedProject: e.target.value })} 
														>
															<option>Choose Project</option>
															{[
																"Dynamic imports","Multiple Entry Modules","Tree-shaking","Default exports","Named exports",
																"External imports","Static namespaces","Dynamic namespaces","React component", 
																"React router dom basic"
															].map((v, i) => 
																<option key={i} value={"0" + i}>0{i}. {v}</option>
															)}
														</select>
														
														<label className="px-1 pb-2 border-bottom small">Package name:
															<input className="form-control form-control-sm" 
																value={moduleTitle} 
																onChange={e => this.setState({ moduleTitle: e.target.value })}
															/>
														</label>
														
														<Btn onClick={this.addFile} size="xs">Add file</Btn>
														
														<div className="list-group list-group-flush py-2 flex-auto ovxhide q-scroll">
															{modules.map((v, i) => 
																<Flex key={i} As="label" align="center" 
																	className="list-group-item list-group-item-action p-0 mb-0 select-no fileItem" 
																>
																	<input type="text" readOnly={!v.edit} value={v.name} 
																		id={v.id || v.name} 
																		className={
																			Q.Cx("form-control form-control-sm border-0 rounded-0 bg-transparent", 
																				{"shadow-none cpoin select-no": !v.edit}
																			)
																		} 
																		onChange={e => this.onChangeFileName(e, v)} 
																		onClick={e => this.onClickFile(e, v)} 										
																		onBlur={() => this.onBlurFile(v)} 
																		onKeyDown={e => {
																			if(e.key === "Enter") this.onBlurFile(v);
																		}}
																	/>
																	
																	<div className="btn-group btn-group-xs mr-1">
																		<Btn onClick={e => this.onClickEdit(e, v)} As="div" hidden={v.edit} 
																			// {"tip tipL qi q-fw qi-" + (v.edit ? "check":"pen")} 
																			className="tip tipL qi qi-pen" 
																			aria-label="Rename" 
																		/>
																		<Btn blur onClick={e => this.onRemoveFile(e, v)} className="tip tipL qi qi-close xx" aria-label="Remove" />
																	</div>
																</Flex>
															)}
														</div>
													</Flex>
												
													<Flex dir="row" grow="1" className="appMain">
														<Flex dir="column" className="w-50">
															<nav className="flexno ml-1-next">
																<Btn onClick={this.loadCodes} size="sm" kind="danger" className="bw-x1">Load Codes</Btn>
																<Btn onClick={this.bundleFinal} disabled={!selectedProject && !(modules.length > 0)} size="sm" kind="warning" className="bw-x1">Bundle Final</Btn>
																<Btn onClick={this.onRemoveImport} size="sm" disabled={output.length < 1} className="bw-x1">Remove imports</Btn>
						
																<Btn size="sm" className="bw-x1" 
																	onClick={() => {
																		console.log('modules: ',modules);
																	}}
																>RUN</Btn>
						
																<Btn onClick={this.onZip} size="sm">ZIP</Btn>
															</nav>
															
															<div className="ovhide position-relative flex-auto">
																{libsOk && 
																	<ControlledEditor 
																		className="position-absolute position-full" // flex-auto 
																		editorDidMount={this.onDidChangeEditor} 
																		// value={selectedProjectModules && selectedProjectModules.length > 0 ? JSON.stringify(selectedProjectModules) : ''}
																		value={code} 
																		onChange={this.onChangeEditor} 
																		language="javascript" 
																		theme="dark" // {theme}
																		options={MONACO_OPTIONS}
																	/>
																}
															</div>
														</Flex>
														
														<Flex dir="column" className="w-50">
															<Flex nowrap align="center" className="flexno ml-1-next">
																<b className="flexno">Format:</b>
																<div className="btn-group btn-group-sm flex1">
																	{['amd','cjs','esm','iife','umd','system'].map((v, i) => 
																		<Btn key={i} kind="dark" className="rounded-0 bw-x1 shadow-none" 
																			active={options.format === v} 
																			onClick={() => this.onClickBundleFormat(v)}
																		>
																			{v}
																		</Btn>
																	)}
																	
																	<Btn onClick={this.onCopy} size="sm" kind="dark" className="rounded-0 bw-x1 shadow-none">Copy</Btn>
																</div>
															</Flex>
															
															<div className="ovhide position-relative flex-auto">
																{libsOk && 
																	<ControlledEditor 
																		className="position-absolute position-full" 
																		value={output.length > 0 ? output[0].output[0].code : ''}
																		// onChange={(e, value) => this.setState({code: value})} 
																		language="javascript" 
																		theme="dark" // {theme}
																		options={MONACO_OPTIONS} 
																	/>
																}
															</div>
														</Flex>
													</Flex>
												</Flex>
											);
										}

										return null;
									}}
								</RollupBrowser>
							)
						}
						return null;
					}}
				</BabelStandalone>
			</ErrorBoundary>
    );
  }
}


