import React, { useState } from 'react';// , { createRef }
import Dropdown from 'react-bootstrap/Dropdown';
// import Nav from 'react-bootstrap/Nav';
import { HexColorPicker, HexColorInput } from "react-colorful";// RgbaStringColorPicker, RgbaColorPicker
import ImageBlobReduce from 'image-blob-reduce';

import Btn from '../components/q-ui-react/Btn';
// import Flex from '../components/q-ui-react/Flex';
import Textarea from '../components/q-ui-react/Textarea';
import Img from '../components/q-ui-react/Img';
import ModalQ from '../components/q-ui-react/ModalQ';
import ImgEditor from '../apps/image-editor/ImgEditor';
// import { fileRead } from '../utils/file/fileRead';
// import { clipboardCopy } from '../utils/clipboard';
import darkOrLight from '../utils/darkOrLight';
// import ImgBlob from '../components/libraries/ImgBlob';

// const SIZES = [16, 32, 36, 48, 57, 60, 70, 72, 76, 96, 114, 120, 144, 150, 152, 180, 192, 310];
const SIZES = [
	{size: 16, ext: "ico"},
	{size: 16}, 
	{size: 32}, 
	{size: 36}, 
	{size: 48}, 
	{size: 57}, 
	{size: 60}, 
	{size: 70}, 
	{size: 72}, 
	{size: 76}, 
	{size: 96}, 
	{size: 114}, 
	{size: 120}, 
	{size: 144}, 
	{size: 150}, 
	{size: 152}, 
	{size: 180}, 
	{size: 192}, 
	{size: 310}, 
];

function setSrcDoc({
	title, 
	theme, 
	body, 
}){
	return (`<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="msapplication-TileColor" content="${theme}">
<meta name="theme-color" content="${theme}">
<link rel="shortcut icon" href="./favicon.ico">
<title>${title}</title>
<style>
body{margin:0;padding:1rem}
.icon{max-width:100%;height:auto}
</style>
</head>
<body>
${body}
</body></html>
`);
}

// const initialValues = {
// 	name: "", 
// 	short_name: "", 
// 	description: "", 
// 	display: "", // standalone
// 	theme_color: "", 
// 	background_color: "", 
// 	start_url: "./", 
// 	scope: "/", 
// };

function makeManifestJson(imgArr, {
	name, // = "App"
	short_name, // = "short_name", 
	description, // = "description", 
	display, // = "standalone", 
	theme_color, // = "#ffffff", 
	background_color, // = "#ffffff", 
	start_url, // ="./", 
	scope, // = "/", 

	zipFolder, // = "icons", 
	jsonTab, // = 2
}){
	return JSON.stringify({
		name, 
		short_name, 
		description, 
		display, 
		theme_color, 
		background_color, 
		// gcm_sender_id: "122867383838", 
		start_url, 
		scope, 
		icons: imgArr.map(v => ({ 
			src: zipFolder + "/" + v.name, 
			type: v.blob.type, 
			sizes: v.size + "x" + v.size, 
			// density: "0.75"
		})), // .filter(f => !f.ext)

		// related_applications: [
		// 	{
		// 		platform: "play",
		// 		id: "org.telegram.messenger",
		// 		url: "https://telegram.org/dl/android?ref=webmanifest"
		// 	}, {
		// 		platform: "itunes",
		// 		url: "https://telegram.org/dl/ios?ref=webmanifest"
		// 	}
		// ]
	}, null, jsonTab);
}

function makeBrowserConfigXml({ 
	path, 
	imgSrc, 
	tileColor
}){
	return (`<?xml version="1.0" encoding="utf-8"?>
<browserconfig>
	<msapplication>
		<tile>
			${imgSrc.map(v => `<square${v.size}x${v.size}logo src="/${path}/${v.name}"/>`).join("\n\t\t\t")}
			<TileColor>${tileColor}</TileColor>
		</tile>
	</msapplication>
</browserconfig>`);
}

export default function ManifestGenerator({
	className, 
	prepend, 

}){
	const [name, setName] = useState("");
	const [short_name, setShortName] = useState("");
	const [description, setDescription] = useState("");
	const [display, setDisplay] = useState("standalone");
	const [start_url, setStartUrl] = useState("./");
	const [scope, setScope] = useState("/");
	const [theme_color, setTheme] = useState("#ffffff");
	const [background_color, setBgColor] = useState("#ffffff");

	const [themePick, setThemePick] = useState(false);
	const [bgPick, setBgPick] = useState(false);
	const [zipFolder, setZipFolder] = useState("icons");
	const [jsonTab, setJsonTab] = useState(2);

	const [fileData, setFileData] = useState(null);
	const [img, setImg] = useState([]); // null
	// const [imgOri, setImgOri] = useState(null);
	const [modal, setModal] = useState(false);
	
	const onToggleTheme = (see, key) => {
		const docActive = document.activeElement;
		
		if(themePick && docActive.dataset.color === key){
			setThemePick(true);
		}else{
			setThemePick(see);
		}
	}

	const onToggleBg = (see, key) => {
		const docActive = document.activeElement;
		
		if(bgPick && docActive.dataset.color === key){
			setBgPick(true);
		}else{
			setBgPick(see);
		}
	}
	
	const isDark = (c) => darkOrLight(c.replace("#",""));

	const onZip = (zip) => {
		const imgNoFavicon = img.filter(v => !v.ext);
		const manifest = makeManifestJson(imgNoFavicon, {
			name, 
			short_name, 
			description, 
			display, 
			start_url, 
			scope, 
			theme_color, 
			background_color, 
			zipFolder, 
			jsonTab
		});

		const browserConfig = makeBrowserConfigXml({ 
			path: zipFolder, 
			imgSrc: imgNoFavicon.filter(v => [70, 150, 310].includes(v.size)), 
			tileColor: theme_color
		});

		const indexHtml = setSrcDoc({
			title: name, 
			theme: theme_color, 
			body: imgNoFavicon.map(v => `<img src="./${v.name}" alt="${v.name}" class="icon"/>`).join("\n"), 
		});

		zip.file(zipFolder + "/manifest.json", manifest);
		zip.file(zipFolder + "/browserconfig.xml", browserConfig);
		zip.file(zipFolder + "/index.html", indexHtml);
		
		img.forEach((v) => {
			zip.file(zipFolder + "/" + v.name, v.blob); // "icons/" + v.name, v.blob
		});
		
		zip.generateAsync({ type: "blob" }).then((blob) => {
			console.log("zip: ", blob);

			let a = Q.makeEl('a');
			a.href = URL.createObjectURL(blob);
			a.rel = "noopener";
			a.download = zipFolder;

			setTimeout(() => a.click(), 1);
			setTimeout(() => {
				URL.revokeObjectURL(a.href);
				a = null;// OPTION
			}, 4E4); // 40s
		});
	}

	const onSave = () => {
		if(window.JSZip){
			onZip(new JSZip());
		}else{
			Q.getScript({src:"/js/libs/jszip/jszip.min.js","data-js":"JSZip"}).then(() => {
				if(window.JSZip){
					onZip(new JSZip());
				}
				else{
					console.log("%cjszip failed", "color:yellow");
				}
			}).catch(e => console.log(e));
		}
	}

	const onSaveIcons = val => {
		console.log('ImgEditor onSave val: ', val);
		const { blob, file } = val;
		const [ fname, ext ] = file.name.replace(/ /g, "-").split(".");
		const files = new File([blob], file.name, { 
			lastModified: Date.now(), 
			type: file.type
		});

		let arr = [];
		SIZES.forEach((v) => {
			ImageBlobReduce().toBlob(files, {
				max: v.size, 
				unsharpAmount: 80, 
				unsharpRadius: 0.6,
				unsharpThreshold: 2
			}).then(blob => {
				arr.push({ 
					...v, 
					blob, 
					// size: v, 
					// name: fname + "-" + v.size + "x" + v.size + "." + (v.ext || ext.toLowerCase())
					name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext.toLowerCase()
				});
			});
		});

		// Promise.all(SIZES.forEach(v => {
		// 	ImageBlobReduce().toBlob(files, {
		// 		max: v.size, 
		// 		unsharpAmount: 80, 
		// 		unsharpRadius: 0.6,
		// 		unsharpThreshold: 2
		// 	}).then(blob => {
		// 		// arr.push({ 
		// 		// 	...v, 
		// 		// 	blob, 
		// 		// 	// size: v, 
		// 		// 	// name: fname + "-" + v.size + "x" + v.size + "." + (v.ext || ext.toLowerCase())
		// 		// 	name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext.toLowerCase()
		// 		// });
		// 		setImg({ 
		// 			...v, 
		// 			blob, 
		// 			// size: v, 
		// 			// name: fname + "-" + v.size + "x" + v.size + "." + (v.ext || ext.toLowerCase())
		// 			name: v.ext ? "favicon." + v.ext : fname + "-" + v.size + "x" + v.size + "." + ext.toLowerCase()
		// 		});
		// 	});
		// }));

		setFileData(file);
		
		setTimeout(() => {
			// const copyArr = [ ...arr ];
			// const arrSort = arr.sort((a, b) => (a.size > b.size) ? 1 : -1);
			console.log('arr: ', arr);
			// console.log('arrSort: ', arrSort);

			setImg(arr.sort((a, b) => (a.size > b.size) ? 1 : -1));
			// setImgOri(URL.createObjectURL(file));
			setModal(false);
		}, 3000);
	}

	return (
		<div className={Q.Cx("manifest-generator", className)}>
			{prepend} 

			<ModalQ 
				open={modal} 
				// toggle
				size="xl" // lg 
				contentClassName="border-0 ovhide" 
				position="center" 
				close={false} 
				bodyClass="p-0" 
				body={
					<ImgEditor 
						// pasteFromClipboard={false} 
						h={450} 
						// wrapClass="rounded" 
						captureImage={false} 
						captureScreen={false} 
						// https://raw.githubusercontent.com/roadmanfong/react-cropper/master/example/img/child.jpg
						// data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhMVFhUXFhcWGBcXFxUXFRcXFRcXFhcVFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAPkAygMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAQIDBAYHAAj/xAA+EAABAwIDBQYFAgQFBAMAAAABAAIRAyEEBTESQVFhcQYigZGxwRMyodHwI1IUQuHxBzNygsJTc5KiNDVi/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAIDAQQF/8QAJxEAAgICAwACAgEFAQAAAAAAAAECEQMhEjFBIjITUUIEI2FxgRT/2gAMAwEAAhEDEQA/AOpAJYXgEsKJ0CQlhKlQAkJYSwlQYJC8lXkANcFFfeqOa5/QofM7vftFz5BZPG9t6rp+GwNbxdc/RK8kYjKDZuS+FEcW3e5s8JErk2Y9pqzpmo93IGw8rIQ7GVXXvHGVn5b8N/HXp2+nim/uHmFL8YcQuBVMwey+0ev5orGX9sa7DZ7iOFz5FOm34I0kd32kpXPMg7aS9ratQQddoAbp1W5wma0ao7jwVtmUWh0TSeSkYZCfC2wor24JWxKm2UmyiwoSEhToSQlNGwkIT4SQgBkJE4hJCAJQEq8lQaeheSpUGCLyVVMdjmU2kuItdAEteu1gLnEAC5JXOO1HbsmWUXQ2YkHvH/du6C6Edse1j67ixpimNAN/MlZzB0do3F/QKUpX/orGJdoPfUO3UPdF4lJWxG0YGm7+yrZhjQO6LAWVjKsPbaOpufsovS5Mqu6RIWhoDj/f+iq1S99924f0Ct4h224/tG7j4KOrtTEEuO4XgcLIiwkgfVYxl3d4/T6oe/MXSQ3u8reoR9nZ/EVbu7o4G3ordPsiZvB9VVZIrsm8cn0ZEY5+vm038kRwOMdIdTcWvGlyCI4FaOr2MDm8Cs3m+RVcOQ6ZE63TxywlpCSxTjs6h2H7YGoRQxAipo1255Hut2HHgvnPD4p4Ic3UGbE2I3grt3YrPRiqAM/qNs8e/imENAlSr0LTBqQp0JEGjYSJ6QhADCkToXoQBIvBR/FCVtUIAkXlC/EgKpWx/BAEmY4vYba54LmnbHMXuPw5vqdbcFqcwxpuZ8Vz7M3bTjxN1DJLdF8cfQNsi+/nxPBTuOww7nO4a9BzUopXA/OZVbGEkk6NFufmlW2O1SKOHobThtak2HAbyeceqPh8NI8/zoheXwXl37W28bBFGtuAdNT+dB9UmV2zcapE2EpWk9T1OiMZbhNm5+Y3PsOgUWBw8gTvufYIvSZCi5WWiizQpKw2kkoBX6NGbrErHZXNNUsZgWvaQ4Ag7kaNFV6lNa1RidnKc8yR2HcXMks9E/sb2iOHxLToDZw0kHiuhY7ChwIIkEQVy7Ncu+HVLRq02PLcurFPkqZyZYcXaPomjVDmhwMggEdCnrN9hMZ8TC05N2iCtKuhO0c7VMavJyRaYNSQnJFgDUickQaUSVVxGJjeqdbMm6SEPxWJWDWWq2MJ3qB2O3IXUrkprqyVs1IfnWJAETuusk90y7joiWbVyRE6oaGzA3flvJcrduzpgqQjGWJ3n8KH5i4QGjSR4onUd3Z4jyCE1ILgeYjqSB7poGT6JsAyGvPF0eSIYZm08DiQPdUMM6KbObijOQiSXncT9kmT1jRDNNsRy/PsiNNqG0qslWv41jdXCeCii/SCdAIlQcguEx1MmNtvmjWHqM12gfFUihZMmcFWrBWxVbxUGIe1M0KmUagWF7b0QypTf+6W+IuPdbXE4um3VwHisT/iTVaaNEtMzVsRya6VuJfJC5vqafsBjdmG7jZdDC4t2QxkEAm9j/Vdhy+vtMBXRifaOTKvSykSrysSESJyRBo1InJFgHLcfLbkGVUpY13FGce+SgmLY2dLp/yr1B+N+FmnjAbGxSVaipvpGJFwoRWNxuU8kVxuI8JPlTIcW6SmsFvzjHoE3EvgbXAJ2DE7A5yfC3uVw+HZ6NzB27h/YfVDMWAC3gCPMkfZEK5lxP5+aIPm9WNnrtew91TGvBMj0WKhinS/OC0OQECiTy+pus3nDu7T6T9AjmTv/RA3n2BS5F8TYfYnZWc6wOyN7t/h909r8I2z3X5uMlBs5w2JiKbe6LkyhGEygOguc4ukEyDFjJHQi2qaGNNXZuTI06SNm7CUXAOY9wG4w7Z5d42VrL21KTgHOt6qPB4Ciyk00tplQA7QG0aRkk7Ja83sYkK0abnUxMAg2gyIvokyKtWPjbfaNHSeSJQPO8RUJ2GmBvP9kay8zT8Fnu0ODqPaWsJbOpGvnuCSO2NJ0gcxlNpmoXO4zMfRDu2dKmaFN1M2+IDEyPldfyCc7IaGw4VGPD4bsupl5O0Bdx23EX4IRXyquzDB1RxLfiWaeYd3p/NV1KKTTs5pSk1TiW8oqbL2kaQPUfddg7N4raaBP91xbCOtO9vpK6d2TxNmndCSLqZk18TdBeTaZsnLrOU8kSryAEKRR16oaFQOKKeONy6MckjnVOuS4yZsqlZ0lXH0g3RUHFQytN6L400tkmGqwYKjxdIQSFXrPi6ne+WTxCS9D1sE5o/uxxICs4V0B3INb4mT7hDcyq99o/LwAre1DOp9BAUGtFk9kNWp3SeMn6wPRBMc/bMdGoji6to5e91Uy+ltVG9QT5yqw1slPei3nfzNbwAH0lG8mEbA5T6IJiG7dT/d9BYI1hXQ4ch9lLJ9Ui2NfKzV0aYcNE9uV0zfZUOBqWCL4czZSiyzQ2ll7Bu91DjqceVkYp04QPOq3e2R+2U8uhIrZZyt3dVllEOkIflrzYIpTEOvCVDNFZ+Cb+0IR2owm1hngDSHf+JBP0laeqwaoLnzooVSf+m70Kb0WtHM6dmu/wBI89qPYroPY100xyv9Fz3FPlro3lo8yT7rovYmnHd//JT/AMkRf1ZvME6WhWVRy11oV5dkejjl2eTajoEpyH46rJgJ4R5OhJOkQ1HF5U4wSkwVCBJVtUnkp1EWMb2zkuOdCHlW8zcqcrifZ2LoqY0pcNV7pb+XTcaFSpVe+7gPZFaC9lOv3sQBw9r+yvYhwlo6eqrUGTWe7h7pMS8zP5uUntoqtWDalWQ49fU/dEcoGy0vOp0QrDM2nFu6T7WRyi2SG8L/AGVJ6VCQ3slwdGL+HifwnwU9Z3eBGkwnU9Z3Cw6nU+ClNK4HD1XM3s6YoLZXiZstBhaqyuHZBkI5gnEpGVD3xybIRmmFf8TbYZBaARwibjjqpHY0M1I8VGMyad8rewX+AZh8Jimu2iQ4HSBs7J89FoMDSeCS902gDnvuo/45kRteTZ9012ZNlbRtMu/GIsVm+3WODMK4b3lrB4mT9AUX+Nt3GiwP+JWL79GlwDnnxOy30cmxLlKiOaXGAKwVWXNnSZ8tPVdO7InvjmFyrKhous9j23HgPuqtf3EQv4GwwQ16q8q2HGvMqyF0xOSQys+AShUyZV3MH2hRUqfcJXVj+Mb/AGSlt0ebjTwTv47kq1CntGFP/BFNKONOmKnI5dmpuFSY5Ws0d3lSBXnPs70QZg6BzQrDAjaJ4K1m1aC2OMqJtMQTuJnwTPURVuZ57tkc3H8KrYmbfmqZjq129QfqPZWfhyWu3AfVSqlZVu3RBhKAYOpnqVbpvgxvP0UNSx2t4BDfcp+Ao7RuYGpKWTvY0VQUwLhIJjZEnoALlSYOuKku4oV2jxgp0vhiz6ggC9qYN+hP34IZk+ZGnDToljibjyGWVKfE3uEaiGBOy4cECwGYNdcFF6VYcVFovZczXBU6tyBI36HzCFNwrWWI37yT6oxRqSrDKLTqtTZsZOIOo4Ci4WIF5sYHXVO/hKUwwTz/AKouzKKRvsDyCf8AwobYCE47ykLLMhcg7XYr4uNqkaNIpj/aL/8AttLp/aPMBQoOfvgho4uOi45QkuJNySZPNWwRq5HBnlbSD2SUZI5XXVex1O09SubZPTgDmurdkKUMHRZHczJ6iaSmII/NynUQ1ClXUjlYMxrpcrTmxT8FTq3f4q/WHd8F0z0oolHtlHBfMiaGYL5kTlLm+xsOjimPrNLgAVWcVDVP6o6J9YwLrlyRqVI6YStWwVmLpKkqu/THMg+qp4l0uCt1h+na8X8tVuRaSMxPbYPqsLgBvmPqiddwYBPQD1KhyylLTUdZokjiVBiMRJ2tnoDeAueW3R0R0rFpsc8yd+72RUVWUWy+NrVrBx4kqrTn4e3MSJtb6oS+pNOeBI+s+6WuRvKiPNS6s4vdcn8AHLcreFy7aYLXUOFv0WiyNuyb6Kk58Y0hccE5WwA+jVpXEwr2C7QEWdZat+HpuFxF481nc67PFjrBTjOMtSKuLj9Q1ludA77cUdoZg3iuWOwlRp7s+EgotldKu4xtOPXQeKJY12mbHI+mjqDMzEahR4jN2BsucABvJQLA5eBTc58mATqdwXL6uMc593E3OpJ8BwRjx8/RcmVQ8NX2szT452hOwJDRxnVxCzWBp/Uog8/pBJgqMOZzlVTqNEauVh7K6ckLrvZmlFOeg8ly/s/RlwC65lNLZpMHKfNLh22Znei07d1UhTHp66UcwK/n8URrfKUOf8/iiNX5fBdGT+JOPoJa6NE/4zuK9QZJhX/4RqrOcYvYiTZwgumqlzJ0NjioqX+Ym498ujgFxyVzOiLqAOZd11dbpCrUWXJVpjZF0mR7KY1SK+LxIa0N3Tp09ghbqxMlFsdRtfVCqtKAVONFZWSYLGdwsJ006FRUjIc3x+6pAkHopXVNkh35BTuG9E1PWx9Go5joBWkyrGk/M0dR9lnqo8Qr+AeRpfkbH7FTyJNFMbaYcrVySdk66azPut5haQrUmuc0SWieu/6ysZlTQCHG/EHct1goAtouV90dSKjMnozdgKIswlJosxo8AnFt0jwdFqMYPzUxh6pH7H+hXDaDZcF3fOKc0XN4tI8wuL5VhTtnatsWPhaFfBKlI580blEI4gQ0NTcM79SOGiixNeXJ2FOr0Vo3Vm+yRoBBG/TqV1HBO7jegXI8hxjXMA3hdMyDHCpTEG4sUn9PKpNMzOvQuQlamynNXYcgMxYh6vTLPBVswbcFSYd8sK6JbgmTWm0VcL8yKoVhvmRVZn7CHR8/PbFRVniSTzVvH/NwhBMZmEfJBjfw8N6R9jroLiloAiWGwkNlxAmY4noFmssrPDTXqlzr91p3ndZFMsrOqA1XmXExbQNtLW8tPJceW9nVja0PxdHUjXnp4oJjRutzWnqskQAhNbAaud66qcJU9lZqzNuF5+ybXZoJRHEUgNPrr4AKrVaTceW9dSlZzOIyg+IB04o5haQcO6gLKuo8FbwOMLDpbgkyRb6HxzSezVYcECeHotnkdaWgclisFmTHNI0tfxWoyUjYbHOPBcUrXZ2LfRpgV6FXZU0n6/dXKYWpgC84dAhc27QU/hTsCDUMnrpA+pXSM70lc87RUy5wIWwfyCcbiZmq0zG+wVwuDAGneIPqpDQgF5HJvuUPrzHOV1r5HI1xC+FxWyQRoVr8gz51IhwNtD9iufYOttDZ3i4RGhXcw2Bg66kKU8e9djRna30dzy7PadYTICMU3iLFcLwOakEbPdP0K2+RdpnAgOt6FNHI19hJY/0bvFslqpYdxEhWsHig9shP+Gu2GT40cso7KOHadpFFFsc17Z5onLkwiqPnDEYw4mqGus0/KPODA+YzuuhxwpfPwmVO7qdkuMgSR3RDfNQsbYuDiC2DpGpiQesK7lzTtACoWFxEuO1AvqY3pHroZb7FzJz2U20zIIbca3P4fNXsnr/pDlM9ZMKTtJljWNJ70wYgWkXg8Ah+Vk/DDBqRJ6lQbUoF4pxn/wADjcxLASBYb+fDmUIxOPqPMuMckzE4mGi1hOwNJ4uKpzuSxxpbGc2ycXnhxKg2e94qzTdALTx+ibgmbTwOfpBHoU3SZj7RG4MHeIkb41B+ycymNQdofUdRuSPGw82ndyI3hSPw5ZFSmZY63Np/aUGBHAUGu5H83rQZbjXMsROzeBYwguXw7kd8flkWY+CHa7Jv0K5pq2dMXSC7O2NH5XsqDyI666K5T7b4OBNWLHVj93gsP2goBri5umtt3MLNVnyfRWhhjJWRnnlFnTMz7a4LZIFRzjFtljvUgBZI5yKzxstIF9dfosw4J+AxPw3gnQqn/mila7EX9XNun0bPMsOO40bmz5IFmOGiOU+aMUsZLmze39fsl/hyT+GFzwbiXklIztHDOHehWqWKINzfqI+4TcbULnOuSG7p8vEqh8Yzc+HDxXSly7OZvi9Gow+NIsHQeOvlKv4fHudqQ7r3XfngsnQrOB49f7K/hyXmWyCNeClKCRSMmzqXZvNbAXka9FtMO7aAIXGMkxbheTYwOa6r2dxO1Sa7jqmxTp0Jkj6FyF7YTwV5dFkT5fwNEipSbIIeWGD8pBMd5psYv5a70YwGHNTEii2AHExtRAEnS94aNBwVHLs2YHuEBjdktY8CajI+UzeTOsQn4HA16j2lg2gCP1GkWAPzXO6+vikd+lI14E+0OYh79gg/DaDSDv5iGd0OIgfzTzglC8JQLXAayABGlv7hP7RVYxFWBHeIi15El1pFyZUJq7NNlQaTBmLOBuRebgqbj8VQ6l8nZFndRrq3cMgACNwI3KClQM6FJhKMukG8k9UWqWEEeIWt8VQJW7KNdthZT4KmWvYd0xO69jPA3Kno4baP5+FHKOWtFJ4JuRLf9Q0IKjLIloqoXszub0tioevrBPqq+HrFktsWnUHQ+O4opmsPqu5AX8AVRrVmMgFsxrdNB2khZabZZyuo3a7ht9R15Iu94tzt+eCzOHrN29qm0t5SY8yr1TGuMQIt+FZOLs2ElRNj6m3ScCNCY8DH2WccfdEHYmzh1Hghj3WXRiVI58rtkaa9spy8VYgWstxpa4NO7QrU0cV/LxCxbmyiWCxpIDXGCNCoZcfqOjFk8ZcbRPw3jfLj5f0Q5jYcAeCO4YFzXSIc255jQkKrjMNZrx0PqpxlTaKONpMny/CbTXO5mB6eqM4DLtkERclC8DiPhyOP90VyzNgXk7hPoFDJy8LQpHsXSFMtgm38oGpXROwlUuoDatfTmsKysHuBOhv0K2PZDHNFJxf3YcSfAxP0TYnumJlRtmlOWLwXbhtbEijRZtNmC6+7hy5rYSupOzmaPmfB5TFI1qpIaDAa27nEu2bu+Vgkb7+8tXOXta2nSmk1moa6S473Od4ndv3qLKseynTftRUdUMlrxLBB+Yk3Lukab1BiK1MUw0Q5xMlwERqYB3wtq3sLSWiehVNaq34svmATPfEmAS60gc03tNhfhvAAhkd3iY1J4qLKcSGVmOdtENIiCLHjpom5/Sc2u/bmSZuZMG4uFlfNG38GTYQtAB3mN0xr9wr5eNx+hHqhDiQ2Bc281bpUe7cy7mbDoEkl6Ug/A7lGIaTBMn88CpM2xRiPb3WTfgXSNh1+RP0hPf8AGYQ17zB4klT/ABK7TH/I/UEKVWQ517WVB1AvJOhRnK6YOpt9EXqsAG7y91n5ODB4+SM1hsLsCXfnndJiKkAnj9Ai9RgPHlv/AKIPjqN726lbF8nsyS4qkUWPk9QVBXEEhPrkCwUBK64o5ZsReSpEwgqcOaavIAv4HM30iDG03gdY4SjGFrseDsGWn+U/M12ojisxKfTcQZFipTxJ9FYZWuw9XFun4VBl1WHuG8/WVTbjXGzr+qnw7gSAeocNRy5hTcGlsqsib0GKeLLS5vECOW4ojTxj3sFOe7O7fvM8RICDGod8E8RqtB2Yw7XVAXfKL/0XPJF0zoXYrKGUqe3sgOdqeXVapZ7L6xqOhvdYwfgRttMwLrph0c0uz5YrNio8CYaTu3Angm02gz3mjrtewKnp0g0sJMucQSLgAWmfCVXBbtXEjru69Fciy1RdSpvDnONSIIDRstnmXCSJ4DxUmcV/iv8AiRsl24kkkDQgRYKi2mXHutMcpKK4fLHDD1KjmwRBbbvRa4vIF+G5TlSdspG2mkUKT4ME33ga+J3L1UnjE7gq7LOPMD+qstINzuQ0C6C2QUW7Uv05n2T+0+ZUz+k2HHedzeQ5rPVsSdAbaKu0rFit8mDy0uKCNHEhrYBcDx2j9Antxv7nv/OaGylCpwRPmwi/MLQ3zJM+UlVX1vFQyvLVFIHJscXyklIvJhR7RzXk1KgBV5IlWAKEqavAoAeCnhx3EqIFOBQAXy3HmYfBHHet92QwPxtt9MnuQCBqeh3FcvpPhdY/wT+XEci30UZ4ovZaGWXRv8pYwsGx8vDS4/dz6oiqOIwZDviUzDv5h/K7rz5pf4up/wBM+YQtGs+Z8spue57toDYpucXO6FoHUzZOrkOa01A5roaAQ0Q5oAiRIvEXVZ3+SP8AX/xKuZl/l0un/Fif0RdGlyTZw+CrvaRVDxMxsxAA2TtamTMa8kDzrNzVYAwFjD84nV8b41bA38OQVnBf/Ar/AOsetNB2/wCW/qz1cpxiuTb/AGUlJ8Ul+gXtkEKw9/dsq+K3dPcpWafnBXogmICnQmpwWmCgpwKjTkAOXki8gBwSpoTggBV5IvFACyllNSoAWV5IV5YAspQU1KtAkaV03/BfNmtrVMO63xBtNPNmrfK/gVzALUf4d/8A2OG/7n/Fyx9Grs+h0kJUikVP/9k=
						// /img/iron_man.jpg
						// src="/img/iron_man.jpg" 
						onSave={onSaveIcons}
					/>
				}
				foot={
					<Btn kind="dark" size="sm" onClick={() => setModal(false)}>Cancel</Btn>
				}
			/>

			<div className="row">
				<div className="col-md-3">
					<div className="card shadow-sm position-sticky" 
						style={{ top:60 }}
					>
						<div className="card-body">
							<div className="form-group">
								<label htmlFor="icons_folder">Icons folder name</label>

								<input className="form-control form-control-sm" type="text" id="icons_folder" 
									value={zipFolder} 
									onChange={e => setZipFolder(e.target.value)}
								/>
							</div>

							<div className="form-group">
								<label htmlFor="jsonTab">manifest.json tab size</label>

								<select className="custom-select custom-select-sm" id="jsonTab" 
									value={jsonTab} 
									onChange={e => setJsonTab(e.target.value)}
								>
									{[2, 4, 6, 8].map((v, i) => <option key={i} value={v}>{v}</option>)}
									<option value={0}>Minify</option>
								</select>
							</div>

							{/* (imgOri && img.length > 0) */}
							{img.length > 0 && <Btn onClick={onSave}>Download</Btn>}
						</div>
					</div>
				</div>

				<div className="col-md-9">
					<div className="form-row">
						<div className="col-md-6 form-group">
							<label htmlFor="name">App name <sup className="text-danger font-weight-bold">*</sup></label>

							<input className="form-control" type="text" id="name" 
								value={name} 
								onChange={e => setName(e.target.value)}
							/>
						</div>

						<div className="col-md-6 form-group">
							<label htmlFor="short_name">Short name <sup className="text-danger font-weight-bold">*</sup></label>

							<input className="form-control" type="text" id="short_name" 
								value={short_name} 
								onChange={e => setShortName(e.target.value)}
							/>
						</div>

						<div className="col-md-12 form-group">
							<label htmlFor="description">Description</label>

							<Textarea id="description" //  type="text"
								value={description} 
								onChange={e => setDescription(e.target.value)}
							/>
						</div>

						<div className="col-md-4 form-group">
							<label htmlFor="display">Display</label>

							<select className="custom-select" id="display" 
								value={display} 
								onChange={e => setDisplay(e.target.value)}
							>
								{["browser", "standalone", "minimal-ui", "fullscreen"].map((v, i) => <option key={i} value={v}>{v}</option>)}
							</select>
						</div>

						<div className="col-md-4 form-group">
							<label htmlFor="start_url">Start url <sup className="text-danger font-weight-bold">*</sup></label>

							<input className="form-control" type="text" id="start_url" 
								value={start_url} 
								onChange={e => setStartUrl(e.target.value)}
							/>
						</div>

						<div className="col-md-4 form-group">
							<label htmlFor="scope">Scope</label>

							<input className="form-control" type="text" id="scope" 
								value={scope} 
								onChange={e => setScope(e.target.value)}
							/>
						</div>

						<div className="col-md-6 form-group">
							<label htmlFor="theme_color">Theme Color</label>
							
							<Dropdown
								show={themePick} 
								onToggle={(isOpen, e, m) => onToggleTheme(isOpen, "theme_color")} 
							>
								<Dropdown.Toggle as="div" bsPrefix="wrap-input-color input-group" tabIndex="0" data-color="theme_color">
									<label className="input-group-prepend mb-0" htmlFor="theme_color">
										<div className="input-group-text">#</div>
									</label>
									
									<HexColorInput 
										color={theme_color} 
										onChange={setTheme} 
										type="text" 
										data-color="theme_color" // ={theme_color} 
										// inputMode="search" // 
										id="theme_color" 
										className={
											Q.Cx("form-control text-monospace", {
												// text-" + (isDark() === "dark" ? "light":"dark")
												["text-" + (isDark(theme_color) === "dark" ? "white":"dark")] : theme_color !== ""
											})
										} 
										style={{ 
											backgroundColor: theme_color, 
											// caretColor: color !== "" && isDark(theme_color) === "light" ? "#000" : undefined // "#fff"
										}}
									/>
								</Dropdown.Toggle>
								
								<Dropdown.Menu className="p-1" onContextMenu={Q.preventQ}>
									<HexColorPicker color={theme_color} onChange={setTheme} />
								</Dropdown.Menu>
							</Dropdown>
						</div>

						<div className="col-md-6 form-group">
							<label htmlFor="background_color">Background Color</label>
							
							<Dropdown
								show={bgPick} 
								onToggle={(isOpen, e, m) => onToggleBg(isOpen, "background_color")} 
							>
								<Dropdown.Toggle as="div" bsPrefix="wrap-input-color input-group" tabIndex="0" data-color="background_color">
									<label className="input-group-prepend mb-0" htmlFor="background_color">
										<div className="input-group-text">#</div>
									</label>
									
									<HexColorInput 
										color={background_color} 
										onChange={setBgColor} 
										type="text" 
										data-color="background_color" 
										// inputMode="search" // 
										id="background_color" 
										className={
											Q.Cx("form-control text-monospace", {
												// text-" + (isDark() === "dark" ? "light":"dark")
												["text-" + (isDark(background_color) === "dark" ? "white":"dark")] : background_color !== ""
											})
										} 
										style={{ 
											backgroundColor: background_color, 
											// caretColor: color !== "" && isDark(background_color) === "light" ? "#000" : undefined // "#fff"
										}}
									/>
								</Dropdown.Toggle>
								
								<Dropdown.Menu className="p-1" onContextMenu={Q.preventQ}>
									<HexColorPicker color={background_color} onChange={setBgColor} />
								</Dropdown.Menu>
							</Dropdown>
						</div>

						<div className="col-md-12 form-group">
							<label htmlFor="icons">Icons <sup className="text-danger font-weight-bold">*</sup></label>

							<Btn block outline 
								kind="secondary" 
								className="text-left" 
								id="icons" 
								title={fileData?.name} 
								onClick={() => setModal(true)}
							>
								{fileData?.name || "Choose file"}
							</Btn>
						</div>
						
						{img.length > 0 && //  ml-2-next
							<div className="col-md-12">
								<div className="card-columns text-center">
									{img.filter(v => !v.ext).map((v, i) => 
										<div key={i} className="card shadow-sm">
											<Img 
												frame="bs" 
												frameClass="card-body p-2 mb-0" 
												className="flexno" 
												fluid 
												src={v.blob && URL.createObjectURL(v.blob)} 
												alt={fileData?.name} //  + " - " + v.size
												caption={v.size + "x" + v.size} 
												onLoad={e => {
													URL.revokeObjectURL(e.target.src)
												}}
											/>
										</div>
									)}
								</div>
							</div>
						}
					</div>
				</div>
			</div>
		</div>
	);
}

/*
<div className="input-group">
	<div className="custom-file text-truncate">
		<input type="file" className="custom-file-input" id="inputile" 
			onChange={onChangeIcon} 
		/>
		<label className="custom-file-label" htmlFor="inputile">
			{fileData?.name || "Choose file"}
		</label>
	</div>
</div>

{(imgOri && img.length > 0) && 
	<div className="col-md-12">
		<div className="row mb-3">
			<div className="col">
				<Img fluid src={imgOri} alt={fileData?.name} 
					onLoad={e => {
						URL.revokeObjectURL(e.target.src)
					}}
				/>
			</div>
			
			<div className="col ml-2-next">
				{img.map((v, i) => 
					<Img key={i} fluid src={URL.createObjectURL(v.blob)} alt={fileData?.name + " - " + v.size} 
						onLoad={e => {
							URL.revokeObjectURL(e.target.src)
						}}
					/>
				)}
			</div>
		</div>
	</div>
}
*/