import React, { useRef, useState, useCallback, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useDropzone } from 'react-dropzone';
import Dropdown from 'react-bootstrap/Dropdown';
// import CropperJS from 'cropperjs';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import { fileRead } from '../../utils/file/fileRead';

const ASPECT_RATIO = [
	{txt: "16 : 9", val: 16 / 9}, 
	{txt: "4 : 3", val: 4 / 3}, 
	{txt: "1 : 1", val: 1 / 1}, 
	{txt: "Free", val: NaN} 
];

export default function ImgEditor({
	src, 
	aspectRatio = 1 / 1, 
	guides = false, // false | true
	rotate = 0, 
	flipX = 1, 
	flipY = 1, 
	enable = false, 
	wrapClass, 
	croppperClass, 
	cropperStyle, 
	w, 
	h = 500,
}){
	// let CropperJS = null;
	const cropperRef = useRef(null);
	const [imgSrc, setImgSrc] = useState(Q.newURL(src).href);
	// const [tools, setTools] = useState(false);
	const [fname, setFname] = useState(null);
	// const [enableCrop, setEnableCrop] = useState(enable);
	// const [reload, setReload] = useState(true);
	// const [guide, setGuide] = useState(guides);
	const [ratio, setRatio] = useState(aspectRatio);
	const [rotates, setRotate] = useState(rotate);
	const [fX, setFx] = useState(flipX);
	const [fY, setFy] = useState(flipY);
	
	const onDrop = useCallback(files => {
		let file = files[0];
		// const blob = window.URL.createObjectURL(file);
		// window.URL.revokeObjectURL(this.src);
		console.log('onDrop files: ', files);

		// setImgSrc(window.URL.createObjectURL(file));
		// setFname(file.name);
		if(file){
			// let percent;
			fileRead(file, {
				// onProgress: (e) => {
				// 	const { loaded, total } = e;
				// 	if (loaded && total) {
				// 		percent = Math.round((loaded / total) * 100);
				// 		console.log("Progress: ", percent);
				// 		setProgressFile(percent);
				// 	}
				// }
			}).then(v => {
				// console.log('v: ', v);
				// let res = v.result;
				// if(res?.length <= 0){
				// 	res = window.URL.createObjectURL(file);
				// }

				setImgSrc(v.result);
				setFname(file.name);

				// if(res && percent === 100) setProgressFile(null);
			}).catch(e => {
				console.log(e);
			});
		}
	}, []);	
	// inputRef
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ 
		onDrop, 
		accept: "image/*", 
		// noDragEventsBubbling: true,
		multiple: false, 
		noClick: true, 
		noKeyboard: true 
	});

	// useEffect(() => {
	// 	// console.log('%cuseEffect in ImgEditor','color:yellow;');
	// 	getCropper().rotateTo(rotate);
	// }, [guide]);

	// useEffect(() => {
	// 	getCropper().setDefaults({ aspectRatio: ratio, rotateTo: rotate, guides: !guide });
	// }, [guide]);

	// useEffect(() => {
	// 	getCropper().enable(enableCrop);
	// }, [enableCrop]);

	useEffect(() => {
		getCropper().rotateTo(rotates);
	}, [rotates]);

	useEffect(() => {
		getCropper().setAspectRatio(ratio);
	}, [ratio]);

	useEffect(() => {
		getCropper().scale(fX, fY);
	}, [fX]);

	useEffect(() => {
		getCropper().scale(fX, fY);
	}, [fY]);

	const getCropper = () => cropperRef?.current?.cropper;

	const onCrop = (e) => {
    // const imgEl = cropperRef?.current;
		// const cropper = imgEl?.cropper;
		
		console.log('onCrop e: ', e);
    // console.log(getCropper().getCroppedCanvas().toDataURL());
	};
	
	// const onSetGuide = () => {
	// 	// setReload(false);

	// 	setGuide(!guide);

	// 	// setTimeout(() => setReload(true), 1);

	// 	// setTimeout(() => {
	// 	// 	let cropper = getCropper();
	// 	// 	// let cropBoxData = cropper.getCropBoxData();
	// 	// 	// let canvasData = cropper.getCanvasData();

	// 	// 	// cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);

  //   //   cropper.destroy();
  //   //   new CropperJS(cropperRef.current, { guides: guide });// cropperRef.current = 
	// 	// }, 1);
	// }

	const onFlipX = () => {
		if(fX > 0) setFx(-1);
		else setFx(1);
	}

	const onFlipY = () => {
		if(fY > 0) setFy(-1);
		else setFy(1);
	}

	const styles = { 
		...cropperStyle, 
		height: h, 
		width: w, // "100%"
	};

	return (
		<div className={Q.Cx("imgEditor", wrapClass)}>
			<Flex wrap className="p-2 ml-1-next bg-strip">
				{fname && // py-1 px-2 bg-light rounded
					<div className="btn-group btn-group-sm">
						<Btn As="div" kind="light" className="cauto">{fname}</Btn>
						<Btn onClick={() => {
							setFname(null);
							setImgSrc(null);
						}} kind="light" className="far fa-times xx" />
					</div>
				} 

				<Btn size="sm" className="ml-auto">Save</Btn>

				<Btn As="label" size="sm" kind="light" tabIndex="0">
					Insert File
					<input // type="file" hidden 
						capture 
						{...getInputProps()} 
						// onChange={e => {
						// 	let file = e.target.files[0];
						// 	console.log(file);

						// 	// setImgSrc(window.URL.createObjectURL(file));
						// 	// setFname(file.name);

						// 	// let percent;
						// 	fileRead(file, {
						// 		// onProgress: (e) => {
						// 		// 	const { loaded, total } = e;
						// 		// 	if (loaded && total) {
						// 		// 		percent = Math.round((loaded / total) * 100);
						// 		// 		console.log("Progress: ", percent);
						// 		// 		setProgressFile(percent);
						// 		// 	}
						// 		// }
						// 	}).then(v => {
						// 		console.log('v: ', v);

						// 		// let res = v.result;
						// 		// if(res?.length <= 0){
						// 		// 	res = window.URL.createObjectURL(file);
						// 		// }

						// 		setImgSrc(v.result);
						// 		setFname(file.name);

						// 		// if(res && percent === 100) setProgressFile(null);
						// 	}).catch(e => {
						// 		console.log(e);
						// 	});
						// }}
					/>
				</Btn>

				<fieldset className="ml-1-next" disabled={imgSrc === null}>
					{/* <Btn onClick={onSetGuide} size="sm" kind="light" className="tip tipB far fa-vector-square" aria-label="Show guides" /> */}

					<div className="btn-group btn-group-sm">
						<Btn kind="light" className="tip tipB far fa-arrows-alt" aria-label="Move" />
						<Btn kind="light" className="tip tipB far fa-crop-alt" aria-label="Crop" />
					</div>

					<div className="btn-group btn-group-sm">
						<Btn onClick={() => setRotate(rotates - 45)} kind="light" className="tip tipB far fa-undo" aria-label="Rotate -45" />
						<Btn onClick={() => setRotate(rotates + 45)} kind="light" className="tip tipB far fa-redo" aria-label="Rotate 45" />
					</div>

					<div className="btn-group btn-group-sm">
						<Btn onClick={onFlipX} kind="light" className="tip tipB far fa-arrows-h" aria-label="Flip X" />
						<Btn onClick={onFlipY} kind="light" className="tip tipB far fa-arrows-v" aria-label="Flip Y" />
					</div>

					<Dropdown alignRight bsPrefix="btn-group">
						<Dropdown.Toggle size="sm" variant="light">Aspect ratio</Dropdown.Toggle>
						<Dropdown.Menu className="mnw-auto text-right">
							{ASPECT_RATIO.map(v => 
								<Dropdown.Item key={v.txt} onClick={() => setRatio(v.val)} active={v.val === ratio} as="button" type="button">{v.txt}</Dropdown.Item>
							)}
						</Dropdown.Menu>
					</Dropdown>
				</fieldset>
			</Flex>

			<div {...getRootProps()} 
				className={Q.Cx("position-relative", { "dropFile": isDragActive || !imgSrc })} 
				aria-label={isDragActive || !imgSrc ? "Drop file here" : undefined} 
			>
				{imgSrc ? 
					<Cropper 
						ref={cropperRef} 
						className={croppperClass} // Q.Cx("px-0 col", croppperClass)
						// Cropper.js options 
						// enable={enableCrop} 
						aspectRatio={ratio} 
						// initialAspectRatio={16 / 9} 
						guides={guides} 
						// zoom={} 
						// onInitialized={c => {
						// 	// console.log('onInitialized c: ', c);
						// 	// CropperJS = c;
						// 	console.log('onInitialized imgSrc: ', imgSrc);
						// 	if(!imgSrc.includes("data:image/") || !imgSrc.startsWith("blob:http:")){
						// 		console.log('onInitialized: ');
						// 		let name = imgSrc.split("/");
						// 		setFname(name[name.length - 1]);
						// 	}
						// }}
						ready={(e) => {
							// console.log('ready imgSrc: ', imgSrc);
							if(!imgSrc.startsWith("data:image/")){
								console.log('ready: ');
								let name = imgSrc.split("/");
								setFname(name[name.length - 1]);
							}
						}} 
						// preview={} 
						// cropstart={} 
						// cropmove={} 
						// crop={onCrop} 
						cropend={onCrop} 
						rotateTo={rotate} 
						// scaleX={fX} 
						// scaleY={fY} 
						src={imgSrc} 
						style={styles}
					/>
					: 
					<div className="cropper-bg" style={styles} />
				}
			</div>
		</div>
	);
}

/*
			<div style={{ height: h }}>
				{reload && }
			</div>

<Flex dir="column" className={Q.Cx("ovhide position-relative imgEditor", wrapClass)}>

			<div tabIndex={tools ? undefined : -1} 
				className={Q.Cx("bg-white position-absolute t0 b0 r0 col-3 crop-tool", { "toolOpen": tools })}
			>
				<Btn As="div" onClick={() => setTools(!tools)} size="sm" kind="light" 
					className="round-right-0 position-absolute zi-1 crop-toggle-tool fas fa-sort i-r90" 
				/>

				<div className="p-2 bg-strip">
					Options
				</div>

				<div className="crop-options">

				</div>

				<Btn>Save</Btn>
			</div>

<div className="position-absolute t0 b0 zi-1 crop-toggle-tool">
	<Btn onClick={() => setTools(!tools)} size="sm" kind="light" className="round-right-0 fas fa-sort i-r90" />
</div>

<React.Fragment></React.Fragment>
*/
