import React, { useRef, useState, useCallback, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useDropzone } from 'react-dropzone';
import Dropdown from 'react-bootstrap/Dropdown';
// import CropperJS from 'cropperjs';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import { fileRead } from '../../utils/file/fileRead';
// DEVS: Screen Capture API (CHANGE TO ES Module)
import captureVideoFrame from '../../utils/img/captureVideoFrame';
import { pasteBlob } from '../../utils/clipboard';
import { screenCaptureAPI, getUserMediaAPI, getDisplayMedia, getUserMedia, stopCapture } from '../../utils/img/screenCaptureAPI';// displayMediaOptions, userMediaOptions

const SM_DEVICE = Q_appData.UA.platform.type === "mobile" || screen.width <= 480;

const VIDEO_ID = Q.Qid();

const ASPECT_RATIO = [
	{txt: "16 : 9", val: 16 / 9}, 
	{txt: "4 : 3", val: 4 / 3}, 
	{txt: "1 : 1", val: 1 / 1}, 
	{txt: "Free", val: NaN} 
];

// console.log('screenCaptureAPI: ', screenCaptureAPI);

export default function ImgEditor({
	src, 
	aspectRatio = 1 / 1, 
	guides = false, // false | true
	rotate = 0, 
	flipX = 1, 
	flipY = 1, 
	// enable = false, 
	wrapClass, 
	croppperClass, 
	cropperStyle, 
	pasteFromClipboard = true, // DEVS
	w, 
	h = 500, 
	captureImage = true, 
	captureScreen = true, 
	onSave = Q.noop,
}){
	// let CropperJS = null;
	const styles = { 
		...cropperStyle, 
		height: h, 
		width: w // "100%"
	};
	const cropperRef = useRef(null);

	// DEVS: Screen Capture API
	const videoRef = useRef(null);
	const [onceCapture, setOnceCapture] = useState(false);
	const [isCapture, setIsCapture] = useState(false);
	// const [imgCapture, setImgCapture] = useState(null);
	// END DEVS: Screen Capture API

	const [imgSrc, setImgSrc] = useState(src); // src ? Q.newURL(src).href : null
	const [externalMedia, setExternalMedia] = useState("");
	const [once, setOnce] = useState(false);
	// const [tools, setTools] = useState(false);
	const [fname, setFname] = useState(null);
	const [ext, setExt] = useState(".png");
	const [flipCapture, setFlipCapture] = useState(false);
	// const [enableCrop, setEnableCrop] = useState(enable);
	// const [reload, setReload] = useState(true);
	// const [guide, setGuide] = useState(guides);
	const [ratio, setRatio] = useState(aspectRatio);
	const [rotates, setRotate] = useState(rotate);
	const [fX, setFx] = useState(flipX);
	const [fY, setFy] = useState(flipY);

	const [dataCropper, setDataCropper] = useState();
	const [dataFile, setDataFile] = useState();
	const [result, setResult] = useState();
	
	const onDrop = useCallback(files => {
		let file = files[0];
		// const blob = window.URL.createObjectURL(file);
		// window.URL.revokeObjectURL(this.src);
		console.log('onDrop files: ', files);

		// setImgSrc(window.URL.createObjectURL(file));
		// setFname(file.name);
		if(file){
			// let percent;
			fileRead(file, {
				// onProgress: (e) => {
				// 	const { loaded, total } = e;
				// 	if (loaded && total) {
				// 		percent = Math.round((loaded / total) * 100);
				// 		console.log("Progress: ", percent);
				// 		setProgressFile(percent);
				// 	}
				// }
			}).then(v => {
				// console.log('v: ', v);
				// let res = v.result;
				// if(res?.length <= 0){
				// 	res = window.URL.createObjectURL(file);
				// } 
				setDataFile(file);
				setImgSrc(v.result);
				setFname(file.name);
				setExt(Q.fileName(file.name).ext);

				// if(res && percent === 100) setProgressFile(null);
			}).catch(e => {
				console.log(e);
			});
		}
	}, []);	
	// inputRef
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ 
		onDrop, 
		accept: "image/*", 
		// noDragEventsBubbling: true,
		multiple: false, 
		noClick: true, 
		noKeyboard: true 
	});

	useEffect(() => {
		if(pasteFromClipboard){
			const pasteFn = (e) => {
				// const docActive = document.activeElement;
				// const editorActive = Q.hasClass(docActive, "imgEditor") ? docActive : docActive.closest(".imgEditor");
				// console.log('docActive: ', docActive);
				// console.log('editorActive: ', editorActive);

				// // NOTE: CHECK SUPPORT
				pasteBlob(e).then(data => {
					// If there's an image, display it in the canvas
					if(data){// editorActive && data
						// let canvas = document.getElementById("canvas");
						// let ctx = canvas.getContext('2d');
						
						// Create an image to render the blob on the canvas
						// let img = new Image();
			
						// Once the image loads, render the img on the canvas
						/* img.onload = function(){
							// Update dimensions of the canvas with the dimensions of the image
							canvas.width = this.width;
							canvas.height = this.height;
			
							// Draw the image
							ctx.drawImage(img, 0, 0);
						}; */
			
						// Crossbrowser support for URL
						const url = window.URL || window.webkitURL;
						const ext = data.type.replace("image/","");

						setImgSrc(url.createObjectURL(data));
						setFname("external-img-" + Date.now() + "." + ext);// + extSet
						setExt(ext);
						// img.src = blob;
					}
				}).catch(e => {
					console.log('Error call pasteBlob e: ', e);
				});
			}

			window.addEventListener('paste', pasteFn);// document

			return () => {
				window.removeEventListener('paste', pasteFn);// document
			}
		}
	}, [pasteFromClipboard]);

	useEffect(() => {
		if(once){
			if(imgSrc && (!imgSrc.startsWith("data:image/") && !imgSrc.startsWith("blob:http"))){
				const names = Q.fileName(imgSrc);
				setFname(names.fullname);// imgSrc.split("/").pop()
				setExt(names.ext);
			}
		}
	}, [once]);// , imgSrc

	// useEffect(() => {
	// 	getCropper().setDefaults({ aspectRatio: ratio, rotateTo: rotate, guides: !guide });
	// }, [guide]);

	// useEffect(() => {
	// 	getCropper().enable(enableCrop);
	// }, [enableCrop]);

	useEffect(() => {
		if(imgSrc) getCropper().rotateTo(rotates);
	}, [imgSrc, rotates]);

	useEffect(() => {
		if(imgSrc) getCropper().setAspectRatio(ratio);
	}, [imgSrc, ratio]);

	useEffect(() => {
		if(imgSrc) getCropper().scale(fX, fY);
	}, [imgSrc, fX]);

	useEffect(() => {
		if(imgSrc) getCropper().scale(fX, fY);
	}, [imgSrc, fY]);

	const getCropper = () => cropperRef?.current?.cropper;

	// const onCrop = (e) => {
  //   // const imgEl = cropperRef?.current;
	// 	// const cropper = imgEl?.cropper;
		
	// 	console.log('onCrop e: ', e);
	// 	console.log(getCropper()); // getCropper().getCroppedCanvas().toDataURL()
		
	// 	setDataCrop(getCropper());
	// };
	
	// const onSetGuide = () => {
	// 	// setReload(false);

	// 	setGuide(!guide);

	// 	// setTimeout(() => setReload(true), 1);

	// 	// setTimeout(() => {
	// 	// 	let cropper = getCropper();
	// 	// 	// let cropBoxData = cropper.getCropBoxData();
	// 	// 	// let canvasData = cropper.getCanvasData();

	// 	// 	// cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);

  //   //   cropper.destroy();
  //   //   new CropperJS(cropperRef.current, { guides: guide });// cropperRef.current = 
	// 	// }, 1);
	// }

	const onFlipX = () => {
		fX > 0 ? setFx(-1) : setFx(1);
	}

	const onFlipY = () => {
		fY > 0 ? setFy(-1) : setFy(1);
	}

// DEVS: Screen Capture API
	const onCapture = () => {
		if(!onceCapture) setOnceCapture(true);

		setTimeout(() => {
			const vid = videoRef.current;
			if(vid){
				getDisplayMedia(vid).then((data) => {
					console.log('onCapture data: ', data);
					vid.srcObject = data;
					// dumpOptionsInfo(vid);

					const vidTrack = vid.srcObject.getVideoTracks()[0];
					console.log(JSON.stringify(vidTrack.getSettings(), null, 2));

					// vidTrack.applyConstraints({
					// 	cursor: "never", // https://github.com/web-platform-tests/wpt/issues/16206
					// 	// width,
					// 	// height,
					// 	// aspectRatio: 1.33,
					// 	// resizeMode: "crop-and-scale"
					// });

					// vidTrack.contentHint = "motion";
					// vidTrack.cursor = "never";

					// if stop sharing (Chrome)
					vidTrack.onended = function(){
						onStopCapture();
					};

					setIsCapture(true);
				})
				.catch(e => {
					console.log("Error: ", e);
					return null;
				});
			}
		}, 9);
	}

	const onStopCapture = () => {
		stopCapture(videoRef.current);
		setIsCapture(false);

		if(flipCapture) setFlipCapture(false);// Reset flip
	}

	const onSaveImg = () => {
		const vid = videoRef.current;
		const frame = captureVideoFrame(vid, { ext:"png", flip:flipCapture });
		// console.log('frame: ', frame);

		stopCapture(vid);
		setIsCapture(false);
		setImgSrc(frame.uri);
		setFname("capture-screen-" + Date.now());// null | Q.Qid()
		setExt("png");

		if(flipCapture) setFlipCapture(false);// Reset flip
	}

	const onSaveEditFilename = () => {
		const name = Q.fileName(fname).name;
		if(name.length < 1){
			setFname("Untitled-" + Date.now() + "." + ext);
		}
	}

	const onGetUserMedia = () => {
		// MUST STRICT CHECK DEVICE: window.screen.width > 480
		// console.log(SM_DEVICE);
		if(!SM_DEVICE && getUserMediaAPI){
			if(!onceCapture) setOnceCapture(true);
			if(!flipCapture) setFlipCapture(true);// Set flip

			setTimeout(() => {
				const options = {// constraints
					video: true,
					// audio: true
				};
				const vid = videoRef.current;

				// navigator.mediaDevices.getUserMedia(options)
				getUserMedia(options).then(stream => {
					console.log('getUserMedia stream: ', stream);
					vid.srcObject = stream;
					setIsCapture(true);
				}).catch(e => {
					console.log('Error getUserMedia: ', e);
				});
			}, 9);
		}
	}

	const onClickExternal = () => {
		const file = Q.fileName(externalMedia);
		const isExt = ["png","jpg","jpeg","svg","gif"].includes(file.ext.toLowerCase());
		// console.log('onClickExternal file: ', file);
		// console.log('onClickExternal isExt: ', isExt);

		if((Q.isAbsoluteUrl(externalMedia) && isExt) || externalMedia.startsWith("data:image/")){
			let extSet = isExt ? file.ext : externalMedia.split(';')[0].slice(5).replace("image/","");
			// console.log('onClickExternal extSet: ', extSet);

			setImgSrc(externalMedia);
			setFname(isExt ? file.fullname : "external-img-" + Date.now() + "." + extSet);
			setExt(extSet);
			setExternalMedia("");// Reset input value

			document.body.click();
		}
	}

	return (
		<div className={Q.Cx("imgEditor", wrapClass)} tabIndex="0">
			<Flex wrap className="p-2 ml-1-next bg-strip">
				{fname && // py-1 px-2 bg-light rounded
					<div className="btn-group btn-group-sm">
						{/* <Btn As="div" kind="light" className="cauto text-truncate">{fname}</Btn> */}
						<Dropdown bsPrefix="btn-group mxw-27vw tip tipBL" aria-label="Edit"
							onToggle={open => {
								if(!open) onSaveEditFilename();
							}}
						>
							<Dropdown.Toggle size="sm" variant="light" bsPrefix="text-truncate" title={fname}>{fname}</Dropdown.Toggle>
							<Dropdown.Menu className="w-400 p-3">
								<label className="input-group">
									<input className="form-control" type="text" 
										value={Q.fileName(fname).name} 
										onChange={e => {
											let val = e.target.value;
											setFname(val.length > 0 ? val + "." + ext : val);
										}} 
									/>
									<div className="input-group-append">
										<div className="input-group-text">{ext}</div>
										<Dropdown.Item onClick={onSaveEditFilename} as="button" type="button" bsPrefix="btn btn-primary">Save</Dropdown.Item>
									</div>
								</label>
							</Dropdown.Menu>
						</Dropdown>

						<Btn onClick={() => {
							setFname(null);
							setImgSrc(null);
						}} kind="light" className="flexno qi qi-close xx" />
					</div>
				} 

				<Flex wrap align="center" className="ml-md-auto ml-1-next">
					<Btn size="sm" disabled={!dataCropper} 
						onClick={() => {
							if(dataCropper){ // typeof dataCrop !== "undefined"
								// console.log('SAVE dataCropper: ', dataCropper);// getCropper().getCroppedCanvas().toDataURL()
								// setDataCropper(null);

								const cropCanvas = dataCropper.getCroppedCanvas();
								cropCanvas.toBlob(blob => {
									// console.log('blob: ', blob);

									const res = {
										blob, // : URL.createObjectURL(blob), 
										url: cropCanvas.toDataURL(), 
										file: dataFile
									};
									setResult(res);
									onSave(res);
								});
							}
						}}
					>Save</Btn>

					<Dropdown>
						<Dropdown.Toggle size="sm" variant="light">Add image</Dropdown.Toggle>
						<Dropdown.Menu className="w-400">
							<Dropdown.Item as="label" role="button">
								Insert File
								<input hidden {...getInputProps()} />
							</Dropdown.Item>

							{captureImage && 
								<Dropdown.Item as="label" role="button" onClick={onGetUserMedia}>
									Capture image
									{/* MUST STRICT CHECK DEVICE */}
									{window.screen.width <= 480 && <input hidden capture {...getInputProps()} />}
								</Dropdown.Item>
							}

							<label className="input-group input-group-sm py-2 px-3">
								<input type="text" className="form-control" placeholder="Insert URL" // id="extMediaInput"
									value={externalMedia} 
									onChange={e => setExternalMedia(e.target.value)}
								/>
								<div className="input-group-append">
									<Btn As="div" onClick={() => setExternalMedia("")} kind="light" className="qi qi-close xx" hidden={externalMedia.length < 1} />
									<Btn As="div" outline onClick={onClickExternal}>Insert</Btn>
								</div>
							</label>
						</Dropdown.Menu>
					</Dropdown>
					
					{/* <Btn blur As="label" size="sm" kind="light" tabIndex="0" className="far fa-file-image">
						Insert File
						<input hidden // type="file" 
							{...getInputProps()} 
						/>
					</Btn> */}

					{/* DEVS: Screen Capture API */}
					{(captureScreen && screenCaptureAPI) && 
						<Btn onClick={onCapture} size="sm" kind="light" className="qi qi-desktop" disabled={isCapture}>Capture screen</Btn>
					}

					<fieldset className="ml-1-next" disabled={!imgSrc}>
						{/* <Btn onClick={onSetGuide} size="sm" kind="light" className="tip tipB far fa-vector-square" aria-label="Show guides" /> */}

						<div className="btn-group btn-group-sm">
							<Btn kind="light" className="tip tipB qi qi-arrows-alt" aria-label="Move" />
							<Btn kind="light" className="tip tipB qi qi-crop" aria-label="Crop" />
						</div>

						<div className="btn-group btn-group-sm">
							<Btn onClick={() => setRotate(rotates - 45)} kind="light" className="tip tipB qi qi-undo" aria-label="Rotate -45" />
							<Btn onClick={() => setRotate(rotates + 45)} kind="light" className="tip tipB qi qi-redo" aria-label="Rotate 45" />
						</div>

						<div className="btn-group btn-group-sm">
							<Btn onClick={onFlipX} kind="light" className="tip tipB qi qi-arrows-h" aria-label="Flip X" />
							<Btn onClick={onFlipY} kind="light" className="tip tipB qi qi-arrows-v" aria-label="Flip Y" />
						</div>

						<Dropdown alignRight bsPrefix="btn-group">
							<Dropdown.Toggle size="sm" variant="light">Aspect ratio</Dropdown.Toggle>
							<Dropdown.Menu className="mnw-auto text-right">
								{ASPECT_RATIO.map(v => 
									<Dropdown.Item key={v.txt} onClick={() => setRatio(v.val)} active={v.val === ratio} as="button" type="button">{v.txt}</Dropdown.Item>
								)}
							</Dropdown.Menu>
						</Dropdown>
					</fieldset>
				</Flex>
			</Flex>

			<div {...getRootProps()} 
				className={Q.Cx("position-relative", { "dropFile": isDragActive || !imgSrc })} 
				aria-label={isDragActive || !imgSrc ? "Drop file here" : undefined} 
			>
				{imgSrc ? 
					<Cropper 
						ref={cropperRef} 
						className={croppperClass} // Q.Cx("px-0 col", croppperClass)
						// Cropper.js options 
						// enable={enableCrop} 
						aspectRatio={ratio} 
						// initialAspectRatio={16 / 9} 
						guides={guides} 
						// zoom={} 
						// onInitialized={c => {
						// 	// console.log('onInitialized c: ', c);
						// 	// CropperJS = c;
						// 	console.log('onInitialized imgSrc: ', imgSrc);
						// 	if(!imgSrc.includes("data:image/") || !imgSrc.startsWith("blob:http:")){
						// 		console.log('onInitialized: ');
						// 		let name = imgSrc.split("/");
						// 		setFname(name[name.length - 1]);
						// 	}
						// }} 
						viewMode={1} 
						minCropBoxHeight={10} 
						minCropBoxWidth={10} 
						autoCropArea={1} 
						checkOrientation={false} // https://github.com/fengyuanchen/cropperjs/issues/671
						onInitialized={(instance) => {
							setDataCropper(instance);
						}}

						ready={() => { // e
							// console.log('ready once: ',once);
							// console.log('ready imgSrc.startsWith: ', imgSrc.startsWith("data:image/"));
							if(!once) setOnce(true);
							// if(imgSrc && (!imgSrc.startsWith("data:image/") && !imgSrc.startsWith("blob:http"))){
							// 	console.log('=============== ready imgSrc: ===============', imgSrc);
							// 	let name = imgSrc.split("/");
							// 	setFname(name[name.length - 1]);
							// }
						}} 

						// preview={} 
						// cropstart={} 
						// cropmove={} 
						// crop={onCrop} 
						// cropend={onCrop} 
						rotateTo={rotate} 
						// scaleX={fX} 
						// scaleY={fY} 
						src={result ? result.url : imgSrc} // imgSrc
						style={styles}
					/>
					: 
					<div className="cropper-bg" style={styles} />
				}

				{/* {result && 
					<div 
						className="embed-responsive embed-responsive-16by9 position-absolute position-full zi-5 bg-dark text-center" 
					>
						<div className="embed-responsive-item">
							<img src={result} alt="Result cropper" className="img-fluid" />
						</div>
					</div>
				} */}

				{(screenCaptureAPI || getUserMediaAPI) && 
					<>
						{onceCapture && 
							<div //  + (isCapture ? "":" d-none")
								className="embed-responsive embed-responsive-16by9 position-absolute position-full zi-5 bg-dark playerQ touch-no" 
								hidden={!isCapture}
								style={{ height: h }}
							>
								<video autoPlay 
									ref={videoRef} 
									className={Q.Cx("embed-responsive-item point-no", { "mirror-q": flipCapture })} 
									id={"videoCapture-" + VIDEO_ID} // "videoCapture" 
									tabIndex="-1" 
								/>

								{isCapture && 
									<div className="p-2 position-absolute b0 l0 r0 text-center">
										<Btn onClick={onStopCapture} kind="danger" className="qi qi-close">Cancel Capture</Btn>{" "}
										<Btn onClick={onSaveImg} className="qi qi-save">Save capture</Btn>
									</div>
								}
							</div>
						}
					</>
				}
			</div>
		</div>
	);
}

/*
			<div style={{ height: h }}>
				{reload && }
			</div>

<Flex dir="column" className={Q.Cx("ovhide position-relative imgEditor", wrapClass)}>

			<div tabIndex={tools ? undefined : -1} 
				className={Q.Cx("bg-white position-absolute t0 b0 r0 col-3 crop-tool", { "toolOpen": tools })}
			>
				<Btn As="div" onClick={() => setTools(!tools)} size="sm" kind="light" 
					className="round-right-0 position-absolute zi-1 crop-toggle-tool fas fa-sort i-r90" 
				/>

				<div className="p-2 bg-strip">
					Options
				</div>

				<div className="crop-options">

				</div>

				<Btn>Save</Btn>
			</div>

<div className="position-absolute t0 b0 zi-1 crop-toggle-tool">
	<Btn onClick={() => setTools(!tools)} size="sm" kind="light" className="round-right-0 fas fa-sort i-r90" />
</div>

<React.Fragment></React.Fragment>
*/
