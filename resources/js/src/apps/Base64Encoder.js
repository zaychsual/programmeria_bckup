import React from 'react';// , { createRef }
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';

import Btn from '../components/q-ui-react/Btn';
import Flex from '../components/q-ui-react/Flex';
import Textarea from '../components/q-ui-react/Textarea';
import Img from '../components/q-ui-react/Img';
import { fileRead } from '../utils/file/fileRead';
import { clipboardCopy } from '../utils/clipboard';

const DATE_OPTIONS = { 
	weekday: "long", 
	year: "numeric", 
	month: "long", 
	day: "numeric" 
};

const SYMBOLS = /[\r\n%#()<>?\[\\\]^`{|}]/g;
const EXM_SVG = `<svg>
	<circle r="50" cx="50" cy="50" fill="tomato"/>
	<circle r="41" cx="47" cy="50" fill="orange"/>
	<circle r="33" cx="48" cy="53" fill="gold"/>
	<circle r="25" cx="49" cy="51" fill="yellowgreen"/>
	<circle r="17" cx="52" cy="50" fill="lightseagreen"/>
	<circle r="9" cx="55" cy="48" fill="teal"/>
</svg>`;
const TB_SIZE = { tabSize: 2 };

export default class Base64Encoder extends React.Component{
  constructor(props){
    super(props);
    this.state = {	
			initTextarea: "",
			resultTextarea: "",
			resultCssTextarea: "",
			externalQuotesValue: `'`, // 'single'
			bg: null,
			bgSvg: null,

			files: null, // For bitmap
		};
		// this.symbols = /[\r\n%#()<>?\[\\\]^`{|}]/g;
  }
	
	encodeSVG = (data) => {
		const { externalQuotesValue } = this.state;
		// Use single quotes instead of double to avoid encoding.
		if(externalQuotesValue === `"`){
			data = data.replace(/"/g, '\'');
		}else{
			data = data.replace(/'/g, '"');
		}
		data = data.replace(/>\s{1,}</g, "><");
		data = data.replace(/\s{2,}/g, " ");
		return data.replace(SYMBOLS, encodeURIComponent);
	}
	
	addNameSpace(data){
		if(data.indexOf('http://www.w3.org/2000/svg') < 0){
			data = data.replace(/<svg/g, `<svg xmlns="http://www.w3.org/2000/svg"`);
		}
		return data.trim();
	}
	
	onChangeInit = (e) => {
		let val = e.target.value;
		this.setState({
			initTextarea: val,
			resultTextarea: val.length > 0 ? this.encodeSVG(val) : val
		},() => {
			this.getResults();
		});
	}
	
	getResults = () => {
		const { initTextarea, externalQuotesValue } = this.state;
		if(initTextarea.length < 1){
			this.setState({
				resultCssTextarea: "",
				bgSvg: null
			});
			return;
		}

		// let namespaced = this.addNameSpace(initTextarea);
		let escaped = this.encodeSVG(this.addNameSpace(initTextarea));
		// background-image: 
		// let bg = escaped ? `url(${externalQuotesValue}data:image/svg+xml,${escaped}${externalQuotesValue})` : "";
		
		this.setState({
			// bgSvg: bg, 
			resultTextarea: escaped,
			// resultCssTextarea: bg
		}, () => {
			if(escaped){
				let bgSvg = `url(${externalQuotesValue}data:image/svg+xml,${escaped}${externalQuotesValue})`;
				this.setState({
					bgSvg, 
					resultCssTextarea: bgSvg
				})
			}
		});
	}
	
	onBackground = bg => this.setState({ bg });
	
	onSetQuotes = (q) => {
		if(this.state.externalQuotesValue === q) return;
		this.setState({ externalQuotesValue: q }, () => {
			this.getResults()
		});
	}
	
	onExample = () => {
		this.setState({
			initTextarea: EXM_SVG,
			// resultTextarea: this.encodeSVG(svg)
		},() => {
			this.getResults();
		});
	}

	onCopy = (e, t) => {
		let et = e.target;
    clipboardCopy(t).then(() => {
      Q.setAttr(et, {"aria-label": "Copied!"});
      setTimeout(() => Q.setAttr(et, "aria-label"), 999);
    }).catch(e => console.log(e));
	}
	
	render(){
		const { initTextarea, resultTextarea, resultCssTextarea, externalQuotesValue, bgSvg, bg, files } = this.state;
		// const { pos, tipTxt } = this.props;
		
		// <div className="container-fluid py-3">
		return (
			<div className="bg-white p-3">				
				<h3 className="hr-h">URL-encoder for image</h3>

				<Tab.Container mountOnEnter id="base64-tools" defaultActiveKey="bitmap">
					<Nav variant="pills">
						<Nav.Item>
							<Nav.Link as="button" type="button" eventKey="bitmap" className="btn">Bitmap</Nav.Link>
						</Nav.Item>
						<Nav.Item>
							<Nav.Link as="button" type="button" eventKey="svg" className="btn">Svg / vector</Nav.Link>
						</Nav.Item>
					</Nav>

					<Tab.Content className="mt-3">
						<Tab.Pane eventKey="bitmap">
							<div>
								<h4>Bitmap</h4>
								<div className="custom-file mb-2">
									<input type="file" className="custom-file-input" id="file" 
										accept=".jpg,.JPG,.jpeg,.JPEG,.png,.PNG,.gif,.GIF,.bmp,.BMP,.svg,.SVG,.webp,.WEBP" 
										onChange={e => {
											let file = e.target.files[0];
											if(file){
												console.log(file);
												fileRead(file).then(r => {
													console.log(r);
													this.setState({ files: r });
												}).catch(e => console.log(e));
											}
										}}
									/>
									<label className="custom-file-label" htmlFor="file">{files ? files.file.name : "Choose file"}</label>
								</div>

								{files && // Q.dateObj(file.lastModified)
									<>
										<h4>Result</h4>
										<Img thumb alt={files.file.name} src={files.result} />

										<div className="alert alert-info pre-wrap mt-2">{`File name			: ${files.file.name}
File size			: ${Q.bytes2Size(files.file.size)}
File type			: ${files.file.type}
Last modified		: ${new Date(files.file.lastModified).toLocaleDateString("id-ID", DATE_OPTIONS)} 
`}</div>
										<div className="form-group mt-23">
											<Flex As="label" htmlFor="base64text" align="center">
												Base64
												<Btn onClick={e => this.onCopy(e, files.result)} size="xs" className="tip tipTR ml-auto">Copy</Btn>
											</Flex>
											<textarea className="form-control form-control-sm bg-white" id="base64text" readOnly rows={9} value={files.result} />
										</div>
									</>
								}
							</div>
						</Tab.Pane>

						<Tab.Pane eventKey="svg">
							<div>
								<h4>Svg / vector</h4>

								<h5 className="py-2 ml-1-next">
									External quotes:{" "}
									{
										[
											{ q:`'`, t:"Single" }, 
											{ q:`"`, t:"Double" }
										].map(v => 
											<Btn key={v.t} outline size="xs" kind="info" 
												onClick={() => this.onSetQuotes(v.q)} 
												active={externalQuotesValue === v.q} 
												title={v.t + " quotes"} 
											>
												{v.t}
											</Btn>
										)
									}
								</h5>
								
								<div className="row">
									<div className="col-md-6 mb-2">
										<Flex justify="between" className="mb-1">
											Insert SVG code: <Btn onClick={this.onExample} size="xs" kind="info">Example</Btn>
										</Flex>
										{/* <textarea onChange={this.onChangeInit} className="form-control" rows="6" value={initTextarea} /> */}
										<Textarea 
											rows="6" 
											style={TB_SIZE} 
											spellCheck="false" 
											value={initTextarea} 
											onChange={this.onChangeInit}
										/>

										<Flex justify="between" className="mt-3 mb-1">
											Preview: 
											<div>
												<input className="btn btn-xs btn-fff" type="color" 
													title="Change background color" 
													onChange={e => this.onBackground(e.target.value)} 
												/>{" "}

												<Btn onClick={() => this.onBackground(null)} size="xs" kind="fff">Transparent</Btn>
												{/* <Btn onClick={() => this.onBackground('silver')} size="xs" tip="Silver" style={{backgroundColor:'silver'}}> &nbsp;</Btn>{' '} */}
												{/* <Btn onClick={() => this.onBackground('black')} size="xs" kind="dark" tip="Black"> &nbsp;</Btn> */}
											</div>
										</Flex>
										
										<div className="rounded svg2url-view" 
											style={{ backgroundColor: bg }}
										>
											<div  
												className="bg-no-repeat ovauto resize-native resize-v qi qi-arrows-v" 
												style={{ 
													backgroundImage: bgSvg, 
													height: 156
												}}
											/>
										</div>
									</div>

									<div className="col-md-6 mb-2">
										<div className="mb-1">Take encoded:</div>
										<Textarea 
											style={TB_SIZE} 
											readOnly 
											rows="6" 
											value={resultTextarea}
										/>

										<div className="mt-3 mb-1">Ready for CSS:</div>
										<Textarea 
											style={TB_SIZE} 
											readOnly 
											rows="6" 
											value={resultCssTextarea}
										/>
									</div>
									
								</div>
							</div>
						</Tab.Pane>
					</Tab.Content>
				</Tab.Container>
			</div>
		);
	}
}
