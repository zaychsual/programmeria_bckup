import React from 'react';
// import P from 'prop-types';
// import stylePropType from 'react-style-proptype';
// import {Cx} from '../../utils/Q';

export const RESIZER_DEFAULT_CLASSNAME = 'Resizer';

export default class Resizer extends React.Component{
  render(){
		// resizerClassName = ORI
    const {className, onClick, onDoubleClick, onMouseDown, onTouchEnd, onTouchStart, resizerClass, split, style, ...etc} = this.props;
    // const classes = [resizerClass, split, className];

    return (
      <div { ...etc} // Q-CUSTOM
				// tabIndex="-1" 
        role="presentation"
        className={Q.Cx(resizerClass, split, className)} // classes.join(' ')
        style={style}
        onMouseDown={e => onMouseDown(e)}
        onTouchStart={e => {
          e.preventDefault();
          onTouchStart(e);
        }}
        onTouchEnd={e => {
          e.preventDefault();
          onTouchEnd(e);
        }}
        onClick={e => {
          if(onClick){
            e.preventDefault();
            onClick(e);
          }
        }}
        onDoubleClick={e => {
          if(onDoubleClick){
            e.preventDefault();
            onDoubleClick(e);
          }
        }}
      />
    );
  }
}

Resizer.defaultProps = {
  resizerClass: RESIZER_DEFAULT_CLASSNAME
};

/* Resizer.propTypes = {
  className: P.string.isRequired,
  onClick: P.func,
  onDoubleClick: P.func,
  onMouseDown: P.func.isRequired,
  onTouchStart: P.func.isRequired,
  onTouchEnd: P.func.isRequired,
  split: P.oneOf(['vertical', 'horizontal']),
  // style: stylePropType,
  resizerClass: P.string.isRequired
}; */

// export default Resizer;
