import React from 'react';
import { Route } from 'react-router-dom';

export default class RouteLazy extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      isError: false
    };
  }

  static getDerivedStateFromError(){// err
    // Update state so the next render will show the fallback UI.
    // console.log('getDerivedStateFromError in RouteLazy err: ',err);
    return { isError: true };
  }

  componentDidCatch(err, errInfo){
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    console.log('componentDidCatch in RouteLazy err: ', err);
    console.log('componentDidCatch in RouteLazy errInfo: ', errInfo);

		// fetch('https://jsonplaceholder.typicode.com/posts', {
		// 	method: 'POST',
		// 	body: JSON.stringify({
		// 		title: 'foo',
		// 		body: 'bar',
		// 		userId: 1
		// 	}),
		// 	headers: {
		// 		"Content-type": "application/json; charset=UTF-8"
		// 	}
		// })
		// .then(response => response.json())
		// .then(json => console.log(json));

    //  let sb = navigator.sendBeacon(
    //    'https://reqres.in/api/users',
    //    JSON.stringify({
		// 		name: 'Husein',
		// 		job: 'Programmer'
		// 	})
    //  );
    //  console.log('sb: ', sb);
  }

  render(){
    const { errorMsg, path, children, ...etc } = this.props;

    if(this.state.isError){
      // You can render any custom fallback UI
      // return errorMsg; // <h1>Something went wrong.</h1>
      if(errorMsg) return errorMsg;

      return (
        <div role="alert" 
          className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
        >&#9888;{` Something went wrong. 

${navigator.onLine ? 'Server error.' : 'Your internet connection is offline.'}`}</div>
      );
    }

    // return this.props.children;
    return path ? <Route {...etc} path={path} /> : children;
  }
}