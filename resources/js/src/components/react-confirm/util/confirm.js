import Confirmation from '../components/Confirmation';
// import ComplexConfirmation from '../components/ComplexConfirmation';

// import { createConfirmation } from 'react-confirm';
import createConfirmation from '../createConfirmation';

const defaultConfirmation = createConfirmation(Confirmation);

export function confirm(text, options = {}) { // confirmation, options = {}
  return defaultConfirmation({ text, ...options });
}

// export const confirmComplex = createConfirmation(ComplexConfirmation);
