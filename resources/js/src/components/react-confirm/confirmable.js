// import React from 'react';

const confirmable = (El) => class extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      show: true
    };
		
		Q.bindFuncs.call(this,['dismiss','cancel','proceed']);
  }
	
  dismiss(){
    this.setState({
      show: false
    }, () => this.props.dispose());
  }
	
  cancel(v){
    this.setState({
      show: false
    }, () => this.props.reject(v));
  }
	
  proceed(v){
    this.setState({
      show: false
    }, () => this.props.resolve(v));
  }
	
  render(){
    return (
			<El  
				proceed={this.proceed} 
				cancel={this.cancel} 
				dismiss={this.dismiss} 
				show={this.state.show} 
				{...this.props} 
			/>
		)
  }
}

// <Component proceed={::this.proceed} cancel={::this.cancel} dismiss={::this.dismiss} show={this.state.show} {...this.props}/>

export default confirmable;
