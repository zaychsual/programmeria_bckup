import React from 'react';
// import PropTypes from 'prop-types';

// import Modal from 'react-bootstrap/Modal';
// import Button from 'react-bootstrap/Button';
import Modal from 'reactstrap/es/Modal';
import ModalHeader from 'reactstrap/es/ModalHeader';

import Btn from '../../q-ui-react/Btn';

// import { confirmable } from 'react-confirm';
import confirmable from '../confirmable';

const Confirmation = ({
	okLabel = "OK",
	cancelLabel = "CANCEL",
	title,
	text, // confirmation
	show,
	proceed, 
	esc = false, // enableEscape = true
	
	size, 
	toastInfoText, 
	icon, 
	type = "modal", // modal / toast
	bodyClass, 
	btnsWrapClass, 
	// ...etc, // ERROR
	backdrop, 
	modalProps
}) => {
	const isModal = type === "modal";
	const bodyCx = { className: Q.Cx(type + "-body", bodyClass) };
	
	const Btns = () => (
		<div className={Q.Cx(btnsWrapClass)}>
			{cancelLabel && <Btn onClick={() => proceed(false)} size="sm" kind="danger">{cancelLabel}</Btn>}
			{" "}
			{okLabel && <Btn onClick={() => proceed(true)} size="sm">{okLabel}</Btn>}
		</div>
	);
	
	return (
		<Modal 
			{...modalProps} 
			isOpen={show} 
			// toggle={() => proceed(false)} 
			backdrop={esc ? true : Q.isBool(backdrop) ? backdrop : "static"} 
			keyboard={esc} 
			// size={type === "toast" ? "sm" : size ? size : null} 
			size={size} 
			
			// !isModal ? "bg-transparent border-0" : ""
			contentClassName={
				Q.Cx({
					"bg-transparent border-0": !isModal
				}, modalProps.contentClassName)
			} 
		>
			{isModal && 
				<ModalHeader toggle={() => proceed(false)}>
					{title}
				</ModalHeader>
			}
			
			{isModal ? 
				<div {...bodyCx}>
					{text}
				</div>
				: 
				<div>
					<div className="toast o-1 mb-0 mx-auto" role="alert" aria-live="assertive" aria-atomic="true">
						<div className="toast-header">
							{icon} 
							{/* <img src="..." className="rounded mr-2" alt="..." /> */}
							
							{title && <strong>{title}</strong>}
							
							<div className="d-flex align-items-center ml-auto ml-2-next">
								{toastInfoText && <small>{toastInfoText}</small>}
								
								<button
									type="button"
									className="close mb-1" 
									aria-label="Close" 
									onClick={() => proceed(false)} 
								>
									&times;
								</button>
							</div>
						</div>
						
						<div {...bodyCx}>
							{text} 
							
							<Btns />
						</div>
					</div>
				</div>
			}

			{isModal && 
				<div className="modal-footer">
					<Btns />
				</div>
			}
		</Modal>
	);
}

export default confirmable(Confirmation);

// <div className="static-modal">
// </div>

/* Confirmation.propTypes = {
  okLabbel: PropTypes.string,
  cancelLabel: PropTypes.string,
  title: PropTypes.string,
  confirmation: PropTypes.string,
  show: PropTypes.bool,
  proceed: PropTypes.func,     // called when ok button is clicked.
  enableEscape: PropTypes.bool,
}; */
