import React from 'react';
import {xhrScript} from '../../utils/xhrScript';
import LiveCode from './LiveCode';

export default function Ngoding({code, ...etc}){
	const [codes, setCodes] = React.useState(window.Babel ? code : undefined);// undefined
	// const [codeNoAuto, setCodeNoAuto] = React.useState(undefined);
	
	React.useEffect(() => {
		const loadBabel = () => {
			/* if(code && !codes){
				getScripts({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"})
				.then(v => {
					if(window.Babel){
						// setTimeout(() => {
							console.log('%cBabel: ', 'color:orange', window.Babel);
							setCodes(code);
							// onRender();
						// },9);
					}
				}).catch(e => {
					console.log(e);
					return false;
				});// NOT RUN...!!!
			} */
			
			/* if(window.Babel){
				console.log('%cBabel is available: ', 'color:orange', window.Babel);
				setCodes(code);
			}else{
				getScripts({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"})
				.then(v => {
					if(window.Babel){
						// setTimeout(() => {
							console.log('%cBabel: ', 'color:orange', window.Babel);
							setCodes(code);
							// onRender();
						// },9);
					}
				}).catch(e => {
					console.log(e);
					return false;
				});// NOT RUN...!!!
			} */
			
			if(window.Babel){//  && renderCount < 1
				setCodes(code);
				// onRender();
			}else{
				xhrScript("/storage/app_modules/@babel-standalone/babel.min.js", "Babel", 
					(fn, v) => {
						// console.log('xhrScript: ', v);
						// console.log('xhrScript: ', fn);
						setCodes(code);
						// onRender();
					},
					(e) => console.warn(e)
				);
			}
		}
		
		loadBabel();
	}, []);
	
	if(codes){
		return (
			<LiveCode {...etc} code={codes} />
		)
	}
	
	return <div>Loading</div>;
}