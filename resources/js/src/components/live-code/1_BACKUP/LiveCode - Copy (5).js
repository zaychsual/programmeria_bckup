import React from 'react';
// import { useInView } from '../react-intersection-observer';// InView
import SplitPane from '../../component-etc/react-split-pane';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-react-bootstrap/Btn';
import Switch from '../../components/q-react-bootstrap/Switch';
// import UctrlAlert from '../../components/q-react-bootstrap/UctrlAlert';// OPTION
import Compiler from './Compiler';
import {ControlledEditor} from '../../components/monaco-react';
import {ResizeSensor} from '../blueprintjs/components/resize-sensor/resizeSensor';
import {Copy} from '../../utils/clipboard/clipboardApi';
// import copy from '../../utils/clipboard/copy-to-clipboard';
import {BabelStandalone} from '../../utils/react/load-llib/Babel';
// import loadable from '@loadable/component';

/** NOT FIX ISSUE: can't render component twice if Babel load with getScripts */
// 
export default class LiveCode extends React.Component{
  constructor(props){
		super(props);
		this.state = {
			// themes: props.theme,
			pos: 'vertical',
			ideOpen: false, 
			autoRun: props.auto,
			codeAuto: props.code, // undefined | props.code
			codeNoAuto: props.code, // undefined | props.code
			err: null,
			editorReady: false,
			maxSize1: 800,
			// mount: false,
		};
		// this._id = null;
		this.ideRef = React.createRef();
		this.liveRef = React.createRef();
		this.babelRef = React.createRef();
	}	
	
	componentDidMount(){// async 
		console.log('%ccomponentDidMount in LiveCode', 'color:yellow');
    // await this.onGetBabel();
	}
	
	/* componentDidUpdate(prevProps, prevState){
		// console.log('%ccomponentDidUpdate in LiveCode this.props.name: ', 'color:red', this.props.name);// , this.state.mount
		// console.log('%ccomponentDidUpdate in LiveCode this.props.code: ', 'color:red', this.props.code);
		// console.log('%ccomponentDidUpdate in LiveCode prevState.codeAuto: ', 'color:red', prevState.codeAuto);
		// console.log('%ccomponentDidUpdate in LiveCode this.state.codeAuto: ', 'color:red', this.state.codeAuto);
		
		if(window.Babel && this.props.id > 0 && this.props.code && !prevState.codeAuto && !this.state.codeAuto){
			console.log('%ccomponentDidUpdate in LiveCode', 'color:red', this.props.name);// , this.state.mount
			console.log('%ccomponentDidUpdate in LiveCode', 'color:red', this.props.code);
			this.setState({
				codeAuto: this.props.code,
				codeNoAuto: this.props.code,
				// mount: true
				// maxSize1: liveRef.clientWidth / 1.4
			}, () => this.props.onRender());
		}
	} */
	
	/* onGetBabel = async () => {
		const {code, onRender} = this.props;// 
		// const liveRef = this.liveRef.current;
		if(window.Babel){
			console.log('%cBabel available: ', 'color:orange', window.Babel);
			this.setState({
				codeAuto: code,
				codeNoAuto: code,
				// mount: true
				// maxSize1: liveRef.clientWidth / 1.4
			}, () => onRender());
		}
		else{
			await getScripts({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"})
			.then(v => {
				if(window.Babel){
					// setTimeout(() => {
						console.log('%cBabel: ', 'color:orange', window.Babel);
						this.setState({
							codeAuto: code,
							codeNoAuto: code,
							// mount: true
							// maxSize1: liveRef.clientWidth / 1.4
						}, () => onRender());
						// onRender();
					// },9);
				}
			}).catch(e => {
				console.log(e);
				return false;
			});// NOT RUN...!!!
		}
	} */
	
  onDidChangeEditor = (ev, editor) => {
    if(!this.state.editorReady){
      this.setState({editorReady: true});
      this.ideRef.current = editor;
      
      if(editor){ // this.ideMain.current
				// Get bind editor contextmenu FROM https://github.com/Microsoft/monaco-editor/issues/484
				editor.onContextMenu(e => {
					let ctxMenu = domQ(".monaco-menu-container", editor.getDomNode());// this.ideMain.current
					if(ctxMenu){
						// window.outerHeight
            let ee = e.event,
                ch = ctxMenu.clientHeight,
                cw = ctxMenu.clientWidth,
                cs = ctxMenu.style;
						const posY = (ee.posy + ch) > window.innerHeight ? ee.posy - ch : ee.posy;
						// window.outerWidth
						const posX = (ee.posx + cw) > document.body.clientWidth ? ee.posx - cw : ee.posx;
				
						cs.position = "fixed";// OPTIONS: set in css internal
						cs.top = Math.max(0, Math.floor(posY)) + "px";
						cs.left = Math.max(0, Math.floor(posX)) + "px";
					}
				});
      }
    }
  }
  
  onSetPosition = () => {
		const liveRef = this.liveRef.current;
    this.setState(s => ({
			pos: s.pos === 'vertical' ? 'horizontal':'vertical',
			maxSize1: s.pos === 'vertical' ? liveRef.clientHeight / 1.3 : liveRef.clientWidth / 1.3
		}), () => {
      let compilerCode = domQ('.compilerCode', liveRef);
      if(compilerCode){
        let s = compilerCode.style;
        if(s.width !== '50%' && s.height !== '50%'){
          this.state.pos === 'vertical' ? s.width = '50%' : s.height = '50%';
        }
      }
    })
  }

  onResize = (ent) => {
    // `${e.contentRect.width} x ${e.contentRect.height}`
    const ents = ent.map(e => [e.contentRect.width, e.contentRect.height]);
    const setMaxSize1 = this.state.pos === 'vertical' ? ents[0][0] / 1.3 : ents[0][1] / 1.3;
    this.setState({maxSize1: setMaxSize1});
  }
	
	onCopy = (to, e) => {
		let et = e.target;
		Copy(to, {
			onOk: (t) => {
				if(et){
					let pTitle = getAttr(et, 'aria-label');// et.title;
					// console.log('onOk: ',t);
					setAttr(et, {'aria-label':'Copied'});// et.title = 'Copied';
					setTimeout(() => setAttr(et, {'aria-label':pTitle}), 1000);// et.title = pTitle
				}
			},
			onErr: (e) => console.warn('Error copy: ',e)
		})
	};
	
	render(){
		// auto, code
    const {scope, inRef, height, className, viewClass} = this.props;
    // themes
		const {editorReady, ideOpen, pos, autoRun, codeAuto, codeNoAuto, err, maxSize1} = this.state;
		
		return (
			<Flex inRef={inRef} dir="column" className={Cx("liveCode", className)}>
				<Flex className="ml-1-next">				
					<Btn onClick={() => this.setState({ideOpen: !ideOpen})} size="sm" className="q q-json q-s12" tip="Show code" />
					
					{(editorReady && ideOpen) && 
						<React.Fragment>
							<Btn onClick={e => this.onCopy(codeAuto, e)} size="sm" qtip="BL" tip="Copy code">Copy</Btn>
							<Btn onClick={this.onSetPosition} size="sm">{pos}</Btn>

              {/* <select onChange={e => {
								this.setState({themes: e.target.value}, () => console.log('themes: ', this.state.themes));
							}} value={themes} className="custom-select custom-select-sm w-auto">
								<option value="light">Light</option>
								<option value="dark">Dark</option>
							</select> */}
							
							{/* <Btn onClick={() => this.setState(!autoRun)} size="sm" kind={autoRun ? "primary" : "secondary"} disabled={!ideOpen}>Auto Run</Btn> */}
							<Switch 
								label="Auto Run" 
								// checked={!autoRun} 
								defaultChecked={autoRun} 
								// disabled={!ideOpen} 
								onChange={e => this.setState({autoRun: e.target.checked})}
							/>
						
							{/* disabled={autoRun}  */}
							{!autoRun && <Btn onClick={() => this.setState({codeNoAuto: codeAuto})} size="sm" kind="primary">RUN</Btn>}
						</React.Fragment>
					}
				</Flex>
				
				<ResizeSensor onResize={this.onResize}>
					<div 
						ref={this.liveRef} 
						tabIndex="-1" 
						style={{height: height}} 
						className="position-relative liveBox" 
					>
						<SplitPane
							split={pos} // "horizontal" // vertical
							minSize={50}
							maxSize={maxSize1} // 1000 | 300
							// defaultSize={100}
							// defaultSize="100%" // 50%
							size={ideOpen ? "50%" : "100%"} 
							allowResize={ideOpen} 
							className="border position-absolute" 
							// className={Cx("border position-absolute", themes)} 
							pane1Class="compilerCode" 
							pane2Class="ovhide bg-dark" 
							// pane2Class={`ovhide bg-${themes}`} 
							// (codeAuto.length > 0 && codeNoAuto.length > 0)
						>
							<div className={Cx("h-100 bg-white liveView", viewClass)}>
								{/* {codeAuto &&  */}
                  <BabelStandalone inRef={this.babelRef}>
                    {(Babel) => 
                      <React.Fragment>
                        {Babel && 
                          <Compiler 
                            babel={Babel} 
                            code={autoRun ? codeAuto : codeNoAuto} 
                            scope={{Btn}} 
                            setError={(e) => {
                              if(e){
                                let err = e.replace('/file.tsx: ', '');
                                this.setState({err});
                              }else{
                                this.setState({err: null});
                              }
                            }}
                            // preview={err !== null ? ErrBabel : null} 
                            className="liveCoding"  
                          />
                        }
                      </React.Fragment>
                    }
                  </BabelStandalone>
								{/* } */}
							</div>

							{ideOpen && 
								<SplitPane 
									split="horizontal" 
									allowResize={err !== null} 
									size={err !== null ? "50%" : "100%"} 
									// pane1Class="ovhide" 
									// resizerClass={err === null ? 'd-none':''} 

									pane2Class="bg-white ovhide"
								>
									<div className="w-100 live-app">
										{/* <textarea onChange={e => this.setState({codeAuto: e.target.value})} value={codeAuto} className="form-control form-control-sm" rows="9" /> */}
										<ControlledEditor
											// height="calc(100% - 31px)" // calc(100vh - 75px) 
											// className={Cx({'d-none': tools !== 'Compiler'})} 
											editorDidMount={this.onDidChangeEditor} 
											value={codeAuto} 
											onChange={(e, codeAuto) => {
												this.setState({codeAuto});
												// console.log('%cControlledEditor value: ', LOG_DEV, v);
											}} 
											// language={compilerLang} // ideLang | 'javascript'
											// theme={themes} 
											options={{
												fontSize: 14, // fontSize
												tabSize: 2, // tabSize
												// dragAndDrop: false,
												minimap: {
													enabled: false, // minimapEnabled, // Defaults = true
													// side: minimapSide, // Default = right | left
												},
												// mouseWheelZoom: zoomIde
											}}
										/>
									</div>
									
									{err && <pre className="alert alert-danger mb-0 border-0 rounded-0 h-100 q-scroll">{err}</pre>} 
								</SplitPane>
							}
						</SplitPane>
					</div>
				</ResizeSensor>
			</Flex>
		);
	}
}

LiveCode.defaultProps = {
  // theme: 'dark', // light | dark
	scope: {}, 
  height: 300,
	// code: '',
	auto: false,
	onRender: noop
};

// export default React.memo(LiveCode, (prevProps, nextProps) => prevProps.code === nextProps.code);

/*
</React.Fragment>
*/
