import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import 'antd/dist/antd.css';
import 'antd/lib/tree/style/index.css';
import { Tree } from 'antd';

// import Btn from '../../components/q-ui-react/Btn';

// const { DirectoryTree } = Tree;

export default function AntdTree({
	data = [], 
	className, 
	onClick = Q.noop, 
	...etc
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in AntdTree','color:yellow;');
	// }, []);

// NOT WORK???
	// const ClickTree = (selectedKeys, info) => { // SelectTree
	// 	console.log('ClickTree selectedKeys: ', selectedKeys);
	// 	console.log('ClickTree info: ', info);
	// }
	
  const Expand = (keys, data) => {
		console.log('Expand keys: ', keys);
		console.log('Expand data: ', data);
		onClick(data);
	};
	
	const CtxMenu = data => {
		console.log('CtxMenu data: ', data);
	}

	return (
		<Tree.DirectoryTree 
			{...etc}
			className={Q.Cx("no-caret-ico", className)} 
			// showLine 
			// multiple 
			selectable={false} 
			draggable 
			// blockNode 
			// switcherIcon={undefined} // <DownOutlined />
			// defaultExpandedKeys={} // ['0-0-0'] 
			// icon={(p) => <i className="fal fa-cog">{console.log(p)}</i>} 
			// onSelect={ClickTree} // SelectTree
			onExpand={Expand} 
			onRightClick={CtxMenu} 
			treeData={data}
		/>
	);
}

/*
<React.Fragment></React.Fragment>
*/
