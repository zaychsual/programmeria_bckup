import React from 'react';

export default class ErrorBoundary extends React.Component{
  constructor(props){
    super(props);
    this.state = { isError: false };
  }

  static getDerivedStateFromError(){// err
    // Update state so the next render will show the fallback UI.
    // console.log('getDerivedStateFromError err: ',err);
    return { isError: true };
  }

  componentDidCatch(err, errInfo){
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    console.log('componentDidCatch in ErrorBoundary err: ', err);
    console.log('componentDidCatch in ErrorBoundary errInfo: ', errInfo);
    //  let sb = navigator.sendBeacon(
    //    'https://reqres.in/api/users',
    //    JSON.stringify({
		// 		name: 'Husein',
		// 		job: 'Programmer'
		// 	})
    //  );
    //  console.log('sb: ', sb);
  }

  render(){
    if(this.state.isError){
      // You can render any custom fallback UI
      return this.props.errorMsg; // <h1>Something went wrong.</h1>
    }

    return this.props.children;
  }
}