import React from 'react';// { useState, useEffect }
// import { useHistory } from 'react-router-dom';
import { Modal, ModalHeader } from "reactstrap";// , ModalBody, ModalFooter

// import Btn from '../../components/q-ui-bootstrap/Btn';

export default function ModalQ({
	open, 
	toggle, 
	modalClassName, 
	// onOpened, 
	// onClosed, 

	// Custom:
	position, 
	head, 
	headProps, 
	// headAs = "h5", 
	close = true, 
	title, 
	body, 
	foot, 
	headClass, 
	bodyClass, 
	footClass, 
	path, // DEV OPTION

	// Spread Modal props:
	...etc
}){
	// let history = useHistory();
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ModalQ','color:yellow;');
	// }, []);

	// useEffect(() => {
	// 	// console.log('%cuseEffect in NavApp','color:yellow;');
	// 	setOpenSet(history.location.hash === "#settings");
	// }, [history.location]);

	// console.log('history: ', history);

	// const onModal = () => {
	// 	if(toggle) toggle();

	// 	if(path){
	// 		console.log('onModal history: ', history);
	// 		if(open){
	// 			console.log('onModal if open');
	// 			history.goBack();
	// 		}
	// 		else{
	// 			console.log('onModal else open');
	// 			history.push(`${history.location.pathname}${path}`);// #settings
	// 		}
	// 	}
	// 	// console.log(history);
	// };

	// const onOpen = () => {
	// 	if(onOpened) onOpened();

	// 	if(path){
	// 		console.log('onOpen history: ', history);
	// 		history.push(`${history.location.pathname}${path}`);// #settings
	// 	}
	// }

	// const onClose = () => {
	// 	if(onClosed) onClosed();

	// 	if(path){
	// 		console.log('onClose history: ', history);
	// 		// if(open){
	// 			// console.log('onModal if open');
	// 			history.goBack();
	// 		// }
	// 		// else{
	// 		// 	console.log('onModal else open');
	// 		// 	history.push(`${history.location.pathname}${path}`);// #settings
	// 		// }
	// 	}
	// }

	return (
		<Modal 
			{...etc}
			isOpen={open} 
			toggle={toggle} // toggle 
			// scrollable={position !== "default"} 
			centered={position === "center"} // "centered"
			// className={className} 
			// position && position !== "default" && position !== "centered" ? position : undefined
			// position && position !== "center" ? position : undefined
			modalClassName={Q.Cx({ [position]: position && position !== "center" }, modalClassName)} 
			// contentClassName={} 
			// onOpened={onOpen} 
			// onClosed={onClose} 
		>
			{(title || close) && // closeHead
				<ModalHeader 
					{...headProps} 
					// tag={headAs}  
					toggle={close && toggle} // closeHead && toggle
					className={headClass}
				>
					{title}
				</ModalHeader>
			}

			{head}
			
			{body && <div className={Q.Cx("modal-body", bodyClass)}>{body}</div>}
			
			{foot && <div className={Q.Cx("modal-footer", footClass)}>{foot}</div>}
		</Modal>
	);
}

/*
<React.Fragment></React.Fragment>
*/
