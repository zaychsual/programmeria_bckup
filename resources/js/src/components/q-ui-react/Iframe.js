import React from 'react';
// import {findDOMNode} from "react-dom";
// import { Cx } from '../../utils/Q.js';
// import { domQ } from '../../libs/dom-q';

// sameOrigin, onClick, onKeyDown, onError, onLoad
export default function Iframe({
	src, 
	inRef, 
	bs, 
	className, 
	title, 
  wrapClass, 
  wrapProps, 
	children, // OPTION
	ratio = '16by9', 
	...etc
}){
  // const [titleFrame, setTitleFrame] = React.useState(null);

  // const Err = e => {
    // onError(e);
  // };

  /*const Load = e => {
    // let titleTag = e.target.contentWindow.document.title;
    // if(!title && titleTag){
      // setTitleFrame(titleTag);
    // }
		let et = e.target;
		
		et.contentWindow.document.addEventListener('click', e => {
			e.stopPropagation();
			// let docActive = document.activeElement;
			let activeEl = domQ('[aria-expanded="true"]');//  || domQ('.active')
				// console.log(docActive);
			if(activeEl) activeEl.click();
			
			if(onClick) onClick(e);
		});
		
		if(typeof onKeyDown === 'function'){
			et.contentWindow.document.addEventListener('keydown', e => {
				e.stopPropagation();
				
				onKeyDown(e);
			});
		}
		
    onLoad(e);
  };*/

  return (
    <div 
      {...wrapProps} 
			className={
        Q.Cx('iframe-q', {
          'embed-responsive': bs,
          [`embed-responsive-${ratio}`]: bs && ratio
        }, wrapClass)
      }
    >
      <iframe 
        {...etc} 
				ref={inRef} 
				src={Q.newURL(src)} 
				// src={sameOrigin & !src ? window.location.origin +'/'+ src : src} 
        // onError={e => onError(e)}
        // onLoad={e => onLoad(e)} // Load | onLoad(e)
        title={title} 
        className={Q.Cx({'embed-responsive-item': bs}, className)} 
      />
			
			{children}{/* OPTION */}
    </div>
  );
}

/* Iframe.defaultProps = {
  ratio: '16by9', // 21by9 = 21:9 | 16by9 = 16:9 | 4by3 = 4:3 | 1by1 = 1:1 
  // title: '', 
  // sameOrigin: true
}; */



