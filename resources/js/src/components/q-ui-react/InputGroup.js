import { groupBy } from 'lodash';
import React from 'react';
// import Btn from '../../components/q-ui-bootstrap/Btn';

export default function InputGroup({
	As = "label", 
	role = "group", 
	size, 
	className, 
	prepend, 
	append, 
	children, 
	...etc
}){
  // const [data, setData] = React.useState();
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in InputGroup','color:yellow;');
	// }, []);

	return (
		<As 
			{...etc} 
			className={
				Q.Cx("input-group", {
					// "input-group": group, 
					["input-group-" + size]: size, 
				}, className) 
			} 
			role={role}
		>
			{prepend && <div className="input-group-prepend">{prepend}</div>}

			{children} 

			{append && <div className="input-group-append">{append}</div>} 
		</As>
	);
}

/*
<React.Fragment></React.Fragment>
*/
