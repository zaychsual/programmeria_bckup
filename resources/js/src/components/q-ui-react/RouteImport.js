import React from 'react';
import { Route } from 'react-router-dom';

// import Import from './Import';

export default function RouteImport({ module, ...etc }){ // children
  const [mod, setMod] = React.useState();
	const [err, setErr] = React.useState(false);
	
	React.useEffect(() => {
		// console.log('%cuseEffect in RouteImport','color:yellow;');
		function getMod(){
			importShim(module).then(m => { // '/js/esm/Laravel.js'
				console.log(m);
				const modName = Object.keys(m);
				if(modName && modName.length > 0){
					if(m.default){
						// console.log('if modName: ', modName);
						// console.log('if m: ', m);
						// window[fileName] = m.default;
						setMod(m.default);
					}else{
						// console.log('else modName: ', modName);
						// console.log('else m: ', m);
						// window[fileName] = m[modName[0]];
						setMod(m[modName[0]]);
					}
				}else{
					// console.warn(module + ' Not Found');
					setErr('Not Found Module in: ' + module);
				}
			})
			.catch(e => {
				console.log(e);
				setErr('Not Found Module in: ' + module);
			});

      // try {
			// 	// File name to function name & store to window
			// 	const fileName = 'Esm_' + module.split('/').pop().replace(/.js|.jsx|.ts|.tsx|.mjs/,'');
				
			// 	if(window[fileName]){// Use from window if available
			// 		setMod(window[fileName]);
			// 	}else{// Use from import module file & store to window
			// 		const m = await Import(module);
			// 		const modName = Object.keys(m);
			// 		// console.log(modName);// JSON.stringify(m) | .includes('TypeError:')
			// 		// console.log('fileName: ', fileName);
					
			// 		if(modName && modName.length > 0){
			// 			if(m.default){
			// 				// console.log('if modName: ', modName);
			// 				// console.log('if m: ', m);
			// 				window[fileName] = m.default;
			// 				setMod(m.default);
			// 			}else{
			// 				// console.log('else modName: ', modName);
			// 				// console.log('else m: ', m);
			// 				window[fileName] = m[modName[0]];
			// 				setMod(m[modName[0]]);
			// 			}
			// 		}else{
			// 			// console.warn(module + ' Not Found');
			// 			setErr('Not Found Module in: ' + module);
			// 		}
			// 	}
      // }catch(e){
      //   console.warn(e.message);
			// 	setErr(e.message);
      // }
		}
		getMod()
		// eslint-disable-next-line
	}, [module]);
	
	return (
		<Route {...etc}>
			<React.Suspense fallback={<div>Loading</div>}>
				{mod ? 
					mod : 
						err ? 
							<div className="d-flex flex-column justify-content-center align-items-center container-fluid flex1 text-danger q q-ban i-3x">
								<h5 className="my-2">{err}</h5>
							</div>
							: 
							null
				}
			</React.Suspense>
		</Route>
	)
}

/*
							<MainContent head={false} title={err}>
								<div className="d-flex flex-column justify-content-center align-items-center container-fluid flex1 text-danger q q-ban i-3x">
									<h5 className="my-2">{err}</h5>
								</div>
							</MainContent>

<React.Fragment></React.Fragment>
*/
