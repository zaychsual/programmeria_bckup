import React, { useRef, useEffect } from 'react';// useState
// import Btn from '../../components/q-ui-bootstrap/Btn';
// import { Cx } from '../../utils/Q';

/* function BgLoader({
	url, 
	// seconds = 1, 
	onLoad = () => {}, 
	onError = () => {}
}) {
  // let loaded = false;
  let img = new Image();

  // this will occur when the image is successfully loaded
  // no matter if seconds past
  img.onload = function(e){
    // loaded = true;
    // let div = document.getElementById("dk");
		// div.style.backgroundImage = "url('" + url + "')";
		// return url;
		onLoad(e);
		return;
	}
	img.onerror = function(e){
		onError(e);
		return;
	}
  img.src = url;

  // setTimeout(() => {
  //   // if when the time has passed image.onload had already occurred, run onLoad()
  //   if (loaded) onLoad();
  //   else onError();
  // }, seconds * 1000);
}*/

export default function BgImage({
	className, 
	url, 
	noRepeat, 
	style, 
	bgSize, //  = "auto", // auto | contain | cover | revert 
	w, //  = 30, // 50
	h, //  = 30, 
	As = "div", 
	// errImg = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cpath fill='currentColor' d='M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z'%3E%3C/path%3E%3C/svg%3E", 
	onLoad = () => {}, 
	onError = () => {}, 
	...etc
}){
	const elRef = useRef(null);
  // const [err, setErr] = useState(false);
	
	useEffect(() => {
		// console.log('%cuseEffect in BgImage','color:yellow;');
		// function loadBg(){
		// 	BgLoader({
		// 		url, 
		// 		onLoad: () => {
		// 			console.log('onLoad bgLoad');
		// 		},
		// 		onError: () => {
		// 			console.log('onError bgLoad');
		// 			setErr(true);
		// 		}
		// 	});
		// }
		// loadBg();

		// BgLoader({
		// 	url, 
		// 	onLoad: (e) => {
		// 		// console.log('onLoad bgLoad');
		// 		onLoad(url, e);
		// 	},
		// 	onError: (e) => {
		// 		// console.log('onError bgLoad');
		// 		setErr(true);
		// 		onError(url, e);
		// 	}
		// });

		let img = new Image();

		// this will occur when the image is successfully loaded, no matter if seconds past
		/* img.onload = function(e){
			onLoad(url, elRef.current, e);
			return null;
		}
		img.onerror = function(e){
			// setErr(true);
			onError(url, elRef.current, e);
			return null;
		} */
		
		const loadEvt = e => {
			onLoad(elRef.current, e);// img
			return;
		};
		const errEvt = e => {
			onError(elRef.current, e);
			return;
		};
		
		img.addEventListener("load", loadEvt);// , {once: true}
		img.addEventListener("error", errEvt);
		
		img.src = url;
		
		return () => {
			img.removeEventListener("load", loadEvt);
			img.removeEventListener("error", errEvt);
		}
		// 
	}, [url]);// , onLoad, onError

	// console.log('err: ', err);

	return (
		<As 
			{...etc} 
			ref={elRef} 
			className={
				Q.Cx("bg-img", {
					"bg-no-repeat": noRepeat
				}, className)
			} 
			style={{
				...style, 
				width: w, 
				height: h, 
				backgroundSize: bgSize, 
				// `url("${err ? errImg : Q.newURL(url).href}")` 
				backgroundImage: `url("${Q.newURL(url).href}")` 
			}}
		/>
	);
}

/*
<React.Fragment></React.Fragment>
*/
