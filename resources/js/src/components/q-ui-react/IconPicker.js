import React, { useRef, useState, useEffect } from 'react';// Fragment
import axios from 'axios';
import Dropdown from 'react-bootstrap/Dropdown';
// import { useInViewport } from 'ahooks';
import List from '@researchgate/react-intersection-list';

import Btn from './Btn';
import Flex from './Flex';
// import Aroute from './Aroute';
import { num2Hex } from '../../utils/number';
import { clipboardCopy } from '../../utils/clipboard';
// import { APP_DOMAIN } from '../../data/appData';
// import { domQall, hasClass, setClass, toggleClass, noop, Qid } from '../../utils/Q';

// function InViewPort({ inRef, children }){
//   // const ref = useRef();
//   const inView = useInViewport(inRef);

//   return (
//     <Fragment>
//       {children && children(inView)}
//     </Fragment>
//   );
// };

export default function IconPicker({
	icons, // Object data 
	url, // String url fetch
	// id, 
	show = false, // Can use with Dropdown / Modal
	label = "Icons", 
	w = 290, // 277 | 329 | 323 
	// maxH = 364, // 248, // 268 | 248 | 265 | 250
	h  = 340, // 380 | 364,
	autoFocus = false, 
	preview = {
		name: "home", 
		code: "e933"
	},
	cache = true, // DEV OPTION: Caching used
	onClick = Q.noop, // () => {}
	onHover = Q.noop // () => {},
}){
	// const iconBoxRef = useRef(null);
	const findRef = useRef(null);
	
	const [load, setLoad] = useState(false);
	const [data, setData] = useState(icons);// false
	const [meta, setMeta] = useState(null);
	const [dd, setDd] = useState(show);// false
  const [view, setView] = useState(preview);
	const [findVal, setFindVal] = useState("");
	const [findArr, setFindArr] = useState([]); // null
	// const [icoCache, setIcoCache] = useState(null);// DEV OPTION: Caching used

	useEffect(() => {
		// console.log('useEffect in IconPicker');// ,'color:yellow;'
		
		if(show){ // cacheIcon || 
			onGetSetData();// null, cacheIcon
		}else{
			setLoad(true);
		}
	}, [show, onGetSetData]);// cache, icons, show

	const fetchIcon = () => { // cb
		// OPTION: Check url props
		axios.get(url).then(r => {
			// console.log('r: ', r);
			if(r.status === 200){
				let data = r.data;

				const metas = {
					name: data.metadata.name, 
					prefix: data.preferences.fontPref.prefix
				};

				const dataIcon = data.icons.map(v => ({
					name: v.properties.name,
					code: num2Hex(v.properties.code)
				}));
				// const dataIcon = parseObj(data);

				setLoad(true);
				setMeta(metas);
				setData(dataIcon);

				// cb(metas);

				if(cache){
					localStorage.setItem("icons", JSON.stringify({ meta: metas, data: dataIcon }));
				}
			}
		}).catch(e => console.log(e));
	}

	// const parseObj = (d) => {
	// 	const arr = d.icons.map(v => ({
	// 		name: v.properties.name,
	// 		code: num2Hex(v.properties.code)
	// 	})).filter((_, i) => i < 42);

	// 	// console.log('arr: ', arr);

	// 	return arr;
	// }

	const onGetSetData = () => {// iconCache
		if(cache){
			const cacheIcon = localStorage.getItem("icons");// qicoUsed
			if(cacheIcon){
				let parseData = JSON.parse(cacheIcon);
				setLoad(true);
				setMeta(parseData.meta);

				// let dataIcon = parseData.data.filter((_, i) => i < 42);
				// console.log('parseData.data: ', parseData.data);
				// console.log('dataIcon: ', dataIcon);

				setData(parseData.data);
			}else{
				fetchIcon();
			}
		}else{
			fetchIcon();
			// fetchIcon(r => {
			// 	if(cb) cb(r);
			// });
		}
	}

	const onHoverIcon = (v, e) => {
		if(preview && view !== v){
			setView(v);// ico
		}
		onHover(v, e);// ico, e
	}

  const onClickIcon = (v, e) => {
		// setDd(false);

		onClick(v, e);// OPTION: i, e
		
		/* if(isFunc(onClick)){
			console.log(v);
			// const ico = {name: v.name, code: v.code};// {name: v[0], code: v[1]}
			onClick(v, i, e);// OPTION: i, e
		} */
		
		// DEV OPTION: Caching used
		// const ico = {name: v.name, code: v.code};
		/* if(cache){
			// Frequently Used | Current Used
			// const qicoUsed = localStorage.getItem('qicoUsed');
			// console.log(icoCache);
			if(icoCache !== null){
				const obj = [ico, ...icoCache];
				setIcoCache(obj);
				localStorage.setItem('qicoUsed', JSON.stringify(obj));// emoji-mart.frequently
			}else{
				setIcoCache([ico]);
				localStorage.setItem('qicoUsed', JSON.stringify([ico]));
			}
		} */
  }

	const onFind = (e) => {
		// const val = e.target ? ev.target.value : e;
		// const iconBox = iconBoxRef.current;
		
		// setFindVal(val);

		// setTimeout(() => {
		// 	let btnIcons = Q.domQall("button.icon-item", iconBox);
		// 	let NO = "d-none";
		// 	// 
		// 	btnIcons.forEach(el => {
		// 		if(el.dataset.name.includes(val)){// el.title.includes(val) | el.className.includes(val)
		// 			if(Q.hasClass(el, NO)) Q.setClass(el, NO, "remove");
		// 		}else{
		// 			Q.setClass(el, NO);
		// 		}
		// 	});
			
		// 	const getIcon = data.filter(v => v.name.includes(val)).length;
		// 	iconBox.classList.toggle("icoNotFound", getIcon < 1);
		// },900);

		let val = e.target.value;

		setFindVal(val);

		setTimeout(() => {
			const finds = data.filter(v => v.name.includes(val.toLowerCase()));
			// console.log('finds: ', finds);
			setFindArr(finds);
		}, 900);
	}

	const onToggle = (isOpen) => { // , e, meta
		if(!data && isOpen){
			onGetSetData();
		}

		if(dd && preview && view.name !== preview.name){
			// console.log('if 1: ',dd);
			setView(preview);
		}
		// dd && findVal !== ''
		if(dd && findVal !== ""){
			// console.log('if 2: ',dd);
			setFindVal("");
			// onFind("");
			setFindArr([]);
		}
		// Set focus 
		if(!dd && autoFocus && findRef.current){
			setTimeout(() => {
				findRef.current.focus();
			},1);
		}

		setDd(isOpen);
	}

  const itemsRenderer = (items, ref) => {
		const notFound = findVal.length > 0 && findLength() === 0;
		return (
			<Flex wrap content="start" 
				inRef={ref} // iconBoxRef 
				className={Q.Cx("position-absolute position-full p-1 border bw-y1 ovyauto icons-box", { "icoNotFound": notFound })} 
				data-notfound="NOT FOUND" 
				// style={{
				// 	width: w,
				// 	maxHeight: maxH
				// }} 
			>
				{notFound ? [] : items}
			</Flex>	
		);
	}

  const renderItem = (index, key) => {
		const v = findLength() > 0 ? findArr[index] : data[index];
		// const v = data[index];
		return (
			<Dropdown.Item key={key} as="button" type="button" 
				onMouseEnter={e => onHoverIcon(v, e)} 
				onClick={e => onClickIcon({ icon: { ...v, className: pref() + meta.prefix + v.name }, key }, e)} 
				// my-1
				className={"icon-item p-2 flex0 m-1 q-fw " + pref() + meta.prefix + v.name} 
				data-name={v.name} 
				// JSON.stringify({name: v[0], code: v[1]}, null, 2)
				title={"name: " + v.name + "\ncode: " + v.code} 
			/>
		);
	}

	const onCopy = (e, t) => {
		let et = e.target;
    clipboardCopy(t).then(() => {
      Q.setAttr(et, {"aria-label": "Copied!"});
      setTimeout(() => Q.setAttr(et, "aria-label"), 999);
    }).catch(e => console.log(e));
	}

	const pref = () => meta.prefix.replace("-"," ");

	const findLength = () => findArr.length;

//  group isOpen={dd} toggle={onToggle} | 
	return (
		<Dropdown bsPrefix="btn-group" 
			show={dd} 
			onToggle={onToggle} 
		>
			<Dropdown.Toggle>{label}</Dropdown.Toggle>{/*  id={id ? id : Q.Qid()} */}
			<Dropdown.Menu 
				// renderOnMount 
				className={Q.Cx("p-0 ovhide icon-picker", { "w-200px": !data })}
			>
				{(data && load) ? 
					<Flex dir="column" align="stretch" 
						style={{
							width: w,
							// maxHeight: maxH
							height: h
						}} 
					>
						<label className="d-flex m-0 p-1 flexno bg-strip">
							<input ref={findRef}
								autoFocus={autoFocus} 
								onChange={onFind} 
								value={findVal} 
								className="form-control form-control-sm" 
								type="search" // text
								placeholder="Search"
							/>

							{(Q_appData.UA.browser.name === "Firefox" && findVal.length > 0) && 
								<Btn As="b" size="sm" kind="flat" 
									className="qi qi-close xx"  
									onClick={() => {
										// if(findVal.length > 0) onFind("");// setFindVal | onFind
										// if(findVal.length > 0){
											setFindVal("");
											setFindArr([]);
										// }
									}}
								/>
							}

							<Btn As="b" size="sm" kind="flat" 
								className="qi qi-search" 
								// className={"q-fw qi qi-" + (findVal.length > 0 ? "close xx":"search")} 
								// onClick={() => {
								// 	// if(findVal.length > 0) onFind("");// setFindVal | onFind
								// 	if(findVal.length > 0){
								// 		setFindVal("");
								// 		setFindArr([]);
								// 	}
								// }}
							/>
						</label>

						<Flex dir="column" align="stretch" className="flex1 h-100 position-relative wrap-icons">
							<List 
								awaitMore={findLength() > 0 ? findLength() > 0 : data.length > 0} 
								pageSize={70} 
								itemCount={findLength() > 0 ? findLength() : data.length} //  
								itemsRenderer={itemsRenderer} 
								renderItem={renderItem} 
								// onIntersection={(size, pageSize) => {
								// 	console.log('onIntersection size: ', size);
								// 	console.log('onIntersection pageSize: ', pageSize);
								// }}
							/>
						</Flex>


						{preview && // JSON.stringify(view, null, 2) //  style={{ width: w }}
							<Flex wrap align="stretch" className="flexno icon-view">
								<div className="btn-group btn-group-xs w-100 ip-btn-copy">
									<Btn onClick={e => onCopy(e, meta.prefix + view.name)} blur kind="light" 
										className="border-top-0 border-left-0 rounded-0 tip tipTL qi qi-copy">Copy name</Btn>
									<Btn onClick={e => onCopy(e, view.code)} blur kind="light" 
										className="border-top-0 rounded-0 tip tipTL qi qi-copy">Copy code</Btn>
									
									{/* OPTION: Check if user usage library */}
									{/* {APP_DOMAIN.includes(window.location.origin) ? 
										<Aroute to="/tools/icon-maker" btn="light" className="rounded-0 border-top-0 border-right-0">
											Create icons
										</Aroute>
										: 
										<Btn href={APP_DOMAIN[0] + "/tools/icon-maker"} kind="light" className="rounded-0 border-top-0 border-right-0" target="_blank">Create icons</Btn>
									} */}

									{/* <Btn href="/tools/icon-maker" kind="light" className="rounded-0 border-top-0 border-right-0">Create icons</Btn> */}
								</div>

								<Flex justify="center" align="center" 
									className={"flexno w-25 p-2 q-s22 " + pref() + meta.prefix + view.name} 
								/>
								<Flex As="pre" align="center" 
									className="mb-0 p-2 small flex1 bg-light border-left q-scroll"
								>{"name: " + meta.prefix + view.name + "\ncode: " + view.code}</Flex>
							</Flex>
						}
					</Flex>
					:
					<Flex dir="column" align="center" 
						className={"text-center p-5 " + (load ? "i-load cwait" : "qi qi-ban ired fa-2x")} 
						style={load ? { '--bg-i-load': '95px' } : undefined} 
					>
						{load ? "LOADING" : "ICONS NOT LOAD"}
					</Flex>
				}
			</Dropdown.Menu>
		</Dropdown>
	);
}

// export default React.memo(IconPicker, (prevProps, nextProps) => prevProps.icons === nextProps.icons);

