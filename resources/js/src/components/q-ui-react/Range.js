import React, { useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

// import Btn from '../../components/q-ui-react/Btn';

export default function Range({
	value, 
	defaultValue, 
	className, 
	grab = true, 
	onChange = Q.noop, 
	...etc
}){
	// let v = value || defaultValue || 0;
	const [val, setVal] = useState(value || defaultValue || 0);
	// console.log('v: ', v);
	
	// useEffect(() => {
  //   console.log('v: ', v);
	// 	if(v){ // text.length > 0 && 
  //     setVal(v);
  //   }

  //   // console.log('txt: ', txt);
  //   // console.log('value: ', value);
  //   // console.log('defaultValue: ', defaultValue);

	// }, [v]); // value, defaultValue

	const Change = e => {
		setVal(e.target.value); 

		// if(onChange) onChange(e);
		onChange(e);
	}

	return (
		<input 
			{...etc} 
			type="range" 
			className={Q.Cx("q-range", {"c-grab": grab}, className)} // rounded-pill 
			// min={0} 
			// max={100} 
      value={value} 
      defaultValue={defaultValue} 
			style={{ '--slider-val': val + '%' }}
			// Number(e.target.value) 
			onChange={Change} 
		/>
	);
}

/*
<React.Fragment></React.Fragment>
*/
