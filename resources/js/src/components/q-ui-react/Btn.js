import React, { forwardRef } from 'react';
// import P from 'prop-types';
// import { mapToCssModules } from './utils';// tagPropType
// import { Cx } from '../../utils/Q';

// tipKind, 
// export default function Btn({
const Btn = forwardRef(
	({
		As, 
		active, 
		block, 
		className, 
		kind, 
		outline, 
		size,
		// inRef, 
		// cssModule, 
		close, 
		// 'aria-label':ariaLabel, 
		type, 
		// tip, qtip, 
		disabled, 
		role, 
		blur, // OPTION FOR blur after click
		// load, 
		// loadType = "border", // border | grow
		// loadSize, 
		// loadKind, 
		// loadClass, 
		onClick, 
		onKeyUp, 
		...etc
	}, 
	ref
) => {
	const isDisabled = e => {
		if(disabled || (As === 'a' && etc.href === '#')){
			e.preventDefault();
			return;
		}
	}
	
	const Click = e => {
		/* if(disabled || (As === 'a' && etc.href === '#')){
			e.preventDefault();
			return;
		} */
		isDisabled(e);
		
		if(onClick) onClick(e);
		// OPTION FOR blur after click
		if(blur){
			let et = e.target;
			setTimeout(() => et.blur(), 150);// OPTION Bootstrap active transition
		}
	}
	
	const KeyUp = e => {
		isDisabled(e);
		
		// let et = e.target;
		// console.log(e.key);
		if((As !== "button" || As !== "a") && etc.tabIndex === "0" && e.key === "Enter"){
			e.target.click();
		}
		
		if(onKeyUp) onKeyUp(e);
	}

	/* const setCls = mapToCssModules(Cls(
		{ close },
		close || 'btn',
		close || btnOutlineKind,
		size ? `btn-${size}` : false,
		block ? 'btn-block' : false,
		{ active, disabled: disabled },
		//ico ? `i q-${ico}` : false,
		qtip ? `tip tip${qtip}` : false,
		qtip && tipKind ? `tip-${tipKind}` : false,
		className
	), cssModule); */

	// const closeAriaLabel = close ? 'Close' : null;
	// const noChild = !etc.children || typeof etc.children === 'undefined' ? tip : null;
	const El = As === 'button' && etc.href ? As = 'a' : As;// NOT FIX...???

	return (
		<El 
			{...etc} 
			ref={ref} // inRef 
			// `btn btn${outline ? '-outline' : ''}-${kind}`,
			className={
				Q.Cx( // Q.mapToCssModules(Q.Cx(
					{ close },
					close || 'btn',
					close || `btn${outline ? '-outline' : ''}-${kind}`,
					size ? `btn-${size}` : false,
					block ? 'btn-block' : false,
					{ active, disabled: disabled }, 
					// qtip ? `tip tip${qtip}` : false,
					// qtip && tipKind ? `tip-${tipKind}` : false,
					// load ? "btnLoad" : false, 
					className
				) //, cssModule)
			} 
			type={As !== 'button' && !type ? undefined : type ? type : 'button'} 
			
			// aria-label={ariaLabel || closeAriaLabel || noChild || (qtip && tip)} 
			// (As === 'a' || As === 'label' || As === 'span' || As === 'div') 
			role={As !== 'button' && !role ? 'button' : role} 
			// title={qtip ? null : closeAriaLabel ? closeAriaLabel : tip} 
			// tabIndex={As !== 'button' && disabled ? "-1" : null} 
			tabIndex={As !== 'button' && disabled && !etc.tabIndex ? "-1" : etc.tabIndex} 
			disabled={As === 'button' && disabled} // (As === 'button' && disabled) || load
			aria-disabled={disabled} 
			onClick={Click} 
			onKeyUp={KeyUp} 
		/>
	);
});

Btn.defaultProps = {
	As: 'button',
  kind: 'primary'
};

export default Btn;	

/* load && 
	<div className={
			Q.Cx("spinner-" + loadType, {
				[`spinner-${loadType}-${loadSize}`] : loadSize,
				["text-" + loadKind] : loadKind
			}, loadClass)
		} 
	/>
*/

/* etc.children */
// </El>

/* Btn.propTypes = {
  active: P.bool,
  'aria-label': P.string,
  block: P.bool,
  kind: P.string,
  disabled: P.bool,
  outline: P.bool,
  // as: tagPropType,
  inRef: P.oneOfType([P.object, P.func, P.string]),
  onClick: P.func,
  size: P.string,
  children: P.node,
  className: P.string,
  cssModule: P.object,
  close: P.bool
}; */


