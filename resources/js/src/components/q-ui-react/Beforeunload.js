import { useRef, useEffect } from 'react';// React, 
import { useHistory } from 'react-router-dom';

// import Btn from '../../components/q-ui-bootstrap/Btn';

// window.confirm("Do you really want to leave?")

// Are you sure you want to quit without saving your changes?
const usePrompt = (when, msg = "") => {
  const history = useHistory();
  const me = useRef(null);

  const onWindowOrTabClose = e => {
    if (!when) {
      return;
    }
    if (typeof e == 'undefined') {
      e = window.event;
    }
    if (e) {
      e.returnValue = msg;
    }
    return msg;
  };

  useEffect(() => {
    if (when) {
      me.current = history.block(msg);
    } else {
      me.current = null;
    }

    window.addEventListener('beforeunload', onWindowOrTabClose);

    return () => {
      if (me.current) {
        me.current();
        me.current = null;
      }

      window.removeEventListener('beforeunload', onWindowOrTabClose);
    }
  }, [msg, when]);
};

function Beforeunload({
	when, 
	msg = "", 
	children = null
}){
	usePrompt(when, msg);

	return children;//  || null
}

export { usePrompt, Beforeunload };

// export default function Beforeunload({
//   // handlerRef, 
// 	// condition, 
// 	when, 
// 	message = '', // Are you sure you want to quit without saving your changes?
//   // onBeforeunload, 
//   children = null
// }){
//   const history = useHistory();
//   const me = useRef(null);

//   const onWindowOrTabClose = e => {
//     if (!when) {
//       return;
//     }
//     if (typeof e == 'undefined') {
//       e = window.event;
//     }
//     if (e) {
//       e.returnValue = message;
//     }
//     return message;
//   };

//   useEffect(() => {
//     // const fnBeforeunload = (e) => {
//     //   // let returnValue;
//     //   // if (handlerRef.current != null) {
//     //   //   returnValue = handlerRef.current(e);
//     //   // }
//     //   // // Chrome requires `returnValue` to be set.
//     //   // if (e.defaultPrevented) {
//     //   //   e.returnValue = '';
//     //   // }

//     //   // if (typeof returnValue === 'string') {
//     //   //   e.returnValue = returnValue;
//     //   //   return returnValue;
//     //   // }

//     //   // Cancel the event as stated by the standard.
//     //   e.preventDefault();
//     //   // Older browsers supported custom message
//     //   e.returnValue = '';
//     // };

//     // if(typeof condition === "boolean"){
//     //   if(condition) window.addEventListener('beforeunload', onWindowOrTabClose);// fnBeforeunload
//     //   else window.removeEventListener('beforeunload', onWindowOrTabClose);
//     // }
//     // else{
//     //   window.addEventListener('beforeunload', onWindowOrTabClose);
//     // }

//     // window.addEventListener('beforeunload', onWindowOrTabClose);

//     // return () => {
//     //   window.removeEventListener('beforeunload', onWindowOrTabClose);
// 		// };
		

//     if (when) {
//       me.current = history.block(message);
//     } else {
//       me.current = null;
//     }

//     window.addEventListener('beforeunload', onWindowOrTabClose);

//     return () => {
//       if (me.current) {
//         me.current();
//         me.current = null;
//       }

//       window.removeEventListener('beforeunload', onWindowOrTabClose);
// 		}
// 		// condition
//   }, [message, when]);// eslint-disable-line react-hooks/exhaustive-deps

//   return children;
// }

/*

*/
