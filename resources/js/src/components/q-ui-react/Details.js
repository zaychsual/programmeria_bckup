import React, { useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

// import Btn from '../../components/q-ui-react/Btn';

export const Summary = ({
	As = "summary", 
	// onKeyDown = Q.noop, 
	inRef, 
	...etc
}) => {
	// const KeyDown = e => {
		// let parent = e.target.parentElement;
		// let next = parent.nextElementSibling;
		
		// console.log('key: ', e.key);
		// console.log('next: ', next);
		// console.log('parent: ', parent);
		// console.log('next.role: ', Q.hasAttr(next, "role"));
		
		// let isRole = Q.hasAttr(next, "role");
		
		// if(e.key === "ArrowDown" && ((parent?.tagName === "DETAILS" && next?.tagName === "DETAILS") || (next && isRole) )){
			// console.log("focus: ", next.firstElementChild);
			
			// if(isRole){
				// next.focus();
			// }else{
				// next?.firstElementChild?.focus();// click
			// }
		// }
	// }
	
	return (
		<As tabIndex="0" {...etc} 
			// onKeyDown={KeyDown} 
		/>
	)
};

export default function Details({
	inRef, 
	className, 
	summary, 
	summaryClass, 
	summaryProps, 
	groupClass, // contentClass
	children, 
	open = false, 
	renderOpen, 
	onToggle = Q.noop, 
	// onClickSummary = Q.noop, 
	...etc
}){
  const [one, setOne] = useState(renderOpen);// false
	const [isOpen, seIsOpen] = useState(open);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Details','color:yellow;');
	// }, []);

	const Toggle = e => {
		let et = e.target;
		// console.log("onToggle e: ", e.target.open);
		if(!one) setOne(true);

		// setTimeout(() => {
		// 	et.open ? Q.domQ(".detail-content", et)
		// }, 1);
		
		// Q.setAttr(et, {"aria-expanded": et.open});
		seIsOpen(et.open);

		onToggle(et.open, e);
	}

  /* const clickSummary = e => {
		// console.log('clickSummary e: ', e);
		onClickSummary(e);
  } */

	return (
		<details 
			{...etc} 
			ref={inRef} 
			open={open} // open | isOpen
			// aria-expanded={isOpen} // OPTION: for accessibility
			className={Q.Cx("detail-q", className)} 
			open={Boolean(one) ? one : open} 
			onToggle={Toggle}
		>
			{summary && 
				<Summary className={summaryClass} {...summaryProps}
					aria-expanded={isOpen} // OPTION: for accessibility
				>
					{summary}
				</Summary>
			}

			{one && // , { "d-none": !open }
				(summary ? // children | Q.Cx("detail-content", groupClass)
					(children && <div role="group" aria-hidden={!isOpen} className={groupClass}>{children}</div>)
					: 
					children
				)
			}
		</details>
	);
}

/*
<React.Fragment></React.Fragment>
*/
