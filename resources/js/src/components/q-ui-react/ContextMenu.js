import React, { Children, useState, useEffect, cloneElement, Fragment } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import ReactDOM from 'react-dom';
import usePopper from 'react-overlays/usePopper';// react-popper
// DEVS OPTION: for hide component when scroll
// import { useScroll } from 'ahooks';
// import debounce from 'lodash.debounce';

// import Btn from '../../components/q-ui-react/Btn';

const makeVirtualEl = (x = 0, y = 0) => ({
  getBoundingClientRect: () => ({
    width: 0,
    height: 0,
    top: y,
    right: x,
    bottom: y,
    left: x
  })
});

export default function ContextMenu({ 
	dom, // target ref with DOM
	
	children, 
	component, 
	popperConfig, // popperOptions
	className, 
	esc = true, 
	hideOnScroll = true, 
	appendTo = document.body, 
	onContextMenu = Q.noop, // Q-CUSTOM
	onEsc = Q.noop, // Q-CUSTOM
	onScrolls = Q.noop, // Q-CUSTOM
}){
	// const scroll = useScroll(document, () => console.log('scroll: ', val));
	
	// const setPopperConfig = (targetFlip) => {
		// if(targetFlip){
			// return {
				// ...popperConfig, 
				// modifiers: [
					// {
						// name: 'flip',
						// options: {
							// boundary: targetFlip,
						// },
					// },
				// ],
			// }
		// }
		// return popperConfig;
	// };
	
	// const [popper, setPopper] = useState(setPopperConfig());
	
  const child = Children.only(children);
  const [active, setActive] = useState(false);
  const [clonedEl, setClonedEl] = useState(null);
  const [virtualEl, setVirtualEl] = useState(makeVirtualEl());
  const { styles, attributes } = usePopper(virtualEl, clonedEl, popperConfig);// popper

  const hide = () => {
		setActive(false);
		onContextMenu(false);// Q-CUSTOM: For ???
	}

  const onCtxMenu = (e) => {
    if(e) e.preventDefault();
    const { clientX, clientY, target } = e;
		
		let rectEl;
		if(dom){
			rectEl = target;
		}else{
			rectEl = makeVirtualEl(clientX, clientY);
		}
		// console.log('rectEl: ', rectEl);
		
		// setPopper(setPopperConfig(target));
    setVirtualEl(rectEl);// makeVirtualEl(clientX, clientY)
    setActive(true);
		
		onContextMenu(active, e);// Q-CUSTOM
  };

  useEffect(() => {
    const onOutsideClick = (e) => {
      if(clonedEl && !clonedEl.contains(e.target)){
        hide();
				onContextMenu(active, e);// Q-CUSTOM
      }
    };
		
		// Q-CUSTOM DEV OPTION: for hide component when scroll 
		// const onScrollDoc = debounce(() => {
			// if(active){
				// hide();
			// }
		// }, 50);
		const onScrollDoc = () => {
			if(active){
				hide();
				onScrolls();
			}
		}
		const onEscKey = (e) => {
			// console.log(e);
			if(active && e.key === "Escape"){
				hide();
				onEsc(false);
			}
		}

    document.addEventListener('mousedown', onOutsideClick);
		// Q-CUSTOM DEV OPTION: for hide component when esc key
		if(esc && active) document.addEventListener('keydown', onEscKey);
		// Q-CUSTOM DEV OPTION: for hide component when scroll 
		if(hideOnScroll && active) document.addEventListener('scroll', onScrollDoc);
		
    return () => {
			document.removeEventListener('mousedown', onOutsideClick);
			// Q-CUSTOM DEV OPTION: for hide component when esc key
			if(esc) document.removeEventListener('keydown', onEscKey);
			// Q-CUSTOM DEV OPTION: for hide component when scroll
			if(hideOnScroll) document.removeEventListener('scroll', onScrollDoc);
		};
  }, [clonedEl, esc, hideOnScroll, active]);

  return (
    <Fragment>
      {
				cloneElement(child, {
					...child.props,
					onContextMenu: onCtxMenu
				})
			}

      {active && 
        ReactDOM.createPortal(
          <div 
            ref={setClonedEl} 
            style={{ ...styles.popper }} 
            {...attributes.popper} 

            className={className} 
          >
            {component && component(hide, onCtxMenu, active)}
          </div>,
          appendTo, // document.body
        )
			}
    </Fragment>
  );
};
