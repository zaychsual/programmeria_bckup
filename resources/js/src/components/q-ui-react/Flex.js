import React from 'react';
// import P from 'prop-types';// {func, string, object, oneOf, oneOfType} | PropTypes
// import {Cls} from '../../utils';// 'classnames'
// import {mapToCssModules} from './utils';
// import { Cx } from '../../utils/Q';

// cssModule
export default function Flex({
	As, 
	inline, 
	dir, 
	wrap, 
	nowrap, 
	justify, 
	align, 
	self, 
	fill, 
	grow, 
	shrink, 
	content, 
	className, 
	inRef, 
	...etc
}){
	return (
		<As 
			{...etc} 
			ref={inRef} 
			className={
				Q.Cx("d-" + (inline ? "inline-" : "") + "flex", { // `d-${inline ? 'inline-' : ''}flex`
					// "d-flex": !inline,
					// "d-inline-flex" : inline,
					
					"flex-wrap" :  wrap && !nowrap,
					"flex-nowrap" :  nowrap && !wrap,
					["flex-" + dir] : dir,
					["justify-content-" + justify] : justify,
					["align-items-" + align] : align, 
					
					["align-content-" + content] : content, // OPTION 
					
					// OPTION:
					["align-self-" + self] : self,
					"flex-fill" : fill,
					["flex-grow-" + grow] : grow,
					["flex-shrink-" + shrink] : shrink
				}, className)
			} 
		/>
	);
}

Flex.defaultProps = {
	As: 'div',
	inline: false
};

/* Flex.propTypes = {
	// as: P.oneOfType([P.func, P.string]),
	// type: P.string,
	dir: P.oneOf(['row', 'row-reverse', 'column', 'column-reverse']),
	justify: P.oneOf(['start', 'end', 'center', 'between', 'around', 'even', 'stretch']),
	align: P.oneOf(['start', 'end', 'center', 'baseline', 'stretch']),
	className: P.string,
	cssModule: P.object
}; */
