import React from 'react';
// import {findDOMNode} from "react-dom";
// import { Cx, toggleClass } from '../../utils/Q';

// wrapClass, upload, id, encType
export default function Form({ 
	inRef, 
	className, 
	fieldsetClass, 
	noValidate, 
	valid, // For formik: formik touched object 
	disabled, 
	onSubmit, 
	children, 
	...etc 
}){ 
	// React.useEffect(() => {
		// console.log('%cuseEffect in Form','color:yellow;');
		// console.log(getEl());
		// console.log(findDOMNode(Form));
	// }, []);
	
	/*const getEl = () => { // getElement
		try{
			// using findDOMNode for two reasons:
			// 1. cloning to insert a ref is unwieldy and not performant.
			// 2. ensure that we resolve to an actual DOM node (instead of any JSX ref instance).
			return findDOMNode(this);
		}catch{
			return null;// swallow error if findDOMNode is run on unmounted component.
		}
	}*/
	
	const Submit = e => {
		if(disabled) return;
		
		let et = e.target;
		
		// Prevent submit form if use xhr
		if(noValidate){
			e.preventDefault();
			e.stopPropagation();
			
			// console.log(!Q.isBool(valid));
			// if(!Q.isBool(valid)) 
			if(!Q.isBool(valid)){ 
				et.classList.toggle("was-validated", !et.checkValidity());
			}
		}
		
		if(onSubmit && !disabled) onSubmit(e, et.checkValidity());// onSubmit to props
	}
	
	return (
		<form 
			{...etc} 
			ref={inRef} 
			className={
				Q.Cx('form-q', {
					"disabled": disabled, 
					// Q.isBool(valid) //  && !isValid
					"was-validated": noValidate && valid && Object.keys(valid).length > 0 
				}, className)
			} 
			// enctype values: (OPTION)
			// 1. application/x-www-form-urlencoded (Default)
			// 2. multipart/form-data (form upload)
			// 3. text/plain (Spaces are converted to "+" symbols, but no special characters are encoded)
			// encType={upload ? 'multipart/form-data' : encType} 
			//data-reset={reset} 
			noValidate={noValidate} // noValidate ? true : undefined
			onSubmit={Submit} 
		>
			<fieldset 
				disabled={disabled} 
				className={fieldsetClass}
			>
				{children}
			</fieldset>
		</form>
	)
}


