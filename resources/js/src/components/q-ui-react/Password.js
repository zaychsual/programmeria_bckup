import React, { useState } from 'react';// { useState, useEffect }

import InputGroup from './InputGroup';
import InputQ from './InputQ';
import Btn from './Btn';

export default function Password({
	className, 
	label, 
	// children, 
	...etc
}){
  const [see, setSee] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Password','color:yellow;');
	// }, []);

	return (
		<label 
			className={Q.Cx("q-input", className)} 
			aria-label={label}
		>
			<InputGroup
				As="div" 
				append={
					<Btn As="div" kind="light" tabIndex="0" 
						className={"tip tipTR qi q-fw qi-eye" + (see ? "-slash":"")} 
						aria-label={(see ? "Hide":"Show") + " Password"}
						onClick={() => setSee(!see)} 
					/>
				}
			>
				<InputQ // wrap 
					{...etc} 
					label={label} // "Password" 
					type={see ? "text":"password"} 
					// name="password" 
					// required 
					// minLength={5} 
					// pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

					// OPTION: From Gmail, toggle or default render
					// spellCheck={see ? false : undefined} 
					// autoComplete={see ? "off" : undefined} 
					// autoCapitalize={see ? "off" : undefined} 
			
					spellCheck={false} 
					autoComplete="off" 
					autoCapitalize="off" 

					// value={formik.values.password} 
					// onChange={formik.handleChange}
				/>

				{/* {children}  */}
			</InputGroup>
		</label>
	);
}

/*
<React.Fragment></React.Fragment>
*/
