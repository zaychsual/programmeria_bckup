import React from 'react';
import ReactDOM from 'react-dom';
// import Portal from '../q-react-bootstrap/Portal';

// import {Boundary} from "../dev/blueprintjs/common/boundary";
import {Breadcrumbs} from "../dev/blueprintjs/components/breadcrumbs/breadcrumbs";
import Btn from '../q-react-bootstrap/Btn';
import UctrlCollapse from '../q-react-bootstrap/UctrlCollapse';
import Loops from '../q-ui-react/Loops';

export default class Doc extends React.Component{
  constructor(props){
    super(props);
    // this.state = {
			// docPosition: 'end',
			// docItems: []
    // };
		this.box = null;
  }

  componentDidMount(){
		console.log('%ccomponentDidMount in Doc','color:yellow');
		this.init();
  }
	
	componentWillUnmount(){
    if(this.box){
      document.body.removeChild(this.box);
      this.box = null;
    }
	}
	
	init(){
    if(!this.box){
      this.box = document.createElement('div');
      this.box.setAttribute('tabindex', '-1');
			this.box.classList.add('doc-box');
			document.body.appendChild(this.box);
		}
	}
	
	/* onAddItem = () => {
		this.setState(s => ({
			docItems: [...s.docItems, {
					// icon: "folder-close", 
					text: "New item " + Number(s.docItems.length + 1)
				}
			]
		}));
	} */

  render(){
		const {position, items, onMin = () => {}, onMax = () => {}, onClose = () => {}} = this.props;

		if(this.box){
			return ReactDOM.createPortal(
				<Breadcrumbs 
					// As="div" 
					popoverProps={{
						boundary: "viewport",// 'viewport', 'window', 'scrollParent' | DEFAULT = scrollParent
						popoverClassName: "doc-pop",
						className: "w-auto" //  border-0 shadow-none
					}}
					collapseFrom={position} 
					items={items} 
					// breadcrumbRenderer={() => <Btn size="sm">X</Btn>}
					// currentBreadcrumbRenderer={renderCurrentAsInput ? this.renderBreadcrumbInput : undefined} 
					// currentBreadcrumbRenderer={() => <Btn size="sm">X</Btn>} 
					className="justify-content-end doc-app" 
					// onClickItem={(i, item, e) => {
						// console.log('onClickItem i: ',i);
						// console.log('onClickItem item: ',item);						
					// }}
					headChild={(id, item) => 
						<div className="btn-group btn-group-xs ml-2">
							<Loops 
								data={[
									{type:'Min', f: onMin}, 
									{type:'Max', f: onMax},
									{type:'Close', f: onClose}
								]}
							>
								{(v, idx) => 
									<Btn id={"btn" + v.type + item.id} // id
										onClick={(e) => v.f(id, item, v.type, e)} 
										kind="light" 
										className={"q q-" + v.type.toLowerCase()} 
										tip={v.type} 
									/>
								}
							</Loops>
						</div>
					} 
					itemChild={(id, item) => 
						<UctrlCollapse timeout={150} toggler={"#btnMax"+item.id} className="doc-el">
							<div className="q-scroll">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit data ok nice.
awesome hehe haha wkwkwk. Hmmm what the hell ask questions info pro fhfh num intg arr obj. Title content desc write over fix.
questions info pro fhfh num intg arr obj questions info pro fhfh num intg arr obj sdsdsdsd</div>
						</UctrlCollapse>
					}
				/>, 
				this.box
			)
		}
		return null;
  }
}

Doc.defaultProps = {
	position: 'end',// start
	items: []
}

/*
<React.Fragment>
</React.Fragment>
*/
