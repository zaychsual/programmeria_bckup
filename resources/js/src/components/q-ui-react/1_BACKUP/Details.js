import React from 'react';
// import Cls from 'classnames';
// import {Cls, isStr} from '../../utils';
import {Cx, isStr} from '../../utils/Q.js';

export default function Details({open, role, className, label, labelClass, children, groupClass, toggle, disabled, labelProps, groupProps, ...etc}){ // ...sums
	const showHide = e => {
		// NOT RUN ???
		if(disabled){
			e.target.open = false;// RUN BUT NOT GOOD
			// open = false;// OPTION (NOT RUN)

			e.preventDefault();
			e.stopPropagation();
			return;
		}
		if(toggle) toggle(e.target.open, e);
	}

	return (
    <details
			{...etc}
			open={disabled ? false : open}
			className={Cx({"detail-q":role === 'treeitem' || role === 'tree'}, className)}
			role={role} // "treeitem"
			disabled={disabled}
			onToggle={showHide}
    >
      {label &&
				<summary
					className={Cx("nav-link q", labelClass)}
					title={isStr(label) ? label : labelProps.title} // title={isStr(label) ? label : null}
					{...labelProps}
				>
					{label}
				</summary>
			}

			{children && <div role="group" className={groupClass} {...groupProps}>{children}</div>}
		</details>
	);
}

Details.defaultProps = {
	role: "treeitem",
	open: false,
	// toggle: () => {}
};

// export function TreeItem({tip, path, ...etc}){
	// return (
		// <div
			// {...etc}
			// draggable
			// className="nav-link"
			// tabIndex="0"
			// title={tip}
			// role="treeitem"
			// data-path={path}
		// />
	// )
// }
