import React from 'react';
import { NavLink } from 'react-router-dom';

// import { Cx, hasClass } from '../../utils/Q'; // , hasAttr, isFunc

// ref, strict, 
export default function Aroute({
	// to, 
	kind, 
	nav, 
	btn, 
	size, 
	outline, 
	dropdown, 
	noLine, 
	stretched, 
	className, 
	disabled, 
	// tip, qtip, 
	role, 
	tabIndex, 
	children, 
	// closeDropdown = true, 
	onClick = () => {}, 
	onContextMenu = () => {},
	...etc
}){
  // const [data, setData] = React.useState();
	// const refA = React.createRef();
	// const getRef = ref || refA;

	const CtxMenu = e => {
		if(disabled){
			e.preventDefault();
			return;
		}
		// if(isFunc(onContextMenu)) onContextMenu(e);
		onContextMenu(e);
	}

/* 	React.useEffect(() => {
		console.log('%cuseEffect in Aroute','color:yellow;');
		console.log(getRef.current);

		if(getRef && disabled){
			getRef.current.addEventListener('auxclick', CtxMenu, false);
		}

		// return () => {
			// if(getRef && disabled){
				// getRef.current.removeEventListener('auxclick', CtxMenu, false);
			// }
		// }

	}, [getRef, disabled, CtxMenu]); */

	const Click = e => {
		let et = e.target;

    if(disabled){
      e.preventDefault();
      return;
    }
		// console.log(hasClass(et, "dropdown-item"));
    if(Q.hasClass(et, "dropdown-item")){
			// console.log('is dropdown-item');
			document.body.click();
		}
		
		// let isActive = hasClass(et, "active") || hasAttr(et, 'aria-current');
		// console.log(isActive);
		// console.log("active", hasClass(et, "active"));
		// console.log('aria-current', hasAttr(et, 'aria-current'));
		
    // if(hasClass(et, "active") || hasAttr(et, 'aria-current')){ // aria-expanded="true" | aria-current
		// 	e.preventDefault();
		// 	e.stopPropagation();
		// 	return;
		// }

		// if(isFunc(onClick)) onClick(e);// custom click to props
		onClick(e);
	}

	return (
		<NavLink
			{...etc}
			// strict={strict} 
			// ref={ref ? ref : refA} 
			// to={Q.newURL(to).href} 
			
			/* isActive={(match, location) => {
				// if(!match){
					// return false;
				// }
				// only consider an event active if its event id is an odd number
				// const eventID = parseInt(match.params.eventID);
				// return !isNaN(eventID) && eventID % 2 === 1;
				console.log(match);
				// isActive
			}}	 */		
			
			onClick={Click}
			onContextMenu={CtxMenu}

			className={
				Q.Cx(`${btn ? 'btn btn':''}${btn && outline ? '-outline' : ''}${btn ? '-'+btn : ''}`, {
					'nav-link' : nav,
					// [`btn btn-${btn}`] : btn,
					[`btn-${size}`] : btn && size,
					[`text-${kind}`] : kind, 
					'text-decoration-none': noLine, 
					'stretched-link': stretched, 
					'dropdown-item' : dropdown, 
					'disabled' : disabled
				}, className)
			}
			role={btn && !role ? 'button' : role} 
			// title={qtip ? null : tip} 
			// aria-label={(!children || typeof children === 'undefined' || qtip) && tip ? tip : null} 
			tabIndex={disabled && !tabIndex ? "-1" : tabIndex} // {disabled ? "-1" : null}
			aria-disabled={disabled ? true : null}
		>
			{children}
		</NavLink>
	);
}

// Aroute.defaultProps = {
	// btn: false
// };

/*

*/
