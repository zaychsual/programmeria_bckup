import React from 'react';
// import useResizeObserver from 'use-resize-observer';
import { useScroll } from 'ahooks';
// import debounce from 'lodash/debounce';// lodash-es/debounce

import Btn from './Btn';

export default function ScrollTo({
	// className, 
  // children, 
  target = document, 
  // wait = 500, 
  onClick = () => {}, 
  ...etc
}){
  // const onResize = React.useMemo(() => debounce(({ width, height }) => {
  //   // console.log('RowWrap onResize width: ', width);
  //   onResizeEnd({ width, height });
  // }, wait, { leading: true, trailing: false }), [wait, onResizeEnd]);

  // const onResize = React.useMemo(() => ({ width, height }) => {
  // 	console.log('RowWrap onResize width: ', width);
  //   onResizeEnd({ width, height });
  // }, [onResizeEnd]);

  // const ref = React.useRef(null);
  // const [pos, setPos] = React.useState(0);
  // const { ref } = useResizeObserver({ onResize });
  let scroll = useScroll(target);

  const Click = e => {
    // console.log('Click scroll: ', scroll);
    if(target && scroll.top > 160){
      window.scrollTo({
        top: 0, // rectP.y | rectP.y - rectD.height
        left: 0,
        behavior: "smooth"
      });

      // window.scrollTo(0, 0); // document.documentElement
    }

    onClick(scroll, e);
  }

  // React.useLayoutEffect(() => {// useEffect
  //   // if(target && scroll?.top > 0){
  //   //   target.scrollBy({
  //   //     top: 0, // rectP.y | rectP.y - rectD.height
  //   //     left: 0,
  //   //     behavior: "smooth"
  //   //   });
  //   // }
  //   if(scroll.top < 1){
  //     console.log('React.useEffect scroll: ', scroll);
  //     console.log('React.useEffect ref: ', ref);
  //     ref.current.hidden = true;
  //   }else{
  //     ref.current.hidden = false;
  //   }

  // }, [scroll]); // target, 

  // return <div ref={ref} className={className}>{children}</div>;

  // if(scroll.top < 1) return null;

  return (
    <React.Fragment>
      {scroll?.top > 160 && 
        <Btn 
          // inRef={ref} 
          {...etc} 
					size="sm" 
          // hidden={scroll.top < 1} 
          onClick={Click} 
        />
      }
    </React.Fragment>
  );
}

