import React, { useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useDropzone } from 'react-dropzone';

// import Btn from '../../components/q-ui-react/Btn';
import { traverseDirectory } from '../../utils/file/traverseDirectory';
// import { treePath } from '../../utils/dir/array-path-to-dir';

// function getExt(fname, bool){
//   return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
// }

function File({ 
	className, 
	children, 
	As = "div", 
}){
	return (
		<As role="treeitem" className={Q.Cx("tree-item", className)}>
			{children}
		</As>
	)
}

function Dir({ 
	open, 
	label, 
	children, 
}){
	return (
		<details 
			open={open} 
			role="tree"
			className="detail-q tree-q tree-folder w-100"
		>
			<File As="summary">{label}</File>

			{children && <div className="detail-content">{children}</div>}
		</details>
	)
}

export default function Drops(){
  const [data, setData] = useState(null);// []
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Drops','color:yellow;');
	// }, []);

	// const scanDir = (item) => {
	// 	// let elem = document.createElement("li");
	// 	// elem.innerHTML = item.name;
	// 	// container.appendChild(elem);
		
	// 	let tree = [];
	 
	//  	if(item.isDirectory){
	// 		let directoryReader = item.createReader();
	// 		// let directoryContainer = document.createElement("ul");
	// 		// container.appendChild(directoryContainer);
	// 		// tree.push();
			
	// 		directoryReader.readEntries((entries) => {
	// 			entries.forEach((entry) => {
	// 				scanDir(entry);
	// 				tree.push(entry);
	// 				// console.log('scanDir entry: ', entry);
	// 			});
	// 		});
	// 	}

	// 	return tree;
	// }

	const Drop = async e => {
		let items = e.dataTransfer.items; // .files
		
		console.log('items: ', items);

		let datas;

		for(let i=0; i<items.length; i++){
			let item = items[i].webkitGetAsEntry();
			if(item){
				// let sitem = scanDir(item);
				// datas.push(item);
				// console.log('Drop item: ', item);

				// traverseDirectory(item).then((entry)=> {
				// 	console.log('entry: ', entry);
				// 	datas = entry;
				// });
				let res = await traverseDirectory(item);
				datas = res;
			}
		}

		if(datas?.length){
			// let paths = treePath(datas[0].map(v => v.path)); // path | fullPath

			setData(datas[0]);// 

			console.log('Drop datas: ', datas[0]);
		}
	}

	const renderTree = (node, key) => {
		if(node == null) return;

		// const {packDetail} = this.state;
		let lng = node.length;
		let tree = [];
		// let pathToArr;
		let pathEnd;
		// let setLink;
		
		for(let i = 0; i < lng; i++){
			pathEnd = node[i].path.split("/").pop();
			// pathEnd = [...pathToArr].pop();

			if(node[i].type === "file"){ // node[i].isFile
				tree = [
					...tree, 
					<File key={i} className="tree-file">
						{node[i].name}
					</File>
				];
			}else{
				tree = [
					<Dir key={i} label={pathEnd}>
						{renderTree(node[i][key], "files")} 
					</Dir>,
					...tree
				];
			}
		}
		// console.log('tree: ', tree);
		return tree;
	}

	return (
		<div className="drops">
			<div className="jumbotron"
				onDrop={Drop} 
			>
				<p className="lead">Drag 'n' drop some files here, or click to select files</p>
			</div>

			<Dir open>
				{data && renderTree(data, "files")}
			</Dir>
		</div>
	);
}

/*
      <div {...getRootProps({className: "jumbotron dropzone"})}>
        <input {...getInputProps()} />
        <p className="lead">Drag 'n' drop some files here, or click to select files</p>
      </div>

      <aside>
			<h4>Files</h4>
			<ul>{files}</ul>
			</aside>
*/
