import React, { useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useDropzone } from 'react-dropzone';

import Btn from '../../components/q-ui-react/Btn';
import Details, { Summary } from '../../components/q-ui-react/Details';
import { traverseDirectory } from '../../utils/file/traverseDirectory';
import { isDir, SUPPORT_DIR } from '../../utils/file/fileRead';// , 
// import { MONACO_LANGS } from '../../data/monaco';

// const ACCEPT_EXT = [...MONACO_LANGS.map(v => "."+v.ex), '.sass','.jsx','.tsx'];
// console.log('ACCEPT_EXT: ', ACCEPT_EXT);

function getExt(fname, bool){
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

const EXT_FILES = ["local","example","lock"]; // File name like .env.local, yarn.lock, .env.example

// directory="" webkitdirectory=""
const directoryAttr = SUPPORT_DIR && { directory: "", webkitdirectory: "" };

export default function Drops(){
  const [data, setData] = useState(null);// []
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Drops','color:yellow;');
	// }, []);

	const inputID = "inputID-" + Q.Qid();

// Change & Drop
	const onChangeFile = async e => {
		let data = e.dataTransfer ? e.dataTransfer : e.target;  // e.dataTransfer.items | .files
		
		let dir = await isDir(data.items);
		// console.log('isDir: ', dir);
		let items = data.items;
		let datas = [];

		if(dir){
			for(let i=0; i<items.length; i++){
				let item = items[i].webkitGetAsEntry();
				if(item){
					let res = await traverseDirectory(item);
					datas = res[0];
				}
			}
		}
		else{
			items = data.files;
			for(let i=0; i<items.length; i++){
				let file = items[i];
				let fileItem = {
					file: file, 
					isDirectory: false, // OPTION
					isFile: true,
					type: "file",
					name: file.name, 
					path: "/" + file.name
				};
				// OPTION
				let fullPath = file.webkitRelativePath;
				if(fullPath) fileItem.fullPath = "/" + fullPath;

				// Object.defineProperty(fileItem, "file", {
				// 	value: fileItem
				// });
				// console.log('files file: ', file);
				// console.log('files fileItem: ', fileItem);
				datas.push(fileItem);
			}
		}

		if(datas?.length){
			setData(datas);
			console.log('Drop datas: ', datas);
		}
	}

	const renderTree = (node, key) => {
		if(node == null) return;

		let lng = node.length;
		let tree = [];
		let pathEnd, me, ext, parseExt;
		
		for(let i = 0; i < lng; i++){
			me = node[i];

			if(me.type === "file" || me.isFile){
				ext = getExt(me.name);
				parseExt = EXT_FILES.includes(ext) ? "file" : getExt(me.name, 1) ? ext : "file";
				
				tree = [
					...tree, 
					<Summary key={i} As="div" role="treeitem" 
						className={"text-truncate tree-item tree-file q-fw i-color qi qi-" + parseExt} // (getExt(me.name, 1) ? parseExt : "file")
						title={me.name} 
					> {me.name}
					</Summary>
				];
			}else{
				pathEnd = me.path.split("/").pop();

				tree = [
					<Details 
						key={i} 
						summary={pathEnd} 
						className="tree-q tree-folder" 
						summaryClass="text-truncate tree-item tree-label q-mr qi qi-folder" 
						summaryProps={{
							role: "treeitem",
							title: pathEnd 
						}}
					>
						{renderTree(me[key], "files")} 
					</Details>,
					...tree
				];
			}
		}
		// console.log('tree: ', tree);
		return tree;
	}

	// const renderDrop = () => {
	// 	return (
	// 		<label className="mb-0 w-100 jumbotron"
	// 			onDrop={onChangeFile} 
	// 		>
	// 			<input onChange={onChangeFile} type="file" {...directoryAttr} multiple hidden />
	// 			<div className="lead">Drag 'n' drop some files here, or click to select files</div>
	// 		</label>
	// 	)
	// }

	return (
		<div className="drops">
			<label className="mb-0 w-100 jumbotron" 
				hidden={data?.length > 0} 
				onDrop={onChangeFile} 
			>
				<input onChange={onChangeFile} id={inputID} type="file" {...directoryAttr} multiple hidden />
				<div className="lead">Drag 'n' drop some files here, or click to select files</div>
			</label>

			{data &&  
				<div>
					<Btn As="label" htmlFor={inputID}>Change folder</Btn>{" "}
					<Btn onClick={() => setData(null)} kind="danger">Remove</Btn>
				</div>
			}

			{data && 
				<div className="col-3 px-0">
					<div 
						className="small" 
					>
						{renderTree(data, "files")} 
						{/* {data && renderTree(data, "files")} */}
					</div>
				</div>
			}
		</div>
	);
}

/*
<Details 
	open 
	renderOpen 
	summary="Folder" // DEV OPTION: get root folder 
	className="small w-100 tree-q tree-folder" 
	summaryClass="text-truncate tree-item tree-file" 
	summaryProps={{
		role: "treeitem",
	}}
>
	{renderTree(data, "files")} 
</Details>

      <div {...getRootProps({className: "jumbotron dropzone"})}>
        <input {...getInputProps()} />
        <p className="lead">Drag 'n' drop some files here, or click to select files</p>
      </div>

      <aside>
			<h4>Files</h4>
			<ul>{files}</ul>
			</aside>
*/
