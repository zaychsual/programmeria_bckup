import React from 'react';// , { Component, cloneElement }
// import P from 'prop-types';// PropTypes

// import glamorous from 'glamorous';
import Resizer from './Resizer';
import Pane from './Pane';

// import { Cx } from '../../utils/Q';// setClass

const DEFAULT_PANE_SIZE = '1';
const DEFAULT_PANE_MIN_SIZE = '0';
const DEFAULT_PANE_MAX_SIZE = '100%';

// const ColumnStyle = glamorous.div({
  // display: 'flex',
  // height: '100%',
  // flexDirection: 'column',
  // flex: 1,
  // outline: 'none',
  // overflow: 'hidden',
  // userSelect: 'text'
// });
// const ColumnStyle = {
  // display: 'flex',
  // height: '100%',
  // flexDirection: 'column',
  // flex: 1,
  // outline: 'none',
  // overflow: 'hidden',
  // userSelect: 'text'
// };

// const RowStyle = glamorous.div({
  // display: 'flex',
  // height: '100%',
  // flexDirection: 'row',
  // flex: 1,
  // outline: 'none',
  // overflow: 'hidden',
  // userSelect: 'text'
// });
// const RowStyle = {
  // display: 'flex',
  // height: '100%',
  // flexDirection: 'row',
  // flex: 1,
  // outline: 'none',
  // overflow: 'hidden',
  // userSelect: 'text'
// };

function convert(str, size){
  const tokens = str.match(/([0-9]+)([px|%]*)/);
  const value = tokens[1];
  const unit = tokens[2];
  return toPx(value, unit, size);
}

function toPx(value, unit = 'px', size){
  switch (unit) {
    case '%': {
      return +(size * value / 100).toFixed(2);
    }
    default: {
      return +value;
    }
  }
}

function removeNullChildren(children){
  return React.Children.toArray(children).filter(c => c);
}

export function getUnit(size) {
  if(size.endsWith('px')) return 'px';

  if(size.endsWith('%')) return '%';

  return 'ratio';
}

export function convertSizeToCssValue(value, resizersSize){
  if(getUnit(value) !== '%') return value;

  if(!resizersSize) return value;

  const idx = value.search('%');
  const percent = value.slice(0, idx) / 100;
	
  if(percent === 0) return value;

  return `calc(${value} - ${resizersSize}px*${percent})`
}

function convertToUnit(size, unit, containerSize){
  switch(unit){
    case '%':
      return `${(size / containerSize * 100).toFixed(2)}%`;
    case 'px':
      return `${size.toFixed(2)}px`;
    case 'ratio':
      return (size * 100).toFixed(0);
		default:
			break;
  }
}

class SplitPane extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      sizes: this.getPanePropSize(props),
			active: false // Q-CUSTOM
    };
		// Q-CUSTOM
		// this.cursor = props.split === 'vertical' ? 'col-resize':'row-resize';
		// this.update = null;// false
  }

  // componentWillReceiveProps(nextProps) {
    // this.setState({sizes: this.getPanePropSize(nextProps)});
  // }
	
	/* shouldComponentUpdate(nextProps){// nextState
		const {size} = this.props;// initialSize
		if(size && size !== nextProps.size){
			this.update = nextProps;
			return true;
		}
		// else{
			// this.update = null;
			// return false;
		// }
		return false;
	}
	
	componentDidUpdate(prevProps){
		// Typical usage (don't forget to compare props):
		// if(this.props.userID !== prevProps.userID) {
			// this.fetchData(this.props.userID);
		// }
		const {size} = this.props;
		if(size && this.update){
			this.setState({sizes: this.getPanePropSize(this.update)});
		}
		// if(size && size !== prevProps.size){
			
		// }
	} */

  componentWillUnmount(){
    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('mousemove', this.onMouseMove);

    document.removeEventListener('touchmove', this.onTouchMove);
    document.removeEventListener('touchend', this.onMouseUp);
  }

  onMouseDown = (e, resizerIndex) => {
    if(e.button !== 0) return;
    e.preventDefault();
    this.onDown(resizerIndex, e.clientX, e.clientY);
  }

  onTouchStart = (e, resizerIndex) => {
    e.preventDefault();

    const {clientX, clientY} = e.touches[0];

    this.onDown(resizerIndex, clientX, clientY);
  }

  onDown = (resizerIndex, clientX, clientY) => {
    const {allowResize, onResizeStart} = this.props;// , split

    if(!allowResize) return;

    this.resizerIndex = resizerIndex;
    this.dimensionsSnapshot = this.getDimensionsSnapshot(this.props);
    this.startClientX = clientX;
    this.startClientY = clientY;

    document.addEventListener('mousemove', this.onMouseMove);
    document.addEventListener('mouseup', this.onMouseUp);

    document.addEventListener('touchmove', this.onTouchMove);
    document.addEventListener('touchend', this.onMouseUp);
    document.addEventListener('touchcancel', this.onMouseUp);
		
		// setClass(document.body, this.cursor);
		// setClass(document.body, 'active');
		// setClass(e.target, 'active');
		this.setState({active: true});

    if(onResizeStart) onResizeStart();
  }

  onMouseMove = e => {
    e.preventDefault();
    this.onMove(e.clientX, e.clientY);
  }

  onTouchMove = e => {
    e.preventDefault();
    const {clientX, clientY} = e.touches[0];
    this.onMove(clientX, clientY);
  }

  onMouseUp = e => {
    e.preventDefault();

    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('mousemove', this.onMouseMove);

    document.removeEventListener('touchmove', this.onTouchMove);
    document.removeEventListener('touchend', this.onMouseUp);
    document.addEventListener('touchcancel', this.onMouseUp);
		
		// setClass(document.body, this.cursor, 'remove');
		// setClass(document.body, 'active', 'remove');
		// setClass(e.target, 'active', 'remove');
		this.setState({active: false});

    if(this.props.onResizeEnd) this.props.onResizeEnd(this.state.sizes);
  }

  getDimensionsSnapshot(props){
    const split = props.split;
    const paneDimensions = this.getPaneDimensions();
    const splitPaneDimensions = this.splitPane.getBoundingClientRect();
    const minSizes = this.getPanePropMinMaxSize(props, 'minSize');
    const maxSizes = this.getPanePropMinMaxSize(props, 'maxSize');

    const resizersSize = this.getResizersSize(removeNullChildren(this.props.children));
    const splitPaneSizePx = split === 'vertical' ? splitPaneDimensions.width - resizersSize : splitPaneDimensions.height - resizersSize;

    const minSizesPx = minSizes.map(s => convert(s, splitPaneSizePx));
    const maxSizesPx = maxSizes.map(s => convert(s, splitPaneSizePx));
    const sizesPx = paneDimensions.map(d => split === 'vertical' ? d.width : d.height);

    return {
      resizersSize,
      paneDimensions,
      splitPaneSizePx,
      minSizesPx,
      maxSizesPx,
      sizesPx
    };
  }

  getPanePropSize(props){
    return removeNullChildren(props.children).map(child => {
      const value = child.props['size'] || child.props['initialSize'];
      if(value === undefined) return DEFAULT_PANE_SIZE;
      
      return String(value);
    });
  }

  getPanePropMinMaxSize(props, key){
    return removeNullChildren(props.children).map(child => {
      const value = child.props[key];
      if(value === undefined) return key === 'maxSize' ? DEFAULT_PANE_MAX_SIZE : DEFAULT_PANE_MIN_SIZE;
      
      return value;
    });
  }

  getPaneDimensions(){
    return this.paneElements.filter(el => el).map(el => el.getBoundingClientRect());
  }

  getSizes(){
    return this.state.sizes;
  }

  onMove(clientX, clientY){
    const { split, onChange } = this.props;
    const resizerIndex = this.resizerIndex;
    const {
      sizesPx,
      minSizesPx,
      maxSizesPx,
      splitPaneSizePx,
      paneDimensions
    } = this.dimensionsSnapshot;

    const sizeDim = split === 'vertical' ? 'width' : 'height';
    const primary = paneDimensions[resizerIndex];
    const secondary = paneDimensions[resizerIndex + 1];
    const maxSize = primary[sizeDim] + secondary[sizeDim];

    const primaryMinSizePx = minSizesPx[resizerIndex];
    const secondaryMinSizePx = minSizesPx[resizerIndex + 1];
    const primaryMaxSizePx = Math.min(maxSizesPx[resizerIndex], maxSize);
    const secondaryMaxSizePx = Math.min(maxSizesPx[resizerIndex + 1], maxSize);

    const moveOffset = split === 'vertical' ? this.startClientX - clientX : this.startClientY - clientY;

    let primarySizePx = primary[sizeDim] - moveOffset;
    let secondarySizePx = secondary[sizeDim] + moveOffset;

    let primaryHasReachedLimit = false;
    let secondaryHasReachedLimit = false;

    if(primarySizePx < primaryMinSizePx){
      primarySizePx = primaryMinSizePx;
      primaryHasReachedLimit = true;
    }else if(primarySizePx > primaryMaxSizePx){
      primarySizePx = primaryMaxSizePx;
      primaryHasReachedLimit = true;
    }

    if(secondarySizePx < secondaryMinSizePx){
      secondarySizePx = secondaryMinSizePx;
      secondaryHasReachedLimit = true;
    }else if(secondarySizePx > secondaryMaxSizePx){
      secondarySizePx = secondaryMaxSizePx;
      secondaryHasReachedLimit = true;
    }

    if(primaryHasReachedLimit){
      secondarySizePx = primary[sizeDim] + secondary[sizeDim] - primarySizePx;
    }else if(secondaryHasReachedLimit){
      primarySizePx = primary[sizeDim] + secondary[sizeDim] - secondarySizePx;
    }

    sizesPx[resizerIndex] = primarySizePx;
    sizesPx[resizerIndex + 1] = secondarySizePx;

    let sizes = this.getSizes().concat();
    let updateRatio;

    [primarySizePx, secondarySizePx].forEach((paneSize, idx) => {
      const unit = getUnit(sizes[resizerIndex + idx]);
      if(unit !== 'ratio'){
        sizes[resizerIndex + idx] = convertToUnit(paneSize, unit, splitPaneSizePx);
      }else{
        updateRatio = true;
      }
    });

    if(updateRatio){
      let ratioCount = 0;
      let lastRatioIdx;
      sizes = sizes.map((size, idx) => {
        if(getUnit(size) === 'ratio'){
          ratioCount++;
          lastRatioIdx = idx;

          return convertToUnit(sizesPx[idx], 'ratio');
        }

        return size;
      });

      if(ratioCount === 1) sizes[lastRatioIdx] = '1';
    }

    onChange && onChange(sizes);

    this.setState({sizes});
  }

  setPaneRef = (idx, el) => {
    if(!this.paneElements) this.paneElements = [];

    this.paneElements[idx] = el;
  }

  getResizersSize(children){
    return (children.length - 1) * this.props.resizerSize;
  }

  render(){
    const { children, className, split, style, theme, allowResize, onDoubleClick, resizeClass } = this.props;// 
    const notNullChildren = removeNullChildren(children);// this.props.children
    const sizes = this.getSizes();
    const resizersSize = this.getResizersSize(notNullChildren);

    const elements = notNullChildren.reduce((acc, child, idx) => {
      let pane;
      const resizerIndex = idx - 1;
      const isPane = child.type === Pane;
      const paneProps = {
        index: idx, 
        // 'data-type': 'Pane',
        split: split,
        key: "Pane-" + idx,
        innerRef: this.setPaneRef,
        resizersSize,
        size: sizes[idx]
      };

      if(isPane){
        pane = React.cloneElement(child, paneProps);
      }else{
        pane = <Pane {...paneProps}>{child}</Pane>;
      }

      if(acc.length === 0 || !allowResize){
        return [...acc, pane];
      }else{
        const resizer = (
          <Resizer
            index={resizerIndex}
            key={"Resizer-" + resizerIndex}
            split={split} 
						active={this.state.active} 
            onMouseDown={this.onMouseDown}
            onTouchStart={this.onTouchStart} 
						onDoubleClick={onDoubleClick} // Q-CUSTOM
						className={resizeClass} // Q-CUSTOM
          />
        );

        return [...acc, resizer, pane];
      }
    }, []);

    // const StyleComponent = split === 'vertical' ? RowStyle : ColumnStyle;

    return (
      <div // StyleComponent 
				// style={split === 'vertical' ? {...RowStyle, ...style} : {...ColumnStyle, ...style}} 
				style={style} 
        className={Q.Cx("SplitPane", split, theme, className)} // 
        // data-type="SplitPane" 
        // data-split={split} 
				// innerRef={el => {
        ref={n => this.splitPane = n} 
      >
        {elements}
      </div>
    );
  }
}

SplitPane.defaultProps = {
  split: 'vertical',
  resizerSize: 1,
  allowResize: true,
	theme: "light"
};

// SplitPane.propTypes = {
  // children: P.arrayOf(P.node).isRequired,
  // className: P.string,
  // split: P.oneOf(['vertical', 'horizontal']),
  // resizerSize: P.number,
  // onChange: P.func,
  // onResizeStart: P.func,
  // onResizeEnd: P.func
// };

export default SplitPane;
