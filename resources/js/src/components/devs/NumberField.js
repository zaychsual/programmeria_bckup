import React, { useState, memo } from "react"; // useMemo

function NumberField({
  value, 
  defaultValue, 

  // Intl Options:
  lang = "en-GB", // en-GB || en-US
  format = "decimal", // decimal | currency (default intl name = style)
  // currency, // "USD" | "IDR"
  // maximumSignificantDigits, //  = 3, 
  intl, // Intl options

  onChange = Q.noop, // () => {} 
  ...etc
}){
  const [val, setVal] = useState(value || defaultValue || "");

  const toCurrency = (v, lang, intlOps) => {
    // const { lang, style, currency } = this.props; // maximumSignificantDigits
    // new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(number)

    // "sv-SE" | en-GB | "id-ID"
    // const formatter = new Intl.NumberFormat(lang, {
    //   style, // : "decimal", // decimal | currency
    // 	currency, // : "IDR", // SEK
    // 	maximumSignificantDigits
    // });

    // const intl = new Intl.NumberFormat(lang, intlOps);
    // return intl.format(v);
    const repVal = v.replace(/,|[a-z]|[A-Z]/g, "");// .replace(/[a-z]|[A-Z]/g, "");
    // console.log('repVal: ', repVal);

    if(/[0-9]/g.test(repVal)){
      return new Intl.NumberFormat(lang, intlOps).format(Number(repVal));
    }

    return "";
  }

  const Change = e => {
    const v = e.target.value;
    setVal(v);
    onChange(v, e);
  }

  return (
    <input 
      {...etc}
      type="text"
      inputMode="numeric" // decimal
      value={toCurrency(val, lang, {
        ...intl, 
        style: format, 
        // currency, 
        // maximumSignificantDigits 
      })}
      onChange={Change}
    />
  );
}

export default memo(NumberField);

/* 
export class Number extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // isEdit: false
      vals: props.value || props.defaultValue || ""
    };
  }

  // componentDidMount(){
  // 	console.log('%ccomponentDidMount in NumberField','color:yellow;');
  // }

  onChange = (e) => {
    const val = e.target.value;
    this.setState({ vals: val });
    this.props.onChange(val, e);
  };

  toCurrency(v, lang, intlOps) {
    // const { lang, style, currency } = this.props; // maximumSignificantDigits
    // new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(number)

    // "sv-SE" | en-GB | "id-ID"
    // const formatter = new Intl.NumberFormat(lang, {
    //   style, // : "decimal", // decimal | currency
    // 	currency, // : "IDR", // SEK
    // 	maximumSignificantDigits
    // });

    // const intl = new Intl.NumberFormat(lang, intlOps);
    // return intl.format(v);
    const repVal = v.replace(/,/g, "");

    if (/[0-9]/.test(repVal)) {
      return new Intl.NumberFormat(lang, intlOps).format(repVal); // val
    }

    return "";
  }

  render() {
    // value, defaultValue,
    const {
      lang,
      style,
      currency,
      maximumSignificantDigits,
      ...etc
    } = this.props; // className, name,

    return (
      <input
        {...etc}
        type="text"
        inputMode="numeric"
        value={this.toCurrency(this.state.vals, lang, {
          style,
          currency,
          maximumSignificantDigits
        })}
        onChange={this.onChange}
      />
    );
  }
}

Number.defaultProps = {
  // Intl Options:
  lang: "en-US", // en-GB
  style: "decimal", // decimal | currency
  // currency, // "USD" | "IDR"
  maximumSignificantDigits: 3,

  // value: "",
  onChange: () => {}
};
*/
