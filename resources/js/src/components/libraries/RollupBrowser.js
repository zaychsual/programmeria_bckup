import React, { Suspense } from 'react'; // useState, useEffect, 
import { lazy } from '@loadable/component';

import SpinLoader from '../q-ui-react/SpinLoader';

const Lib = lazy.lib(() => import(/* webpackChunkName: "RollupBrowser" */ "rollup/dist/rollup.browser.js"));

export default function RollupBrowser({ children, load }){
  if(load){
    return (
      <Suspense fallback={<SpinLoader bg="/icon/android-icon-36x36.png" />}>
        <Lib>
          {(rollup) => {
            if(rollup && children){
              return children(rollup);
            }
            return null;
          }}
        </Lib>
      </Suspense>
    )
  }

  return null;
}

