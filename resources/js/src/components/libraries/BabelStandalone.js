import React, { Suspense } from 'react'; // useState, useEffect, 
// import loadable from '@loadable/component';
import { lazy } from '@loadable/component';// loadable

import SpinLoader from '../q-ui-react/SpinLoader';

// const Moment = loadable.lib(() => import('@babel/standalone'));
const Lib = lazy.lib(() => import(/* webpackChunkName: "BabelStandalone" */ "@babel/standalone")); // props.module

export default function BabelStandalone({ children, load }){ // , onLoad
  // const loadProps = Q.isBool(load);
  // const [isLoad, setIsLoad] = useState(loadProps);

  // useEffect(() => {
  //   if(!isLoad) setIsLoad(true);
  // }, [isLoad]);

  if(load){
    return (
      <Suspense fallback={<SpinLoader bg="/icon/android-icon-36x36.png" />}>
        <Lib>
          {(Babel) => {
            if(Babel && children){
              // window.Babel = Babel;
              // if(onLoad) onLoad(Babel);

              return children(Babel);
            }
            return null;
          }}
        </Lib>
      </Suspense>
    )
  }

  return null;
}

/* 
if(load){
  if(window.Babel){
    return children(window.Babel);
  }

  return (
    <Suspense fallback={<SpinLoader bg="/icon/android-icon-36x36.png" />}>
      <Lib>
        {(Babel) => {
          if(Babel && children){
            window.Babel = Babel;

            return children(Babel);
          }
          return null;
        }}
      </Lib>
    </Suspense>
  )
}
*/

// export default BabelStandalone;
// export { LoadLibrary };