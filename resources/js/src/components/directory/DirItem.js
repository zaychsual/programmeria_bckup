import React, { useState } from 'react'; // , { useRef, useState, useEffect }
import { useDrag } from 'react-dnd'; // useDrop
import Dropdown from 'react-bootstrap/Dropdown';

import Btn from '../q-ui-react/Btn';
// import ContextMenu from '../q-ui-react/ContextMenu';
// import { clipboardCopy } from '../../utils/clipboard';
import { treeType } from './utils';

// function stringify(obj, replacer, spaces, cycleReplacer) {
//   return JSON.stringify(obj, serializer(replacer, cycleReplacer), spaces)
// }

// function serializer(replacer, cycleReplacer) {
//   let stack = [], keys = []

//   if (cycleReplacer == null) cycleReplacer = function(key, value) {
//     if (stack[0] === value) return "[Circular ~]"
//     return "[Circular ~." + keys.slice(0, stack.indexOf(value)).join(".") + "]"
//   }

//   return function(key, value) {
//     if (stack.length > 0) {
//       let thisPos = stack.indexOf(this)
//       ~thisPos ? stack.splice(thisPos + 1) : stack.push(this)
//       ~thisPos ? keys.splice(thisPos, Infinity, key) : keys.push(key)
//       if (~stack.indexOf(value)) value = cycleReplacer.call(this, key, value)
//     }
//     else stack.push(value)

//     return replacer == null ? value : replacer.call(this, key, value)
//   }
// }

export default function DirItem({
	type, // Dnd
	data, 
	className, 
	isDrop, 
	children,  
	title, 
	titleAttr, 
	icon, 
	// appendCtxMenu, 
	ariaControls, // id
	expanded, 
  labelClass, 
  copy, 
  cut, //  = false
  rename, 
  onClick = Q.noop, 
  // onDoubleClick = Q.noop, // OPTION
	onCut = Q.noop, 
	onCopy = Q.noop, 
  onPaste = Q.noop, 
  // onCtrlPaste = Q.noop, 
  onClickRename = Q.noop, 
  onRename = Q.noop, 
  onBlurRename = Q.noop, 
  // onKeyDownRename = Q.noop, 
  onSaveRename = Q.noop, 
	onDelete = Q.noop, 
}){
	// const ID = "DirItem" + "-" + Q.Qid(3);
	// const targetFlip = useRef(null);
	// const [focusCtxMenu, setFocusCtxMenu] = useState(false);
	// const [targetFlip, setTargetFlip] = useState(null);
	// const [focusPaste, setFocusPaste] = useState(null);// false
	const [showMenu, setShowMenu] = useState(false);

  const [{ isDragging }, drag, preview] = useDrag({
    item: { type, ...data }, // type: `${color}`
    canDrag: type === treeType.DIR || type === treeType.FILE, // !forbidDrag
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    })
	});

	const BTNS = [ 
		{t:"Rename", i:"pen", fn: onClickRename}, 
		{t:"Delete", i:"trash", fn: onDelete} // fn: onDelete | fn: (e) => onDelete(data, e)
	].filter(() => type !== "root");

  // paste && type === treeType.DIR ? {t:"Paste", i:"check", fn: onPaste} : null;
  // (paste && type !== treeType.FILE) || (copy && type !== treeType.DIR)
	const PASTE = (cut || copy) && type === treeType.DIR ? {t:"Paste", i:"check", fn: onPaste} : null;

  // const onCopyItem = (e) => {
  //   console.log("onCopyItem e.target: ", e.target);
  //   console.log('onCopyItem data: ', data);

  //   const dataObj = stringify(data, null, 2);
  //   console.log('onCopyItem dataObj: ', dataObj);

  //   onCopy(e);
    
  //   // clipboardCopy(JSON.stringify(data)).then(v => {
  //   //   console.log(v);
  //   // }).catch(e => console.log(e));
  // }

	const CUT_COPY = [
		{t:"Cut", i:"cut", fn: onCut}, 
		{t:"Copy", i:"copy", fn: onCopy}, 
		PASTE
	].filter(v => v && v.fn);

	// const unFocus = () => setFocusCtxMenu(false);

	// const onMouseLeave = () => { // e
	// 	// const et = e.target;
	// 	// const dd = Q.domQ('button[aria-expanded="true"]', et.closest('.tree-item'));
	// 	// if(dd) dd.click();

	// 	if(showMenu) setShowMenu(false);
  // }
  
  // const Click = (e, item) => {
  //   setFocusPaste({ e, item }); // Q.domQ("#" + ID)
  //   onClick(e);
  // }

  // console.log('focusPaste: ', focusPaste);
  // console.log('document.activeElement: ', document.activeElement);

  // useEffect(() => {
  //   const pasteEvt = e => {
  //     Q.preventQ(e);
  //     let dataPaste = (e.clipboardData || window.clipboardData).getData('text');
  //     console.log('pasteEvt dataPaste: ', dataPaste);

  //     onCtrlPaste(e);// onCtrlPaste | onPaste, 
  //   }

  //   if(focusPaste) document.addEventListener("paste", pasteEvt);
  //   else document.removeEventListener("paste", pasteEvt);

  //   return () => {
  //     if(focusPaste) document.removeEventListener("paste", pasteEvt);
  //   }
	// }, [focusPaste]);
	
	// console.log('targetFlip: ', targetFlip);

	return (
		<div 
			ref={preview} 
			role="treeitem" 
			className={
				Q.Cx("tree-item", {
					"isDragging bg-main": isDragging, 
					// "focus": showMenu // focusCtxMenu
				}, isDrop)
			}
			// onMouseLeave={onMouseLeave} 
		>
			<form noValidate 
				// ref={targetFlip} 
				onSubmit={onSaveRename} 
				onContextMenu={(e) => {
					Q.preventQ(e);
					document.body.click();
					setShowMenu(true);
				}}
				className={Q.Cx("input-group input-group-xs", className)} //  flex-nowrap
			>
				{Q.isStr(rename) ? 
					<>
						<div className="input-group-prepend">
							<div className={"input-group-text bg-white border-transparent i-color qi qi-" + icon} />
						</div>

						<input defaultValue={rename} onChange={onRename} className="form-control shadow-none" type="text" 
							autoFocus 
							spellCheck="false" 
							onBlur={onBlurRename} 
							// onKeyDown={onKeyDownRename} 
						/>
					</>
					: 
					<div ref={drag} 
						className={
							Q.Cx("tree-label form-control btn btn-flat text-left", { 
								["i-color qi qi-" + icon]: icon, 
								"isFile": type === treeType.FILE 
							}, labelClass) 
						} 
						tabIndex="0" 
						aria-controls={ariaControls} 
						aria-expanded={expanded} 
						title={titleAttr || title} // titleAttr ? titleAttr : title
						onClick={onClick} // e => Click(e, data) | onClick 
						// onDoubleClick={onDoubleClick} 
						// OPTION for paste
						// onBlur={() => setFocusPaste(null)} 
						// id={ID} 
						// spellCheck={focusPaste ? false : null} 
						// contentEditable={focusPaste ? true : null} 
						// dangerouslySetInnerHTML={focusPaste ? { __html: title } : undefined} 
						// onKeyDown={e => {
						//   console.log(e.key);
						//   console.log(e.ctrlKey);
						//   if(e.key.toLowerCase() === "v" && e.ctrlKey){
						//     Q.preventQ(e);
						//   }
						// }} 

						// onPaste={(e) => {
						//   console.log('onPaste e: ', e);
						//   // console.log('onPaste e.target: ', e.target);
						//   // console.log('onPaste e.nativeEvent: ', e.nativeEvent);
						// }}
					>
						{title}
					</div>
				}
				
				<div className="input-group-append tree-tool">
					<Dropdown bsPrefix="btn-group btn-group-xs" alignRight
						show={showMenu} 
						onToggle={see => setShowMenu(see)}
					>
						<Dropdown.Toggle variant="flat" className="rounded-0" tabIndex="-1" />
						<Dropdown.Menu className="dropdown-menu show py-0 v-dd-sets mnw-auto w-auto" 
							// onClick={e => Q.preventQ(e)} 
							popperConfig={{
								modifiers: [{
									name: "offset",
									options: {
										offset: [0, -0.05]
									}
								}]
							}}
						>
							{children}

							{CUT_COPY.map((v, i) => 
								<Dropdown.Item key={i} as="button" type="button" 
									className={"qi qi-" + v.i} 
									tabIndex="-1"
									onClick={v.fn} 
								>
									{v.t}
								</Dropdown.Item>
							)}
						</Dropdown.Menu>
					</Dropdown>

					{BTNS.map(v => 
						<Btn key={v.t} 
							onClick={v.fn} blur kind="flat" className={"qi qi-" + v.i + (type === treeType.FILE ? " isFile":"")} 
							title={v.t} 
							tabIndex="-1" 
						/>
					)}
				</div>
			</form>
		</div>
	)
}

