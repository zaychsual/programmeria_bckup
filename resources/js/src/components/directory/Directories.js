import React, { useRef, useState } from 'react';// { useMemo, useEffect, useRef, useContext, useLayoutEffect, useCallback }
// import { useDrag, useDrop } from 'react-dnd';
// import { DndProvider } from 'react-dnd';

// import DndBackend from './parts/DndBackend';
import Directory from './Directory';
import DirItem from './DirItem';
// import Flex from '../q-ui-react/Flex';
import { confirm } from '../react-confirm/util/confirm';
import { treeType } from './utils';
import { searchDFS } from './utils';

function getExt(fname, bool){
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

const CONFIRM_OPTION = {
	type: "toast", 
	// title: "Title",
	// icon: <i className="fab fa-react mr-2" />, 
	bodyClass: "text-center", 
	btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next", 
	modalProps: {
		centered: true, 
		returnFocusAfterClose: false
	}
};

const NEW_FILE = { type: "file", path: "" };

export default function Directories({
	data, 
	copy, 
	cut, 
	onDrop = Q.noop, 
	onCut = Q.noop, 
	onCopy = Q.noop, 
	onPaste = Q.noop, 
	onDelete = Q.noop, 
	onNewFile = Q.noop, 
	// onRename = Q.noop, 
	// onToggle = Q.noop, 
}){
	const inputFile = useRef(null);
	const [itemFile, setItemFile] = useState(null);// For upload file item position
	const [rename, setRename] = useState(null);
	const [renameVal, setRenameVal] = useState("");

	// const onConfirm = async (txt, onOk = Q.noop, options = {}) => { //  
	// 	if(await confirm(txt, { ...CONFIRM_OPTION, ...options })){
	// 		onOk();
	// 	}
	// }

	// console.log('rename: ', rename);
	// console.log('renameVal: ', renameVal);

	const onDeleteItem = async (item) => {
		let newState = [...data];// _cloneDeep(state);
		let node = null;
  	let parent = null;
    const foundNode = searchDFS({
      data: newState,
      cond: (val) => {
				return val.path === item.path; // val.path === action.payload.id
      },
    });
    node = foundNode.item;
		parent = node.parentNode; // foundNode.parent | node.parentNode

		const dataName = node.path.split('/').pop();
		// const conf = await confirm(<Flex As="h6" dir="column" justify="center" className={"q-mb qi-3x i-color qi qi-" + (item.type === "file" ? getExt(dataName):"folder")}><small className="mb-3">{dataName}</small>Are you sure to delete this {node.type}</Flex>, CONFIRM_OPTION);
		const conf = await confirm(<h6>Are you sure to delete this {node.type}?<br/>{dataName}</h6>, CONFIRM_OPTION);

		if(!parent || Array.isArray(parent)){
			// onConfirm(msg, () => {
			// 	newState = newState.filter(file => file.path !== item.path); // file.id !== action.payload.id
			// 	onDelete(newState);
			// });
			if(conf){
				newState = newState.filter(file => file.path !== item.path); // file.id !== action.payload.id
				onDelete(newState);
			}
		} else {
			if(conf){
				parent.files = parent.files.filter(file => file.path !== item.path);
				onDelete(newState);
			}
		}
		// console.log('onDeleteItem ext: ', getExt(dataName));
		// console.log('onDeleteItem item: ', item);
	}

	const onClickRenameItem = (i, data, val) => {
		console.log('onClickRenameItem i: ', i);
		console.log('onClickRenameItem data: ', data);
		console.log('onClickRenameItem val: ', val);

		setRename({ i, data });
		setRenameVal(val);
	}

	const onRenameItem = e => {
		const val = e.target.value;
		console.log('onRename val: ', val);
		setRenameVal(val);
		// onRename();
	}

	const onBlurRenameItem = () => {
		const isVal = renameVal.length > 0;
		// if(isVal){
		// 	setRename(null);
		// }
		
		setRename(null);
		setRenameVal(isVal ? renameVal : rename.data.path.split('/').pop());
	}

	// const onKeyDownRenameItem = e => {
	// 	console.log(e.key);
	// 	if(e.key === "Enter" && renameVal.length > 0){
	// 		setRename(null);
	// 	}
	// }

	const onSaveRenameItem = e => {
		Q.preventQ(e);

		// const valid = /^[^\\\/\:\*\?\"\<\>\|\.]+(\.[^\\\/\:\*\?\"\<\>\|\.]+)+$/.test(renameVal);
		const valid = /^[^\\/?%*:|"<>\.]+$/.test(renameVal);
		console.log("onSaveRenameItem valid: ", valid);

		if(renameVal.length > 0 && valid){
			setRename(null);
		}
	}

	const renderTree = (node, parentNode, key = "files") => { 
		if(node == null) return;

		let lng = node.length;
		let tree = [];
		
		for(let i = 0; i < lng; i++){
			let item = node[i];
			let pathEnd = item.path.split('/').pop();

			item.parentNode = parentNode;
			if(!parentNode){
				item.parentNode = item.files;
			}

			if(item.type === 'file' || item.isFile){
				tree = [
					...tree, 
					<DirItem key={i} className="tree-file" 
						type={treeType.FILE} 
						icon={getExt(pathEnd, 1) ? getExt(pathEnd) : "file"}  
						title={pathEnd} 
						data={item} 
						copy={copy} 
						cut={cut} 
						rename={rename?.data.path === item.path ? renameVal : null} 
						// ariaControls={ID} 
						labelClass={cut?.path === item.path ? "o-05" : ""} 
						onClick={(e) => {
							console.log('onClick TreeItem label Directory item: ', item);
							// console.log('onClick TreeItem label Directory document.activeElement: ', document.activeElement);

						}} 
						onCut={(e) => onCut(item, e)} // (e, sum) => onCut(e, sum, item)
						onCopy={(e) => onCopy(item, e)} // 
						onDelete={() => onDeleteItem(item)} 
						onClickRename={() => onClickRenameItem(i, item, pathEnd)} 
						onRename={onRenameItem} 
						onBlurRename={onBlurRenameItem} 
						// onKeyDownRename={onKeyDownRenameItem} 
						onSaveRename={onSaveRenameItem} 
					/>
				];
			}else{
				tree = [
					<Directory key={i} 
						data={item} 
						title={pathEnd} 
						// id={Q.Qid() + "-" + pathEnd} 
						// inputFileId={Q.Qid()} 
						copy={copy} 
						cut={cut} 
						rename={rename?.data.path === item.path ? renameVal : null} 
						labelClass={cut?.path === item.path ? "o-05" : ""} 
						onCut={(e) => onCut(item, e)} // (e, sum) => onCut(e, sum, item)
						onCopy={(e) => onCopy(item, e)} 
						onPaste={(e) => onPaste(i, e)} // item
						// onCtrlPaste={e => {
						// 	console.log('onCtrlPaste e: ', e);
						// 	console.log('onCtrlPaste item: ', item);
						// }}
						onClickRename={() => onClickRenameItem(i, item, pathEnd)} 
						onRename={onRenameItem}  
						onBlurRename={onBlurRenameItem} 
						// onKeyDownRename={onKeyDownRenameItem} 
						onSaveRename={onSaveRenameItem} 
						onDelete={() => onDeleteItem(item)} // (datas, e) => onDeleteItem(datas, item)
						// onToggle={onToggle} 
						onDrop={onDrop} 
						onNewFile={() => {
							const newData = data.map((v, i2) => {
								if(i2 === i){
									return {
										...v,
										files: [
											...v.files, 
											NEW_FILE
										]
									}
								}
								return v;
							});
							console.log("onNewFile newData: ", newData);

							onNewFile(newData);

							setTimeout(() => {
								onClickRenameItem(i, NEW_FILE, "")
							}, 99);
						}} 
						onNewDirectory={() => {
							console.log("onNewDirectory");
						}}
						onClickUpload={() => {
							inputFile.current.click();
							setItemFile({ index: i, data: item });
						}}
					>
						{renderTree(item[key], item)} 
					</Directory>,
					...tree
				];
			}
		}
		
		return tree;
	}

	return (
		<Directory 
			open 
			mountOnEnter 
			title="Project" 
			type="root" 
			cut={cut} 
			onCut={null} 
			onCopy={null} 
			onPaste={(e) => onPaste(e, 0)} 
		>
			{data && renderTree(data)} 

			<input ref={inputFile} type="file" tabIndex="-1" hidden 
				onChange={e => {
					const file = e.target.files[0];

					console.log('onChange file: ', file);
					console.log('onChange itemFile: ', itemFile);
				}}
			/>
		</Directory>
	);
}

/*
		<div className="detail-q tree-q tree-folder w-100">
			{data && renderTree(data, "files")}
		</div>
*/
