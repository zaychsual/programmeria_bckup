import React, { useState } from 'react';// { useMemo, useEffect, useRef, useContext, useLayoutEffect, useCallback }
import { useDrag, useDrop } from 'react-dnd';
// import { DndProvider } from 'react-dnd';
import Dropdown from 'react-bootstrap/Dropdown';

// import DndBackend from './parts/DndBackend';
// import Details from '../q-ui-react/Details';
import Btn from '../q-ui-react/Btn';
import ContextMenu from '../q-ui-react/ContextMenu';
import { searchDFS } from './utils';

function getExt(fname, bool){
  return bool ? fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).length : fname.split('.').pop().toLowerCase();
}

const treeType = {
  DIR: "directory",
  FILE: "file"
};

function TreeItem({
	type, // Dnd
	// dropRef, 
	data, 
	// parentNode, 
	className, 
	isDrop, 
	children,  
	title, 
	titleAttr, 
	icon, 
	appendCtxMenu, 
	summaryID, 
	expanded, 
	labelClass, 
	paste, //  = false
	onClick = Q.noop,
	onCut = Q.noop, 
	onCopy = Q.noop, 
	onPaste = Q.noop, 
	onRename = Q.noop, 
	onDelete = Q.noop,  
	As = "div", // summary
}){
	const [focusCtxMenu, setFocusCtxMenu] = useState(false);

  const [{ isDragging }, drag, preview] = useDrag({
    item: { type, ...data }, // type: `${color}`
    canDrag: type === treeType.DIR || type === treeType.FILE, // !forbidDrag
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    })
	});

	const BTNS = [ 
		{t:"Rename", i:"pen", fn: onRename}, 
		{t:"Delete", i:"trash", fn: onDelete} // fn: onDelete | fn: (e) => onDelete(data, e)
	].filter(() => type !== "root");

	// paste && type === treeType.DIR ? {t:"Paste", i:"check", fn: onPaste} : null;
	const PASTE = paste && type !== treeType.FILE ? {t:"Paste", i:"check", fn: onPaste} : null;

	const CUT_COPY = [
		{t:"Cut", i:"pen", fn: onCut}, 
		{t:"Copy", i:"copy", fn: onCopy}, 
		PASTE
	].filter(v => v && v.fn);

	const unFocus = () => setFocusCtxMenu(false);

	const onMouseLeave = (e) => {
		const et = e.target;
		const dd = Q.domQ('.btn[aria-expanded="true"]', et.closest('.tree-item'));
		if(dd) dd.click();
	}

	return (
		<ContextMenu 
			className="zi-1001" // zi-5
			esc={false} 
			// appendTo={this.wrap.current || document.body} 
			// popperConfig={{
			// 	strategy: "fixed",
			// 	// modifiers: [
			// 	//   {
			// 	//     name: 'preventOverflow',
			// 	//     options: {
			// 	//       // padding: 100, 
			// 	//       // boundary: document.body, // this.wrap.current
			// 	//       // altBoundary: true, // false by default
			// 	//       // rootBoundary: 'document', // 'viewport' by default
			// 	//       // mainAxis: false, // true by default
			// 	//     }
			// 	//   },
			// 	// ]
			// }} 
			// , ctxMenu
			component={(hide) => (
				<div className="dropdown-menu show py-1 v-dd-sets mnw-auto w-auto" 
					// ctxMenu | Q.preventQ | (e) => {Q.preventQ(e);hide()}
					onContextMenu={(e) => {
						Q.preventQ(e);
						hide();
						// ctxMenu(e);
						unFocus();
						// setFocusCtxMenu(true);
					}} 
				>
					{appendCtxMenu && appendCtxMenu(hide, unFocus)} 

					{[
						...CUT_COPY, 
						...BTNS
					].map(v => 
						<button key={v.t} 
							onClick={(e) => {
								v.fn(e);
								hide();
								unFocus();
							}} 
						className={"dropdown-item qi q-fw qi-" + v.i} type="button">{v.t}</button>
					)}
				</div>
			)}
			onContextMenu={(active) => { // , e
				// console.log('ContextMenu onContextMenu active: ', active);
				setFocusCtxMenu(!active);
			}}
			// onEsc={() => {
			// 	setFocusCtxMenu(false);
			// }}
			onScrolls={() => {
				setFocusCtxMenu(false);
			}}
		>
			<As 
				ref={preview} 
				role="treeitem" 
				id={summaryID} 
				tabIndex="0" 
				aria-expanded={expanded} 
				className={
					Q.Cx("tree-item", {
						"bg-main": isDragging,
						"focus": focusCtxMenu
					}, isDrop)
				}
				onMouseLeave={onMouseLeave}
			>
				<div 
					className={Q.Cx("input-group input-group-xs", className)} //  flex-nowrap
				>
					<div ref={drag} 
						className={
							Q.Cx("tree-label form-control btn btn-flat text-left", { 
								["i-color qi qi-" + icon]: icon, 
								"isFile": As === "div" 
							}, labelClass) 
						}  
						title={titleAttr ? titleAttr : title} 
						onClick={onClick} 
					>
						{title}
					</div>
					
					<div className="input-group-append tree-tool">
						<Dropdown bsPrefix="btn-group btn-group-xs" alignRight>
							<Dropdown.Toggle variant="flat" className="rounded-0" tabIndex="-1" />
							<Dropdown.Menu className="dropdown-menu show py-0 v-dd-sets mnw-auto w-auto" 
								// onClick={e => Q.preventQ(e)} 
								popperConfig={{
									modifiers: [{
										name: "offset",
										options: {
											offset: [0, -0.05]
										}
									}]
								}}
							>
								{children}

								{CUT_COPY.map((v, i) => 
									<Dropdown.Item key={i} as="button" type="button" 
										className={"qi qi-" + v.i} 
										tabIndex="-1"
										onClick={v.fn} 
									>
										{v.t}
									</Dropdown.Item>
								)}
							</Dropdown.Menu>
						</Dropdown>

						{BTNS.map(v => 
							<Btn key={v.t} 
								onClick={v.fn} blur kind="flat" className={"qi qi-" + v.i + (As === "div" ? " isFile":"")} 
								title={v.t} 
								tabIndex="-1" 
							/>
						)}
					</div>
				</div>
			</As>
		</ContextMenu>
	)
}

function Dir({
	inputFileId, 
	data, 
	type = treeType.DIR, 
	title, 
	className, 
	open = false, // 
	renderOpen, // 
	children, 
	labelClass, 
	paste, //  = false
	onDrop = Q.noop, 
	onToggle = Q.noop, 
	onCut = Q.noop, 
	onCopy = Q.noop, 
	onPaste = Q.noop, 
	onDelete = Q.noop, 
	...etc
}){
	const [one, setOne] = useState(renderOpen);// false
	const [openDir, setOpenDir] = useState(open);
	const ID = Q.Qid();

	const [{ isOver, isOverCurrent }, drop] = useDrop({
		accept: [treeType.DIR, treeType.FILE],
		drop(item){ // , monitor
			// const didDrop = monitor.didDrop();
			// console.log('drop didDrop: ', didDrop);
			// console.log('drop openDir: ', openDir);

			// handleDrop({
			// 	...item, 
			// 	...data
			// });

			// if(didDrop){
			// 	return;
			// }

			console.log('drop openDir: ', openDir);
			setOpenDir(true);

			onDrop(item, data); // , Q.domQ('#' + ID)

			// setTimeout(() => setOpenDir(true), 1);
			// const summary = Q.domQ('#' + ID);
			// if(summary) summary.click();

			// return undefined;
		},
		// hover(){
		// 	setOpenDir(true);
		// },
		collect: (monitor) => ({
			isOver: monitor.isOver(), 
			isOverCurrent: monitor.isOver({ shallow: true }),
		}),
	});

	// const handleDrop = useCallback((dropData) => {
	// 	console.log('handleDrop dropData: ', dropData);
	// 	onDrop(dropData, data);
	// }, []);

	const Toggle = e => {
		const o = e.target.open;
		// console.log("onToggle open: ", o);
		if(!one) setOne(true);

		setOpenDir(o);

		onToggle(o, e);
	}

  const clickCtxMenuUploadFiles = () => {
		console.log("clickCtxMenuUploadFiles");
		let fileInput = Q.domQ("#" + inputFileId);
		if(fileInput){
			fileInput.click();
		}
	}

	return (
		<details 
			{...etc} 
			role="tree" 
			ref={drop} 
			className={
				Q.Cx("detail-q tree-q tree-folder w-100", { "bg-light": (isOver && isOverCurrent) && type !== "root" }, className)
			} 
			open={Boolean(one) ? one : openDir} // open | openDir
			onToggle={Toggle} 
		>
			<TreeItem 
				type={type} 
				As="summary"  
				icon="folder" 
				title={title} 
				summaryID={type !== "root" ? ID : null} 
				expanded={type !== "root" ? openDir : null} 
				isDrop={(isOver && isOverCurrent) && type !== "root" ? "bg-strip" : undefined} 
				data={data} 
				appendCtxMenu={(hide, unFocus) => 
					<>
						{
							[
								{t:"Create file", i:"file", fn: () => console.log("onClick Create file")},
								{t:"Create directory", i:"folder", fn: () => console.log("onClick Create directory")},
								{t:"Upload files", i:"upload", fn: clickCtxMenuUploadFiles}
							].map(v => 
								<button key={v.t} 
									onClick={() => {
										hide();
										unFocus();
										v.fn();
									}} 
									className={"dropdown-item qi q-fw qi-" + v.i} type="button">{v.t}</button>	
							)
						}
					</>
				} 
				labelClass={labelClass} 
				paste={paste} 
				onCut={onCut} 
				onCopy={onCopy} 
				onPaste={onPaste} 
				onDelete={onDelete} // (data, e) => onDelete(data, e)
			>
				<Dropdown.Item as="label" className="btnFile qi qi-upload" tabIndex="-1">
					Upload files
					<input type="file" tabIndex="-1" hidden id={inputFileId} />
				</Dropdown.Item>
				<Dropdown.Item as="button" type="button" className="qi qi-file" tabIndex="-1">New file</Dropdown.Item>
				<Dropdown.Item as="button" type="button" className="qi qi-folder" tabIndex="-1">New directory</Dropdown.Item>
				
				{/* <Btn blur As="label" kind="flat" className="rounded btnFile qi qi-upload" title="Upload files">
					<input type="file" tabIndex="-1" hidden />
				</Btn>
				<Btn blur kind="flat" className="rounded qi qi-file" title="New file" tabIndex="-1" />
				<Btn blur kind="flat" className="rounded qi qi-folder" title="New directory" tabIndex="-1" /> */}
			</TreeItem>

			{(one && children) && <div role="group" className="detail-content">{children}</div>}
		</details>
	);
}

export default function Directory({
	data, 
	paste, //  = false
	onDrop = Q.noop, 
	onCut = Q.noop, 
	onCopy = Q.noop, 
	onPaste = Q.noop, 
	onDelete = Q.noop, 
}){
	const onDeleteItem = (item) => {
		let newState = [...data];// _cloneDeep(state);
		let node = null;
  	let parent = null;
    let foundNode = searchDFS({
      data: newState,
      cond: (val) => {
				// console.log('cond val.path === item.path: ', val.path === item.path);
				return val.path === item.path; // val.path === action.payload.id
      },
    });
    node = foundNode.item;
		parent = node.parentNode; // foundNode.parent | node.parentNode

		let dataName = node.path.split('/').pop();
		let c = window.confirm("Are you sure to delete " + node.type + " " + dataName);
		if(!parent || Array.isArray(parent)){
			if(c){
				newState = newState.filter(file => file.path !== item.path); // file.id !== action.payload.id
				onDelete(newState);
			}
		} else {
			if(c){
				parent.files = parent.files.filter(file => file.path !== item.path);
				onDelete(newState);
			}
		}

		// console.log('onDeleteItem item: ', item);
		// console.log('onDeleteItem node: ', node);
		// console.log('onDeleteItem parent: ', parent);
		// console.log('onDeleteItem foundNode: ', foundNode);
		// console.log('onDeleteItem newState: ', newState);
		// console.log('=================================================================================================');
	}

	const renderTree = (node, parentNode, key = "files") => {// 
		if(node == null) return;

		let lng = node.length;
		let tree = [];
		let pathEnd;
		
		for(let i = 0; i < lng; i++){
			pathEnd = node[i].path.split('/').pop();

			node[i].parentNode = parentNode;
			if(!parentNode){
				node[i].parentNode = node[i].files;
			}

			if(node[i].type === 'file' || node[i].isFile){
				tree = [
					...tree, 
					<TreeItem key={i} className="tree-file" 
						type={treeType.FILE} 
						icon={getExt(pathEnd, 1) ? getExt(pathEnd) : "file"}  
						title={pathEnd} 
						data={node[i]} 
						paste={paste} 
						labelClass={paste && paste.path === node[i].path ? "o-05" : ""} 
						onClick={() => { // e
							console.log('onClick TreeItem label Directory node[i]: ', node[i]);
						}} 
						onCut={(e) => onCut(e, node[i])} // (e, sum) => onCut(e, sum, node[i])
						onCopy={(e) => onCopy(e, node[i])} 
						onDelete={() => onDeleteItem(node[i])} 
					/>
				];
			}else{
				//  text-truncate |  className="dark"
				tree = [
					<Dir key={i} 
						data={node[i]} 
						title={pathEnd} 
						inputFileId={Q.Qid()} 
						paste={paste} 
						labelClass={paste && paste.path === node[i].path ? "o-05" : ""} 
						onCut={(e) => onCut(e, node[i])} // (e, sum) => onCut(e, sum, node[i])
						onCopy={(e) => onCopy(e, node[i])} 
						onPaste={(e) => onPaste(e, i)} // (e, sum) => onPaste(e, sum, i)
						onDelete={() => onDeleteItem(node[i])} // (datas, e) => onDeleteItem(datas, node[i])
						onDrop={onDrop} 
					>
						{renderTree(node[i][key], node[i])} 
					</Dir>,
					...tree
				];
			}
		}
		
		return tree;
	}

	return (
		<Dir 
			open renderOpen 
			title="Project" 
			type="root" 
			paste={paste} 
			onCut={null} 
			onCopy={null} 
			onPaste={(e) => onPaste(e, 0)} 
		>
			{data && renderTree(data)} 
		</Dir>
	);
}

/*


		<div className="detail-q tree-q tree-folder w-100">
			{data && renderTree(data, "files")}
		</div>

<Dir open renderOpen title="Folder name" type="root"></Dir>

		<Details 
			open={open} 
			renderOpen={renderOpen} 
			className="tree-q tree-folder" 
			onToggle={(open, e) => {
				console.log('onToggle open: ', open);
				console.log('onToggle e: ', e);
			}}
		>

		</Details>

		<DndBackend>
			{(backend) => {
				// console.log('backend: ',backend);
				return (

				)
			}}
		</DndBackend>

<div className="input-group input-group-xs tree-item">
	<div className="form-control btn btn-flat text-left fab i-color fa-js" title="App.js">App.js</div>
	<div className="input-group-append tree-tool">
		<Btn kind="flat" className="rounded far fa-pen" title="Rename" tabIndex="-1" />
		<Btn kind="flat" className="rounded far fa-times xx" title="Delete" tabIndex="-1" />
	</div>
</div>
*/
