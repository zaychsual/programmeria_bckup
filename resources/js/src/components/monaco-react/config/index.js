/* const config = {
  urls: {
		// https://cdn.jsdelivr.net/npm/monaco-editor@0.19.3/min/vs/loader.js
    monacoLoader: '/storage/app_modules/monaco-editor/min/vs/loader.js',
		// https://cdn.jsdelivr.net/npm/monaco-editor@0.19.3/min/vs
    monacoBase: '/storage/app_modules/monaco-editor/min/vs'
  }
} */

// 3.3.0
const config = {
  paths: {
		// https://cdn.jsdelivr.net/npm/monaco-editor@0.20.0/min/vs
    vs: "/storage/app_modules/monaco-editor/min/vs" // CDN: 'https://cdn.jsdelivr.net/npm/monaco-editor@0.20.0/min/vs'
  }
}

export default config;
