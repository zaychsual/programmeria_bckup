import React, { useState, useEffect, useRef, useCallback } from 'react';
// import P from 'prop-types';

import MonacoContainer from '../MonacoContainer';

import { monaco } from '../utils';// noop
import { useMount, useUpdate } from '../utils/hooks';

import themes from '../config/themes';

const Editor = ({ 
	value, 
	language, 
	editorDidMount, 
	theme, 
	line, 
	width, 
	height, 
	loading, 
	options, 
	overrideServices, 
	_isControlledMode, 
	className, 
	// wrapperClassName, 
}) => {
  const [isEditorReady, setIsEditorReady] = useState(false);
  const [isMonacoMounting, setIsMonacoMounting] = useState(true);
  const editorRef = useRef();
  const monacoRef = useRef();
  const containerRef = useRef();

  useMount(() => {
    const cancelable = monaco.init();

    cancelable
      .then(monaco => ((monacoRef.current = monaco) && setIsMonacoMounting(false)))
      .catch(err => console.error('Monaco initialization: error:', err));
			
    // return _ => editorRef.current ? disposeEditor() : cancelable.cancel();
		return () => editorRef.current ? disposeEditor() : cancelable.cancel();
  });

  useUpdate(_ => {
    if(options.readOnly){
      editorRef.current.setValue(value);
    }else{
      editorRef.current.executeEdits('', [{
        range: editorRef.current.getModel().getFullModelRange(),
        text: value
      }]);

      if(_isControlledMode){
        const model = editorRef.current.getModel();
        model.forceTokenization(model.getLineCount());
      }

      editorRef.current.pushUndoStop();
    }
  }, [value], isEditorReady);

  useUpdate(() => {
    editorRef.current.updateOptions(options);
  }, [options], isEditorReady);

  useUpdate(() => {
    if (editorRef.current.getOption(monacoRef.current.editor.EditorOption.readOnly)) {
      editorRef.current.setValue(value);
    } else {
      if (value !== editorRef.current.getValue()) {
        editorRef.current.executeEdits('', [{
          range: editorRef.current.getModel().getFullModelRange(),
          text: value,
        }]);

        if (_isControlledMode) {
          const model = editorRef.current.getModel();

          model.forceTokenization(model.getLineCount());
        }

        editorRef.current.pushUndoStop();
      }
    }
  }, [value], isEditorReady);

  useUpdate(() => {
    // set last value by .setValue method before changing the language
    editorRef.current.setValue(value);
    monacoRef.current.editor.setModelLanguage(editorRef.current.getModel(), language);
  }, [language], isEditorReady);

  useUpdate(() => {
    editorRef.current.setScrollPosition({ scrollTop: line });
  }, [line], isEditorReady);

  useUpdate(() => {
    monacoRef.current.editor.setTheme(theme);
  }, [theme], isEditorReady);

  const createEditor = useCallback(() => {
    editorRef.current = monacoRef.current.editor.create(containerRef.current, {
      value,
      language,
      automaticLayout: true,
      ...options,
    }, overrideServices);

    editorDidMount(editorRef.current.getValue.bind(editorRef.current), editorRef.current);

    monacoRef.current.editor.defineTheme('dark', themes['night-dark']);
    monacoRef.current.editor.setTheme(theme);

    setIsEditorReady(true);
  }, [editorDidMount, language, options, overrideServices, theme, value]);

  useEffect(() => {
    !isMonacoMounting && !isEditorReady && createEditor();
  }, [isMonacoMounting, isEditorReady, createEditor]);

  const disposeEditor = () => editorRef.current.dispose();

  return (
		<MonacoContainer
			width={width}
			height={height}
			isEditorReady={isEditorReady}
			loading={loading}
			_ref={containerRef} 
			className={className} 
			// wrapperClassName={wrapperClassName}
		/>
	);
};

Editor.defaultProps = {
  editorDidMount: Q.noop,
  theme: 'dark',// light
	language: 'javascript',
  width: '100%',
  height: '100%',
  loading: 'Loading...',
  options: {}, 
	overrideServices: {}, 
  _isControlledMode: false
};

export default Editor;

// Editor.propTypes = {
  // value: P.string,
  // language: P.string,
  // editorDidMount: P.func,
  // theme: P.string,
  // line: P.number,
  // width: P.oneOfType([P.number, P.string]),
  // height: P.oneOfType([P.number, P.string]),
  // loading: P.oneOfType([P.element, P.string]),
  // options: P.object,
  // _isControlledMode: P.bool
// };
