import React, { useEffect } from 'react';

const useMount = effect => useEffect(effect, []);

export default useMount;
