import React, { Component, createRef, Fragment } from 'react';// { useState,  }
// import { useFullscreen } from 'ahooks';
import screenfull from 'screenfull';
import Dropdown from 'react-bootstrap/Dropdown';
import Tab from 'react-bootstrap/Tab';
import ReactPlayer from 'react-player/lazy';
import IdleTimer from 'react-idle-timer';
import { useInViewport } from 'ahooks';

import Flex from '../q-ui-react/Flex';
import Btn from '../q-ui-react/Btn';
import ContextMenu from '../q-ui-react/ContextMenu';
import BgImage from '../q-ui-react/BgImage';
import { clipboardCopy } from '../../utils/clipboard';

import { formatTime, findElPosition, getPointerPosition, BEZEL_KEYS, CC_FONT_COLORS, CC_FONT_SIZES } from './utils';

function InViewPort({ inRef, children }){
  // const ref = useRef();
  const inView = useInViewport(inRef);

  return (
    <Fragment>
      {children && children(inView)}
    </Fragment>
  );
};

function validateExternalUrl(val){
	const urlReg = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; // /^((https?|ftp|file):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
	const isUrl = urlReg.exec(val);
	
	if(isUrl){
		let sUrl;
		if(isUrl[0]?.startsWith("https://youtu.be/")){
			sUrl = "https://www.youtube.com/embed" + isUrl[4].replace("/embed","");
		}else{
			sUrl = isUrl[1] ? val : isUrl[4] ? "https://www.youtube.com/embed" + isUrl[4].replace("/embed","") : 'https://' + isUrl[0];
		}
		return sUrl;
	}
	else{
		// const iframeReg =  /<iframe.*?src=\s*(["'])(.*?)\1/; //  /<iframe.*?src="(.*?)"/;
		const getSrc = /<iframe.*?src=\s*(["'])(.*?)\1/.exec(val);
		if(getSrc){
			console.log(getSrc[2]);
			return getSrc[2];
		}
		return false;
	}
}

function getYouTubeId(url){
  const arr = url.split(/(vi\/|v%3D|v=|\/v\/|youtu\.be\/|\/embed\/\/?)/);
  return undefined !== arr[2] ? arr[2].split(/[^\w-]/i)[0] : arr[0];
}

const DdItem = ({ className, ...etc }) => (
  <button {...etc} className={Q.Cx("dropdown-item", className)} type="button" />
);

export default class PlayerQ extends Component{
  constructor(props){
    super(props);
    this.state = {
      ready: false, 
      playing: false, 
      played: 0, 
      // seeking: false, 
      duration: 0,
      loaded: 0, 
      loop: false, 
      volume: 0.5, // 0.8
      muted: false, 
      pip: false, 
      playbackRate: 1.0, 

      lastVol: 0, 
      size: null, // 360 
      // ccActive: false, 
      ccShow: "Off", // Selected track cc: null | "Off"
      ccFontSize: 100, 
      ccFontColor: {
        name:"White",
        hex:"#fff"
      },
      // defaultCC: null, 
      active: false, 
      ddSet: false, // Dropdown setting
      keyVol: false, 
      bezelPlay: null, 
      bezelCopy: false, 
      // vPoster: props.poster ? true : false
      mouseTime: {
        time: null,
        position: 0
      }, 
      isFullscreen: false, 
      tab: "menus", 
      openInfo: false, 
      autoPlayState: false, 
      oncePlay: false, // First play palyer if youtube src (DEVS for performance)
    };

    this.wrap = createRef();
    this.btnBeezelRef = createRef();
    this.idleTimer = null;
    this.timePos = 20;

    Q.bindFuncs.call(this, [
      'onFs','onFsError','onPlay','onPause','onPlayPause','onSeekMouseDown','onSeekMouseUp','onSeekChange',
      'onProgress','onDuration','onEnded','onVolumeChange','onMute','onBezelPlayPause','onBezelKeyDown',
      'onActiveCC','onSetCc','onCueChange','onReady','onActive','onIdle','onPip',
      'onCtxCopy','onToggleSet','onSetPlaybackRate','onSetSize','onMouseMoveRange','toFocusBezel', 
    ]);
  }

  componentDidMount(){
    // console.log('%ccomponentDidMount in PlayerQ','color:yellow;');
    if(screenfull.isEnabled){
      screenfull.on('change', this.onFs);
      screenfull.on('error', this.onFsError);
    }
  }

  // componentDidUpdate(prevProps){
  //   const { tracks } = this.props;

  //   if(tracks !== prevProps.tracks && tracks?.length > 0){
  //     let ply = this.getPlayerDOM();
  //     let trackActive = Q.domQ("track[default]", ply);
  //     console.log('componentDidUpdate PlayerQ tracks: ', tracks);
  //     // console.log('onReady this.state.played: ', this.state.played);// trackActive.label

  //     // this.setState({  });
  
  //     if(trackActive){
  //       // const ccShow = trackActive.label;
  //       this.track = trackActive;
  //       // this.defaultCC = trackActive.label;// ccShow;
  //       // this.track.addEventListener("cuechange", this.onCueChange);
  //       this.setState({ defaultCC: trackActive.label });// , ccActive: true
  //     }
  //   }
  // }

  componentWillUnmount(){
    if(screenfull.isEnabled){
      screenfull.off('change', this.onFs);
      screenfull.off('error', this.onFsError);
    }
    if(this.track){
      this.track.removeEventListener("cuechange", this.onCueChange);
    }
    // this.idleTimer = null;// OPTION
  }

  onFs(){
    this.setState({ isFullscreen: screenfull.isFullscreen });
  }

  onFsError(e){
    console.log('onFsError: ', e);
    this.setState({ fullscreenError: "Failed to enable fullscreen" });
  }

  onPlay(){
    this.setState({ playing: true });
  }

  onPause(){
    this.setState({ playing: false });
  }

  onPlayPause(){
    this.setState({ playing: !this.state.playing }); // this.setState(s => ({ playing: !s.playing }));
  }

  onSeekMouseDown(){
    // console.log('onSeekMouseDown');
    this.setState({ seeking: true });
  }

  onSeekMouseUp(e){
    let val = e.target.value;
    // this.player.seekTo(parseFloat(e.target.value));
    // setTimeout(() => {
    //   this.setState({ seeking: false });
    // }, 9);
    // console.log('onSeekMouseUp');
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(val));
  }

  onSeekChange(e){
    // let played = parseFloat(e.target.value);
    // this.player.seekTo(played);
    // this.setState({ played });
    this.setState({ played: parseFloat(e.target.value) })
  }

  onProgress(state){
    // console.log('onProgress state: ', state);
    // We only want to update time slider if we are not currently seeking
    // setTimeout(() => {
      // console.log('onProgress seeking: ', !this.state.seeking);
    // }, 1);

    if(!this.state.seeking){
      this.setState(state);// { ...this.state, ...state }
    }
  }

  onDuration(duration){
    // console.log('onDuration duration: ', duration);// this.state.
    if(duration >= 1000) this.timePos = 27;
    this.setState({ duration });
  }

  onEnded(){
    // console.log('onEnded');
    this.setState(s => ({ playing: s.loop, ccTxt: null }));
  }

// NOT FIX:
  onVolumeChange(e){
    let v = e.target.value;
    this.setState(s => ({ 
      // lastVol: s.volume, 
      volume: parseFloat(v) 
    }), () => {
      this.setState(s => ({ muted: s.volume === 0 }))
    });
  }

// NOT FIX:
  onMute(){
    // const { muted, volume, lastVol } = this.state;
    this.setState(s => ({ 
      // muted: !s.muted,
      lastVol: s.volume !== 0 ? s.volume : 0.5, 
      // volume: s.muted && s.volume === 0 ? s.lastVol : 0
    }), () => {
      this.setState(s => ({ 
        muted: !s.muted, 
        volume: s.muted && s.volume === 0 ? s.lastVol : 0 
      }))
    });
  }

  onBezelPlayPause(){
    this.onPlayPause();
    this.setState(s => ({ bezelPlay: "fa fa-" + (s.playing ? "pause" : "play") + "-circle " }));
    setTimeout(() => this.setState({ bezelPlay: null }), 500);

    if(!this.state.oncePlay) this.setState({ oncePlay: true });// First play if youtube src
  }

  onBezelKeyDown(e){
    if(BEZEL_KEYS.includes(e.key)){
      Q.preventQ(e);

      const { volume, keyVol } = this.state;
      let v;
      switch(e.key){
        case "k": // Play/Pause
          this.onBezelPlayPause();
          break;
        case "m": // Muted
          this.onMute();
          break;
        case "f": // Fullscreen
          screenfull.toggle(this.wrap.current);
          break;
        case "i": // Pip / Mini player
          this.onPip();
          break;
        case "ArrowRight":
          console.log("ArrowRight NEXT");
          break;
        case "ArrowLeft":
          console.log("ArrowLeft PREV");
          break;
        case "ArrowUp": // Volume Up
          v = volume + 0.05;
          if(v > 1) v = 1;
          this.setState({ volume: v });
          // Show range volume
          if(!keyVol) this.setState({ keyVol: true });
          break;
        case "ArrowDown":// Volume Down
          v = volume - 0.05;
          if(v < 0) v = 0;
          this.setState({ volume: v });
          // Show range volume
          if(!keyVol) this.setState({ keyVol: true });
          break;
        default: 
          return;
      }
    }
  }
  
  onActiveCC(){
    const { ccShow, defaultCC } = this.state;// ccActive, 
    // this.setState({ ccActive: !ccActive });
    // this.onSetCc(!ccActive ? ccShow : "Off");
    this.onSetCc(ccShow === "Off" ? defaultCC : "Off");

    console.log('ccShow: ', ccShow);
    console.log('defaultCC: ', defaultCC);
  }

  // setCc = v => {
  //   if(ccShow !== v) onSetCc(v);
  //   this.setState({ tab: "menus" });
  // }

  onSetCc(ccShow){
    if(this.track){
      // let isOff = ccShow === "Off";
      this.setState({ ccShow });// , ccActive: isOff

      if(ccShow === "Off"){
        this.setState({ ccTxt: null });
        this.track.removeEventListener("cuechange", this.onCueChange);
      }else{
        let ply = this.getPlayerDOM();
        let trackActive = Q.domQ("track[default]", ply);
        Q.setAttr(trackActive, "default");

        let trackActiveNow = Q.domQ(`track[label="${ccShow}"]`, ply);
        trackActiveNow.default = true;

        // console.log('trackActive: ', trackActive);
        this.track = trackActiveNow;
        this.track.addEventListener("cuechange", this.onCueChange);
      }
    }

    this.setState({ tab: "menus" });
  }

  setUrl(url){
    return Array.isArray(url) ? 
      url.map(v => ({ 
          ...v, 
          src: Q.newURL(v.src).href, 
          sizes: v.sizes ?  v.sizes + "px" : undefined
        })) 
      : 
      Q.newURL(url).href;
  }

  onCueChange(e){
    let cues = e.target.track.activeCues;
    let ccTxt = cues[0]?.text;
    // console.log('e: ', e);
    // console.log('cues: ', cues);
    // console.log('ccTxt: ', ccTxt);

    this.setState({ ccTxt });
  }

  onReady(p){
    const { tracks } = this.props;

    this.setState({ ready: true });

    if(tracks?.length > 0){
      let ply = p.getInternalPlayer();
      let trackActive = Q.domQ("track[default]", ply);
      // console.log('onReady p: ', p);
      // console.log('onReady trackActive: ', trackActive.label);
  
      if(trackActive){
        // const ccShow = trackActive.label;
        this.track = trackActive;
        // this.defaultCC = trackActive.label;// ccShow;
        // this.track.addEventListener("cuechange", this.onCueChange);
        this.setState({ defaultCC: trackActive.label });// , ccActive: true
      }
    }
  }

  // onAction = e => {
  //   let et = e?.target;
  //   console.log('onAction - user did something e: ', e);
  //   console.log('onAction target: ', et);
  // }
  onActive(){
    // let et = e?.target;
    // console.log('onActive - user is active e: ', e);
    // console.log('onActive - time remaining: ', this.idleTimer.getRemainingTime());
    // console.log('onActive target: ', et?.tagName);

    this.setState({ active: true  });
  }

  onIdle(){
    const { ddSet, keyVol } = this.state;
    // console.log('onIdle: ');
    if(!ddSet) this.setState({ active: false });
    if(keyVol) this.setState({ keyVol: false });
  }

  onPip(){
    this.setState(s => ({ pip: !s.pip }));
  }

  onCtxCopy(txt){
    // let id = window.location.href.split("/");
    // id[id.length - 1]
    clipboardCopy(txt).then(() => {
      this.setState({ bezelCopy: true });
      setTimeout(() => this.setState({ bezelCopy: false }), 500);
    }).catch(e => console.log(e));
  }

  // parseFileConfig(fileConfig){
  //   return fileConfig?.tracks ? { ...fileConfig, tracks: fileConfig.tracks.map(v => ({ ...v, src: Q.newURL(v.src).href })) } : fileConfig;
  // }

  parseCC(tracks){ // fileConfig
    return tracks ? ["Off", ...tracks.map(v => v.label)] : null;
  }

  onToggleSet(open){
    this.setState({ ddSet: open });
    if(!open && this.state.tab !== "menus") this.setState({ tab: "menus" });
  }

  onSetPlaybackRate(v){
    if(this.state.playbackRate !== v) this.setState({ playbackRate: v });
    this.setState({ tab: "menus" });
  }

  onSetSize(size){
    if(size !== this.state.size) this.setState({ size });
    this.setState({ tab: "menus" });
  }

  onMouseMoveRange(e){
    let x = e.pageX;
    if(!x) return;

    let et = e.target;
    const { duration } = this.state;
    let time = getPointerPosition(et, e).x * duration;
    let rect = findElPosition(et);
    let position = (x - rect.left) - this.timePos;

    let rectW = Math.round(rect.width) - this.timePos;

    if((position + this.timePos) >= rectW) position = rectW - this.timePos;
    if(position < 0) position = 0;

    this.setState({ mouseTime: { time, position } });
  }

  toFocusBezel(){
    this.btnBeezelRef.current.focus();
  }

  ref = p => {
    this.player = p
  }

  getPlayerDOM = () => {
    return this.player.getInternalPlayer();
  }

  render(){
    const { 
      // fileConfig
      reactPlayerClass, url, config, poster, tracks, 
      className, wrapStyle, tmode, enablePip, videoID, playbackRates, 
      theaterMode, onTheaterMode, autoPlay, onSetAutoPlay, 
    } = this.props;

    const { 
      ready, playing, played, duration, loaded, loop, volume, muted, playbackRate, pip, 
      tab, size, active, ddSet, keyVol, ccShow, ccFontSize, ccFontColor, ccTxt, bezelPlay, bezelCopy, 
      isFullscreen, fullscreenError, openInfo, mouseTime, autoPlayState, oncePlay, 
    } = this.state;// 

    const sizes = Array.isArray(url) ? url.map(v => v.sizes && Number(v.sizes)) : undefined;
    const cc = this.parseCC(tracks);// fileConfig
    const isYoutube = !oncePlay && Q.isStr(url) && validateExternalUrl(url);

    return (
      <InViewPort inRef={this.wrap}>
        {view => {
          // console.log('view: ', view);
          return (
            <ContextMenu 
              className="zi-5" 
              appendTo={this.wrap.current || document.body} 
              // NOT FIX: popper config
              popperConfig={{
                strategy: "fixed",
                // modifiers: [
                //   {
                //     name: 'preventOverflow',
                //     options: {
                //       // padding: 100, 
                //       // boundary: document.body, // this.wrap.current
                //       // altBoundary: true, // false by default
                //       // rootBoundary: 'document', // 'viewport' by default
                //       // mainAxis: false, // true by default
                //     }
                //   },
                // ]
              }} 
              // , ctxMenu
              component={(hide, _, active) => {
                if(!view && active){
                  hide();
                  // return null;
                }

                return (
                  <div className="dropdown-menu show v-dd-sets"
                    onContextMenu={(e) => {Q.preventQ(e);hide()}} // ctxMenu | Q.preventQ | (e) => {Q.preventQ(e);hide()}
                  >
                    <DdItem 
                      className={"q-mr far fa-" + (playing ? "pause":"play")} 
                      onClick={() => {
                        hide();
                        this.onBezelPlayPause();
                        this.toFocusBezel();
                      }} 
                    >
                      {playing ? "Pause":"Play"}
                    </DdItem>
  
                    <DdItem  
                      className={"q-mr far fa-" + (isFullscreen ? "compress":"expand")} 
                      onClick={() => {
                        hide();
                        screenfull.toggle(this.wrap.current);
                        this.toFocusBezel();
                      }}
                    >
                      {isFullscreen ? "Exit " : ""}Full screen
                    </DdItem>
  
                    {videoID && 
                      <Fragment>
                        <DdItem className="q-mr far fa-copy" 
                          onClick={() => {
                            hide();
                            this.onCtxCopy(Q.baseURL + "/" + videoID);
                            this.toFocusBezel();
                          }}
                        >
                          Copy video URL
                        </DdItem>
  
                        <DdItem className="q-mr far fa-copy" 
                          onClick={() => {
                            hide();
                            this.onCtxCopy(Q.baseURL + "/" + videoID + "?t=" + formatTime(duration * played, "."));
                            this.toFocusBezel();
                          }}
                        >
                          Copy video URL at current time
                        </DdItem>
  
                        <DdItem className="q-mr far fa-code" 
                          onClick={() => {
                            hide();
                            this.onCtxCopy(`<iframe width="716" height="403" src="${Q.baseURL}/embed/${videoID}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`);
                            this.toFocusBezel();
                          }} 
                        >
                          Copy embed code
                        </DdItem>
                      </Fragment>
                    }
                    <hr/>
                    <DdItem className="q-mr far fa-info-circle" 
                      onClick={() => {
                        hide();
                        this.setState({ openInfo: true });
                      }} 
                    >
                      Info
                    </DdItem>
                  </div>
                )
              }}
            >
              <div // role="application" // region
                ref={this.wrap} 
                className={
                  Q.Cx("embed-responsive embed-responsive-16by9 playerQ", {
                    "v-ready": ready, 
                    "v-playing": playing,
                    "v-active": active,
                    "v-pip": pip, 
                    "ddSetOpen": ddSet, 
                  }, className)
                } 
                style={wrapStyle} 
              >
                {ready && 
                  <IdleTimer
                    ref={r => { this.idleTimer = r }} 
                    element={this.wrap.current} 
                    timeout={3000} // 1000 * 60 * 15
                    debounce={250} 
                    startOnMount={false} 
                    onActive={this.onActive} 
                    onIdle={this.onIdle} 
                    // onAction={this.onAction} 
                  />
                }

                {/*
                Youtube video image thumbnail: 
                FROM: https://stackoverflow.com/questions/2068344/how-do-i-get-a-youtube-video-thumbnail-from-the-youtube-api

                OPTION URL:
                https://img.youtube.com/vi/VIDEO_ID/maxresdefault.jpg
                https://i3.ytimg.com/vi/VIDEO_ID/maxresdefault.jpg

                OPTION image name:
                0.jpg | 1.jpg | 2.jpg | 3.jpg | default.jpg | hqdefault.jpg | mqdefault.jpg | sddefault.jpg | maxresdefault.jpg
                */}

                {/* DEV OPTION: Custom poster for youtube API etc | poster && Q.isAbsoluteUrl(url) */}
                {isYoutube ? //  
                  <BgImage noRepeat 
                    className="position-absolute position-full zi-1 w-100 h-100 bg-000 bg-pos-center v-poster" 
                    // NOT FIX:
                    url={"https://i3.ytimg.com/vi/" + getYouTubeId(url) + "/hqdefault.jpg"} 
                    // bgSize={"auto " + ref.clientHeight + " px"}
                    onLoad={(url, ref, e) => {
                      // console.log("e: ", e.path[0].width);// naturalWidth
                      // console.log('ref.clientWidth: ', ref.clientWidth);
                      ref.style.backgroundSize = "auto " + ref.clientHeight + "px";// ref.clientWidth + "px " + ref.clientHeight + "px";
                    }}
                  />
                  : 
                  <ReactPlayer ref={this.ref} 
                    // controls 
                    className={reactPlayerClass} 
                    width="100%" 
                    height="100%" 
                    config={{
                      ...config, 
                      file:{
                        // ...fileConfig, 
                        tracks: tracks || [], 
                        // ...this.parseFileConfig(fileConfig), 
                        attributes:{
                          preload: poster ? "metadata" : "auto", // OPTION
                          // crossOrigin: "anonymous", // OPTION
                          tabIndex: "-1",
                          controlsList: "nodownload nofullscreen noremoteplayback", // 
                          playsInline: true, // OPTION
                          "webkit-playsinline": "", 
                          "x-webkit-airplay": "allow", 
                          poster
                        }
                      }
                    }} 
                    url={this.setUrl(url)} 
                    playing={playing} 
                    loop={loop} 
                    playbackRate={playbackRate} 
                    volume={volume} 
                    muted={muted} 
                    pip={pip} 
                    onReady={this.onReady} 
                    // onStart={() => console.log('onStart')} 
                    onError={e => console.log('onError: ', e)} 
                    onPlay={this.onPlay} 
                    onPause={this.onPause} 
                    onProgress={this.onProgress} 
                    onDuration={this.onDuration} 
                    onEnded={this.onEnded} 
                    // onBuffer={() => console.log('onBuffer')} 
                    // onBufferEnd={() => console.log('onBufferEnd')} 
                    onEnablePIP={(e) => {
                      console.log('onEnablePIP: ', e);
                      this.setState({ pip: true });
                    }}
                    onDisablePIP={(e) => {
                      console.log('onDisablePIP: ', e);
                      this.setState({ pip: false });
                    }}
                    // onSeek={(e) => {
                    //   console.log('onSeek e: ', e);
                    //   this.setState({ played });
                    // }}
                  />
                }

                <button ref={this.btnBeezelRef} type="button" 
                  className={
                    Q.Cx("btn rounded-0 shadow-none cauto w-100 text-primary fa-3x position-absolute position-full zi-1 v-bezel", {
                      "i-load": !isYoutube && !ready,
                      "playerQ-bezel-animate": bezelPlay || bezelCopy, 
                      "far fa-copy": bezelCopy,
                      ["" + bezelPlay]: bezelPlay
                    })
                  } 
                  style={isYoutube && ready ? undefined : { '--bg-i-load': '95px' }} 
                  onClick={this.onBezelPlayPause} 
                  onKeyDown={this.onBezelKeyDown}
                />

                <div className={Q.Cx("text-white zi-2 v-ctrl", { "keyVol": keyVol })}>
                  <div className="v-range" 
                    onMouseMove={this.onMouseMoveRange}
                  >
                    {mouseTime?.time > 0 && 
                      <time className="tooltip-inner small point-no v-time-tip"
                        style={{ left: mouseTime.position }}
                      >
                        {formatTime(mouseTime.time)}
                      </time>
                    }

                    <input type="range" min={0} max={0.999999} step="any" className="q-range rounded-pill" 
                      style={{ '--slider-val': (played * 100) + '%' }} 
                      value={played} 
                      onMouseDown={this.onSeekMouseDown} 
                      onMouseUp={this.onSeekMouseUp} 
                      onChange={this.onSeekChange} 
                    />

                    <div role="progressbar" className="q-progress rounded-pill point-no" aria-valuemin="0" aria-valuemax="100" 
                      aria-valuenow={loaded * 100} 
                      style={{ height:5, "--val": (loaded * 100) + "%" }}
                    />
                  </div>

                  <Flex align="center" className="mt-2 v-btn-ctrl">
                    <Btn size="sm" // kind="flat" 
                      aria-label={(playing ? "Pause" : "Play") + " (k)"} 
                      className={"mr-1 tip tipTL far fa-" + (playing ? "pause" : "play")} 
                      onClick={() => {
                        this.onPlayPause();
                        this.toFocusBezel();
                      }} 
                    />

                    <Flex align="center" className="mr-2 v-volume">
                      <Btn size="sm" 
                        className={
                          Q.Cx("px-1 tip tipT flexno q-s13 fa-fw far", {
                            "fa-volume": (volume * 10) >= 5, // volume > 0, // 
                            "fa-volume-down": (volume * 10) < 5, // volume.toFixed(1) < 0.5
                            "fa-volume-mute": muted || volume === 0, // slash
                          })
                        } 
                        aria-label={(muted || volume === 0 ? "Unmute" : "Mute") + " (m)"} 
                        onClick={() => {
                          this.onMute();
                          this.toFocusBezel();
                        }}
                      />

                      <input type="range" min={0} max={1} step="any" className="q-range rounded-pill ml-1" 
                        style={{ '--slider-val': (volume * 100) + '%' }} 
                        value={volume} 
                        onChange={this.onVolumeChange} 
                      />
                    </Flex>

                    <small className="mr-auto v-time">
                      {formatTime(duration * played)} / {formatTime(duration)}
                    </small>

                    {cc?.length > 0 && 
                      <Btn size="sm" className="tip tipT q-s12 far fa-closed-captioning" 
                        active={ccShow !== "Off"} 
                        aria-label="Subtitles/closed captions" 
                        onClick={() => {
                          this.onActiveCC();
                          this.toFocusBezel();
                        }}
                      />
                    }

                    <Dropdown alignRight drop="up" 
                      show={ddSet} 
                      onToggle={this.onToggleSet} 
                    >
                      <Dropdown.Toggle size="sm" bsPrefix="tip tipT q-s11 far fa-cog v-btn-set" aria-label="Settings" />
                      <Dropdown.Menu flip={false} className="v-dd-sets">
                        <Tab.Container id="playerQ-tabSets" 
                          activeKey={tab} 
                          onSelect={tab => this.setState({ tab })} 
                        >
                          <Tab.Content>
                            <Tab.Pane eventKey="menus">
                              <Flex As="label" justify="between" align="center" className="dropdown-item">
                                Autoplay 
                                <div className="custom-control custom-switch d-block ml-5">
                                  <input type="checkbox" className="custom-control-input" 
                                    checked={Q.isBool(autoPlay) ? autoPlay : autoPlayState} 
                                    onChange={e => {
                                      let checked = e.target.checked;
                                      Q.isBool(autoPlay) ? onSetAutoPlay(checked) : this.setState({ autoPlayState: checked });
                                    }}
                                  />
                                  <div className="custom-control-label" />
                                </div>
                              </Flex>

                              <Flex As="label" justify="between" align="center" className="dropdown-item">
                                Annotations 
                                <div className="custom-control custom-switch d-block ml-5">
                                  <input type="checkbox" className="custom-control-input" />
                                  <div className="custom-control-label" />
                                </div>
                              </Flex>
                              
                              <Flex justify="between" align="center" className="dropdown-item" role="button" 
                                onClick={() => this.setState({ tab: "playbackSpeed" })} 
                              >
                                Playback speed 
                                <div className="ml-5 faa faa-chevron-right q-ml">{playbackRate === 1.0 ? "Normal" : playbackRate}</div>
                              </Flex>
                              
                              {cc?.length > 0 && 
                                <Flex justify="between" align="center" className="dropdown-item" role="button" 
                                  onClick={() => this.setState({ tab: "cc" })} 
                                >
                                  Subtitles/CC 
                                  <div className="ml-5 faa faa-chevron-right q-ml">{ccShow}</div>
                                </Flex>
                              }

                              {size && 
                                <Flex justify="between" align="center" className="dropdown-item" role="button" 
                                  onClick={() => this.setState({ tab: "quality" })} 
                                >
                                  Quality 
                                  <div className="ml-5">{size}p</div>
                                </Flex>
                              }
                            </Tab.Pane>

                            <Tab.Pane eventKey="playbackSpeed">
                              <DdItem onClick={() => this.setState({ tab: "menus" })} className="far fa-chevron-left q-mr">
                                Playback speed
                              </DdItem>
                              <hr />

                              {playbackRates.map((v, i) => 
                                <DdItem key={i} 
                                  className={playbackRate === v ? "fal fa-check q-mr" : "pl-36px"} 
                                  onClick={() => this.onSetPlaybackRate(v)} 
                                >
                                  {v === 1.0 ? "Normal" : v}
                                </DdItem>
                              )}
                            </Tab.Pane>

                            {cc?.length > 0 && 
                              <Fragment>
                                <Tab.Pane eventKey="cc">
                                  <Flex justify="between" className="py-1 px-3">
                                    <Btn onClick={() => this.setState({ tab: "menus" })} 
                                      outline kind="dark" size="xs" 
                                      className="fw600 far fa-chevron-left q-mr"
                                    >
                                      Subtitles/CC 
                                    </Btn>

                                    <Btn onClick={() => this.setState({ tab: "ccOptions" })} outline kind="dark" size="xs" className="fw600">Options</Btn>
                                  </Flex>
                                  <hr className="dd-hr" />
                                  
                                  {cc.map((v, i) => 
                                    <DdItem key={i} 
                                      className={ccShow === v ? "fal fa-check q-mr" : "pl-36px"} 
                                      onClick={() => this.onSetCc(v)} 
                                    >
                                      {v}
                                    </DdItem>
                                  )}
                                </Tab.Pane>

                                <Tab.Pane eventKey="ccOptions">
                                  <DdItem onClick={() => this.setState({ tab: "cc" })} className="far fa-chevron-left q-mr">
                                    Options
                                  </DdItem>
                                  <hr />

                                  {[
                                    // {name:'Font family'},
                                    {name:"Font color", use:ccFontColor.name}, 
                                    {name:"Font size", use:ccFontSize + "%"}
                                  ].map(v => 
                                    <Flex key={v.name} justify="between" align="center" className="dropdown-item" role="button"
                                      onClick={() => this.setState({ tab: v.name })} 
                                    >
                                      {v.name} 
                                      <div className="ml-5 faa faa-chevron-right q-ml">{v.use}</div>
                                    </Flex>
                                  )}
                                </Tab.Pane>

                                {/* <Tab.Pane eventKey="Font family">
                                  <button onClick={() => this.setState({ tab: "ccOptions" })} className="dropdown-item far fa-chevron-left q-mr" type="button">
                                    Font family
                                  </button>
                                  <hr />
                                </Tab.Pane> */}

                                <Tab.Pane eventKey="Font color">
                                  <DdItem onClick={() => this.setState({ tab: "ccOptions" })} className="far fa-chevron-left q-mr">
                                    Font color
                                  </DdItem>
                                  <hr />

                                  {CC_FONT_COLORS.map(v => 
                                    <DdItem key={v.name} 
                                      className={ccFontColor.hex === v.hex ? "fal fa-check q-mr" : "pl-36px"} 
                                      onClick={() => this.setState({ ccFontColor: v })} 
                                    >
                                      {v.name}
                                    </DdItem>
                                  )}
                                </Tab.Pane>

                                <Tab.Pane eventKey="Font size">
                                  <DdItem onClick={() => this.setState({ tab: "ccOptions" })} className="far fa-chevron-left q-mr">
                                    Font size
                                  </DdItem>
                                  <hr />

                                  {CC_FONT_SIZES.map((v, i) => 
                                    <DdItem key={i} 
                                      className={ccFontSize === v ? "fal fa-check q-mr" : "pl-36px"} 
                                      onClick={() => this.setState({ ccFontSize: v })} 
                                    >
                                      {v}%
                                    </DdItem>
                                  )}
                                </Tab.Pane>
                              </Fragment>
                            }

                            <Tab.Pane eventKey="quality">
                              <DdItem onClick={() => this.setState({ tab: "menus" })} className="far fa-chevron-left q-mr">
                                Quality
                              </DdItem>
                              <hr />
                              
                              {sizes?.length > 0 && sizes.map((v, i) => 
                                <DdItem key={i} 
                                  className={size === v ? "fal fa-check q-mr" : "pl-36px"} 
                                  onClick={() => this.onSetSize(v)} 
                                >
                                  {v}p
                                </DdItem>
                              )}
                            </Tab.Pane>
                          </Tab.Content>
                        </Tab.Container>
                      </Dropdown.Menu>
                    </Dropdown>

                    {/*  && ReactPlayer.canEnablePIP(url) */}
                    {(enablePip && !isFullscreen) && 
                      <Btn size="sm" className="tip tipT far fa-tv-alt" 
                        aria-label="Mini player (i)" 
                        onClick={() => {
                          this.onPip();
                          this.toFocusBezel();
                        }} 
                      />
                    }

                    {(theaterMode && !isFullscreen) && 
                      <Btn size="sm" className="tip tipTR q-s11 far fa-film" 
                        aria-label={(tmode ? "Default view" : "Theater mode") + " (t)"} 
                        onClick={() => {
                          this.toFocusBezel();
                          onTheaterMode();
                        }} 
                      />
                    }

                    {(screenfull.isEnabled && !fullscreenError) && 
                      <Btn size="sm" // className="tip tipTR far fa-expand" 
                        className={"tip tipTR q-s11 far fa-" + (isFullscreen ? "compress" : "expand")} 
                        aria-label={(isFullscreen ? "Exit " : "") + "Full screen (f)"}
                        onClick={() => {
                          screenfull.toggle(this.wrap.current);
                          this.toFocusBezel();
                        }} // toggleFull
                      />
                    }
                  </Flex>
                </div>

                {ccTxt && 
                  <div className="position-absolute l0 r0 mx-auto text-center v-cc"
                    style={{ 
                      fontSize: ccFontSize !== 100 ? ccFontSize + "%" : undefined, 
                      color: ccFontColor.name !== "White" ? ccFontColor.hex : undefined
                    }}
                  >
                    <span>{ccTxt}</span>
                  </div>
                }

                {(openInfo || fullscreenError) && // OPTION
                  <Flex justify="center" align="center" className="position-absolute position-full h-100 zi-3 v-info" aria-live="polite" aria-atomic>
                    <div className="toast show">
                      <div className="toast-header">
                        <strong className="mr-auto fa fa-info-circle q-mr">Q Player</strong>
                        <Btn onClick={() => this.setState({ openInfo: false })} close className="mb-1" aria-label="Close">&times;</Btn>
                      </div>
                      <div className="toast-body">
                        {fullscreenError}
                      </div>
                    </div>
                  </Flex>
                }
              </div>
            </ContextMenu>
          )
        }}
      </InViewPort>
    );
  }
}

PlayerQ.defaultProps = {
  playbackRates: [0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2], 
  onTheaterMode: () => {}, 
  onSetAutoPlay: () => {}, 
}; 

