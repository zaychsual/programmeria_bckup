import React, { createContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

// Q-CUSTOM:
// import Cookie from '../utils/cookie';

const AuthContext = createContext();
const { Provider } = AuthContext;

const AuthProvider = ({ children }) => {
  const history = useHistory();

  // const access_token = Cookie.get('access_token');// localStorage.getItem('token');
  const user = localStorage.getItem('user');
  const expired = localStorage.getItem('expired');// expiresAt

  const [authState, setAuthState] = useState({
    // access_token,
    expired,
    user: user && user !== "undefined" ? JSON.parse(user) : null
  });

  const setAuthInfo = ({ user, expired }) => { // access_token, 
    // Cookie.set('access_token', access_token, { path: "/" });// access_token | token
    
    // localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('expired', expired);

    setAuthState({
      // access_token, 
      user, 
      expired
    });
  };

  const logout = () => {
    // post | get
		axios.post('/api/logout')
    .then(r => {
      console.log(r);// .data

      // Cookie.remove('access_token'); // access_token | token
      // localStorage.removeItem('token');
      
      // localStorage.removeItem('user');
      // localStorage.removeItem('expired');
      ["user","expired"].forEach(v => localStorage.removeItem(v));
  
      setAuthState({});
      history.replace('/login');// push
    }).catch(e => console.log(e));
  };

  // Q-CUSTOM:
  const localAuth = () => {
    if(authState.user) return authState;
  }

  const isAuthenticated = async () => {
    // if (!authState.token || !authState.expiresAt) {
    //   return false;
    // }
    // return (
    //   new Date().getTime() / 1000 < authState.expiresAt
    // );

    // if(!authState.access_token || !authState.user){ // !authState.token
    //   return false;
    // }

    // !Cookie.get('access_token') || 
    // if(!localStorage.getItem('user')){
    //   return false;
    // }

    // return authState;

    // axios.post('/api/islogin')
    // .then(r => {
    //   console.log(r);// .data

    //   if(r.status === 200){
    //     if(r.data.err) return false;
    //     if(r.data.value) return authState;
    //     return false;
    //   }
    //   return false;
    // }).catch(e => {
    //   console.log(e);
    //   return false;
    // });

    try{
      const res = await axios.post('/api/islogin');
      // console.log('res: ', res);
      // console.log('res.status: ', res.status);
      // console.log('res.data.value: ', res.data);

      if(res.status === 200){
        // if(res.data.err) return false;
        if(res.data.value) return authState;
        return false;
      }
      return false;
    }catch(e){
      console.log(e);
      return false;
    }
  };

  const isAdmin = () => {
    return authState?.user?.level === 2;// authState.user.role === 'admin'
  };

  return (
    <Provider
      value={{
        authState,
        setAuthState: authInfo => setAuthInfo(authInfo),
        logout,
        isAuthenticated, 
        localAuth, // Q-CUSTOM
        isAdmin
      }}
    >
      {children}
    </Provider>
  );
};

export { AuthContext, AuthProvider };
