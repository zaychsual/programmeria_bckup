import React, { useEffect, useRef, useState } from 'react';
// import { useMediaQuery } from 'react-responsive';
// import { useLocation } from 'react-router-dom';// , useHistory, useRouteMatch
import Dropdown from 'react-bootstrap/Dropdown';

// import Popup from 'reactjs-popup';
// import 'reactjs-popup/dist/index.css';

import Btn from '../../components/q-ui-react/Btn';
import InputGroup from '../../components/q-ui-react/InputGroup';
import Aroute from '../../components/q-ui-react/Aroute';
import ModalQ from '../../components/q-ui-react/ModalQ';

export default function AsideAdmin({
  isMobile, 
  basename = "", 
  headText, 
  asideMin = false, 
  menus = [], 
  onToggleAside, //  = () => {}
}){
  // const isMobile = useMediaQuery({ query: '(max-width: 767px)' });// (max-width: 1224px)
  const asideMenuCol = useRef(null);
  const [ddMenuOpen, setDdMenuOpen] = useState(false);

  const scrollMenu = (dd, behavior = "smooth") => {
    setTimeout(() => {
      let rect = dd.getBoundingClientRect();
      asideMenuCol.current.scrollBy({
        top: rect.y - rect.height, 
        left: 0,
        behavior
      });
    }, 1);
  }

  // console.log('isMobile: ', isMobile);

	useEffect(() => {
    // console.log('%cuseEffect in AsideMain location: ','color:yellow;');
    const path = window.location.pathname;

    // console.log('path: ', path);
    // console.log('basename: ', basename);
    // console.log('IS ROOT: ', (path === basename || (path.endsWith("/") || path === basename + "/"))); // path !== basename || path !== basename + "/"

    if(!(path === basename || (path.endsWith("/") || path === basename + "/"))){
      const findActive = menus.findIndex(v => v.menus && v.menus.find(v2 => basename + v2.to === path || v2.href === path));
      // console.log('findActive: ', findActive);

      if(findActive > -1){
        // console.log('is findActive: ', findActive);

        setDdMenuOpen(findActive);
        setTimeout(() => {
          const dd = Q.domQ('.menu-dd.show');
          if(dd){
            let ahref = Q.domQall('a.menu-item', dd);
            ahref.forEach(a => {
              // console.log('with a native: ', a);
              if(a.href === window.location.href){
                // console.log('with a native if: ', a);
                Q.setClass(a, 'active');
              }
            });

            scrollMenu(dd, "auto");
          }
        }, 9);
      }
      else{
        // console.log('No findActive: ', findActive);

        let menuCol = Q.domQ(`.aside-menu-col a[href="${window.location.pathname}"]`); // Q.domQ('.aside-menu-col');
        if(menuCol){
          Q.setClass(menuCol, 'active');
          // console.log('menuCol OK: ');
        }
      }
    }
  }, [menus]);
  
  // useEffect(() => {
  //   window.addEventListener('keyup', (e) => {
  //     // console.log(ddMenuOpen);
  //     if(e.key === "Escape"){// ddMenuOpen && 
  //       console.log(e.key);
  //       e.preventDefault();
  //       e.stopPropagation();
  //       setDdMenuOpen(ddMenuOpen);
  //     }
  //   })
  // }, []);

  const onScrollAsideMenuCol = (e) => {
    e.stopPropagation();

    // let ok = Q.hasClass(e.target, "aside-menu-col");
    // console.log('ok: ', ok);

    if(Q.hasClass(e.target, "aside-menu-col") && asideMin && !isMobile && ddMenuOpen){
      let dd = document.activeElement;
      if(Q.getAttr(dd, 'aria-expanded') === 'true'){
        dd.click();
        dd.blur();
      }
    }
  }

  const onToggleMenuDropdown = (open, e, i) => {
    setDdMenuOpen(open ? i : false);

    if(!asideMin && open && e){
      let dd = e.target.parentElement;
      if(dd) scrollMenu(dd);
    }
  }

  const onClickDropMenuItem = e => {
    if(asideMin && !isMobile){
      let dd = Q.domQ('[aria-expanded]', e.target.closest('.dropright'));
      if(dd) dd.click();
    }
    if(isMobile){
      onToggleAside();
    }
  }

  const renderSearchMenu = () => {
    const inDrawer = asideMin && !isMobile;
    // bg-strip
    return (
      <InputGroup 
        // className={"input-group mb-0 p-2 bg-light" + (!isMobile ? " input-group-sm border-bottom position-sticky":"")} 
        size={!isMobile ? "sm" : null} 
        className={"p-2 bg-strip" + (!isMobile ? " border-bottom position-sticky" : "")} 
        append={
          <Btn As="div" kind="light" tabIndex="0" 
            className={"qi qi-search" + (inDrawer ? " rounded":"")} 
            onClick={inDrawer ? onToggleAside : undefined}
          />
        }
      >
        <input type="text" placeholder="Search" 
          className="form-control" 
          hidden={inDrawer} 
          disabled={inDrawer} 
        />
      </InputGroup>
    );
  }

  const renderMenu = () => {
    const isMin = asideMin && !isMobile;
    return (
      <div ref={asideMenuCol} 
        // dd-menu-open scroll-hide
        // className={"nav flex-column flex-nowrap py-1 border-top-next ovyscroll q-scroll aside-menu-col" + (asideMin ? " scroll-hide":"")}
        className={
          Q.Cx("nav flex-column flex-nowrap py-1 border-top-next ovyscroll q-scroll aside-menu-col", {
            "scroll-hide": asideMin,
            "position-absolute position-full": isMobile
          })
        }
        onScroll={onScrollAsideMenuCol}
      >
        {menus.map((v, i) => {
          if(v.menus){
            return (
              <Dropdown key={i} 
                show={ddMenuOpen === i} 
                drop={isMin ? "right":"down"} 
                className="menu-dd" // qi 
                onToggle={(open, e) => onToggleMenuDropdown(open, e, i)}
                // onMouseEnter={e => {
                //   console.log('onMouseEnter e: ',e);
                //   let dd = e.target.querySelector('[aria-expanded]');
                //   if(dd) dd.click();
                // }}
                // onMouseLeave={e => {
                //   console.log('onMouseLeave e: ',e);
                //   let dd = e.target.querySelector('[aria-expanded]');
                //   if(dd) dd.click();
                // }}
              >
                <Dropdown.Toggle 
                  as="button" 
                  type="button" 
                  // bsPrefix={"menu-item dd-toggle-menu list-group-item list-group-item-action border-0 rounded-0 shadow-none q-mr fal fa-" + v.icon} 
                  //  zi-1001
                  bsPrefix={"menu-item nav-link btn w-100 rounded-0 shadow-none q-mr dd-toggle-menu qia q-fw qi qi-" + v.icon}
                >
                  {v.label}
                </Dropdown.Toggle>

                <Dropdown.Menu 
                  flip={isMin} // false
                  // Prevent close with click outside, null, "mousedown"
                  rootCloseEvent={isMin ? "mousedown" : null} 
                  className={"px-2 qi dd-menu-asidemain" + (isMin ? " ovyauto q-scroll scroll-fff":"")} 
                  popperConfig={{
                    strategy: "fixed", // asideMin ? "fixed":"relative", // "relative", // fixed
                    modifiers: [{
                      name: "offset",
                      options: {
                        offset: isMin ? [0, -0.05] : [0, 0]  
                      }
                    }]
                  }}
                >
                  {(v.menus && v.menus.length) && v.menus.map((v2, i2) => 
                    React.createElement(
                      v2.to ? Aroute : v2.href ? "a" : "button", 
                      {
                        key: i2,
                        // dropdown: Boolean(v.to), 
                        // closeDropdown: asideMin,
                        as: !asideMin && (v2.to || v2.href) ? null : "button",
                        type: v2.to || v2.href ? null : "button",
                        to: v2.to,
                        href: v2.href, // v2.icon ? "q-mr fal fa-" + v2.icon : undefined
                        className: "dropdown-item rounded text-truncate menu-item" + (v2.icon ? " q-mr q-fw qi qi-" + v2.icon : ""), 
                        title: v2.label, 
                        onClick: onClickDropMenuItem
                      }, v2.label)
                    )
                  }
                </Dropdown.Menu>
              </Dropdown>
            );
          }

          return React.createElement(
            v.to ? Aroute : "a", 
            {
              key: i,
              to: v.to, 
              href: v.href,
              exact: v.exact, 
              strict: v.strict,
              className: "menu-item nav-link q-mr q-fw qi qi-" + v.icon,
              onClick: isMobile ? onToggleAside : undefined
          }, v.label);
        })}
      </div>
    );    
  }

  if(isMobile){
    return (
      <ModalQ 
        // backdrop="static" 
        // keyboard={false} 
        unmountOnClose={false} // OPTION
        open={asideMin} 
        size="sm" // sm | lg | xl
        toggle={onToggleAside} 
        // scrollable 
        className="asideModalApp" // mx-0 | modal-full-width 
        contentClassName="w-256px vh-100" // {modalPosition === "modal-full" ? "vh-100" : undefined} 

        // path="#modal-hash" 
        position="modal-left" // {modalPosition} 
        close={false}  
        // headClass="headClass" 
        head={
          <div className="modal-header p-0 border-0">
            {renderSearchMenu()}
            
            <Btn onClick={onToggleAside} kind="fff" size="lg" className="h-100 qi qi-close btnToogleAsideMain" aria-label="Close"  />
            {/* <button onClick={onToggleAside}  type="button" className="close" aria-label="Close">&times;</button> */}
          </div>
        }
        bodyClass="p-0" 
        body={renderMenu()} 
        // foot={
        // 	<Btn onClick={toggleModal}>Close</Btn>
        // }
      />
    );
  }

  //  pancake-stack
  return (
    <aside className="flexno border-right shadow-sm" id="asideAdmin">
      <header className={"bg-main d-flex align-items-center p-2 sticky-top" + (asideMin ? " justify-content-center":"")}>
        <Aroute to="/" className="navbar-brand mr-0 py-1">
          <img width="28" height="28" // loading="lazy" 
            className="d-inline-block flexno" // align-top 
            alt="Programmeria" 
            src="/icon/android-icon-36x36.png" 
          />

          {(!asideMin && headText) && <strong className="ml-2">{headText}</strong>}
        </Aroute>

      </header>

      {renderSearchMenu()}

      <div className="asidemain-menu-wrap">
        {renderMenu()}
      </div>

      <div className="p-2 border-top asidemain-foot">
        <Btn kind="info" size="sm" className="qi qi-cog" />
      </div>
    </aside>
  );
}
