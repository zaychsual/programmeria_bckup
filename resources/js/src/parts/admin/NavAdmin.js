import React from 'react';// , { useEffect, useState }
import { useFullscreen } from 'ahooks';
import Dropdown from 'react-bootstrap/Dropdown';

import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import Flex from '../../components/q-ui-react/Flex';
import InputGroup from '../../components/q-ui-react/InputGroup';

export default function NavAdmin({
  isMobile, 
  asideMin, 
  onToggleAside, //  = () => {}
}){
  const [isFullscreen, { toggleFull }] = useFullscreen(document.documentElement);// setFull, exitFull, 

  return (
    <nav id="navAdmin" 
      className="navbar navbar-expand-lg navbar-light bg-main py-1 pl-0 shadow-sm border-bottom sticky-top" 
    >
      {/* <a href="/" className="navbar-brand py-1">
        <img 
          src="/icon/programmeria-36x36.png" 
          width="28" height="28" 
          className="d-inline-block" // align-top 
          alt="Programmeria" 
          loading="lazy" 
        />
      </a> */}

      <Btn As="div" 
        size="sm" 
        outline 
        className={"pl-5px rounded-circle round-left-0 mr-lg-2 qi qi-chevron-2-" + (isMobile || asideMin ? "right":"left")} 
        // className="pl-5px rounded-circle round-left-0 mr-lg-2 fal fa-chevron-double-left btnToggleAsideMain" 
        onClick={(e) => {
          let dd = Q.domQ('.dd-toggle-menu[aria-expanded="true"]');
          if(dd){
            dd.click();
            dd.blur();
          }
          document.body.classList.toggle('aside-min');
          onToggleAside(e);
        }}
      />

      <div className="collapse navbar-collapse" id="navAdminCollapse">
        <div className="navbar-nav link-sm ml-1-next-lg">
          {/*  className="py-1" */}
          <Aroute exact strict nav to="/" className="qi qi-home" />
          <Aroute nav to="/settings">Settings</Aroute>
        </div>
      </div>

      <Flex className="ml-1-next">
        {/* NOT FIX: must disabled popper js */}
        <Dropdown alignRight drop="down">
          <Dropdown.Toggle bsPrefix="tip tipB qi qi-browser" size="sm" variant="light" aria-label="Open App" />
          <Dropdown.Menu flip={false}>
            <Dropdown.Item href="/">Open App</Dropdown.Item>
            <Dropdown.Item href="/" target="_blank" rel="noopener">Open App New Tab</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <Btn size="sm" kind="light" id="btnToggleFullscreenPage" 
          className={"tip tipB qi qi-fullscreen" + (isFullscreen ? "-exit" : "")} 
          aria-label={(isFullscreen ? "Exit " : "") + "Full screen"} 
          onClick={toggleFull} 
        />

        <InputGroup 
          size="sm"  
          className="w-auto" 
          append={
            <Btn As="div" outline className="qi qi-search" tabIndex="0" />
          }
        >
          <input type="search" // text
            className="form-control" 
            placeholder="Search" 
          />
        </InputGroup>
      </Flex>
    </nav>
  );
}