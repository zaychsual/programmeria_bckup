import React, { useState } from 'react';// { useState, useEffect, Fragment }
import { Route } from 'react-router-dom';
import Downshift from 'downshift';
import axios from 'axios';

import ModalQ from '../components/q-ui-react/ModalQ';
import Btn from '../components/q-ui-react/Btn';
import InputGroup from '../components/q-ui-react/InputGroup';
import Flex from '../components/q-ui-react/Flex';
import Aroute from '../components/q-ui-react/Aroute';
import Ava from '../components/q-ui-react/Ava';

export default function RouteSearch({ history }){// , children
	const [inputVal, setInputVal] = useState("");
  const [finds, setFinds] = useState([]);
	const [findsError, setFindsError] = useState(null);

	// !(history.location.pathname === "/search")
	if(!history.location.pathname.includes('/search')) return null;
	
	/* useEffect(() => {
		console.log('%cuseEffect in RouteSearch','color:yellow;');
		// &callback=test
    axios.get("https://api.themoviedb.org/3/movie/100196?api_key=1b5adf76a72a13bad99b8fc0c68cb085").then(r => {
			console.log('r: ',r);
			if(r.status === 200){
				console.log('if r: ',r)
			}
			else{
				console.log('else r: ', r)
			}
    }).catch(e => {
			console.log(e);
		});
	}, []); */

  const onXhr = (val) => {
    const api = `https://api.themoviedb.org/3/search/movie?api_key=1b5adf76a72a13bad99b8fc0c68cb085&query=${val}`;
    axios.get(api).then(r => {
			console.log('r: ',r);
			if(r.status === 200){
				setFinds(r.data.results);
				if(findsError) setFindsError(null);
			}
			else{
				setFindsError("Error");
			}
    }).catch(e => {
			console.log(e);
			setFindsError(e);
		});
  }
	
	const onFind = (e) => {
		let val = e.target.value;
		// let vtrim = val ? val.trim() : "";
		
		// console.log('onFind vtrim: ', vtrim);
		
    // if(("Enter Escape Shift Control Alt ArrowUp ArrowDown ArrowRight ArrowLeft".includes(e.key)) || vtrim.length < 1){
      // return;
    // }
		
		setInputVal(val);
		
		if(val.length < 1){
			setFinds([]);
		}
		
    setTimeout(() => onXhr(val.trim()), 300);
  }
	
	// const onChangeInput = e => {
		// let val = e.target.value;
		
		// setInputVal(val);
		
		// if(val.length < 1){
			// setFinds([]);
		// }
	// }
	
  const downshiftOnChange = (select) => {
    console.log('downshiftOnChange select: ', select);// `your favourite movie is ${select.title}`
		if(select){
			setInputVal(select.title);
			setFinds([]);
		}
  }

  const back = () => history.goBack();

  return (
    <Route path="/search">
      <ModalQ 
        backdrop="static" 
        keyboard={false} 
				autoFocus={false} 
        // unmountOnClose={false} // OPTION
        open={true} // history.location.pathname === "/search" | true 
        toggle={back} 
        scrollable 
				returnFocusAfterClose={false} 
        // className="asideModalApp" // mx-0 | modal-full-width 
        contentClassName="vh-100" 

        position="modal-full" 
        close={false}  
        // headClass="headClass" 
        // head={
          // <InputGroup 
            // As="div" 
            // size="lg" 
            // noValidate 
            // prepend={
              // <Btn onClick={back} kind="fff" className="px-2 rounded-0 qi qi-arrow-left" aria-label="Back" />
            // }
          // >
            // <input autoFocus type="text" placeholder="Search" 
              // className="form-control rounded-0"  
              // onChange={onFind} 
            // />
          // </InputGroup>
        // }
        bodyClass="p-0" 
        body={					
					<Downshift 
						// onStateChange={} 
						onChange={downshiftOnChange}
						itemToString={item => (item ? item.title : "")}
					>
						{({
							selectedItem,
							getInputProps,
							getItemProps,
							highlightedIndex,
							isOpen,
							// inputValue,
							clearSelection, 
							// getLabelProps
						}) => (
							<div className="findMain">								
								<InputGroup size="lg" 
									// As="div"
									prepend={
										<Btn As="div" tabIndex="0" onClick={back} kind="fff" className="px-2 rounded-0 qi qi-arrow-left" aria-label="Back" />
									}
									append={
										inputVal?.length > 0 && 
											<Btn As="div" tabIndex="0" kind="fff" className="rounded-0 qi qi-close" aria-label="Clear" 
												onClick={() => {
													clearSelection();
													setInputVal("");
													setFinds([]);
												}}
											/>
									}
								>
									<input 
										{...getInputProps({
											// id: "findInputMain", 
											type: "text", 
											autoFocus: true, 
											className: "form-control rounded-0", 
											placeholder: "Search", 
											value: inputVal, 
											onChange: onFind,
											// onBlur: e => setInputVal(e.target.value) // Q.preventQ
										})}
										
										// autoFocus 
										// value={inputVal} 
										// onChange={onFind} // onChangeInput
										// onKeyUp={onFind} // onKeyDown | onKeyUp
									/>
								</InputGroup>
								
								{((isOpen || finds?.length > 0) && !findsError) && 
									<div className="nav flex-column py-3 empty-hide">
										{/* <h4>Search Results: {inputValue}</h4> */}
										
										{
											finds.filter(item => !inputVal || item.title.toLowerCase().includes(inputVal.toLowerCase()))
												.slice(0, 10)
												.map((item, index) => (
													<Aroute replace nav 
														to={"/article/" + item.id + "/" + Q.str2slug(item.title)} 
														className={
															Q.Cx("btn text-left d-flex align-items-center", {
																"active": highlightedIndex === index,
																"bold": selectedItem === item
															})
														} 
														
														{...getItemProps({ key: index, index, item })}
														
														// style={{
															// backgroundColor: highlightedIndex === index ? "lightgray" : "white",
															// fontWeight: selectedItem === item ? "bold" : "normal"
														// }}
														onClick={() => {
															
														}}
													>
														{item.poster_path ? 
															<Ava round 
																wrapClass="flexno mr-3" 
																w="40" 
																h="40"  
																className="of-cov" 
																alt={item.title} 
																src={"https://image.tmdb.org/t/p/w220_and_h330_face/" + item.poster_path} 
															/> 
															: 
															<i className="qi qi-film q-2x q-fw py-1 bg-light border rounded mr-3" />
														}
														
														{item.title}
													</Aroute>
												))
										}
									</div>
								}
								
								{(!selectedItem && inputVal?.length > 0 && finds?.length < 1) && 
									<Flex align="center" justify="center" grow="1" style={{ height: 'calc(100vh - 48px)' }}>
										Not Found
									</Flex>
								}
								
								{findsError && 
									<Flex align="center" justify="center" grow="1" style={{ height: 'calc(100vh - 48px)' }}>
										{findsError.toString()}
										{/* Error */}
									</Flex>
								}
							</div>
						)}
					</Downshift>
        }
      />
    </Route>
  );
}

/*
<div className="modal-header p-0 border-0">
</div>

<React.Fragment></React.Fragment>
*/
