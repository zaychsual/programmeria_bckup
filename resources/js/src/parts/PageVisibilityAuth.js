import React, { useContext, useEffect } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useDocumentVisibility } from 'ahooks';

import { AuthContext } from '../context/AuthContext';
// import Btn from '../../components/q-ui-react/Btn';

export default function PageVisibilityAuth({
	children
}){
	const auth = useContext(AuthContext);
	const docVisibility = useDocumentVisibility();
  // const [data, setData] = useState();
	
	/* const redirect = (e) => {
		if(e) e.stopPropagation();
		if(document.hasFocus() && !auth?.isAuthenticated()){
			window.location.replace("/");
		}
	} */
	
	/* useEffect(() => {
		redirect();
		// console.log('%cuseEffect in PageVisibilityAuth hasFocus: ','color:yellow;', document.hasFocus());
		window.addEventListener("focus", redirect, false);
		window.removeEventListener("blur", redirect, false);
	}, []); */
	
	useEffect(() => {
		// console.log('%cuseEffect in PageVisibilityAuth hasFocus: ','color:yellow;', document.hasFocus());
		// console.log('%cuseEffect in PageVisibilityAuth activeElement: ','color:yellow;', document.activeElement);

		if(docVisibility === "visible" && !auth?.isAuthenticated()){
			// console.log('NO AUTH');
			// history.replace("/");
			// setNoLog(true);
			window.location.replace("/");
		}
		
		// axios.get('/api/getauth')
    // .then(r => {
    //   console.log(r);// .data
    // }).catch(e => console.log(e));
	}, [docVisibility, auth]);
	
	if(!auth?.isAuthenticated()) return null;

	return children;
}

/*
<React.Fragment>

</React.Fragment>
*/
