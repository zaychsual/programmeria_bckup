import React, { useState, useContext, useEffect } from 'react';// useRef, 
import { useHistory } from 'react-router-dom';// , Route
import Dropdown from 'react-bootstrap/Dropdown';

import { AuthContext } from '../../context/AuthContext';

import Aroute from '../../components/q-ui-react/Aroute';
import Flex from '../../components/q-ui-react/Flex';
import Btn from '../../components/q-ui-react/Btn';
import InputGroup from '../../components/q-ui-react/InputGroup';
import ModalQ from '../../components/q-ui-react/ModalQ';
import ModalSetApp from '../ModalSetApp';
// import Cookie from '../../utils/cookie';
import { APP_NAME } from '../../data/appData';// , APP_LANG

const APP_MENUS = [
	{txt:"Home", to:"/", icon:"home", exact:true, strict:true},
	{txt:"About", to:"/about"},
	{txt:"Projects", to:"/projects"},
	// {txt:"Laravel", to:"/laravel"}, 
];

const NavContent = ({ isLogin, auth, toggle, align, children, dir = "column", btnSize = "sm" }) => (
	<Flex dir={dir} align={align} className="ml-1-next-lg">
		{/* auth.isAuthenticated() */}
		{/* <Aroute to="/login" size="sm" btn="danger">Login</Aroute> */}
		{isLogin ? 
			<Dropdown>
				<Dropdown.Toggle variant="outline-primary" size="sm" title="Settings" />
				<Dropdown.Menu alignRight>
					{auth.isAdmin() && <Dropdown.Item href="/admin" target="_blank" rel="noopener">Admin</Dropdown.Item>}
					
					<Dropdown.Item onClick={toggle} as="button" type="button">Setting</Dropdown.Item>
					<Dropdown.Item as="button" type="button" 
						onClick={() => {
							auth.logout();
							bcApi.postMessage("logout");
						}} 
					>Logout</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
			: 
			!auth.localAuth() && 
				<div className={"mb-p-lp-2 btn-group btn-group-" + btnSize}>
					<Aroute to="/login" btn="light">Login</Aroute>
					<Aroute to="/register" btn="primary">Register</Aroute>
				</div>
		}

		{children}
	</Flex>
);

export default function NavApp({
	isLogin, 
	isMobile, 
	onClickSearch, //  = () => {} 
}){
	const auth = useContext(AuthContext);
	const history = useHistory();
	const [openMenu, setMenu] = useState(false);
	const [openSet, setOpenSet] = useState(false);
	// const [log, setLog] = useState(false);
	// const [loadAuth, setLoadAuth] = useState(false);

	const loc = history.location;

	// DEV: history API toggle Menu & Modal Settings
	useEffect(() => {
		// console.log('%cuseEffect in NavApp','color:yellow;');
		setMenu(loc.hash === "#menu");
		setOpenSet(loc.hash === "#settings");
	}, [loc]);

	// useEffect(() => {
	// 	// const isLogin = async () => await auth.isAuthenticated();
	// 	// console.log('isLogin: ', isLogin());
	// 	auth.isAuthenticated().then(v => {
	// 		// console.log(v);
	// 		setLog(v);
	// 		setLoadAuth(true);
	// 	}).catch(e => console.log(e));
	// }, [auth]);
 
	const toggleMenu = (open) => {
		if(Q.isBool(open)){
			open ? history.push(loc.pathname + "#menu") : history.goBack();
		}
	};

	const toggleSetting = () => {
		// setOpenSet(!openSet);
		openSet ? history.goBack() : history.push(loc.pathname + "#settings");
	};

	const renderLinks = () => 
		APP_MENUS.map((v, i) => 
			<Aroute key={i} nav 
				exact={v.exact} 
				strict={v.strict} 
				replace={isMobile && openMenu} 
				to={v.to} 
				className={v.icon && "qi qi-" + v.icon} 
				title={v.txt} 
				onClick={isMobile ? toggleMenu : undefined}
			>
				{!v.icon ? v.txt : null}
			</Aroute>
		);

	return (
		<nav className="navbar navbar-expand-lg navbar-light bg-main py-1 shadow-sm border-bottom h-48px sticky-top" id="navApp">
			<a href="/" className="navbar-brand">
				<img width="28" height="28" loading="lazy" alt={APP_NAME} src="/icon/android-icon-36x36.png" />
			</a>

			{isMobile ? 
				<React.Fragment>
					<Btn onClick={onClickSearch} outline className="d-lg-none ml-auto q-fw qi qi-search" />

					<Btn outline // kind="light" 
						className="d-lg-none ml-1 q-fw qi qi-bars" 
						aria-label="Toggle navigation" 
						onClick={() => toggleMenu(true)} // toggleMenu | setMenu
					/>

					<ModalQ 
						// backdrop="static" 
						keyboard={false} 
						unmountOnClose={false} // OPTION
						open={openMenu} 
						size="sm" // sm | lg | xl
						toggle={() => toggleMenu(!openMenu)} // toggleMenu | setMenu
						// scrollable 
						// className="asideModalApp" // mx-0 | modal-full-width 
						contentClassName="w-256px vh-100" // {modalPosition === "modal-full" ? "vh-100" : undefined} 

						position="modal-left" // {modalPosition} 
						close={false}  
						// headClass="headClass" 
						// head={
						// 	<div className="modal-header p-0 border-0">								
						// 		<Btn // toggleMenu | setMenu
						// 			onClick={() => toggleMenu(false)} // !openMenu 
						// 			kind="fff" 
						// 			size="lg" 
						// 			className="h-100 qi qi-close btnToogleAsideMain" 
						// 			aria-label="Close" 
						// 		/>
						// 	</div>
						// }
						bodyClass="p-0" 
						body={
							<>
								<Btn // toggleMenu | setMenu
									onClick={() => toggleMenu(false)} // !openMenu 
									kind="fff" 
									size="lg" 
									className="position-absolute t0 qi qi-close btnToogleAsideMain" 
									aria-label="Close" 
								/>

								<NavContent 
									// load={loadAuth} 
									isLogin={isLogin} // log
									auth={auth} 
									toggle={toggleSetting} 
									btnSize="lg"
								>
									<div className="h-full-nav pb-3 ovyauto q-scroll">
										{renderLinks()}
									</div>
								</NavContent>
							</>
						} 
						// foot={
						// 	<Btn onClick={toggleModal}>Close</Btn>
						// }
					/>
				</React.Fragment>
				: 
				<div className="collapse navbar-collapse" id="navAppCollapse">
					<div className="navbar-nav mr-auto link-sm ml-1-next-lg">
						{renderLinks()}
					</div>

					<NavContent 
						// load={loadAuth} 
						isLogin={isLogin} // log
						auth={auth} 
						toggle={toggleSetting} 
						dir="row" 
						align="center"
					>
						<InputGroup 
							// role="form" 
							size="sm" 
							append={
								<Btn As="div" kind="light" className="qi qi-search" tabIndex="0" />
							}
						>
							<input type="text" placeholder="Search" 
								className="form-control"  
								
							/>
						</InputGroup>
					</NavContent>
				</div>
			}

			{/** @NOTE: Set to context */}
			<ModalSetApp 
				open={openSet} 
				toggle={toggleSetting} 
			/>

			{/* <button onClick={handleButtonClick} className="btn btn-info" type="button">Test Notif</button> */}
		</nav>
	);
}

/*

*/
