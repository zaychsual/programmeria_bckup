import React, { useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';// MediaQuery

// import Btn from '../../components/q-ui-bootstrap/Btn';

export default function Mq({ children }){ // sm, 
	/* useEffect(() => {
		// console.log('useEffect in Mq SM_DEVICE: ',SM_DEVICE);
		if(sm) Q.setClass(document.body, "isMobile");
	}, [sm]);// OPTION */

	// matches will be true or false based on the value for the media query
	const onChange = (matches) => {
		// console.log('onChange matches: ', matches);
		document.body.classList.toggle("isMobile", matches);
		// if(matches) Q.setClass(document.body, "isMobile");
		
		// DEV OPTION: Set class & data- to DOM
		setTimeout(() => {
			Q_appData.UA = bowser.parse(navigator.userAgent);
			// console.log('Q_appData: ', Q_appData);
			
			Q.setUpDOM();
		}, 1);
		
		// let UA = bowser.parse(navigator.userAgent);
		
		// Q.setClass(document.documentElement, UA.platform.type + " " + UA.os.name + " " + UA.browser.name);
		
		// [UA.platform.type, UA.os.name, UA.browser.name].forEach(v => document.documentElement.classList.toggle(v));
		
		// let cls = [];
		// document.documentElement.classList.forEach((v, i) => {
			// if(i > 0) document.documentElement.classList.remove(v);// cls.push(v);
		// });
		
		// [UA.platform.type, UA.os.name, UA.browser.name].forEach(v => document.documentElement.classList.toggle(v));

		// Q.setAttr(document.documentElement, {
			// "data-os-v": UA.os.versionName || UA.os.version,
			// "data-browser-v": UA.browser.version
		// });
	}

	// maxDeviceWidth
	const isMobile = useMediaQuery(
    { maxWidth: 767 }, undefined,  onChange
  );

	// children && children(isMobile);
  return children(isMobile);// { isLarge, isMobile }
}

/*

*/
