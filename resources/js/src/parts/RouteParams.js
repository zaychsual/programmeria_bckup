import React from 'react';// { useState, useEffect }
import { Switch, Route } from 'react-router-dom';// useParams
// import Btn from '../../components/q-ui-bootstrap/Btn';

export default function RouteParams({ 
	path, 
	...etc 
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
		// console.log('%cuseEffect in RouteParams','color:yellow;');
	// }, []);

	return (
		<Switch>
			<Route {...etc} path={path + "/:id"} />
		</Switch>
	);
}

/*
<React.Fragment></React.Fragment>
*/
