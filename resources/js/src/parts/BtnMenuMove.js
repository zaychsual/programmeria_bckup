import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import Draggable from 'react-draggable';

import Btn from '../components/q-ui-react/Btn';

const bodyWidth = document.body.clientWidth;
const bounds = {
	top: -screen.height, 
	left: -(bodyWidth - 230),
	right: bodyWidth,
	bottom: 572
};

export default function BtnMenuMove(){
  // const [w, setW] = useState(null);
	
	// useEffect(() => {
	// 	const wrapApp = Q.domQ("#wrapApp");
	// 	console.log('%cuseEffect in BtnMenuMove','color:yellow;',wrapApp);
		
	// 	if(wrapApp){
	// 		setW(wrapApp.clientWidth + 210);
	// 	}
	// }, []);

	// if(!w) return null;

	return (
		<Draggable 
			// bounds="parent" 
			bounds={bounds} 

			// bounds={{top: -100, left: -100, right: 100, bottom: 100}}

			// axis="x" 
			// handle=".handle" 
			// offsetParent={Q.domQ("#wrapApp")} 
			defaultPosition={{
				x: 230, // bodyWidth - 200, // 36 
				y: screen.height - 140 // 75
			}} 
			position={null} 
			// grid={[25, 25]} 
			// scale={1} 
			// onStart={this.handleStart}
			// onDrag={this.handleDrag}
			// onStop={this.handleStop}
		>
			<Btn size="sm" className="position-fixed far fa-arrows btnMenuMove" title="Open menu" />
		</Draggable>
	);
}

/*
<React.Fragment></React.Fragment>
*/
