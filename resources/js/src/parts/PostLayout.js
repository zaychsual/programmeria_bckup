import React, { Fragment } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

import Head from '../components/q-ui-react/Head';
import Flex from '../components/q-ui-react/Flex';
import Placeholder from '../components/q-ui-react/Placeholder';

export default function PostLayout({
	className, 
	// colMediaClass, 
	data, 
	rowStyle, 
	append, 
	children, 
}){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in PostLayout','color:yellow;');
	// }, []);

// pb-3
//  + (tmode ? 12 : 8)
	return (
		<div className={Q.Cx("container pt-0 pt-md-3", className)}>
			<div style={rowStyle} className="row mx-lg-n2 position-relative">
				{data ? 
					<Fragment>
						<Head title={data.title} />
						
						{children}
					</Fragment>
					: 
					<Fragment>
						<div className="px-2 col-md-8">
							<Placeholder type="thumb" 
								h={370} // {screen.height / 2} // "55vh" 
								// className="card-img-top"
								// label="Post" 
							/>
							
							<Placeholder type="h5" className="w-50 my-3" />
							<Flex>
								<Placeholder w="40%" />
								<Placeholder className="flex1 ml-5" />
							</Flex>
						</div>
						
						<div className="px-2 col-md-4 mt-1-next">
							<Placeholder type="h3" className="pb-1" />
						
							{[1,2,3].map(v => 
								<div key={v} className="media media-post">
									<Placeholder w={120} h={65} className="mr-2" />
									
									<div className="media-body lh-normal">
										<Placeholder type="h6" className="mb-1" />
										<Placeholder className="mb-0" />
									</div>
								</div>
							)}
						</div>
					</Fragment>
				}
			</div>

			{append}
		</div>
	);
}

/*
<div className={Q.Cx("px-0 px-md-2", colMediaClass)}>

</div>
<React.Fragment></React.Fragment>
*/
