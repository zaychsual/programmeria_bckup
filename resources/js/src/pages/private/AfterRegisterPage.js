import React, { useContext } from 'react';// { useState, useEffect }
import { Redirect } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';

export default function AfterRegisterPage(){
	const auth = useContext(AuthContext);
  // const [isLog, setIsLog] = useState(auth.isAuthenticated());
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in AfterRegisterPage','color:yellow;');
	// }, []);

	if(auth.isAuthenticated()) return <Redirect to="/" />;

	return (
		<Flex dir="column" justify="center" align="center" className="h-full-nav">
			<h1>Register Success</h1>

			<Aroute to="/login" btn="primary">Login</Aroute>
		</Flex>
	);
}

/*
<React.Fragment></React.Fragment>
*/
