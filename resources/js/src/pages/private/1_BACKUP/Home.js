import React, { useState, useEffect, Fragment } from 'react';// useState, useEffect, useRef
import axios from 'axios';

// Default theme. ~960B
// import '@vime/core/themes/default.css';

// Optional light theme (extends default). ~400B
// import '@vime/core/themes/light.css';

// import { VimePlayer, VimeVideo, VimeDefaultUi } from '@vime/react';// VimeUi

import Head from '../../components/q-ui-react/Head';

import Placeholder from '../../components/q-ui-react/Placeholder';
import Flex from '../../components/q-ui-react/Flex';
import Img from '../../components/q-ui-react/Img';// Img | Ava
import Aroute from '../../components/q-ui-react/Aroute';
import Time from '../../components/q-ui-react/Time';
import NotifApi from '../../components/q-ui-react/NotifApi';

const cardLoders = () => {
	return Array.from({length: 6}).map((v, i) => 
		<div key={i} className="col-md-4 mb-4">
			<div className="card">
				<Placeholder type="thumb" h={190} className="card-img-top"
					// label="Post" 
				/>
				
				<div className="card-body">
					<Placeholder type="h5" className="card-title" />

					<Placeholder className="card-text" />
				</div>
			</div>
		</div>
	)
}

const LINKS = [
	{label:"Web Technologies", to:"/web-technologies"}, 
	{label:"Web Tools", to:"/web-tools"}, 
	{label:"Frameworks", to:"/frameworks"}
];

// const randColor = () => Math.floor(Math.random() * 16777215).toString(16);

export default function Home(){
	const [data, setData] = useState([]);
	const [ignore, setIgnore] = useState(true);
	const [title, setTitle] = useState("");
	const [options, setOptions] = useState({});

	const [audio, setAudio] = useState(null);

	useEffect(() => {
		console.log('%cuseEffect in Home','color:yellow;');

		axios.get('/DUMMY/json/articles.json').then(r => {
			setData(r.data);
		}).catch(e => console.log(e));
	}, []);

  const handlePermissionGranted = () => {
    console.log('Permission Granted');
    setIgnore(false);
	}
	
  const handlePermissionDenied = () => {
    console.log('Permission Denied');
    setTitle(true);
	}
	
  const handleNotSupported = () => {
    console.log('Web Notification not Supported');
    setIgnore(true);
  }

  const handleNotificationOnClick = (e, tag) => {
    console.log(e, 'Notification clicked tag:' + tag);
  }

  const handleNotificationOnError = (e, tag) => {
    console.log(e, 'Notification error tag:' + tag);
  }

  const handleNotificationOnClose = (e, tag) => {
    console.log(e, 'Notification closed tag:' + tag);
  }

  const handleNotificationOnShow = (e, tag) => {
    playSound();
    console.log(e, 'Notification shown tag:' + tag);
  }

  const playSound = () => {
		// document.getElementById('sound').play();
		if(audio){
			audio.play();
		}else{
			const sound = new Audio('/media/bell.mp3');
			setAudio(sound);
			console.log(sound);
			if(sound) sound.play();
		}
  }

  const handleButtonClick = () => {
    if(ignore) {
      return;
    }

    const now = Date.now();

    const title = 'React-Web-Notification' + now;
    const body = 'Hello' + new Date();
    const tag = now;
    // const icon = 'http://mobilusoss.github.io/react-web-notification/example/Notifications_button_24.png';
    // const icon = 'http://localhost:3000/Notifications_button_24.png';

    // Available options
    // See https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification
    const opts = {
      tag: tag,
      body: body,
      // icon: icon,
      lang: 'en',
			dir: 'ltr',
			silent: true,
      // sound: './sound.mp3'  // no browsers supported https://developer.mozilla.org/en/docs/Web/API/notification/sound#Browser_compatibility
		}
		
		setTitle(title);
		setOptions(opts);

    // this.setState({
    //   title: title,
    //   options: options
    // });
  }

  // const handleButtonClick2 = () => {
  //   this.props.swRegistration.getNotifications({}).then(function(notifications) {
  //     console.log(notifications);
  //   });
  // }

	return (
		<Fragment>
			<Head title="Home" />

			<div className="jumbotron jumbotron-fluid bg-strip text-center">
				<h1 className="txt-outline text-white font-weight-bold">Programmeria</h1>

				<Flex justify="center" className="mt-4 ml-2-next">
					{LINKS.map((v, i) => <Aroute key={i} to={v.to} outline btn="primary">{v.label}</Aroute>)}
				</Flex>
			</div>

			<button onClick={handleButtonClick} className="btn btn-info" type="button">Notif!</button>
      
			<NotifApi 
				ignore={ignore && title !== ''}
				notSupported={handleNotSupported}
				onPermissionGranted={handlePermissionGranted}
				onPermissionDenied={handlePermissionDenied}
				onShow={handleNotificationOnShow}
				onClick={handleNotificationOnClick}
				onClose={handleNotificationOnClose}
				onError={handleNotificationOnError}
				timeout={5000}
				title={title}
				options={options}
				// swRegistration={swRegistration} 
			/>
			
			<div className="container py-3">
				<div className="row">
					{data.length > 0 ? 
						data.map((v, i) => 
							<div key={i} className="col-md-4 mb-4">
								<div className="card h-100 post-1">
									{v.img ? 
										<Img frame 
											// w={350} 
											h={190} 
											alt={v.title} src={v.img} className="card-img-top bg-light of-cov" 
											// onLoad={e => {
											// 	console.log('onLoad img');
											// 	let et = e.target;

											// }}
										/> 
										: 
										<Placeholder type="thumb" className="card-img-top no-animate cauto" 
											h={190} 
											label={v.title} 
											// style={{ color: '#' + randColor() }}
										/>
									}

									{/* <Ava // frame 
										// w={350} 
										h={190} alt={v.title} src={v.img} className="card-img-top bg-light of-cov" 
										// onLoad={e => {
										// 	console.log('onLoad img');
										// 	let et = e.target;

										// }}
									/>  */}

									{/* <time className="badge badge-info ml-3 shadow-sm zi-1">27 July 2020</time> */}
									<Time 
										datetime={v.created_at} // "2020-08-08 08:08:08" 
										// locale="zh_CN" 
										className="badge badge-info ml-3 shadow-sm zi-1"
									/>
									
									<div className="card-body">
										<Aroute to={"/article/" + v.id} noLine kind="secondary" 
											className="h6 card-title truncate-line max-2 stretched-link" 
											title={v.title} 
										>
											{v.title}
										</Aroute>
										
										<p className="card-text small truncate-line">{v.desc}</p>{/* {v.desc.substring(0, 78)}... */}

										{/* <a href="/" className="btn btn-sm btn-primary stretched-link">Read</a> */}
									</div>
								</div>
							</div>
						)
						: 
						cardLoders()
					}
				</div>

			</div>

			{/* {['p','h1','h2','h3','h4','h5','h6'].map((v, i) => 
				React.createElement(v, { key: i, className: "px-3 bg-warning text-white" }, v)
			)} */}

		</Fragment>
	);
}

/*
<React.Fragment></React.Fragment>
*/
