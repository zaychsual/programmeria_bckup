import React from 'react';

import Head from '../../components/q-ui-react/Head';

// DEVS Components:
import Btn from '../../components/q-ui-react/Btn';
import BgImage from '../../components/q-ui-react/BgImage';
import Ava from '../../components/q-ui-react/Ava';
import SpinLoader from '../../components/q-ui-react/SpinLoader';
import InputGroup from '../../components/q-ui-react/InputGroup';
import ScrollTo from '../../components/q-ui-react/ScrollTo';

import PageLoader from '../../components/PageLoader';
import NumberField from '../../components/devs/NumberField';
// END DEVS Components:

export default function Devs(){
	const [pageLoad, setPageLoad] = React.useState(false);
	const [progress, setProgress] = React.useState(0);
	const [numVal, setNumVal] = React.useState("");
	const [inputNum, setInputNum] = React.useState([""]);
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in Devs','color:yellow;');
	// }, []);

	return (
		<div className="container-fluid py-3">
			<Head title="Devs" />

			{/* <hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/><hr/> */}

			{/* <h5>ScrollTo</h5> */}
			<ScrollTo 
				className="position-fixed zi-2 tip tipL fal fa-arrow-up" 
				style={{ bottom:9, right:9 }} 
				aria-label="Scroll To" 
				onClick={(scroll, e) => {
					console.log('onClick scroll: ', scroll);
					// console.log('onClick e: ', e);
				}}
			/>
			
			<hr/>

			<h5>InputGroup</h5>
			<InputGroup 
				className="mb-3" 
				prepend={
					<div className="input-group-text">Rp. </div>
				}
			>
				<input type="text" className="form-control" />
			</InputGroup>

			<InputGroup 
				className="mb-3" 
				append={
					<Btn As="div" kind="light" className="fal fa-search" />
				}
			>
				<input type="text" className="form-control" />
			</InputGroup>

			<InputGroup 
				className="mb-3" 
				prepend={
					<>
						<span className="input-group-text">*</span>
						<span className="input-group-text">Rp. </span>
					</>
				}
			>
				<input type="text" className="form-control" />
			</InputGroup>

			<InputGroup
				prepend={
					<span className="input-group-text">First and last name</span>
				}
			>
				<input type="text" className="form-control" />
				<input type="text" className="form-control" />
			</InputGroup>

			<hr/>

			<h5>NumberField</h5>
			<label className="d-block">
				Number: 
				<NumberField className="form-control" value={numVal} onChange={val => setNumVal(val)} />
			</label>
			
			<label className="d-block">
				Default input type="number"
				<input className="form-control" type="number" />
			</label>

			<hr/>

			<h5>progress</h5>
			{/* <progress 
				className="q-progress progress-txt" // animate
				// dir="ltr" 
				max="100" 
				value={progress} 
				aria-label={progress + "%"} 
				style={{ "--val": progress + "%" }}
			/> */}

			<div 
				className="q-progress mb-2" 
				role="progressbar" 
				aria-valuemin="0" 
				aria-valuemax="100" 
				aria-valuenow={progress} 
				aria-label={progress + "%"} 
				style={{ "--val": progress + "%" }}
			/>

			<input type="range" className="custom-range cpoin" // q-range
				// style={{ "--val": "50%" }}
				min="0" 
				// max="5"
				value={progress} 
				onChange={e => setProgress(e.target.value)}
			/>
			<hr/>
			
			<h5>{`<SpinLoader />`}</h5>
			<SpinLoader 

			/>

			<hr/>

			<h5>{`<PageLoader />`}</h5>
			<Btn onClick={() => {
				setPageLoad(true);
				setTimeout(() => setPageLoad(false), 10000000);
			}}>Try Load Page</Btn>

			{pageLoad && 
				<PageLoader 
					progress 
					// top 
					bottom 
					w={Q.UA.platform.type === "mobile" ? null : "calc(100% - 220px)"} 
					left={Q.UA.platform.type === "mobile"} 
					className="bg-white" 
				>
					
				</PageLoader>
			}

			<hr/>
			
			<h5>{`<BgImage />`}</h5>
			<BgImage 
				noRepeat 
				bgSize="contain" // auto | contain | cover | revert 
				className="bg-light mb-3" 
				url="/img/iron_man.jpg" 
				w={"55vw"} // 715 
				h={400} 
				// style={{width:200, height:200}} 
				// onLoad={(url, ref, e) => {
				//   console.log('onLoad BgImage url: ', url);
				//   // console.log('onLoad BgImage ref: ', ref);
				//   // console.log('onLoad BgImage e: ', e);
				// }}
				// onError={(url, ref, e) => {
				//   console.log('onError BgImage url: ', url);
				//   // console.log('onError BgImage ref: ', ref);
				//   // console.log('onError BgImage e: ', e);
				// }}
			/>

			<h6>Error background image</h6>
			<BgImage 
				noRepeat 
				As="section" 
				// bgSize="auto" // auto | contain | cover | revert 
				className="mt-2 bg-light" 
				url="/img-not-available.png" 
				w={100} 
				h={100} 
				// style={{width:200, height:200}} 
				// onLoad={(url, e) => {
				//   console.log('onLoad BgImage url: ', url);
				//   // console.log('onLoad BgImage e: ', e);
				// }}
				// onError={(url, e) => {
				//   console.log('onError BgImage url: ', url);
				//   // console.log('onError BgImage e: ', e);
				// }}
			/>

			<hr/>

			<h5>{`<Ava />`}</h5>
			<div className="ml-2-next">
				{[
					{src:"/img/avatar_man.png",alt:"Avatar Man", circle:true},
					{src:"/img/no_img.png",alt:"Brad Pitt", rounded:true}
				].map((v, i) => 
					<Ava key={i} 
						// thumb 
						rounded={v.rounded} 
						// fluid 
						circle={v.circle} 
						src={v.src} 
						alt={v.alt}
						w={70} // null
						h={70} 
					/>
				)}
			</div>

			<hr/>

			<h5>Ellipsis</h5>
			<div className="d-inline-flex align-items-center text-truncate text-muted far fa-envelope q-mr">
				<input readOnly 
					className="form-control form-control-sm h-auto p-0 bg-transparent text-muted border-0 rounded-0 shadow-none" 
					title="muhamad.husein@email.com" 
					defaultValue="muhamad.husein@email.com" 
					// onFocus={e => {
					// 	e.target.select();
					// }}
				/>
			</div>

			{/* <br/> */}

			{/* d-inline-grid grid-1fr-1 */}
			{/* <div className="col-2 px-0">
				<div className="ellipsis d-inline-flex align-items-center flex-nowrap text-truncate far fa-user-circle q-mr">
					<div className="small text-truncate">muhamad.husein@finansys.com</div>
				</div>
			</div> */}

			<hr/>

			<h5>Input Number</h5>
			<div className="input-group w-auto">
				{inputNum.map((v, i) => 
					<input key={i} className="form-control flexno w-auto" type="number" 
						value={v} 
						onChange={e => {
							let et = e.target;
							let val = et.value;
							console.log('val: ',val.length);
							
							// setInputNum([val]);

							if(val.length === 4){
								setInputNum([val[0], val.slice(1)]);
								setTimeout(() => et.nextElementSibling.focus(), 9);
							}
							else{
								setInputNum([val]);
							}
						}}
					/>
				)}
			</div>

			{/* Array.from({length:30}).map((_, i) => 
				<p key={i}>Dummy main content {i + 1}</p>
			) */}
		</div>
	);
}

/*
<React.Fragment>
	Devs
</React.Fragment>
*/
