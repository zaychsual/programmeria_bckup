import React from 'react';// useState, useEffect, useRef

// Default theme. ~960B
// import '@vime/core/themes/default.css';

// Optional light theme (extends default). ~400B
// import '@vime/core/themes/light.css';

// import { VimePlayer, VimeVideo, VimeDefaultUi } from '@vime/react';// VimeUi

import Head from '../../components/q-ui-react/Head';
// import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';
// import CheckRadioGroupQ from '../../components/q-ui-react/CheckRadioGroupQ';

// const RADIOS = [
// 	{text:"Pickup", value:"pickup"},
// 	{text:"Home Delivery", value:"home delivery"},
// 	{text:"Dine in", value:"dine in", disabled:true},
// 	{text:"COD", value:"cod"},
// ];

export default function Home(){
	// const [checked, setChecked] = useState(null);

	// useEffect(() => {
	// 	console.log('%cuseEffect in Home','color:yellow;');

	// 	// Q.getScript({src:"/js/libs/FileSaver.min.js","data-js":"FileSaver"})
	// 	// 	.then(v => {
	// 	// 		console.log('OK v: ',v);
	// 	// 	}).catch(e => console.log('ERROR e: ',e));
	// }, []);

	return (
		<div className="container-fluid py-3">
			<Head title="Home" />

			<h1>Home</h1>
			<hr/>

			{/* <VimePlayer>
				<VimeVideo>
					<source data-src="/media/video_360.mp4" type="video/mp4" />
					 <track
						default
						kind="subtitles"
						src="/media/subs/en.vtt"
						srcLang="en"
						label="English"
					/>
				</VimeVideo>
				
				<VimeDefaultUi />
			</VimePlayer> */}

			{/* <h5>Radio</h5>
			<CheckRadioGroupQ 
				labelledby="radioGroup-1"
			>
				{RADIOS.map((v, i) => 
					<CheckRadioQ 
						key={i} 
						// As="b" 
						type="radio" // role
						checked={v.value === checked} 
						value={v.value} 
						disabled={v.disabled} 
						// tabIndex={v.disabled ? "-1" : "0"} // "0" 
						// onClick={() => setChecked(v.value)} 
						onChange={(e) => {
							console.log(e);
							setChecked(e.target.value); // v.value
						}} 
					>
						{v.text}
					</CheckRadioQ>
				)}
			</CheckRadioGroupQ>

			<hr/>

			{[
				{txt:"React", value:"react", checked:true},
				{txt:"Angular", value:"angular", disabled:true},
				{txt:"Vue", value:"vue", indeterminate:true}
			].map((v) => 
				<CheckRadioQ 
					key={v.txt}
					value={v.value} 
					checked={v.checked} 
					indeterminate={v.indeterminate} 
					disabled={v.disabled} 
					// tabIndex="0"
				>
					{v.txt}
				</CheckRadioQ>
			)} */}
			
			<hr/>

			<form noValidate
				onSubmit={e => {
					e.preventDefault();
					e.stopPropagation();

					console.log(e.nativeEvent);
				}}
			>
				<div className="form-group">
					<label htmlFor="exampleInputEmail1">Email address</label>
					<input
						type="email"
						className="form-control"
						id="exampleInputEmail1"
						aria-describedby="emailHelp"
					/>
					<small id="emailHelp" className="form-text text-muted">
						We'll never share your email with anyone else.
					</small>
				</div>
				<div className="form-group">
					<label htmlFor="exampleInputPassword1">Password</label>
					<input
						type="password"
						className="form-control"
						id="exampleInputPassword1"
					/>
				</div>
				<div className="form-group form-check">
					<input type="checkbox" className="form-check-input" id="exampleCheck1" required />
					<label className="form-check-label" htmlFor="exampleCheck1">
						Check me out
					</label>
				</div>
				<button type="submit" className="btn btn-primary">
					Submit
				</button>
			</form>
		</div>
	);
}

/*
<div 
	className="ml-1-next"
	role="radiogroup" 
	// aria-labelledby="group_label_2" 
	// id="rg2"
>
	<h3 id="group_label_2">Pizza Delivery</h3>
	{RADIOS.map((v, i) => 
		<CheckRadioQ 
			key={i} 
			role="radio" 
			checked={v.value === checked} 
			value={v.value} 
			disabled={v.disabled} 
			tabIndex={v.disabled ? "-1" : "0"} // "0" 
			onClick={() => setChecked(v.value)} 
		>
			{v.text}
		</CheckRadioQ>
	)}
</div>

<React.Fragment></React.Fragment>
*/
