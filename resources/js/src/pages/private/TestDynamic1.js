import React from 'react';// { useState, useEffect }

import Head from '../../components/q-ui-react/Head';

export default function TestDynamic1(){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ComponentName','color:yellow;');
	// }, []);

	return (
		<section>
			<Head title="Test Dynamic 1" />
		
			<h1>TestDynamic1</h1>
		</section>
	);
}

/*
<React.Fragment></React.Fragment>
*/
