import React, { useContext, useState, useEffect } from 'react';// useEffect, Fragment
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
import axios from 'axios';
import { Redirect } from 'react-router-dom';// useHistory, 
// import Modal from 'antd/es/modal';
// import { Checkbox } from 'antd';

import { AuthContext } from '../../context/AuthContext';
// import Cookie from '../../utils/cookie';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import InputQ from '../../components/q-ui-react/InputQ';
// import InputGroup from '../../components/q-ui-react/InputGroup';
import Password from '../../components/q-ui-react/Password';
// import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';

// "card p-3 w-25 w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";
const CARD_CLASS = "card p-3 w-350px w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";

const initialValues = {
  email: "", // eve.holt@reqres.in
	password: "",
	remember: false
};

function toggleFocus(on){
	// Disabled / Enable all focused element
	Q.domQall(Q.FOCUSABLE).forEach(n => on ? Q.setAttr(n, "tabindex") : n.tabIndex = "-1");
}

const expiresAt = 60 * 24;

export default function Login(){
	const auth = useContext(AuthContext);
  const [loginError, setLoginError] = useState();
  const [isLog, setIsLog] = useState(auth.localAuth());// auth.isAuthenticated() | false
	// const [load, setLoad] = useState(false);
	// const [seePass, setSeePass] = useState(false);
	
	// useEffect(() => {
	// 	// console.log('%cuseEffect in Login','color:yellow;', isLog);
	// 	auth.isAuthenticated().then(v => {
	// 		// console.log(v);
	// 		setIsLog(v);
	// 	}).catch(e => console.log(e));
	// }, []);

	// let history = useHistory();

	const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("Wrong email format")
			// .max(50, "Maximum 50 symbols")
			.required("Email is required"),
      // .required(
      //   intl.formatMessage({
      //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
      //   })
      // ),
    password: Yup.string()
      .min(5, "Minimum 5 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Password is required")
	});

	// const onLogin = async credentials => {
	// 	try {
	// 		// setLoginLoading(true);
	// 		const req = await axios.post("/api/login", credentials);// { data }
	// 		console.log('req: ', req);
			
	// 		if(req.status === 200){
	// 			if(req.data?.user){
	// 				auth.setAuthState(req.data);
	// 				// setLoginSuccess(data.message);
	// 				setLoginError(null);
		
	// 				setTimeout(() => {
	// 					setIsLog(true);
	// 				}, 700);
	// 			}
	// 			else{
	// 				console.log('message: ', req.data.message);
	// 				setLoginError(req.data.message);

	// 				toggleFocus(1);
	// 				Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
	// 			}
	// 		}
	// 	} catch (e) {
	// 		// setLoginLoading(false);
	// 		const { data } = e.response;
	// 		setLoginError(data.message);
	// 		// setLoginSuccess(null);
	// 	}
	// }
	
  const formik = useFormik({
    initialValues,
    validationSchema, // () => Schema
    onSubmit: (values, { setSubmitting }) => { // setStatus, 
			// setSubmitting(true);
			console.log('values: ',values);

			// Disabled all focused element
			toggleFocus();
			Q.setClass(Q.domQ('#QloadStartUp'), 'd-none', 'remove');
			// setLoad(true);

			// onLogin(values);

			// setTimeout(() => {
				// login | https://reqres.in/api/login
				axios.post('/api/login', values)
					.then(r => {
						console.log('r: ', r);
						setSubmitting(false);
						if(r.status === 200){
							if(r.data?.user){
								console.log('r.data: ', r.data);

								// sessionStorage.setItem('auth', JSON.stringify(r.data));
								// history.replace("/");// Redirect to Home

								if(values.remember){
									let date = new Date();
									date.setTime(date.getTime() + expiresAt * 60 * 1000);

									// Cookie.set("access_token", r.data.token, { path: "/", expires: date });

									auth.setAuthState({ ...r.data, expired: date });// r.data
								}
								else{
									// Cookie.set("access_token", r.data.token, { path: "/" });
									auth.setAuthState(r.data);
								}

								// auth.setAuthState(r.data);
								// setLoginSuccess(data.message);
								setLoginError(null);
					
								// setTimeout(() => {
									setIsLog(true);
								// }, 700);
							}
							else{
								// setLoad(false);
								// toggleFocus(1);
								// Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
								console.log('message: ', r.data.message);

								setLoginError(r.data.message);
							}
						}
						toggleFocus(1);
						Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
					})
					.catch(e => {
						console.log('e: ', e);
						
						toggleFocus(1);
						Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
						
						// setLoad(false);
						setSubmitting(false);
					});
			// }, 2000);
    },
	});
	
	// console.log('formik: ', formik);
	// console.log('isAuthenticated: ', auth.isAuthenticated());

	if(isLog) return <Redirect to="/" />;
	
// d-grid place-center
	return (
		<Flex dir="column" justify="center" align="center" 
			className="container-fluid py-3 mh-full-navmain"
		>
			<Head title="Login" />

			<div className={CARD_CLASS}>{/* bg-strip */}
				<h1 className="h4 hr-h mb-3">Login</h1>

				{loginError && <div className="alert alert-danger">{loginError}</div>}

				<Form noValidate 
					valid={formik.touched} 
					// className={load ? "cwait":undefined} 
					// className={formik.isValid ? undefined:"was-validated"} 
					// className={formik.isSubmitting ? "i-load" : undefined} 
					// fieldsetClass="text-center" 
					disabled={formik.isSubmitting} // isProgress
					onSubmit={formik.handleSubmit} 
				>
					{/* <fieldset disabled={formik.isSubmitting}>
						<label className="q-input" aria-label="Email">
							<input className={Q.Cx("form-control", )} type="text" id="email" required 
								{...formik.getFieldProps("email")}
							/>

							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</label>

						<label className="q-input" aria-label="Password">
							<input className="form-control" type="password" id="password" required 
								{...formik.getFieldProps("password")}
							/>

							{(formik.touched.password && formik.errors.password) && 
								<div className="invalid-feedback">{formik.errors.password}</div>
							}
						</label> */}

						<InputQ wrap 
							label="Email" 
							type="email" 
							name="email" 
							required 
							// pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 

							value={formik.values.email} 
							onChange={formik.handleChange}
							// {...formik.getFieldProps("email")} 
						>
							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</InputQ>

						{/* <label className="q-input mb-0" aria-label="Password">
							<InputGroup
								As="div" 
								append={
									<Btn As="div" kind="light" tabIndex="0" 
										className={"tip tipTR fal fa-eye" + (seePass ? "-slash":"")} 
										aria-label={(seePass ? "Hide":"Show") + " Password"}
										onClick={() => setSeePass(!seePass)} 
									/>
								}
							>
								<InputQ // wrap 
									label="Password" 
									type={seePass ? "text":"password"} 
									name="password" 
									required 
									// minLength={5} 
									pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

									// OPTION: From Gmail
									spellCheck={seePass ? false : undefined} 
									autoComplete={seePass ? "off" : undefined} 
									autoCapitalize={seePass ? "off" : undefined} 

									value={formik.values.password} 
									onChange={formik.handleChange}
								/>
							</InputGroup>
						</label> */}

						<Password 
							className="mb-0" 
							label="Password" 
							type="password" 
							name="password" 
							required 
							// minLength={5} 
							pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

							value={formik.values.password} 
							onChange={formik.handleChange}
						/>

						{(formik.touched.password && formik.errors.password) && 
							<div className="invalid-feedback d-block">{formik.errors.password}</div>
						}

						<Flex justify="between" align="center" className="my-3">
							{/* <Checkbox>Remember me</Checkbox> */}

							{/* <CheckRadioQ 
								className="focusBg small" // mb-0
								name="remember" 
								disabled={formik.isSubmitting} 
							>
								Remember me
							</CheckRadioQ> */}

							<div className="custom-control custom-checkbox">
								<input onChange={formik.handleChange} type="checkbox" className="custom-control-input" id="remember" />
								<label className="custom-control-label text-sm" htmlFor="remember">
									Remember me
								</label>
							</div>

							<Btn type="submit">Login</Btn>
						</Flex>

						<Aroute 
							to="/forgot-password" 
							kind="secondary" 
							className="text-sm fa fa-question-circle"  
							// disabled={formik.isSubmitting} 
						> Forgot password?
						</Aroute>
					{/* </fieldset> */}
				</Form>
			</div>

			<div className={CARD_CLASS + " d-block mt-2 text-center text-sm"}>
				Don't have account? <Aroute to="/register">Register</Aroute>
			</div>
		</Flex>
	);
}

/*
<Fragment></Fragment>
*/
