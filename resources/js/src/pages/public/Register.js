import React, { useContext, useState } from 'react';// { , useEffect }
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
import axios from 'axios';
import { Redirect } from 'react-router-dom';// useHistory, 
// import Popover from 'antd/lib/popover';
// import FormCheck from 'react-bootstrap/FormCheck';

import { AuthContext } from '../../context/AuthContext';
import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import InputQ from '../../components/q-ui-react/InputQ';
import Password from '../../components/q-ui-react/Password';
// import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';

const CARD_CLASS = "card p-3 w-350px w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";

const initialValues = {
	email: "", // eve.holt@reqres.in
	username: "", 
	password: "", // pistol
	// confirmPassword: "", 
	// term: false
};

export default function Register(){
	// let history = useHistory();
	const auth = useContext(AuthContext);
	const isLog = auth.localAuth();
	// const [isLog, setLog] = useState(auth.localAuth());
	const [ok, setOk] = useState(false);//  | false
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Register','color:yellow;');
	// }, []);

	const validationSchema = Yup.object().shape({
		email: Yup.string()
			// .label('Email')
      .email("Wrong email format")
			// .max(50, "Maximum 50 symbols")
			.required("Email is required"),
		username: Yup.string()
			.min(3, "Minimum 3 symbols")
			.max(55, "Maximum 55 symbols")
			.required("Username is required"),
    password: Yup.string()
      .min(5, "Minimum 5 symbols")
			.required("Password is required"),
		// confirmPassword: Yup.string()
		// 	// 
		// 	// .min(5, "Minimum 5 symbols")
		// 	// .required()
		// 	// .required('Confirm Password must same'),
		// 	.test('passwords-match', 'Password must match', v => v === Yup.ref("password")),
		// term: Yup.bool().oneOf([true], 'Terms & Conditions must be accepted')
	});
	
  const formik = useFormik({
    initialValues,
    validationSchema, // () => Schema
    onSubmit: (values, { setSubmitting }) => { // setStatus, 
			console.log('values: ',values);
			// console.log('setStatus: ',setStatus);
			// setLoad(true);

			// setTimeout(() => {
				// https://reqres.in/api/register
				axios.post('/api/register', {
					email: values.email,
					name: values.username,
					password: values.password,
					// level: 2 // 1 = member, 2 = admin
				}).then(r => {
					console.log('r: ', r);
					// setSubmitting(false);
					if(r.status === 200 && r.data?.access_token){ // token
						// console.log('r.data: ', r.data);

						// sessionStorage.setItem('auth', JSON.stringify(r.data));
						// history.replace("/");// Redirect to Home
						setOk(true);
					}
					// else{
					// 	setLoad(false);
					// }
				})
				.catch(e => {
					console.log('e: ', e);
					setSubmitting(false);
					// setLoad(false);
				});
			// }, 9);
    },
	});
	
	// console.log('formik: ', formik);
	// console.log('formik getFieldProps: ', formik.getFieldProps("email"));

	if(isLog) return <Redirect to="/" />;

	if(ok){
		return (
			<Flex dir="column" justify="center" align="center" className="h-full-nav">
				<h1>Register Success</h1>
				<Aroute to="/login" btn="primary">Login</Aroute>
			</Flex>
		)
	}

// d-grid place-center
	return (
		<Flex dir="column" justify="center" align="center" className="container-fluid py-3 mh-full-navmain">
			<Head title="Register" />

			<div className={CARD_CLASS}>{/* bg-strip */}
				<h1 className="h4 hr-h mb-3">Register</h1>

				<Form noValidate 
					valid={formik.touched} 
					// className={"position-relative" + (load ? " i-load bg-size-90px-after cwait" : "")}  
					// className={formik.isValid ? undefined : "was-validated"} 
					// className={
					// 	Q.Cx({
					// 		"was-validated": Object.keys(formik.touched).length > 0, 
					// 		"cwait": load
					// 	})
					// } 
					disabled={formik.isSubmitting} // isProgress
					onSubmit={formik.handleSubmit} 
				>
					<InputQ wrap 
						label="Email" 
						type="email" 
						name="email" 
						required 
						// pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 

						value={formik.values.email} 
						onChange={formik.handleChange}
						// {...formik.getFieldProps("email")} 
					>
						{(formik.touched.email && formik.errors.email) && 
							<div className="invalid-feedback">{formik.errors.email}</div>
						}
					</InputQ>

					<InputQ wrap 
						label="Username" 
						name="username" 
						required 
						pattern=".{3,}" 
						spellCheck={false} 

						value={formik.values.username} 
						onChange={formik.handleChange}
					>
						{(formik.touched.username && formik.errors.username) && 
							<div className="invalid-feedback">{formik.errors.username}</div>
						}
					</InputQ>

					<Password 
						className="mb-0" 
						label="Password" 
						type="password" 
						name="password" 
						required 
						// minLength={5} 
						pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

						value={formik.values.password} 
						onChange={formik.handleChange}
					/>

					{(formik.touched.password && formik.errors.password) && 
						<div className="invalid-feedback d-block">{formik.errors.password}</div>
					}

					{/* <InputQ wrap 
						label="Confirm Password" 
						type="password" 
						name="confirmPassword" 
						required 
						pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

						value={formik.values.confirmPassword} 
						onChange={formik.handleChange}
					>
						{(formik.touched.confirmPassword && formik.errors.confirmPassword) && 
							<div className="invalid-feedback">{formik.errors.confirmPassword}</div>
						}
					</InputQ> */}

					<Flex align="start" className="mt-3">
						<div className="small mr-2">
							By register, you agree to our Terms, Data Policy and Cookies Policy. 
							<Aroute 
								to="/terms-and-conditions" 
								kind="dark" // secondary
								className="d-inline-block mt-2 fa fa-info-circle" // 
								disabled={formik.isSubmitting} 
								tabIndex="-1" 
							> Read terms & conditions
							</Aroute>
						</div>
						
						<Btn type="submit">Register</Btn>
					</Flex>
				</Form>
			</div>

			<div className={CARD_CLASS + " d-block mt-2 text-center text-sm"}>
				Have an account? <Aroute to="/login">Login</Aroute>
			</div>
		</Flex>
	);
}

/*
<React.Fragment></React.Fragment>
*/
