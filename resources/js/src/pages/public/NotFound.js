import React from 'react';
// import { useLocation } from 'react-router-dom';// Route,
import { useLastLocation } from 'react-router-last-location';
// import {Helmet, HelmetProvider} from 'react-helmet-async';// ../components_dev/react-helmet-async
// import {Helmet} from "react-helmet";
// import MainContent from '../parts/MainContent';
import Head from '../../components/q-ui-react/Head';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';

// const w = window;
// const origin = w.location.origin;
// const hLength = w.history.length;

export default function NotFound(){ // {backUrl}
  // const [backUrl, setBackUrl] = React.useState('/');
	// let location = useLocation();
	const lastLocation = useLastLocation();
	// const prevURL = localStorage.prevURL;

	// React.useEffect(() => {
	// 	// console.log('%cuseEffect in NotFound','color:yellow;');
	// 	if(document.referrer.includes(origin) && hLength > 2){
	// 		console.log('document.referrer: ', document.referrer);
	// 		console.log(document.referrer.includes(origin));
	// 		console.log('hLength', hLength);
	// 		setBackUrl(location.pathname);
	// 	}
	// 	 return () => {
	//
	// 	 }
	// }, [backUrl]);

	// document.referrer.includes(origin)
	// console.log(document.referrer.includes(origin) && hLength > 2 && backUrl && backUrl !== '/');
	console.log('lastLocation: ', lastLocation);
	// console.log('prevURL: ', prevURL);

	// const onBack = e => {
	// 	if(document.referrer.includes(origin) && hLength > 2){
	// 		e.preventDefault();
	// 		console.log('onBack');
	// 		console.log('onBack hLength: ', hLength);
	// 		console.log('onBack w.history', w.history);
	// 		w.history.back();
	// 	}
	// }

	return (
		<Flex 
			dir="column" 
			justify="center" 
			align="center" 
			className="container-fluid text-center mh-full-navmain" // min-vh-100 bg-no-repeat 
			// style={{
			// 	height: 'calc(100vh - 48px)'
			// 	backgroundImage: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' enable-background='new 0 0 48 48' viewBox='0 0 48 48'%3E%3Cpath d='M43,48H5c-2.757,0-5-2.243-5-5V5c0-2.757,2.243-5,5-5h38c2.757,0,5,2.243,5,5v38C48,45.757,45.757,48,43,48z M5,2 C3.346,2,2,3.346,2,5v38c0,1.654,1.346,3,3,3h38c1.654,0,3-1.346,3-3V5c0-1.654-1.346-3-3-3H5z'/%3E%3Cpath d='M47 10H1c-.552 0-1-.447-1-1s.448-1 1-1h46c.552 0 1 .447 1 1S47.552 10 47 10zM6 6C5.87 6 5.74 5.97 5.62 5.92 5.5 5.87 5.39 5.8 5.29 5.71 5.2 5.609 5.13 5.5 5.08 5.38 5.03 5.26 5 5.13 5 5s.03-.26.08-.38C5.13 4.5 5.2 4.39 5.29 4.29 5.39 4.2 5.5 4.13 5.62 4.08c.37-.16.81-.07 1.09.21C6.8 4.39 6.87 4.5 6.92 4.62 6.97 4.74 7 4.87 7 5S6.97 5.26 6.92 5.38C6.87 5.5 6.8 5.609 6.71 5.71 6.61 5.8 6.5 5.87 6.38 5.92S6.13 6 6 6zM10 6C9.87 6 9.74 5.97 9.62 5.92 9.5 5.87 9.39 5.8 9.29 5.71 9.2 5.609 9.13 5.5 9.08 5.38 9.03 5.26 9 5.13 9 5s.03-.26.08-.38C9.13 4.5 9.2 4.39 9.29 4.29 9.39 4.2 9.5 4.13 9.62 4.08c.37-.16.81-.07 1.09.21.09.1.16.21.21.33C10.97 4.74 11 4.87 11 5s-.03.26-.08.38c-.05.12-.12.229-.21.33-.1.09-.21.16-.33.21S10.13 6 10 6zM36 35H12c-.552 0-1-.447-1-1s.448-1 1-1h24c.552 0 1 .447 1 1S36.552 35 36 35zM33 39H15c-.552 0-1-.447-1-1s.448-1 1-1h18c.552 0 1 .447 1 1S33.552 39 33 39zM25 31h-2c-1.654 0-3-1.346-3-3v-8c0-1.654 1.346-3 3-3h2c1.654 0 3 1.346 3 3v8C28 29.654 26.654 31 25 31zM23 19c-.551 0-1 .448-1 1v8c0 .552.449 1 1 1h2c.551 0 1-.448 1-1v-8c0-.552-.449-1-1-1H23zM17 27h-6c-.328 0-.635-.161-.822-.431-.187-.27-.229-.613-.115-.921l3-8c.195-.517.771-.775 1.288-.585.517.194.779.771.585 1.288L12.443 25H17c.552 0 1 .447 1 1S17.552 27 17 27z'/%3E%3Cpath d='M16 31c-.552 0-1-.447-1-1v-8c0-.553.448-1 1-1s1 .447 1 1v8C17 30.553 16.552 31 16 31zM37 27h-6c-.328 0-.635-.161-.822-.431-.187-.27-.229-.613-.115-.921l3-8c.195-.517.771-.775 1.288-.585.517.194.779.771.585 1.288L32.443 25H37c.552 0 1 .447 1 1S37.552 27 37 27z'/%3E%3Cpath d='M36,31c-0.552,0-1-0.447-1-1v-8c0-0.553,0.448-1,1-1s1,0.447,1,1v8C37,30.553,36.552,31,36,31z'/%3E%3C/svg%3E")`
			// }}
		>
			<Head title="Not Found - 404" />

			<div className="bg-no-repeat mb-3 p-5" 
				style={{
					backgroundImage: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' enable-background='new 0 0 48 48' viewBox='0 0 48 48'%3E%3Cpath d='M43,48H5c-2.757,0-5-2.243-5-5V5c0-2.757,2.243-5,5-5h38c2.757,0,5,2.243,5,5v38C48,45.757,45.757,48,43,48z M5,2 C3.346,2,2,3.346,2,5v38c0,1.654,1.346,3,3,3h38c1.654,0,3-1.346,3-3V5c0-1.654-1.346-3-3-3H5z'/%3E%3Cpath d='M47 10H1c-.552 0-1-.447-1-1s.448-1 1-1h46c.552 0 1 .447 1 1S47.552 10 47 10zM6 6C5.87 6 5.74 5.97 5.62 5.92 5.5 5.87 5.39 5.8 5.29 5.71 5.2 5.609 5.13 5.5 5.08 5.38 5.03 5.26 5 5.13 5 5s.03-.26.08-.38C5.13 4.5 5.2 4.39 5.29 4.29 5.39 4.2 5.5 4.13 5.62 4.08c.37-.16.81-.07 1.09.21C6.8 4.39 6.87 4.5 6.92 4.62 6.97 4.74 7 4.87 7 5S6.97 5.26 6.92 5.38C6.87 5.5 6.8 5.609 6.71 5.71 6.61 5.8 6.5 5.87 6.38 5.92S6.13 6 6 6zM10 6C9.87 6 9.74 5.97 9.62 5.92 9.5 5.87 9.39 5.8 9.29 5.71 9.2 5.609 9.13 5.5 9.08 5.38 9.03 5.26 9 5.13 9 5s.03-.26.08-.38C9.13 4.5 9.2 4.39 9.29 4.29 9.39 4.2 9.5 4.13 9.62 4.08c.37-.16.81-.07 1.09.21.09.1.16.21.21.33C10.97 4.74 11 4.87 11 5s-.03.26-.08.38c-.05.12-.12.229-.21.33-.1.09-.21.16-.33.21S10.13 6 10 6zM36 35H12c-.552 0-1-.447-1-1s.448-1 1-1h24c.552 0 1 .447 1 1S36.552 35 36 35zM33 39H15c-.552 0-1-.447-1-1s.448-1 1-1h18c.552 0 1 .447 1 1S33.552 39 33 39zM25 31h-2c-1.654 0-3-1.346-3-3v-8c0-1.654 1.346-3 3-3h2c1.654 0 3 1.346 3 3v8C28 29.654 26.654 31 25 31zM23 19c-.551 0-1 .448-1 1v8c0 .552.449 1 1 1h2c.551 0 1-.448 1-1v-8c0-.552-.449-1-1-1H23zM17 27h-6c-.328 0-.635-.161-.822-.431-.187-.27-.229-.613-.115-.921l3-8c.195-.517.771-.775 1.288-.585.517.194.779.771.585 1.288L12.443 25H17c.552 0 1 .447 1 1S17.552 27 17 27z'/%3E%3Cpath d='M16 31c-.552 0-1-.447-1-1v-8c0-.553.448-1 1-1s1 .447 1 1v8C17 30.553 16.552 31 16 31zM37 27h-6c-.328 0-.635-.161-.822-.431-.187-.27-.229-.613-.115-.921l3-8c.195-.517.771-.775 1.288-.585.517.194.779.771.585 1.288L32.443 25H37c.552 0 1 .447 1 1S37.552 27 37 27z'/%3E%3Cpath d='M36,31c-0.552,0-1-0.447-1-1v-8c0-0.553,0.448-1,1-1s1,0.447,1,1v8C37,30.553,36.552,31,36,31z'/%3E%3C/svg%3E")`
				}}
			/>

			<h4>Not Found for <code>{window.location.pathname}</code></h4>

			{/*  && backUrl && backUrl !== '/' | document.referrer */}
			{/* lastLocation ? lastLocation.pathname : document.referrer?.length ? */}
			{/* lastLocation ? lastLocation.pathname : prevURL ? prevURL.replace(window.location.origin + "/admin", "") : "/" */}
			<Aroute exact strict to={lastLocation ? lastLocation.pathname : ""} btn="primary">
				{lastLocation && lastLocation.pathname !== "/" ? "Back" : "Go to Home"}
			</Aroute>
		</Flex>
	);
}

// <MainContent title="Not Found - 404" head={false}></MainContent>

// export default withLastLocation(NotFound);

/*
{(document.referrer.includes(origin) && hLength > 2) ?
	<Aroute
		// replace | backUrl
		to={`/`} btn="primary">
		BACK
	</Aroute>
	:
	<a href={origin} className="btn btn-primary">BACK TO HOME</a>
}

<Route
	path={location.pathname} // "*"
>
</Route>
							onClick={e => {
								let et = e.target;
								e.preventDefault();
								e.stopPropagation();
								et
							}}
*/
