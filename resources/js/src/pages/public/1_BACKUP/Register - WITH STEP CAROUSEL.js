import React from 'react';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
import axios from 'axios';
// import { useHistory } from 'react-router-dom';
// import Modal from 'antd/es/modal';
import Carousel from 'react-bootstrap/Carousel';
import { Checkbox } from 'antd';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import InputQ from '../../components/q-ui-react/InputQ';

const CARD_CLASS = "card p-3 w-25 w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";

const initialValues = {
  email: "eve.holt@reqres.in", 
  password: ""
};

export default function Register(){
  const [step, setStep] = React.useState(0);
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in Register','color:yellow;');
	// }, []);

	// let history = useHistory();

	const Schema = Yup.object().shape({
    email: Yup.string()
      //.email("Wrong email format")
      .min(3, "Minimum 3 symbols")
			.max(50, "Maximum 50 symbols")
			.required(),
    password: Yup.string()
      .min(5, "Minimum 5 symbols")
      // .max(50, "Maximum 50 symbols")
      .required()
	});
	
  const formik = useFormik({
    initialValues,
    validationSchema: Schema, // () => Schema
    onSubmit: (values, { setStatus, setSubmitting }) => {
			// setSubmitting(true);
			console.log('values: ',values);

			axios.post('https://reqres.in/api/register', values)
				.then(r => {
					console.log('r: ', r);
					if(r.status === 200 && r.data?.token){
						setSubmitting(false);
						console.log('r.data: ', r.data);

						// sessionStorage.setItem('auth', JSON.stringify(r.data));

						// history.replace("/");// Redirect to Home
					}
				})
				.catch(e => {
					console.log('e: ', e);
					// setModal(true);
					setSubmitting(false);
				});
    },
	});
	
	// console.log('formik: ', formik);
	// console.log('formik getFieldProps: ', formik.getFieldProps("email"));

  const onStep = (id, e) => {
    setStep(id);
  }

// d-grid place-center
	return (
		<Flex dir="column" justify="center" align="center" className="container-fluid py-3 mh-full-navmain">
			<Head title="Register" />

			<div className={CARD_CLASS}>{/* bg-strip */}
				<h1 className="h4 hr-h mb-3 mx-1">Register</h1>

				<Form noValidate 
					isValid={formik.isValid} 
					// className={formik.isValid ? undefined:"was-validated"} 
					// fieldsetClass="text-center" 
					disabled={formik.isSubmitting} // isProgress
					onSubmit={formik.handleSubmit} 
				>
					<Carousel 
						className="q-steps" 
						interval={null} 
						controls={false} 
						indicators={false} 
						activeIndex={step} 
						onSelect={onStep} 
					>
						<Carousel.Item 
							className="w-100 pb-1 px-1 bg-light" 
							// style={{ height: 300 }}
						>
							<InputQ wrap 
								label="Email" 
								// id="email" 
								name="email" 
								required 
								pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 

								value={formik.values.email} 
								onChange={formik.handleChange}
								// {...formik.getFieldProps("email")} 
							>
								{(formik.touched.email && formik.errors.email) && 
									<div className="invalid-feedback">{formik.errors.email}</div>
								}
							</InputQ>

							<Flex align="center" className="mt-3">
								{/* <Aroute 
									to="/forgot-password" 
									kind="secondary" 
									className="text-sm" //  fa fa-info-circle
									disabled={formik.isSubmitting}
								> More ways to register
								</Aroute> */}

								<Btn className="ml-auto" onClick={() => onStep(1)}>Next</Btn>
							</Flex>
						</Carousel.Item>

						<Carousel.Item className="w-100 pb-1 px-1bg-light">
							<InputQ wrap 
								label="Password" 
								type="password" 
								name="password" 
								required 
								// minLength={5} 
								pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

								value={formik.values.password} 
								onChange={formik.handleChange}
							>
								{(formik.touched.password && formik.errors.password) && 
									<div className="invalid-feedback">{formik.errors.password}</div>
								}
							</InputQ>

							
						</Carousel.Item>

						{/* <Flex justify="between" align="center" className="mb-2">
							<Checkbox>Remember me</Checkbox>
							
							<Btn type="submit">Login</Btn>
						</Flex> */}
					</Carousel>
				</Form>
			</div>

			{/* <div className={CARD_CLASS + " mt-2 text-center"}>
				<h6>Don't have account? <Aroute to="/register">Register</Aroute></h6>
			</div> */}
		</Flex>
	);
}

/*
<React.Fragment></React.Fragment>
*/
