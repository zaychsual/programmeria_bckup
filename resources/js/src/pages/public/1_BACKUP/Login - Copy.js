import React from 'react';
import * as Yup from 'yup';
import { useFormik } from 'formik';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import InputQ from '../../components/q-ui-react/InputQ';

const initialValues = {
  email: "", 
  password: ""
};

export default function Login(){
  // const [data, setData] = React.useState();
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in Login','color:yellow;');
	// }, []);

	const Schema = Yup.object().shape({
    email: Yup.string()
      //.email("Wrong email format")
      .min(3, "Minimum 3 symbols")
			.max(50, "Maximum 50 symbols")
			.required(),
      // .required(
      //   intl.formatMessage({
      //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
      //   })
      // ),
    password: Yup.string()
      .min(5, "Minimum 5 symbols")
      // .max(50, "Maximum 50 symbols")
      .required()
	});
	
  const formik = useFormik({
    initialValues,
    validationSchema: Schema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
			console.log('values: ',values);

			// setSubmitting(false);

      // enableLoading();
      // props.sendError("");
      //setTimeout(() => {
      // login(values.email, values.password)
      //   .then(({ data }) => {
      //     if (!data.status.success) {
      //       props.sendError(data.status.message);
      //     } else {
      //       props.login(data.data.token);
      //     }
      //     disableLoading();
      //     setSubmitting(false);
      //   })
      //   .catch((e) => {
      //     disableLoading();
      //     setSubmitting(false);
      //     props.sendError(
      //       e.message
      //     );
      //   });
      //}, 1000);
    },
	});
	
	// console.log('formik: ', formik);
	// console.log('formik getFieldProps: ', formik.getFieldProps("email"));

	return (
		<div className="container-fluid py-3 d-grid place-center mh-full-navmain">
			<Head title="Login" />

			<div className="card card-body w-25 bg-strip">
				<h1 className="h4 hr-h">Login</h1>

				<Form // noValidate 
					disabled={formik.isSubmitting} // isProgress
					onSubmit={(e) => formik.handleSubmit(e)} 
				>
					{/* <fieldset disabled={formik.isSubmitting}>
						<label className="q-input" aria-label="Email">
							<input className="form-control" type="text" 
								{...formik.getFieldProps("email")}
							/>

							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</label>

						<label className="q-input" aria-label="Password">
							<input className="form-control" type="password" name="password" 
								{...formik.getFieldProps("password")}
							/>

							{(formik.touched.password && formik.errors.password) && 
								<div className="invalid-feedback">{formik.errors.password}</div>
							}
						</label> */}

						<InputQ 
							label="Email" 
							name="email" 
							// required 

							{...formik.getFieldProps("email")} 
						>
							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</InputQ>

						<InputQ 
							label="Password" 
							type="password" 
							name="password" 
							required 

							{...formik.getFieldProps("password")}
						>
							{(formik.touched.password && formik.errors.password) && 
								<div className="invalid-feedback">{formik.errors.password}</div>
							}
						</InputQ>

						<Flex justify="between" align="center">
							<Aroute to="/forgot-password" text="secondary">Forgot password?</Aroute>
							<Btn type="submit">Login</Btn>
						</Flex>
					{/* </fieldset> */}
				</Form>
			</div>
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
