import React, { useState } from 'react';// { useEffect }
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
import axios from 'axios';
import { useHistory } from 'react-router-dom';
// import Modal from 'antd/es/modal';
import Popover from 'antd/lib/popover';// Checkbox
// import FormCheck from 'react-bootstrap/FormCheck';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import InputQ from '../../components/q-ui-react/InputQ';
// import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';

const CARD_CLASS = "card p-3 w-30 w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";

const initialValues = {
	email: "", // eve.holt@reqres.in
	username: "", 
	password: "", // pistol
	// confirmPassword: "", 
	term: false
};

export default function Register(){
  const [load, setLoad] = useState(false);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Register','color:yellow;');
	// }, []);

	let history = useHistory();

	const Schema = Yup.object().shape({
		email: Yup.string()
			// .label('Email')
      .email("Wrong email format")
			// .max(50, "Maximum 50 symbols")
			.required("Email is required"),
		username: Yup.string()
			.min(3, "Minimum 3 symbols")
			.max(55, "Maximum 55 symbols")
			.required("Username is required"),
    password: Yup.string()
      .min(5, "Minimum 5 symbols")
			.required("Password is required"),
		// confirmPassword: Yup.string()
		// 	// 
		// 	// .min(5, "Minimum 5 symbols")
		// 	// .required()
		// 	// .required('Confirm Password must same'),
		// 	.test('passwords-match', 'Password must match', v => v === Yup.ref("password")),
		term: Yup.bool()
			.oneOf([true], 'Terms & Conditions must be accepted')
	});
	
  const formik = useFormik({
    initialValues,
    validationSchema: Schema, // () => Schema
    onSubmit: (values, { setStatus, setSubmitting }) => {
			console.log('values: ',values);
			// console.log('setStatus: ',setStatus);
			// setLoad(true);

			// setTimeout(() => {
				// https://reqres.in/api/register
				axios.post('/api/register', {
					email: values.email,
					name: values.username,
					password: values.password,
					// level: 2 // 1 = member, 2 = admin
				}).then(r => {
					console.log('r: ', r);
					// setSubmitting(false);
					if(r.status === 200 && r.data?.token){
						console.log('r.data: ', r.data);

						// sessionStorage.setItem('auth', JSON.stringify(r.data));
						history.replace("/");// Redirect to Home
					}else{
						setLoad(false);
					}
				})
				.catch(e => {
					console.log('e: ', e);
					setSubmitting(false);
					setLoad(false);
				});
			// }, 9);
    },
	});
	
	// console.log('formik: ', formik);
	// console.log('formik getFieldProps: ', formik.getFieldProps("email"));

// d-grid place-center
	return (
		<Flex dir="column" justify="center" align="center" className="container-fluid py-3 mh-full-navmain">
			<Head title="Register" />

			<div className={CARD_CLASS}>{/* bg-strip */}
				<h1 className="h4 hr-h mb-3">Register</h1>

				<Form noValidate 
					valid={formik.touched} 
					// className={"position-relative" + (load ? " i-load bg-size-90px-after cwait" : "")}  
					// className={formik.isValid ? undefined : "was-validated"} 
					// className={
					// 	Q.Cx({
					// 		"was-validated": Object.keys(formik.touched).length > 0, 
					// 		"cwait": load
					// 	})
					// } 
					disabled={formik.isSubmitting} // isProgress
					onSubmit={formik.handleSubmit} 
				>
					<InputQ wrap 
						label="Email" 
						type="email" 
						name="email" 
						required 
						// pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 

						value={formik.values.email} 
						onChange={formik.handleChange}
						// {...formik.getFieldProps("email")} 
					>
						{(formik.touched.email && formik.errors.email) && 
							<div className="invalid-feedback">{formik.errors.email}</div>
						}
					</InputQ>

					<InputQ wrap 
						label="Username" 
						name="username" 
						required 
						pattern=".{3,}" 
						spellCheck={false} 

						value={formik.values.username} 
						onChange={formik.handleChange}
					>
						{(formik.touched.username && formik.errors.username) && 
							<div className="invalid-feedback">{formik.errors.username}</div>
						}
					</InputQ>
					
					<InputQ wrap 
						label="Password" 
						type="password" 
						name="password" 
						required 
						// minLength={5} 
						pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

						value={formik.values.password} 
						onChange={formik.handleChange}
					>
						{(formik.touched.password && formik.errors.password) && 
							<div className="invalid-feedback">{formik.errors.password}</div>
						}

						{/* <Btn className="tip tipTL fa fa-eye" aria-label="Show Password" /> */}
					</InputQ>

					{/* <InputQ wrap 
						label="Confirm Password" 
						type="password" 
						name="confirmPassword" 
						required 
						pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

						value={formik.values.confirmPassword} 
						onChange={formik.handleChange}
					>
						{(formik.touched.confirmPassword && formik.errors.confirmPassword) && 
							<div className="invalid-feedback">{formik.errors.confirmPassword}</div>
						}
					</InputQ> */}

					<Flex align="center" className="mt-3">
						{/* <Checkbox>I agree to terms & conditions</Checkbox> */}
						<div className="mr-auto">
							<Popover 
								title={null} // "Title" 
								content={
									<Aroute 
										to="/terms-and-conditions" 
										kind="secondary" 
										className="pl-5px text-sm fa fa-info-circle" // d-block 
										disabled={formik.isSubmitting} 
										tabIndex="-1" 
									> Read terms & conditions
									</Aroute>
								}
							>
								{/* <FormCheck custom
									type="checkbox" 
									name="term" 
									disabled={formik.isSubmitting} 
									required 
									isInvalid={formik.touched.term && formik.errors.term && !formik.values.term} 
									onChange={formik.handleChange} 
									label="I agree to terms & conditions" 
								/> */}

								<div className="custom-control custom-checkbox">
									<input type="checkbox" className="custom-control-input" id="term" 
										required 
										onChange={formik.handleChange} 
									/>
									<label className="custom-control-label text-sm" htmlFor="term">
										I agree to terms & conditions
									</label>

									{(formik.touched.term && formik.errors.term) && 
										<div className="invalid-feedback" style={{left:'-1.5rem',position:'relative'}}>{formik.errors.term}</div>
									}
								</div>
							</Popover>


						</div>
						
						<Btn type="submit">Register</Btn>
					</Flex>
				</Form>
			</div>

			{/* <div className={CARD_CLASS + " mt-2 text-center"}>
				<h6>Don't have account? <Aroute to="/register">Register</Aroute></h6>
			</div> */}
		</Flex>
	);
}

/*
<React.Fragment></React.Fragment>
*/
