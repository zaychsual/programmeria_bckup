import React, { useState } from 'react';
import * as Yup from 'yup';
import { useFormik } from 'formik';// useFormik | Formik
import axios from 'axios';
import { useHistory } from 'react-router-dom';
// import Modal from 'antd/es/modal';
// import { Checkbox } from 'antd';

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
import Flex from '../../components/q-ui-react/Flex';
import Aroute from '../../components/q-ui-react/Aroute';
import Btn from '../../components/q-ui-react/Btn';
import InputQ from '../../components/q-ui-react/InputQ';
import InputGroup from '../../components/q-ui-react/InputGroup';
// import CheckRadioQ from '../../components/q-ui-react/CheckRadioQ';
// import PageLoader from '../../components/PageLoader';

// "card p-3 w-25 w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";
const CARD_CLASS = "card p-3 w-30 w-t-p-50 w-t-l-35 w-p-lp-50 w-p-p-100 shadow-sm bg-light";

const initialValues = {
  email: "", // eve.holt@reqres.in
  password: ""
};

function toggleFocus(on){
	// Disabled / Enable all focused element
	Q.domQall(Q.FOCUSABLE).forEach(n => on ? Q.setAttr(n, "tabindex") : n.tabIndex = "-1");
}

export default function Login(){
	// const [load, setLoad] = useState(false);
	const [seePass, setSeePass] = useState(false);
	
	// React.useEffect(() => {
	// 	console.log('%cuseEffect in Login','color:yellow;');
	// }, []);

	let history = useHistory();

	const Schema = Yup.object().shape({
    email: Yup.string()
      .email("Wrong email format")
			// .max(50, "Maximum 50 symbols")
			.required("Email is required"),
      // .required(
      //   intl.formatMessage({
      //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
      //   })
      // ),
    password: Yup.string()
      .min(5, "Minimum 5 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Password is required")
	});
	
  const formik = useFormik({
    initialValues,
    validationSchema: Schema, // () => Schema
    onSubmit: (values, { setStatus, setSubmitting }) => {
			// setSubmitting(true);
			console.log('values: ',values);

			// Disabled all focused element
			toggleFocus();
			Q.setClass(Q.domQ('#QloadStartUp'), 'd-none', 'remove');
			// setLoad(true);

			// setTimeout(() => {
				// login | https://reqres.in/api/login
				axios.post('/api/login', values)
					.then(r => {
						console.log('r: ', r);
						setSubmitting(false);
						if(r.status === 200){
							if(r.data?.user){
								console.log('r.data: ', r.data);

								// sessionStorage.setItem('auth', JSON.stringify(r.data));
								// toggleFocus(1);
								// Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');

								history.replace("/");// Redirect to Home
							}
							else{
								// setLoad(false);
								// toggleFocus(1);
								// Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
								console.log('message: ', r.data.message);
							}
						}
						toggleFocus(1);
						Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
					})
					.catch(e => {
						console.log('e: ', e);
						
						toggleFocus(1);
						Q.setClass(Q.domQ('#QloadStartUp'), 'd-none');
						
						// setLoad(false);
						setSubmitting(false);
					});
			// }, 2000);
    },
	});
	
	// console.log('formik: ', formik);
	// console.log('formik getFieldProps: ', formik.getFieldProps("email"));

// d-grid place-center
	return (
		<Flex dir="column" justify="center" align="center" className="container-fluid py-3 mh-full-navmain">
			<Head title="Login" />

			<div className={CARD_CLASS}>{/* bg-strip */}
				<h1 className="h4 hr-h mb-3">Login</h1>

				{/* load && <PageLoader top left bottom	/> */}

				<Form noValidate 
					valid={formik.touched} 
					// className={load ? "cwait":undefined} 
					// className={formik.isValid ? undefined:"was-validated"} 
					// fieldsetClass="text-center" 
					disabled={formik.isSubmitting} // isProgress
					onSubmit={formik.handleSubmit} 
				>
					{/* <fieldset disabled={formik.isSubmitting}>
						<label className="q-input" aria-label="Email">
							<input className={Q.Cx("form-control", )} type="text" id="email" required 
								{...formik.getFieldProps("email")}
							/>

							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</label>

						<label className="q-input" aria-label="Password">
							<input className="form-control" type="password" id="password" required 
								{...formik.getFieldProps("password")}
							/>

							{(formik.touched.password && formik.errors.password) && 
								<div className="invalid-feedback">{formik.errors.password}</div>
							}
						</label> */}

						<InputQ wrap 
							label="Email" 
							type="email" 
							name="email" 
							required 
							// pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 

							value={formik.values.email} 
							onChange={formik.handleChange}
							// {...formik.getFieldProps("email")} 
						>
							{(formik.touched.email && formik.errors.email) && 
								<div className="invalid-feedback">{formik.errors.email}</div>
							}
						</InputQ>

						<label className="q-input mb-0" aria-label="Password">
							<InputGroup
								As="div" 
								append={
									<Btn As="div" kind="light" 
										className={"tip tipTR fal fa-eye" + (seePass ? "-slash":"")} 
										aria-label={(seePass ? "Hide":"Show") + " Password"}
										onClick={() => setSeePass(!seePass)} 
									/>
								}
							>
								<InputQ // wrap 
									label="Password" 
									type={seePass ? "text":"password"} 
									name="password" 
									required 
									// minLength={5} 
									pattern=".{5,}" // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 

									// OPTION: From Gmail
									spellCheck={seePass ? false : undefined} 
									autoComplete={seePass ? "off" : undefined} 
									autoCapitalize={seePass ? "off" : undefined} 

									value={formik.values.password} 
									onChange={formik.handleChange}
								/>
							</InputGroup>
						</label>

						{(formik.touched.password && formik.errors.password) && 
							<div className="invalid-feedback d-block">{formik.errors.password}</div>
						}

						<Flex justify="between" align="center" className="my-3">
							{/* <Checkbox>Remember me</Checkbox> */}

							{/* <CheckRadioQ 
								className="focusBg small" // mb-0
								name="remember" 
								disabled={formik.isSubmitting} 
							>
								Remember me
							</CheckRadioQ> */}

							<div className="custom-control custom-checkbox">
								<input type="checkbox" className="custom-control-input" id="remember" 
								/>
								<label className="custom-control-label text-sm" htmlFor="remember">
									Remember me
								</label>
							</div>

							<Btn type="submit">Login</Btn>
						</Flex>

						<Aroute 
							to="/forgot-password" 
							kind="secondary" 
							className="text-sm fa fa-question-circle"  
							// disabled={formik.isSubmitting} 
						> Forgot password?
						</Aroute>
					{/* </fieldset> */}
				</Form>
			</div>

			<div className={CARD_CLASS + " mt-2 text-center"}>
				<h6>Don't have account? <Aroute to="/register">Register</Aroute></h6>
			</div>
		</Flex>
	);
}

/*
<React.Fragment></React.Fragment>
*/
