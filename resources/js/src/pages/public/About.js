import React, { useEffect } from 'react';// { useState, useEffect }
// import { useHistory } from 'react-router-dom';

import Head from '../../components/q-ui-react/Head';
// import Btn from '../../components/q-ui-bootstrap/Btn';

export default function About(){
	// let history = useHistory();
  // const [data, setData] = useState();
	
	useEffect(() => {
		// bcApi.onmessage = (e) => {

		// }
		const onBc = (e) => {
			console.log(e);
			if(e.data === "logout"){
				window.location.replace("/login");
				// window.history.replaceState(null, null, "/login");
				// history.replace("/");
			}

			// if(e.data === "closeBC"){
			// 	setBcState(false);
			// 	bcApi.removeEventListener('message', onBc);
			// }
			// if(e.data === "openBC"){
			// 	setBcState(true);
			// 	bcApi.addEventListener('message', onBc);
			// }
		}

		bcApi.addEventListener('message', onBc);

		return () => {
			console.log('useEffect removeEventListener');
			bcApi.removeEventListener('message', onBc);
		}
	}, []);

	return (
		<div className="jumbotron jumbotron-fluid text-center">
			<Head title="About" />
			
			<h1 className="display-4">About Programmeria</h1>
			<p className="lead">Programmeria is web app</p>
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
