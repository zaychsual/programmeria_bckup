import React from 'react';// { useState, useEffect } 
// import * as Yup from 'yup';
// import { useFormik } from 'formik';// useFormik | Formik

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
// import InputQ from '../../components/q-ui-react/InputQ';
import Btn from '../../components/q-ui-react/Btn';
// import Loops from '../../components/q-ui-react/Loops';

const FormGroup = ({ 
	label, 
	id, 
	iSize, 
	classNames, 
	formText, 
	formTextClass, 
	type = "text", 
	children, 
	...etc 
}) => {
	return (
		<div className={"form-group" + (classNames ? " " + classNames : "")}>
			<label htmlFor={id}>{label}</label>
			<input {...etc} 
				type={type} 
				className={"form-control" + (iSize ? " form-control-" + iSize : "")} 
				id={id} 
			/>

			{formText && 
				<small className={"form-text" + (formTextClass ? " " + formTextClass : "")}>
					{formText}
				</small>
			}

			{children} 
		</div>
	)
}

export default function Settings(){
	// const [lang, setLang] = useState(Qlang());
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Settings','color:yellow;');
	// }, []);

	const onSave = (e, valid) => {
		console.log('onSave valid: ', valid);
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Settings" />
			
			<h1>Settings</h1>

			{/* <Btn onClick={() => console.log('uniq: ', Q.uniq())}>uniq</Btn>{" "}
			<Btn onClick={() => console.log('uniq: ', Q.Qid())}>Qid</Btn> */}

			<Form noValidate
				className="card" 
				onSubmit={onSave}
			>
				<div className="card-body">
					<FormGroup 
						label="App Name" 
						id="appName" 
						required 
					/>

					<FormGroup 
						label="Email" 
						type="email" 
						id="email" 
						required 
					/>

					

					<Btn type="submit">Save</Btn>
				</div>
			</Form>
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
