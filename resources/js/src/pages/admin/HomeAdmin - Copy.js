import React, { useState } from 'react';// { useState, useRef, useEffect }
// import { Redirect } from 'react-router-dom';// Redirect, useHistory
// import { useDocumentVisibility } from 'ahooks';
// import axios from 'axios';
import Collapse from 'react-bootstrap/Collapse';

// import { AuthContext } from '../../context/AuthContext';
import Head from '../../components/q-ui-react/Head';
// import Btn from '../../components/q-ui-react/Btn';
// import Img from '../../components/q-ui-react/Img';
// import Loops from '../../components/q-ui-react/Loops';

// let battery;

// function toTime(sec) {
// 	sec = parseInt(sec, 10);

// 	let hours = Math.floor(sec / 3600),
// 			minutes = Math.floor((sec - (hours * 3600)) / 60),
// 			seconds = sec - (hours * 3600) - (minutes * 60);

// 	if (hours < 10) { hours   = '0' + hours; }
// 	if (minutes < 10) { minutes = '0' + minutes; }
// 	if (seconds < 10) { seconds = '0' + seconds; }

// 	return hours + ':' + minutes;
// }

// function readBattery(battery){
// 	// battery = b || battery;

// 	let percentage = parseFloat((battery.level * 100).toFixed(2)) + '%',
// 			fully,
// 			remaining;

// 	if (battery.charging && battery.chargingTime === Infinity) {
// 		fully = 'Calculating...';
// 	} else if (battery.chargingTime !== Infinity) {
// 		fully = toTime(battery.chargingTime);
// 	} else {
// 		fully = '---';
// 	}

// 	if (!battery.charging && battery.dischargingTime === Infinity) {
// 		remaining = 'Calculating...';
// 	} else if (battery.dischargingTime !== Infinity) {
// 		remaining = toTime(battery.dischargingTime);
// 	} else {
// 		remaining = '---';
// 	}

// 	// document.styleSheets[0].insertRule('.battery:before{width:' + percentage + '}', 0);
// 	// document.querySelector('.battery-percentage').innerHTML = percentage;
// 	// document.querySelector('.battery-status').innerHTML = battery.charging ? 'Adapter' : 'Battery';
// 	// document.querySelector('.battery-level').innerHTML = percentage;
// 	// document.querySelector('.battery-fully').innerHTML = fully;
// 	// document.querySelector('.battery-remaining').innerHTML = remaining;

// 	return {
// 		battery, 
// 		percentage, 
// 		fully, 
// 		remaining, 
// 		charging: battery.charging ? 'Adapter' : 'Battery',
// 	}
// }

const DATAS = [
	{
		"path": "/package.json",
		"type": "file",
		// isFile: true, 
		"contentType": "application/json", // contentType
		"integrity": "sha384-Pdp+4TbCSkCdnlbhVzYTcNg0VdRQEnLsrB8A18y/tptwTZUq6Zn8BnWcA4Om7E3I",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 759
	}, {
		"path": "/build-info.json",
		"type": "file", 
		"contentType": "application/json",
		"integrity": "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 163
	}, {
		"path": "/index.js",
		"type": "file",
		"contentType": "application/javascript",
		"integrity": "sha384-uYepeL3qyzb/7G5T1fizoxPoKmV6ftFXdz4jeQlBff4HqMuNVJPqiNjvN38BeHUk",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 190
	}, {
		"path": "/LICENSE",
		"type": "file",
		"contentType": "text/plain",
		"integrity": "sha384-dIN5gxuW3odCH42AKpxql36fRQlrVsjCi4EJgGC2Ty8Ihkcq9cdrChEZpKhSAfks",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 1086
	}, {
		"path": "/README.md",
		"type": "file",
		"contentType": "text/markdown",
		"integrity": "sha384-WuEvTet+scWBliRT6iLsyZZfFLBEFU/QBmfzdOvLj9mfU4CNvX7WbMosvPxHLjrS",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 737
	}, {
		"path": "/cjs",
		"type": "directory",
		// isDirectory: true, 
		"files": [{
			"path": "/cjs/react.development.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-M/8nyoIVh1dygRP3vsJsXo7izkbkAKPA2bON89F2e26vho6tVvv08kWLp2hZ7M86",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 74430
		}, {
			"path": "/cjs/react.production.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-sbVXGGS221NzmofOIH3f7+u0yCox/aqW276fniG9ttEWvZV8NSnpj+fFo7ZxrjRf",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 6645
		}]
	}, {
		"path": "/umd",
		"type": "directory",
		// isDirectory: true, 
		"files": [{
			"path": "/umd/react.development.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-5l9PFjfzS2E5Gx88inFinqBHRa+gZtXaPxAFp9a54+T34j/Mp4dKFndKLBWJs9dz",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 115653
		}, {
			"path": "/umd/react.production.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-3LaF+GDeugL4KEmnsbb+HuxO42PMnX+W5VYrAF98SHC8Wq47T6D7WpBckxQuazOA",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 12588
		}, {
			"path": "/umd/react.profiling.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-lclrAlvhGDKbAhot+O7b99hyYkco0LGhaVbQwwBOkCfdx+OE3/Qonk2VvSvU1tVN",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 16191
		}]
	}
];

// const TREE = [
// 	{path:"/public", files: [
// 		{path:"/public/index.html"},
// 		{path:"/public/favicon.ico"}
// 	]},
// 	{path:"/src", files: [
// 		{path:"/src/index.js"},
// 		{path:"/src/App.js"}
// 	]}
// ];

const Tree = ({
	className, 
	id, 
	label, 
	children, 
	open = false, 
	timeout = 90, // 150 
	mountOnEnter = true, 

	role = "tree", 
	onToggle = Q.noop, 
	...etc
}) => {
	const [see, setSee] = useState(open);

	const toggle = (e) => {
		setSee(!see);
		onToggle(!see, e);
	}

	return (
		<div className={className}>
			{label && 
				<button  type="button" 
					onClick={e => toggle(e)} 
					className="nav-link btn text-left w-100" 
					role="treeitem" 
					aria-controls={id} 
					aria-expanded={see} 
				>{label}</button>
			}

			<Collapse 
				{...etc} 
				mountOnEnter={mountOnEnter} 
				timeout={timeout} 
				in={see} 
				role={role} 
			>
				{children && 
					<div className="pl-3" id={id} aria-hidden={!see}>
						{children}
					</div>
				}
			</Collapse>
		</div>
	);
}

export default function HomeAdmin(){
	// const [files, setFiles] = useState(null);
	// const [battery, setBattery] = useState(null);
	const [data, setData] = useState(DATAS);// null | [] | false

	/* useEffect(() => {
		// navigator.getBattery().then(b => {
		// 	if(b){
		// 		const bt = readBattery(b);
		// 		if(bt){
		// 			setBattery(bt);
		// 		}
		// 	}
		// });

		let bt = null;
		// const batteryApi = () => {
			if(navigator.battery){
				bt = readBattery(navigator.battery);
				if(bt){
					setBattery(bt);
					console.log(bt);
					bt.battery.addEventListener('chargingchange', () => {
						readBattery(navigator.battery);
					});
				}
			} 
			else if(navigator.getBattery){
				navigator.getBattery().then(b => {
					if(b){
						bt = readBattery(b);
						if(bt){
							setBattery(bt);
							console.log(bt);

							bt.battery.addEventListener('chargingchange', () => {
								navigator.getBattery().then(b2 => {
									let bt2 = readBattery(b2);
									if(bt2){
										console.log(bt2);
										setBattery(bt2);
									}
								});
							});
						}
					}
				}).catch(e => {
					console.log(e);
					setBattery(false);
				});
			} 
			else{
				setBattery(false);
			}
		// }

		// batteryApi();

		// if(bt){
		// 	bt.battery.addEventListener('chargingchange', () => {
		// 		readBattery();
		// 	});
		// }

		return () => {
			if(bt){
				bt.battery.removeEventListener('chargingchange', () => {
					readBattery()
				});
			}
		};
	}, []); */

	// const openTree = (v) => {
	// 	const dataOpens = opens.map(o => (o.path === v.path ? { ...o, open: !o.open } : o) );
	// 	console.log('dataOpens: ', dataOpens);
	// 	setOpens(dataOpens);
	// }

	return (
		<div className="container-fluid py-3">
			<Head title="Home" />
			
			<h1>Home Admin</h1>

			{/* <button onClick={() => setOpens(opens.map((o, i) => (i === 6 ? { ...o, open: !o.open } : o) ))} className="btn btn-info" type="button">OPEN </button> */}

			<div className="nav link-sm flex-column col-3 px-0">
				{data.map((v, i) => {
					if(v.type === "file"){
						return <button key={i} className="nav-link btn text-left" type="button" role="treeitem">{v.path}</button>;
					}

					return (
						<Tree key={i} label={v.path}>
							{v.files.map((f, fi) => 
								<button key={fi} className="nav-link btn text-left w-100" type="button" role="treeitem">{f.path}</button>
							)}
						</Tree>
					)
				})}
			</div>

			<hr/>

			<details open={false}>
				<summary onClick={Q.preventQ}>Details</summary>
				<div>
					Content
				</div>
			</details>

			{/* <h4>Battery API</h4>

			{battery !== null ? 
				<div>
					<b className="battery" 
						style={{ '--battery-val': battery.percentage }} 
					>
						{battery.percentage}
					</b>
				</div>
				: 
				Q.isBool(battery) && 
					<div className="alert alert-danger" role="alert">Your browser doesn't support the Battery Status API</div>
			} */}
		</div>
	);
}

// Sequence generator function (commonly referred to as "range", e.g. Clojure, PHP etc)
// const range = (start, stop, step) => Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));

// Generate the alphabet using Array.from making use of it being ordered as a sequence
// const ALPAHABET = range('A'.charCodeAt(0), 'Z'.charCodeAt(0), 1).map(x => String.fromCharCode(x));

// const Qlang = () => {
//   let l = navigator.language;
//   return l[0] + l[1];
// }

// const auth = useContext(AuthContext);
// const docVisibility = useDocumentVisibility();
// // let history = useHistory();
// const [bc, setBc] = useState(true);// !auth?.isAuthenticated()

// React.useEffect(() => {
		// bcApi.onmessage = (e) => {

		// }

	// 	bcApi.addEventListener('message', handler);

	// 	return () => bcApi.removeEventListener('message', handler);

// 	Q.importJS("/storage/app_modules/@babel-standalone/babel.min.js");
// }, []);

// if(!auth?.isAuthenticated()) return null;// <Redirect to="/" />

/* <Btn onClick={() => bcApi.postMessage("logout")}>Logout</Btn>{" "}
	<Btn 
		onClick={() => {
			setBc(!bc);
			// bcApi.close();
			// bcApi.postMessage(bc ? 'closeBC' : 'openBC');
		}}
	>Close Broadcast Channel</Btn>

	<input className="form-control" type="text" 
		onChange={e => {
			if(bc){
				let value = e.target.value;
				// if(bc){
					bcApi.postMessage(value);// Message from HomeAdmin.js: ' + 
			}
		}}
	/> */