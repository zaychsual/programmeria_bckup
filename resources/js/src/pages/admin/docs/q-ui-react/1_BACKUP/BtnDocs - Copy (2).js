import React from 'react';
import MainContent from '../../../parts/MainContent';
import Loops from '../../../components/q-ui-react/Loops';
import Btn from '../../../components/q-react-bootstrap/Btn';

// import { InView } from '../../../components-dev/react-intersection-observer';// useInView | InView
import LiveCode from '../../../components-dev/live-code/LiveCode';// LiveCode | Ngoding

export default class BtnDocs extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			// codeFirst: null,
			// codeLoad: false,
			codes: [], // null
    };
  }

  componentDidMount(){
		console.log('%ccomponentDidMount in BtnDocs','color:yellow;');
		axios({
			url: '/storage/json/docs/components/q-ui-react/Btn.json'
		}).then(v => {
			let codes = v.data;
			console.log(codes);
			if(v.status === 200 && Array.isArray(codes)){
				this.setState({codes});
			}
		}).catch(e => console.log(e));
  }
	
	onAdd = () => {
		this.setState(s => ({
			codes: [{label:"", code:"", edited:true}, ...s.codes]
		}));
	}
	
	// onChange = () => {e => this.setState({codes: [] })}

  render(){
		const {codes} = this.state;// codeFirst, codeLoad, 

    return (
			<MainContent title="Q-UI-React - Btn" 
				headChild={
					<React.Fragment>
						<Btn onClick={this.onAdd} size="sm" kind="info">Add</Btn>
						{/* <Btn size="sm">Save</Btn> */}
					</React.Fragment>
				}
			>
				<div className="p-3 mt-3-next">				
					{codes.map((v,i) =>  
						<div key={i + v.label + '-docs'}>
							{v.edited ? 
								<input defaultValue={v.label} className="form-control form-control-sm mb-2" placeholder="Label / title" type="text" />
								:
								<h5>{v.label}</h5>
							}
							<LiveCode // LiveCode | Ngoding
								renderInc={i} 
								// theme="dark" 
								// id={i} 
								// name={v.label} 
								height={235} 
								scope={{Btn}} 
								code={v.code} 
								// className="mb-3" 
								// viewClass="p-3" 
								// onRender={(renderInc) => {
									// console.log('onRender: ', i, renderInc);
								// }}
							/>
						</div>
					)}
					
				</div>
			</MainContent>
    );
  }
}

/*
								<React.Fragment>
									<h5>{v.label}</h5>
									<LiveCode 
										// theme="dark" 
										scope={{Btn}} 
										code={v.code} 
										viewClass="p-3" 
										onRender={() => {
											console.log('onRender 1');
											// this.setState({code1: });
										}}
									/>
								</React.Fragment>
								
								<InView 
									triggerOnce 
									threshold={1} 
								>
									{({ inView, ref, entry }) => 
										<div ref={ref}>
											{inView && 
												<React.Fragment>
													<h5>{v.label} - {inView + ''}</h5>
													<LiveCode // inRef={ref} 
														// theme="dark" 
														scope={{Btn}} 
														code={v.code} 
														viewClass="p-3" 
														onRender={() => {
															console.log('onRender: ', i);
															// this.setState({code2: })
														}}
													/>
													<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
												</React.Fragment>
											}
										</div>
									}
								</InView>
								
								<InView // as="div" 
									triggerOnce 
									threshold={1} 
									onChange={(inView, entry) => console.log('Inview: ', inView)}
								>
									<React.Fragment>
										<h5>{v.label}</h5>
										<LiveCode // inRef={ref} 
											// theme="dark" 
											scope={{Btn}} 
											code={v.code} 
											viewClass="p-3" 
											onRender={() => {
												console.log('onRender: ', i);
												// this.setState({code2: })
											}}
										/>
										<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
									</React.Fragment>
								</InView>
								


<React.Fragment></React.Fragment>
*/
