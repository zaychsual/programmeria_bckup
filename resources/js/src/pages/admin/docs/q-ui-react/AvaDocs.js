import React from 'react';// , { useState, useEffect, Fragment }

import DocsPage from '../DocsPage';

// import Flex from '../../../../components/q-ui-react/Flex';
import Ava from '../../../../components/q-ui-react/Ava';

export default function AvaDocs(){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in AvaDocs','color:yellow;');
	// }, []);

	return (
		<DocsPage 
			api="/storage/json/docs/components/q-ui-react/Ava.json" // Table | Btn
			scope={{ Ava }} 
		/>
	);
}

