import React from 'react';// , { useState, useEffect, Fragment }
import Dropdown from 'react-bootstrap/Dropdown';

import DocsPage from '../DocsPage';

// import Browser from '../../../../components/browser/Browser';
import Flex from '../../../../components/q-ui-react/Flex';
import Aroute from '../../../../components/q-ui-react/Aroute';

export default function ArouteDocs(){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ArouteDocs','color:yellow;');
	// }, []);

	return (
		<DocsPage 
			browser 
			api="/storage/json/docs/components/q-ui-react/Aroute.json" 
			scope={{ Dropdown, Flex, Aroute }} // Browser
		/>
	);
}
