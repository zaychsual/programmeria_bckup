import React, { useState } from 'react';// , { useState, useEffect, Fragment }
import { Modal, ModalHeader } from "reactstrap";// , ModalBody, ModalFooter

import DocsPage from '../DocsPage';

// import Flex from '../../../../components/q-ui-react/Flex';
// import Btn from '../../../../components/q-ui-react/Btn';

export default function ModalPage(){
  // const [modalTop, setModalTop] = useState(false);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ModalPage','color:yellow;');
	// }, []);

	return (
		<DocsPage 
			api="/storage/json/docs/components/q-ui-react/Modal.json" 
			scope={{ useState, Modal, ModalHeader }} 
		/>
	);
}

