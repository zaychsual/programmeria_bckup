import React from 'react';
import axios from 'axios';

// import MainContent from '../../../parts/MainContent';
// import { InView } from '../../../components-dev/react-intersection-observer';// useInView | InView
import LiveCode from '../../../../components/live-code/LiveCode';// LiveCode | Ngoding

import Btn from '../../../../components/q-ui-react/Btn';
// import Modal from '../../../components/q-react-bootstrap/Modal';
// import ModalHeader from '../../../components/q-react-bootstrap/ModalHeader';

import Form from '../../../../components/q-ui-react/Form';// Loops
import { ControlledEditor } from '../../../../components/monaco-react';// {ControlledEditor} | Editor
import onDidMount from '../../../../components/monaco-react/ide/onDidMount';

// import { LazyLoadComponent, trackWindowScroll } from '../../../components/react-lazy-load-image-component';
// import Ava from '../../../components/q-ui-react/Ava';

/** NOT FIX: hack load babel */
export default class BtnDocs extends React.Component{
  constructor(props){
    super(props);
    this.state = {
			docFirst: null,
			docsLoad: false,
			docs: [], // null
			
			modal: false,
			// editorReady: false,
			addVal: '',
			newDocs: []
    };
		
		this.addTitleRef = React.createRef();
		// this.addIdeRef = React.createRef();
  }

  componentDidMount(){
		// console.log('%ccomponentDidMount in BtnDocs','color:yellow;');
		// Get data docs:
		axios({
			url: '/storage/json/docs/components/q-ui-react/Btn.json'
		}).then(v => {
			let docs = v.data;
			// console.log(docs);
			if(v.status === 200 && Array.isArray(docs)){
				if(window.Babel){
					this.setState({docs, docsLoad: true});
				}else{
					this.setState({docFirst: docs[0]}, () => {
						// const docs = data.filter((v,i) => i !== 0);
						this.setState({docs: docs.filter((v,i) => i !== 0)});
					});
				}
			}
		}).catch(e => console.log(e));
  }
	
	onAdd = (e) => {
		let et = e.target;
		if(et.checkValidity()){
			const {addVal} = this.state;// docFirst, docs
			// console.log('OK', this.addIdeRef.current());
			const newDoc = {
				openIde: true,
				label: this.addTitleRef.current.value,
				code: addVal // this.addIdeRef.current()
			};
			
			this.setState(s => ({
				newDocs: [newDoc, ...s.newDocs], 
				modal:false, 
				// addVal:""
			}));// this.setState({modal: false})
			
			/* if(docFirst){
				this.setState(s => ({
					docs: [docFirst, ...s.docs],
					docFirst: newDoc,
					modal: false
				}));
			}
			else{
				this.setState(s => ({
					docs: [newDoc, ...s.docs],
					modal: false
				}));
			} */
		}

	}
	
	onModal = () => this.setState(s => ({modal: !s.modal}));
	
/** CREATE component / method */
	ideDidMount = (v, editor) => {
		onDidMount(v, editor, true);// {required:true, onMount: noop}
    /* if(!this.state.editorReady){
      this.setState({editorReady: true});
      this.addIdeRef.current = editor;// editor | v
      
      if(editor){ // this.ideMain.current
				// OPTION DEV: set attribute required in monaco textarea
				const ta = domQ("textarea", editor.getDomNode());
				if(ta) setAttr(ta, {required:''});
			
				// Get bind editor contextmenu FROM https://github.com/Microsoft/monaco-editor/issues/484
				editor.onContextMenu(e => {
					let ctxMenu = domQ(".monaco-menu-container", editor.getDomNode());// this.ideMain.current
					if(ctxMenu){
						// window.outerHeight
            let ee = e.event,
                ch = ctxMenu.clientHeight,
                cw = ctxMenu.clientWidth,
                cs = ctxMenu.style;
						const posY = (ee.posy + ch) > window.innerHeight ? ee.posy - ch : ee.posy;
						// window.outerWidth
						const posX = (ee.posx + cw) > document.body.clientWidth ? ee.posx - cw : ee.posx;
				
						cs.position = "fixed";// OPTIONS: set in css internal
						cs.top = Math.max(0, Math.floor(posY)) + "px";
						cs.left = Math.max(0, Math.floor(posX)) + "px";
					}
				});
      }
    } */
	}// onDidMount
	
	onSaveAll = () => {
		const {docFirst, docs, newDocs} = this.state;
		const newDocsOk = newDocs.map((v) => ({label: v.label, code: v.code}));
		const mergeDoc = docFirst ? [...newDocsOk, docFirst, ...docs] : [...newDocsOk, ...docs];
		
		// console.log('newDocsOk: ', newDocsOk);
		console.log(JSON.stringify(mergeDoc, null, 2));
	}

  render(){
		const {docFirst, docsLoad, docs, newDocs, modal, addVal} = this.state;
		const isNewDocs = newDocs.length > 0;
		
    return (
			<div className="p-3 mt-3-next">
				{isNewDocs && 
					newDocs.map((v,i) => 
						<div key={i + v.label + '-newDocs'}>
							<h5>
								{v.label}
								<Btn onClick={() => this.setState({newDocs: newDocs.filter((v2,i2) => i !== i2)})} 
									size="xs" kind="danger" className="float-right q q-close" tip="Remove / cancel" />
							</h5>
							<LiveCode 
								openIde={v.openIde} 
								viewClass="w-100" 
								h={v.height ? v.height : '70vh'} 
								scope={{Btn}} 
								code={v.code} 
								// onRender={() => this.setState({docsLoad: true})}
							/>
						</div>
					)
				}
				
				{docFirst && 
					<div>
						<h5>{docFirst.label}</h5>
						<LiveCode 
							viewClass="w-100" 
							h={docFirst.height ? docFirst.height : '70vh'} 
							scope={{Btn}} 
							code={docFirst.code} 
							onRender={() => this.setState({docsLoad: true})}
						/>
					</div>
				}
			
				{docsLoad && 
					docs.map((v,i) => 
						<div key={i + v.label + '-docs'}>
							{/*v.edited ? 
								<input defaultValue={v.label} className="form-control form-control-sm mb-2" placeholder="Label / title" type="text" />
								:
								<h5>{v.label}</h5>
							*/}
							
							<h5>{v.label}</h5>
							<LiveCode // LiveCode | Ngoding
								// theme="dark" 
								viewClass="w-100" 
								h={v.height ? v.height : '70vh'} 
								scope={{Btn}} 
								code={v.code} 
								// className="mb-3" 
								// viewClass="p-3" 
							/>
						</div>
					)
				}
			</div>
    );
  }
}

/* <Modal unmountOnClose={false} isOpen={modal} size="xl" toggle={this.onModal}
				onClosed={() => {
					if(addVal.length > 0) this.setState({addVal: ''})
				}}
			>
				<ModalHeader As="h6" toggle={this.onModal} modalTitle="Add item" />
				<div className="modal-body p-0">
					<Form noValidate onSubmit={this.onAdd}>
						<div className="p-2">
							<input ref={this.addTitleRef} className="form-control" type="text" placeholder="Label / title" required />
						</div>
						
						<ControlledEditor 
							height="70vh" 
							// language="javascript" 
							// className="mx-min15" 
							value={addVal} 
							onChange={(e, addVal) => this.setState({addVal})} 
							editorDidMount={this.ideDidMount} 
							options={{
								fontSize: 14,
								tabSize: 2, // tabSize
								// dragAndDrop: false,
								minimap: {
									enabled: false, // Defaults = true
									// side: minimapSide, // Default = right | left
									// showSlider: 'always', // Default = mouseover
									// renderCharacters: false, // Default =  true
								},
								// colorDecorators: false, // Css color preview
								// readOnly: true,
								// mouseWheelZoom: zoomIde
							}}
						/>
						
						<button type="submit" id="btnSubmitDoc" hidden />
					</Form>
				</div>
				
				<div className="modal-footer">
					<Btn>Cancel</Btn>
					<Btn As="label" kind="primary" htmlFor="btnSubmitDoc">Save</Btn>
				</div>
			</Modal> */

/*
								<React.Fragment>
									<h5>{v.label}</h5>
									<LiveCode 
										// theme="dark" 
										scope={{Btn}} 
										code={v.code} 
										viewClass="p-3" 
										onRender={() => {
											console.log('onRender 1');
											// this.setState({code1: });
										}}
									/>
								</React.Fragment>
								
								<InView 
									triggerOnce 
									threshold={1} 
								>
									{({ inView, ref, entry }) => 
										<div ref={ref}>
											{inView && 
												<React.Fragment>
													<h5>{v.label} - {inView + ''}</h5>
													<LiveCode // inRef={ref} 
														// theme="dark" 
														scope={{Btn}} 
														code={v.code} 
														viewClass="p-3" 
														onRender={() => {
															console.log('onRender: ', i);
															// this.setState({code2: })
														}}
													/>
													<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
												</React.Fragment>
											}
										</div>
									}
								</InView>
								
								<InView // as="div" 
									triggerOnce 
									threshold={1} 
									onChange={(inView, entry) => console.log('Inview: ', inView)}
								>
									<React.Fragment>
										<h5>{v.label}</h5>
										<LiveCode // inRef={ref} 
											// theme="dark" 
											scope={{Btn}} 
											code={v.code} 
											viewClass="p-3" 
											onRender={() => {
												console.log('onRender: ', i);
												// this.setState({code2: })
											}}
										/>
										<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
									</React.Fragment>
								</InView>
								


<React.Fragment></React.Fragment>
*/
