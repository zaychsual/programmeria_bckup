import React, { useState } from 'react';// , { useState, useEffect, Fragment }

import DocsPage from '../DocsPage';

import Flex from '../../../../components/q-ui-react/Flex';
import Table from '../../../../components/q-ui-react/Table';

export default function TableDocs(){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in TableDocs','color:yellow;');
	// }, []);

	return (
		<DocsPage 
			// title="Table" 
			api="/storage/json/docs/components/q-ui-react/Table.json" // Table | Btn
			scope={{ useState, Table, Flex }} 
		/>
	);
}

/*
<React.Fragment></React.Fragment>
*/
