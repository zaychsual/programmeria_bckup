import React, { useState } from 'react';// , { useState, useEffect, Fragment }

import DocsPage from '../DocsPage';

// import Flex from '../../../../components/q-ui-react/Flex';
import Loops from '../../../../components/q-ui-react/Loops';

export default function LoopsPage(){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in LoopsPage','color:yellow;');
	// }, []);

	return (
		<DocsPage 
			api="/storage/json/docs/components/q-ui-react/Loops.json" 
			scope={{ useState, Loops }} 
		/>
	);
}

