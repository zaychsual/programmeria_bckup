import React, { useRef, useState } from 'react';// { useState, useRef, useEffect }
// import { useDocumentVisibility } from 'ahooks';
// import axios from 'axios';
import Collapse from 'react-bootstrap/Collapse';
import Dropdown from 'react-bootstrap/Dropdown';
import Downshift from 'downshift';

// import { HexColorPicker, HexColorInput, RgbaColorPicker } from "react-colorful";// RgbaStringColorPicker
// import "react-colorful/dist/index.css";

// import { AuthContext } from '../../context/AuthContext';
import Head from '../../components/q-ui-react/Head';
import Btn from '../../components/q-ui-react/Btn';

import Aroute from '../../components/q-ui-react/Aroute';
// import ColorPicker from '../../components/color-picker/ColorPicker';

// import darkOrLight from '../../utils/darkOrLight';

const DATAS = [
	{
		"path": "/package.json",
		"type": "file",
		// isFile: true, 
		"contentType": "application/json", // contentType
		"integrity": "sha384-Pdp+4TbCSkCdnlbhVzYTcNg0VdRQEnLsrB8A18y/tptwTZUq6Zn8BnWcA4Om7E3I",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 759
	}, {
		"path": "/build-info.json",
		"type": "file", 
		"contentType": "application/json",
		"integrity": "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 163
	}, {
		"path": "/index.js",
		"type": "file",
		"contentType": "application/javascript",
		"integrity": "sha384-uYepeL3qyzb/7G5T1fizoxPoKmV6ftFXdz4jeQlBff4HqMuNVJPqiNjvN38BeHUk",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 190
	}, {
		"path": "/LICENSE",
		"type": "file",
		"contentType": "text/plain",
		"integrity": "sha384-dIN5gxuW3odCH42AKpxql36fRQlrVsjCi4EJgGC2Ty8Ihkcq9cdrChEZpKhSAfks",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 1086
	}, {
		"path": "/README.md",
		"type": "file",
		"contentType": "text/markdown",
		"integrity": "sha384-WuEvTet+scWBliRT6iLsyZZfFLBEFU/QBmfzdOvLj9mfU4CNvX7WbMosvPxHLjrS",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 737
	}, {
		"path": "/cjs",
		"type": "directory",
		// isDirectory: true, 
		"files": [{
			"path": "/cjs/react.development.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-M/8nyoIVh1dygRP3vsJsXo7izkbkAKPA2bON89F2e26vho6tVvv08kWLp2hZ7M86",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 74430
		}, {
			"path": "/cjs/react.production.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-sbVXGGS221NzmofOIH3f7+u0yCox/aqW276fniG9ttEWvZV8NSnpj+fFo7ZxrjRf",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 6645
		}]
	}, {
		"path": "/umd",
		"type": "directory",
		// isDirectory: true, 
		"files": [{
			"path": "/umd/react.development.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-5l9PFjfzS2E5Gx88inFinqBHRa+gZtXaPxAFp9a54+T34j/Mp4dKFndKLBWJs9dz",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 115653
		}, {
			"path": "/umd/react.production.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-3LaF+GDeugL4KEmnsbb+HuxO42PMnX+W5VYrAF98SHC8Wq47T6D7WpBckxQuazOA",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 12588
		}, {
			"path": "/umd/react.profiling.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-lclrAlvhGDKbAhot+O7b99hyYkco0LGhaVbQwwBOkCfdx+OE3/Qonk2VvSvU1tVN",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 16191
		}]
	}
];

const Tree = ({
	className, 
	id, 
	label, 
	children, 
	open = false, 
	timeout = 90, // 150 
	mountOnEnter = true, 

	role = "tree", 
	onToggle = Q.noop, 
	...etc
}) => {
	const [see, setSee] = useState(open);

	const toggle = (e) => {
		setSee(!see);
		onToggle(!see, e);
	}

	return (
		<div className={className}>
			{label && 
				<button  type="button" 
					onClick={e => toggle(e)} 
					className="nav-link btn text-left w-100" 
					role="treeitem" 
					aria-controls={id} 
					aria-expanded={see} 
				>{label}</button>
			}

			<Collapse 
				{...etc} 
				mountOnEnter={mountOnEnter} 
				timeout={timeout} 
				in={see} 
				role={role} 
			>
				{children && 
					<div className="pl-3" id={id} aria-hidden={!see}>
						{children}
					</div>
				}
			</Collapse>
		</div>
	);
}

// Downshift
const items = [
  {value: 'apple'},
  {value: 'pear'},
  {value: 'orange'},
  {value: 'grape'},
  {value: 'banana'},
];

export default function HomeAdmin(){
	const [data, setData] = useState(DATAS);// null | [] | false

	return (
		<div className="container-fluid py-3">
			<Head title="Home" />
			
			<h1>Home Admin</h1>
			
			{/*<h5>Q Color Picker</h5>
			<ColorPicker 
				// theme="dark" 
				// color="#aabbcc" 
				// color={{ r: 0, g: 0, b: 0 }} 
				color={{ r: 0, g: 0, b: 0, a: 1 }} 
				// 
				// hex 
				// rgb 
				// rgba 
				
				className="border" 
				onChange={(val, e) => {
					console.log('ColorPicker onChange val: ', val);
					// console.log('ColorPicker onChange e: ', e);
				}}
			/>*/}
			
			<hr/>
			
			<Downshift
				// onChange={selection => {
					// alert(selection ? `You selected ${selection.value}` : 'Selection Cleared')
				// }}
				itemToString={item => (item ? item.value : "")}
			>
				{({
					getInputProps,
					getItemProps,
					// getLabelProps,
					getMenuProps,
					isOpen,
					inputValue,
					highlightedIndex,
					selectedItem,
					getRootProps,
				}) => (
					<Dropdown 
						{...getRootProps({ 
								className:"d-flex flex1", 
								noValidate:true, 
								as: "form", 
								onSubmit: e => {
									Q.preventQ(e);
									console.log('onSubmit');
								}
							}, { 
								suppressRefError: true 
							}
						)} 
						
						// as="form" 
						show={isOpen} 
					>
						<Dropdown.Toggle as="div" bsPrefix="w-100">
							<input 
								{...getInputProps({ 
										className:"form-control form-control-sm shadow-none", 
										type:"search", 
										// autoFocus:true, 
										spellCheck:false, 
										required:true, 
										onKeyDown: e => e.stopPropagation()
									}
								)} 
							/>
						</Dropdown.Toggle>

						<div {...getMenuProps()}>
							<Dropdown.Menu className="dd-sm py-1 w-100">
								{items
									.filter(item => !inputValue || item.value.includes(inputValue))
									.map((item, index) => (										
										<Dropdown.Item {...Q.DD_BTN}
											{...getItemProps({
												key: item.value,
												index,
												item, 
												active: selectedItem === item, // || highlightedIndex === index
												className: highlightedIndex === index && "highlighted"
												// style: {
													// backgroundColor:
														// highlightedIndex === index ? 'lightgray' : 'white',
													// fontWeight: selectedItem === item ? 'bold' : 'normal',
												// },
											})}
										>
											{item.value}
										</Dropdown.Item>
									))
								}
							</Dropdown.Menu>
						</div>
					</Dropdown>
				)}
			</Downshift>
			
			<hr/>

			<div className="nav link-sm flex-column col-3 px-0">
				{data.map((v, i) => {
					if(v.type === "file"){
						return <button key={i} className="nav-link btn text-left" type="button" role="treeitem">{v.path}</button>;
					}

					return (
						<Tree key={i} label={v.path}>
							{v.files.map((f, fi) => 
								<button key={fi} className="nav-link btn text-left w-100" type="button" role="treeitem">{f.path}</button>
							)}
						</Tree>
					)
				})}
			</div>

			<hr/>
			
			<h5>Aroute No New Tab</h5>
			<Aroute 
				noNewTab 
				to="/link-dummy" 
				
			>
				Click me!
			</Aroute>
		</div>
	);
}

/* <li className="breadcrumb-item active" aria-current="page">
	index.html
</li> */