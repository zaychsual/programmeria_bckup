import React, { Fragment } from 'react';

import Head from '../../../components/q-ui-react/Head';
import Base64Encoder from '../../../apps/Base64Encoder'; // SvgToUrl

export default function Base64EncoderPage(){
	return (
		<Fragment>
			<Head title="Base64 Encoder" />

			<Base64Encoder />
		</Fragment>
	)
}