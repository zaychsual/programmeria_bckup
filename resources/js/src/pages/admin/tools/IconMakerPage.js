import React, { Fragment } from 'react';// Component
// import axios from 'axios';

import Head from '../../../components/q-ui-react/Head';
import Iframe from '../../../components/q-ui-react/Iframe';

export default function IconMakerPage(){
	return (
		<Fragment>
			<Head title="Icon Maker" />

			<Iframe 
				// bs 
				title="Icon Maker" 
				src={Q.baseURL + "/iconmaker"} 
				wrapClass="position-relative mh-full-navmain" //  h-100
				className="iframe-app position-absolute position-full" 
				// style={{ height: 'calc(100vh - 48px)' }}
			/>
		</Fragment>
	)
}