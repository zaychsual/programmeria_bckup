import React, { Fragment } from 'react';

import Head from '../../../components/q-ui-react/Head';
import ManifestGenerator from '../../../apps/ManifestGenerator'; // SvgToUrl

export default function ManifestGeneratorPage(){
	return (
		<Fragment>
			<Head title="Manifest Generator" />

			<ManifestGenerator 
				className="p-3" 
				// title="Manifest Generator" 
				prepend={
					<h1 className="h4">Manifest Generator</h1>
				}

			/>

			{/* {Array.from({length:20}).map((v, i) => <p key={i}>DUMMY - {1 + i}</p>)} */}
		</Fragment>
	)
}