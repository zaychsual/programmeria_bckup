import React, { useState, useRef, useEffect } from 'react';// , { useState, useEffect, useRef, } 

import Head from '../../../components/q-ui-react/Head';
import Btn from '../../../components/q-ui-react/Btn';
import Flex from '../../../components/q-ui-react/Flex';
import { ControlledEditor } from '../../../components/monaco-react';
import onDidMount from '../../../components/monaco-react/ide/onDidMount';

import BabelStandalone from '../../../components/libraries/BabelStandalone';
import RollupBrowser from '../../../components/libraries/RollupBrowser';

export default function LoadLibraryPage(){
	const ideRef = useRef(null);
	// const [load, setLoad] = useState(false);
	const [editorReady, setEditorReady] = useState(false);
	const [code, setCode] = useState("");
	
	useEffect(() => {
		console.log('%cuseEffect in LoadLibraryPage','color:yellow;');
		// Q.getScript({
		// 	tag:"link", 
		// 	rel:"stylesheet", 
		// 	href:"https://res.cloudinary.com/finnhvman/raw/upload/matter/matter-0.2.2.min.css"
		// }, "head")
		// 	.then(v => {
		// 		console.log(v);

		// 	}).catch(e => {
		// 		console.log(e);

		// 	});
	}, []);

	const onDidChangeEditor = (v, editor) => {
    if(!editorReady){
			setEditorReady(true);
      ideRef.current = editor;
			
			onDidMount(v, editor);
			// console.log('onDidChangeEditor editor: ', editor);
    }
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Load Library" />

			<h1>Load Library</h1>

			{/* <Btn onClick={() => setLoad(true)}>Load Babel</Btn> */}

			<BabelStandalone 
				load={true}
				// onLoad={Babel => {
				// 	console.log('BabelStandalone onLoad: ', Babel);
				// }}
			>
				{(Babel) => (
					Babel ? 
						<Flex dir="column">
							<Btn onClick={() => {
								console.log('Babel: ', Babel);
							}}>Babel</Btn>

							<ControlledEditor 
								// loading={false} 
								className="monacoFixedCtxMenu"
								height={"75vh"} // HEIGHT_1 
								editorDidMount={onDidChangeEditor} 
								value={code} 
								onChange={(e, val) => setCode(val)} // onChangeCode(e, code) | this.setState({code})
								// language={lang} 
								// theme={theme} 
								// options={MONACO_OPTIONS} 
							/>
						</Flex>
						: 
						<div>Error load Babel</div>
				)}
			</BabelStandalone>

			<RollupBrowser load>
				{rollup => (
					rollup ? 
						<Btn onClick={() => {
							console.log('rollup: ', rollup);
						}}>rollup</Btn>
						: 
						null
				)}
			</RollupBrowser>
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
