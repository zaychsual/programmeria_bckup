import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
// import { useDropzone } from 'react-dropzone';

import Head from '../../../components/q-ui-react/Head';
import Drops from '../../../components/drops/Drops';

export default function DropsPage(){


	return (
		<div className="container">
			<Head title="Drops" />
			
			<Drops />
		</div>
	);
}

/*
      <div {...getRootProps({className: "jumbotron dropzone"})}>
        <input {...getInputProps()} />
        <p className="lead">Drag 'n' drop some files here, or click to select files</p>
      </div>

      <aside>
			<h4>Files</h4>
			<ul>{files}</ul>
			</aside>
*/
