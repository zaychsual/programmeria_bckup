import React from 'react';// , { useState, useEffect, useRef, } 

import Head from '../../../components/q-ui-react/Head';
import Aroute from '../../../components/q-ui-react/Aroute'; // BtnRoute
import FileManager from '../../../apps/file-manager/FileManager';
import Dir from '../../../apps/file-manager/parts/Dir';

function RootDir(){
	return (
		<Dir
			emptyTxt={<><b>programmeria</b> directory is empty.</>} 
		/>
	)
}

function PublicDir(){
	// Public directory
	return (
		<Dir
		
		/>
	)
}

const DATE_DUMMY = new Date();

function ImgDir(){
	return (
		<Dir 
			data={[
				// File folder
				{ type:"folder", name: "FinanSys Flow Landing Page BG long text test data users", path: "/users", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "bg", path: "/bg", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "icons", path: "/icons", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "svg", path: "/programmeria/public/img/svg", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "logos", path: "/programmeria/public/img/logos", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "dummy", path: "/programmeria/public/img/dummy", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "sprite", path: "/programmeria/public/img/sprite", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "posts", path: "/programmeria/public/img/posts", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "avatar", path: "/programmeria/public/img/avatar", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "reports", path: "/programmeria/public/img/reports", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "poster", path: "/programmeria/public/img/poster", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "ads", path: "/programmeria/public/img/ads", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "banner", path: "/programmeria/public/img/banner", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "etc", path: "/programmeria/public/img/etc", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "misc", path: "/programmeria/public/img/misc", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "library", path: "/programmeria/public/img/library", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "thumbs", path: "/programmeria/public/img/thumbs", created_at: DATE_DUMMY }, 
				{ type:"folder", name: "articles", path: "/programmeria/public/img/articles", created_at: DATE_DUMMY }, 
				{ type:"file", name: "angelina-jolie.jpg", path: "/public/img/users/angelina-jolie.jpg", created_at: DATE_DUMMY, size: "145 KB", ext: "jpg" }, 
				{ type:"file", name: "husein.jpg", path: "/public/img/users/husein.jpg", created_at: DATE_DUMMY, size: "182 KB", ext: "jpg" }, 
				{ type:"file", name: "favicon.ico", path: "/public/favicon.ico", created_at: DATE_DUMMY, size: "1.12 KB", ext: "ico" }, 
				{ type:"file", name: "favicon-offline.png", path: "/public/favicon-offline.png", created_at: DATE_DUMMY, size: "624 B", ext: "png" }, 
				{ type:"file", name: "Q.js", path: "/public/js/Q.js", created_at: DATE_DUMMY, size: "27 KB", ext: "js" }, 
				{ type:"file", name: "app.js", path: "/public/css/app.css", created_at: DATE_DUMMY, size: "72 KB", ext: "css" }, 
			]}
		/>
	)
}

function Dir404(){
	return (
		<div>Not Found</div>
	)
}

// const INPUT_ID = "dir-url-" + Q.Qid();

// ["/", "/public", "/img"]
const INIT_PATHS = [
	{ pathname: "/programmeria", label:"programmeria", exact: true }, 
	{ pathname: "/public", label:"public" }, 
	{ pathname: "/img", label:"img" }, 
	// { pathname: "*" }, 
];

const DIRS = [
	RootDir, 
	PublicDir, 
	ImgDir, 
	// Dir404, 
];

const OBJ_404 = { pathname: "*", component: Dir404 };

let ROUTES = INIT_PATHS.map((v, i) => ({ ...v, component: DIRS[i] }));
// ROUTES.push(OBJ_404);

export default function FileManagerPage(){
	// const [pageLoad, setPageLoad] = useState(false);
	
	// useEffect(() => {
		// console.log('%cuseEffect in FileManagerPage','color:yellow;');
	// }, []);

// div className="container-fluid p-2"
	return (
		<div className="container-fluid d-flex flex-column flex-auto mnh-100 p-2">
			<Head title="File Manager" />
			{/*<h1 className="h5">File Manager</h1>*/}

			<FileManager 
				// theme="dark" 
				// view="list" 
				className="flex-auto mnh-100 border rounded shadow-sm" 
				data={INIT_PATHS} 
				routes={ROUTES} 
			/>
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
