import React, { Component } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

import Head from '../../../components/q-ui-react/Head';
import Repl from '../../../apps/repl/Repl';
import { strInsertBefore } from '../../../utils/string';

// INIT TABS CODE:
/* const TABS = [
	// {id:"HTML", lang:"html", name:"HTML", code:"<!doctype html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\t\t<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n\t\t<title>Programmeria</title>\n\t\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" crossorigin=\"anonymous\">\n\t</head>\n\t<body>\n\t\t<!-- Button trigger modal -->\n\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n\t\t\tLaunch demo modal\n\t\t</button>\n\n\t\t<!-- Modal -->\n\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n\t\t\t<div class=\"modal-dialog\">\n\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\tModal content\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<script src=\"https://unpkg.com/jquery@3.5.1/dist/jquery.min.js\" crossorigin=\"anonymous\"></script>\n\t\t<script src=\"https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js\" crossorigin=\"anonymous\"></script>\n\t</body>\n<html>"},
	{id:"HTML", lang:"html", name:"HTML", code:"<!doctype html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\t\t<link rel=\"shortcut icon\" href=\"./favicon.ico\">\n\t\t<title>Programmeria</title>\n\t\t\n\t</head>\n\t<body>\n\t\t<!-- Button trigger modal -->\n\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n\t\t\tLaunch demo modal\n\t\t</button>\n\n\t\t<!-- Modal -->\n\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n\t\t\t<div class=\"modal-dialog\">\n\t\t\t\t<div class=\"modal-content\">\n\t\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>\n\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\tModal content\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</body>\n<html>"},
	{id:"CSS", lang:"css", name:"CSS", code:"body{\n\tbackground-color: lightblue;\n}"},
	// {id:"JS", lang:"javascript", name:"JS", code:"console.log('This JS man', 6+1);\n$('.btn').trigger('click');"},
]; */

export default class ReplPage extends Component{
  constructor(props){
    super(props);
    this.state = {
			// tabs: [...TABS], 
			// tabActive: 0, 
			// lang: "html", // HTML Mode
			// code: TABS[0].code, // HTML Mode & Code
			
			// htmlCode: "", 
			
			// autoRun: false, 
			// script: [
				// "https://unpkg.com/jquery@3.5.1/dist/jquery.min.js", 
				// "https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"
			// ]
    };
		// this.iframe = React.createRef();
  }

  componentDidMount(){
		// console.log('%ccomponentDidMount in ReplPage','color:yellow;');
		// this.setState(s => ({
			// htmlCode: this.addScriptInline(s.tabs)
		// }));
  }
	
	addScriptInline(tabs){
		// const {tabs} = this.state;// script
		// let blob = new Blob([tabs[1].code], {type: 'text/css'});
		// let objUrl = window.URL.createObjectURL(blob);
		// console.log('blob: ', blob);
		// console.log('objUrl: ', objUrl);
		
		// const addScripts = script.map(v => `<script src="${v}" crossorigin="anonymous"></script>`);
		// HTML
		let setHtml = tabs[0].code;

		// DEV OPTION: Insert history.js for manipulate iframe history API
		// setHtml = strInsertBefore(setHtml, "</head>", historyApi);

		// CSS
		if(tabs[1] && tabs[1].code.length) setHtml = strInsertBefore(setHtml, "</head>", `<style>${tabs[1].code}</style>`);

		// Minify Html & Css
		setHtml = setHtml.replace(/\n|\t/gm,'');
		// JS
		if(tabs[2] && tabs[2].code.length) setHtml = strInsertBefore(setHtml, "</body>", `<script>${tabs[2].code}</script>`);// addScripts.join('\n\t') + '\n'
		// console.log('setHtml: ', setHtml.replace(/\n|\t/gm,''));
		return setHtml;// setHtml.replace(/\n|\t/gm,'');// setHtml
	}
	
	setTab = (v, i) => {
		this.setState({
			tabActive: i,
			lang: v.lang, 
			code: v.code
		});
	}
	
	onTab = (v, i, e) => {
		const {tabActive} = this.state;// tabs, code, autoRun
		if(i === tabActive) return;
		// console.log('onClickTab v: ',v);
		const {tabs, code} = this.state;
		const activeTab = tabs[tabActive];
		// console.log('onClickTab activeTab.code: ', activeTab.code);
		if(activeTab.code !== code){
			this.setState(s => {
				const tabs = s.tabs.map(t => { 
					if(activeTab.id === t.id){
						return {...t, code};
					}
					return t;
				});
				const htmlCode = s.autoRun ? this.addScriptInline(tabs) : s.htmlCode;
				return {tabs, htmlCode};
			}, () => {
				this.setTab(v, i);
				// NOT FIX: Add css code to tag style in iframe with click Reload Button
				// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();
			});
		}else{
			this.setTab(v, i);
		}
	}
	
	render(){
		// code, tabs, tabActive, autoRun, htmlCode, 
		// const { lang } = this.state;
		
    return (
			<div className="w-100 mh-full-navmain position-relative">
				<Head title="Repl" />
				
				<Repl 
					// theme="light" 
					aside 
					// autoRun={false}  
					projectSrc="/DUMMY/rollup/08/08.json" 
					className="w-100 h-100 position-absolute position-full" 
					// theme="light" // dark | light
					// presets={['esm']} 
					// type="component" // DEFAULT = native | component 
					/* frameProps={{
						name: "iframeBrowser"
						// className: "repl-web-programming",
						// title: "Test title",
					}}  */
					// frameRefNative={this.iframe} 
					// addStyleParent={false} // 
					// html={htmlCode} // tabs[0].code | html | lang === "html" ? code : "" | `<h1>OKEH</h1>`
					
					// stylesheet={[
						// "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
					// ]}
					
					/* script={[
						// "https://code.jquery.com/jquery-3.5.1.min.js"
						"https://unpkg.com/jquery@3.5.1/dist/jquery.min.js", 
						"https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"
					]}  */
					
					// onLoadIframe={(e, script) => {
					// 	let history = createBrowserHistory({
					// 		window: e.target.contentWindow // iframe.contentWindow
					// 	});
					// 	console.log('onLoadIframe e.target.contentWindow: ', e.target.contentWindow);
					// 	console.log('onLoadIframe history: ', history);

					// 	// this.onLoadIframe(e, script, tabs);
						
					// 	// this.onLoadIframe(e, 'style', tabs[1].code);// ADD CSS
					// 	// this.onLoadIframe(e, 'script', tabs[2].code);// ADD JS
					// }}
					
					// lang={lang} // "html" 
					// code={code} 
					/* onChangeCode={(e, code) => {
						this.setState({code}, () => {
							// const { autoRun } = this.state;
							// console.log('onChangeCode autoRun: ', autoRun);
							// if(autoRun){
							// 	const activeTab = tabs[tabActive];
							// 	// // NOT FIX: Add css code to tag style in iframe with click Reload Button
							// 	// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();

							// 	// this.addScriptInline();

							// 	if(activeTab.code !== code){
							// 		this.setState(s => {
							// 			const tabs = s.tabs.map(t => { 
							// 				if(activeTab.id === t.id){
							// 					return {...t, code};
							// 				}
							// 				return t;
							// 			});
							// 			const htmlCode = s.autoRun ? this.addScriptInline(tabs) : s.htmlCode;
							// 			return {tabs, htmlCode};
							// 		}, () => {
							// 			// this.setTab(v, i);
							// 			// NOT FIX: Add css code to tag style in iframe with click Reload Button
							// 			// if((activeTab.id === "CSS" || activeTab.id === "JS") && autoRun) domQ('.browserBtnReload').click();
							// 		});
							// 	}
							// 	// else{
							// 	// 	this.setTab(v, i);
							// 	// }
							// }
						})
					}}  */
					
					// onChangeLang={e => this.setState({lang: e.target.value})} 
					// onChangeAutoRun={e => this.setState({autoRun: e.target.checked})} 
					/* onRun={e => { // onClickRun
						if(!autoRun){
							// NOT FIX:
							// this.setState(s => ({
								// htmlCode: this.addScriptInline(s.tabs)
							// }));
							
							this.setState(s => {
								const tabs = s.tabs.map(t => { 
									if(s.tabs[tabActive].id === t.id){
										return {...t, code};
									}
									return t;
								});
								// const htmlCode = autoRun ?  : s.htmlCode;
								return {tabs, htmlCode: this.addScriptInline(tabs)};
							});
						}
					}} */
					
					// tabs={tabs} 
					// tabActive={tabActive} 
					
					// onClickTab={this.onTab} 
					
					// onCloseTab={(v, i, e) => {
						// console.log('onCloseTab v: ',v);
						// console.log('onCloseTab i: ',i);
					// }} 
				/>
				
			</div>
    );
  }
}

/*
<React.Fragment></React.Fragment>
*/
