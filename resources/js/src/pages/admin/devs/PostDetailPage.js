import React, { useState, useEffect, Fragment } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import axios from 'axios';
import Dropdown from 'react-bootstrap/Dropdown';

// import Head from '../../../components/q-ui-react/Head';
import PostLayout from '../../../parts/PostLayout';
import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';
import Aroute from '../../../components/q-ui-react/Aroute';
import Img from '../../../components/q-ui-react/Img';
import ModalQ from '../../../components/q-ui-react/ModalQ';
import PlayerQ from '../../../components/player-q/PlayerQ';
import Placeholder from '../../../components/q-ui-react/Placeholder';
import { clipboardCopy } from '../../../utils/clipboard';
import { EXTERNAL_VIDEO_KEY, EXTERNAL_IMG_KEY } from '../../../data/appData';
// import captureVideoFrame from '../../../utils/img/captureVideoFrame';

// const EXTERNAL_VIDEO_KEY = "external-video";
// const EXTERNAL_IMG_KEY = "external-image";

// From Ion Auth CI
// 60 * 60 * 24 * 365 * 2
// if the user_expire is set to zero we'll set the expiration two years from now.
// if($this->config->item('user_expire', 'ion_auth') === 0){
	// $expire = (60*60*24*365*2);
// }

/* const DUMMY_DATA = {
	id: "dummy-video-id", 
	created_at: "Feb 14, 2020", 
	title: "Dummy post data", 
	views: "1,359,848", // NOTE: Number parse to separate
	like: 22, // NOTE: Number parse to k = 22k
	dislike: 438, // NOTE: Number parse to separate
	mediaType: "video", 
	media: {
		poster: "/media/poster/video_360.png", // maxresdefault.webp | video_360.png
		// /media/video_360.mp4
		// https://www.youtube.com/watch?v=ysz5S6PUM-U
		// https://youtu.be/6Ovj_XK6yqc
		// url="/media/video_360.mp4" 
		src: "/media/video_360.mp4" 
		// src: [
			// {src:"/media/SampleVideo_240p_10mb.3gp", type:"video/3gp", sizes:240}, // "data-size"
			// {src:"/media/video_360.mp4", type:"video/mp4", sizes:360},
		// ], 
		// tracks: [
			// {kind:"subtitles", src:"/media/vtt/introduction-subtitle-en.vtt", label:"English", srcLang:"en", default:true},
			// {kind:"subtitles", src:"/media/vtt/introduction-subtitle-id.vtt", label:"Bahasa Indonesia", srcLang:"id"},
		// ]
	}
}; */

function kFormat(num){
	return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num)/1000).toFixed(1)) + "k" : Math.sign(num) * Math.abs(num)
}

// {
	// className, 
	// dataEdit, 
	// data, 
	// children, 
// }
export default function PostDetailPage(){
	const [data, setData] = useState(null);// null | dataEdit
	const [modal, setModal] = useState(false);
  const [tmode, setTmode] = useState(false);
	const [startCheck, setStartCheck] = useState(false);
	const [start, setStart] = useState("0:00");
	// const [like, setLike] = useState(false);
	// const [dislike, setDislike] = useState(false);
	// const [capture, setCapture] = useState(null);
	const [autoPlay, setAutoPlay] = useState(false);
	
	// let likeInit;
	// let dislikeInit;
	useEffect(() => {
		// console.log('%cuseEffect in PostDetailPage','color:yellow;');
		
		// if(!dataEdit){
			axios.get("/DUMMY/json/detail-post-1.json").then(r => {
				console.log('r: ', r);
				if(r.status === 200){
					setData(r.data);
				}
			}).catch(e => {
				console.log('Error: ', e);
			});
			// setTimeout(() => {
				// setData(DUMMY_DATA);
				// likeInit = DUMMY_DATA.like;
				// dislikeInit = DUMMY_DATA.dislike;
				// );
			// }, 3000);
		// }
	}, []);
	
	/* const onLike = () => {
		// Xhr here save dislike data to db
		setLike(!like);
		
		setData({ 
			...data, 
			like: like ? data.like - 1 : data.like + 1
		});
		
		if(dislike){
			setDislike(false);
			setData({ ...data, dislike: data.dislike - 1 });
		}
	};
	
	const onDislike = () => {
		// Xhr here save dislike data to db
		setDislike(!dislike);
		
		setData({ 
			...data, 
			dislike: dislike ? data.dislike - 1 : data.dislike + 1
		});
		
		if(like){
			setLike(false);
			setData({ ...data, like: data.like - 1 });
		}
	}; */
	
	const onModalShare = () => setModal(!modal);
	
	const onCopy = (txt, e) => {
		let et = e?.target;
    clipboardCopy(txt).then(() => {
      if(et){
				Q.setAttr(et, { "aria-label":"Copied!" });
				setTimeout(() => {
					Q.setAttr(et, "aria-label");
				}, 3000);
			}
    }).catch(e => console.log(e));
	};
	
	const onSetStart = e => {
		setStart(e.target.value);// val.length === 0 ? "0:00" : val
	};
	
	const onBlurStart = e => {
		if(e.target.value.length === 0) setStart("0:00");
	};

// DEVS:
	// const onCapture = () => {
		// const frame = captureVideoFrame(".playerQ video", "png");
		// console.log('frame: ', frame);
		// setCapture(frame.uri);// dataUri
	// }
	
	// console.log("data: ", data);
	// console.log(data?.mediaType);

// Q.Cx("container pt-0 pt-md-3 pb-3", className)
	return (
		<PostLayout  
			className="pb-3" 
			data={data} 
			// rowStyle={{ minHeight:'calc(100vh - 115px)' }} 
			// append={}
		>
			{data ? 
				<Fragment>
					<div className={"px-0 px-md-2 col-md-" + (tmode ? 12 : 8)}>
						{(data?.mediaType.startsWith("video/") || data?.mediaType === EXTERNAL_VIDEO_KEY) && // data.mediaType === "video"
							<PlayerQ 
								className="bg-000 shadow-sm" 
								// /media/video_360.mp4
								// https://www.youtube.com/watch?v=ysz5S6PUM-U
								// https://youtu.be/6Ovj_XK6yqc
								// url="/media/video_360.mp4" 
								url={data.media.src} 
								// fileConfig={data.media?.tracks ? { tracks: data.media.tracks } : undefined} 
								tracks={data.media?.tracks || undefined} 
								poster={data.media?.poster} 
								videoID={data.id} 
								wrapStyle={{ height: tmode ? 'calc(83vh - 48px)' : undefined }} // 'calc(100vh - 242px)'
								enablePip 
								theaterMode 
								tmode={tmode} 
								autoPlay={autoPlay} 
								onTheaterMode={() => setTmode(!tmode)} 
								onSetAutoPlay={c => setAutoPlay(c)} 
							/>
						}
							
						{(data?.mediaType.startsWith("image/") || data?.mediaType === EXTERNAL_IMG_KEY) && 
							<Img frame WrapAs="figure" fluid 
								frameClass="mb-0 d-block" 
								className="w-100 bg-dark" 
								style={{ minHeight: 400 }} 
								alt={data.title} 
								src={data.media.src} 
							/>
						}
					</div>
						
					<div className="col-md-8 px-2">
						<h1 className="h5 my-3">{data.title}</h1>
						
						<Flex wrap>
							<div className="fs-14">
								<time>{data.created_at}</time> • {data.views} views
							</div>
							
							<div className="btn-group mt-2 mt-lg-0 ml-0 ml-lg-auto">
								<Btn // onClick={onLike} active={like} 
									kind="light" className="qi qi-thumbs-up">{kFormat(data.like)}</Btn>
								<Btn // onClick={onDislike} active={dislike} 
									kind="light" className="qi qi-thumbs-down">{kFormat(data.dislike)}</Btn>
								<Btn onClick={onModalShare} kind="light" className="qi qi-share">SHARE</Btn>
								<Btn kind="light" className="qi qi-plus">SAVE</Btn>{/* list-music */}
								<Dropdown bsPrefix="btn-group">
									<Dropdown.Toggle variant="light" bsPrefix="rounded-right qi qi-ellipsis-h" />
									<Dropdown.Menu>
										<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-flag">Report</Dropdown.Item>
										<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-file">Open transcript</Dropdown.Item>
									</Dropdown.Menu>
								</Dropdown>
							</div>
						</Flex>
						
						<div className="mt-3 py-3 border-top">
							{Array.from({ length:30 }).map((v, i) => <p key={i}>Dummy - {i + 1}</p>)}
						</div>
					</div>
					
					{/* Next post / latest */}
					<div className={"px-2 col-md-4 mt-1-next " + (tmode ? "mt-4" : "absolute-lg t0 r0")}>
						<Flex className="p-2">
							Up next 
							<label className="custom-control custom-switch ml-auto">
								<input type="checkbox" className="custom-control-input" 
									checked={autoPlay} 
									onChange={e => setAutoPlay(e.target.checked)} 
								/>
								<div className="custom-control-label">Autoplay</div>
							</label>
						</Flex>
					
						{
							[
								{thumb:"/media/poster/poster_SampleVideo_360x240_20mb.png"}, 
								{thumb:"/media/poster/video_360.png"}, 
								{thumb:"/media/poster/video_360.png"}, 
								
							].map((v, i) => 
								<div key={i} className="media position-relative media-post">
									<Img w={120} h={65} className="of-cov mr-2 shadow-sm" alt="Post title" src={v.thumb} />
									
									<div className="media-body lh-normal">
										<Flex As="h6" className="mb-1 text-sm">
											<Aroute noLine stretched kind="dark" to="/post/id" className="d-block pr-3">Post title test long text title user data ok</Aroute>
											
											<Dropdown className="ml-auto zi-2" 
												// drop="left" 
												// onToggle={(open, e) => {
													// console.log(e);
													// if(open && e){
														// Q.setClass(e.target.closest(".media"), "menuOpen");
													// }
												// }}
											>
												<Dropdown.Toggle size="sm" variant="flat" bsPrefix="rounded-right qi qi-ellipsis-v" />
												<Dropdown.Menu alignRight>
													<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-flag">Add to queue</Dropdown.Item>
													<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-clock">Save to Watch later</Dropdown.Item>
													<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-playlist-add q-s13">Save to playlist</Dropdown.Item>
													<hr />
													<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-ban">Not interested</Dropdown.Item>
													<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-flag">Report</Dropdown.Item>
												</Dropdown.Menu>
											</Dropdown>
										</Flex>
										
										<p className="mb-0 small text-muted">
											Paul Gilbert<br />
											477k views • <time>2 years ago</time>
										</p>
										
									</div>
								</div>
							)
						}
					</div>
					
					{/* DEVS */}
					{/* <div className="position-fixed l0 r0 b0 text-center zi-upModal">
						{capture && 
							<Img fluid alt="Capture" src={capture} />
						}
						<div className="btn-group">
							<Btn onClick={onCapture} className="far fa-camera">Capture Frame</Btn>
						</div>
					</div> */}
					
					<ModalQ 
						open={modal} 
						toggle={onModalShare} 
						title="Share" 
						position="center" 
						body={
							<div>
								<div className="input-group">
									<input readOnly className="form-control" value={"https://programmeria.com/" + data.id} type="text" />
									<div className="input-group-append">
										<Btn onClick={e => onCopy("https://programmeria.com/" + data.id, e)} kind="light" className="tip tipTL">Copy</Btn>
									</div>
								</div>
								
								<hr/>
								
								<label className="custom-control custom-checkbox d-inline-block">
									<input type="checkbox" className="custom-control-input" 
										checked={startCheck} 
										onChange={e => setStartCheck(e.target.checked)} 
									/>
									<div className="custom-control-label">Start at</div>
								</label>{" "}
								
								<input className="form-control form-control-sm d-inline-block w-95px" type="text" 
									readOnly={!startCheck} 
									value={start} 
									onChange={onSetStart} 
									onBlur={onBlurStart}
								/>
							</div>
						}
					/>
				</Fragment>
				: 
				<Fragment>
					<div className="px-2 col-md-8">
						<Placeholder type="thumb" 
							h={370} // {screen.height / 2} // "55vh" 
							// className="card-img-top"
							// label="Post" 
						/>
						
						<Placeholder type="h5" className="w-50 my-3" />
						<Flex>
							<Placeholder w="40%" />
							<Placeholder className="flex1 ml-5" />
						</Flex>
					</div>
					
					<div className="px-2 col-md-4 mt-1-next">
						<Placeholder type="h3" className="pb-1" />
					
						{[1,2,3].map(v => 
							<div key={v} className="media media-post">
								<Placeholder w={120} h={65} className="mr-2" />
								
								<div className="media-body lh-normal">
									<Placeholder type="h6" className="mb-1" />
									<Placeholder className="mb-0" />
								</div>
							</div>
						)}
					</div>
				</Fragment>
			}
		</PostLayout>
	);
}

/*
<React.Fragment></React.Fragment>
*/
