import React, { useRef, useState } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

import Head from '../../../components/q-ui-react/Head';
import Btn from '../../../components/q-ui-react/Btn';
// import Iframe from '../../../components/q-ui-react/Iframe';
// import PlayerQ from '../../../components/player-q/PlayerQ';
import Img from '../../../components/q-ui-react/Img';
import captureVideoFrame from '../../../utils/img/captureVideoFrame';

// Options for getDisplayMedia()

const displayMediaOptions = {
  video: {
    cursor: "always"
  },
  audio: false
};

// async function startCapture(videoElem){
// 	// logElem.innerHTML = "";
// 	try {
// 		videoElem.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
// 		dumpOptionsInfo(videoElem);
// 	}catch(e){
// 		console.error("Error: " + e);
// 	}
// }

function startCapture(){
	// let captureStream = null;
	return navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
		//  .catch(err => { console.error("Error:" + err); return null; });
 }

function stopCapture(videoElem){ // evt
	let tracks = videoElem.srcObject.getTracks();

	tracks.forEach(track => track.stop());
	videoElem.srcObject = null;
}

function dumpOptionsInfo(videoElem){ // , cb
	const videoTrack = videoElem.srcObject.getVideoTracks()[0];

	const trackSettings = JSON.stringify(videoTrack.getSettings(), null, 2);
	const getConstraints = JSON.stringify(videoTrack.getConstraints(), null, 2);

	console.log("%cvideoTrack:","color:yellow", videoTrack);

	console.log("%cTrack settings:","color:yellow", trackSettings);
	console.log("%cTrack constraints:","color:yellow", getConstraints);

	// cb({ trackSettings, getConstraints });
}

export default function ScreenCapturePage(){
	const videoRef = useRef(null);
	const [once, setOnce] = useState(false);
	const [isCapture, setIsCapture] = useState(false);
  const [img, setImg] = useState(null);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in ScreenCapturePage','color:yellow;', videoRef);
	// }, []);

	const onCapture = () => {
		if(!once) setOnce(true);

		setTimeout(() => {
			const vid = videoRef.current;

			startCapture(vid).then((data) => {
				console.log('onCapture data: ', data);
	
				vid.srcObject = data;
				dumpOptionsInfo(vid);
				setIsCapture(true);
			})
			.catch(err => { console.error("Error: " + err); return null; });
		}, 9);
	}

	const onStopCapture = () => {
		stopCapture(videoRef.current);
		setIsCapture(false);
	}

	const onSaveImg = () => {
		const frame = captureVideoFrame(videoRef.current, { ext:"png" });
		console.log('frame: ', frame);
		stopCapture(videoRef.current);
		setIsCapture(false);
		setImg(frame.uri);
	}

	return (
		<div className="container-fluid py-3">
			<Head title="Screen Capture API" />

			<Btn onClick={onCapture} className="fal fa-camera">Start Capture</Btn>{" "}
			{isCapture && 
				<>
					<Btn onClick={onStopCapture} kind="danger" className="fal fa-stop">Stop Capture</Btn>{" "}
					<Btn onClick={onSaveImg} className="fal fa-image">Save to image</Btn>
				</>
			}

			<div className="form-row my-3">
				<div className="col-md-6">
					<div className="embed-responsive embed-responsive-16by9 border">
						{once && 
							<video className="embed-responsive-item" 
								id="video" 
								autoPlay 
								ref={videoRef} 
								// src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" 
							/>
						}
					</div>
				</div>

				{img && 
					<div className="col-md-6">
						<Img fluid className="border" alt="Capture" src={img} />
					</div>
				}
			</div>

			<hr/>
			<strong>Log:</strong>
			<pre className="alert alert-danger empty-hide" id="log" />
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
