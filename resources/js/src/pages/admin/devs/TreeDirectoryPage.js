import React, { useState, useEffect } from 'react';// , { useState, useEffect, useRef, } 
// import { DragDropContext } from 'react-dnd';
// import HTML5Backend from 'react-dnd-html5-backend';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';

import Head from '../../../components/q-ui-react/Head';
// import Flex from '../../../components/q-ui-react/Flex';

import Tree from '../../../components/tree-directory/Tree';

const DATAS = [
	{
		id: 1, title: 'Tatooine',
		children: [
			{id: 2, title: 'Endor', children: []},
			{id: 3, title: 'Hoth', children: []},
			{id: 4, title: 'Dagobah', children: []},
		]
	}, {
		id: 5, title: 'Death Star',
		children: []
	}, {
		id: 6, title: 'Alderaan',
		children: [
			{
				id: 7, title: 'Bespin',
				children: [
					{id: 8, title: 'Jakku', children: []}
				]
			}
		]
	}
];

export default function TreeDirectoryPage(){
	const [tree, setTree] = useState(DATAS);
	
	useEffect(() => {
		console.log('%cuseEffect in TreeDirectoryPage','color:yellow;');
		
	}, []);

	const moveItem = (id, afterId, nodeId) => {
    if (id == afterId) return

		// let {tree} = this.state
		let dataTree = tree;

    const removeNode = (id, items) => {
      for (const node of items) {
        if (node.id == id) {
          items.splice(items.indexOf(node), 1)
          return
        }

        if (node.children && node.children.length) {
          removeNode(id, node.children)
        }
      }
    }

    const item = { ...findItem(id, dataTree) }
    if (!item.id) {
      return
    }

    const dest = nodeId ? findItem(nodeId, dataTree).children : dataTree

    if (!afterId) {
      removeNode(id, dataTree)
      dest.push(item)
    } else {
      const index = dest.indexOf(dest.filter(v => v.id == afterId).shift())
      removeNode(id, dataTree)
      dest.splice(index, 0, item)
    }

		// this.setState({tree});
		setTree(dataTree);
  }

  const findItem = (id, items) => {
    for (const node of items) {
      if (node.id == id) return node
      if (node.children && node.children.length) {
        const result = findItem(id, node.children)
        if (result) {
          return result
        }
      }
    }

    return false
  }

	return (
		<div className="container-fluid py-3">
			<Head title="Tree Directory" />

			<h1>Tree Directory</h1>

			<hr/>

			<DndProvider backend={HTML5Backend}>
				<Tree 
					parent={null} 
					items={tree} 
					move={moveItem} 
					find={findItem}
				/>
			</DndProvider>
		</div>
	);
}

// export default DragDropContext(HTML5Backend);
