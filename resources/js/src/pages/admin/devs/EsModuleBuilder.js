import React from 'react';// , { useState, useEffect, useRef, } 

import Head from '../../../components/q-ui-react/Head';
// import Flex from '../../../components/q-ui-react/Flex';
import Textarea from '../../../components/q-ui-react/Textarea';
import Btn from '../../../components/q-ui-react/Btn';

import { evalCode } from '../../../components/live-code/Compiler';// Compiler, 
import { fileRead } from '../../../utils/file/fileRead';

// import Ava from '../../../components/q-ui-react/Ava';
// import SpinLoader from '../../../components/q-ui-react/SpinLoader';
// import PageLoader from '../../../components/PageLoader';

export default class EsModuleBuilder extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			loadBabel: false, 
			fnName: "",
			code: "",
			exportDefault: false,
			Module: null
		}
	}

	componentDidMount(){
		console.log('%ccomponentDidMount in EsModuleBuilder','color:yellow;');

		Q.getScript({src:"/storage/app_modules/@babel-standalone/babel.min.js", "data-js":"Babel"}).then(() => {
			if(window.Babel){
				// window.define = windowDefine;
				this.setState({ loadBabel: true });
			}else{
				// If failed load use jspm.io cdn
				importShim("https://jspm.dev/@babel/standalone").then(m => {
					// window.Babel = m.default;// store Babel to window
					// window.define = windowDefine;
					
					this.setState({ loadBabel: true });
				}).catch(e => console.log('importShim catch: ', e));
			}
		}).catch(e => {
			console.log('catch e: ', e);
		});
	}

	render(){
		const { loadBabel, fnName, code, exportDefault, Module } = this.state;

		return (
			<div className="container-fluid py-3">
				<Head title="Es Module Builder" />

				<h1>Es Module Builder</h1>

				{loadBabel ? 
					<>
						<form noValidate 
							onSubmit={e => {
								let et = e.target;
								Q.preventQ(e);

								if(et.checkValidity()){
									// const transformCode = generateEl(code, {}, (err) => {
									// 	console.log('generateEl err: ', err);
									// });

									// console.log('transformCode: ', transformCode);

									// transformCode(window.Babel, code, {}).then(mod => {
									// 	console.log('transformCode mod: ', mod);
									// }).catch(e => console.log(e));

									let transformCode = evalCode(window.Babel, code.replace(/export|default/gm, "").trim());
									let resCode = "export " + (exportDefault ? "default " : "") + transformCode; // Q.isObj(transformCode)

									let blob = new Blob([resCode], {type: 'text/javascript'});
									// let blobCode = window.URL.createObjectURL(blob);

									// , { readAs: "Text" }
									fileRead(blob).then(r => {
										console.log('r: ', r);
										console.log('r.result: ', r.result);

										importShim(r.result).then(m => {				
											const modName = Object.keys(m);
	
											console.log('importShim m: ', m);
											console.log('importShim modName: ', modName);
											
											if(modName && modName.length > 0){
												if(m.default){
													// console.log('if modName: ', modName);
													// console.log('if m.default: ', m.default);
													this.setState({ Module: m.default });
												}else{
													// console.log('else modName: ', modName);
													// console.log('else m: ', m);
													this.setState({ Module: m[modName[0]] });
												}
	
												// Clear blob from memory
												// window.URL.revokeObjectURL(blobCode);
											}else{
												console.log('module Not Found...!!!');
											}
										}).catch(e => console.log(e));

									}).catch(e => console.log(e));

									console.log('transformCode: ', transformCode);
									console.log('resCode: ', resCode);
									// console.log('blobCode: ', blobCode);

									Q.setClass(et, "was-validated", "remove");
								}else{
									Q.setClass(et, "was-validated");
								}
							}}
						>
							<div className="form-group">
								<label htmlFor="fnName">Function Name</label>
								<input type="text" className="form-control" id="fnName" 
									// required 
									value={fnName} 
									onChange={e => this.setState({ fnName: e.target.value })} 
								/>
							</div>

							<div className="form-group">
								<label htmlFor="code">Code</label>
								<Textarea rows="9" id="code" 
									required 
									value={code} 
									onChange={e => this.setState({ code: e.target.value })}
								/>
							</div>

							<div className="form-group">
								<div className="custom-control custom-checkbox">
									<input type="checkbox" className="custom-control-input" id="exportDefault"  
										checked={exportDefault} 
										onChange={e => this.setState({ exportDefault: e.target.checked })}
									/>
									<label className="custom-control-label" htmlFor="exportDefault">Export default</label>
								</div>
							</div>

							<Btn type="submit">Submit</Btn>
						</form>

						<div className="my-3">
							<h6>Module result:</h6>

							{Module ? 
								<>
									<Btn 
										onClick={() => {
											// window.URL.revokeObjectURL(Module);
											this.setState({ Module: null });
										}}
									>Remove</Btn>
									<hr/>

									{React.createElement(Module, null)}

									{/* <Compiler 
										babel={window.Babel} 
										code={autoRun ? codeAuto : codeNoAuto} 
										scope={this.props.scope} 
										setError={e => {
											// if(e){
												// this.setState({ err: e.replace('/file.tsx: ', '') });
											// }else{
												// this.setState({ err: null });
											// }
											
											this.setState({ err: e ? e.replace('/file.tsx: ', '') : null });
										}}
										// preview={err !== null ? ErrBabel : null} // ???
									/> */}
								</> 
								: 
								null
							}
						</div>
					</>
					: 
					<div>LOADING</div>
				}
			</div>
		);
	}
}

/*
<React.Fragment></React.Fragment>
*/
