import React, { useState, useCallback } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo, Fragment }
import { useDropzone } from 'react-dropzone';
import Dropdown from 'react-bootstrap/Dropdown';
// import { FormattedDateParts } from 'react-intl';
// import DOMPurify from 'dompurify';
// import { CircularProgressbar } from 'react-circular-progressbar';
// import 'react-circular-progressbar/dist/styles.css';

import PostLayout from '../../../parts/PostLayout';
// import PostDetailPage from './PostDetailPage';
import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';
import Img from '../../../components/q-ui-react/Img';
// import ModalQ from '../../../components/q-ui-react/ModalQ';
import Placeholder from '../../../components/q-ui-react/Placeholder';
import PlayerQ from '../../../components/player-q/PlayerQ';

import { fileRead } from '../../../utils/file/fileRead';
import srt2vtt from '../../../utils/srt2vtt';
import { EXTERNAL_VIDEO_KEY, EXTERNAL_IMG_KEY } from '../../../data/appData';

// const EXTERNAL_VIDEO_KEY = "external-video";
// const EXTERNAL_IMG_KEY = "external-image";

function validateExternalUrl(val){
	const urlReg = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; // /^((https?|ftp|file):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
	const isUrl = urlReg.exec(val);
	
	if(isUrl){
		let sUrl;
		if(isUrl[0]?.startsWith("https://youtu.be/")){
			sUrl = "https://www.youtube.com/embed" + isUrl[4].replace("/embed","");
		}else{
			sUrl = isUrl[1] ? val : isUrl[4] ? "https://www.youtube.com/embed" + isUrl[4].replace("/embed","") : 'https://' + isUrl[0];
		}
		return sUrl;
	}
	else{
		// const iframeReg =  /<iframe.*?src=\s*(["'])(.*?)\1/; //  /<iframe.*?src="(.*?)"/;
		const getSrc = /<iframe.*?src=\s*(["'])(.*?)\1/.exec(val);
		if(getSrc){
			console.log(getSrc[2]);
			return getSrc[2];
		}
		return false;
	}
}

const DATE_NOW = () => new Date(Date.now());
const DATE_OPTIONS = { 
	weekday: "long", 
	year: "numeric", 
	month: "short", // long
	day: "numeric" // numeric | 2-digit
};
const INIT_DATE = DATE_NOW().toLocaleDateString(document.documentElement.lang, DATE_OPTIONS).split(",");
const INIT_DATA = {
	id: "MAKE_POST_INIT_DUMMY", 
	// DATE_NOW().toLocaleDateString('en-GB', DATE_OPTIONS),
	// new Intl.DateTimeFormat('en-US', DATE_OPTIONS).format(DATE_NOW()), 
	created_at: INIT_DATE[1] + "," + INIT_DATE[2], 
	title: "Make New Post",
	views: 0,
	like: 0,
	dislike: 0, 
	mediaType: "video/mp4", // "video" | null
	media: {
		// poster: "",
		src: "/media/blank.mp4",
		// tracks: [
		// 	{kind:"subtitles", src:"/media/vtt/introduction-subtitle-en.vtt", label:"English", srcLang:"en", default:true},
		// 	{kind:"subtitles", src:"/media/vtt/introduction-subtitle-id.vtt", label:"Bahasa Indonesia", srcLang:"id"},
		// ]
	}
};

export default function PostEdit(){
	// const fileRef = useRef(null);
	const [load, setLoad] = useState(true);// false
	const [data, setData] = useState(INIT_DATA);
	// const [metaData, setMetaData] = useState(null);
	const [externalMedia, setExternalMedia] = useState("");
	const [tmode, setTmode] = useState(false);
	const [autoPlay, setAutoPlay] = useState(false);
	// const [progressFile, setProgressFile] = useState(null);

	const onDrop = useCallback(files => {
		console.log('onDrop files: ', files);

		const file = files[0];
		// const blob = window.URL.createObjectURL(file);
		// window.URL.revokeObjectURL(this.src);

		if(file && (file.type.startsWith("video/") || file.type.startsWith("image/"))){
			// let fSize = Q.bytes2Size(file.size);
			// console.log('file fSize: ', fSize);

			setData({
				...data, 
				id: Q.Qid(), 
				mediaType: file.type,
				media: {
					...data.media,
					src: window.URL.createObjectURL(file)
				}
			});

			// let fSize = Q.bytes2Size(file.size, false);
			// console.log('file fSize: ', Number(fSize));

			// if(Number(fSize) >= 1.5){
			// 	setData({
			// 		...data, 
			// 		id: Q.Qid(), 
			// 		mediaType: file.type,
			// 		media: {
			// 			...data.media,
			// 			src: window.URL.createObjectURL(file)
			// 		}
			// 	});
			// 	return;
			// }

			// let percent;
			// fileRead(file, {
			// 	onProgress: (e) => {
			// 		const { loaded, total } = e;
			// 		if (loaded && total) {
			// 			percent = Math.round((loaded / total) * 100);
			// 			console.log("Progress: ", percent);
			// 			setProgressFile(percent);
			// 		}
			// 	}
			// }).then(v => {
			// 	console.log('v: ', v);

			// 	let res = v.result;
			// 	if(res?.length <= 0){
			// 		res = window.URL.createObjectURL(file);
			// 	}

			// 	setData({
			// 		...data, 
			// 		id: Q.Qid(), 
			// 		mediaType: file.type,
			// 		media: {
			// 			...data.media,
			// 			src: res
			// 		}
			// 	});

			// 	if(res && percent === 100) setProgressFile(null);
			// }).catch(e => {
			// 	console.log(e);
			// });
	
			setExternalMedia("");// Clear Insert URL
		}

		// setMetaData({
		// 	mime: file.type,
		// 	fileName: file.name,
		// 	// data: mediaUrl.split(',')[1] // imageUrl.substr(imageUrl.indexOf(',') + 1)
		// });
	}, []);
	// noDragEventsBubbling: true
  const { getRootProps, getInputProps, inputRef, isDragActive } = useDropzone({ 
		onDrop, 
		multiple: false, 
		noClick: true, 
		noKeyboard: true 
	});
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in PostEdit','color:yellow;');
	// }, []);

	// CHECK Youtube video url: https://www.youtube.com/watch?v=VppULWpfJZo
	const onClickExternal = () => {
		let validVideoExt = validateExternalUrl(externalMedia);
		console.log('validVideoExt: ', validVideoExt);
		// console.log('data.media.src: ', data.media.src.startsWith('blob:'));

		if(validVideoExt || Q.isAbsoluteUrl(externalMedia)){ // externalMedia.length > 0
			// Clean blob
			if(data.media?.src?.startsWith('blob:')){ // data.mediaType !== EXTERNAL_VIDEO_KEY
				window.URL.revokeObjectURL(data.media.src);
			}

			let mediaType = EXTERNAL_IMG_KEY;
			if(validVideoExt && (validVideoExt.includes("youtube.com/") || validVideoExt.includes("youtu.be/"))){
				mediaType = EXTERNAL_VIDEO_KEY;
			}

			setData({
				...data, 
				id: Q.Qid(), 
				mediaType, 
				media: {
					...data.media,
					src: validVideoExt || externalMedia
				}
			});

			document.body.click();
		}
	}

	const onEnter = e => {
		if(e.target.value.length > 0 && e.key === "Enter"){
			onClickExternal();
		}
	}

	const	renderAddMediaToool = (size) => (
		<Dropdown>
			<Dropdown.Toggle size={size}>Add media</Dropdown.Toggle>
			<Dropdown.Menu alignRight className="w-400">
				<h6 className="dropdown-header">Video / image URL</h6>
				<Dropdown.Item as="button" type="button" className="qi qi-upload q-mr" // id="btnUploadFile" 
					onClick={() => inputRef.current.click()}
				>Upload file</Dropdown.Item>

				{/* <Dropdown.Item as="button" type="button"></Dropdown.Item> */}
				<label className="input-group input-group-sm py-2 px-3">
					<input type="search" className="form-control" placeholder="Insert URL" // id="extMediaInput"
						value={externalMedia} 
						onChange={e => setExternalMedia(e.target.value)} 
						onKeyUp={onEnter} 
					/>
					<div className="input-group-append">
						{Q_appData.UA.browser.name === "Firefox" && 
							<Btn As="div" onClick={() => setExternalMedia("")} kind="light" className="qi qi-close xx" hidden={externalMedia.length < 1} />
						}
						
						{/* <Btn As="label" htmlFor="extMediaInput" outline onClick={() => {
							navigator.clipboard.readText().then(v => {
								console.log(v.length);
								if(v?.length > 0) setExternalMedia(v);
							}).catch(e => console.log(e))
						}}>paste</Btn> */}
						<Btn As="div" outline onClick={onClickExternal}>Insert</Btn>
					</div>
				</label>
			</Dropdown.Menu>
		</Dropdown>
	);

	return (
		<div {...getRootProps()} 
			className={Q.Cx("position-relative post-edit", { "dropFile fullpage": isDragActive })} 
			aria-label={isDragActive ? "Drop file here" : undefined} 
		>
			<PostLayout  
				// className="" 
				data={data} 
				rowStyle={{ minHeight:'calc(100vh - 115px)' }} 
				append={
					<div 
						className="p-2 bg-light border-top position-sticky b0 zi-3 mx-min15" 
						// style={{ margin:'0 -15px -1rem' }} 
					>
						<Flex justify="center" align="center" className="ml-1-next">
							<input {...getInputProps()} />

							{/* <div className="input-group input-group-sm w-50">
								<input type="text" className="form-control" placeholder="Choose file" 
									readOnly 
									value={metaData?.fileName || ""} 
								/>
								<div className="input-group-append">
									<Btn outline 
										onClick={() => {
											inputRef.current.click();// open
										}} 
									>Browse</Btn>
								</div>
							</div> */}

							{renderAddMediaToool("sm")} 

							<Btn onClick={() => {
								const setData = {
									...data, 
									// created_at: new Date(Date.now()).toMysqlFormat()
									// created_at: new Intl.DateTimeFormat(document.documentElement.lang, {
									// 	weekday: 'long',
									// 	year: 'numeric',
									// 	month: 'numeric',
									// 	day: 'numeric',
									// 	hour: 'numeric',
									// 	minute: 'numeric',
									// 	second: 'numeric',
									// 	fractionalSecondDigits: 3,
									// 	hour12: true,
									// 	timeZone: 'UTC'
									// }).format(Date.UTC(Date.now()))

									created_at: Q.dateObj(Date.now())
									// created_at: new Date(Date.now()).toISOString().slice(0, 19).replace('T', ' ')
									// created_at: new Date(Date.now()).toLocaleString('en-GB', { timeZone: 'UTC' })
									// created_at: <FormattedDateParts
									// 	value={new Date(Date.now())}
									// 	year="numeric"
									// 	month="long"
									// 	day="2-digit"
									// >
									// 	{parts => parts[0].value + parts[1].value + parts[2].value}
									// </FormattedDateParts>
								};
								console.log('setData: ', setData);
							}} size="sm">Save</Btn>
						</Flex>
					</div>
				}
			>
				{data?.media?.src ? 
					<div className={"px-0 px-md-2 col-md-" + (tmode ? 12 : 8)}>
						<Flex className="mt-2 mr-3 position-absolute t0 r0 zi-5 ml-1-next">
							<Btn kind="danger" className="tip tipB qi qi-close" aria-label="Remove" 
								onClick={() => {
									setData({
										...data, 
										mediaType: null, 
										media: {}
									})
								}} 
							/>

							{(data.id !== "MAKE_POST_INIT_DUMMY" && data.mediaType.startsWith("video/")) && 
								<Btn As="label">
									Add CC
									<input type="file" hidden 
										onChange={e => {
											let file = e.target.files[0];

											if(file && (file.type.startsWith("text/") || file.type === "")){
												let src;

												// if(file.name.split(".").pop() === "srt"){
													// console.log("IS SRT");
													fileRead(file, { readAs:"Text" }).then(v => {
														// DOMPurify.sanitize(srt2vtt(v.result), {ALLOWED_TAGS: ['#text']})
														let vtt = file.name.split(".").pop() === "srt" ? srt2vtt(v.result) : v.result;
														let blob = new Blob([vtt], { type: "text/vtt " }); // plain
														src = window.URL.createObjectURL(blob);

														console.log('v: ', v);
														console.log('vtt: ', vtt);
														// console.log('src: ', src);
														setLoad(false);

														setData({
															...data, 
															media: {
																...data.media, 
																// src: dataPrev.src, 
																tracks: [
																	{kind:"subtitles", src, label:"Indonesia", srcLang:"id", default:true}
																]
															}
														});

														setLoad(true);
													}).catch(e => {
														console.log(e);
													});
												// }
												// else{
												// 	console.log("IS VTT");
												// 	src = window.URL.createObjectURL(file);

												// 	setData({
												// 		...data, 
												// 		media: {
												// 			...data.media, 
												// 			tracks: [
												// 				{kind:"subtitles", src, label:"Bahasa Indonesia", srcLang:"id", default:true}
												// 			]
												// 		}
												// 	});
												// }
											}
										}}
									/>
								</Btn>
							}

							{renderAddMediaToool()}
						</Flex>

						{(load && data?.mediaType.startsWith("video/") || data?.mediaType === EXTERNAL_VIDEO_KEY) && 
							<PlayerQ 
								className="bg-000 shadow-sm" 
								config={{
									youtube: {
										playerVars: { 
											// showinfo: 1,
											controls: 0, 
											modestbranding: 1,
										}, 
									}
								}}
								url={data.media.src} 
								// fileConfig={data.media?.tracks ? { tracks: data.media.tracks } : undefined} 
								tracks={data.media?.tracks || undefined} 
								poster={data.media?.poster} 
								videoID={data.id} 
								wrapStyle={{ height: tmode ? 'calc(83vh - 48px)' : undefined }} // 'calc(100vh - 242px)'
								enablePip 
								theaterMode 
								tmode={tmode} 
								autoPlay={autoPlay} 
								onTheaterMode={() => setTmode(!tmode)} 
								onSetAutoPlay={c => setAutoPlay(c)} 
							/>
						} 

						{(load && data?.mediaType.startsWith("image/") || data?.mediaType === EXTERNAL_IMG_KEY) && 
							<Img frame WrapAs="figure" fluid 
								frameClass="mb-0 d-block" 
								className="w-100 bg-dark" 
								style={{ minHeight: 400 }} 
								alt={data.title} 
								src={data.media.src} 
							/>
						}
					</div>
					: 
					null
				}
							
				<div className="col-md-8 px-2">
					{/* <h1 className="h5 mt-3">{data?.title}</h1> */}
					<input type="text" className="form-control-plaintext form-control-lg h-auto py-0 fw600 mt-3 mb-2" id="postTitle" 
						value={data?.title} 
						onChange={e => {
							setData({
								...data, 
								title: e.target.value
							});
						}}
					/>
					
					<Flex wrap>
						<div className="fs-14">
							<time>{data?.created_at}</time> • {data?.views} views
						</div>
						
						<div className="btn-group mt-2 mt-lg-0 ml-0 ml-lg-auto">
							<Btn kind="light" className="qi qi-thumbs-up">0</Btn>
							<Btn kind="light" className="qi qi-thumbs-down">0</Btn>
							<Btn kind="light" className="qi qi-share">SHARE</Btn>
							<Btn kind="light" className="qi qi-plus">SAVE</Btn>
							<Dropdown bsPrefix="btn-group">
								<Dropdown.Toggle variant="light" bsPrefix="rounded-right qi qi-ellipsis-h" />
								<Dropdown.Menu>
									<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-flag">Report</Dropdown.Item>
									<Dropdown.Item as="button" type="button" className="q-mr qi q-fw qi-file">Open transcript</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown>
						</div>
					</Flex>
					
					<div className="mt-3 py-3 border-top">
						{/* {Array.from({ length:10 }).map((v, i) => <p key={i}>Dummy - {i + 1}</p>)} */}
					</div>
				</div>
						
				{/* Next post / latest */}
				<div className={"px-2 col-md-4 mt-1-next " + (tmode ? "mt-4" : "absolute-lg t0 r0")}>
					<Placeholder type="h3" className="no-animate cauto pb-1" />
					
					{[1,2,3].map(v => 
						<div key={v} className="media media-post">
							<Placeholder w={120} h={65} className="no-animate cauto mr-2" />
							
							<div className="media-body lh-normal">
								<Placeholder type="h6" className="no-animate cauto mb-1" />
								<Placeholder className="no-animate cauto mb-0" />
							</div>
						</div>
					)}
				</div>
						
				{/* DEVS */}
				{/* <div className="position-fixed l0 r0 b0 text-center zi-upModal">
					{capture && 
						<Img fluid alt="Capture" src={capture} />
					}
					<div className="btn-group">
						<Btn onClick={onCapture} className="qi qi-camera">Capture Frame</Btn>
					</div>
				</div> */}
						
				{/* <ModalQ 
					open={modal} 
					toggle={onModalShare} 
					title="Share" 
					position="center" 
					body={
						<div>
							<div className="input-group">
								<input readOnly className="form-control" value={"https://programmeria.com/" + data.id} type="text" />
								<div className="input-group-append">
									<Btn onClick={e => onCopy("https://programmeria.com/" + data.id, e)} kind="light" className="tip tipTL">Copy</Btn>
								</div>
							</div>
							
							<hr/>
							
							<label className="custom-control custom-checkbox d-inline-block">
								<input type="checkbox" className="custom-control-input" 
									checked={startCheck} 
									onChange={e => setStartCheck(e.target.checked)} 
								/>
								<div className="custom-control-label">Start at</div>
							</label>{" "}
							
							<input className="form-control form-control-sm d-inline-block w-95px" type="text" 
								readOnly={!startCheck} 
								value={start} 
								onChange={onSetStart} 
								onBlur={onBlurStart}
							/>
						</div>
					}
				/> */}
			</PostLayout>

			{/* data && 
				<div className="embed-responsive embed-responsive-16by9">
					<video controls className="embed-responsive-item" src={data.media.src}>
						{data.media?.tracks && 
							data.media.tracks.map((v, i) => 
								<track key={i} kind="subtitles" src={v.src} label={v.label} default={v.default} />
							)	
						}
					</video>
				</div>
			*/}
		</div>
	);
}


