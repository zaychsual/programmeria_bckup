import React, { useState, useRef } from 'react';// useEffect, useRef, 
// import axios from 'axios';
// import MediaQuery from 'react-responsive';
import { useFormik } from 'formik';
import * as Yup from "yup";
import { toPng } from 'html-to-image';// , toJpeg, toBlob, toPixelData, toSvg
import html2canvas from 'html2canvas';
// import List from '@researchgate/react-intersection-list';

import Head from '../../../components/q-ui-react/Head';

// DEVS Components:
// import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';
import BgImage from '../../../components/q-ui-react/BgImage';
import Ava from '../../../components/q-ui-react/Ava';
import Img from '../../../components/q-ui-react/Img';
import SpinLoader from '../../../components/q-ui-react/SpinLoader';
import InputGroup from '../../../components/q-ui-react/InputGroup';
import InputQ from '../../../components/q-ui-react/InputQ';
import ModalQ from '../../../components/q-ui-react/ModalQ';
// import Sticky from '../../../components/q-ui-react/Sticky';
import Textarea from '../../../components/q-ui-react/Textarea';
import Range from '../../../components/q-ui-react/Range';
import Details from '../../../components/q-ui-react/Details';
import Scroller from '../../../components/q-ui-react/Scroller';
import { Beforeunload } from '../../../components/q-ui-react/Beforeunload';
import { confirm } from '../../../components/react-confirm/util/confirm';// , confirmComplex
import PageLoader from '../../../components/PageLoader';
import PlayerQ from '../../../components/player-q/PlayerQ';
import ImgEditor from '../../../apps/image-editor/ImgEditor';
import AntdTree from '../../../components/antd/tree/AntdTree';
import IconPicker from '../../../components/q-ui-react/IconPicker';

import NumberField from '../../../components/devs/NumberField';

// ../../../css-modules/Button.module.css
// import btnStles from './Button.module.css';// scss
// END DEVS Components:

// FROM: https://blueprintjs.com/docs/#timezone/timezone-picker
// import * as moment from "moment-timezone";

// function getTimezoneMetadata(timezone, date = new Date()){
// 	const timestamp = date.getTime();
// 	const zone = moment.tz.zone(timezone);
// 	const zonedDate = moment.tz(timestamp, timezone);
// 	const offset = zonedDate.utcOffset();
// 	const offsetAsString = zonedDate.format("Z");

// 	// Only include abbreviations that are not just a repeat of the offset:
// 	// moment-timezone's `abbr` falls back to the time offset if a zone doesn't have an abbr.
// 	const abbr = zone.abbr(timestamp);
// 	const abbreviation = ABBR_REGEX.test(abbr) ? abbr : undefined;

// 	return {
// 			abbreviation,
// 			offset,
// 			offsetAsString,
// 			population: zone.population,
// 			timezone,
// 	};
// }

const FILE_SIZE = 160 * 1024;
const SUPPORTED_FORMATS = [
	"application/javascript",
	"text/javascript",
	"image/jpg",
	"image/jpeg",
	"image/gif",
	"image/png"
];

const INIT_TEXTAREA_VAL = `lorem\nipsum\nsit\namet\ndolor`;

// console.log('btnStles: ', btnStles);

export default function Devs(){
	const [pageLoad, setPageLoad] = useState(false);
	const [progress, setProgress] = useState(0);
	const [numVal, setNumVal] = useState("");
	// const [inputNum, setInputNum] = useState([""]);
	const [lang, setLang] = useState(Q.lang());
	const [draw, setDraw] = useState(null);
	// const [bcState, setBcState] = useState(true);
	const [modal, setModal] = useState(false);
	const [cvs, setCvs] = useState(null);
	const [textareaVal, setTextareaVal] = useState("change me");
	const [sliderVal, setSliderVal] = useState(0);
	
	const dom2Draw = useRef(null);

	// useEffect(() => {
	// 	console.log('%cuseEffect in Devs','color:yellow;');
	// 	// 'https://jspm.dev/@babel/core' | 
	// 	importShim('/storage/js/esm/clipboardApi.js').then(m => {
	// 		console.log(m);
	// 	}).catch(e => console.log(e));
	// }, []);

	// useEffect(() => {
	// 	// Q.domQ('#btnToggleFullscreenPage').click();

	// 	// bcApi.onmessage = (e) => {

	// 	// }
	// 	const onBc = (e) => {
	// 		console.log(e);
	// 		if(e.data === "logout") window.location.replace("/login");

	// 		// if(e.data === "closeBC"){
	// 		// 	setBcState(false);
	// 		// 	bcApi.removeEventListener('message', onBc);
	// 		// }
	// 		// if(e.data === "openBC"){
	// 		// 	setBcState(true);
	// 		// 	bcApi.addEventListener('message', onBc);
	// 		// }
	// 	}

	// 	bcApi.addEventListener('message', onBc);

	// 	return () => bcApi.removeEventListener('message', onBc);
	// }, []);

	const onConfirm = async () => { //  
		const options = {
			// type: "toast",
			// title: "Title",
			// toastInfoText: "11 min ago",
			// icon: <i className="fab fa-react mr-2" />, 

			size: "sm",  
			bodyClass: "text-center", 
			btnsWrapClass: "d-flex w-50 mt-3 mx-auto flex1-all ml-1-next", 
			// backdrop: false, 

			modalProps: {
				// size: "sm",
				// contentClassName: "HELL", 
				// centered: true, 
				// modalClassName: "modal-right", // up | down | left | right
				returnFocusAfterClose: false, 
				onOpened: () => {
					console.log('onOpened');
				},
				onClosed: () => {
					console.log('onClosed');
				}
			}
		};

		if(await confirm(<h6>Are your sure?</h6>, options)){
			console.log('YES');
		}else{
			console.log('NO');
		}

		// confirm("Are your sure?").then(() => {
		// 	console.log('OKEH');
		// }, () => {
		// 	console.log('CANCEL!');
		// });
	}
	
	// const handleOnClickComplex = () => {
	// 	confirmComplex({ message: 'hello' }).then(({ button, input }) => {
	// 		console.log('proceed! pressed:' + button + ' input:' + input);
	// 	}, () => {
	// 		console.log('cancel!');
	// 	});
	// }

	// const setLoads = (id, bool = true) => {
	// 	setBtnLoads(btnLoads.map((v, i) => i === id ? {...v, load: bool} : v));
	// }

  const validationSchema = Yup.object().shape({
    userfile: Yup.mixed()
      .required("File is required")
			.test(
				"fileSize",
				"File too large",
				value => value && value.size <= FILE_SIZE
			)
			.test(
				"fileFormat",
				"Unsupported Format",
				value => value && SUPPORTED_FORMATS.includes(value.type)
			)
  });

  const formik = useFormik({
    initialValues: { userfile: null },
		validationSchema, 
		// validateOnBlur: true,
    onSubmit: (values, { setSubmitting }) => { // , { setFieldError, setSubmitting, setStatus,  }
      console.log('values: ', values);
			setSubmitting(true);
			
			let formData = new FormData();
			formData.append("userfile", values.userfile, values.userfile.name);

			// console.log('file: ',file);
			// console.log('formData: ',formData.get('userfile'));

			fetch("/api/upload/public/js/esm/core", {
				method:'POST', // *GET, POST, PUT, DELETE, etc.
				// cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
				// headers: {
				// 	// 'Content-Type': 'application/json'
				// 	// 'multipart/form-data' | application/x-www-form-urlencoded
				// 	'Content-Type': "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2), 
				// },
				// referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
				body: formData // formData // JSON.stringify(data) // body data type must match "Content-Type" header
			})
			.then(r => r.json()) // json | text
			.then(res => {
				console.log(`%cfetch res: `, 'color:yellow', res);
				// alert('Success upload file: '+ file.name + ' to path: ' + res.data);
			})
			.catch(e => {
				console.log(e);
			});
    }
	});
	
  const onChangeFile = (e, formik) => {
		e.preventDefault();
		let file = e.target.files[0];
		console.log('file: ',file);
    if (file) {
			let reader = new FileReader();
      reader.onloadend = () => formik.setFieldValue("userfile", file); // setFileName(file.name)
      reader.readAsDataURL(file);
      // formik.setFieldValue("userfile", file);
    }
	};

	const onModal = () => {
		setModal(!modal);
		if(cvs && !modal) setCvs(null);
	}
	
  // const itemsRenderer = (items, ref) => (
  //   <ul className="list-group ovyauto" ref={ref} style={{ maxHeight: 200 }}>
  //     {items}
  //   </ul>
  // );

  // const itemRenderer = (index, key) => <li key={key} className="list-group-item">{index}</li>;

	// console.log('formik: ',formik);
	return (
		<div className="container-fluid py-3">
			<Head title="Devs" />

			<Btn 
				// cssModule={btnStles.error}
			>Btn</Btn>
			<hr/>

      {/* <List
        itemCount={1000} 
        itemsRenderer={itemsRenderer} 
        renderItem={itemRenderer} 
      />
			<hr/> */}

			<h6>{"<IconPicker />"}</h6>
			<IconPicker 
				autoFocus 
				label="Icon Picker" 
				// fa-regular-400
				url="/storage/fonts/q-icon-v1.0/selection.json" 
				onClick={(ico) => { // , e
					console.log('onClick ico: ' ,ico);
					// console.log('onClick e: ' , e);
				}}
			/>
			<hr/>

			<h6>{"<Details />"}</h6>
			<p>Default</p>
			<Details 
				open 
				summary="System Requirements" 
				// className="tree-q"
			>
				<p>
					Requires a computer running an operating system. 
					The computer must have some memory and ideally some kind of long-term storage. 
					An input device as well as some form of output device is recommended.
				</p>
			</Details>

			<hr/>

			<p>No render children on mount</p>
			<Details 
				// open 
				renderOpen={false} 
				summary="System Requirements" 
				// className="tree-q"
			>
				<p>
					Requires a computer running an operating system. 
					The computer must have some memory and ideally some kind of long-term storage. 
					An input device as well as some form of output device is recommended.
				</p>
			</Details>

			<hr/>

			{/* <p>Details Tree style</p>
			<div className="col-3 px-0">
				<Details 
					open 
					className="tree-q tree-folder" 
					summaryClass="input-group input-group-sm qi q-fw qi-folder"  
					summary={
						<>
							<div className="form-control btn btn-flat text-left">public</div>
							<div className="input-group-append tree-tool">
								<Btn kind="flat" className="rounded far fa-pen" />
								<Btn kind="flat" className="rounded far fa-times xx" />
							</div>
						</>
					} 
					onClickSummary={e => {
						console.log('onClickSummary e, ', e);
					}}
				>
					<div className="input-group input-group-sm">
						<div className="form-control btn btn-flat text-left fab i-color fa-html5"> index.html</div>
						<div className="input-group-append tree-tool">
							<Btn kind="flat" className="rounded far fa-pen" />
							<Btn kind="flat" className="rounded far fa-times xx" />
						</div>
					</div>

					<div className="input-group input-group-sm">
						<div className="form-control btn btn-flat text-left fab i-color fa-js"> App.js</div>
						<div className="input-group-append tree-tool">
							<Btn kind="flat" className="rounded far fa-pen" />
							<Btn kind="flat" className="rounded far fa-times xx" />
						</div>
					</div>
				</Details>
			</div> */}

			<h6>{"<ImgEditor /> with react-cropper - <Cropper />"}</h6>
			<ImgEditor 
				// pasteFromClipboard={false} 
				wrapClass="bg-light" 

				// https://raw.githubusercontent.com/roadmanfong/react-cropper/master/example/img/child.jpg
				// data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhMVFhUXFhcWGBcXFxUXFRcXFRcXFhcVFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAPkAygMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAQIDBAYHAAj/xAA+EAABAwIDBQYFAgQFBAMAAAABAAIRAyEEBTESQVFhcQYigZGxwRMyodHwI1IUQuHxBzNygsJTc5KiNDVi/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAIDAQQF/8QAJxEAAgICAwACAgEFAQAAAAAAAAECEQMhEjFBIjITUUIEI2FxgRT/2gAMAwEAAhEDEQA/AOpAJYXgEsKJ0CQlhKlQAkJYSwlQYJC8lXkANcFFfeqOa5/QofM7vftFz5BZPG9t6rp+GwNbxdc/RK8kYjKDZuS+FEcW3e5s8JErk2Y9pqzpmo93IGw8rIQ7GVXXvHGVn5b8N/HXp2+nim/uHmFL8YcQuBVMwey+0ev5orGX9sa7DZ7iOFz5FOm34I0kd32kpXPMg7aS9ratQQddoAbp1W5wma0ao7jwVtmUWh0TSeSkYZCfC2wor24JWxKm2UmyiwoSEhToSQlNGwkIT4SQgBkJE4hJCAJQEq8lQaeheSpUGCLyVVMdjmU2kuItdAEteu1gLnEAC5JXOO1HbsmWUXQ2YkHvH/du6C6Edse1j67ixpimNAN/MlZzB0do3F/QKUpX/orGJdoPfUO3UPdF4lJWxG0YGm7+yrZhjQO6LAWVjKsPbaOpufsovS5Mqu6RIWhoDj/f+iq1S99924f0Ct4h224/tG7j4KOrtTEEuO4XgcLIiwkgfVYxl3d4/T6oe/MXSQ3u8reoR9nZ/EVbu7o4G3ordPsiZvB9VVZIrsm8cn0ZEY5+vm038kRwOMdIdTcWvGlyCI4FaOr2MDm8Cs3m+RVcOQ6ZE63TxywlpCSxTjs6h2H7YGoRQxAipo1255Hut2HHgvnPD4p4Ic3UGbE2I3grt3YrPRiqAM/qNs8e/imENAlSr0LTBqQp0JEGjYSJ6QhADCkToXoQBIvBR/FCVtUIAkXlC/EgKpWx/BAEmY4vYba54LmnbHMXuPw5vqdbcFqcwxpuZ8Vz7M3bTjxN1DJLdF8cfQNsi+/nxPBTuOww7nO4a9BzUopXA/OZVbGEkk6NFufmlW2O1SKOHobThtak2HAbyeceqPh8NI8/zoheXwXl37W28bBFGtuAdNT+dB9UmV2zcapE2EpWk9T1OiMZbhNm5+Y3PsOgUWBw8gTvufYIvSZCi5WWiizQpKw2kkoBX6NGbrErHZXNNUsZgWvaQ4Ag7kaNFV6lNa1RidnKc8yR2HcXMks9E/sb2iOHxLToDZw0kHiuhY7ChwIIkEQVy7Ncu+HVLRq02PLcurFPkqZyZYcXaPomjVDmhwMggEdCnrN9hMZ8TC05N2iCtKuhO0c7VMavJyRaYNSQnJFgDUickQaUSVVxGJjeqdbMm6SEPxWJWDWWq2MJ3qB2O3IXUrkprqyVs1IfnWJAETuusk90y7joiWbVyRE6oaGzA3flvJcrduzpgqQjGWJ3n8KH5i4QGjSR4onUd3Z4jyCE1ILgeYjqSB7poGT6JsAyGvPF0eSIYZm08DiQPdUMM6KbObijOQiSXncT9kmT1jRDNNsRy/PsiNNqG0qslWv41jdXCeCii/SCdAIlQcguEx1MmNtvmjWHqM12gfFUihZMmcFWrBWxVbxUGIe1M0KmUagWF7b0QypTf+6W+IuPdbXE4um3VwHisT/iTVaaNEtMzVsRya6VuJfJC5vqafsBjdmG7jZdDC4t2QxkEAm9j/Vdhy+vtMBXRifaOTKvSykSrysSESJyRBo1InJFgHLcfLbkGVUpY13FGce+SgmLY2dLp/yr1B+N+FmnjAbGxSVaipvpGJFwoRWNxuU8kVxuI8JPlTIcW6SmsFvzjHoE3EvgbXAJ2DE7A5yfC3uVw+HZ6NzB27h/YfVDMWAC3gCPMkfZEK5lxP5+aIPm9WNnrtew91TGvBMj0WKhinS/OC0OQECiTy+pus3nDu7T6T9AjmTv/RA3n2BS5F8TYfYnZWc6wOyN7t/h909r8I2z3X5uMlBs5w2JiKbe6LkyhGEygOguc4ukEyDFjJHQi2qaGNNXZuTI06SNm7CUXAOY9wG4w7Z5d42VrL21KTgHOt6qPB4Ciyk00tplQA7QG0aRkk7Ja83sYkK0abnUxMAg2gyIvokyKtWPjbfaNHSeSJQPO8RUJ2GmBvP9kay8zT8Fnu0ODqPaWsJbOpGvnuCSO2NJ0gcxlNpmoXO4zMfRDu2dKmaFN1M2+IDEyPldfyCc7IaGw4VGPD4bsupl5O0Bdx23EX4IRXyquzDB1RxLfiWaeYd3p/NV1KKTTs5pSk1TiW8oqbL2kaQPUfddg7N4raaBP91xbCOtO9vpK6d2TxNmndCSLqZk18TdBeTaZsnLrOU8kSryAEKRR16oaFQOKKeONy6MckjnVOuS4yZsqlZ0lXH0g3RUHFQytN6L400tkmGqwYKjxdIQSFXrPi6ne+WTxCS9D1sE5o/uxxICs4V0B3INb4mT7hDcyq99o/LwAre1DOp9BAUGtFk9kNWp3SeMn6wPRBMc/bMdGoji6to5e91Uy+ltVG9QT5yqw1slPei3nfzNbwAH0lG8mEbA5T6IJiG7dT/d9BYI1hXQ4ch9lLJ9Ui2NfKzV0aYcNE9uV0zfZUOBqWCL4czZSiyzQ2ll7Bu91DjqceVkYp04QPOq3e2R+2U8uhIrZZyt3dVllEOkIflrzYIpTEOvCVDNFZ+Cb+0IR2owm1hngDSHf+JBP0laeqwaoLnzooVSf+m70Kb0WtHM6dmu/wBI89qPYroPY100xyv9Fz3FPlro3lo8yT7rovYmnHd//JT/AMkRf1ZvME6WhWVRy11oV5dkejjl2eTajoEpyH46rJgJ4R5OhJOkQ1HF5U4wSkwVCBJVtUnkp1EWMb2zkuOdCHlW8zcqcrifZ2LoqY0pcNV7pb+XTcaFSpVe+7gPZFaC9lOv3sQBw9r+yvYhwlo6eqrUGTWe7h7pMS8zP5uUntoqtWDalWQ49fU/dEcoGy0vOp0QrDM2nFu6T7WRyi2SG8L/AGVJ6VCQ3slwdGL+HifwnwU9Z3eBGkwnU9Z3Cw6nU+ClNK4HD1XM3s6YoLZXiZstBhaqyuHZBkI5gnEpGVD3xybIRmmFf8TbYZBaARwibjjqpHY0M1I8VGMyad8rewX+AZh8Jimu2iQ4HSBs7J89FoMDSeCS902gDnvuo/45kRteTZ9012ZNlbRtMu/GIsVm+3WODMK4b3lrB4mT9AUX+Nt3GiwP+JWL79GlwDnnxOy30cmxLlKiOaXGAKwVWXNnSZ8tPVdO7InvjmFyrKhous9j23HgPuqtf3EQv4GwwQ16q8q2HGvMqyF0xOSQys+AShUyZV3MH2hRUqfcJXVj+Mb/AGSlt0ebjTwTv47kq1CntGFP/BFNKONOmKnI5dmpuFSY5Ws0d3lSBXnPs70QZg6BzQrDAjaJ4K1m1aC2OMqJtMQTuJnwTPURVuZ57tkc3H8KrYmbfmqZjq129QfqPZWfhyWu3AfVSqlZVu3RBhKAYOpnqVbpvgxvP0UNSx2t4BDfcp+Ao7RuYGpKWTvY0VQUwLhIJjZEnoALlSYOuKku4oV2jxgp0vhiz6ggC9qYN+hP34IZk+ZGnDToljibjyGWVKfE3uEaiGBOy4cECwGYNdcFF6VYcVFovZczXBU6tyBI36HzCFNwrWWI37yT6oxRqSrDKLTqtTZsZOIOo4Ci4WIF5sYHXVO/hKUwwTz/AKouzKKRvsDyCf8AwobYCE47ykLLMhcg7XYr4uNqkaNIpj/aL/8AttLp/aPMBQoOfvgho4uOi45QkuJNySZPNWwRq5HBnlbSD2SUZI5XXVex1O09SubZPTgDmurdkKUMHRZHczJ6iaSmII/NynUQ1ClXUjlYMxrpcrTmxT8FTq3f4q/WHd8F0z0oolHtlHBfMiaGYL5kTlLm+xsOjimPrNLgAVWcVDVP6o6J9YwLrlyRqVI6YStWwVmLpKkqu/THMg+qp4l0uCt1h+na8X8tVuRaSMxPbYPqsLgBvmPqiddwYBPQD1KhyylLTUdZokjiVBiMRJ2tnoDeAueW3R0R0rFpsc8yd+72RUVWUWy+NrVrBx4kqrTn4e3MSJtb6oS+pNOeBI+s+6WuRvKiPNS6s4vdcn8AHLcreFy7aYLXUOFv0WiyNuyb6Kk58Y0hccE5WwA+jVpXEwr2C7QEWdZat+HpuFxF481nc67PFjrBTjOMtSKuLj9Q1ludA77cUdoZg3iuWOwlRp7s+EgotldKu4xtOPXQeKJY12mbHI+mjqDMzEahR4jN2BsucABvJQLA5eBTc58mATqdwXL6uMc593E3OpJ8BwRjx8/RcmVQ8NX2szT452hOwJDRxnVxCzWBp/Uog8/pBJgqMOZzlVTqNEauVh7K6ckLrvZmlFOeg8ly/s/RlwC65lNLZpMHKfNLh22Znei07d1UhTHp66UcwK/n8URrfKUOf8/iiNX5fBdGT+JOPoJa6NE/4zuK9QZJhX/4RqrOcYvYiTZwgumqlzJ0NjioqX+Ym498ujgFxyVzOiLqAOZd11dbpCrUWXJVpjZF0mR7KY1SK+LxIa0N3Tp09ghbqxMlFsdRtfVCqtKAVONFZWSYLGdwsJ006FRUjIc3x+6pAkHopXVNkh35BTuG9E1PWx9Go5joBWkyrGk/M0dR9lnqo8Qr+AeRpfkbH7FTyJNFMbaYcrVySdk66azPut5haQrUmuc0SWieu/6ysZlTQCHG/EHct1goAtouV90dSKjMnozdgKIswlJosxo8AnFt0jwdFqMYPzUxh6pH7H+hXDaDZcF3fOKc0XN4tI8wuL5VhTtnatsWPhaFfBKlI580blEI4gQ0NTcM79SOGiixNeXJ2FOr0Vo3Vm+yRoBBG/TqV1HBO7jegXI8hxjXMA3hdMyDHCpTEG4sUn9PKpNMzOvQuQlamynNXYcgMxYh6vTLPBVswbcFSYd8sK6JbgmTWm0VcL8yKoVhvmRVZn7CHR8/PbFRVniSTzVvH/NwhBMZmEfJBjfw8N6R9jroLiloAiWGwkNlxAmY4noFmssrPDTXqlzr91p3ndZFMsrOqA1XmXExbQNtLW8tPJceW9nVja0PxdHUjXnp4oJjRutzWnqskQAhNbAaud66qcJU9lZqzNuF5+ybXZoJRHEUgNPrr4AKrVaTceW9dSlZzOIyg+IB04o5haQcO6gLKuo8FbwOMLDpbgkyRb6HxzSezVYcECeHotnkdaWgclisFmTHNI0tfxWoyUjYbHOPBcUrXZ2LfRpgV6FXZU0n6/dXKYWpgC84dAhc27QU/hTsCDUMnrpA+pXSM70lc87RUy5wIWwfyCcbiZmq0zG+wVwuDAGneIPqpDQgF5HJvuUPrzHOV1r5HI1xC+FxWyQRoVr8gz51IhwNtD9iufYOttDZ3i4RGhXcw2Bg66kKU8e9djRna30dzy7PadYTICMU3iLFcLwOakEbPdP0K2+RdpnAgOt6FNHI19hJY/0bvFslqpYdxEhWsHig9shP+Gu2GT40cso7KOHadpFFFsc17Z5onLkwiqPnDEYw4mqGus0/KPODA+YzuuhxwpfPwmVO7qdkuMgSR3RDfNQsbYuDiC2DpGpiQesK7lzTtACoWFxEuO1AvqY3pHroZb7FzJz2U20zIIbca3P4fNXsnr/pDlM9ZMKTtJljWNJ70wYgWkXg8Ah+Vk/DDBqRJ6lQbUoF4pxn/wADjcxLASBYb+fDmUIxOPqPMuMckzE4mGi1hOwNJ4uKpzuSxxpbGc2ycXnhxKg2e94qzTdALTx+ibgmbTwOfpBHoU3SZj7RG4MHeIkb41B+ycymNQdofUdRuSPGw82ndyI3hSPw5ZFSmZY63Np/aUGBHAUGu5H83rQZbjXMsROzeBYwguXw7kd8flkWY+CHa7Jv0K5pq2dMXSC7O2NH5XsqDyI666K5T7b4OBNWLHVj93gsP2goBri5umtt3MLNVnyfRWhhjJWRnnlFnTMz7a4LZIFRzjFtljvUgBZI5yKzxstIF9dfosw4J+AxPw3gnQqn/mila7EX9XNun0bPMsOO40bmz5IFmOGiOU+aMUsZLmze39fsl/hyT+GFzwbiXklIztHDOHehWqWKINzfqI+4TcbULnOuSG7p8vEqh8Yzc+HDxXSly7OZvi9Gow+NIsHQeOvlKv4fHudqQ7r3XfngsnQrOB49f7K/hyXmWyCNeClKCRSMmzqXZvNbAXka9FtMO7aAIXGMkxbheTYwOa6r2dxO1Sa7jqmxTp0Jkj6FyF7YTwV5dFkT5fwNEipSbIIeWGD8pBMd5psYv5a70YwGHNTEii2AHExtRAEnS94aNBwVHLs2YHuEBjdktY8CajI+UzeTOsQn4HA16j2lg2gCP1GkWAPzXO6+vikd+lI14E+0OYh79gg/DaDSDv5iGd0OIgfzTzglC8JQLXAayABGlv7hP7RVYxFWBHeIi15El1pFyZUJq7NNlQaTBmLOBuRebgqbj8VQ6l8nZFndRrq3cMgACNwI3KClQM6FJhKMukG8k9UWqWEEeIWt8VQJW7KNdthZT4KmWvYd0xO69jPA3Kno4baP5+FHKOWtFJ4JuRLf9Q0IKjLIloqoXszub0tioevrBPqq+HrFktsWnUHQ+O4opmsPqu5AX8AVRrVmMgFsxrdNB2khZabZZyuo3a7ht9R15Iu94tzt+eCzOHrN29qm0t5SY8yr1TGuMQIt+FZOLs2ElRNj6m3ScCNCY8DH2WccfdEHYmzh1Hghj3WXRiVI58rtkaa9spy8VYgWstxpa4NO7QrU0cV/LxCxbmyiWCxpIDXGCNCoZcfqOjFk8ZcbRPw3jfLj5f0Q5jYcAeCO4YFzXSIc255jQkKrjMNZrx0PqpxlTaKONpMny/CbTXO5mB6eqM4DLtkERclC8DiPhyOP90VyzNgXk7hPoFDJy8LQpHsXSFMtgm38oGpXROwlUuoDatfTmsKysHuBOhv0K2PZDHNFJxf3YcSfAxP0TYnumJlRtmlOWLwXbhtbEijRZtNmC6+7hy5rYSupOzmaPmfB5TFI1qpIaDAa27nEu2bu+Vgkb7+8tXOXta2nSmk1moa6S473Od4ndv3qLKseynTftRUdUMlrxLBB+Yk3Lukab1BiK1MUw0Q5xMlwERqYB3wtq3sLSWiehVNaq34svmATPfEmAS60gc03tNhfhvAAhkd3iY1J4qLKcSGVmOdtENIiCLHjpom5/Sc2u/bmSZuZMG4uFlfNG38GTYQtAB3mN0xr9wr5eNx+hHqhDiQ2Bc281bpUe7cy7mbDoEkl6Ug/A7lGIaTBMn88CpM2xRiPb3WTfgXSNh1+RP0hPf8AGYQ17zB4klT/ABK7TH/I/UEKVWQ517WVB1AvJOhRnK6YOpt9EXqsAG7y91n5ODB4+SM1hsLsCXfnndJiKkAnj9Ai9RgPHlv/AKIPjqN726lbF8nsyS4qkUWPk9QVBXEEhPrkCwUBK64o5ZsReSpEwgqcOaavIAv4HM30iDG03gdY4SjGFrseDsGWn+U/M12ojisxKfTcQZFipTxJ9FYZWuw9XFun4VBl1WHuG8/WVTbjXGzr+qnw7gSAeocNRy5hTcGlsqsib0GKeLLS5vECOW4ojTxj3sFOe7O7fvM8RICDGod8E8RqtB2Yw7XVAXfKL/0XPJF0zoXYrKGUqe3sgOdqeXVapZ7L6xqOhvdYwfgRttMwLrph0c0uz5YrNio8CYaTu3Angm02gz3mjrtewKnp0g0sJMucQSLgAWmfCVXBbtXEjru69Fciy1RdSpvDnONSIIDRstnmXCSJ4DxUmcV/iv8AiRsl24kkkDQgRYKi2mXHutMcpKK4fLHDD1KjmwRBbbvRa4vIF+G5TlSdspG2mkUKT4ME33ga+J3L1UnjE7gq7LOPMD+qstINzuQ0C6C2QUW7Uv05n2T+0+ZUz+k2HHedzeQ5rPVsSdAbaKu0rFit8mDy0uKCNHEhrYBcDx2j9Antxv7nv/OaGylCpwRPmwi/MLQ3zJM+UlVX1vFQyvLVFIHJscXyklIvJhR7RzXk1KgBV5IlWAKEqavAoAeCnhx3EqIFOBQAXy3HmYfBHHet92QwPxtt9MnuQCBqeh3FcvpPhdY/wT+XEci30UZ4ovZaGWXRv8pYwsGx8vDS4/dz6oiqOIwZDviUzDv5h/K7rz5pf4up/wBM+YQtGs+Z8spue57toDYpucXO6FoHUzZOrkOa01A5roaAQ0Q5oAiRIvEXVZ3+SP8AX/xKuZl/l0un/Fif0RdGlyTZw+CrvaRVDxMxsxAA2TtamTMa8kDzrNzVYAwFjD84nV8b41bA38OQVnBf/Ar/AOsetNB2/wCW/qz1cpxiuTb/AGUlJ8Ul+gXtkEKw9/dsq+K3dPcpWafnBXogmICnQmpwWmCgpwKjTkAOXki8gBwSpoTggBV5IvFACyllNSoAWV5IV5YAspQU1KtAkaV03/BfNmtrVMO63xBtNPNmrfK/gVzALUf4d/8A2OG/7n/Fyx9Grs+h0kJUikVP/9k=
				// /img/iron_man.jpg
				// src="/img/iron_man.jpg" 
				onSave={val => {
					console.log('ImgEditor onSave val: ', val);
				}}
			/>
			<hr/>

			{/* <img 
				className="img-fluid w-25" 
				alt="test drag" 
				src="/img/iron_man.jpg" 
				onDrag={e => {
					e.preventDefault();
					// console.log(e.nativeEvent);// pageX | clientX
					console.log(e.clientX);
				}}
			/>
			<hr/> */}

			<h6>{"<Scroller />"}</h6>
			<Scroller 
				className="mx-1" 
				setScrollTo={1000} 
			>
				{Array.from({length:20}).map((v,i) => 
					<div key={i} className={"btn-group" + (i === 7 ? " active":"")}>
						<Btn kind="light">Dummy - {i + 1}</Btn>
						<Btn kind="light" className="qi qi-close xx" />
					</div>
				)}
			</Scroller>
			<hr/>

			<h5>Custom input range - {"<Range />"}</h5>
			{/* <input className="q-range rounded-pill" type="range" 
				min={0} 
				max={100} 
				style={{ '--slider-val': sliderVal + '%' }}
				value={sliderVal} 
				// Number(e.target.value) 
				onChange={e => setSliderVal(e.target.value)} 
			/> */}

			<h6>Uncontrolled:</h6>
			<Range 
				className="rounded-pill mb-3" 
				min={0} 
				max={100} 
				defaultValue={50} 
				// onChange={e => setSliderVal(e.target.value)} 
			/>

			<h6>Controlled:</h6>
			<Range 
				className="rounded-pill mb-3" 
				min={0} 
				max={100} 
				value={sliderVal} 
				onChange={e => setSliderVal(e.target.value)} 
			/>

			<h6>Without cursor grab:</h6>
			<Range 
				grab={false} 
				className="rounded-pill" 
				min={0} 
				max={100} 
				defaultValue={20} 
			/>
			<hr/>

			<h5>progress</h5>
			{/* <progress 
				className="q-progress progress-txt" // animate
				// dir="ltr" 
				max="100" 
				value={progress} 
				aria-label={progress + "%"} 
				style={{ "--val": progress + "%" }}
			/> */}

			<div 
				className="q-progress mb-2" 
				role="progressbar" 
				aria-valuemin="0" 
				aria-valuemax="100" 
				aria-valuenow={progress} 
				aria-label={progress + "%"} 
				style={{ "--val": progress + "%" }}
			/>

			<input type="range" className="custom-range cpoin" // q-range
				// style={{ "--val": "50%" }}
				min="0" 
				// max="5"
				value={progress} 
				onChange={e => setProgress(e.target.value)}
			/>
			<hr/>

			<h5>{"<PlayerQ />"}</h5>
			<div className="col-7">
				<PlayerQ 
					// /media/video_360.mp4
					// https://www.youtube.com/watch?v=ysz5S6PUM-U
					// https://youtu.be/6Ovj_XK6yqc
					url="/media/video_360.mp4" 
					
				/>
			</div>
			<hr/>

			<h5>{"Antd <Tree /> = <AntdTree />"}</h5>
			<AntdTree 
				data={[
					{
						title: 'Folder 1',
						key: '0-0',
						children: [
							{
								title: 'app.js',
								key: '0-0-0',
								icon: <i className="qi i-color qi-js" />,
							},{
								title: 'Component.jsx',
								key: '0-0-1',
								icon: <i className="qi i-color qi-react" />,
							},{
								title: 'style.css',
								key: '0-0-3',
								icon: <i className="qi i-color qi-css" />, // css3 | css3-alt
							},{
								title: 'index.html',
								key: '0-0-4',
								icon: <i className="qi i-color qi-html" />, 
							},{
								title: 'package.json',
								key: '0-0-5',
								icon: <i className="qi i-color qi-json" />, 
							},{
								title: 'routes.php',
								key: '0-0-6',
								icon: <i className="qi i-color qi-php" />, 
							},{
								title: 'apps.sql',
								key: '0-0-7',
								icon: <i className="qi i-color qi-sql" />, 
							},{
								title: '.env',
								key: '0-0-8',
								isLeaf: true,
							},
						],
					},
					{
						title: 'Folder 2',
						key: '0-1',
						children: [
							{
								title: 'leaf 1-0',
								key: '0-1-0',
								isLeaf: true,
							},{
								title: 'leaf 1-1',
								key: '0-1-1',
								isLeaf: true,
							},
						],
					},
				]}
			/>
			<hr/>

			<h5>{"<Textarea />"}</h5>
			<Textarea 
				defaultValue={INIT_TEXTAREA_VAL} 
			/>
			<hr/>

			<h5>{"<Beforeunload />"}</h5>
			<Beforeunload 
				when={textareaVal !== "change me"} // INIT_TEXTAREA_VAL  
				msg="CLOSE ME...!!!" 
			>
				<Textarea 
					value={textareaVal} 
					onChange={e => setTextareaVal(e.target.value)} 
				/>
			</Beforeunload>
			<hr/>

			{/* <h5>{"<Sticky />""}</h5>
			<Sticky 
				El="nav" 
				position={-48} 
				// style={{ top: '-48px' }} 
				className="navbar bg-light zi-upModal" // t0 zi-upModal | t48 zi-2 
				// <div className={Q.Cx("navbar position-sticky bg-light t48 zi-2", {"shadow-sm": see})}>
				// </div>
				onSticky={(data) => {
					console.log('onSticky data: ', data);
					// Q.setClass(data.elm, "shadow-sm", );
					data.elm.classList.toggle("shadow-sm", data?.top);
				}}
			>
				{(data) => 
					<span className="navbar-text">Sticky</span>
				}
			</Sticky> */}

			<hr/>

			<h5>Facebook link</h5>
			<a 
				// https://l.facebook.com/l.php?u=https%3A%2F%2F
				// https://davidwalsh.name/cookiestore?fbclid=IwAR3qY5lGkyGucp4aubMvdUJyiLniUX7ul_zOpaSX8JkMghLZLRdjOmDgWn0
				href="https://davidwalsh.name/cookiestore/?plink=123" 
				// onClick={e => {
				// 	let et = e.target;
				// 	// let href = et.href;
				// }}
				target="_blank" 
				rel="nofollow noopener" 
				onMouseEnter={e => {
					let et = e.target;
					let href = et.href;
					if(href.startsWith(Q.baseURL)){
						// decodeURI | decodeURIComponent
						et.href = decodeURIComponent(href.replace(Q.baseURL + '/blank/?u=',''));
					}
				}}
				onClick={e => {
					e.preventDefault();
					let href = Q.baseURL + '/blank/?u=' + encodeURIComponent(e.target.href);

					// let data = JSON.stringify({
					// 	name: "Husein",
					// 	job: "Programmer"
					// });
				
					// navigator.sendBeacon('https://reqres.in/api/users', data);

					// axios.post('https://reqres.in/api/users', {
					// 	name: "Husein",
					// 	job: "Programmer"
					// }).then(r => {
					// 	console.log(r)
					// }).catch(e => {
					// 	console.log(e)
					// });

					// let formData = new FormData();
					// formData.append("name", "Husein");
					// formData.append("job", "Programmer");

					// fetch('https://reqres.in/api/users', {
					// 	method:'POST', // *GET, POST, PUT, DELETE, etc.
					// 	// cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
					// 	mode: 'no-cors',
					// 	// headers: {
					// 	// 	// 'Content-Type': 'application/x-www-form-urlencoded'
					// 	// 	Accept: "application/json, text/plain, */*",
					// 	// 	'Content-Type': 'application/json;charset=utf-8',
					// 	// 	// 'multipart/form-data' | application/x-www-form-urlencoded
					// 	// 	// 'Content-Type': "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2), 
					// 	// },
					// 	// referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
					// 	body: formData // JSON.stringify(data) // body data type must match "Content-Type" header
					// })
					// .then(r => r.text())
					// .then(json => console.log(json))
					// .catch(e => {
					// 	console.log(e);
					// });

					window.open(href, '_blank');
				}}
				onContextMenu={e => {
					let et = e.target;
					let href = et.href;
					if(!href.startsWith(Q.baseURL)){
						et.href = Q.baseURL + '/blank/?u=' + encodeURIComponent(href);
					}
				}}
			>
				Link to blank
			</a>
			<hr/>

			<h5>{"<Ava />"}</h5>
			<div className="ml-2-next">
				{[
					{src:"/img/avatar_man.png",alt:"Avatar Man", circle:true},
					{src:"/img/users/angelina-jolie.jpg",alt:"Bootstrap Thumbnail", thumb:true}, // , wrapClass:"tip tipT"
					{src:"/img/no_img.png",alt:"Brad Pitt", round:true}, 
					{src:"/img/no_img.png",alt:"Bootstrap Thumbnail", thumb:true}, 
					{src:"/img/no_img.png",alt:"Set Background", bg:"#000000"},
					{src:"/img/no_img.png",alt:"Mike Portnoy", circle:true}, // , bg:"#e3f2fd"
				].map((v, i) => 
					<Ava key={i} 
						// thumb 
						round={v.round} 
						bg={v.bg} 
						// fluid 
						circle={v.circle} 
						thumb={v.thumb} 
						wrapClass="tip tipT" // {v.wrapClass} 
						wrapProps={{
							"aria-label": v.alt
						}} 
						src={v.src} 
						alt={v.alt}
						w={70} // null
						h={70} 
					/>
				)}
			</div>

			<hr/>

			{/* <h5>Ellipsis</h5>
			<div className="d-inline-flex align-items-center text-truncate text-muted far fa-envelope q-mr">
				<input readOnly 
					className="form-control form-control-sm h-auto p-0 bg-transparent text-muted border-0 rounded-0 shadow-none" 
					title="muhamad.husein@email.com" 
					defaultValue="muhamad.husein@email.com" 
					// onFocus={e => {
					// 	e.target.select();
					// }}
				/>
			</div>

			<hr/> */}

			<h5>Capture Web Page / DOM</h5>
			<Btn 
				onClick={() => {
					html2canvas(document.documentElement).then(canvas => {
						console.log('canvas: ', canvas);
						setModal(true);
						// setCvs(canvas);
						setTimeout(() => {
							Q.domQ('#frameCvs').appendChild(canvas);
						}, 1000);
					});
				}}
			>
				html2canvas
			</Btn>{" "}

			<Btn
				onClick={() => {
					toPng(document.documentElement).then((dataUrl) => {
						// var img = new Image();
						// img.src = dataUrl;
						// document.body.appendChild(img);
						setDraw(dataUrl);
						// saveAs(dataUrl, 'dom2img.png');
						// Q.setAttr(dom2Draw.current, "style");
						setModal(true);
					})
					.catch((e) => {
						console.log('oops, something went wrong!', e);
					});
				}}
			>
				html-to-image
			</Btn>

			<ModalQ 
				open={modal} 
				toggle={onModal} 
				title="html2canvas result" 
				// scrollable 
				position="modal-full" 
				bodyClass="p-0" 
				body={
					<div id="frameCvs">
						{draw && <img className="img-fluid" src={draw} alt="Draw from DOM" />}
					</div>
				}
			/>

			<hr/>

			<h5>{"<Img />"}</h5>
			<div className="ml-2-next">
				<Img 
					thumb 
					w={100} 
					h={100} 
					alt="Angelina Jolie" 
					src="/img/users/angelina-jolie.jpg" 
					// drag={null} 
				/>

				<Img 
					thumb 
					// drag 
					w={100} 
					h={100} 
					alt="Brad Pitt" 
					src="/img/users/not.jpg" 
				/>
				
				{/* <Img 
					fluid  
					w={100} 
					h={100} 
					alt="Anne Hathaway" 
					src="https://cdn06.pramborsfm.com/storage/app/media/Prambors/Editorial/ANNE%20HATHAWAY-20200911101938.jpg?tr=w-600,h-400" 
				/> */}
			</div>
			<hr/>

			<h5>Upload Api</h5>
			<form // noValidate 
				onSubmit={formik.handleSubmit}
			>
				<fieldset disabled={formik.isSubmitting}>
					{/* <label className="input-group">
						<div className="form-control">
							{formik?.values?.userfile?.name || "Choose file"}
						</div>

						<input name="userfile" type="file" className="sr-only" 
							id="customFile" 
							// required 
							onChange={e => onChangeFile(e, formik)}
						/>

						<div className="input-group-append">
							<Btn kind="light" className="fal fa-times" 
								hidden={formik?.values?.userfile ? false : true} 
								type="reset" 
								onClick={() => formik.resetForm({})} 
							/>

							<div className="input-group-text">Browse</div>
						</div>
					</label> */}

					<label className="input-group">
						<div className="custom-file">
							<input name="userfile" type="file" className="custom-file-input" 
								id="customFile" 
								onChange={e => onChangeFile(e, formik)}
							/>
							<div className="custom-file-label">{formik?.values?.userfile?.name || "Choose file"}</div>
						</div>

						<div className="input-group-append">
							<Btn kind="light" className="qi qi-close" 
								// disabled={!(formik?.values?.userfile)} 
								type="reset" 
								onClick={() => {
									if(formik?.values?.userfile) formik.resetForm({})
								}} 
							/>
						</div>
					</label>

					{(formik.touched.userfile && formik.errors.userfile) && 
						<div className="invalid-feedback d-block">{formik.errors.userfile}</div>
					}

					{/* {fileName && <small className="form-text text-muted">{fileName}</small>} */}
					<Btn type="submit" className="mt-2">UPLOAD</Btn>
				</fieldset>
			</form>
			<hr/>

			<h5>Dynamic import ES Module</h5>
			<Btn
				onClick={(e) => {
					let et = e.target;

					// copy2Clipboard | https://programmeria.com/js/esm/clipboardApi.js
					// /storage/js/esm/clipboardApi.js
					importShim('/esm/core/clipboard/clipboardApi.js').then(m => {
						console.log(m);
						if(m){
							// window.clipboardApi = m.default;
							// "STRING COPY..."
							m.default(et).then((ok) => {
								console.log('SUCCESS COPY ok: ', ok);
							}).catch(e => console.log(e));
						}
					}).catch(e => console.log(e));
				}}
			>
				Copy ME
			</Btn>

			{/* <h5>Btn</h5>
			<h6>Custom Spinner Bootstrap 4</h6>

			<div className="ml-1-next">
				{btnLoads.map((v, i) => 
					<Btn key={i} 
						kind={v.kind} 
						load={v.load} 
						loadSize="sm" 
						loadKind={v.loadKind} 
						onClick={() => {
							setLoads(i);
							setTimeout(() => setLoads(i, false), 4000);
						}}
					>
						Submit
					</Btn>
				)}
			</div> */}

			{/* <h6>Q-Custom</h6>
			<Btn 
				disabled 
				className="fal fa-paper-plane loads" // loads loads-sm
			>
				<span className="loads-sm" />
				Submit
			</Btn> */}

			<hr/>

			<h5>CSS Helper: </h5>
			<h6>i-load</h6>

			<span className="i-load p-3 position-relative" />
			<span className="i-load p-3 position-relative" 
				style={{ 
					'--bg-i-load': '70px'
				}}
			> </span>

			<hr/>

			<h5>Html input types:</h5>
			<div className="mt-3-next">
				<InputQ wrap label="color" type="color" className="form-control" />
				<InputQ wrap label="number" type="number" className="form-control" />
				<InputQ wrap label="search" type="search" className="form-control" />
				<InputQ wrap label="tel" type="tel" className="form-control" />
				<InputQ wrap label="url" type="url" className="form-control" />
				<InputQ wrap label="date" type="date" className="form-control" />
				<InputQ wrap label="month" type="month" className="form-control" />
				<InputQ wrap label="time" type="time" className="form-control" />
				<InputQ wrap label="week" type="week" className="form-control" />
			</div>

			<hr/>

			{/* <Btn onClick={() => console.log('uniq: ', Q.uniq())}>uniq</Btn>{" "}
			<Btn onClick={() => console.log('uniq: ', Q.Qid())}>Qid</Btn> */}

			<h5>html2canvas</h5>
			<div ref={dom2Draw} className="card">
				<div className="card-body">
					<h1>Home</h1>

					<select className="custom-select" 
						value={lang} 
						onChange={e => {
							let val = e.target.value;
							document.documentElement.lang = val;
							setLang(val);
						}}
					>
						{[
							{lang:"en", txt:"English"},
							{lang:"id", txt:"Indonesia"}
						].map((v, i) => 
							<option key={i} value={v.lang}>{v.txt}</option>
						)}
					</select>

					<Btn kind="info" className="qi qi-book" 
						onClick={() => {
							console.log('Q.lang(): ', Q.lang());
							console.log('draw: ', draw);
						}}
					>
						{" "}GET LANG: {lang}
					</Btn>
				</div>
			</div>

			{/* <Btn block className="mt-2" 
				onClick={() => {
					// let domWidth = dom2Draw.current.getBoundingClientRect();
					if(window.innerWidth !== screen.width){
						let setSize = ((screen.width - 220) - 30) + "px";
						console.log('setSize: ',setSize);
						// dom2Draw.current.style.width = "1013.33px";
						dom2Draw.current.style.width = setSize; 
						// dom2Draw.current.style.height = "174px";
					}

					setTimeout(() => {
						toPng(dom2Draw.current)
						.then((dataUrl) => {
							// var img = new Image();
							// img.src = dataUrl;
							// document.body.appendChild(img);
							setDraw(dataUrl);
							// saveAs(dataUrl, 'dom2img.png');
							Q.setAttr(dom2Draw.current, "style");
						})
						.catch((e) => {
							console.log('oops, something went wrong!', e);
						});
					}, 9);
				}}
			>html2canvas</Btn> */}
			
			{/* {draw && <img className="img-fluid mt-2" src={draw} alt="Draw from DOM" />} */}

			<hr/>

			<h5>react-confirm</h5>
			<Btn onClick={onConfirm}>Confirm</Btn>
			
			<hr/>

			<h5>{`<InputGroup />`}</h5>
			<InputGroup 
				className="mb-3" 
				prepend={
					<div className="input-group-text">Rp. </div>
				}
			>
				<input type="text" className="form-control" />
			</InputGroup>

			<InputGroup 
				className="mb-3" 
				append={
					<Btn As="div" kind="light" className="qi qi-search" />
				}
			>
				<input type="text" className="form-control" />
			</InputGroup>

			<InputGroup 
				className="mb-3" 
				prepend={
					<>
						<span className="input-group-text">*</span>
						<span className="input-group-text">Rp. </span>
					</>
				}
			>
				<input type="text" className="form-control" />
			</InputGroup>

			<InputGroup
				prepend={
					<span className="input-group-text">First and last name</span>
				}
			>
				<input type="text" className="form-control" />
				<input type="text" className="form-control" />
			</InputGroup>

			<hr/>

			<h5>{`<NumberField />`}</h5>
			<label className="d-block">
				Number: 
				<NumberField className="form-control" value={numVal} onChange={val => setNumVal(val)} />
			</label>
			
			<label className="d-block">
				Default input type="number"
				<input className="form-control" type="number" />
			</label>

			<hr/>

			<h5>{`<SpinLoader />`}</h5>
			<SpinLoader 

			/>
			<hr/>

			<h5>{"<PageLoader />"}</h5>
			<Btn onClick={() => {
				setPageLoad(true);
				setTimeout(() => setPageLoad(false), 5000);
			}}>Try Load Page</Btn>

			{pageLoad && 
				<PageLoader 
					progress 
					// top 
					bottom 
					w={Q.UA.platform.type === "mobile" ? null : "calc(100% - 220px)"} 
					left={Q.UA.platform.type === "mobile"} 
					// className="bg-white" 
				>
					
				</PageLoader>
			}

			<hr/>
			
			<h5>{"<BgImage />"}</h5>
			<BgImage 
				noRepeat 
				bgSize="contain" // auto | contain | cover | revert 
				className="bg-light mb-3" 
				url="/img/iron_man.jpg" 
				w={"55vw"} // 715 
				h={400} 
				// style={{width:200, height:200}} 
				// onLoad={(el, e) => {
				//   console.log('onLoad BgImage el: ', el);
				//   // console.log('onLoad BgImage e: ', e);
				// }}
				// onError={(el, e) => {
				//   // console.log('onError BgImage el: ', el);
				//   // console.log('onError BgImage e: ', e);
				// }}
			/>

			<h6>Error background image</h6>
			<BgImage 
				noRepeat 
				As="section" 
				// bgSize="auto" // auto | contain | cover | revert 
				className="mt-2 bg-info" 
				url="/img-not-available.png" 
				w={100} 
				h={100} 
				// style={{width:200, height:200}} 
				// onLoad={(el, e) => {
				//   // console.log('onLoad BgImage e: ', e);
				// }}
				onError={(el, e) => {
					// console.log('onError BgImage url: ', el);
				  console.log('onError BgImage e: ', e);
				}}
			/>

			{/* <br/> */}

			{/* d-inline-grid grid-1fr-1 */}
			{/* <div className="col-2 px-0">
				<div className="ellipsis d-inline-flex align-items-center flex-nowrap text-truncate far fa-user-circle q-mr">
					<div className="small text-truncate">muhamad.husein@finansys.com</div>
				</div>
			</div> */}

			<hr/>

			{/* <h5>Input Number</h5>
			<div className="input-group w-auto">
				{inputNum.map((v, i) => 
					<input key={i} className="form-control flexno w-auto" type="number" 
						value={v} 
						onChange={e => {
							let et = e.target;
							let val = et.value;
							console.log('val: ',val.length);
							
							// setInputNum([val]);

							if(val.length === 4){
								setInputNum([val[0], val.slice(1)]);
								setTimeout(() => et.nextElementSibling.focus(), 9);
							}
							else{
								setInputNum([val]);
							}
						}}
					/>
				)}
			</div> */}

			{/* Array.from({length:30}).map((_, i) => 
				<p key={i}>Dummy main content {i + 1}</p>
			) */}
		</div>
	);
}

/*
<React.Fragment>
	Devs
</React.Fragment>
*/
