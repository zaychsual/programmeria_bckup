import React, { useState, useCallback } from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }
import { useDropzone } from 'react-dropzone';
import Dropdown from 'react-bootstrap/Dropdown'; 

import PostDetailPage from './PostDetailPage';
import Flex from '../../../components/q-ui-react/Flex';
import Btn from '../../../components/q-ui-react/Btn';

// function getBase64(img, cb){
//   const reader = new FileReader();
//   reader.addEventListener('load', () => cb(reader.result));
//   reader.readAsDataURL(img);
// }

const DATE_NOW = new Date(Date.now());
const DATE_OPTIONS = { 
	weekday: "long", 
	year: "numeric", 
	month: "long", 
	day: "numeric" 
};
const INIT_DATA = {
	id: "MAKE_POST",
	created_at: DATE_NOW.toLocaleDateString('en-GB', DATE_OPTIONS),
	title: "Make New Post",
	views: 0,
	like: 0,
	dislike: 0, 
	mediaType: "video/mp4", // "video" | null
	media: {
		// poster: "",
		src: "/media/blank.mp4",
		// tracks: [] 
	}
};

export default function PostEdit(){
	// const fileRef = useRef(null);
	const [data, setData] = useState(INIT_DATA);
	// const [metaData, setMetaData] = useState(null);
	const [externalMedia, setExternalMedia] = useState("");

	const onDrop = useCallback(files => {
		console.log('onDrop files: ', files);

		const file = files[0];
		const blob = window.URL.createObjectURL(file);
		// window.URL.revokeObjectURL(this.src);

		setData({
			...data, 
			mediaType: file.type,
			media: {
				...data.media,
				src: blob
			}
		});

		setExternalMedia("");// Clear Insert URL

		// setMetaData({
		// 	mime: file.type,
		// 	fileName: file.name,
		// 	// data: mediaUrl.split(',')[1] // imageUrl.substr(imageUrl.indexOf(',') + 1)
		// });
	}, []);
	// noDragEventsBubbling: true
  const { getRootProps, getInputProps, inputRef, isDragActive } = useDropzone({ 
		onDrop, 
		multiple: false, 
		noClick: true, 
		noKeyboard: true 
	});
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in PostEdit','color:yellow;');
	// }, []);

	return (
		<div {...getRootProps()} 
			className={Q.Cx("position-relative post-edit", { "drop": isDragActive })} 
			aria-label={isDragActive ? "Drop file here" : undefined} 
		>
			<PostDetailPage 
				data={data} 
			>
				<form 
					className="p-2 bg-light border-top position-sticky b0 zi-3" 
					style={{ margin:'0 -15px -1rem' }}
				>
					{/* <div className="btn-group btn-group-sm">
						
					</div> */}
					<Flex justify="center" align="center" className="ml-1-next">
						<input {...getInputProps()} />

						<Dropdown drop="up">
							<Dropdown.Toggle size="sm" variant="outline-primary">Add media</Dropdown.Toggle>
							<Dropdown.Menu alignRight className="w-400">
								<Dropdown.Item as="button" type="button" className="far fa-upload q-mr"
									onClick={() => {
										inputRef.current.click();// open
									}}
								>Upload file</Dropdown.Item>

								{/* <Dropdown.Item as="button" type="button"></Dropdown.Item> */}
								<div className="input-group input-group-sm py-2 px-3">
									<input type="text" className="form-control" placeholder="Insert URL"  
										value={externalMedia} 
										onChange={e => setExternalMedia(e.target.value)}
									/>
									<div className="input-group-append">
										<Btn outline 
											onClick={() => {
												if(externalMedia.length > 0){
													setData({
														...data, 
														mediaType: "external",
														media: {
															...data.media,
															src: externalMedia
														}
													});
													document.body.click();
												}
											}}
										>Insert</Btn>
									</div>
								</div>
							</Dropdown.Menu>
						</Dropdown>

						{/* <div className="input-group input-group-sm w-50">
							<input type="text" className="form-control" placeholder="Choose file" 
								readOnly 
								value={metaData?.fileName || ""} 
							/>
							<div className="input-group-append">
								<Btn outline 
									onClick={() => {
										inputRef.current.click();// open
									}} 
								>Browse</Btn>
							</div>
						</div> */}

						<Btn size="sm" type="submit">Save</Btn>
					</Flex>
				</form>
			</PostDetailPage>
		</div>
	);
}

/* <div className="custom-file w-auto">
	<input type="file" className="custom-file-input" id="media" 
		onChange={(e) => {
			let file = e.target.files[0];
			console.log('file: ', file);
			getBase64(file, mediaUrl => {
				setData({
					...data, 
					media: {
						...data.media,
						mediaType: file.type,
						src: mediaUrl
					}
				});

				setMetaData({
					mime: file.type,
					fileName: file.name,
					// data: mediaUrl.split(',')[1] // imageUrl.substr(imageUrl.indexOf(',') + 1)
				});
			});
		}}
	/>
	<label className="custom-file-label" htmlFor="media">{metaData?.fileName || "Choose file"}</label>
</div> */
