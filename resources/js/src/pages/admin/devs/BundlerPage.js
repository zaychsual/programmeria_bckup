import React from 'react';// { useState, useEffect, useRef, useContext, useLayoutEffect, useMemo }

import Head from '../../../components/q-ui-react/Head';
import Bundler from '../../../apps/repl/bundler/Bundler';

export default function BundlerPage(){
  // const [data, setData] = useState();
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in BundlerPage','color:yellow;');
	// }, []);

	return (
		<>
			<Head title="Bundler" />
			
			<Bundler />
		</>
	);
}

/*
<React.Fragment></React.Fragment>
*/
