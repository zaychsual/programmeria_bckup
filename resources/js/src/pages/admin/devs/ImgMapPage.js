import React from 'react';// , { useState, useEffect, useRef, } 

import Head from '../../../components/q-ui-react/Head';
import ImgMap from '../../../components/img-map/ImgMap';

// DEVS Components:
// import SpinLoader from '../../../components/q-ui-react/SpinLoader';
// END DEVS Components:

export default function ImgMapPage(){
	// const [pageLoad, setPageLoad] = useState(false);
	
	// useEffect(() => {
		// console.log('%cuseEffect in ImgMapPage','color:yellow;');
	// }, []);

	return (
		<div className="container-fluid mh-full-navmain">
			<Head title="Img Map" />

			<ImgMap 
				
			/>
		</div>
	);
}

/*
<h1>Img Map</h1>

<React.Fragment></React.Fragment>
*/
