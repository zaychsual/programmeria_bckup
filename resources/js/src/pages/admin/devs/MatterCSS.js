import React, { useEffect } from 'react';// , { useState, useEffect, useRef, } 

import Head from '../../../components/q-ui-react/Head';

import Flex from '../../../components/q-ui-react/Flex';

// DEVS Components:
// import BgImage from '../../../components/q-ui-react/BgImage';
// import Ava from '../../../components/q-ui-react/Ava';
// import SpinLoader from '../../../components/q-ui-react/SpinLoader';
// import InputGroup from '../../../components/q-ui-react/InputGroup';
// import ScrollTo from '../../../components/q-ui-react/ScrollTo';

// import PageLoader from '../../../components/PageLoader';
// import NumberField from '../../../components/devs/NumberField';
// END DEVS Components:

export default function MatterCSS(){
	// const [pageLoad, setPageLoad] = useState(false);
	// const [progress, setProgress] = useState(0);
	// const [numVal, setNumVal] = useState("");
	
	useEffect(() => {
		console.log('%cuseEffect in MatterCSS','color:yellow;');
		// Q.getScript({
		// 	tag:"link", 
		// 	rel:"stylesheet", 
		// 	href:"https://res.cloudinary.com/finnhvman/raw/upload/matter/matter-0.2.2.min.css"
		// }, "head")
		// 	.then(v => {
		// 		console.log(v);

		// 	}).catch(e => {
		// 		console.log(e);

		// 	});
	}, []);

	return (
		<div className="container-fluid py-3">
			<Head title="Matter CSS" />

			<h1>Matter CSS - Pure CSS Material</h1>

			<label className="pure-material-slider">
				<input type="range" min={0} max={100} />
				<span>Slider</span>
			</label>

			<h5 className="hr-h">Progress- Circular</h5>

			<progress className="pure-material-progress-circular cwait" />

			<h5 className="hr-h mb-3">Progress- Linear</h5>

			<Flex dir="column" align="baseline">
				<progress className="pure-material-progress-linear" value="50" max="100"></progress>
        <progress className="pure-material-progress-linear mt-4"></progress>
			</Flex>

			{/* Array.from({length:30}).map((_, i) => 
				<p key={i}>Dummy main content {i + 1}</p>
			) */}
		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
