import React, { useState } from 'react';// , { useState, useEffect, useRef, } 
import { DndProvider } from 'react-dnd';

import Head from '../../../components/q-ui-react/Head';
// import Flex from '../../../components/q-ui-react/Flex';

import DndBackend from '../../../components/directory/parts/DndBackend';
// import DirectoryDetails from '../../../components/directory/DirectoryDetails';
import Directories from '../../../components/directory/Directories';

const DATAS = [
	{
		"path": "/package.json",
		"type": "file",
		// isFile: true, 
		"contentType": "application/json", // contentType
		"integrity": "sha384-Pdp+4TbCSkCdnlbhVzYTcNg0VdRQEnLsrB8A18y/tptwTZUq6Zn8BnWcA4Om7E3I",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 759
	}, {
		"path": "/build-info.json",
		"type": "file", 
		"contentType": "application/json",
		"integrity": "sha384-587fmomseJLRYDx7qDgCb06KZbMzfwfciFVnsTI64sEqecGBQVHIMIm3ZUMzNOwl",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 163
	}, {
		"path": "/index.js",
		"type": "file",
		"contentType": "application/javascript",
		"integrity": "sha384-uYepeL3qyzb/7G5T1fizoxPoKmV6ftFXdz4jeQlBff4HqMuNVJPqiNjvN38BeHUk",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 190
	}, {
		"path": "/LICENSE",
		"type": "file",
		"contentType": "text/plain",
		"integrity": "sha384-dIN5gxuW3odCH42AKpxql36fRQlrVsjCi4EJgGC2Ty8Ihkcq9cdrChEZpKhSAfks",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 1086
	}, {
		"path": "/README.md",
		"type": "file",
		"contentType": "text/markdown",
		"integrity": "sha384-WuEvTet+scWBliRT6iLsyZZfFLBEFU/QBmfzdOvLj9mfU4CNvX7WbMosvPxHLjrS",
		"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
		"size": 737
	}, {
		"path": "/cjs",
		"type": "directory",
		// isDirectory: true, 
		"files": [{
			"path": "/cjs/react.development.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-M/8nyoIVh1dygRP3vsJsXo7izkbkAKPA2bON89F2e26vho6tVvv08kWLp2hZ7M86",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 74430
		}, {
			"path": "/cjs/react.production.min.js",
			"type": "file",
			"contentType": "application/javascript",
			"integrity": "sha384-sbVXGGS221NzmofOIH3f7+u0yCox/aqW276fniG9ttEWvZV8NSnpj+fFo7ZxrjRf",
			"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
			"size": 6645
		}]
	}, {
		"path": "/umd",
		"type": "directory",
		// isDirectory: true, 
		"files": [
			{
				path: "/umd/test", 
				type: "directory",
				files: [
					{
						"path": "/umd/test/dev.js",
						"type": "file",
						"contentType": "application/javascript",
						"integrity": "sha384-5l9PFjfzS2E5Gx88inFin+gZtXaPxAFp9a54+T34j",
						"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
						"size": 1156
					}, {
						"path": "/umd/test/testDev.js",
						"type": "file",
						"contentType": "application/javascript",
						"integrity": "sha384-4l9PFjfzS2E5Gx88inFin+gZtXaPxAFp9a54+Tdsdsd3",
						"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
						"size": 1323
					}
				]
			}, {
				"path": "/umd/react.development.js",
				"type": "file",
				"contentType": "application/javascript",
				"integrity": "sha384-5l9PFjfzS2E5Gx88inFinqBHRa+gZtXaPxAFp9a54+T34j/Mp4dKFndKLBWJs9dz",
				"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
				"size": 115653
			}, {
				"path": "/umd/react.production.min.js",
				"type": "file",
				"contentType": "application/javascript",
				"integrity": "sha384-3LaF+GDeugL4KEmnsbb+HuxO42PMnX+W5VYrAF98SHC8Wq47T6D7WpBckxQuazOA",
				"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
				"size": 12588
			}, {
				"path": "/umd/react.profiling.min.js",
				"type": "file",
				"contentType": "application/javascript",
				"integrity": "sha384-lclrAlvhGDKbAhot+O7b99hyYkco0LGhaVbQwwBOkCfdx+OE3/Qonk2VvSvU1tVN",
				"lastModified": "Sat, 26 Oct 1985 08:15:00 GMT",
				"size": 16191
			}
		]
	}
];

export default function DirectoryPage(){
	const [tree, setTree] = useState(DATAS);
	const [cutData, setCutData] = useState(null);
	const [copyData, setCopyData] = useState(null);
	
	/* useEffect(() => {
		console.log('%cuseEffect in DirectoryPage','color:yellow;');
		
	}, []); */

	/* const moveItem = (id, afterId, nodeId) => {
    if (id == afterId) return

		// let {tree} = this.state
		let dataTree = tree;

    const removeNode = (id, items) => {
      for (const node of items) {
        if (node.id == id) {
          items.splice(items.indexOf(node), 1)
          return
        }

        if (node.children && node.children.length) {
          removeNode(id, node.children)
        }
      }
    }

    const item = { ...findItem(id, dataTree) }
    if (!item.id) {
      return
    }

    const dest = nodeId ? findItem(nodeId, dataTree).children : dataTree

    if (!afterId) {
      removeNode(id, dataTree)
      dest.push(item)
    } else {
      const index = dest.indexOf(dest.filter(v => v.id == afterId).shift())
      removeNode(id, dataTree)
      dest.splice(index, 0, item)
    }

		// this.setState({tree});
		setTree(dataTree);
  }

  const findItem = (id, items) => {
    for (const node of items) {
      if (node.id == id) return node
      if (node.children && node.children.length) {
        const result = findItem(id, node.children)
        if (result) {
          return result
        }
      }
    }

    return false
  } */

	return (
		<div className="container-fluid py-3">
			<Head title="Directory Tree" />

			<h1>Directory Tree</h1>

			<hr/>
			
			<pre className="alert alert-warning">{`1. Rename Directory & File name (With validation)
2. Dnd
3. Cut, Copy & Paste
4. New File (With validation)
5. New Directory (With validation)
6. Upload File`}</pre>

			<div className="col-3 px-0">
				<DndBackend>
					{(backend) => (
						<DndProvider backend={backend}>
							<Directories 
								// open 
								data={tree} 
								copy={copyData} 
								cut={cutData} 
								onDrop={(dropData, target) => {
									console.log('onDrop dropData: ', dropData);
									console.log('onDrop target: ', target);
								}} 
								onDelete={(data) => setTree(data)} 
								onCut={(data, e) => { // sum
									// console.log('onCut sum: ', sum);
									console.log('onCut data: ', data);
									console.log('onCut e: ', e);
									if(copyData) setCopyData(null);
									setCutData(data);
								}} 
								onCopy={(data, e) => {
									console.log('onCopy data: ', data);
									console.log('onCopy e: ', e);
									if(cutData) setCutData(null);
									setCopyData(data);
								}} 
								onPaste={(idx, e) => {
									console.log('onPaste e: ', e);
									console.log('onPaste idx: ', idx);
									
									// const dataIndex = tree.findIndex(v => v.path === cutData.path);
									// MUST recursive:
									let newData;
									
									if(copyData){
										newData = tree.map((v, i) => {
											if(i === idx){
												return {
													...v,
													files: [
														...v.files, 
														{ ...copyData, path: v.path + copyData.path }
													]
												}
											}
											return v;
										});
									}else{
										newData = tree.map((v, i) => {
											if(i === idx){
												return {
													...v,
													files: [
														...v.files, 
														{ ...cutData, path: v.path + cutData.path }
													]
												}
											}
											return v;
										}).filter(v => v.path !== cutData.path);
									}
									
									// console.log('onPaste dataIndex: ', dataIndex);
									console.log('onPaste newData: ', newData);
									
									setTree(newData);
									setCutData(null);
									
									// console.log('onPaste label: ', label);
								}} 
								// onRename={(i, item) => {
									// console.log('onRename i: ', i);
									// console.log('onRename item: ', item);
								// }} 
								// onToggle
								onNewFile={data => {
									console.log('onNewFile DirectoryPage: ', data);
									setTree(data);
								}}
							/>
						</DndProvider>
					)}
				</DndBackend>
			</div>
		</div>
	);
}

// export default DragDropContext(HTML5Backend);
