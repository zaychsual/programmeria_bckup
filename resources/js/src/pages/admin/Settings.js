import React, { useState } from 'react';// { useState, useEffect } 
import * as Yup from 'yup';
import { Field, Formik } from 'formik';// useFormik | Formik

import Head from '../../components/q-ui-react/Head';
import Form from '../../components/q-ui-react/Form';
// import InputQ from '../../components/q-ui-react/InputQ';
import Btn from '../../components/q-ui-react/Btn';
// import Loops from '../../components/q-ui-react/Loops';

// import { confirm } from '../../components/react-confirm/util/confirm';// , confirmComplex

const FormGroup = ({ 
	label, 
	id, 
	iSize, 
	className, 
	wrapClass, 
	formText, 
	formTextClass, 
	type = "text", 
	children, 
	// formik:
	field, 
	form, 
	// meta, 
	...etc 
}) => {
	// console.log('field: ', field);
	// console.log('form: ', form);
	// console.log('meta: ', meta);

	return (
		<div className={"form-group" + (wrapClass ? " " + wrapClass : "")}>
			<label htmlFor={id}>{label}</label>
			<input 
				{...field} 
				// {...form} 
				// {...meta} 
				{...etc} 

				type={type} 
				className={
					Q.Cx("form-control", {
						[" form-control-" + iSize] : iSize
					}, className)
				} 
				id={id} 
			/>

			{formText && 
				<small className={"form-text" + (formTextClass ? " " + formTextClass : "")}>
					{formText}
				</small>
			}

			{/* {children}  */}

			{(form && form?.touched[id] && form?.errors[id]) && 
				<div className="invalid-feedback">{form.errors[id]}</div>
			}
		</div>
	)
}

const Schema = Yup.object().shape({
	appName: Yup.string()
		// .min(5, "Minimum 5 symbols")
		// .max(50, "Maximum 50 symbols")
		.required("App Name is required"),
		// .required(
		//   intl.formatMessage({
		//     id: "AUTH.VALIDATION.REQUIRED_FIELD",
		//   })
		// ),
	email: Yup.string()
		.email("Wrong email format")
		.required("Email is required"), 
	theme: Yup.string()
		.required("Theme is required")
});

const initValues = {
	appName: "Programmeria",
	email: "", 
	theme: "#e3f2fd", 
};

export default function Settings(){
	const [vals, setVals] = useState(initValues);
	
	// useEffect(() => {
	// 	console.log('%cuseEffect in Settings','color:yellow;');
	// }, []);

	const onSave = (e, valid, formik) => { // , valid
		// console.log('onSave e: ', e);
		console.log('onSave valid: ', valid);
		console.log('onSave formik: ', formik);

		if(valid){
			formik.handleSubmit();
		}
	}

	return (
		<div className="container py-3">
			<Head title="Settings" />
			
			<h5 className="hr-h hr-left mb-4">Settings</h5>

			<div className="row flex-row-reverse">
				<div className="col-md-5">
					<div className="card bg-light shadow-sm">
						<div className="card-header">Header</div>
  					<div className="card-body">
							Info

						</div>
					</div>
				</div>

				<div className="col-md-7">
					<Formik
						initialValues={vals} // initValues 
						validationSchema={Schema} 
						onSubmit={(values, fn) => {
							fn.setSubmitting(true);

							setTimeout(() => {
								console.log("values: ", values); // JSON.stringify(values, null, 2)

								setVals(values);
								fn.setSubmitting(false);
							}, 3000);
						}}
					>
						{(formik) => (
							<Form 
								noValidate 
								valid={formik.touched} 
								className={"card shadow-sm" + (formik.isSubmitting ? " i-load" : "")} 
								// style={{ '--iload-size': formik.isSubmitting ? "70" : undefined }}
								disabled={formik.isSubmitting} 
								onSubmit={(e, valid) => onSave(e, valid, formik)} 
								onReset={formik.handleReset} 
							>
								<div className="card-header bg-light py-2 position-sticky t48 zi-1">
									<Btn type="reset" kind="dark">Reset</Btn>{" "}
									<Btn type="submit">Save</Btn>
								</div>

								<div className="card-body">
									{/* <Field name="appName" id="appName" label="App Name" required component={FormGroup}  /> */}

									<Field id="appName" name="appName" label="App Name" required component={FormGroup} />

									{/* <Field name="appName">
										{({
											field, // { name, value, onChange, onBlur }
											form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
											// meta
										}) => (
											<FormGroup {...field} id="appName" label="App Name" required>
												{(touched.appName && errors.appName) && 
													<div className="invalid-feedback">{errors.appName}</div>
												}
											</FormGroup>
										)}
									</Field> */}

									<Field id="email" name="email" label="Email" type="email" required component={FormGroup} />

									<Field id="theme" name="theme" label="Theme" type="color" className="p-1 cpoin" required component={FormGroup} />
								</div>
							</Form>
						)}
					</Formik>
				</div>
			</div>

		</div>
	);
}

/*
<React.Fragment></React.Fragment>
*/
