import React, { useState, useEffect, Fragment } from 'react';// useState, useEffect, useRef
import axios from 'axios';

// Default theme. ~960B
// import '@vime/core/themes/default.css';

// Optional light theme (extends default). ~400B
// import '@vime/core/themes/light.css';

// import { VimePlayer, VimeVideo, VimeDefaultUi } from '@vime/react';// VimeUi

import Head from '../components/q-ui-react/Head';

import Placeholder from '../components/q-ui-react/Placeholder';
import Flex from '../components/q-ui-react/Flex';
import Img from '../components/q-ui-react/Img';// Img | Ava
import Aroute from '../components/q-ui-react/Aroute';
import Time from '../components/q-ui-react/Time';
// import Scroller from '../components/q-ui-react/Scroller';
// import Table from '../components/q-ui-react/Table';
// import Btn from '../components/q-ui-react/Btn';

const cardLoders = () => {
	return Array.from({length: 6}).map((v, i) => 
		<div key={i} className="col-md-4 mb-4">
			<div className="card">
				<Placeholder type="thumb" h={190} className="card-img-top"
					// label="Post" 
				/>
				
				<div className="card-body">
					<Placeholder type="h5" className="card-title" />
					<Placeholder className="card-text" />
				</div>
			</div>
		</div>
	)
}

const LINKS = [
	{label:"Web Technologies", to:"/web-technologies"}, 
	{label:"Web Tools", to:"/web-tools"}, 
	{label:"Frameworks", to:"/frameworks"}
];

// const randColor = () => Math.floor(Math.random() * 16777215).toString(16);

export default function Home(){
	const [data, setData] = useState([]);

	useEffect(() => {
		// console.log('%cuseEffect in Home','color:yellow;');

		// axios.get('/api/getauth')
    // .then(r => {
    //   console.log(r);// .data
    // }).catch(e => console.log(e));

		axios.get('/DUMMY/json/articles.json').then(r => {
			// console.log(r.data);
			setData(r.data);
		}).catch(e => console.log(e));
	}, []);

	return (
		<Fragment>
			<Head title="Home" />

			{/* <button onClick={handleButtonClick} className="btn btn-info" type="button">Test Notif</button> */}

			<div className="jumbotron jumbotron-fluid bg-strip text-center">
				<h1 className="txt-outline text-white bold">Programmeria</h1>

				<Flex dir="lg-row flex-column" justify="center" className="px-2 mt-4 ml-2-next-lg mt-2-next-p-lp">
					{LINKS.map((v, i) => <Aroute key={i} to={v.to} outline btn="primary">{v.label}</Aroute>)}
				</Flex>
			</div>

			{/* <Ava circle w={50} h={50} src="/img/avatar_man.png" alt="Ava Man" /> */}
			
			<div className="container py-3">
				<h4 className="hr-h mb-4 bold">Latest Article</h4>

				<div className="row">
					{data.length > 0 ? 
						data.map((v, i) => 
							<div key={i} className="col-md-4 mb-4">
								<article className="card h-100 post-1">
									{v.img ? 
										<Img frame="bs" 
											frameClass="mb-0"
											// w={350} 
											h={190} 
											alt={v.title} 
											src={v.img} 
											className="mb-0 card-img-top bg-light of-cov" 
											// onLoad={e => {
											// 	console.log('onLoad img');
											// 	let et = e.target;

											// }}
										/> 
										: 
										<Placeholder type="thumb" className="card-img-top no-animate cauto qi qi-7x" 
											h={190} 
											// label={v.title} 
											label="&#xE92F;" // "\E92F"
											// style={{ color: '#' + randColor() }}
										/>
									}

									{/* <Ava // frame 
										// w={350} 
										h={190} alt={v.title} src={v.img} className="card-img-top bg-light of-cov" 
										// onLoad={e => {
										// 	console.log('onLoad img');
										// 	let et = e.target;

										// }}
									/>  */}

									{/* <time className="badge badge-info ml-3 shadow-sm zi-1">27 July 2020</time> */}
									<Time 
										datetime={v.created_at} // "2020-08-08 08:08:08" 
										// locale="zh_CN" 
										className="badge badge-info ml-3 shadow-sm zi-1"
									/>
									
									<div className="card-body">
										<Aroute to={"/article/" + v.id} noLine kind="secondary" 
											className="h6 card-title truncate-line max-2 stretched-link" 
											title={v.title} 
										>
											{v.title}
										</Aroute>
										
										<p className="card-text small truncate-line">{v.desc}</p>{/* {v.desc.substring(0, 78)}... */}

										{/* <a href="/" className="btn btn-sm btn-primary stretched-link">Read</a> */}
									</div>
								</article>
							</div>
						)
						: 
						cardLoders()
					}
				</div>

			</div>

			{/* {['p','h1','h2','h3','h4','h5','h6'].map((v, i) => 
				React.createElement(v, { key: i, className: "px-3 bg-warning text-white" }, v)
			)} */}

		</Fragment>
	);
}

/*
<React.Fragment></React.Fragment>
*/
