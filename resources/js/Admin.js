import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React, { lazy, Suspense } from 'react';// { useEffect }
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
// import { lazy as lazyLoadable } from '@loadable/component';// loadable

import { APP_NAME } from './src/data/appData';

// AUTH:
import { AuthProvider } from './src/context/AuthContext';// , AuthContext
import { FetchProvider } from './src/context/FetchContext';

// import PageVisibilityAuth from './src/parts/PageVisibilityAuth';
import Mq from './src/parts/Mq';
import AsideAdmin from './src/parts/admin/AsideAdmin';
import NavAdmin from './src/parts/admin/NavAdmin';
import RouteLazy from './src/components/RouteLazy'; // ErrorBoundary | RouteLazy
import PageLoader from './src/components/PageLoader';
import Flex from './src/components/q-ui-react/Flex';
import ScrollTo from './src/components/q-ui-react/ScrollTo';
// import BtnMenuMove from './src/parts/BtnMenuMove';
// import RouteParams from './src/parts/RouteParams';
// import DynamicImport from './src/parts/DynamicImport';

import Ide from './src/apps/ide/Ide';

// PAGES:
// import HomeAdmin from './src/pages/admin/HomeAdmin';
// import DocsPage from './src/pages/admin/docs/DocsPage';
import NotFound from './src/pages/public/NotFound';

// ==================================================
// DEV OPTION: Store to window object / global
window.React = React;
window.ReactDOM = ReactDOM;
// END DEV OPTION: Store to window object / global
// ==================================================

const IdePage = () => <Ide />;

// lazy:
const HomeAdmin = lazy(() => import(/* webpackChunkName: "HomeAdmin" */'./src/pages/admin/HomeAdmin'));
const Settings = lazy(() => import(/* webpackChunkName: "Settings" */'./src/pages/admin/Settings'));

// Components:
// storage/js/chunk/pv/adm/docs/ | private/
const ArouteDocs = lazy(() => import(/* webpackChunkName: "ArouteDocs" */'./src/pages/admin/docs/q-ui-react/ArouteDocs'));
const AvaDocs = lazy(() => import(/* webpackChunkName: "AvaDocs" */'./src/pages/admin/docs/q-ui-react/AvaDocs'));
const BtnDocs = lazy(() => import(/* webpackChunkName: "BtnDocs" */'./src/pages/admin/docs/q-ui-react/BtnDocs'));
const LoopsPage = lazy(() => import(/* webpackChunkName: "LoopsPage" */'./src/pages/admin/docs/q-ui-react/LoopsPage'));
const ModalPage = lazy(() => import(/* webpackChunkName: "ModalPage" */'./src/pages/admin/docs/q-ui-react/ModalPage'));
const TableDocs = lazy(() => import(/* webpackChunkName: "TableDocs" */'./src/pages/admin/docs/q-ui-react/TableDocs'));
// Tools:
const IconMakerPage = lazy(() => import(/* webpackChunkName: "IconMakerPage" */'./src/pages/admin/tools/IconMakerPage'));
const Base64EncoderPage = lazy(() => import(/* webpackChunkName: "Base64EncoderPage" */'./src/pages/admin/tools/Base64EncoderPage'));// SvgToUrl
const ManifestGeneratorPage = lazy(() => import(/* webpackChunkName: "ManifestGeneratorPage" */'./src/pages/admin/tools/ManifestGeneratorPage'));
// Documentation:
const Laravel = lazy(() => import(/* webpackChunkName: "Laravel" */'./src/pages/Laravel'));
// Develop:
const Devs = lazy(() => import(/* webpackChunkName: "Devs" */'./src/pages/admin/devs/Devs'));
const MatterCSS = lazy(() => import(/* webpackChunkName: "MatterCSS" */'./src/pages/admin/devs/MatterCSS'));
const DropsPage = lazy(() => import(/* webpackChunkName: "DropsPage" */'./src/pages/admin/devs/DropsPage'));
const ImgMapPage = lazy(() => import(/* webpackChunkName: "ImgMapPage" */'./src/pages/admin/devs/ImgMapPage'));
// const Devs = loadable(() => import(/* webpackChunkName: "storage/js/chunk/pv/adm/Devs" */'./src/pages/private/Devs'));
const PosPage = lazy(() => import(/* webpackChunkName: "PosPage" */'./src/pages/private/PosPage'));
const ScreenCapturePage = lazy(() => import(/* webpackChunkName: "ScreenCapturePage" */'./src/pages/admin/devs/ScreenCapturePage'));
const PostDetailPage = lazy(() => import(/* webpackChunkName: "PostDetailPage" */'./src/pages/admin/devs/PostDetailPage'));
const PostEdit = lazy(() => import(/* webpackChunkName: "PostEdit" */'./src/pages/admin/devs/PostEdit'));
// const ReactPlayerDemoPage = lazy(() => import(/* webpackChunkName: "ReactPlayerDemoPage" */'./src/pages/admin/devs/react-player-demo/ReactPlayerDemoPage'));
const EsModuleBuilder = lazy(() => import(/* webpackChunkName: "EsModuleBuilder" */'./src/pages/admin/devs/EsModuleBuilder'));
const BundlerPage = lazy(() => import(/* webpackChunkName: "BundlerPage" */'./src/pages/admin/devs/BundlerPage'));
const ReplSimplePage = lazy(() => import(/* webpackChunkName: "ReplSimplePage" */'./src/pages/admin/devs/ReplSimplePage'));
const ReplPage = lazy(() => import(/* webpackChunkName: "ReplPage" */'./src/pages/admin/devs/ReplPage'));
const LoadLibraryPage = lazy(() => import(/* webpackChunkName: "LoadLibraryPage" */'./src/pages/admin/devs/LoadLibraryPage'));
// const TreeDirectoryPage = lazy(() => import(/* webpackChunkName: "TreeDirectoryPage" */'./src/pages/admin/devs/TreeDirectoryPage'));
const DirectoryPage = lazy(() => import(/* webpackChunkName: "DirectoryPage" */'./src/pages/admin/devs/DirectoryPage'));
const FileManagerPage = lazy(() => import(/* webpackChunkName: "FileManagerPage" */'./src/pages/admin/devs/FileManagerPage'));
// END lazy
// END PAGES

const BASENAME = "/admin";// Admin url basename

const MENUS = [
	// {label:"Testing", href:"/admin/testing", icon:"cogs"},
	{label:"Home", to:"/", icon:"home", exact:true, strict:true}, 
	{label:"Settings", to:"/settings", icon:"cog"}, 
	{label:"Articles", icon:"edit", 
		menus: [
			{label:"All article", to:"/articles", icon:"list"},
			{label:"Create", to:"/create-article", icon:"edit"}
		]
	}, 
	{label:"Projects", to:"/projects", icon:"list"}, 
	{label:"Components", icon:"code", 
    menus: [
			{label:"Aroute", to:"/components/aroute", icon:"code"}, 
			{label:"Ava", to:"/components/ava", icon:"code"}, 
			{label:"BgImage", to:"/components/bg-image", icon:"code"}, 
			{label:"Btn", to:"/components/btn", icon:"code"}, 
			{label:"Flex", to:"/components/flex", icon:"code"},
			{label:"Form", to:"/components/form", icon:"code"},
			// {label:"Head", to:"/components/head", icon:"code"},
			{label:"Iframe", to:"/components/iframe", icon:"code"},
			{label:"Img", to:"/components/img", icon:"code"},
			{label:"InputGroup", to:"/components/input-group", icon:"code"},
			{label:"InputQ", to:"/components/input-q", icon:"code"},
			{label:"Loops", to:"/components/loops", icon:"code"},
			{label:"Modal", to:"/components/modal", icon:"code"},
			{label:"NetWork", to:"/components/network", icon:"code"},
			{label:"ResizeSensor", to:"/components/resize-sensor", icon:"code"},
			{label:"RouteImport", to:"/components/route-import", icon:"code"},
			{label:"Script", to:"/components/script", icon:"code"},
			{label:"ScrollTo", to:"/components/scroll-to", icon:"code"},
			{label:"SpinLoader", to:"/components/spin-loader", icon:"code"},
			{label:"Table", to:"/components/table", icon:"code"}
    ]
	},
  {label:"Tools", icon:"wrench", 
    menus: [
			{label:"Icon Maker", to:"/tools/icon-maker", icon:"code"}, 
			{label:"Base64 encoder", to:"/tools/base64-encoder", icon:"code"}, 
			{label:"Manifest Generator", to:"/tools/manifest-generator", icon:"code"}, 
			{label:"Transform Code", to:"/tools/transform-code", icon:"code"},
    ]
	}, 
  {label:"Develop", icon:"code", 
    menus: [
			{label:"Devs", to:"/devs/all", icon:"code"}, 
			{label:"Matter CSS", to:"/devs/matter-css", icon:"browser"}, 
			{label:"Drops", to:"/devs/drops", icon:"code"}, 
			{label:"ImgMap", to:"/devs/img-map", icon:"code"}, 
			{label:"Point Of Sales App", to:"/devs/pos-app", icon:"code"}, // cash-register
			{label:"Integrated Development Environment", to:"/devs/ide", icon:"code"}, 
			{label:"Post Detail Page", to:"/devs/post-detail-page", icon:"code"}, 
			{label:"Post Edit", to:"/devs/post-edit", icon:"code"}, 
			// {label:"React Player Demo Page", to:"/devs/react-player-demo", icon:"code"}
			{label:"Es Module Builder", to:"/devs/es-module-builder", icon:"code"}, 
			{label:"Bundler", to:"/devs/bundler", icon:"code"}, 
			{label:"Repl Simple", to:"/devs/repl-simple", icon:"code"}, 
			{label:"Repl", to:"/devs/repl", icon:"code"}, 
			{label:"Load Library", to:"/devs/load-library", icon:"code"}, 
			// {label:"Tree Directory", to:"/devs/tree-directory", icon:"code"}, 
			{label:"Directory", to:"/devs/directory", icon:"code"}, 
			{label:"File Manager", to:"/devs/file-manager", icon:"code"}, 
    ]
	}, 
	{label:"Documentation", icon:"book", 
    menus: [
			{label:"JS Utilities", to:"/documentation/js-utilities", icon:"js fab"}, 
			{label:"Laravel", to:"/documentation/laravel", icon:"laravel fab"}, 
    ]
  },
	{label:"Experiments", icon:"flask",
		menus: [
			{label:"Screen Capture API", to:"/experiments/screen-capture-api", icon:"camera"}, 
		]
	}, 
	// {label:"Bug", href:"/bug", icon:"bug"}, 
];

const SM_DEVICE = Q_appData.UA.platform.type === "mobile" || screen.width <= 480; // Q_UA.platform.type === "mobile" && screen.width < 480;

// const Loader = () => (
// 	<PageLoader bottom left={SM_DEVICE} w={SM_DEVICE ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />
// );

// const ErrMsg = ()  => (
// 	<div role="alert" 
// 		className="alert alert-danger mb-0 rounded-0 pre-wrap ovauto" // ff-inherit fs-inherit
// 	>&#9888;{` Something went wrong. 

// ${navigator.onLine ? 'Server error.':'Your internet connection is offline.'}`}</div>
// );

const FAV = '/favicon.ico';

class Root extends React.Component{
	constructor(props){
		super(props);

		// SETUP DOM 
		//if(!document.documentElement?.lang?.length > 0) document.documentElement.lang = Q.lang();

		Q.setUpDOM();

		if(SM_DEVICE) Q.setClass(document.body, 'isMobile');

		// DEVS NOT FIX: Store favicon offline file
		if(!Q_appData?.faviconOffline){ // Q.faviconOffline() | Q.FAV_OFF
			// 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACN0lEQVR42pRTTUhUURT+zn13xtEoG5KMyNpILloVuBjIFHMRFfZDtGtTZFItIyJw126gKDSKJKKd1CINU6ZVGES1CAYKsgRNF85o/oymvjvv3tOZGTfxBrHDO++dd+53/s8FOwZziV3h67jxd2Y0tZrLLrNzbAPjRF5cXsr0OXaNzA4lLmJBzjkQhAixuezP1/Ga+jZangN/eAOa+gEUwLvrgcQJoHoHjPnTF41tvQjGSsGsGFU8xxZnxyecWWPu7WJuq2Ruwr/cWsHcc4OtWeG11aW0BK4Su1IG8zNjQ/F43TG61Q58TmFDOtQKlxwEK/1CKe88nHUHnQ3YPbwZjjqWZp6eCOndvetsrRV2R1RuYfIOL8yCXt7HZon6H8HOT7M0qFNFdGUzfRwG8v6mHcAG0CMD5KxtU7Gq+BZMfsd/k9iQ59VoJgLlTXlQsgPwdPkzmYCMj7S/ksvF6hq2UTnQ+LfigpSjYO9+cWKzirQesonjgI6EUT3vgWfpsF6yCppOWyKVUlpXdiNeC3fmWhiYnQIyv8I9bL8CXV3ryTL3kjEG1vrPFdOFSNc50KfhDXvHBxIwd99apaMDUt1ZcSDjY4rmg9WUhmrWT25D9T+WIk0obXvyMoKrSUte9Kv0vkW082R8I/dCdoIoAue6pa6OYDHj9OBT5Y1+AXbugb+vAfbwqaBi+y4ZCb+S63NJ8HNUMPP99QUqNJuLUouIneL0qChr1g9m5DUi/ED+3xU1BWt5/gowAIJUQNn6yq1DAAAAAElFTkSuQmCC'
			let faviconOffline = 'data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"%3E%3Cpath fill="red" d="M633.99 471.02L36 3.51C29.1-2.01 19.03-.9 13.51 6l-10 12.49C-2.02 25.39-.9 35.46 6 40.98l598 467.51c6.9 5.52 16.96 4.4 22.49-2.49l10-12.49c5.52-6.9 4.41-16.97-2.5-22.49zM80 384H48c-8.84 0-16 7.16-16 16v96c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-96c0-8.84-7.16-16-16-16zm400-272c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v127.66l64 50.04V112zm128-96c0-8.84-7.16-16-16-16h-32c-8.84 0-16 7.16-16 16v323.73l64 50.04V16zM416 496c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-23.52l-64-50.04V496zm-128 0c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V372.41l-64-50.04V496zm-80-208h-32c-8.84 0-16 7.16-16 16v192c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16V304c0-8.84-7.16-16-16-16z"%3E%3C/path%3E%3C/svg%3E';
			Q_appData.faviconOffline = faviconOffline; // Q.FAV_OFF
			localStorage.setItem('favicon-offline', faviconOffline);
		}
		// END SETUP DOM

		this.state = {
			asideMin: false, 
		};
	}

	componentDidMount(){
		// const { history } = this.props;
		// console.log('componentDidMount in Admin window.history: ', window.history);

		// if ('scrollRestoration' in window.history) {
		// 	// Back off, browser, I got this...
		// 	window.history.scrollRestoration = 'manual';
		// }

		// DEV: Prevent page if signout from other tab / window
		// Q.domQ('#root').addEventListener('click', Q.preventQ);
		// Q.domQ('#root').addEventListener('keydown', Q.preventQ);

		// console.log('componentDidMount in Admin window.history after: ', window.history);

		Q.setClass(Q.domQ('#QloadStartUp'),'d-none');// Hide Loader Splash / start up

    ['dragover','drop'].forEach(v => window.addEventListener(v, Q.preventQ));// Prevent Drag & Drop File
		
		// window.addEventListener('keydown', this.onCtrlSaveNPrint);// DEV OPTION: for prevent ctrl + S, ctrl + P

		['online','offline'].forEach(v => window.addEventListener(v, this.onConnection));

		// DEVS OPTION: Prevent Zoom when ctrl + wheel
		// let ROOT = Q.domQ("#root");
		// document.addEventListener("wheel", e => {
		// 	// console.log('onWheel e: ', e.ctrlKey);
		// 	if(e.ctrlKey){
		// 		Q.preventQ(e);
		// 		return;
		// 	}
		// }, {
		// 	passive: false,
		// 	capture: true
		// });
		
		// const Qworker = new Worker('/storage/src/worker/tryWorker.js');
		
		// Qworker.onmessage = (e) => {
			// if(e && e.data){
				// console.log('Qworker: ',e.data);
			// }
    // };
		
		// Qworker.addEventListener('message', (e) => {
			// if(e && e.data){
				// console.log('Qworker: ',e.data);
			// }
		// }, false);
		
		// Qworker.postMessage('babel');
		
		// import(/*webpackIgnore:true*/'https://dev.jspm.io/react-dom').then(m => {
			// console.log(m);
			// localStorage.setItem('modules', m.default);
		// }).catch(e => console.error(e));
		
		// https://dev.jspm.io/react-router-dom | https://unpkg.com/react-router-dom?module
		// import(/*webpackIgnore:true*/'https://dev.jspm.io/reactstrap').then(v => {
			// console.log(v);
		// }).catch(e => console.error(e));

		// Store scroll position before reload page (Chrome Only)
		// if(Q_appData.UA.browser.name === "Chrome"){
		// 	let scroll = Number(sessionStorage.getItem('scroll'));
		// 	if(scroll && document.body.offsetHeight > scroll){
		// 		window.scrollTo(0, scroll);
		// 	}

		// 	window.addEventListener("unload", (e) => {
		// 		//  localStorage.removeItem('persist:programmeria');// console.log('unload ', e);
		// 		sessionStorage.setItem('scroll', window.scrollY);
		// 	});
		// }
		// END Store scroll position before reload page (Chrome Only)

    // window.onbeforeunload = function(e){
    //  let sb = navigator.sendBeacon(
    //    'https://reqres.in/api/users',
    //    JSON.stringify({
		// 		name: 'Husein',
		// 		job: 'Programmer'
		// 	})
    //  );
    //  console.log('sb: ', sb);
     // empty return is required to not show popup
    //  return;
		// }
		
		// fetch('https://jsonplaceholder.typicode.com/posts', {
		// 	method: 'POST',
		// 	body: JSON.stringify({
		// 		title: 'foo',
		// 		body: 'bar',
		// 		userId: 1
		// 	}),
		// 	headers: {
		// 		"Content-type": "application/json; charset=UTF-8"
		// 	}
		// })
		// .then(response => response.json())
		// .then(json => console.log(json));
		
		// console.log('%cProgrammeria App Designer','color:#666;font-family:sans-serif;letter-spacing:2px;font-size:26px;font-weight:700;text-shadow:1px 1px 0 #A0E7FE,2px 2px 1px rgba(0,0,0,.3)');
		// console.log('%cDevelopment by: https://programmeria.com','font-size:15px');
	}

	// componentDidUpdate(prevProps){
		// const { location } = this.props;
		// // console.log('componentDidUpdate in Admin prevProps: ',prevProps);
		// // console.log('componentDidUpdate in Admin location: ',location);

		// if(location.pathname !== prevProps.location.pathname){ //   && !location.state | && !location.pathname.includes('/detail/')
    //   window.scrollTo(0, 0);
    //   // DEV to set previous url FOR Not Found page
    //   // this.setState({backUrl: prevProps.location.pathname});
		// }
		
		// 	// FOR close dropdown-menu with dropdown-item react-router NavLink / Link component
		// 	let btn = document.activeElement;
		// 	if(Q.getAttr(btn, 'aria-expanded') === 'true'){ // hasClass(btn, "ddRoute") ||
		// 		btn.click();
		// 		btn.blur();
		// 	}
	// }
	
// DEVS NOT FIX:
	onConnection = () => { // e
		const ico = Q.domQ('link[rel="shortcut icon"]');
		const href = Q.getAttr(ico, 'href');
		// let fav = '/favicon.ico';
		if(navigator.onLine){
			if(href !== FAV) ico.href = FAV;
		}else{
			// Q.FAV_OFF
			if(href === FAV) ico.href = Q_appData?.faviconOffline || '/favicon-offline.png';
		}
	}
	
// DEV OPTION: Prevent ctrl + S, ctrl + P
  // onCtrlSaveNPrint = e => {
	// 	console.log('key: ',e.key);
	// 	console.log('keyCode: ',e.keyCode);
  //   e.stopPropagation();// OPTION

	// 	let kc = e.keyCode, 
	// 			ctrl = e.ctrlKey;

  //   // 80 = P
  //   if(((ctrl && kc === 80) && !Q.hasClass(document.documentElement, 'print-on')) || ctrl && kc === 83){
  //     e.preventDefault();
  //   }
  //   // 83 = S,
  //   // if(ctrl && kc === 83){
  //     // e.preventDefault();
  //   // }
  // }

  onToggleAside = () => {
    if(this.state.asideMin){
      this.setState({asideMin: false});
      document.body.classList.toggle('aside-min');
    }
  }

	render(){
		// const { location } = this.props;
		const { asideMin } = this.state;

		// <PageVisibilityAuth>
		return (
				<Mq>
					{(isMobile) => (
						<Flex dir="row" className="layout">
							<AsideAdmin 
								isMobile={isMobile} // SM_DEVICE 
								basename={BASENAME} 
								headText={APP_NAME} // "Programmeria" 
								asideMin={asideMin} 
								menus={MENUS} 
								onToggleAside={this.onToggleAside} 
							/>
							
							<Flex dir="column" className="min-vh-100 wrap-app" id="wrapApp">{/* flex-auto pancake-stack */}
								<NavAdmin 
									isMobile={isMobile} // SM_DEVICE 
									asideMin={asideMin} 
									onToggleAside={() => this.setState({asideMin: !asideMin})} 
								/>

								<main>
									<Suspense 
										fallback={<PageLoader bottom left={isMobile} w={isMobile ? null : "calc(100% - 220px)"} h={Q.PAGE_FULL_NAV} />}
									>
										<Switch>
											<Route exact strict path="/" component={HomeAdmin} />

											<RouteLazy path="/settings" component={Settings} 
												// errorMsg={<ErrMsg />} src/pages/private/
											/>

											{/* <DocsPage /> */}
											
											<RouteLazy path="/components/Aroute" component={ArouteDocs} />
											<RouteLazy path="/components/ava" component={AvaDocs} />
											<RouteLazy path="/components/btn" component={BtnDocs} />
											<RouteLazy path="/components/loops" component={LoopsPage} />
											<RouteLazy path="/components/modal" component={ModalPage} />
											<RouteLazy path="/components/table" component={TableDocs} />
											
											<RouteLazy path="/tools/icon-maker" component={IconMakerPage} />
											<RouteLazy path="/tools/base64-encoder" component={Base64EncoderPage} />
											<RouteLazy path="/tools/manifest-generator" component={ManifestGeneratorPage} />

											{/* DEVS ONLY */}
											<RouteLazy path="/documentation/laravel" component={Laravel} />

											<RouteLazy path="/devs/all" component={Devs} />
											<RouteLazy path="/devs/matter-css" component={MatterCSS} />
											<RouteLazy path="/devs/drops" component={DropsPage} />
											<RouteLazy path="/devs/img-map" component={ImgMapPage} />
											<RouteLazy path="/devs/pos-app" component={PosPage} />

											<RouteLazy path="/devs/ide" component={IdePage} />
											
											{/* <Route path="/devs/all" component={Devs} /> */}
											<RouteLazy path="/devs/pos-app" component={PosPage} />
											<RouteLazy path="/devs/post-detail-page" component={PostDetailPage} />
											<RouteLazy path="/devs/post-edit" component={PostEdit} />
											<RouteLazy path="/devs/es-module-builder" component={EsModuleBuilder} />
											<RouteLazy path="/devs/bundler" component={BundlerPage} />
											<RouteLazy path="/devs/repl-simple" component={ReplSimplePage} />
											<RouteLazy path="/devs/repl" component={ReplPage} />
											{/* <RouteLazy path="/devs/react-player-demo" component={ReactPlayerDemoPage} /> */}
											<RouteLazy path="/devs/load-library" component={LoadLibraryPage} />
											{/* <RouteLazy path="/devs/tree-directory" component={TreeDirectoryPage} /> */}
											<RouteLazy path="/devs/directory" component={DirectoryPage} />
											<RouteLazy path="/devs/file-manager" component={FileManagerPage} />
											{/* END DEVS ONLY */}

											{/* EXPERIMENTALS */}
											<RouteLazy path="/experiments/screen-capture-api" component={ScreenCapturePage} />

											<Route component={NotFound} />{/*  path="*" */}

											{/* <ErrorBoundary
												errorMsg={<ErrMsg />} 
												path="*" 
												component={NotFound}
											/> */}
										</Switch>
									</Suspense>
								</main>
							</Flex>

							{/* DEVS OPTION: for toggle hide menu */}
							{/* <BtnMenuMove /> */}

							{/* NOT FIX: */}
							<ScrollTo 
								className="position-fixed zi-5 tip tipL qi qi-arrow-up" 
								style={{ bottom:9, right:9 }} 
								aria-label="Back To top" 
								// onClick={(scroll, e) => {
								// 	console.log('onClick scroll: ', scroll);
								// }}
							/>
						</Flex>
					)}
				</Mq>
			
		);
	}
}
// </PageVisibilityAuth>

const Admin = () => (
	<BrowserRouter basename={BASENAME}>
		<AuthProvider>
			<FetchProvider>
				<LastLocationProvider>
					<Route component={Root} />
				</LastLocationProvider>
			</FetchProvider>
		</AuthProvider>
	</BrowserRouter>
);

// let ROOT = document.getElementById('root');
// Q.setAttr(ROOT,"hidden");
// ROOT.removeAttribute("hidden");
ReactDOM.render(<Admin />, Q.domQ('#root'));

// if(document.getElementById('root')){
  // ReactDOM.render(<Example />, document.getElementById('root'));
// }
