// require('./bootstrap');

import 'react-app-polyfill/ie11'; // OPTION
import 'react-app-polyfill/stable';
import React, { lazy, Fragment, Suspense } from 'react';// useState, useContext
import ReactDOM from 'react-dom';
// import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';// useParams, useRouteMatch, Redirect

// AUTH:
import { AuthProvider } from './src/context/AuthContext';// , AuthContext
import { FetchProvider } from './src/context/FetchContext';

import Mq from './src/parts/Mq';
import NavApp from './src/parts/app/NavApp';
import PageFetch from './src/parts/PageFetch';
import RouteSearch from './src/parts/RouteSearch';
import ErrorBoundary from './src/components/ErrorBoundary';
import PageLoader from './src/components/PageLoader';
// import RouteImport from './src/components/q-ui-react/RouteImport';

// PAGES:
import Home from './src/pages/Home';
import NotFound from './src/pages/public/NotFound';
// END PAGES

// ==================================================
// DEV OPTION: Store to window object / global
window.React = React;
window.ReactDOM = ReactDOM;
// END DEV OPTION: Store to window object / global
// ==================================================

// lazy:
const Login = lazy(() => import(/* webpackChunkName: "Login" */'./src/pages/public/Login'));
const Register = lazy(() => import(/* webpackChunkName: "Register" */'./src/pages/public/Register'));
const About = lazy(() => import(/* webpackChunkName: "About" */'./src/pages/public/About'));
// END lazy
// END PAGES

const SM_DEVICE = Q.UA.platform.type === "mobile" || screen.width <= 480;

// const UnauthenticatedRoutes = () => (
//   <Switch>
//     <Route exact path="/">
//       <Home />
//     </Route>
//     <Route path="/login">
//       <Login />
//     </Route>
//     <Route path="/register">
//       <Register />
//     </Route>
//     <Route path="*">
//       <NotFound />
//     </Route>
//   </Switch>
// );

// const AuthenticatedRoute = ({ children, ...etc }) => {
//   const auth = useContext(AuthContext);
//   return (
//     <Route 
//       {...etc}
//       render={() => auth.isAuthenticated() ? children : <NotFound />} // <Redirect to="/" />
//     />
//   );
// };

// const AdminRoute = ({ children, ...rest }) => {
//   const auth = useContext(AuthContext);
//   return (
//     <Route
//       {...rest}
//       render={() =>
//         auth.isAuthenticated() && auth.isAdmin() ? (
//           <AppShell>{children}</AppShell>
//         ) : (
//           <Redirect to="/" />
//         )
//       }
//     ></Route>
//   );
// };

class Root extends React.Component{
  constructor(props){
    super(props);

    // SETUP DOM
    let html = document.documentElement;
    Q.setClass(html, Q.UA.platform.type + " " + Q.UA.os.name + " " + Q.UA.browser.name);

		Q.setAttr(html, {
			"data-os-v": Q.UA.os.versionName || Q.UA.os.version,
			"data-browser-v": Q.UA.browser.version
		});
		
    if(SM_DEVICE) Q.setClass(document.body, 'isMobile');
    // END SETUP DOM

    // DEV OPTION: back to home if from /search
    // const searchMain = props.location.state && props.location.state.searchMain;
    // console.log('searchMain: ',searchMain);

    // props.location.pathname === '/search'
    if((props.location.state && props.location.state.searchMain) || props.location.pathname.includes('/search')) props.history.replace('/');

    // this.state = {
      // lang: Q.lang(), 
      // auth: sessionStorage.getItem('auth'),
    // };
  }

  componentDidMount(){
    // console.log('auth: ',this.state.auth);
		Q.setClass(Q.domQ('#QloadStartUp'),'d-none');// Hide Loader Splash / start up

    ['dragover', 'drop'].forEach(v => window.addEventListener(v, Q.preventQ));// Prevent Html Drop File in window app

		// window.addEventListener('keydown', this.onCtrlSaveNPrint);// DEV OPTION: for prevent ctrl + S, ctrl + P

    // https://jsonplaceholder.typicode.com/posts | 
		// fetch('https://reqres.in/api/login', {
		// 	method: 'POST',
		// 	body: JSON.stringify({
    //     email: "eve.holt@reqres.in",
    //     password: "cityslicka" // null
    //   }),
		// 	headers: {
		// 		"Content-type": "application/json; charset=UTF-8"
		// 	}
		// })
		// .then(response => response.json())
		// .then(json => console.log(json));

    // console.log('%cWelcome to Programmeria', 'color:#666;font-family:sans-serif;letter-spacing:2px;font-size:26px;font-weight:700;text-shadow:1px 1px 0 #E1E9FF,2px 2px 1px rgba(0,0,0,.3)');
    // console.log('React version: ', React.version);
  }

  componentDidUpdate(prevProps){
    const { location } = this.props;
  	if(location.pathname !== prevProps.location.pathname){ //   && !location.state | && !location.pathname.includes('/detail/')
      window.scrollTo(0,0);
    }
    // FOR close dropdown-menu with dropdown-item react-router NavLink / Link component
  	let btn = document.activeElement;
  	if(Q.getAttr(btn, 'aria-expanded') === 'true'){
  		btn.click();
  		btn.blur();
  	}
  }

// DEV OPTION: Prevent ctrl + S, ctrl + P
  // onCtrlSaveNPrint = e => {
  //   e.stopPropagation();// OPTION

  //   let kc = e.keyCode;
  //   let ctrl = e.ctrlKey;

  //   // 80 = P
  //   if(((ctrl && kc === 80) && !Q.hasClass(document.documentElement, 'print-on')) || ctrl && kc === 83){
  //     e.preventDefault();
  //   }
  //   // 83 = S,
  //   // if(ctrl && kc === 83){
  //     // e.preventDefault();
  //   // }
  // }

  onClickSearch = () => {
    const { history, location } = this.props;
    history.push({
      pathname: "/search", // ${i.id}
      state: { searchMain: location }
    });
  }

  render(){
    // const {  } = this.state;
    const { history, location } = this.props; 
    const searchMain = location.state && location.state.searchMain;

    // console.log('history: ',history);

    return (
			<Mq>
				{(isMobile) => (
          <Fragment>
            <NavApp 
              isMobile={isMobile} // SM_DEVICE
              onClickSearch={this.onClickSearch}
            />

            <main>
              <Suspense fallback={<PageLoader bottom left className="w-100" />}>
                <ErrorBoundary>
                  <Switch location={searchMain || location}>
                    {/* <AuthenticatedRoute path="/laravel">
                      <Laravel />
                    </AuthenticatedRoute> */}

                    {/* <UnauthenticatedRoutes /> */}

                    <Route exact strict path="/" component={Home} />
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    <Route path="/about" component={About} />

                    {/* <RouteImport path="/laravel" module="/js/esm/Laravel.js" /> */}

                    <Route path="/article/:id">
                      <PageFetch />
                    </Route>

                    <Route path="*">
                      <NotFound />
                    </Route>
                  </Switch>

                  {/* (searchMain || (history.action === "POP" && !searchMain && location.pathname !== "/")) */}
                  {searchMain  && 
                    <RouteSearch history={history} />
                  }
                </ErrorBoundary>
              </Suspense>
            </main>
          </Fragment>
				)}
      </Mq>
    );
  }
}

const App = () => (
  <BrowserRouter>
    <AuthProvider>
      <FetchProvider>
        <Route component={Root} />
      </FetchProvider>
    </AuthProvider>
  </BrowserRouter>
);

ReactDOM.render(<App />, document.getElementById('root'));

// <React.StrictMode>
// <App />
// </React.StrictMode>

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

// serviceWorker.unregister();
