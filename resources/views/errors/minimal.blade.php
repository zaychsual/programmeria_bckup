<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,viewport-fit=cover">
<meta name="apple-mobile-web-app-status-bar-style" content="default" />
<meta name="mobile-web-app-capable" content="yes" />
<meta name="msapplication-TileColor" content="#e3f2fd" />
<meta name="theme-color" content="#e3f2fd" />
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}">
<title>@yield('title')</title>
{{-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
{{--<style>
/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}code{font-family:monospace,monospace;font-size:1em}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}code{font-family:Menlo,Monaco,Consolas,Liberation Mono,Courier New,monospace}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-gray-400{--border-opacity:1;border-color:#cbd5e0;border-color:rgba(203,213,224,var(--border-opacity))}.border-t{border-top-width:1px}.border-r{border-right-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-xl{max-width:36rem}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-4{padding-left:1rem;padding-right:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.uppercase{text-transform:uppercase}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.tracking-wider{letter-spacing:.05em}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@-webkit-keyframes spin{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes spin{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@-webkit-keyframes ping{0%{transform:scale(1);opacity:1}75%,to{transform:scale(2);opacity:0}}@keyframes ping{0%{transform:scale(1);opacity:1}75%,to{transform:scale(2);opacity:0}}@-webkit-keyframes pulse{0%,to{opacity:1}50%{opacity:.5}}@keyframes pulse{0%,to{opacity:1}50%{opacity:.5}}@-webkit-keyframes bounce{0%,to{transform:translateY(-25%);-webkit-animation-timing-function:cubic-bezier(.8,0,1,1);animation-timing-function:cubic-bezier(.8,0,1,1)}50%{transform:translateY(0);-webkit-animation-timing-function:cubic-bezier(0,0,.2,1);animation-timing-function:cubic-bezier(0,0,.2,1)}}@keyframes bounce{0%,to{transform:translateY(-25%);-webkit-animation-timing-function:cubic-bezier(.8,0,1,1);animation-timing-function:cubic-bezier(.8,0,1,1)}50%{transform:translateY(0);-webkit-animation-timing-function:cubic-bezier(0,0,.2,1);animation-timing-function:cubic-bezier(0,0,.2,1)}}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
</style>--}}
<style>
body{
    margin:0;
    font-family:sans-serif;
    font-size:1rem;
    background-color:#e3f2fd
}
.d-flex{display:flex}
.root{
    flex-direction:column;
    height:100vh;
    justify-content:center;
    align-items:center
}
.ico{
    background:no-repeat;
    padding:3rem;
    margin-bottom:1rem
}
h1{
    font-size:1.5rem;
    margin-top:0;
    margin-bottom:.5rem;
    font-weight:600;
    line-height:1.2;
    text-align:center
}
a{
    text-decoration:none;
    display:inline-block;
    font-weight:400;
    color:#fff;
    text-align:center;
    vertical-align:middle;
    -webkit-user-select:none;
    -moz-user-select:none;
    -ms-user-select:none;
    user-select:none;
    background-color:#007bff;
    border:1px solid #007bff;
    padding:.375rem .75rem;
    font-size:1rem;
    line-height:1.5;
    border-radius:.25rem
}
a:hover{
    background-color:#0069d9;
    border-color:#0062cc
}
</style>
</head>
    <body>
        <div class="d-flex root">
            <div class="ico" style="background-image:url(&quot;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' enable-background='new 0 0 48 48' viewBox='0 0 48 48'%3E%3Cpath d='M43,48H5c-2.757,0-5-2.243-5-5V5c0-2.757,2.243-5,5-5h38c2.757,0,5,2.243,5,5v38C48,45.757,45.757,48,43,48z M5,2 C3.346,2,2,3.346,2,5v38c0,1.654,1.346,3,3,3h38c1.654,0,3-1.346,3-3V5c0-1.654-1.346-3-3-3H5z'/%3E%3Cpath d='M47 10H1c-.552 0-1-.447-1-1s.448-1 1-1h46c.552 0 1 .447 1 1S47.552 10 47 10zM6 6C5.87 6 5.74 5.97 5.62 5.92 5.5 5.87 5.39 5.8 5.29 5.71 5.2 5.609 5.13 5.5 5.08 5.38 5.03 5.26 5 5.13 5 5s.03-.26.08-.38C5.13 4.5 5.2 4.39 5.29 4.29 5.39 4.2 5.5 4.13 5.62 4.08c.37-.16.81-.07 1.09.21C6.8 4.39 6.87 4.5 6.92 4.62 6.97 4.74 7 4.87 7 5S6.97 5.26 6.92 5.38C6.87 5.5 6.8 5.609 6.71 5.71 6.61 5.8 6.5 5.87 6.38 5.92S6.13 6 6 6zM10 6C9.87 6 9.74 5.97 9.62 5.92 9.5 5.87 9.39 5.8 9.29 5.71 9.2 5.609 9.13 5.5 9.08 5.38 9.03 5.26 9 5.13 9 5s.03-.26.08-.38C9.13 4.5 9.2 4.39 9.29 4.29 9.39 4.2 9.5 4.13 9.62 4.08c.37-.16.81-.07 1.09.21.09.1.16.21.21.33C10.97 4.74 11 4.87 11 5s-.03.26-.08.38c-.05.12-.12.229-.21.33-.1.09-.21.16-.33.21S10.13 6 10 6zM36 35H12c-.552 0-1-.447-1-1s.448-1 1-1h24c.552 0 1 .447 1 1S36.552 35 36 35zM33 39H15c-.552 0-1-.447-1-1s.448-1 1-1h18c.552 0 1 .447 1 1S33.552 39 33 39zM25 31h-2c-1.654 0-3-1.346-3-3v-8c0-1.654 1.346-3 3-3h2c1.654 0 3 1.346 3 3v8C28 29.654 26.654 31 25 31zM23 19c-.551 0-1 .448-1 1v8c0 .552.449 1 1 1h2c.551 0 1-.448 1-1v-8c0-.552-.449-1-1-1H23zM17 27h-6c-.328 0-.635-.161-.822-.431-.187-.27-.229-.613-.115-.921l3-8c.195-.517.771-.775 1.288-.585.517.194.779.771.585 1.288L12.443 25H17c.552 0 1 .447 1 1S17.552 27 17 27z'/%3E%3Cpath d='M16 31c-.552 0-1-.447-1-1v-8c0-.553.448-1 1-1s1 .447 1 1v8C17 30.553 16.552 31 16 31zM37 27h-6c-.328 0-.635-.161-.822-.431-.187-.27-.229-.613-.115-.921l3-8c.195-.517.771-.775 1.288-.585.517.194.779.771.585 1.288L32.443 25H37c.552 0 1 .447 1 1S37.552 27 37 27z'/%3E%3Cpath d='M36,31c-0.552,0-1-0.447-1-1v-8c0-0.553,0.448-1,1-1s1,0.447,1,1v8C37,30.553,36.552,31,36,31z'/%3E%3C/svg%3E&quot;);"></div>
            <h1>{{-- @yield('code') --}}@yield('message')</h1>
            <a href="{{ url('/') }}">Back to Hone</a>
        </div>
    </body>
</html>
