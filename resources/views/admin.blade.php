<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="adminApp"> {{-- DEV: Set lang from db data --}}
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
{{-- <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover"> --}}
<meta name="robots" content="none,nosnippet,noarchive,noimageindex">
<meta name="googlebot" content="none,nosnippet,noarchive,noimageindex">
<meta name="AdsBot-Google" content="none,nosnippet,noarchive,noimageindex">
<meta name="googlebot-news" content="none,nosnippet,noarchive,noimageindex">
<meta name="bing" content="none,nosnippet,noarchive,noimageindex">
<meta name="baidu" content="none,nosnippet,noarchive,noimageindex">
<meta name="description" content="Programmer App Designer">
<meta name="author" content="Programmeria">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta name="mobile-web-app-capable" content="yes">{{-- Chrome Android --}}
<meta name="apple-mobile-web-app-capable" content="yes">{{-- Safari fullscreen --}}
<meta name="msapplication-TileColor" content="#e3f2fd">
<meta name="theme-color" content="#e3f2fd">
{{--<meta name="_token" content="{{ csrf_token() }}">--}}
<base href="{{ url('/') }}">
<link rel="shortcut icon" href="/favicon.ico">
<title>Programmeria App Designer</title>
<style id="inStyle1">{{-- /* NOTES: Move html & body style here */ --}}
body:not(.modal-open):not(.body-iframe){
  overflow-y:scroll
}
#QloadStartUp{
	position:fixed
}
.load-spin,
.nojs{
  display:flex;
  flex-direction:column;
  justify-content:center;
  align-items:center;
  width:100%;
  height:100%
}
.load-spin{
  background:no-repeat 50% rgba(255,255,255,.5);
  user-select:none;
  z-index:1020;
  cursor:wait
}
.svg-spin{
  -webkit-animation:loads 2s linear infinite;
  animation:loads 2s linear infinite;
  z-index:2
}
.svg-spin circle{
  stroke-dasharray:1,150;
  stroke-dashoffset:0;
  stroke-linecap:round;
  -webkit-animation:dashLoad 1.5s ease-in-out infinite;
  animation:dashLoad 1.5s ease-in-out infinite
}
.load-spin:not(.withFill) circle{
  fill:none
}
.load-spin:not(.withStroke) circle{
  stroke:#25A7FA
}
@keyframes loads{
  100%{transform:rotate(360deg)}
}
@-webkit-keyframes loads{
  100%{-webkit-transform:rotate(360deg)}
}
@keyframes dashLoad{
  0%{
    stroke-dasharray:1,150;
    stroke-dashoffset:0
  }
  50%{
    stroke-dasharray:90,150;
    stroke-dashoffset:-35
  }
  100%{
    stroke-dasharray:90,150;
    stroke-dashoffset:-124
  }
}
@-webkit-keyframes dashLoad{
  0%{
    stroke-dasharray:1,150;
    stroke-dashoffset:0
  }
  50%{
    stroke-dasharray:90,150;
    stroke-dashoffset:-35
  }
  100%{
    stroke-dasharray:90,150;
    stroke-dashoffset:-124
  }
}
.bg-programmeria-logo,
.nojs::before{
	background-image:url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 47.5 47.5' style='enable-background:new 0 0 47.5 47.5;'%3E%3Cdefs id='defs6'%3E%3CclipPath id='clipPath16' clipPathUnits='userSpaceOnUse'%3E%3Cpath id='path18' d='M 0,38 38,38 38,0 0,0 0,38 Z'/%3E%3C/clipPath%3E%3C/defs%3E%3Cg transform='matrix(1.25,0,0,-1.25,0,47.5)' id='g10'%3E%3Cg id='g12'%3E%3Cg clip-path='url(%23clipPath16)' id='g14'%3E%3Cg transform='translate(37,5)' id='g20'%3E%3Cpath id='path22' style='fill:%233b88c3;fill-opacity:1;fill-rule:nonzero;stroke:none' d='m 0,0 c 0,-2.209 -1.791,-4 -4,-4 l -28,0 c -2.209,0 -4,1.791 -4,4 l 0,28 c 0,2.209 1.791,4 4,4 l 28,0 c 2.209,0 4,-1.791 4,-4 L 0,0 Z'/%3E%3C/g%3E%3Cg transform='translate(16.4282,19.4102)' id='g24'%3E%3Cpath id='path26' style='fill:%23ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none' d='m 0,0 3.162,0 c 1.954,0 3.194,1.426 3.194,3.287 0,1.86 -1.24,3.286 -3.194,3.286 L 0,6.573 0,0 Z m -4.651,8.248 c 0,1.457 0.868,2.418 2.419,2.418 l 5.487,0 c 4.559,0 7.938,-2.977 7.938,-7.41 0,-4.527 -3.504,-7.349 -7.752,-7.349 l -3.441,0 0,-5.085 c 0,-1.551 -0.992,-2.418 -2.326,-2.418 -1.333,0 -2.325,0.867 -2.325,2.418 l 0,17.426 z'/%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/svg%3E");
	background-repeat:no-repeat
}
.bg-size-36{
  background-size:36px
}
.i-load:after,
.position-full{
  top:0;
  right:0;
  bottom:0;
  left:0
}

.i-load:after{
  content:"";
  background:no-repeat center rgba(255,255,255,.4) url("data:image/svg+xml,%3Csvg width='40' height='40' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100' enable-background='new 0 0 0 0'%3E%3Cpath fill='%2325A7FA' d='M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50' transform='rotate(149.85 50 50)'%3E%3CanimateTransform attributeName='transform' attributeType='XML' type='rotate' dur='0.75s' from='0 50 50' to='360 50 50' repeatCount='indefinite'%3E%3C/animateTransform%3E%3C/path%3E%3C/svg%3E");
  background-size:var(--bg-i-load);
  display:block;
  position:absolute;
  z-index:2
}

{{-- .bg-load:before{
  content:"";
  background:no-repeat center url('data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 44 44"%3E%3Cstyle%3Esvg%7Bz-index:2;stroke-dasharray:1,150;stroke-dashoffset:0;stroke-linecap:round;-webkit-animation:dashLoad 1.5s ease-in-out infinite;animation:dashLoad 1.5s ease-in-out infinite%7Dcircle%7Bfill:none;stroke:%2325A7FA%7D@keyframes loads%7B100%25%7Btransform:rotate(360deg)%7D%7D@-webkit-keyframes loads%7B100%25%7B-webkit-transform:rotate(360deg)%7D%7D@keyframes dashLoad%7B0%25%7Bstroke-dasharray:1,150;stroke-dashoffset:0%7D50%25%7Bstroke-dasharray:90,150;stroke-dashoffset:-35%7D100%25%7Bstroke-dasharray:90,150;stroke-dashoffset:-124%7D%7D@-webkit-keyframes dashLoad%7B0%25%7Bstroke-dasharray:1,150;stroke-dashoffset:0%7D50%25%7Bstroke-dasharray:90,150;stroke-dashoffset:-35%7D100%25%7Bstroke-dasharray:90,150;stroke-dashoffset:-124%7D%7D%3C/style%3E%3Ccircle cx="22" cy="22" r="20" stroke-width="4"%3E%3C/circle%3E%3C/svg%3E');
  background-size:auto;
  display:block;
  position:absolute;
  top:0;
  left:0;
  right:0;
  bottom:0;
  z-index:2
} --}}
</style>
</head>
<body>{{--  class="text-sm" adm --}}
<div id="root" hidden></div>
<div id="QportalApp"></div>
<noscript>
<style>
body{
  margin:0;
  {{-- font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
  font-size:1rem;
  font-weight:400;
  line-height:1.5;
  color:#212529;
  text-align:left;
  background-color:#fff --}}
}
#QloadStartUp{
	display:none
}
.nojs{
	font-family:sans-serif;
	height:100vh;
	background-color:#e3f2fd {{-- #83bdfb --}}
}
.nojs::before{
	background-size:45px;
	background-position:center 5px;
  padding-top:55px;
	font-size:1.5rem;
	display:block
}
[lang=id] .nojs::before{
	content:"Anda harus mengaktifkan JavaScript untuk menjalankan aplikasi ini."
}
[lang=en] .nojs::before{
	content:"You need to enable JavaScript to run this app."
}
.nojs::after{
	display:block;
	margin:9px 0;
}
[lang=id] .nojs::after{
	content:"Harap aktifkan JavaScript di pengaturan browser Anda."
}
[lang=en] .nojs::after{
	content:"Please enable JavaScript in your browser setting."
}
.nojs a{
	display:inline-block;
	order:3;
	font-weight:700
}
</style>
<div class="nojs">
	<a href="https://www.enablejavascript.io/" data-nosnippet="true" rel="noopener noreferrer nofollow" target="_blank">GUIDE</a>
</div>
</noscript>

<div id="QloadStartUp" class="load-spin position-full bg-programmeria-logo bg-size-36" role="status">
	<svg class="svg-spin" width="86" height="86" viewBox="0 0 44 44">
		<circle cx="22" cy="22" r="20" stroke-width="4"></circle>
	</svg>
</div>

{{-- <script src="https://polyfill.io/v3/polyfill.min.js?features=es2017%2Cdefault%2Ces2015%2Ces2016%2CIntl" crossorigin="anonymous"></script> --}} {{-- Setup `default`, `es2015`, `es2016`, `es2017`, and `Intl` polyfills from https://cdn.polyfill.io/v3/url-builder/ --}}
<script src="/js/libs/bowser.js?v={{ config('app.version') }}" data-js="bowser"></script> {{-- nonce="{{csrf_token()}}" --}}
<script src="/js/Q.js?v={{ config('app.version') }}" data-js="Q"></script>
{{-- <script src="/js/libs/screenfull.js" crossorigin="anonymous"></script> --}}

<script id="QstartupCode">
(function(){
  try{// globalThis
    new Function(`import("/js/esm/tryEsm.js").then(m => {
      window.importShim = function(m){return import(m)}
      Q.getScript({tag:"link",rel:"stylesheet",href:"/css/app.css?v={{ config('app.version') }}"},"head").then(() => {
        Q.getScript({tag:"link",rel:"stylesheet",href:"/css/admin/admin-ui.css?v={{ config('app.version') }}"},"head").then(() => {
          Q.domQ("#root").removeAttribute("hidden");
        });
      })
    })`)();
  }catch(e){
    Q.getScript({src:"/js/libs/polyfill/es-module-shims.min.js?v={{ config('app.version') }}","data-js":"importShim"}).then(function(){
      Q.getScript({tag:"link",rel:"stylesheet",href:"/css/app.css?v={{ config('app.version') }}"},"head").then(function(){
        Q.getScript({tag:"link",rel:"stylesheet",href:"/css/admin/admin-ui.css?v={{ config('app.version') }}"}),"head".then(function(){
          Q.domQ("#root").removeAttribute("hidden");
        });
      })
    }).catch(function(e){console.log('ERROR load polyfill es module dynamic import: ',e)});
  }
  // Q.getScript({tag:"link",rel:"stylesheet",href:"/storage/fonts/q-icon-v1.0/style.css"},"head");
  
  /* DEVS: SET previous URL to back in error page */
  // let urlNow = window.location.href;
  // if(!localStorage.prevURL || localStorage.prevURL !== urlNow){
  //   localStorage.setItem("prevURL",urlNow);
  // }
  // console.log(localStorage.prevURL);

  {{-- if('BroadcastChannel' in (self || window)){
    // DEV: Broadcast Channel API
    const bcApi = new BroadcastChannel('programmeria_channel');

    // bcApi.onmessage = (e) => {
    //   // If our broadcast message is 'update_title' then get the new title from localStorage
    //   if (e.data === 'update_title') {
    //     // localStorage is domain specific so when it changes in one window it changes in the other
    //     setTitle(localStorage.getItem('title'));
    //   }
    // }

    window.bcApi = bcApi;
  } --}}
}());
</script>
<script async defer importance="low" src="{{ asset('/js/admin/Admin.js') }}?v={{ config('app.version') }}"></script>
</body></html>
