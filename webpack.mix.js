const mix = require('laravel-mix');
// require('laravel-mix-react-css-modules');// DEV OPTION: https://github.com/leinelissen/laravel-mix-react-css-modules

const IS_ADMIN = true; // true | false

/*--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files. */

// ORI
// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);

// mix.disableNotifications(); // Disable Notifications: 

// mix.webpackConfig({
//     output: {
//         chunkFilename: 'js/chunk/[name].js'
//     }
// });

if (mix.inProduction()) {
    mix.webpackConfig({
        output: {
            chunkFilename: 'js/chunks/[name].[chunkhash].chunk.js' // 'js/chunk/[name].[chunkhash].chunk.js'
        }
    });
} else {
    mix.webpackConfig({
        output: {
            chunkFilename: 'js/chunks/[name].chunk.js' // 'js/chunk/[name].chunk.js'
        }
    });
}

if(IS_ADMIN){
    // ====== Admin App ======
    mix.react('resources/js/Admin.js', 'public/js/admin').sourceMaps(false,'source-map')
        .sass('resources/scss/admin-ui/admin-ui.scss', 'public/css/admin') // Admin App
        .sass('resources/scss/app.scss', 'public/css') // Front App (OPTION: USAGE in Admin App, NOTE: separate if too much)
        .sass('resources/scss/preload/preload.scss', 'public/css/preload'); // preload (NOT IMPORTANT / USE LATER)
        // .sass('resources/scss/frameworks/antd/antd.scss', 'public/css/frameworks');
        // .reactCSSModules();
}else{
    // ====== Front App ======
    mix.react('resources/js/App.js', 'public/js').sourceMaps(false,'source-map')
        // .react('resources/js/Admin.js', 'public/js/admin').sourceMaps(false,'source-map') 
        .sass('resources/scss/app.scss', 'public/css') // Front App
        .sass('resources/scss/admin-ui/admin-ui.scss', 'public/css/admin') // Admin App
        .sass('resources/scss/preload/preload.scss', 'public/css/preload'); // preload (NOT IMPORTANT / USE LATER)
        // .sass('resources/scss/frameworks/antd/antd.scss', 'public/css/frameworks');
        // .reactCSSModules();
}

// ======================================================================================================================================================

// https://github.com/leinelissen/laravel-mix-react-css-modules
// It is also possible to modify the way a classname is generated, by supplying a new syntax to the function. Check this to see how the syntax works.
// DEFAULT: [name]__[local]___[hash:base64:5]
// mix.react('resources/assets/app.js', 'public/js')
   // .reactCSSModules('[path]__[name]___[hash:base64]');